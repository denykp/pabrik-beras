VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmMasterProses 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Proses"
   ClientHeight    =   7995
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7950
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7995
   ScaleWidth      =   7950
   Begin VB.CommandButton cmdMasterBahan 
      Caption         =   "Master Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4485
      Picture         =   "frmMasterProses.frx":0000
      TabIndex        =   35
      Top             =   135
      Width           =   1395
   End
   Begin VB.ComboBox cmbJenisMesin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterProses.frx":0102
      Left            =   1830
      List            =   "frmMasterProses.frx":010C
      TabIndex        =   2
      Text            =   "cmbJenisMesin"
      Top             =   1050
      Width           =   2415
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3060
      Picture         =   "frmMasterProses.frx":0122
      TabIndex        =   9
      Top             =   7380
      Width           =   1290
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1830
      TabIndex        =   1
      Top             =   585
      Width           =   5865
   End
   Begin VB.Frame Frame1 
      Caption         =   "Input"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1305
      Left            =   135
      TabIndex        =   21
      Top             =   1455
      Width           =   7590
      Begin VB.TextBox txtField 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   1395
         TabIndex        =   4
         Top             =   780
         Width           =   1140
      End
      Begin VB.CommandButton cmdSearchInput 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2985
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   22
         Top             =   330
         Width           =   420
      End
      Begin VB.TextBox txtField 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   1395
         TabIndex        =   3
         Top             =   345
         Width           =   1530
      End
      Begin VB.Label lblSatuanInput 
         Caption         =   "satuan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2640
         TabIndex        =   30
         Top             =   825
         Width           =   1050
      End
      Begin VB.Label Label1 
         Caption         =   "Qty "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   150
         TabIndex        =   25
         Top             =   825
         Width           =   600
      End
      Begin VB.Label Label19 
         Caption         =   "Kode Bahan "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   150
         TabIndex        =   24
         Top             =   375
         Width           =   1275
      End
      Begin VB.Label lblNamaBahanInput 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "nama"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3510
         TabIndex        =   23
         Top             =   375
         Width           =   480
      End
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1830
      TabIndex        =   0
      Top             =   150
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6090
      TabIndex        =   19
      Top             =   255
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      Caption         =   "Output"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Left            =   135
      TabIndex        =   14
      Top             =   2880
      Width           =   7590
      Begin VB.Frame FrPlong 
         BorderStyle     =   0  'None
         Height          =   600
         Left            =   3915
         TabIndex        =   32
         Top             =   540
         Width           =   3555
         Begin VB.TextBox txtQtyPlong 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   2220
            TabIndex        =   33
            Top             =   165
            Width           =   990
         End
         Begin VB.Label Label4 
            Caption         =   "Faktor kali hasil plong"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   225
            TabIndex        =   34
            Top             =   195
            Width           =   1980
         End
      End
      Begin VB.CommandButton cmdSearchBrg 
         BackColor       =   &H8000000C&
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2985
         Picture         =   "frmMasterProses.frx":0224
         TabIndex        =   27
         Top             =   270
         Width           =   420
      End
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   465
         Left            =   90
         ScaleHeight     =   465
         ScaleWidth      =   3870
         TabIndex        =   20
         Top             =   1140
         Width           =   3870
         Begin VB.CommandButton cmdDelete 
            BackColor       =   &H8000000C&
            Caption         =   "&Hapus"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2595
            TabIndex        =   12
            Top             =   45
            Width           =   1050
         End
         Begin VB.CommandButton cmdClear 
            BackColor       =   &H8000000C&
            Caption         =   "&Baru"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1440
            TabIndex        =   11
            Top             =   45
            Width           =   1050
         End
         Begin VB.CommandButton cmdOk 
            BackColor       =   &H8000000C&
            Caption         =   "&Tambahkan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   7
            Top             =   45
            Width           =   1350
         End
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1425
         TabIndex        =   6
         Top             =   690
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1425
         TabIndex        =   5
         Top             =   270
         Width           =   1500
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2520
         Left            =   105
         TabIndex        =   26
         Top             =   1695
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   4445
         _Version        =   393216
         Cols            =   6
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label14 
         Caption         =   "Kode Bahan "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   165
         TabIndex        =   18
         Top             =   315
         Width           =   1830
      End
      Begin VB.Label Label3 
         Caption         =   "Qty "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   165
         TabIndex        =   17
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBahanOutput 
         AutoSize        =   -1  'True
         Caption         =   "nama"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3495
         TabIndex        =   16
         Top             =   300
         Width           =   480
      End
      Begin VB.Label lblSatuanOutput 
         Caption         =   "satuan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2655
         TabIndex        =   15
         Top             =   720
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3780
      Picture         =   "frmMasterProses.frx":0326
      TabIndex        =   13
      Top             =   135
      Width           =   420
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      Picture         =   "frmMasterProses.frx":0428
      TabIndex        =   10
      Top             =   7380
      Width           =   1290
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1710
      Picture         =   "frmMasterProses.frx":052A
      TabIndex        =   8
      Top             =   7380
      Width           =   1290
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Jenis Mesin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   31
      Top             =   1050
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   29
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   28
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterProses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim foc As Byte
Dim totalQty As Integer
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Const querybrg As String = "select kode_bahan,nama_bahan, Kategori, Satuan from ms_bahan"



Private Sub cmbJenisMesin_Change()
    If UCase(cmbJenisMesin.text) = "POTONG" Then
        FrPlong.Visible = True
    Else
        FrPlong.Visible = False
    End If
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuanOutput = ""
    LblNamaBahanOutput = ""
    txtQty.text = "1"
    txtQtyPlong.text = "1"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""

    row = flxGrid.row

    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.col - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = 4
    txtKdBrg.text = ""
    lblSatuanOutput = ""
    txtQty.text = "1"
    txtQtyPlong.text = "1"
    LblNamaBahanOutput = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False

End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub



Private Sub cmdMasterBahan_Click()
    frmMasterBarang.Show vbModal
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBahanOutput <> "" Then
    
        If Not IsNumeric(txtQty.text) Or Len(Trim(txtQty.text)) = 0 Or Val(txtQty.text) <= 0 Then
            txtQty.text = ""
            MsgBox "Qty harus lebih besar dari nol!", vbExclamation
            txtQty.SetFocus
            Exit Sub
        End If

        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBahanOutput
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        flxGrid.TextMatrix(row, 4) = lblSatuanOutput
        flxGrid.TextMatrix(row, 5) = txtQtyPlong
        
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 4
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        txtKdBrg.text = ""
        lblSatuanOutput = ""
        LblNamaBahanOutput = ""
        txtQty.text = "1"
        txtQtyPlong.text = "1"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.Rows - 2
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearchInput_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmMasterProses"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = 2
    frmSearch.proc = "search_input"

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub search_input()
On Error Resume Next
    If cek_kodebarang Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmMasterProses"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_proses,nama_proses,kode_bahan,qty,jenis_proses  from ms_prosesh"
    frmSearch.nmform = "frmMasterProses"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_proses"
    frmSearch.col = 0
    frmSearch.Index = 0

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub



Private Sub cmdSimpan_Click()
Dim i As Integer
Dim KodeProses As String
Dim row As Integer

On Error GoTo err
    If txtField(0).text = "" Then
        MsgBox "Silahkan mengisi kode proses terlebih dahulu!"
        txtField(0).SetFocus
        Exit Sub
    End If
    If txtField(2).text = "" Then
        MsgBox "Silahkan mengisi kode bahan terlebih dahulu!"
        txtField(2).SetFocus
        Exit Sub
    End If
    If txtField(3).text = "" Then
        MsgBox "Silahkan mengisi qty bahan terlebih dahulu!"
        txtField(3).SetFocus
        Exit Sub
    End If
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Sub
    End If
    
   
    KodeProses = txtField(0).text
    conn.ConnectionString = strcon
    conn.Open

    If chkNumbering <> "1" Then
    rs.Open "select * from ms_prosesh where kode_proses='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Kode proses " & txtField(0).text & " sudah digunakan, silahkan menggunakan kode yang lain"
        rs.Close
        DropConnection
        Exit Sub
    End If
    rs.Close
    End If
    
    conn.BeginTrans
    If txtField(0).text <> "" Then
        conn.Execute "delete from ms_prosesH where kode_proses ='" & txtField(0).text & "'"
        conn.Execute "delete from ms_prosesd where kode_proses ='" & txtField(0).text & "'"
    End If
    
    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next

    conn.CommitTrans
    
    DropConnection

    MsgBox "Data sudah tersimpan"
    reset_form

    Call MySendKeys("{tab}")
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    If KodeProses <> txtField(0).text Then txtField(0).text = KodeProses
    MsgBox err.Description
End Sub

Public Sub reset_form()
Dim i As Byte
    txtField(0).text = "-"
    For i = 0 To 3
        txtField(i).text = ""
    Next
    cmdClear_Click
    cmdSimpan.Enabled = True
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    lblNamaBahanInput.Caption = ""
    LblNamaBahanOutput.Caption = ""
    lblSatuanInput.Caption = ""
    lblSatuanOutput.Caption = ""
    If cmbJenisMesin.ListCount > 0 Then cmbJenisMesin.ListIndex = 0
'    txtField(0).SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "ms_prosesh"
    fields(0) = "kode_proses"
    fields(1) = "nama_proses"
    fields(2) = "kode_bahan"
    fields(3) = "qty"
    fields(4) = "jenis_proses"
     
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = txtField(2).text
    nilai(3) = Replace(txtField(3).text, ",", ".")
    nilai(4) = cmbJenisMesin.text
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(4)
    ReDim nilai(4)

    table_name = "ms_prosesd"
    fields(0) = "kode_proses"
    fields(1) = "kode_bahan"
    fields(2) = "qty_hasil"
    fields(3) = "no_urut"

    nilai(0) = txtField(0).text
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = Replace(flxGrid.TextMatrix(row, 3), ",", ".")
    nilai(3) = row

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select h.*,b.nama_bahan,b.satuan from ms_prosesh h " & _
            "inner join ms_bahan b on b.kode_bahan=h.kode_bahan " & _
            "where kode_proses='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        txtField(1).text = rs("nama_proses")
        txtField(2).text = rs("kode_bahan")
        txtField(3).text = rs("qty")
        SetComboText rs!jenis_proses, cmbJenisMesin
        lblNamaBahanInput = rs("nama_bahan")
        lblSatuanInput = rs("satuan")
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.kode_bahan,m.nama_bahan,d.qty_hasil as qty,m.satuan from ms_prosesd d inner join ms_bahan m on d.kode_bahan=m.kode_bahan where d.kode_proses='" & txtField(0).text & "' order by d.no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs("kode_bahan")
                flxGrid.TextMatrix(row, 2) = rs("nama_bahan")
                flxGrid.TextMatrix(row, 3) = rs("qty")
                flxGrid.TextMatrix(row, 4) = rs("satuan")
                row = row + 1
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBahanOutput = flxGrid.TextMatrix(flxGrid.row, 2)
                End If
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
            txtQtyPlong.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtQty.SetFocus
        End If

End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        txtQty.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        loaddetil
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub

Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
'    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    conn.Open strcon
    cmbJenisMesin.Clear
    cmbJenisMesin.AddItem ""
    rs.Open "select * from var_jenis order by jenis_proses", conn
        While Not rs.EOF
            cmbJenisMesin.AddItem rs(0)
            cmbJenisMesin.Refresh
        rs.MoveNext
        Wend
    rs.Close
    conn.Close
    
    reset_form
    total = 0

    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 3800
    flxGrid.ColWidth(3) = 900
    flxGrid.ColWidth(4) = 1200
    flxGrid.ColWidth(5) = 0
    flxGrid.TextMatrix(0, 1) = "Kode"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "nama"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 4) = "Satuan"
    flxGrid.TextMatrix(0, 5) = "Qty Plong"
End Sub

Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdBrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdBrg = txtKdBrg
    rs.Open "select nama_bahan,satuan from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBahanOutput = rs(0)
        lblSatuanOutput = rs(1)
        cek_kodebarang1 = True
        rs.Close
    Else
        LblNamaBahanOutput = ""
        lblSatuanOutput = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_kodebarang() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    If InStr(1, txtField(2), "*") > 0 Then kdBrg = Mid(txtField(2), InStr(1, txtField(2), "*") + 1, Len(txtField(2))) Else kdBrg = txtField(2)
    rs.Open "select nama_bahan,satuan from ms_bahan where [kode_bahan]='" & txtField(2) & "'", conn
    If Not rs.EOF Then
        lblNamaBahanInput = rs(0)
        lblSatuanInput = rs(1)
        cek_kodebarang = True
        rs.Close
    Else
        lblNamaBahanInput = ""
        lblSatuanInput = ""
        cek_kodebarang = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 And Index = 2 Then cmdSearchInput_Click
End Sub
Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cek_notrans
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cek_notrans
End Sub
Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQtyPlong_GotFocus()
    txtQtyPlong.SelStart = 0
    txtQtyPlong.SelLength = Len(txtQtyPlong.text)
End Sub

Private Sub txtQtyPlong_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub


