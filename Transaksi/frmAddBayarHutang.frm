VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT3N.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAddBayarHutang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PembayaranHutang"
   ClientHeight    =   9000
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   14010
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   14010
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5325
      Picture         =   "frmAddBayarHutang.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   105
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4230
      Picture         =   "frmAddBayarHutang.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   104
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.Frame frTab 
      BorderStyle     =   0  'None
      Height          =   4290
      Index           =   0
      Left            =   360
      TabIndex        =   49
      Top             =   2655
      Visible         =   0   'False
      Width           =   7665
      Begin VB.TextBox txtNoFaktur 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1395
         TabIndex        =   4
         Top             =   135
         Width           =   1875
      End
      Begin VB.TextBox txtNoInv 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1395
         TabIndex        =   5
         Top             =   540
         Width           =   1875
      End
      Begin VB.CommandButton cmdSearchInv 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3345
         Picture         =   "frmAddBayarHutang.frx":0A64
         TabIndex        =   66
         Top             =   540
         Width           =   375
      End
      Begin VB.TextBox txtBayar 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         TabIndex        =   6
         Text            =   "0"
         Top             =   1215
         Width           =   1635
      End
      Begin VB.CommandButton cmdAddJual 
         Caption         =   "Tambah / Ubah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3870
         TabIndex        =   7
         Top             =   540
         Width           =   1770
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid2 
         Height          =   2355
         Left            =   180
         TabIndex        =   10
         Top             =   1800
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   4154
         _Version        =   393216
         Cols            =   6
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label20 
         Caption         =   "No Faktur"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   91
         Top             =   135
         Width           =   1050
      End
      Begin VB.Label lblInvoice 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5370
         TabIndex        =   65
         Top             =   1275
         Visible         =   0   'False
         Width           =   1545
      End
      Begin VB.Label lbltanggal 
         Caption         =   "Tanggal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3510
         TabIndex        =   62
         Top             =   915
         Width           =   1545
      End
      Begin VB.Label Label23 
         Caption         =   "Bayar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   61
         Top             =   1260
         Width           =   1050
      End
      Begin VB.Label Label22 
         Caption         =   "Sisa Hutang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   60
         Top             =   915
         Width           =   1050
      End
      Begin VB.Label Label21 
         Caption         =   "ID"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   59
         Top             =   540
         Width           =   1050
      End
      Begin VB.Label lblSisaHutang 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1440
         TabIndex        =   58
         Top             =   915
         Width           =   1650
      End
   End
   Begin VB.Frame frTab 
      BorderStyle     =   0  'None
      Height          =   3345
      Index           =   1
      Left            =   405
      TabIndex        =   50
      Top             =   2655
      Visible         =   0   'False
      Width           =   7530
      Begin VB.CommandButton cmdAddRetur 
         Caption         =   "Tambah / Ubah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3465
         TabIndex        =   9
         Top             =   135
         Width           =   1680
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid1 
         Height          =   2040
         Left            =   135
         TabIndex        =   16
         Top             =   990
         Width           =   6180
         _ExtentX        =   10901
         _ExtentY        =   3598
         _Version        =   393216
         Cols            =   3
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.ComboBox cmbRetur 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1395
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   180
         Width           =   1950
      End
      Begin VB.Label Label18 
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   57
         Top             =   585
         Width           =   1050
      End
      Begin VB.Label Label15 
         Caption         =   "No Retur"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   56
         Top             =   225
         Width           =   1050
      End
      Begin VB.Label lblRetur 
         Caption         =   "Label15"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1395
         TabIndex        =   55
         Top             =   585
         Width           =   1545
      End
   End
   Begin VB.ComboBox cmbTipe 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmAddBayarHutang.frx":0B66
      Left            =   1665
      List            =   "frmAddBayarHutang.frx":0B73
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   495
      Width           =   2310
   End
   Begin VB.TextBox txtDeposit 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   11265
      TabIndex        =   22
      Text            =   "0"
      Top             =   7020
      Width           =   2175
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting (F5)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5130
      Style           =   1  'Graphical
      TabIndex        =   64
      Top             =   8280
      Width           =   1320
   End
   Begin VB.TextBox txtTunai 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   11205
      TabIndex        =   20
      Text            =   "0"
      Top             =   5475
      Width           =   2220
   End
   Begin VB.TextBox txtSelisih 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   11250
      TabIndex        =   21
      Text            =   "0"
      Top             =   6480
      Width           =   2175
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6615
      TabIndex        =   38
      Top             =   135
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   3765
      Picture         =   "frmAddBayarHutang.frx":0B96
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   105
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTBayar 
      Height          =   330
      Left            =   1665
      TabIndex        =   2
      Top             =   1305
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   100728835
      CurrentDate     =   38927
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3150
      Picture         =   "frmAddBayarHutang.frx":0C98
      TabIndex        =   31
      Top             =   900
      Width           =   375
   End
   Begin VB.TextBox txtSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1665
      TabIndex        =   1
      Top             =   900
      Width           =   1410
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1665
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   1710
      Width           =   3570
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2115
      Picture         =   "frmAddBayarHutang.frx":0D9A
      TabIndex        =   24
      Top             =   8280
      Width           =   1320
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3645
      Picture         =   "frmAddBayarHutang.frx":0E9C
      TabIndex        =   25
      Top             =   8280
      Width           =   1320
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   585
      Picture         =   "frmAddBayarHutang.frx":0F9E
      TabIndex        =   23
      Top             =   8280
      Width           =   1320
   End
   Begin VB.Frame frTab 
      BorderStyle     =   0  'None
      Height          =   4290
      Index           =   2
      Left            =   405
      TabIndex        =   67
      Top             =   2610
      Visible         =   0   'False
      Width           =   7665
      Begin VB.TextBox txtTransfer 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1380
         TabIndex        =   18
         Text            =   "0"
         Top             =   465
         Width           =   2100
      End
      Begin VB.TextBox txtKode 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1380
         TabIndex        =   17
         Top             =   0
         Width           =   1395
      End
      Begin VB.CommandButton cmdSearchBank 
         Caption         =   "F4"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2820
         TabIndex        =   86
         Top             =   0
         Width           =   375
      End
      Begin VB.CommandButton cmdAddTransfer 
         Caption         =   "Tambah / Ubah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1395
         TabIndex        =   19
         Top             =   900
         Width           =   1770
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid3 
         Height          =   2895
         Left            =   180
         TabIndex        =   68
         Top             =   1260
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   5106
         _Version        =   393216
         Cols            =   5
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblNamaBank 
         Caption         =   "-"
         Height          =   255
         Index           =   1
         Left            =   3240
         TabIndex        =   103
         Top             =   0
         Width           =   4095
      End
      Begin VB.Label Label30 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1215
         TabIndex        =   90
         Top             =   495
         Width           =   105
      End
      Begin VB.Label Label29 
         Caption         =   "Jml. Transfer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   0
         TabIndex        =   89
         Top             =   495
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Kode Bank"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   0
         TabIndex        =   88
         Top             =   30
         Width           =   945
      End
      Begin VB.Label Label25 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1215
         TabIndex        =   87
         Top             =   30
         Width           =   105
      End
      Begin VB.Label Label38 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5370
         TabIndex        =   69
         Top             =   870
         Visible         =   0   'False
         Width           =   1545
      End
   End
   Begin VB.Frame frTab 
      BorderStyle     =   0  'None
      Height          =   4830
      Index           =   3
      Left            =   360
      TabIndex        =   70
      Top             =   2565
      Visible         =   0   'False
      Width           =   7665
      Begin VB.CommandButton cmdSearchBuku 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3105
         Picture         =   "frmAddBayarHutang.frx":10A0
         TabIndex        =   96
         Top             =   450
         Width           =   375
      End
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   45
         ScaleHeight     =   345
         ScaleWidth      =   3090
         TabIndex        =   92
         Top             =   45
         Width           =   3120
         Begin VB.OptionButton OptLunas 
            Caption         =   "Cek"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   90
            TabIndex        =   94
            Top             =   45
            Width           =   915
         End
         Begin VB.OptionButton OptLunas 
            Caption         =   "Giro"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   1575
            TabIndex        =   93
            Top             =   45
            Value           =   -1  'True
            Width           =   915
         End
         Begin VB.Label Label12 
            BackStyle       =   0  'Transparent
            Caption         =   "Jumlah bayar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   135
            TabIndex        =   95
            Top             =   1035
            Visible         =   0   'False
            Width           =   1725
         End
      End
      Begin VB.TextBox txtCek 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   1380
         TabIndex        =   14
         Text            =   "0"
         Top             =   1560
         Width           =   1740
      End
      Begin VB.TextBox txtNoCek 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1395
         MaxLength       =   10
         TabIndex        =   11
         Top             =   450
         Width           =   1650
      End
      Begin VB.TextBox txtKodeBank 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1395
         TabIndex        =   12
         Top             =   810
         Width           =   1650
      End
      Begin VB.CommandButton cmdAddCek 
         Caption         =   "Tambah / Ubah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5625
         TabIndex        =   15
         Top             =   450
         Width           =   1770
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid4 
         Height          =   2445
         Left            =   180
         TabIndex        =   71
         Top             =   2115
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   4313
         _Version        =   393216
         Cols            =   5
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComCtl2.DTPicker DCair 
         Height          =   330
         Left            =   1395
         TabIndex        =   13
         Top             =   1200
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   100728835
         CurrentDate     =   38927
      End
      Begin VB.Label lblNamaBank 
         AutoSize        =   -1  'True
         Caption         =   "Nama Bank"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   3150
         TabIndex        =   97
         Top             =   855
         Width           =   960
      End
      Begin VB.Label Label37 
         Caption         =   "Nilai Cek / BG"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   0
         TabIndex        =   79
         Top             =   1590
         Width           =   1215
      End
      Begin VB.Label Label36 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1215
         TabIndex        =   78
         Top             =   1590
         Width           =   105
      End
      Begin VB.Label Label34 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   1215
         TabIndex        =   77
         Top             =   525
         Width           =   105
      End
      Begin VB.Label Label35 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1215
         TabIndex        =   76
         Top             =   1245
         Width           =   105
      End
      Begin VB.Label Label34 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   1215
         TabIndex        =   75
         Top             =   885
         Width           =   105
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "No. Cek / BG"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   0
         TabIndex        =   74
         Top             =   510
         Width           =   1200
      End
      Begin VB.Label Label33 
         Caption         =   "Tgl. Cair"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   0
         TabIndex        =   73
         Top             =   1245
         Width           =   1035
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Bank"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   0
         TabIndex        =   72
         Top             =   870
         Width           =   1095
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5415
      Left            =   135
      TabIndex        =   48
      Top             =   2160
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   9551
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   4
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Pembelian"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Retur"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Transfer Bank"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Cek / BG"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label32 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   102
      Top             =   540
      Width           =   105
   End
   Begin VB.Label Label28 
      Caption         =   "Tipe"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   101
      Top             =   540
      Width           =   1320
   End
   Begin VB.Label Label27 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11400
      TabIndex        =   100
      Top             =   7080
      Width           =   2040
   End
   Begin VB.Label Label26 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10770
      TabIndex        =   99
      Top             =   7080
      Width           =   420
   End
   Begin VB.Label Label24 
      Caption         =   "Deposit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8640
      TabIndex        =   98
      Top             =   7080
      Width           =   1770
   End
   Begin VB.Label Label1 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   10770
      TabIndex        =   85
      Top             =   4005
      Width           =   420
   End
   Begin VB.Label lblTotalTransfer 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11175
      TabIndex        =   84
      Top             =   4005
      Width           =   2265
   End
   Begin VB.Label Label45 
      AutoSize        =   -1  'True
      Caption         =   "Total Transfer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8640
      TabIndex        =   83
      Top             =   4005
      Width           =   1755
   End
   Begin VB.Label Label46 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10770
      TabIndex        =   82
      Top             =   4500
      Width           =   420
   End
   Begin VB.Label lblTotalCek 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11175
      TabIndex        =   81
      Top             =   4500
      Width           =   2265
   End
   Begin VB.Label Label44 
      AutoSize        =   -1  'True
      Caption         =   "Total Cek/BG "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8640
      TabIndex        =   80
      Top             =   4500
      Width           =   1710
   End
   Begin VB.Label Label31 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   105
      TabIndex        =   63
      Top             =   135
      Width           =   1320
   End
   Begin VB.Label Label14 
      Caption         =   "Tunai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8625
      TabIndex        =   54
      Top             =   5520
      Width           =   2040
   End
   Begin VB.Label Label11 
      Caption         =   "Total Bayar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8625
      TabIndex        =   53
      Top             =   6045
      Width           =   1905
   End
   Begin VB.Label Label10 
      Caption         =   "Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8625
      TabIndex        =   52
      Top             =   3525
      Width           =   1320
   End
   Begin VB.Label Label3 
      Caption         =   "Piutang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8625
      TabIndex        =   51
      Top             =   3030
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Jml Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   765
      TabIndex        =   47
      Top             =   7740
      Width           =   1320
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2295
      TabIndex        =   46
      Top             =   7740
      Width           =   1185
   End
   Begin VB.Label lblTotalBayar 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11160
      TabIndex        =   45
      Top             =   6045
      Width           =   2265
   End
   Begin VB.Label Label17 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10755
      TabIndex        =   44
      Top             =   6045
      Width           =   420
   End
   Begin VB.Label Label19 
      Caption         =   "Selisih"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8625
      TabIndex        =   43
      Top             =   6540
      Width           =   1770
   End
   Begin VB.Label Label16 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10755
      TabIndex        =   42
      Top             =   6540
      Width           =   420
   End
   Begin VB.Label lblSisa 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11385
      TabIndex        =   41
      Top             =   6540
      Width           =   2040
   End
   Begin VB.Label Label13 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10755
      TabIndex        =   40
      Top             =   3030
      Width           =   420
   End
   Begin VB.Label lbltotalretur 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11160
      TabIndex        =   39
      Top             =   3525
      Width           =   2265
   End
   Begin VB.Label Label9 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   36
      Top             =   1350
      Width           =   105
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   35
      Top             =   1350
      Width           =   1320
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   34
      Top             =   1755
      Width           =   105
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   33
      Top             =   1755
      Width           =   1320
   End
   Begin VB.Label lblNmsUPPLIER 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3780
      TabIndex        =   32
      Top             =   945
      Width           =   2940
   End
   Begin VB.Label Label7 
      Caption         =   "Pemberi Hutang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   30
      Top             =   945
      Width           =   1365
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   29
      Top             =   945
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   28
      Top             =   75
      Width           =   2040
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11160
      TabIndex        =   27
      Top             =   3030
      Width           =   2265
   End
   Begin VB.Label Label1 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   10755
      TabIndex        =   26
      Top             =   3525
      Width           =   420
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
      End
   End
End
Attribute VB_Name = "frmAddBayarHutang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryJual As String = "select * from vwhutang "
Const queryRetur As String = "select * from q_returJual"
Const querybayar As String = "select * from t_bayarhutangh where left(nomer_bayarhutang,2)='PH'"

Dim total, TotalRetur, TotalBayar, totalcek, totalTransfer As Currency
Dim seltab As Byte
Dim supplier As String
Dim NoJurnal As String

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 nomer_bayarhutang from T_BAYARhutangH where left(nomer_bayarhutang,2)='PH' and substring(nomer_bayarhutang,3,2)='" & Format(DTBayar, "yy") & "' and substring(nomer_bayarhutang,5,2)='" & Format(DTBayar, "MM") & "' order by nomer_bayarhutang desc", conn
    If Not rs.EOF Then
        No = "PH" & Format(DTBayar, "yy") & Format(DTBayar, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "PH" & Format(DTBayar, "yy") & Format(DTBayar, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub


Private Sub cmdAddCek_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    
    With flxGrid4
    If txtNoCek <> "" And txtCek > 0 Then
'        For i = 1 To .Rows - 1
'            If .TextMatrix(i, 1) = txtNoCek.text Then row = i
'        Next
        If Row = 0 Then
            Row = .Rows - 1
            .Rows = .Rows + 1
        End If
        .TextMatrix(.Row, 0) = ""
        .Row = Row
        currentRow = .Row

        .TextMatrix(Row, 1) = txtNoCek.text
        .TextMatrix(Row, 2) = txtNama2
        .TextMatrix(Row, 3) = Format(DCair, "dd/MM/yyyy")
        .TextMatrix(Row, 4) = Format(txtCek, "#,##0")
        
        totalcek = totalcek + (.TextMatrix(Row, 4))
        lblTotalCek = Format(totalcek, "#,##0")
        hitungulang
        
        
        '.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            .TopRow = Row - 7
        Else
            .TopRow = 1
        End If
        txtNoCek = ""
        txtNoCek.SetFocus
        LblSisaHutang = "0"
        txtCek = "0"
        
        txtNama2 = ""
        
    End If
    mode = 1
    
End With
End Sub

Private Sub cmdAddTransfer_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    
    With flxGrid3
    If txtKode <> "" And txtTransfer > 0 Then
'        For i = 1 To .Rows - 1
'            If .TextMatrix(i, 1) = txtNoCek.text Then row = i
'        Next
        If Row = 0 Then
            Row = .Rows - 1
            .Rows = .Rows + 1
        End If
        .TextMatrix(.Row, 0) = ""
        .Row = Row
        currentRow = .Row
        Dim nama() As String
        nama = Split(lblNamaBank(1), "-")
        .TextMatrix(Row, 1) = txtKode
        .TextMatrix(Row, 2) = nama(0)
        .TextMatrix(Row, 3) = nama(1)
        .TextMatrix(Row, 4) = Format(txtTransfer, "#,##0")
        
        totalTransfer = totalTransfer + (.TextMatrix(Row, 4))
        lblTotalTransfer = Format(totalTransfer, "#,##0")
        hitungulang
        
        '.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            .TopRow = Row - 7
        Else
            .TopRow = 1
        End If
        txtKode = ""
        txtKode.SetFocus
        txtTransfer = "0"
    End If
    mode = 1
    
End With
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_bayarhutang] from t_bayarhutangh where [nomer_bayarhutang]>'" & lblNoTrans & "' order by [nomer_bayarhutang]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_bayarhutang] from t_bayarhutangh where [nomer_bayarhutang]<'" & lblNoTrans & "' order by [nomer_bayarhutang] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdSearchBuku_Click()
    If OptLunas(0).value = True Then
        frmSearch.query = "select * from vw_bukucek where jenis='Cek'"
    Else
        frmSearch.query = "select * from vw_bukucek where jenis='Giro'"
    End If
    frmSearch.nmform = "frmAddBayarhutang"
    frmSearch.nmctrl = "txtnocek"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "cek"
    frmSearch.connstr = strcon
    frmSearch.Col = 1
    frmSearch.Index = -1
    frmSearch.proc = "cek_buku"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchInv_Click()
    txtNoFaktur.text = ""
    With frmSearch
'    .query = "select nomer_transaksi,kode_supplier, nomer_referensi,tanggal_jatuhtempo,hutang,total_bayar from list_hutang where kode_supplier='" & txtSupplier & "' and (hutang-total_bayar)>0"
    .query = "select nomer_transaksi,tanggal_transaksi,kode_supplier, nomer_referensi,tanggal_jatuhtempo,hutang,total_bayar from list_hutang where (hutang-total_bayar)>0 and kode_supplier='" & txtSupplier & "'"
    .nmform = "frmAddBayarhutang"
    .nmctrl = "txtnoinv"
    .nmctrl2 = ""
    .connstr = strcon
    .keyIni = "hutanghutang"
    .Col = 0
    .Index = -1
    .proc = "cek_jual"
    .loadgrid frmSearch.query
    Set .frm = Me
    
    .Show vbModal
End With

End Sub

Private Sub cmbRetur_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim jumlah, sudahbayar As Currency
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select (piutang-total_bayar) from list_piutang_retur where nomer_transaksi='" & cmbRetur.text & "'", conn
    If Not rs.EOF Then
        lblRetur = IIf(IsNull(rs(0)), 0, rs(0))
    Else
        lblRetur = 0
    End If
    rs.Close
    conn.Close
    Set conn = Nothing
    Set rs = Nothing
End Sub

Private Sub cmdAddJual_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtNoInv.text <> "" Then
        
        For i = 1 To flxGrid2.Rows - 1
            If flxGrid2.TextMatrix(i, 1) = txtNoInv.text Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid2.Rows - 1
            flxGrid2.Rows = flxGrid2.Rows + 1
        End If
        flxGrid2.TextMatrix(flxGrid2.Row, 0) = ""
        flxGrid2.Row = Row
        currentRow = flxGrid2.Row

        flxGrid2.TextMatrix(Row, 1) = txtNoInv.text
        flxGrid2.TextMatrix(Row, 2) = lblInvoice
        flxGrid2.TextMatrix(Row, 3) = lbltanggal
        flxGrid2.TextMatrix(Row, 4) = LblSisaHutang
        If flxGrid2.TextMatrix(Row, 5) = "" Then
            flxGrid2.TextMatrix(Row, 5) = Format(txtBayar, "#,##0")
        Else
            
            total = total - (flxGrid2.TextMatrix(Row, 5))
            flxGrid2.TextMatrix(Row, 5) = Format(txtBayar, "#,##0")

        End If
        flxGrid2.Col = 0
        flxGrid2.ColSel = 5
        
        total = total + (flxGrid2.TextMatrix(Row, 5))
        lblTotal = Format(total, "#,##0")
        hitungulang
        
        'flxgrid2.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid2.TopRow = Row - 7
        Else
            flxGrid2.TopRow = 1
        End If
        txtNoInv = ""
        txtNoFaktur = ""
        
        txtNoInv.SetFocus
        LblSisaHutang = "0"
        lblInvoice = ""
        lbltanggal = ""
        txtBayar = ""
    End If
    mode = 1
    lblQty = flxGrid2.Rows - 2
End Sub

Private Sub cmdAddRetur_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If cmbRetur.text <> "" Then
        
        For i = 1 To flxGrid1.Rows - 1
            If flxGrid1.TextMatrix(i, 1) = cmbRetur.text Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid1.Rows - 1
            flxGrid1.Rows = flxGrid1.Rows + 1
        End If
        flxGrid1.TextMatrix(flxGrid1.Row, 0) = ""
        flxGrid1.Row = Row
        currentRow = flxGrid1.Row

        flxGrid1.TextMatrix(Row, 1) = cmbRetur.text
        
        If flxGrid1.TextMatrix(Row, 2) = "" Then
            flxGrid1.TextMatrix(Row, 2) = lblRetur
        Else
            
            TotalRetur = TotalRetur - (flxGrid1.TextMatrix(Row, 2))
            flxGrid1.TextMatrix(Row, 3) = lblRetur

        End If
        flxGrid1.Col = 0
        flxGrid1.ColSel = 2
        
        TotalRetur = TotalRetur + (flxGrid1.TextMatrix(Row, 2))
        lbltotalretur = Format(TotalRetur, "#,##0")
        hitungulang
        
        'flxgrid1.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid1.TopRow = Row - 7
        Else
            flxGrid1.TopRow = 1
        End If
        cmbRetur.ListIndex = 0
        cmbRetur.SetFocus
        lblRetur = "0"

    End If
    mode = 1
    lblItem = flxGrid1.Rows - 2
End Sub

Private Sub cmdHapus_Click()
On Error GoTo err
Dim mode
    mode = 0
    If MsgBox("Apakah anda yakin hendak menghapus?", vbYesNo) = vbYes Then
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    mode = 1
    conn.Execute "delete from t_bayarhutangh where nomer_bayarhutang='" & lblNoTrans & "'"
    conn.Execute "delete from t_bayarhutang_beli where nomer_bayarhutang='" & lblNoTrans & "'"
    conn.Execute "delete from t_bayarhutang_retur where nomer_bayarhutang='" & lblNoTrans & "'"
    conn.Execute "delete from t_bayarhutang_cek where nomer_bayarhutang='" & lblNoTrans & "'"
    conn.Execute "delete from t_bayarhutang_transfer where nomer_bayarhutang='" & lblNoTrans & "'"
    conn.CommitTrans
    mode = 0
    conn.Close
    MsgBox "Data sudah dihapus"
    reset_form
    End If
    Exit Sub
err:
    If mode = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPosting_Click()
'    txtselisih.text = CDbl(lblSisa.Caption)
    simpan
    Call Posting_BayarHutang(lblNoTrans, conn)
    reset_form
End Sub

Private Sub cmdSearch_Click()
    If cmbTipe.text = "Supplier" Then
        frmSearch.query = SearchSupplier
    ElseIf cmbTipe.text = "Karyawan" Then
        frmSearch.query = searchKaryawan
    ElseIf cmbTipe.text = "Ekspedisi" Then
        frmSearch.query = SearchEkspedisi
    ElseIf cmbTipe.text = "Lain lain" Then
        frmSearch.query = SearchContact
    End If
    
    frmSearch.nmform = "frmAddBayarhutang"
    frmSearch.nmctrl = "txtsupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodesupplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchBank_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_bank"
    frmSearch.nmform = "frmAddBayarhutang"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bank"
    frmSearch.proc = "cari_bank"
    frmSearch.Col = 0
    
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
'    cmdSimpan.SetFocus
End Sub

Public Sub cari_bank()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

conn.Open strcon
Dim str As String

rs.Open "select * from ms_bank where kode_bank = '" & txtKode & "'", conn
If Not rs.EOF Then
    lblNamaBank(1) = rs!nama_bank & "-" & rs!nomer_rekening
End If
rs.Close

conn.Close

End Sub

Private Sub cmdSearchID_Click()
    frmSearch.query = "select * from vw_search_hutang where left(nomer_bayarhutang,2)='PH'"
'    frmSearch.query = "select t.nomer_bayarhutang,t.tanggal_bayarhutang,t.kode_supplier,s.nama_supplier,t.keterangan,t.status_posting from t_bayarhutangh t " & _
'                      "left join ms_supplier s on s.kode_supplier=t.kode_supplier where left(t.nomer_bayarhutang,2)='PH'"
    frmSearch.nmform = "frmAddBayarhutang"
    frmSearch.nmctrl = "lblnotrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "bayarhutangH"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
 
End Sub

Public Sub cek_notrans()
Dim rs As New ADODB.Recordset, StatusPosting As String
'    reset_form
    
    conn.ConnectionString = strcon
    conn.Open
    
    LblSisaHutang = "0"
    lbltanggal = ""
    txtBayar.text = "0"
    lblInvoice = ""
    
    rs.Open "select * from t_bayarhutangh where nomer_bayarhutang='" & lblNoTrans & "'", conn
    
    If Not rs.EOF Then
        DTBayar = rs("tanggal_bayarhutang")
        txtSupplier.text = rs("kode_supplier")
        txtSelisih.text = rs!selisih
        
        txtDeposit.text = rs!deposit
        txtTunai = rs!jumlah_tunai
'        SetComboText rs!Tipe, cmbTipe
        StatusPosting = rs("status_posting")
        supplier = rs("kode_supplier")
'        DTransfer = rs("tanggaltransfer")
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        
        rs.Close
        total = 0
        flxGrid2.Rows = 1
        flxGrid2.Rows = 2
        Row = 1
        rs.Open "select nomer_beli,tanggal_beli,sisa_hutang,jumlah_bayar from t_bayarhutang_beli  " & _
                " where nomer_bayarhutang='" & lblNoTrans & "' order by no_urut", conn
        
        
        While Not rs.EOF
            flxGrid2.TextMatrix(Row, 1) = rs(0)
'            flxGrid2.TextMatrix(row, 2) = rs(1)
            flxGrid2.TextMatrix(Row, 3) = Format(rs(1), "dd/MM/yyyy")
            flxGrid2.TextMatrix(Row, 4) = Format(rs(2), "#,##0")
            flxGrid2.TextMatrix(Row, 5) = Format(rs(3), "#,##0")
            
            Row = Row + 1
            flxGrid2.Rows = flxGrid2.Rows + 1
            total = total + rs(3)
            rs.MoveNext
        Wend
        
        
        lblTotal = Format(total, "#,##0")
        rs.Close
        TotalRetur = 0
        flxGrid1.Rows = 1
        flxGrid1.Rows = 2
        Row = 1
        rs.Open "select d.nomer_retur, d.jumlah from t_bayarhutang_retur d where d.nomer_bayarhutang='" & lblNoTrans & "'", conn
        While Not rs.EOF
            flxGrid1.TextMatrix(Row, 1) = rs(0)
            flxGrid1.TextMatrix(Row, 2) = rs(1)
            Row = Row + 1
            flxGrid1.Rows = flxGrid1.Rows + 1
            TotalRetur = TotalRetur + rs(1)
            rs.MoveNext
        Wend
        TotalRetur = Format(TotalRetur, "#,##0")
        lbltotalretur = Format(TotalRetur, "#,##0")
        rs.Close
        totalTransfer = 0
        flxGrid3.Rows = 1
        flxGrid3.Rows = 2
        Row = 1
        rs.Open "select d.kode_bank, d.nilai_transfer,m.nama_bank,m.nomer_rekening from t_bayarhutang_transfer d inner join ms_bank m on d.kode_bank = m.kode_bank where d.nomer_bayarhutang='" & lblNoTrans & "'", conn
        While Not rs.EOF
            flxGrid3.TextMatrix(Row, 1) = rs(0)
            flxGrid3.TextMatrix(Row, 2) = rs(2)
            flxGrid3.TextMatrix(Row, 3) = rs(3)
            flxGrid3.TextMatrix(Row, 4) = rs(1)
            Row = Row + 1
            flxGrid3.Rows = flxGrid3.Rows + 1
            totalTransfer = totalTransfer + rs(1)
            rs.MoveNext
        Wend
        rs.Close
        lblTotalTransfer = Format(totalTransfer, "#,##0")
        totalcek = 0
        flxGrid4.Rows = 1
        flxGrid4.Rows = 2
        Row = 1
        rs.Open "select d.no_cek, kode_bank,tanggal_cair,nilai_cek from t_bayarhutang_cek d where d.nomer_bayarhutang='" & lblNoTrans & "'", conn
        While Not rs.EOF
            flxGrid4.TextMatrix(Row, 1) = rs(0)
            flxGrid4.TextMatrix(Row, 2) = rs(1)
            flxGrid4.TextMatrix(Row, 3) = Format(rs(2), "dd/MM/yyyy")
            flxGrid4.TextMatrix(Row, 4) = rs(3)
            Row = Row + 1
            flxGrid4.Rows = flxGrid4.Rows + 1
            totalcek = totalcek + rs(3)
            rs.MoveNext
        Wend
        rs.Close
        lblTotalCek = Format(totalcek, "#,##0")
        

        
        
        hitungulang
        
        If StatusPosting = "1" Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
            cmdHapus.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
            cmdHapus.Enabled = True
        End If
        
    End If
    If rs.State Then rs.Close
    conn.Close
    cek_kodesupplier
End Sub

Private Sub cmdSimpan_Click()

    If Trim(cmbTipe.text) = "" Then
        MsgBox "Pilih Tipe !", vbExclamation
        cmbTipe.SetFocus
        Exit Sub
    End If

    hitungulang
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub


Private Function simpan() As Boolean
Dim i As Byte
Dim counter As Integer
On Error GoTo err
    simpan = False
    If Format(txtSelisih.text + txtDeposit.text, "#,##0") <> Format(total - TotalRetur - totalcek - totalTransfer - CDbl(txtTunai.text), "#,##0") Then
        MsgBox "Jumlah uang harus sama dengan jumlah sisa"
        txtSelisih.text = Format(total - TotalRetur - totalcek - totalTransfer - CDbl(txtTunai.text) - CDbl(txtDeposit.text), "#,##0")
        Exit Function
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        conn.Execute "delete from t_bayarhutangh where nomer_bayarhutang='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarhutang_beli where nomer_bayarhutang='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarhutang_cek where nomer_bayarhutang='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarhutang_transfer where nomer_bayarhutang='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarhutang_retur where nomer_bayarhutang='" & lblNoTrans & "'"
    End If
    add_dataheader
    
    For i = 1 To flxGrid2.Rows - 2
        If flxGrid2.TextMatrix(i, 1) <> "" Then add_datadetail (i)
    Next
    For i = 1 To flxGrid1.Rows - 2
        If flxGrid1.TextMatrix(i, 1) <> "" Then add_datadetail1 (i)
    Next
    For i = 1 To flxGrid3.Rows - 2
        If flxGrid3.TextMatrix(i, 1) <> "" Then add_datadetail2 (i)
    Next
    For i = 1 To flxGrid4.Rows - 2
        If flxGrid4.TextMatrix(i, 1) <> "" Then add_datadetail3 (i)
    Next
    
    conn.CommitTrans
    simpan = True
    conn.Close
    
    Exit Function
err:
    conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Function

Public Sub reset_form()
    lblNoTrans = "-"
    lblNmsUPPLIER = ""
    txtSupplier.text = ""
    txtKeterangan.text = ""
    txtSelisih.text = "0"
    txtDeposit.text = "0"
    DTBayar = Now
    DTransfer = Now
    cmdSimpan.Enabled = True
    cmdPosting.Enabled = False
    TotalRetur = 0
    TotalBayar = 0
    totalcek = 0
    totalTransfer = 0
    flxGrid1.Rows = 1
    flxGrid1.Rows = 2
    flxGrid2.Rows = 1
    flxGrid2.Rows = 2
    flxGrid3.Rows = 1
    flxGrid3.Rows = 2
    flxGrid4.Rows = 1
    flxGrid4.Rows = 2
    total = 0
    lblTotal = 0
    lblTotalBayar = 0
    lbltotalretur = 0
    lblTotalTransfer = 0
    lblTotalCek = 0
    txtTunai.text = "0"
    cmbTipe.ListIndex = 0
    lblSisa = 0
    
End Sub

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(11)
    ReDim nilai(11)
    
    table_name = "T_BAYARhutangH"
    fields(0) = "nomer_bayarhutang"
    fields(1) = "kode_supplier"
    fields(2) = "tanggal_bayarhutang"
    fields(3) = "keterangan"
    fields(4) = "selisih"
    fields(5) = "userid"
    fields(6) = "jumlah_tunai"
    fields(7) = "deposit"
    fields(8) = "tipe"
    fields(9) = "nilai_cek"
    fields(10) = "nilaitransfer"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = txtSupplier.text
    nilai(2) = Format(DTBayar, "yyyy/MM/dd")
    nilai(3) = txtKeterangan.text
    nilai(4) = Replace(Format(txtSelisih.text, "###0"), ",", ".")
    nilai(5) = userid
    nilai(6) = Replace(Format(txtTunai.text, "###0"), ",", ".")
    
    nilai(7) = Replace(Format(txtDeposit.text, "###0"), ",", ".")
    nilai(8) = cmbTipe.text
    nilai(9) = Replace(Format(lblTotalCek, "###0"), ",", ".")
    nilai(10) = Replace(Format(lblTotalTransfer, "###0"), ",", ".")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "t_bayarhutang_beli"
    fields(0) = "nomer_bayarhutang"
    fields(1) = "nomer_beli"
    fields(2) = "tanggal_beli"
    fields(3) = "sisa_hutang"
    fields(4) = "jumlah_bayar"
    fields(5) = "no_urut"
            
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid2.TextMatrix(Row, 1)
'    nilai(2) = flxGrid2.TextMatrix(row, 2)
    nilai(2) = Right(flxGrid2.TextMatrix(Row, 3), 4) & "/" & Mid(flxGrid2.TextMatrix(Row, 3), 4, 2) & "/" & Left(flxGrid2.TextMatrix(Row, 3), 2)
    nilai(3) = Replace(Format(flxGrid2.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(4) = Replace(Format(flxGrid2.TextMatrix(Row, 5), "###0.##"), ",", ".")
    nilai(5) = Row
       
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetail1(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "T_BAYARhutang_RETUR"
    fields(0) = "nomer_bayarhutang"
    fields(1) = "nomer_retur"
    fields(2) = "JUMLAH"
    fields(3) = "no_urut"
            
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid1.TextMatrix(Row, 1)
    nilai(2) = Replace(Format(flxGrid1.TextMatrix(Row, 2), "###0.##"), ",", ".")
    nilai(3) = Row
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetail2(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "T_BAYARhutang_transfer"
    fields(0) = "nomer_bayarhutang"
    fields(1) = "kode_bank"
    fields(2) = "nilai_transfer"
    fields(3) = "no_urut"
            
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid3.TextMatrix(Row, 1)
    nilai(2) = Replace(Format(flxGrid3.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(3) = Row
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetail3(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(7)
    ReDim nilai(7)
    
    table_name = "T_BAYARhutang_cek"
    fields(0) = "nomer_bayarhutang"
    fields(1) = "kode_bank"
    fields(2) = "no_cek"
    fields(3) = "tanggal_cair"
    fields(4) = "nilai_cek"
    fields(5) = "no_urut"
    fields(6) = "jenis"
            
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid4.TextMatrix(Row, 2)
    nilai(2) = flxGrid4.TextMatrix(Row, 1)
    nilai(3) = Right(flxGrid4.TextMatrix(Row, 3), 4) & "/" & Mid(flxGrid4.TextMatrix(Row, 3), 4, 2) & "/" & Left(flxGrid4.TextMatrix(Row, 3), 2)
    nilai(4) = Replace(Format(flxGrid4.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(5) = Row
    nilai(6) = IIf(OptLunas(0).value = True, "Cek", "Giro")
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub


Public Sub cek_kodesupplier()
    conn.ConnectionString = strcon
    conn.Open
    If cmbTipe.text = "Supplier" Then
        rs.Open "select * from ms_supplier where kode_supplier='" & txtSupplier.text & "'", conn
    ElseIf cmbTipe.text = "Karyawan" Then
        rs.Open "select * from ms_karyawan where nik='" & txtSupplier.text & "'", conn
    ElseIf cmbTipe.text = "Ekspedisi" Then
        rs.Open "select * from ms_ekspedisi where kode_ekspedisi='" & txtSupplier.text & "'", conn
    ElseIf cmbTipe.text = "Lain lain" Then
        rs.Open "select * from ms_contact where kode_contact='" & txtSupplier.text & "'", conn
    End If
    
    If Not rs.EOF Then
        lblNmsUPPLIER = rs(1)
        cmbRetur.Clear
        cmbRetur.AddItem ""
        
        
        rs.Close
        Dim where As String
        If lblNoTrans <> "-" Then where = " where id<>'" & lblNoTrans & "'"
        'and left(nomer_transaksi,2)='RB'
        rs.Open "select nomer_transaksi from list_piutang_retur where kode_supplier='" & txtSupplier.text & "' " & _
                " " & _
                "and (piutang-total_bayar)>0", conn
        While Not rs.EOF
            cmbRetur.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close

        
        If txtSupplier.text <> supplier Then
            flxGrid1.Rows = 1
            flxGrid1.Rows = 2
            flxGrid2.Rows = 1
            flxGrid2.Rows = 2
            TotalRetur = 0
            total = 0
            lbltotalretur = "0"
            lblTotal = "0"
            lblSisa = "0"
        End If
        supplier = txtSupplier
    Else
        cmbRetur.Clear
        
        lblNmsUPPLIER = ""
        Text1 = ""
        Text2 = ""
        Text3 = ""
        Text4 = ""
        If txtSupplier.text <> supplier Then
            flxGrid1.Rows = 1
            flxGrid1.Rows = 2
            flxGrid2.Rows = 1
            flxGrid2.Rows = 2
            TotalRetur = 0
            total = 0
            lbltotalretur = "0"
            lblTotal = "0"
            lblSisa = "0"
        End If
        supplier = ""
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Public Sub cek_jual()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim jumlah, sudahbayar As Currency
    conn.ConnectionString = strcon
    conn.Open
    
    If Trim(txtNoFaktur.text) <> "" Then
        rs.Open "select h.nomer_transaksi,h.nomer_referensi,h.tanggal_jatuhtempo,(h.hutang+h.koreksi) as hutang,h.total_bayar from list_hutang h " & _
                "left join sa_hutang a on a.nomer_transaksi=h.nomer_transaksi " & _
                "where (h.kode_supplier='" & txtSupplier.text & "' and h.nomer_referensi like '%" & txtNoFaktur & "%')  order by a.tanggal", conn
    Else
        rs.Open "select h.nomer_transaksi,h.nomer_referensi,h.tanggal_jatuhtempo,(h.hutang+h.koreksi) as hutang,h.total_bayar from list_hutang h " & _
                "left join sa_hutang a on a.nomer_transaksi=h.nomer_transaksi " & _
                "where (h.nomer_transaksi = '" & txtNoInv.text & "')  order by a.tanggal", conn
    End If
    If Not rs.EOF Then
        txtNoFaktur = rs!nomer_referensi
        txtNoInv = rs!nomer_transaksi
        lbltanggal = Format(rs("tanggal_jatuhtempo"), "dd/MM/yyyy")
        jumlah = rs("hutang")
        sudahbayar = rs("total_bayar")
        LblSisaHutang = Format(jumlah - sudahbayar, "#,##0")
        txtBayar = Format(jumlah - sudahbayar, "#,##0")
    Else
        lblInvoice = ""
        lbltanggal = ""
        sudahbayar = 0
        jumlah = 0
    End If
    rs.Close
    conn.Close
    
    Set conn = Nothing
    Set rs = Nothing
End Sub
Public Sub cek_buku()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim Jenis As String
    conn.ConnectionString = strcon
    conn.Open
    If OptLunas(0).value = True Then
        Jenis = "Cek"
    Else
        Jenis = "Giro"
    End If
    rs.Open "select * from vw_bukucek where jenis='" & Jenis & "' and nomer='" & txtNoCek & "'", conn
    If Not rs.EOF Then
        txtKodeBank = rs!Kode_Bank
        lblNamaBank(0) = rs!nama_bank
    Else
        txtKodeBank = ""
    End If
    rs.Close
    conn.Close
    
    Set conn = Nothing
    Set rs = Nothing
End Sub
Private Sub flxGrid1_Click()
    If flxGrid1.TextMatrix(flxGrid1.Row, 1) <> "" Then SetComboText flxGrid1.TextMatrix(flxGrid1.Row, 1), cmbRetur
End Sub

Private Sub flxGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then Delete_Retur
End Sub

Private Sub flxGrid2_Click()
    If flxGrid2.TextMatrix(flxGrid1.Row, 1) <> "" Then
        txtNoInv = flxGrid2.TextMatrix(flxGrid2.Row, 1)
        txtBayar.text = flxGrid2.TextMatrix(flxGrid2.Row, 5)
    End If
End Sub

Private Sub flxGrid2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then Delete_Jual
End Sub

Private Sub flxGrid3_DblClick()
    If flxGrid3.TextMatrix(flxGrid1.Row, 1) <> "" Then
        txtKode = flxGrid3.TextMatrix(flxGrid3.Row, 1)
        txtTransfer.text = flxGrid3.TextMatrix(flxGrid3.Row, 4)
    End If
End Sub

Private Sub flxGrid3_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then Delete_transfer
End Sub

Private Sub flxGrid4_DblClick()
    If flxGrid4.TextMatrix(flxGrid1.Row, 1) <> "" Then
        txtNoCek = flxGrid4.TextMatrix(flxGrid4.Row, 1)
        txtNama2.text = flxGrid4.TextMatrix(flxGrid4.Row, 2)
        DTBayar = flxGrid4.TextMatrix(flxGrid4.Row, 3)
        txtCek.text = flxGrid4.TextMatrix(flxGrid4.Row, 4)
    End If
End Sub

Private Sub flxGrid4_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then Delete_cek
End Sub

Private Sub Form_Activate()
'    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    
    If KeyCode = vbKeyF4 Then cmdSearchBank_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then MySendKeys "{tab}"
End Sub
Private Sub Delete_Jual()
Dim Row, Col As Integer
    If flxGrid2.Rows <= 2 Then Exit Sub
    flxGrid2.TextMatrix(flxGrid2.Row, 0) = ""
    Row = flxGrid2.Row
    total = total - (flxGrid2.TextMatrix(Row, 5))
        
    lblTotal = Format(total, "#,##0")
    hitungulang
    
    
    For Row = Row To flxGrid2.Rows - 1
        If Row = flxGrid2.Rows - 1 Then
            For Col = 1 To flxGrid2.cols - 1
                flxGrid2.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid2.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid2.cols - 1
                flxGrid2.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid2.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid2.cols - 1
            flxGrid2.TextMatrix(Row, Col) = flxGrid2.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If flxGrid2.Row > 1 Then flxGrid2.Row = flxGrid2.Row - 1
        
    flxGrid2.Rows = flxGrid2.Rows - 1
    flxGrid2.Col = 0
    flxGrid2.ColSel = 5
    
    LblSisaHutang = "0"
    lbltanggal = ""
    txtBayar.text = "0"
    lblQty = flxGrid2.Rows - 2
End Sub
Private Sub Delete_cek()
Dim Row, Col As Integer
    With flxGrid4
    If .Rows <= 2 Then Exit Sub
    .TextMatrix(.Row, 0) = ""
    Row = .Row
    totalcek = totalcek - (.TextMatrix(Row, 4))
        
    lblTotalCek = Format(totalcek, "#,##0")
    hitungulang
    
    
    For Row = Row To .Rows - 1
        If Row = .Rows - 1 Then
            For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf .TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = ""
            Next
        ElseIf .TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To .cols - 1
            .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If .Row > 1 Then .Row = .Row - 1
        
    .Rows = .Rows - 1
    .Col = 0
    .ColSel = 2
    End With
    
End Sub
Private Sub Delete_transfer()
Dim Row, Col As Integer
    With flxGrid3
    If .Rows <= 2 Then Exit Sub
    .TextMatrix(.Row, 0) = ""
    Row = .Row
    totalTransfer = totalTransfer - (.TextMatrix(Row, 4))
        
    lblTotalTransfer = Format(totalTransfer, "#,##0")
    hitungulang
    
    
    For Row = Row To .Rows - 1
        If Row = .Rows - 1 Then
            For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf .TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = ""
            Next
        ElseIf .TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To .cols - 1
            .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If .Row > 1 Then .Row = .Row - 1
        
    .Rows = .Rows - 1
    .Col = 0
    .ColSel = 2
    End With
    
End Sub

Private Sub Delete_Retur()
Dim Row, Col As Integer
    If flxGrid1.Rows <= 2 Then Exit Sub
    flxGrid1.TextMatrix(flxGrid1.Row, 0) = ""

    Row = flxGrid1.Row
    
    TotalRetur = TotalRetur - (flxGrid1.TextMatrix(Row, 2))
        
    lbltotalretur = Format(TotalRetur, "#,##0")
    hitungulang
    

    For Row = Row To flxGrid1.Rows - 1
        If Row = flxGrid1.Rows - 1 Then
            For Col = 1 To flxGrid1.cols - 1
                flxGrid1.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid1.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid1.cols - 1
                flxGrid1.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid1.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid1.cols - 1
            flxGrid1.TextMatrix(Row, Col) = flxGrid1.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If flxGrid1.Row > 1 Then flxGrid1.Row = flxGrid1.Row - 1
        
    flxGrid1.Rows = flxGrid1.Rows - 1
    flxGrid1.Col = 0
    flxGrid1.ColSel = 2
    cmbRetur.ListIndex = 0
    lblRetur = "0"
        
End Sub
Private Sub Form_Load()
    reset_form
    seltab = 0
    frTab(0).Visible = True
    flxGrid1.ColWidth(0) = 300
    flxGrid1.ColWidth(1) = 1300
    flxGrid1.ColWidth(2) = 1200
    flxGrid1.TextMatrix(0, 1) = "No. Retur"
    flxGrid1.TextMatrix(0, 2) = "Jumlah"
    flxGrid2.ColWidth(0) = 300
    flxGrid2.ColWidth(1) = 1400
    flxGrid2.ColWidth(2) = 1500
    flxGrid2.ColWidth(3) = 1200
    flxGrid2.ColWidth(4) = 1300
    flxGrid2.ColWidth(5) = 1300
    flxGrid2.TextMatrix(0, 1) = "No. Jual"
    flxGrid2.TextMatrix(0, 2) = "No Invoice"
    flxGrid2.TextMatrix(0, 3) = "Tanggal"
    flxGrid2.TextMatrix(0, 4) = "Sisa hutang"
    flxGrid2.TextMatrix(0, 5) = "jumlah_bayar"
With flxGrid3
    .ColWidth(0) = 300
    .ColWidth(1) = 1200
    .ColWidth(2) = 1800
    .ColWidth(3) = 1300
    .ColWidth(4) = 1800
    
    .TextMatrix(0, 1) = "Kode Bank"
    .TextMatrix(0, 2) = "Nama"
    .TextMatrix(0, 3) = "No. Rekening"
    .TextMatrix(0, 4) = "Nominal"
End With
With flxGrid4
    .ColWidth(0) = 300
    .ColWidth(1) = 1300
    .ColWidth(2) = 1800
    .ColWidth(3) = 1200
    .ColWidth(4) = 1800
    
    .TextMatrix(0, 1) = "No Cek/BG"
    .TextMatrix(0, 2) = "Nama Bank"
    .TextMatrix(0, 3) = "Tgl Cair"
    .TextMatrix(0, 4) = "Jumlah"
End With
DCair = Now
End Sub


Private Sub mnuKeluar_Click()
    cmdKeluar_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub OptLunas_Click(Index As Integer)
    txtNoCek = ""
    txtNama2 = ""
End Sub

Private Sub TabStrip1_Click()
    frTab(seltab).Visible = False
    frTab(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub txtBayar_Change()
    If Len(txtBayar.text) = 0 Then
        txtBayar.text = 0
        txtBayar.SelStart = 0
        txtBayar.SelLength = Len(txtBayar.text)
    End If
End Sub

Private Sub txtBayar_GotFocus()
    txtBayar.SelStart = 0
    txtBayar.SelLength = Len(txtBayar.text)
End Sub

Private Sub txtBayar_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBayar_LostFocus()
     
'    If CDbl(txtBayar.text) > CDbl(lblSisaHutang) Then
'        MsgBox "jumlah_bayar tidak boleh lebih dari sisa hutang"
'        txtBayar.SetFocus
'    End If
    If CDbl(txtBayar.text) < 0 Then
        MsgBox "jumlah_bayar tidak boleh kurang dari nol"
        txtBayar.SetFocus
    End If
    If Not IsNumeric(txtBayar.text) Then
        MsgBox "jumlah_bayar harus berupa angka"
        txtBayar.SetFocus
    End If
End Sub

Private Sub txtsupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearch_Click
End Sub

Private Sub txtselisih_GotFocus()
    txtSelisih.SelStart = 0
    txtSelisih.SelLength = Len(txtSelisih.text)
End Sub

Private Sub txtselisih_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtselisih_LostFocus()
    If Not IsNumeric(txtSelisih.text) Then txtSelisih.text = "0"
    
End Sub
Private Sub txtdeposit_GotFocus()
    txtDeposit.SelStart = 0
    txtDeposit.SelLength = Len(txtDeposit.text)
End Sub

Private Sub txtdeposit_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtdeposit_LostFocus()
    If Not IsNumeric(txtDeposit.text) Then txtDeposit.text = "0"
    
End Sub

Private Sub txtsupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cek_kodesupplier
    End If
End Sub

Private Sub txtsupplier_LostFocus()
    cek_kodesupplier
End Sub


Private Sub txtNoFaktur_LostFocus()
    If txtNoFaktur <> "" Then
        txtNoInv = ""
        cek_jual
    End If
End Sub

Private Sub txtNoInv_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchInv_Click
End Sub

Private Sub txtNoInv_LostFocus()
    If txtNoInv <> "" Then
        txtNoFaktur = ""
        cek_jual
    End If
End Sub

Private Sub txtTransfer_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub hitungulang()
    If txtTunai.text = "" Then txtTunai.text = "0"
    
    lblSisa = Format(total - TotalRetur - totalcek - totalTransfer - CDbl(txtTunai.text) - CDbl(txtDeposit.text), "#,##0")
    lblTotalBayar = Format(TotalRetur + totalcek + totalTransfer + txtTunai, "#,##0")
    txtSelisih.text = lblSisa
End Sub
Private Sub txtTunai_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
'        lblTotalBayar = Format(totalbayar, "#,##0")
        
        hitungulang
        
    End If
End Sub

Private Sub txtTunai_LostFocus()
    hitungulang
End Sub

