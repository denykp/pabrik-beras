VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmDetilJual 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1710
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   2100
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1710
   ScaleWidth      =   2100
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   600
      Left            =   45
      TabIndex        =   8
      Top             =   855
      Width           =   645
      Begin VB.OptionButton Opt1 
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   2
         Top             =   45
         Width           =   600
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   0
         TabIndex        =   3
         Top             =   270
         Value           =   -1  'True
         Width           =   510
      End
   End
   Begin VB.TextBox txtDisc 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   855
      TabIndex        =   4
      Top             =   810
      Width           =   1005
   End
   Begin VB.TextBox txtQty 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   855
      TabIndex        =   0
      Top             =   0
      Width           =   870
   End
   Begin VB.ComboBox cmbHarga 
      Height          =   315
      ItemData        =   "frmDetilJual.frx":0000
      Left            =   2250
      List            =   "frmDetilJual.frx":0013
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   405
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.TextBox txtHarga 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   330
      Left            =   855
      TabIndex        =   1
      Top             =   405
      Width           =   1005
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   5175
      Top             =   90
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   45
      Width           =   600
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   450
      Width           =   600
   End
End
Attribute VB_Name = "frmDetilJual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public disc, disc1, disc2, disc3, disc4, disc5 As Double
Public disc_tipe, disc1_tipe, disc2_tipe, disc3_tipe, disc4_tipe, disc5_tipe As Byte


Private Sub cmbHarga_Click()
    Select Case cmbHarga.text
    Case 1: disc = disc1
            disc_tipe = disc1_tipe
    Case 2: disc = disc2
            disc_tipe = disc2_tipe
    Case 3: disc = disc3
            disc_tipe = disc3_tipe
    Case 4: disc = disc4
            disc_tipe = disc4_tipe
    Case 5: disc = disc5
            disc_tipe = disc5_tipe
    End Select
    If disc_tipe = "1" Then txtHarga.text = harga_jual - disc Else txtHarga.text = harga_jual - (harga_jual * disc / 100)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call MySendKeys("{tab}")
        KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    tutupform
End Sub

Private Sub txtDisc_GotFocus()
    txtDisc.SelStart = 0
    txtDisc.SelLength = Len(txtDisc.text)
End Sub

Private Sub txtDisc_Keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        Unload Me
    End If
End Sub
Private Sub tutupform()
    With frmAddPenjualan
        .Enabled = True
        If .flxGrid.TextMatrix(.flxGrid.row, 1) <> "" Then
        If Not IsNumeric(txtDisc.text) Then txtDisc.text = "0"
        .qty = .qty - (.flxGrid.TextMatrix(.flxGrid.row, 3))
        .total = .total - (.flxGrid.TextMatrix(.flxGrid.row, 3) * .flxGrid.TextMatrix(.flxGrid.row, 5))
        .flxGrid.TextMatrix(.flxGrid.row, 3) = txtQty.text
        If txtDisc.text >= 0 Then
        .flxGrid.TextMatrix(.flxGrid.row, 5) = Format(.flxGrid.TextMatrix(.flxGrid.row, 8) - (IIf(Opt1(0).Value, txtDisc.text, (.flxGrid.TextMatrix(.flxGrid.row, 8) * txtDisc.text / 100))), "#,##0")
        Else
        .flxGrid.TextMatrix(.flxGrid.row, 5) = Format(txtHarga.text, "#,##0")
        End If
        .flxGrid.TextMatrix(.flxGrid.row, 6) = Format(.flxGrid.TextMatrix(.flxGrid.row, 3) * .flxGrid.TextMatrix(.flxGrid.row, 5), "#,##0")
        .flxGrid.TextMatrix(.flxGrid.row, 12) = txtDisc.text
        .flxGrid.TextMatrix(.flxGrid.row, 13) = IIf(Opt1(0).Value = True, 1, 2)
        .qty = .qty + (.flxGrid.TextMatrix(.flxGrid.row, 3))
        .total = .total + (.flxGrid.TextMatrix(.flxGrid.row, 3) * .flxGrid.TextMatrix(.flxGrid.row, 5))
        .lblQty = Format(.qty, "#,##0")
        .lblTotal = Format(.total, "#,##0")
        .lblSisa = Format(.total - .lblDisc, "#,##0")
        .lblSubTotal = .lblSisa
        If txtHarga.Enabled = True Then .flxGrid.TextMatrix(.flxGrid.row, 8) = txtHarga
        .LblNamaBarang = .flxGrid.TextMatrix(.flxGrid.row, 2)
        .lblQuantity = .flxGrid.TextMatrix(.flxGrid.row, 3) & " " & .flxGrid.TextMatrix(.flxGrid.row, 4) & " x " & Format(.flxGrid.TextMatrix(.flxGrid.row, 5), "#,##0") & "  = Rp. " & Format(.flxGrid.TextMatrix(.flxGrid.row, 6), "#,##0")
        .lblTotal = Format(.total, "#,##0")
        .lblSisa = Format(.total - .lblDisc, "#,##0")
        .lblSubTotal = .lblSisa
        End If
        DoEvents
        
    End With
        
End Sub

Private Sub txtDisc_KeyPress(KeyAscii As Integer)
NumberOnly KeyAscii
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
NumberOnly KeyAscii
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty)

End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
NumberOnly KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Or txtQty.text = "0" Then
        txtQty.text = 1
        txtQty.SetFocus
    End If
End Sub
