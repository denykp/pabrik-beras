VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTransBeliCekGiro 
   Caption         =   "Pembelian Buku Cek/Giro"
   ClientHeight    =   6525
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8190
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6525
   ScaleWidth      =   8190
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1935
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   5940
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4695
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   5940
      Width           =   1230
   End
   Begin VB.TextBox txtJumlah 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1845
      TabIndex        =   6
      Top             =   2745
      Width           =   1410
   End
   Begin VB.TextBox txtBiaya 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1845
      TabIndex        =   5
      Top             =   2340
      Width           =   1410
   End
   Begin VB.TextBox txtNomer 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1845
      TabIndex        =   7
      Top             =   3150
      Width           =   1410
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   270
      ScaleHeight     =   345
      ScaleWidth      =   3090
      TabIndex        =   22
      Top             =   1935
      Width           =   3120
      Begin VB.OptionButton Optcek 
         Caption         =   "Giro"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1575
         TabIndex        =   4
         Top             =   45
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.OptionButton Optcek 
         Caption         =   "Cek"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   3
         Top             =   45
         Width           =   915
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah bayar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   23
         Top             =   1035
         Visible         =   0   'False
         Width           =   1725
      End
   End
   Begin VB.TextBox txtKodeBank 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1845
      TabIndex        =   2
      Top             =   1530
      Width           =   1410
   End
   Begin VB.CommandButton cmdSearchBank 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3330
      Picture         =   "frmTransBeliCekGiro.frx":0000
      TabIndex        =   18
      Top             =   1530
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1845
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   1125
      Width           =   3570
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4020
      Picture         =   "frmTransBeliCekGiro.frx":0102
      TabIndex        =   11
      Top             =   180
      Width           =   420
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2220
      Left            =   1845
      TabIndex        =   8
      Top             =   3555
      Width           =   3300
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1845
      TabIndex        =   0
      Top             =   720
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   109576195
      CurrentDate     =   38927
   End
   Begin VB.Label Label13 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   29
      Top             =   2790
      Width           =   105
   End
   Begin VB.Label Label11 
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   28
      Top             =   2790
      Width           =   1320
   End
   Begin VB.Label Label10 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   27
      Top             =   2385
      Width           =   105
   End
   Begin VB.Label Label3 
      Caption         =   "Biaya"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   26
      Top             =   2385
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   25
      Top             =   3195
      Width           =   105
   End
   Begin VB.Label Label1 
      Caption         =   "Nomor"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   24
      Top             =   3195
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   21
      Top             =   1575
      Width           =   105
   End
   Begin VB.Label Label7 
      Caption         =   "Kode Bank"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   20
      Top             =   1575
      Width           =   1320
   End
   Begin VB.Label lblNamaBank 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3960
      TabIndex        =   19
      Top             =   1575
      Width           =   2940
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   17
      Top             =   1170
      Width           =   1320
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   16
      Top             =   1170
      Width           =   105
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   15
      Top             =   765
      Width           =   1320
   End
   Begin VB.Label Label9 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   14
      Top             =   765
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1890
      TabIndex        =   13
      Top             =   180
      Width           =   2085
   End
   Begin VB.Label Label22 
      Caption         =   "No.Urut"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   12
      Top             =   270
      Width           =   1410
   End
End
Attribute VB_Name = "frmTransBeliCekGiro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearchBank_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_bank"
    frmSearch.nmform = "frmTransBeliCekGiro"
    frmSearch.nmctrl = "txtkodeBank"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bank"
    frmSearch.proc = "cari_bank"
    frmSearch.col = 0
    
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub
Public Function cari_bank() As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    cari_bank = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bank where kode_bank='" & txtKodeBank.text & "'", conn
    If Not rs.EOF Then
        lblNamaBank = rs(1)
        cari_bank = True
    Else
        lblNamaBank = ""
    End If
    rs.Close
    conn.Close
End Function

Private Sub cmdSearchID_Click()
    frmSearch.query = "select * from t_belibukuh " ' order by kode_Member"
    frmSearch.nmform = "frmtransBeliCekGiro"
    frmSearch.nmctrl = "lblnotrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "belibuku"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = -1
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub
Public Sub cari_data()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_belibukuh where nomer_belibuku='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs!tanggal
        txtKeterangan = rs!keterangan
        If rs!Jenis = "Cek" Then Optcek(0).value = True Else Optcek(1).value = True
        txtKodeBank = rs!Kode_Bank
        cari_bank
        txtBiaya = rs!biaya
        If rs.State Then rs.Close
        List1.Clear
        rs.Open "select * from t_belibukud  where nomer_belibuku='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
            List1.AddItem rs(1)
            rs.MoveNext
        Wend
        
        txtJumlah = List1.ListCount
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then MySendKeys "{tab}"
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub List1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And List1.ListIndex >= 0 Then
        List1.RemoveItem (List1.ListIndex)
    End If
End Sub

Private Sub txtBiaya_GotFocus()
    txtBiaya.SelStart = 0
    txtBiaya.SelLength = Len(txtBiaya)
End Sub

Private Sub txtJumlah_GotFocus()
    txtJumlah.SelStart = 0
    txtJumlah.SelLength = Len(txtJumlah)
End Sub

Private Sub txtKodeBank_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBank_Click
End Sub

Private Sub txtNomer_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And txtNomer <> "" Then
        List1.AddItem txtNomer
        txtNomer = ""
        MySendKeys "+{tab}"
    End If
End Sub
Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer

On Error GoTo err
    If List1.ListCount <> txtJumlah Then
        MsgBox "Jumlah halaman tidak sama dengan nomor yang telah dimasukkan"
        txtNomer.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_belibukuh", "nomer_belibuku", DTPicker1, "CG")
        
    Else
            conn.Execute "delete from t_belibukuh where nomer_belibuku='" & lblNoTrans & "'"
            conn.Execute "delete from t_belibukud where nomer_belibuku='" & lblNoTrans & "'"
        
    End If
    add_dataheader
    
    For row = 0 To List1.ListCount - 1
            add_datadetail (row)
    Next
    
'    add_jurnal
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    txtKodeBank = ""
    txtJumlah = 0
    txtBiaya = 0
    txtKeterangan = ""
    txtNomer = ""
    
    DTPicker1 = Now
    List1.Clear
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_belibukuh"
    fields(0) = "nomer_belibuku"
    fields(1) = "tanggal"
    fields(2) = "jenis"
    fields(3) = "kode_bank"
    fields(4) = "biaya"
    fields(5) = "userid"
    fields(6) = "keterangan"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = IIf(Optcek(0).value = True, Optcek(0).Caption, Optcek(1).Caption)
    nilai(3) = txtKodeBank.text
    nilai(4) = txtBiaya
    nilai(5) = User
    nilai(6) = txtKeterangan
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_belibukud"
    fields(0) = "nomer_belibuku"
    fields(1) = "nomer"
    fields(2) = "status"
    
    fields(3) = "no_urut"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = List1.List(row)
    nilai(2) = "1"
    
    nilai(3) = row
    
    tambah_data table_name, fields, nilai
    
End Sub
