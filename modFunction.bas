Attribute VB_Name = "modFunction"
Option Explicit
Public passcode_result As Boolean
Public BG, BGColor1 As String
Public btncolor As String
Public Function calc(text As String) As Double
Dim A, B, C As Double

        If InStr(1, text, "*") > 0 And InStr(1, text, "+") > 0 Then
            If InStr(1, text, "*") > InStr(1, text, "+") Then
                A = CDbl(Left(text, InStr(1, text, "+") - 1))
                B = CDbl(Mid(text, InStr(1, text, "+") + 1, (InStr(1, text, "*")) - (InStr(1, text, "+") + 1)))
                C = CDbl(Mid(text, InStr(1, text, "*") + 1, Len(text) - InStr(1, text, "*")))
                calc = (A + B) * C
            Else
                A = CDbl(Left(text, InStr(1, text, "*") - 1))
                B = CDbl(Mid(text, InStr(1, text, "*") + 1, (InStr(1, text, "+")) - (InStr(1, text, "*") + 1)))
                C = CDbl(Mid(text, InStr(1, text, "+") + 1, Len(text) - InStr(1, text, "+")))
                calc = (A * B) + C
            End If
            Exit Function
        ElseIf InStr(1, text, "+") Then
            calc = CDbl(Left(text, InStr(1, text, "+") - 1)) + CDbl(Mid(text, InStr(1, text, "+") + 1, Len(text) - InStr(1, text, "+")))
            Exit Function
        ElseIf InStr(1, text, "/") Then
            calc = CDbl(Left(text, InStr(1, text, "/") - 1)) / CDbl(Mid(text, InStr(1, text, "/") + 1, Len(text) - InStr(1, text, "/")))
            Exit Function
        ElseIf InStr(1, text, "*") Then
            calc = CDbl(Left(text, InStr(1, text, "*") - 1)) * CDbl(Mid(text, InStr(1, text, "*") + 1, Len(text) - InStr(1, text, "*")))
            Exit Function
        Else
            calc = text
        End If


End Function
Public Function getDiscArray(text As String) As Double
    Dim A() As Double
    Dim pointer As Integer
    Dim i As Integer
    pointer = 1
    If text = "" Then text = "0"
    ReDim A(0)
    While InStr(pointer + 1, text, "+") > 0
        If UBound(A) >= 1 Then
            ReDim Preserve A(UBound(A) + 1) As Double
        Else
            ReDim A(1)
        End If
        
        A(UBound(A) - 1) = Mid(text, pointer, InStr(pointer + 1, text, "+") - pointer)
        pointer = InStr(pointer + 1, text, "+")
    Wend
    ReDim Preserve A(UBound(A) + 1) As Double
    A(UBound(A) - 1) = Mid(text, pointer, Len(text) - (pointer - 1))
    Dim disc As Double
    For i = 0 To UBound(A) - 1
        If disc > 0 Then
            disc = disc * ((100 - A(i)) / 100)
        Else
            disc = ((100 - A(i)) / 100)
        End If
    Next
    getDiscArray = disc * 100
End Function
Public Function CountRow(strFrom As String) As Integer
    Dim strSQL As String
    
    CountRow = 0
    
    strSQL = "SELECT COUNT(*) " & strFrom
    rs.Open strSQL, conn
    If Not rs.EOF Then CountRow = rs(0)
    
    rs.Close
End Function

'Generate No PI atau No PO
Public Function GenNumber(No_For As String) As String   'No_For (PI atau PO)
    Dim strSQL As String
    Dim MaxNo As String
    Dim No As Integer
    Dim CurNo As String         ' Current No
    Dim fieldnm As String
    
    fieldnm = "NO_" & No_For
    
    strSQL = "SELECT MAX(" & fieldnm & ") FROM TR_" & No_For & "_H where left(" & fieldnm & ",2)='" & Right(Year(Date), 2) & "'"
    rs.Open strSQL, conn, adOpenForwardOnly, adLockOptimistic
    If Not IsNull(rs(0)) Then
        MaxNo = rs(0)
    Else
        MaxNo = "00000000"
    End If
    
    No = CInt(Mid(MaxNo, 3, Len(MaxNo)))
    No = No + 1
    CurNo = Right("00" + CStr(Year(Date)), 2) & Right("000000" + CStr(No), 6)
    
    GenNumber = CurNo

    rs.Close
End Function


Public Function Confirm_ReInsert(Action_Fg As String) As Boolean
    Dim Response As String
    
    If Action_Fg = "Insert" Then
        Confirm_ReInsert = False
        Response = MsgBox("Data telah berhasil disimpan." & vbCrLf & _
                           "Apakah anda ingin memasukkan data lain?", _
                           vbQuestion + vbYesNo, "Data")
        If Response = vbYes Then Confirm_ReInsert = True
    Else
        MsgBox "Data telah berhasil disimpan."
    End If
End Function

Public Function getConfig() As String
On Error Resume Next
    Dim filename As String
    filename = App.Path & "\Config.txt"
    Open filename For Input As #1
    Line Input #1, BG
    Line Input #1, btncolor
    Line Input #1, BGColor1
    Line Input #1, printername
    Close #1
    
End Function
Public Function setConfig(BG As String, BGColor1 As String, btncolor As String) As String
On Error Resume Next
    Dim filename As String
    
    
    filename = App.Path & "\Config.txt"
    Open filename For Output As #1
    Print #1, BG
    Print #1, BGColor1
    Print #1, btncolor
    Print #1, printername
    Close #1
    
End Function
'Public Function GetHPP(ByVal kdbrg As String, ByVal kdgdg As String, cnstr As String) As Currency
'Dim rs As New ADODB.Recordset
'Dim conn As New ADODB.Connection
'conn.Open cnstr
'rs.Open "select hpp from hpp where [kode_bahan]='" & kdbrg & "' and gudang='" & kdgdg & "'", conn
'If Not rs.EOF Then
'    GetHPP = rs(0)
'Else
''    rs.Close
''    rs.Open "select hppawal from ms_bahan where [kode]='" & kdbrg & "'", conn
''    If Not rs.EOF Then
''        getHpp = rs(0)
''    Else
'        GetHPP = 0
''    End If
'End If
'rs.Close
'conn.Close
'End Function

Public Function GetHPP(ByVal kdBrg As String, gudang As String, cnstr As String) As Currency
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select hpp from stock where [kode_bahan]='" & kdBrg & "' and gudang='" & gudang & "' ", conn
If Not rs.EOF And IsNull(rs!hpp) = False Then
    GetHPP = rs(0)
Else
'    rs.Close
'    rs.Open "select hppawal from ms_bahan where [kode]='" & kdbrg & "'", conn
'    If Not rs.EOF Then
'        getHpp = rs(0)
'    Else
        GetHPP = 0
'    End If
End If
rs.Close
conn.Close
End Function

Public Function GetStock(ByVal kdBrg As String, ByVal kdgdg As String, cnstr As String) As Long
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select stock from q_stock1 where [kode_bahan]='" & kdBrg & "' and gudang='" & kdgdg & "'", conn
If Not rs.EOF Then
    GetStock = rs(0)
Else
    GetStock = 0
End If
rs.Close
conn.Close

End Function
Public Function getStock2(ByVal kdBrg As String, ByVal kdgdg As String, cnstr As String) As Long
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select stock from stock s  where [kode_bahan]='" & kdBrg & "' and gudang='" & kdgdg & "'", conn
If Not rs.EOF Then
    getStock2 = rs(0)
Else
    getStock2 = 0
End If
rs.Close
conn.Close

End Function
Public Function RC4(ByVal Expression As String, ByVal password As String) As String
On Error Resume Next
Dim RB(0 To 255) As Integer, X As Long, y As Long, Z As Long, key() As Byte, ByteArray() As Byte, Temp As Byte
If Len(password) = 0 Then
    Exit Function
End If
If Len(Expression) = 0 Then
    Exit Function
End If

If Len(password) > 256 Then
    key() = StrConv(Left$(password, 256), vbFromUnicode)
Else
    key() = StrConv(password, vbFromUnicode)
End If
For X = 0 To 255
    RB(X) = X
Next X
X = 0
y = 0
Z = 0
For X = 0 To 255
    y = (y + RB(X) + key(X Mod Len(password))) Mod 256
    Temp = RB(X)
    RB(X) = RB(y)
    RB(y) = Temp
Next X
X = 0
y = 0
Z = 0
ByteArray() = StrConv(Expression, vbFromUnicode)
For X = 0 To Len(Expression)
    y = (y + 1) Mod 256
    Z = (Z + RB(y)) Mod 256
    Temp = RB(y)
    RB(y) = RB(Z)
    RB(Z) = Temp
    ByteArray(X) = ByteArray(X) Xor (RB((RB(y) + RB(Z)) Mod 256))
Next X
RC4 = StrConv(ByteArray, vbUnicode)
End Function
Public Function getcolindex(Col As String, colname() As String) As Byte
Dim i As Integer

For i = 0 To UBound(colname)
If LCase(colname(i)) = LCase(Col) Then
    getcolindex = i
    Exit For
End If
Next i
     
End Function

Public Function Nomor_Jurnal(tanggal As Date) As String
Dim rs As New ADODB.Recordset
Dim No As String
    rs.Open "select top 1 no_jurnal from t_jurnalh where substring(no_jurnal,3,2)='" & Format(tanggal, "yy") & "' and substring(no_jurnal,5,2)='" & Format(tanggal, "MM") & "' order by no_jurnal desc", conn
    If Not rs.EOF Then
        No = "JU" & Format(tanggal, "yy") & Format(tanggal, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "JU" & Format(tanggal, "yy") & Format(tanggal, "MM") & Format("1", "00000")
    End If
    rs.Close
    Nomor_Jurnal = No
End Function
Public Function Nomor_Jurnal2(tanggal As Date) As String
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim No As String
    con.Open strcon
    rs.Open "select top 1 no_jurnal from t_jurnalh where substring(no_jurnal,3,2)='" & Format(tanggal, "yy") & "' and substring(no_jurnal,5,2)='" & Format(tanggal, "MM") & "' order by no_jurnal desc", con
    If Not rs.EOF Then
        No = "JU" & Format(tanggal, "yy") & Format(tanggal, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "JU" & Format(tanggal, "yy") & Format(tanggal, "MM") & Format("1", "00000")
    End If
    rs.Close
    con.Close
    Nomor_Jurnal2 = No
End Function


Public Function getAcc(ByVal kodeAcc As String) As String
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open strcon

rs.Open "Select kode_acc from setting_coa where kode='" & LCase(kodeAcc) & "'", conn
If Not rs.EOF Then
    getAcc = rs(0)
End If
rs.Close
conn.Close
End Function


Public Function BulanPrev(bulan As String, tahun As String) As String
    If Val(bulan) = 1 Then
        BulanPrev = "12"
    Else
        BulanPrev = Val(bulan) - 1
    End If
End Function

Public Function TahunPrev(bulan As String, tahun As String) As String
    If Val(bulan) = 1 Then
        TahunPrev = Val(tahun) - 1
    Else
        TahunPrev = tahun
    End If
End Function

Public Function BulanNext(bulan As String, tahun As String) As String
    If Val(bulan) = 12 Then
        BulanNext = "1"
    Else
        BulanNext = Val(bulan) + 1
    End If
End Function

Public Function TahunNext(bulan As String, tahun As String) As String
    If Val(bulan) = 12 Then
        TahunNext = Val(tahun) + 1
    Else
        TahunNext = tahun
    End If
End Function

Public Function CekSettingCOA(conStr As String) As Boolean
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
    CekSettingCOA = True
    conn.ConnectionString = conStr
    conn.Open
    rs.Open "select * from setting_coa where kode_acc is null or kode_acc=''", conn
    If Not rs.EOF Then
        MsgBox "Setting Perkiraan harus dilengkapi dulu sebelum melakukan transaksi !", vbExclamation
        CekSettingCOA = False
        rs.Close
        conn.Close
        Exit Function
    End If
    rs.Close
    conn.Close
End Function
                

