USE [pabrik_kertas]
GO
/****** Object:  Table [dbo].[t_TransferBahan]    Script Date: 03/21/2011 11:31:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_TransferBahan](
	[nomer_TransferBahan] [varchar](15) NOT NULL,
	[tanggal] [datetime] NULL,
	[keterangan] [varchar](50) NULL,
	[kode_gudang] [varchar](10) NULL,
	[kode_bahan] [varchar](35) NULL,
	[nomer_serial] [varchar](35) NULL,
	[kode_hasil] [varchar](35) NULL,
	[nomer_serial_hasil] [varchar](35) NULL,
	[qty] [numeric](18, 3) NULL,
	[hpp] [numeric](18, 3) NULL,
	[userid] [varchar](10) NULL,
	[status_posting] [char](1) NULL CONSTRAINT [DF_t_TransferBahanH_status]  DEFAULT ('0'),
	[no_history] [varchar](15) NULL,
 CONSTRAINT [PK_t_TransferBahanH] PRIMARY KEY CLUSTERED 
(
	[nomer_TransferBahan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
USE [pabrik_kertas]
GO
/****** Object:  Trigger [dbo].[TRG_postingtransferbahan]    Script Date: 03/21/2011 11:31:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRG_postingtransferbahan]
   ON  [dbo].[t_TransferBahan]
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT COUNT(*) FROM inserted)=1
	begin
	declare 
		@sebelum char(1),
		@sesudah char(1),
		@notrans varchar(12)
    -- Insert statements for trigger here
	set @sebelum=(select status_posting from deleted)
	set @sesudah=(select status_posting from inserted)
	set @notrans=(select nomer_transferbahan from inserted)
	if @sebelum=0 and @sesudah =1 
		if not exists (select * from history_posting where nomer_transaksi=@notrans)
			insert into history_posting select nomer_transferbahan,getDate(),userid,'1' from inserted
	END
END

