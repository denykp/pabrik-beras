Attribute VB_Name = "ModPostingJurnal"
Public Function PostingJurnal_Penjualan(NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte, hpp As Double
Dim kode_customer As String
Dim Nama_Customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim noFaktur As String
Dim pajak As Double

Dim Acc_Penjualan_Lain As String
Dim Acc_Pendapatan As String

i = 0
PostingJurnal_Penjualan = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1

rs.Open "select kode_acc from setting_coa where kode='jual lain'", conn
If Not rs.EOF Then Acc_Penjualan_Lain = rs(0)
rs.Close

If Left(NoTransaksi, 2) = "PJ" Then
    Acc_Pendapatan = var_accpendapatan
Else
    Acc_Pendapatan = Acc_Penjualan_Lain
End If

rs.Open "select t.*,c.nama_customer from t_jualh t " & _
        "left join ms_customer c on c.kode_customer=t.kode_customer " & _
        "where t.nomer_jual='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    
    tanggal = rs!tanggal_jual
    kode_customer = rs!kode_customer
    Nama_Customer = rs!Nama_Customer
    noFaktur = rs!nomer_invoice
    pajak = rs!tax
    total = rs!total
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
'    insertpiutang rs!kode_customer, NoTransaksi, noFaktur, (total * (100 + pajak) / 100), rs!tanggal_jatuhtempo, conntrans
    
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Penjualan", (total * (100 + pajak) / 100), "PJ", conntrans
    If rs!cara_bayar = "T" Then
'        insertbayarpiutangtunai NoTransaksi, rs!tanggal_jual, rs!kode_customer, (total * (100 + pajak) / 100), conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckas), CCur((total * (100 + pajak) / 100)), 0, Row, conntrans, "Penjualan", Left(Nama_Customer, 35)
    Else
        rs.Close
        rs.Open "select acc_piutang from setting_acccustomer where kode_customer='" & kode_customer & "'"
        add_jurnaldetail NoTransaksi, NoJurnal, rs(0), (total * (100 + pajak) / 100), 0, Row, conntrans, "Penjualan", Left(Nama_Customer, 35)
    End If
    Row = Row + 1
    add_jurnaldetail NoTransaksi, NoJurnal, CStr(Acc_Pendapatan), 0, total, Row, conntrans, "Penjualan", Left(Nama_Customer, 35)
    
    Row = Row + 1
    add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accpajakjual), 0, (total * pajak / 100), Row, conntrans, "Penjualan", noFaktur
    rs.Close
    
    
    
    rs.Open "select d.kode_bahan,b.nama_bahan,s.total,m.acc_hppjual,m.acc_persediaan,s.qty from t_juald d " & _
            "left join t_jualh t on t.nomer_jual=d.nomer_jual " & _
            "left join (select nomer_order,kode_bahan,sum(berat) as qty,sum(berat*hpp) as Total from t_suratjalan_timbang " & _
            "group by nomer_order,kode_bahan) s on s.nomer_order=t.nomer_order and s.kode_bahan=d.kode_bahan " & _
            "left join setting_accbahan m on d.kode_bahan=m.kode_bahan " & _
            "left join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
            "where d.nomer_jual='" & NoTransaksi & "'"
    While Not rs.EOF
        Row = Row + 1
        
        If rs!Acc_Persediaan <> "" Then
            add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs(1), Row, conntrans, "Surat Jalan", Left(rs!nama_bahan, 35)
    '        JurnalD NoTransaksi, NoJurnal, rs!Acc_Persediaan, "k", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
            Row = Row + 1
            add_jurnaldetail NoTransaksi, NoJurnal, rs!acc_hppjual, rs(1), 0, Row, conntrans, "Surat Jalan", Left(rs!nama_bahan, 35)
    '        JurnalD NoTransaksi, NoJurnal, rs!acc_hppjual, "d", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
            conntrans.Execute "update t_jurnald set nilai='" & Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##") & "' where no_transaksi='" & NoTransaksi & "' and kode1='" & rs!kode_bahan & "'"
        End If
        rs.MoveNext
    Wend
    
'    rs.Open "select s.kode_bahan,sum(s.berat*s.hpp) as total,acc_hppjual,acc_persediaan,sum(s.berat) as qty from t_suratjalan_timbang s " & _
'        " inner join setting_accbahan m on s.kode_bahan=m.kode_bahan where s.nomer_suratjalan='" & NoTransaksi & "' group by s.kode_bahan,acc_hppjual,acc_persediaan", conn, adOpenForwardOnly
'    While Not rs.EOF
'        row = row + 1
'
'        If rs!Acc_Persediaan <> "" Then
'            add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs(1), row, conntrans, "Surat Jalan", rs!kode_bahan
'    '        JurnalD NoTransaksi, NoJurnal, rs!Acc_Persediaan, "k", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
'            row = row + 1
'            add_jurnaldetail NoTransaksi, NoJurnal, rs!acc_hppjual, rs(1), 0, row, conntrans, "Surat Jalan", rs!kode_bahan
'    '        JurnalD NoTransaksi, NoJurnal, rs!acc_hppjual, "d", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
'            conntrans.Execute "update t_jurnald set nilai='" & Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##") & "' where no_transaksi='" & NoTransaksi & "' and kode1='" & rs!kode_bahan & "'"
'        End If
'        rs.MoveNext
'    Wend
    
    
    
    
    conntrans.Execute "update t_jualh set status_posting='3' where nomer_jual='" & NoTransaksi & "'"
    PostingJurnal_Penjualan = True
Else
    GoTo err
End If
If rs.State Then rs.Close
conn.Close
conntrans.CommitTrans
i = 0

Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function


Public Function PostingJurnal_SuratJalan(NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim NoJurnal As String
Dim tanggal As Date
Dim rs As New ADODB.Recordset
Dim Row As Integer
Dim i As Byte
    i = 0
    PostingJurnal_SuratJalan = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    
    rs.Open "select * from t_suratjalanh where nomer_suratjalan='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
        tanggal = rs("tanggal_suratjalan")
    End If
    rs.Close
    
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Surat Jalan", 0, "KB", conntrans
    
    rs.Open "select s.kode_bahan,b.nama_bahan,sum(s.berat*s.hpp) as total,acc_hppjual,acc_persediaan,sum(s.qty) as qty from t_suratjalan_timbang s " & _
        " inner join setting_accbahan m on s.kode_bahan=m.kode_bahan " & _
        " left join ms_bahan b on b.kode_bahan=s.kode_bahan " & _
        " where s.nomer_suratjalan='" & NoTransaksi & "' group by s.kode_bahan,acc_hppjual,acc_persediaan", conn, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        
        If rs!Acc_Persediaan <> "" Then
            add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs(1), Row, conntrans, "Surat Jalan", Left(rs!nama_bahan, 35)
    '        JurnalD NoTransaksi, NoJurnal, rs!Acc_Persediaan, "k", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
            Row = Row + 1
            add_jurnaldetail NoTransaksi, NoJurnal, rs!acc_hppjual, rs(1), 0, Row, conntrans, "Surat Jalan", Left(rs!nama_bahan, 35)
    '        JurnalD NoTransaksi, NoJurnal, rs!acc_hppjual, "d", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
            conntrans.Execute "update t_jurnald set nilai='" & Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##") & "' where no_transaksi='" & NoTransaksi & "' and kode1='" & rs!kode_bahan & "'"
        End If
        rs.MoveNext
    Wend
    
    rs.Close
    conntrans.Execute "update t_suratjalanh set status_posting='3' where nomer_suratjalan='" & NoTransaksi & "'"
    conntrans.CommitTrans
    PostingJurnal_SuratJalan = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close

End Function


Public Function PostingJurnal_Pembelian(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim i As Byte
Dim NoJurnal As String
Dim NoSuratJalan As String
Dim kode_supplier As String
Dim Nama_Supplier As String
Dim total As Currency
Dim gudang As String
Dim pajak As Currency
Dim Row As Integer
i = 0
PostingJurnal_Pembelian = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select t.*,s.Nama_Supplier from t_belih t left join ms_supplier s on s.kode_supplier=t.kode_supplier where t. nomer_beli='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    tanggal = rs!tanggal_beli
    tanggal_jatuhtempo = rs!tanggal_jatuhtempo
    kode_supplier = rs!kode_supplier
    Nama_Supplier = Left(rs!Nama_Supplier, 35)
    NoSuratJalan = rs!nomer_invoice
    total = rs!total
    pajak = 0
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Pembelian", rs!total, "PB", conntrans

    If rs!cara_bayar = "T" Then
'        insertbayarhutangtunai NoTransaksi, rs!tanggal_beli, kode_supplier, rs!total, conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckas), 0, CCur(rs!total), Row, conntrans, "Pembelian", Nama_Supplier
    Else
'        insertkartuHutang "Pembelian", NoTransaksi, Tanggal, kode_supplier, rs!total * -1, conntrans
'        inserthutang kode_supplier, NoTransaksi, NoSuratJalan, rs!total, tanggal_jatuhtempo, conntrans

        rs.Close
        rs.Open "select acc_hutang from setting_accsupplier where kode_supplier='" & kode_supplier & "'"
        If Not rs.EOF Then add_jurnaldetail NoTransaksi, NoJurnal, rs(0), 0, total, Row, conntrans, "Pembelian", Nama_Supplier
    End If
    rs.Close
    
    rs.Open "select d.*,b.nama_bahan from t_belid d inner join setting_accbahan s on d.kode_bahan=s.kode_bahan " & _
            "left join  ms_bahan b on b.kode_bahan=d.kode_bahan " & _
            "where d.nomer_beli='" & NoTransaksi & "'", conn, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        'updatehpp NoTransaksi, rs!kode_bahan, gudang, rs!nomer_serial, rs!berat, rs!harga, conntrans
'        conntrans.Execute "update stock set hpp='" & rs!harga & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "'"
'        conntrans.Execute "update tmp_kartustock set hpp='" & rs!harga & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "' and id='" & NoSuratJalan & "'"
        add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, (rs!berat * rs!harga), 0, Row, conntrans, "Pembelian", Left(rs!nama_bahan, 35)
        rs.MoveNext
    Wend
    Row = Row + 1
    
    conntrans.Execute "update t_belih set status_posting='3' where nomer_beli='" & NoTransaksi & "'"
    PostingJurnal_Pembelian = True
Else
    GoTo err
End If
rs.Close
conntrans.CommitTrans
PostingJurnal_Pembelian = True
i = 0
conn.Close
conntrans.Close
Exit Function
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function

Public Function postingjurnal_returbeli(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim NoJurnal As String
Dim kode_supplier As String
Dim total As Currency
Dim gudang As String
Dim pajak As Currency
Dim Row As Integer
i = 0
postingjurnal_returbeli = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select * from t_returbelih where nomer_returbeli='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    
    tanggal = rs!tanggal_returbeli
    kode_supplier = rs!kode_supplier
    total = rs!total
    pajak = 0
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Retur Beli", rs!total, "RB", conntrans
'    insertpiutangretur kode_supplier, NoTransaksi, rs!total, conntrans
    rs.Close
    rs.Open "select acc_piutangretur from setting_accsupplier where kode_supplier='" & kode_supplier & "'", conntrans
    If rs.EOF Then
        MsgBox "Perkiraan retur untuk supplier " & kode_supplier & " belum diisi"
        GoTo err
    End If
    add_jurnaldetail NoTransaksi, NoJurnal, rs(0), total, 0, Row, conntrans, "Retur Beli", kode_supplier
    rs.Close
    
    rs.Open "select d.kode_bahan,acc_persediaan,sum(berat*harga) as total from t_returbelid d inner join setting_accbahan s on d.kode_bahan=s.kode_bahan where nomer_returbeli='" & NoTransaksi & "' group by d.kode_bahan,acc_persediaan", conn, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs!total, Row, conntrans, "Retur Beli", rs!kode_bahan
'        pajak = pajak + ((rs!qty * rs!harga) / 11)
        rs.MoveNext
    Wend
    Row = Row + 1

    conntrans.Execute "update t_returbelih set status_posting='3' where nomer_returbeli='" & NoTransaksi & "'"
    postingjurnal_returbeli = True
Else
    GoTo err
End If
rs.Close
conntrans.CommitTrans
i = 0
conn.Close
conntrans.Close
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function


Public Function postingjurnal_returjual(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim kode_customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim pajak As Double
i = 0
postingjurnal_returjual = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1
rs.Open "select * from t_returjualh where nomer_returjual='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    gudang = rs!kode_gudang
    tanggal = rs!tanggal_returjual
    kode_customer = rs!kode_customer
'    pajak = rs!tax
    total = rs!total
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
'    insertpiutang rs!kode_customer, NoTransaksi, rs!total, conntrans

'    inserthutangretur rs!kode_customer, NoTransaksi, rs!total, conntrans

    add_jurnal NoTransaksi, NoJurnal, tanggal, "Penjualan", rs!total, "RJ", conntrans
    rs.Close
    rs.Open "select acc_hutangretur from setting_acccustomer where kode_customer='" & kode_customer & "'"
    If rs.EOF Then
        MsgBox "Perkiraan hutang retur untuk customer " & kode_customer & " belum diisi"
        GoTo err
    End If
    add_jurnaldetail NoTransaksi, NoJurnal, rs(0), 0, total, Row, conntrans, "Retur", kode_customer
    Row = Row + 1
    add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accreturjual), total, 0, Row, conntrans, "", kode_customer

    rs.Close
    
    rs.Close
    rs.Open "select s.kode_bahan,sum(s.qty*s.hpp),acc_hpp,acc_persediaan from t_returjuald s " & _
        " inner join setting_accbahan m on s.kode_bahan=m.kode_bahan where s.nomer_returjual='" & NoTransaksi & "' group by s.kode_bahan,acc_hpp,acc_persediaan", conn, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, rs(1), 0, Row, conntrans, "Retur", rs!kode_bahan
        Row = Row + 1
        add_jurnaldetail NoTransaksi, NoJurnal, rs!acc_hpp, 0, rs(1), Row, conntrans, , "Retur", rs!kode_bahan
        rs.MoveNext
    Wend
    conntrans.Execute "update t_returjualh set status_posting='3' where nomer_returjual='" & NoTransaksi & "'"
    postingjurnal_returjual = True
Else
    GoTo err
End If
If rs.State Then rs.Close
conn.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function


Private Sub add_jurnal(ByRef NoTransaksi As String, no_jurnal As String, tanggal As Date, keterangan As String, Nominal As Currency, Tipe As String, ByRef cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
'    fields(4) = "totaldebet"
'    fields(5) = "totalkredit"
    fields(4) = "tipe"

    nilai(0) = NoTransaksi
    nilai(1) = no_jurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
'    nilai(4) = CCur(nominal)
'    nilai(5) = CCur(nominal)
    nilai(4) = Tipe

    cn.Execute tambah_data2(table_name, fields, nilai)

End Sub

Private Sub add_jurnaldetail(ByRef NoTransaksi As String, NoJurnal As String, kode_acc As String, debet As Currency, kredit As Currency, Row As Integer, ByRef cn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "kredit"
    fields(4) = "no_urut"
    fields(5) = "no_jurnal"
    fields(6) = "keterangan"
    fields(7) = "kode1"
    fields(8) = "kode2"

    nilai(0) = NoTransaksi
    nilai(1) = kode_acc
    nilai(2) = Replace(Format(debet, "###0.##"), ",", ".")
    nilai(3) = Replace(Format(kredit, "###0.##"), ",", ".")
    nilai(4) = Row
    nilai(5) = NoJurnal
    nilai(6) = keterangan
    nilai(7) = kode1
    nilai(8) = kode2

    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub


Public Function PostingJurnal_KasBank(ByRef NoTransaksi As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim AccKasBank As String
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String, KetBG As String
Dim NoJurnal As String
Dim ket As String

'On Error GoTo err

    PostingJurnal_KasBank = False

    conn.Open strcon
    
    conn.BeginTrans

    Tipe = Left(NoTransaksi, 2)

    rs.Open "select tanggal,acc_kasbank,total,keterangan,ket_BG from t_kasbankh where no_transaksi='" & NoTransaksi & "'", conn
    If rs.EOF Then GoTo err
    tanggal = rs("tanggal")
    AccKasBank = rs("acc_kasbank")
    NilaiJurnalH = rs("total")
    ket = rs!keterangan
    KetBG = rs!ket_bg
    rs.Close
    
    Select Case Tipe
        Case "KM"
            keterangan = "Kas Masuk"
        Case "KK"
            keterangan = "Kas Keluar"
        Case "BM"
            keterangan = "Bank Masuk"
        Case "BK"
            keterangan = "Bank Keluar"
    End Select
    
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    
    If Tipe = "KM" Or Tipe = "BM" Then
        JurnalD NoTransaksi, NoJurnal, AccKasBank, "d", NilaiJurnalH, conn, ket, KetBG, , , y + 1
    End If
    
    rs.Open "select d.kode_acc,d.nilai,keterangan from t_kasbankd d " & _
            "where d.no_transaksi='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        y = y + 1
        NilaiJurnalD = rs("nilai")
                
        If Tipe = "KM" Or Tipe = "BM" Then
            JurnalD NoTransaksi, NoJurnal, rs("kode_acc"), "k", NilaiJurnalD, conn, rs!keterangan, , , , y
        Else
            JurnalD NoTransaksi, NoJurnal, rs("kode_acc"), "d", NilaiJurnalD, conn, rs!keterangan, , , , y
        End If
        
        rs.MoveNext
    Wend
    rs.Close
    
    If Tipe = "KK" Or Tipe = "BK" Then
        JurnalD NoTransaksi, NoJurnal, AccKasBank, "k", NilaiJurnalH, conn, ket, , , , y + 1
    End If
    
    JurnalH NoTransaksi, NoJurnal, tanggal, keterangan, conn
    
    conn.Execute "update t_kasbankh set status_posting='3' where no_transaksi='" & NoTransaksi & "'"

    
    conn.CommitTrans
    
    DropConnection

    PostingJurnal_KasBank = True
    
    Exit Function
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Function


Public Function PostingJurnal_BayarPiutang(ByRef NoTransaksi As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_Kas As String, Acc_Bank As String, Acc_HutangReturJual As String
Dim Acc_PendapatanLain As String, Acc_BiayaLain As String
Dim Acc_UMJual As String
Dim Acc_piutang As String
Dim CaraBayar As String
Dim JumlahBayar As Double, NilaiTransfer As Double, UangMuka As Double
Dim TotalRetur As Double
Dim KodeBank As String
Dim selisih As Double
Dim TotalPiutang As Double
Dim KodeCustomer As String
Dim Acc_PiutangGiroDiTangan As String, NilaiBG As Double
Dim NoBG As String, TanggalCair As Date

'On Error GoTo err
    
    PostingJurnal_BayarPiutang = False

    conn.Open strcon
    conn.BeginTrans
    
    Acc_Kas = getAcc("kas")
    Acc_HutangReturJual = getAcc("Hutang Retur Jual")
    Acc_PendapatanLain = getAcc("pendapatanlain")
    Acc_BiayaLain = getAcc("biayalain")
    Acc_UMJual = getAcc("um Penjualan")
    Acc_PiutangGiroDiTangan = getAcc("piutang giro")
    
    rs.Open "select tanggal_bayarPiutang as tanggal,kode_Customer,jumlah_tunai,selisih, " & _
            "uang_muka from t_bayarPiutangh where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    
    
    UangMuka = rs("uang_muka")
    
    KodeCustomer = rs("kode_Customer")
    rs.Close
    
    rs.Open "select sum(jumlah) as TotalRetur from t_bayarPiutang_retur where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    If Not rs.EOF And IsNull(rs("TotalRetur")) = False Then
        TotalRetur = rs("TotalRetur")
    End If
    rs.Close
    
    NilaiTransfer = 0
    Dim rs1 As New ADODB.Recordset
    rs.Open "select * from t_bayarpiutang_transfer where nomer_bayarpiutang='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        
        rs1.Open "Select kode_acc from ms_bank " & _
                "where kode_bank='" & rs!Kode_Bank & "'", conn
        If Not rs1.EOF Then
            Acc_Bank = rs1("kode_acc")
        Else
            MsgBox "Kode Bank " & rs!Kode_Bank & " belum mempunyai Account"
            GoTo err
        End If
        rs1.Close
        NilaiTransfer = NilaiTransfer + rs!Nilai_Transfer
        JurnalD NoTransaksi, NoJurnal, Acc_Bank, "d", rs!Nilai_Transfer, conn, "Pembayaran Piutang", KodeCustomer, , , y
        rs.MoveNext
    Wend
    rs.Close
    NilaiBG = 0
    rs.Open "select * from t_bayarpiutang_cek where nomer_bayarpiutang='" & NoTransaksi & "'", conn
    While Not rs.EOF
        NilaiBG = NilaiBG + rs("nilai_cek")
        rs.MoveNext
    Wend
    rs.Close
    JurnalD NoTransaksi, NoJurnal, Acc_PiutangGiroDiTangan, "d", NilaiBG, conn, "Pembayaran Piutang", KodeCustomer, , , y
    TotalPiutang = JumlahBayar + UangMuka + TotalRetur + selisih + NilaiBG + NilaiTransfer
    rs.Open "Select acc_Piutang from setting_accCustomer  " & _
            "where kode_Customer='" & KodeCustomer & "'", conn
    If Not rs.EOF Then
        Acc_piutang = rs("acc_Piutang")
    End If
    rs.Close
    
    
    
    y = 1
    
    If JumlahBayar > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_Kas, "d", JumlahBayar, conn, "Pembayaran Piutang", KodeCustomer, , , y
    End If
    
    y = y + 1
    If TotalRetur > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_HutangReturJual, "d", TotalRetur, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
        y = y + 1
    End If
    
    If UangMuka > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_UMJual, "d", UangMuka, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
        y = y + 1
    End If
    
    
    JurnalD NoTransaksi, NoJurnal, Acc_piutang, "k", TotalPiutang, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
    y = y + 1
    
    
    If selisih < 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_PendapatanLain, "k", selisih * -1, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
    ElseIf selisih > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_BiayaLain, "k", selisih, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
    End If

    
    JurnalH NoTransaksi, NoJurnal, tanggal, "Pembayaran Piutang", conn
    
    conn.Execute "update t_bayarPiutangh set status_posting='3' where nomer_bayarPiutang='" & NoTransaksi & "'"

    conn.CommitTrans
    
    DropConnection

    PostingJurnal_BayarPiutang = True
    
    Exit Function
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Function


Public Function PostingJurnal_BayarHutang(ByRef NoTransaksi As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_Kas As String, Acc_Bank As String, Acc_PiutangReturBeli As String
Dim Acc_PendapatanLain As String, Acc_BiayaLain As String
Dim Acc_UMBeli As String
Dim Acc_Hutang As String
Dim CaraBayar As String
Dim JumlahBayar As Double, NilaiTransfer As Double, UangMuka As Double
Dim TotalRetur As Double
Dim KodeBank As String
Dim selisih As Double
Dim TotalHutang As Double
Dim Kodesupplier As String
Dim Acc_HutangGiro As String, NilaiBG As Double
Dim NoBG As String, TanggalCair As Date

'On Error GoTo err
    PostingJurnal_BayarHutang = False
    conn.Open strcon
    conn.BeginTrans
    
    Acc_Kas = getAcc("kas")
    Acc_PiutangReturBeli = getAcc("piutang retur beli")
    Acc_PendapatanLain = getAcc("pendapatanlain")
    Acc_BiayaLain = getAcc("biayalain")
    Acc_UMBeli = getAcc("um pembelian")
    Acc_HutangGiro = getAcc("hutang giro")
        
    rs.Open "select tanggal_bayarhutang as tanggal,kode_supplier,jumlah_tunai, uang_muka,selisih from t_bayarhutangh where nomer_bayarhutang='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    UangMuka = rs("uang_muka")
    Kodesupplier = rs("kode_supplier")
    rs.Close
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    rs.Open "select sum(jumlah) as TotalRetur from t_bayarhutang_retur where nomer_bayarhutang='" & NoTransaksi & "'", conn
    If Not rs.EOF And IsNull(rs("TotalRetur")) = False Then
        TotalRetur = rs("TotalRetur")
    End If
    rs.Close
    y = 1
    NilaiTransfer = 0
    Dim rs1 As New ADODB.Recordset
    rs.Open "select * from t_bayarhutang_transfer where nomer_bayarhutang='" & NoTransaksi & "'", conn
    While Not rs.EOF
        
        rs1.Open "Select kode_acc from ms_bank " & _
                "where kode_bank='" & rs!Kode_Bank & "'", conn
        If Not rs1.EOF Then
            Acc_Bank = rs1("kode_acc")
        Else
            MsgBox "Kode Bank " & rs!Kode_Bank & " belum mempunyai Account"
            GoTo err
        End If
        rs1.Close
        NilaiTransfer = NilaiTransfer + rs!Nilai_Transfer
        JurnalD NoTransaksi, NoJurnal, Acc_Bank, "k", rs!Nilai_Transfer, conn, "Pembayaran Hutang", Kodesupplier, , , y
        
        y = y + 1
        rs.MoveNext
    Wend
    rs.Close
    NilaiBG = 0
'    rs.Open "select * from t_bayarhutang_cek where nomer_bayarhutang='" & NoTransaksi & "'", conn
'    While Not rs.EOF
'        conn.Execute "insert into list_bg(nomer_transaksi,tanggal,nomer_bg,tanggal_cair, " & _
'                     "jenis,kode_supplier,keterangan_bank,nominal) " & _
'                    "values ('" & NoTransaksi & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & rs!no_cek & "','" & Format(rs!Tanggal_Cair, "yyyy/MM/dd HH:mm:ss") & "', " & _
'                    "'h','" & Kodesupplier & "','" & rs!Kode_Bank & "'," & Replace(rs!nilai_cek, ",", ".") & " )"
'        NilaiBG = NilaiBG + rs("nilai_cek")
'        conn.Execute "update t_belibukud set status=0 where jenis='" & rs!Jenis & "' and nomer='" & rs!no_cek & "'"
'        rs.MoveNext
'    Wend
'    rs.Close
'
    TotalHutang = JumlahBayar + UangMuka + TotalRetur + selisih + NilaiBG + NilaiTransfer
    
    JurnalD NoTransaksi, NoJurnal, Acc_HutangGiro, "k", NilaiBG, conn, "Pembayaran Hutang", Kodesupplier, , , y
    y = y + 1
    
    rs.Open "Select acc_hutang from setting_accSupplier  " & _
            "where kode_supplier='" & Kodesupplier & "'", conn
    If Not rs.EOF Then
        Acc_Hutang = rs("acc_hutang")
    End If
    rs.Close
    
    
    
    JurnalD NoTransaksi, NoJurnal, Acc_Hutang, "d", TotalHutang, conn, "Pembayaran Hutang", Kodesupplier, , , y
    y = y + 1

    
    If JumlahBayar > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_Kas, "k", JumlahBayar, conn, "Pembayaran Hutang", Kodesupplier, , , y
    End If
    y = y + 1
    
    If TotalRetur > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_PiutangReturBeli, "k", TotalRetur, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
        y = y + 1
    End If
    If selisih > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_BiayaLain, "k", selisih, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
        y = y + 1
    ElseIf selisih < 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_PendapatanLain, "d", selisih * -1, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
        y = y + 1
    End If
    
    If UangMuka > 0 Then
        JurnalD NoTransaksi, NoJurnal, Acc_UMBeli, "k", UangMuka, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
    End If
    
    JurnalH NoTransaksi, NoJurnal, tanggal, "Pembayaran Hutang", conn
    
'    table_name = "t_bayarhutang_beli"
'    fields(0) = "nomer_bayarhutang"
'    fields(1) = "nomer_beli"
'    fields(2) = "jumlah_bayar"
'    fields(3) = "no_urut"
'
    
'    rs.Open "select nomer_beli,jumlah_bayar from t_bayarhutang_beli " & _
'            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
'    While Not rs.EOF
'        conn.Execute "update list_hutang set total_bayar=total_bayar + " & rs("jumlah_bayar") & " " & _
'                     "where nomer_transaksi='" & rs("nomer_beli") & "'"
'
'        insertkartuHutang "Bayar Hutang", NoTransaksi, tanggal, Kodesupplier, rs("jumlah_bayar"), conn
'
'        rs.MoveNext
'    Wend
'    rs.Close
    
'    rs.Open "select nomer_retur,jumlah from t_bayarhutang_retur " & _
'            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
'    While Not rs.EOF
'        conn.Execute "update list_piutang_retur set total_bayar=total_bayar + " & rs("jumlah") & " " & _
'                     "where nomer_transaksi='" & rs("nomer_retur") & "'"
'
'        insertkartuHutang "Bayar Hutang (Retur Beli)", NoTransaksi, tanggal, Kodesupplier, rs("jumlah"), conn
'
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Execute "update ms_supplier set deposit=deposit-" & Replace(UangMuka, ",", ".") & " where kode_supplier='" & Kodesupplier & "'"
    conn.Execute "update t_bayarhutangh set status_posting='3' where nomer_bayarhutang='" & NoTransaksi & "'"

    conn.CommitTrans
    
    DropConnection

    PostingJurnal_BayarHutang = True

    Exit Function
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Function

Private Sub JurnalH(ByRef NoTransaksi As String, NoJurnal As String, tanggal As Date, keterangan As String, ByRef cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "tipe"
    fields(5) = "userid"

    nilai(0) = NoTransaksi
    nilai(1) = NoJurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
    nilai(4) = Left(NoTransaksi, 2)
    nilai(5) = User

'    tambah_data table_name, fields, nilai
    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub JurnalD(ByRef NoTransaksi As String, NoJurnal As String, kodeAcc As String, Posisi As String, nilaiJurnal As Double, ByRef cn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String, Optional KeteranganNilai As String, Optional NomorUrut As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    
    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    If UCase(Posisi) = "D" Then
        fields(2) = "debet"
    Else
        fields(2) = "kredit"
    End If
    fields(3) = "no_jurnal"
    fields(4) = "keterangan"
    fields(5) = "kode1"
    fields(6) = "kode2"
    fields(7) = "nilai"
    fields(8) = "no_urut"
    

    nilai(0) = NoTransaksi
    nilai(1) = kodeAcc
    nilai(2) = Replace(nilaiJurnal, ",", ".")
    nilai(3) = NoJurnal
    nilai(4) = keterangan
    nilai(5) = kode1
    nilai(6) = kode2
    nilai(7) = Replace(KeteranganNilai, ",", ".")
    nilai(8) = NomorUrut

'    tambah_data table_name, fields, nilai
    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub
