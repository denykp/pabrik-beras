VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterTinta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Tinta"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9060
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   9060
   Begin VB.TextBox txtIsi 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1815
      TabIndex        =   5
      Top             =   2190
      Width           =   870
   End
   Begin VB.ComboBox cmbSatuan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1815
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1770
      Width           =   1995
   End
   Begin VB.ComboBox cmbWarna 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterTinta.frx":0000
      Left            =   1800
      List            =   "frmMasterTinta.frx":0002
      TabIndex        =   3
      Text            =   "cmbWarna"
      Top             =   1350
      Width           =   1995
   End
   Begin VB.ComboBox cmbMerk 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel"
      Height          =   330
      Left            =   5655
      TabIndex        =   20
      Top             =   2955
      Width           =   1230
   End
   Begin VB.CommandButton cmdBaru 
      Caption         =   "&Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3720
      Picture         =   "frmMasterTinta.frx":0004
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   3540
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3270
      Picture         =   "frmMasterTinta.frx":0536
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   2865
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4485
      Picture         =   "frmMasterTinta.frx":0A68
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   2865
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdHistory 
      Caption         =   "&History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   7455
      Picture         =   "frmMasterTinta.frx":0F9A
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   3540
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4320
      Picture         =   "frmMasterTinta.frx":109C
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   2010
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CheckBox chkAktif 
      Caption         =   "Aktif"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   14
      Top             =   2685
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   765
      Picture         =   "frmMasterTinta.frx":119E
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3540
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3870
      Picture         =   "frmMasterTinta.frx":12A0
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2250
      Picture         =   "frmMasterTinta.frx":13A2
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3540
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5175
      Picture         =   "frmMasterTinta.frx":14A4
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   3540
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1800
      TabIndex        =   1
      Top             =   570
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1800
      TabIndex        =   0
      Top             =   180
      Width           =   1950
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8100
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Kg"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2808
      TabIndex        =   27
      Top             =   2232
      Width           =   780
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Isi Per Pail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   228
      TabIndex        =   26
      Top             =   2196
      Width           =   960
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1470
      TabIndex        =   25
      Top             =   2220
      Width           =   150
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Satuan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   24
      Top             =   1770
      Width           =   780
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Warna"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   225
      TabIndex        =   23
      Top             =   1395
      Width           =   1320
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Merk"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   21
      Top             =   930
      Width           =   780
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1455
      TabIndex        =   19
      Top             =   600
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   18
      Top             =   180
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   75
      TabIndex        =   17
      Top             =   3210
      Width           =   2130
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Tinta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   228
      TabIndex        =   16
      Top             =   576
      Width           =   1080
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Tinta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   22
      Top             =   225
      Width           =   1140
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterTinta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim foc As Byte
Dim colname() As String
Private Sub cmdBaru_Click()
    reset_form
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
i = 0
    If txtField(0).text = "" Then
        MsgBox "Silahkan masukkan Kode tinta terlebih dahulu!", vbCritical
        txtField(0).SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    rs.Open "select * from t_hasilcetakD_tinta where kode_tinta='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
'        conn.Execute "update ms_tinta set flag='0' where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from ms_tinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from rpt_ms_tinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from stock_tinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from tmp_kartustocktinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from hpp_tinta where kode_tinta='" & txtField(0).text & "'"
    Else
        conn.Execute "delete from ms_tinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from rpt_ms_tinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from stock_tinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from tmp_kartustocktinta where kode_tinta='" & txtField(0).text & "'"
        conn.Execute "delete from hpp_tinta where kode_tinta='" & txtField(0).text & "'"
    End If
    If rs.State Then rs.Close

    conn.CommitTrans
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    Set conn = Nothing
    cmdPrev_Click
    cmdNext_Click
    reset_form
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    conn.RollbackTrans
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 kode_tinta from ms_tinta where kode_tinta>'" & txtField(0).text & "' order by kode_tinta", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection

    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 kode_tinta from ms_tinta where kode_tinta<'" & txtField(0).text & "' order by kode_tinta desc", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
'    frmPrintPLU.kode = txtField(0).text
'    frmPrintPLU.Show
    frmPrintListBarang.Show vbModal
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select *  from ms_tinta" ' order by kode_tinta"
    frmSearch.nmform = "frmMasterTinta"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_tinta"
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchPerkiraan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.proc = "cari_perkiraan"
    frmSearch.col = 0

    frmSearch.Index = -1

    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
'    cmdSimpan.SetFocus
End Sub


Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next J
      
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
i = 1
    rs.Open "select * from stock_tinta where kode_tinta='" & txtField(0).text & "' ", conn
    If rs.EOF Then
        rs.Close
        conn.Execute "insert into stock_tinta  (kode_tinta,stock,kode_gudang)  select '" & txtField(0).text & "',0,kode_gudang from ms_gudang where stock_tinta=1"

    End If
    If rs.State Then rs.Close
    
    rs.Open "select * from hpp_tinta where kode_tinta='" & txtField(0).text & "'", conn
    If rs.EOF Then
        rs.Close
        conn.Execute "insert into hpp_tinta  (kode_tinta,hpp) values ('" & txtField(0).text & "',0)"
    End If
    If rs.State Then rs.Close

    rs.Open "select * from hst_hpptinta where kode_tinta='" & txtField(0).text & "'", conn
    If rs.EOF Then
        rs.Close
        add_dataHstHpp 1, conn
    End If
    If rs.State Then rs.Close
    
    rs.Open "select * from ms_tinta where kode_tinta='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        add_dataheader 2, conn
        add_dataheaderRpt 2, conn
'        add_dataAcc 2, conn
    Else
        add_dataheader 1, conn
        add_dataheaderRpt 1, conn
        conn.Execute "insert into setting_accTinta  (kode_tinta,kode_acc,kode_gudang)  select '" & txtField(0).text & "',tinta_acc,kode_gudang from ms_gudang where stock_tinta=1"

'        add_dataAcc 1, conn
    End If
    If rs.State Then rs.Close

    
'    If lblHPP.Locked = False And lblStock.Locked = False Then
'        conn.Execute "update hpp_tinta set hpp=" & lblHPP & " where kode_tinta='" & txtField(0).text & "'"
'        conn.Execute "update hst_hpptinta set qty=" & lblStock & ",harga=" & lblHPP & ",hpp=" & lblHPP & ",stockakhir=" & lblStock & " where kode_tinta='" & txtField(0).text & "' and kode_gudang='" & gudang & "' and nomer_transaksi='Awal'"
'
'    End If
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"

    DropConnection
    reset_form

    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtIsi.text = 0
    If cmbMerk.ListCount > 0 Then cmbMerk.ListIndex = 0
    If cmbSatuan.ListCount > 0 Then cmbSatuan.ListIndex = 0
    If cmbWarna.ListCount > 0 Then cmbWarna.ListIndex = 0
    
'    lblStock.Locked = False
'    lblHPP.Locked = False
'    lblStock = "0"
'    lblHPP = "0"
    txtField(0).SetFocus
    cmdPrev.Enabled = False
    cmdNext.Enabled = False
    
    
End Sub
Private Sub add_dataheader(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "ms_tinta"
    
    fields(0) = "kode_tinta"
    fields(1) = "nama_tinta"
    fields(2) = "merek"
    fields(3) = "warna"
    fields(4) = "satuan"
    fields(5) = "isi"
    
    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbMerk.text
    nilai(3) = cmbWarna.text
    nilai(4) = cmbSatuan.text
    nilai(5) = Replace(txtIsi.text, ",", ".")

    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "kode_tinta='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_dataheaderRpt(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)

    table_name = "rpt_ms_tinta"
    fields(0) = "kode_tinta"
    fields(1) = "nama_tinta"
    fields(2) = "warna"
    
    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbWarna.text

    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "kode_tinta='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_dataAcc(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(3)
    ReDim nilai(3)
    table_name = "setting_accTinta"
    fields(0) = "kode_tinta"
    fields(1) = "kode_acc"
    fields(2) = "kode_gudang"
    
    nilai(0) = txtField(0).text
    nilai(1) = ""
    nilai(2) = ""
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
'    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub

Private Sub add_dataHstHpp(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(11)
    ReDim nilai(11)

    table_name = "hst_hppTinta"
    fields(0) = "jenis"
    fields(1) = "kode_gudang"
    fields(2) = "nomer_transaksi"
    fields(3) = "tanggal"
    fields(4) = "kode_tinta"
    fields(5) = "stockawal"
    fields(6) = "hppawal"
    fields(7) = "qty"
    fields(8) = "harga"
    fields(9) = "hpp"
    fields(10) = "stockakhir"

    nilai(0) = "Awal"
    nilai(1) = gudang
    nilai(2) = "-"
    nilai(3) = Format(Now, "yyyy/MM/dd hh:mm:ss")
    nilai(4) = txtField(0).text
    nilai(5) = 0
    nilai(6) = 0
    nilai(7) = 0
    nilai(8) = 0
    nilai(9) = 0
    nilai(10) = 0
    
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub

Private Sub add_stockTinta(action As String, gudang As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)

    table_name = "stock_tinta"
    fields(0) = "kode_tinta"
    fields(1) = "stock"
    fields(2) = "kode_gudang"
    
    nilai(0) = txtField(0).text
    nilai(1) = 0
    nilai(2) = gudang
    If action = 1 Then
    conn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "kode_tinta='" & txtField(0).text & "'", conn
    End If
End Sub

Public Sub cari_data()
    If txtField(0).text = "" Then Exit Sub
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from ms_tinta where kode_tinta='" & txtField(0).text & "' ", conn

    If Not rs.EOF Then
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        If Not IsNull(rs!nama_tinta) Then txtField(1).text = rs!nama_tinta
        If Not IsNull(rs!isi) Then txtIsi.text = rs!isi Else txtIsi = 0
'        If IsNull(rs("nama_acc")) = False Then
'            txtKode.text = rs("kode_acc")
'            txtNama.text = rs("nama_acc")
'        End If
        If Not IsNull(rs!merek) Then SetComboTextRight rs!merek, cmbMerk
        If Not IsNull(rs!warna) Then SetComboText rs!warna, cmbWarna
       

        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
'   If rs.State Then rs.Close
        
'        lblHPP = GetHPPTinta(txtField(0).text, conn)
'        lblHPP.Locked = True
'        lblStock = getStockTinta(txtField(0), gudang, conn)
'        lblStock.Locked = True
    Else
        cmdPrev.Enabled = False
        cmdPrev.Enabled = False
        txtField(1).text = ""
'        txtKode.text = ""
'        txtNama.text = ""
        txtIsi = 0
        cmdHapus.Enabled = False
        mnuHapus.Enabled = True
'        lblHPP = 0
'        lblStock = 0
''        lblHPP.Locked = False
'        lblStock.Locked = False
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub load_combo()
    
    conn.ConnectionString = strcon
    conn.Open
    cmbMerk.Clear
    rs.Open "select * from var_merk order by merk", conn
    While Not rs.EOF
        cmbMerk.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbWarna.Clear
    rs.Open "select * from ms_warna order by warna", conn
    While Not rs.EOF
        cmbWarna.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbSatuan.Clear
    rs.Open "select * from var_satuan order by satuan", conn
        While Not rs.EOF
            cmbSatuan.AddItem rs(0)
        rs.MoveNext
        Wend
    rs.Close
    
    If cmbMerk.ListCount > 0 Then cmbMerk.ListIndex = 0
    If cmbWarna.ListCount > 0 Then cmbWarna.ListIndex = 0
    conn.Close
End Sub




Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
   
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
'    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        If foc = 1 And txtField(0).text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If

End Sub

Private Sub Form_Load()
    load_combo
    reset_form
       
End Sub



Private Sub lblHPP_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub lblStock_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub


Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index).text)
    foc = 1
End Sub

Private Sub lblHPP_GotFocus()
    lblHPP.SelStart = 0
    lblHPP.SelLength = Len(lblHPP.text)
    foc = 1
End Sub

Private Sub lblStock_GotFocus()
    lblStock.SelStart = 0
    lblStock.SelLength = Len(lblStock.text)
    foc = 1
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index = 0 Then
            cari_data
        End If
    ElseIf Index = 2 Then
        Angka KeyAscii
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    foc = 0
    If Index = 0 And txtField(0).text <> "" Then cari_data

End Sub


Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
        MsgBox "Loading selesai"
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Function loadexcelfile1(filename As String) As Boolean
On Error GoTo err
Dim exapp As Object
Dim range As String
Dim rs2 As New ADODB.Recordset
Dim col As Byte
Dim i As Integer
loadexcelfile1 = False
Set exapp = CreateObject("excel.application")
exapp.Workbooks.Open filename
exapp.Sheets(1).Select
conn.Open strcon
col = Asc("A")
range = Chr(col) & "1"

While exapp.Workbooks.Application.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exapp.Workbooks.Application.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

i = 2
While exapp.range("A" & CStr(i)) <> ""
        rs2.Open "select * from var_kategori where kategori='" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
'        rs2.Open "select * from ms_bahan where kode_tinta='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
'        If rs2.EOF Then
'            newitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))
'        Else
'            updateitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))
'
'        End If
'        rs2.Close
        rs2.Open "select * from stock_tinta where kode_tinta='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
        conn.Execute "insert into stock_tinta  (kode_gudang,kode_tinta,stock)  values ('" & gudang & "'," & _
            "'" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
            "'" & exapp.range(Chr(getcolindex("stok", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from hpp_tinta where kode_tinta='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
        conn.Execute "insert into hpp_tinta (kode_tinta,hpp)  values ('" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
            "" & IIf(IsNumeric(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), CDbl(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), 0) & ")"
        End If
        rs2.Close

    i = i + 1
Wend
exapp.Workbooks.Close
loadexcelfile1 = True
exapp.Application.Quit
Set exapp = Nothing

MsgBox "Success"
conn.Close
Exit Function
err:
MsgBox err.Description
Resume Next
''If conn.State Then conn.Close
''If exapp.Workbooks.Count > 0 Then exapp.Workbooks.Close
''exapp.Application.Quit
''Set exapp = Nothing
End Function
Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset

Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=Excel 8.0"
con.Open stcon
conn.Open strcon
rs.Open "select * from [sheet1$]", con
While Not rs.EOF
    If rs(0) <> "E" Then
        rs2.Open "select * from var_kategori where kategori='" & rs("kategori") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & rs("kategori") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & rs("merk") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & rs("merk") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & rs("satuan") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & rs("satuan") & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_tinta where kode_tinta='" & rs("kode_tinta") & "'", conn
        If rs2.EOF Then
            newitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")

            conn.Execute "insert into stock_tinta  (kode_gudang,kode_tinta,stock)  select gudang,'" & rs("kodebarang") & "','" & rs("stok") & "' from var_gudang"
            conn.Execute "insert into hpp (kode_gudang,kode_tinta,hpp)  select gudang,'" & rs("kodebarang") & "','" & rs("hargapokok") & "' from var_gudang"
        Else
            updateitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")
        End If
        rs2.Close
    End If
    rs.MoveNext
Wend
rs.Close
con.Close
DropConnection
Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub

Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
 If KeyCode = vbKeyF3 Then cmdSearchPerkiraan_Click
End Sub

Private Sub txtIsi_GotFocus()
txtIsi.SelStart = 0
txtIsi.SelLength = Len(txtIsi)
End Sub
