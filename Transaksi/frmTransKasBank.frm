VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransKasBank 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Kas & Bank"
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9585
   DrawStyle       =   1  'Dash
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   9585
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting (F5)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4245
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   7845
      Width           =   1230
   End
   Begin VB.TextBox txtAccKasBank 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1605
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   990
      Width           =   1605
   End
   Begin VB.TextBox txtNamaKasBank 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3255
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   990
      Width           =   4350
   End
   Begin VB.CommandButton cmdSearchKas 
      Caption         =   "F4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7665
      TabIndex        =   32
      Top             =   990
      Width           =   375
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   25
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5955
      TabIndex        =   24
      Top             =   510
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   135
      TabIndex        =   17
      Top             =   2190
      Width           =   6990
      Begin VB.TextBox txtKeteranganDetail 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   5
         Top             =   1080
         Width           =   4695
      End
      Begin VB.TextBox txtNilai 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   4
         Top             =   675
         Width           =   1140
      End
      Begin VB.CommandButton cmdSearchAcc 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2835
         TabIndex        =   18
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2745
         TabIndex        =   8
         Top             =   1485
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1635
         TabIndex        =   7
         Top             =   1485
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   225
         TabIndex        =   6
         Top             =   1485
         Width           =   1350
      End
      Begin VB.TextBox txtAcc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   3
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         Caption         =   "Keterangan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   180
         TabIndex        =   36
         Top             =   1125
         Width           =   1050
      End
      Begin VB.Label LblNamaAcc 
         BackColor       =   &H8000000C&
         Caption         =   "Nama Account"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   435
         Left            =   3285
         TabIndex        =   19
         Top             =   315
         Width           =   3525
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         Caption         =   "Nilai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   31
         Top             =   720
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4185
         TabIndex        =   22
         Top             =   1575
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   21
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Account"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   20
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   495
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7845
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   16
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1605
      TabIndex        =   2
      Top             =   1395
      Width           =   3885
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6945
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   7845
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2565
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7845
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1605
      TabIndex        =   0
      Top             =   585
      Width           =   2475
      _ExtentX        =   4366
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   104136707
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2850
      Left            =   135
      TabIndex        =   23
      Top             =   4260
      Width           =   9330
      _ExtentX        =   16457
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   5
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8460
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   34
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label lblKasBank 
      BackStyle       =   0  'Transparent
      Caption         =   "Acc. Kas :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   33
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5625
      TabIndex        =   30
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6975
      TabIndex        =   29
      Top             =   225
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7155
      TabIndex        =   28
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6180
      TabIndex        =   27
      Top             =   7230
      Width           =   2115
   End
   Begin VB.Label Label17 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5265
      TabIndex        =   26
      Top             =   7230
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   15
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   14
      Top             =   1425
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   13
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransKasBank"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryCOA As String = "select * from ms_coa"

Dim mode As Byte
Dim totalNilai As Long

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String
Public Tipe As String

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 no_transaksi from t_kasbankh where substring(no_transaksi,3,2)='" & Format(DTPicker1, "yy") & "' and substring(no_transaksi,5,2)='" & Format(DTPicker1, "MM") & "' and tipe='" & Tipe & "' order by no_transaksi desc", conn
    If Not rs.EOF Then
        No = Tipe & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = Tipe & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub



Private Sub chkNumbering_Click()
    If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub



Private Sub cmdClear_Click()
    txtAcc.text = ""
    debet = True
    LblNamaAcc = ""
    txtNilai.text = "0"
    txtKeteranganDetail.text = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalNilai = totalNilai - (flxGrid.TextMatrix(row, 3))
    lblTotal = Format(totalNilai, "#,##0")
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtAcc.text <> "" And LblNamaAcc <> "" Then
        
'        For i = 1 To flxGrid.Rows - 1
'            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtAcc.text) Then row = i
'        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = txtAcc.text
        flxGrid.TextMatrix(row, 2) = LblNamaAcc
        
        If flxGrid.TextMatrix(row, 3) <> "" Then totalNilai = totalNilai - (flxGrid.TextMatrix(row, 3))
        flxGrid.TextMatrix(row, 3) = txtNilai
        flxGrid.TextMatrix(row, 4) = txtKeteranganDetail
        totalNilai = totalNilai + (flxGrid.TextMatrix(row, 3))
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 3
        
        lblTotal = Format(totalNilai, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtAcc.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub cmdPosting_Click()
    Call Posting_KasBank(lblNoTrans, conn)
    reset_form
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = queryCOA
    frmSearch.nmform = "frmTransKasBank"
    frmSearch.nmctrl = "txtAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_Acc Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select no_transaksi,Tanggal,Keterangan from t_kasbankh where tipe='" & Tipe & "' and status_posting='0'"
    frmSearch.nmform = "frmAddjurnal"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "kasBankH"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 0
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKas_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa"
    frmSearch.nmform = "frmSetting"
    frmSearch.nmctrl = "txtAccKasBank"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
'    frmSearch.txtSearch.text = txtKodeCustomer.text
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_kas"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtKeterangan.SetFocus
End Sub

Public Sub cari_kas()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_coa where kode_acc='" & txtAccKasBank.text & "'", conn
    If Not rs.EOF Then
        txtNamaKasBank = rs(1)
    Else
        txtNamaKasBank = ""
    End If
    rs.Close
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer

On Error GoTo err
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan detail terlebih dahulu"
        txtAcc.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
'        NoJurnal = Nomor_Jurnal(DTPicker1)
    Else
        
            rs.Open "select no_jurnal from t_jurnalh where no_transaksi='" & lblNoTrans & "'", conn
            If Not rs.EOF Then
                NoJurnal = rs("no_jurnal")
            End If
            rs.Close
            
            conn.Execute "delete from t_kasbankh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_kasbankd where no_transaksi='" & lblNoTrans & "'"
            
            conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
            
            
'            conn.Execute "delete from jurnal where no_trans='" & lblNoTrans & "'"
        
    End If
    If chkNumbering = "0" Then lblNoTrans = txtID.text
    
    
    add_dataheader
'    add_jurnalheader
    
    For row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(row, 1) <> "" Then
            add_datadetail (row)
'            add_jurnaldetail (row)
        End If
    Next
    
    
    
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub


Public Sub reset_form()
    lblNoTrans = "-"


    txtKeterangan.text = ""
    totalNilai = 0

    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_kasbankh"
    fields(0) = "no_transaksi"
    fields(1) = "tanggal"
    fields(2) = "acc_kasbank"
    fields(3) = "keterangan"
    fields(4) = "total"
    fields(5) = "tipe"
    fields(6) = "userid"
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtAccKasBank.text
    nilai(3) = txtKeterangan.text
    nilai(4) = Format(totalNilai, "###0")
    nilai(5) = Tipe
    nilai(6) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "t_kasbankd"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "nilai"
    fields(3) = "no_urut"
    fields(4) = "keterangan"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = row
    nilai(4) = flxGrid.TextMatrix(row, 4)
    
    tambah_data table_name, fields, nilai
End Sub




Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim StatusPosting As String
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.*,m.nama as nama_acc from t_kasbankh t " & _
            "inner join ms_coa m on m.kode_acc=t.acc_kasbank " & _
            "where no_transaksi='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
        
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtAccKasBank.text = rs("acc_kasbank")
        txtNamaKasBank.text = rs("nama_acc")
        StatusPosting = rs("status_posting")
        totalNilai = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.kode_acc,m.Nama,d.nilai,d.keterangan from t_kasbankd d inner join ms_coa m on d.kode_acc=m.kode_acc where d.no_transaksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
'                flxGrid.TextMatrix(row, 4) = rs(3)
                
                row = row + 1
                totalNilai = totalNilai + rs(2)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(totalNilai, "#,##0")
        
        If StatusPosting = "1" Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
        End If

        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub Command1_Click()
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then

    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtAcc.text = flxGrid.TextMatrix(flxGrid.row, 1)
            cek_Acc
            txtNilai.text = flxGrid.TextMatrix(flxGrid.row, 3)
            txtKeteranganDetail.text = flxGrid.TextMatrix(flxGrid.row, 4)
            txtNilai.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchAcc_Click
    If KeyCode = vbKeyF4 Then cmdSearchKas_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtAcc.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    LblNamaAcc = ""
    loadcombo
    reset_form
    total = 0
    DTPicker1 = Now
    
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    'End If
    'rs.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1500
    flxGrid.ColWidth(2) = 3100
    flxGrid.ColWidth(3) = 1100
    flxGrid.ColWidth(4) = 2600
    
    flxGrid.TextMatrix(0, 1) = "Kode Acc"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Nilai"
    flxGrid.TextMatrix(0, 4) = "Keterangan"
    


End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_Acc() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where kode_acc='" & txtAcc & "' ", conn
    If Not rs.EOF Then
        LblNamaAcc = rs(1)
        cek_Acc = True
        'If rs(4) = "D" Then debet = True Else debet = False
    Else
        LblNamaAcc = ""
        lblKode = ""
        
        cek_Acc = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtAcc_GotFocus()
    txtAcc.SelStart = 0
    txtAcc.SelLength = Len(txtAcc.text)
'    foc = 1
End Sub

Private Sub txtAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
'        If txtAcc.text = "" Then
'            flxGrid_Click
'        Else
        If txtAcc.text = "" Then KeyCode = 0
        
        If txtAcc.text <> "" Then
            If Not cek_Acc Then
                MsgBox "Kode yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    End If
End Sub


Private Sub txtNilai_GotFocus()
    txtNilai.SelStart = 0
    txtNilai.SelLength = Len(txtNilai.text)
End Sub

Private Sub txtNilai_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtNilai_LostFocus()
    If Not IsNumeric(txtNilai.text) Then txtNilai.text = "1"
End Sub

Private Sub txtKeterangan_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtAcc.SetFocus
End Sub

