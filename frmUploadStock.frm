VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmUploadStock 
   Caption         =   "Form Upload"
   ClientHeight    =   1545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11535
   LinkTopic       =   "Form1"
   ScaleHeight     =   1545
   ScaleWidth      =   11535
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Upload"
      Height          =   420
      Left            =   135
      TabIndex        =   4
      Top             =   90
      Width           =   1905
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   420
      Left            =   135
      TabIndex        =   2
      Top             =   990
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   741
      _Version        =   393216
      Appearance      =   1
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   330
      Left            =   135
      TabIndex        =   0
      Top             =   2610
      Width           =   11220
      ExtentX         =   19791
      ExtentY         =   582
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin VB.Label lblProgress 
      Caption         =   " "
      Height          =   240
      Left            =   10395
      TabIndex        =   3
      Top             =   675
      Width           =   870
   End
   Begin VB.Label lblMessage 
      Caption         =   " "
      Height          =   285
      Left            =   180
      TabIndex        =   1
      Top             =   630
      Width           =   2805
   End
End
Attribute VB_Name = "frmUploadStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim doc As HTMLDocument

Private Sub Command1_Click()
URL = "http://www.3pm-solution.com/buchi/aplot.htm"
WebBrowser1.Navigate URL

End Sub

Private Sub WebBrowser1_DocumentComplete(ByVal pDisp As Object, URL As Variant)
Dim i As Integer
Dim Count As Long
Dim insert As String
Dim rs1 As New ADODB.Recordset
     lblMessage = "Loading Data"
     lblProgress = "0 %"
     
     If InStr(1, URL, "aplot") > 0 And WebBrowser1.LocationName = "Form" Then
        Set doc = WebBrowser1.Document
        For q = 0 To doc.Forms(Form).Length - 1
        If doc.Forms(Form)(q).Name = "sql" Then
        conn.Open strcon
        insert = ""
        Count = 0
        rs.Open "select m.[kode barang],m.[nama barang],stock from ms_bahan m inner join stock s on m.[kode barang]=s.[kode barang] where flag='1'", conn, adOpenForwardOnly, adLockReadOnly, -1
        rs1.Open "select count(*) from ms_bahan m inner join stock s on m.[kode barang]=s.[kode barang] where flag='1'", conn, adOpenForwardOnly, adLockReadOnly, -1
        ProgressBar1.Max = rs1(0)
        insert = ""
        While Not rs.EOF
        
            insert = insert & "insert into stock (kodebarang,namabarang,stock) values ('" & rs(0) & "','" & Replace(rs(1), "'", "''") & "','" & rs(2) & "');"
            Count = Count + 1
            lblProgress = CInt((Count / rs1(0)) * 100) & " %"
            If ((Count Mod 100) = 0) Then
            ProgressBar1.value = Count
            DoEvents
            End If
        rs.MoveNext
        Wend
        rs.Close
        rs1.Close
        conn.Close
        doc.Forms(Form)(q).value = insert
        Exit For
        End If
        Next q
        MsgBox "Load Complete"
        lblMessage = "Saving"
        doc.Forms(Form).submit
    Else
        lblMessage = WebBrowser1.LocationName
    End If
End Sub
Public Sub SetInputField(doc As HTMLDocument, Form As Integer, Name As String, value As String)
'doc = HTMLDocument, can be retrieved from webbrowser --> webbrowser.document
'Form = number of the form (if only one form in the doc --> Form = 0)
'Name = Name of the field you would like to fill
'Value = The new value for the input field called name
'PRE: Legal parameters entered
'POST: Input field with name Name on form Form in document doc will be filled with Value

For q = 0 To doc.Forms(Form).Length - 1
If doc.Forms(Form)(q).Name = Name Then
doc.Forms(Form)(q).value = value
Exit For
End If
Next q
End Sub
Public Function GetInputField(doc As HTMLDocument, Form As Integer, Name As String) As String
For q = 0 To doc.Forms(Form).Length - 1
If doc.Forms(Form)(q).Name = Name Then
GetInputField = doc.Forms(from)(q).value
Exit For
End If
Next q
End Function
Private Sub WebBrowser1_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
1     On Local Error Resume Next
2     ProgressBar1.Min = 0
3     ProgressBar1.Max = ProgressMax + 1
4     ProgressBar1.value = Progress
lblProgress = CInt((Progress / ProgressMax) * 100) & " %"

End Sub
