VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransKoreksiStockPlat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Koreksi Stock Plat"
   ClientHeight    =   7170
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7770
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   7770
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3930
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   6540
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   7590
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   6540
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1425
      MaxLength       =   80
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   1005
      Width           =   5910
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   225
      TabIndex        =   14
      Top             =   1620
      Width           =   7395
      Begin VB.CommandButton cmdSearchBarang 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3540
         TabIndex        =   25
         Top             =   285
         Width           =   375
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   3
         Top             =   720
         Width           =   1140
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3060
         TabIndex        =   10
         Top             =   1305
         Width           =   1350
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1695
         TabIndex        =   9
         Top             =   1305
         Width           =   1350
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   330
         TabIndex        =   4
         Top             =   1305
         Width           =   1350
      End
      Begin VB.TextBox txtKode 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   2
         Top             =   270
         Width           =   1980
      End
      Begin VB.Label lblStock 
         BackColor       =   &H8000000C&
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   5085
         TabIndex        =   27
         Top             =   1170
         Width           =   2160
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H8000000C&
         Caption         =   "Satuan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   2730
         TabIndex        =   26
         Top             =   795
         Width           =   2160
      End
      Begin VB.Label Label6 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   24
         Top             =   780
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4905
         TabIndex        =   18
         Top             =   1170
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   17
         Top             =   1545
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   16
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label LblNamaBarang 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   3990
         TabIndex        =   15
         Top             =   330
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3600
      TabIndex        =   13
      Top             =   165
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5190
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   6540
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1440
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6540
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   1440
      TabIndex        =   0
      Top             =   585
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   20381699
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2850
      Left            =   180
      TabIndex        =   19
      Top             =   3555
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   4
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8925
      Top             =   210
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9555
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label8 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   23
      Top             =   1035
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor        :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   22
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7650
      TabIndex        =   21
      Top             =   5760
      Visible         =   0   'False
      Width           =   2400
   End
   Begin VB.Label label36 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6765
      TabIndex        =   20
      Top             =   5760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal      :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   12
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      AutoSize        =   -1  'True
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1470
      TabIndex        =   11
      Top             =   150
      Width           =   1995
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransKoreksiStockPlat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mode As Byte
Dim total As Long
Dim totalHpp As Double

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String

Private Sub cmdClear_Click()
    txtKode.text = ""
    LblNamaBarang = ""

    txtQty.text = "0"

    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    If flxGrid.Rows <= 2 Then Exit Sub
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKode.text <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKode.text) Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = txtKode.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        
        
        If flxGrid.TextMatrix(row, 1) = txtKode.text Then
             flxGrid.TextMatrix(row, 3) = txtQty.text
        Else
            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 3
        
        If row > 3 Then
            flxGrid.TopRow = row - 3
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtKode.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub cmdPosting_Click()
    If simpan Then
        postingKoreksiPlat lblNoTrans, True
        reset_form
    End If
End Sub

Private Sub cmdPrint_Click()
'    cetaknota lblNoTrans
End Sub



Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSearchBarang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_plat "
    frmSearch.nmform = "frmTransKoreksiStockPlat"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_plat"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_barang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtQty.SetFocus
End Sub

Public Function cari_barang() As Boolean
If txtKode = "" And cari_barang = False Then Exit Function
cari_barang = False
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select kode_plat,nama_plat from ms_plat where kode_plat='" & txtKode.text & "'", conn
    If Not rs.EOF Then
    cari_barang = True
        LblNamaBarang = rs(1)
        txtQty.text = 1
    Else
        LblNamaBarang = ""
        txtQty.text = 0

    End If
    rs.Close
    
    conn.Close
End Function


Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select t.nomer_koreksiplat,t.tanggal,t.keterangan from t_stockkoreksiplath t where t.status_posting=0"
    frmSearch.nmform = "frmTransStockOpname"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "koreksiStockPlat"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err

simpan = False

    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan item detail terlebih dahulu"
        txtKeterangan.SetFocus
        Exit Function
    End If
    
    conn.Open strcon
    
    conn.BeginTrans
    
    i = 1
    If lblNoTrans = "-" Then
       lblNoTrans = newid("t_stockkoreksiplath", "nomer_koreksiplat", DTPicker1, "KRP")
    Else
        conn.Execute "delete from t_stockkoreksiplath where nomer_koreksiplat='" & lblNoTrans & "'"
        conn.Execute "delete from t_stockkoreksiplatd where nomer_koreksiplat='" & lblNoTrans & "'"
    End If
    
    add_dataheader
    
    
    totalHpp = 0
    For row = 1 To flxGrid.Rows - 2
            add_datadetail (row)
    Next row
    
    conn.CommitTrans
    DropConnection
    simpan = True
    MySendKeys "{tab}"
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    MsgBox err.Description
End Function
Public Sub reset_form()
    lblNoTrans = "-"
    lblTotal = "0"
    txtKeterangan.text = ""
    total = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    txtQty = 0
    cmdPrint.Visible = False
'    cmdPosting.Enabled = False
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(4)
    ReDim nilai(4)
    table_name = "t_stockkoreksiplath"
    fields(0) = "nomer_koreksiplat"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "userid"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim selisih As Double
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "t_stockkoreksiplatd"
    fields(0) = "nomer_koreksiplat"
    fields(1) = "kode_plat"
    fields(2) = "qty"
    fields(3) = "no_urut"
    fields(4) = "hpp"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = row
    
    nilai(4) = Replace(Format(GetHPPPlatVia(nilai(1), conn), "###0.##"), ",", ".")
    tambah_data table_name, fields, nilai
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_stockkoreksiplath t " & _
            "where t.nomer_koreksiplat='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.kode_plat,b.nama_plat,d.qty " & _
                "from t_stockkoreksiplatd d inner join ms_plat b on b.kode_plat=d.kode_plat " & _
                "where d.nomer_koreksiplat='" & lblNoTrans & "' order by d.no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
     
                flxGrid.TextMatrix(row, 3) = rs(2) 'qty
                row = row + 1
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        cmdPosting.Enabled = True
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKode.SetFocus
End Sub

Private Sub cmdSimpan_Click()
     If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKode.text = flxGrid.TextMatrix(flxGrid.row, 1)
        LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
    Else
    Exit Sub
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKode.text = flxGrid.TextMatrix(flxGrid.row, 1)
            LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
       
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)

        End If
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    
    LblNamaBarang = ""
    lblSatuan = ""
    lblStock = ""

    reset_form
    total = 0
   
    lblGudang = gudang
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1200
    flxGrid.ColWidth(2) = 4000
    flxGrid.ColWidth(3) = 800 ' qty
     
    flxGrid.TextMatrix(0, 1) = "Kode Plat"
    flxGrid.TextMatrix(0, 2) = "Nama Plat"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 3) = "Qty"

    
    
End Sub

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtKeterangan_GotFocus()
    txtKeterangan.SelStart = 0
    txtKeterangan.SelLength = Len(txtKeterangan.text)
    foc = 1
End Sub



Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
'If txtKode <> "" Then cari_barang
End Sub

Private Sub txtKode_LostFocus()
'    If Not cari_barang And txtKode <> "" Then txtKode.SetFocus
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub



Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub
