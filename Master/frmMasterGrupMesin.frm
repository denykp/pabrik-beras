VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMasterGroupMesin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Grup Mesin"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7695
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   7695
   Begin VB.TextBox txtGrup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5850
      TabIndex        =   5
      Top             =   630
      Width           =   1725
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5040
      Picture         =   "frmMasterGrupMesin.frx":0000
      TabIndex        =   2
      Top             =   3015
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "&New"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5040
      Picture         =   "frmMasterGrupMesin.frx":0102
      TabIndex        =   1
      Top             =   2475
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5040
      Picture         =   "frmMasterGrupMesin.frx":0204
      TabIndex        =   0
      Top             =   1935
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3840
      Left            =   225
      TabIndex        =   9
      Top             =   225
      Width           =   4380
      _ExtentX        =   7726
      _ExtentY        =   6773
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   4
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblKey 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5895
      TabIndex        =   8
      Top             =   675
      Width           =   1005
   End
   Begin VB.Label lblKetParent 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5850
      TabIndex        =   7
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label lblParent 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5850
      TabIndex        =   6
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label Label2 
      Caption         =   "Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4770
      TabIndex        =   4
      Top             =   675
      Width           =   1005
   End
   Begin VB.Label Label1 
      Caption         =   "Parent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4770
      TabIndex        =   3
      Top             =   270
      Width           =   1005
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterGroupMesin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim uncheck As Boolean


Private Sub cmdHapus_Click()
    
End Sub

Private Sub cmdKeluar_Click()
Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [group] from var_usergroup" ' order by satuan"
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.nmform = "frmMasteruserGroup"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "varUserGroup"
    frmSearch.col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.top = 800
    frmSearch.Left = 70
    frmSearch.Show vbModal
    
End Sub

Private Sub reset_form()
On Error Resume Next
    loadgrup ""
    tvwMain.SetFocus
    lblParent = tvwMain.SelectedItem.key
    lblKetParent = tvwMain.SelectedItem.text
    txtGrup = ""
End Sub


Private Sub cmdNew_Click()
    lblKetParent = tvwMain.SelectedItem.text
    lblParent = tvwMain.SelectedItem.key
    lblKey = ""
    txtGrup = ""
End Sub

Private Sub cmdSimpan_Click()
    conn.Open
    conn.Execute "insert into ms_grupmesin (parent,grup) values ('" & Mid(lblParent, 2, Len(lblParent)) & "','" & txtGrup.text & "')"
    conn.Close
    tvwMain.Nodes.Clear
    loadgrup ""
    reset_form
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
loadgrup ""
'lblParent = tvwMain.Nodes.Item(0).key
'lblKetParent = tvwMain.Nodes.Item(0).text
'
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub tvwMain_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        deletegrup Mid(tvwMain.SelectedItem.key, 2, Len(tvwMain.SelectedItem.key))
        deletekey Mid(tvwMain.SelectedItem.key, 2, Len(tvwMain.SelectedItem.key))
        reset_form
    End If
End Sub
Private Sub deletegrup(key As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select * from ms_grupmesin where parent='" & key & "'", conn
    While Not rs.EOF
        deletegrup (rs!urut)
        conn.Execute "delete from ms_grupmesin where urut='" & rs!urut & "'"
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub
Private Sub deletekey(key As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.Execute "delete from ms_grupmesin where urut='" & key & "'"
    conn.Close
End Sub
Private Sub tvwMain_NodeCheck(ByVal Node As MSComctlLib.Node)
'Dim nod As Node
'    Node.Checked = True
'    uncheck = False
'    Set nod = Node.Child
'    For i = 1 To Node.Children
'        nod.Checked = True
'        Set nod = nod.Next
'    Next
'    Set xnode = Node
'    uncheck = True
'

End Sub



Private Sub loadgrup(parent As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim nodex As Node
conn.Open strcon
rs.Open "select * from ms_grupmesin where parent='" & parent & "' order by urut", conn
While Not rs.EOF
    Set nodex = tvwMain.Nodes.Add(IIf(rs("parent") = "", Null, "a" & rs("parent").value), IIf(rs("parent") = "", Null, tvwChild), "a" & rs("urut"), rs("grup"))
    nodex.Expanded = True
    loadgrup (rs!urut)
    rs.MoveNext
Wend
rs.Close
conn.Close
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    If InStr(1, Node.FullPath, "\") Then
        lblParent = Node.parent.key
        lblKetParent = Node.parent.text
    Else
        lblParent = ""
        lblKetParent = ""
    End If
    lblKey = Node.key
    txtGrup.text = Node.text
End Sub
