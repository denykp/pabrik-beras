VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmAddReturBeli 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Retur Pembelian Kertas"
   ClientHeight    =   8025
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9780
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   9780
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1590
      Left            =   135
      TabIndex        =   26
      Top             =   1530
      Width           =   6990
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   2
         Top             =   225
         Width           =   1770
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   3
         Top             =   600
         Width           =   1140
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   255
         TabIndex        =   4
         Top             =   1080
         Width           =   1350
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   5
         Top             =   1080
         Width           =   1050
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2805
         TabIndex        =   6
         Top             =   1080
         Width           =   1050
      End
      Begin VB.CommandButton cmdSearchBrg 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3135
         Picture         =   "frmAddReturBeli.frx":0000
         TabIndex        =   27
         Top             =   225
         Width           =   375
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   330
         Left            =   1260
         TabIndex        =   34
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3240
         TabIndex        =   33
         Top             =   270
         Width           =   3525
      End
      Begin VB.Label Label8 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   32
         Top             =   660
         Width           =   600
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   31
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   30
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   270
         TabIndex        =   29
         Top             =   1260
         Width           =   870
      End
      Begin VB.Label lblStock 
         BackStyle       =   0  'Transparent
         Height          =   330
         Left            =   2520
         TabIndex        =   28
         Top             =   675
         Visible         =   0   'False
         Width           =   870
      End
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3570
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0102
      TabIndex        =   25
      Top             =   660
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtNoNota 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1710
      TabIndex        =   0
      Top             =   675
      Width           =   1800
   End
   Begin VB.ComboBox cmbTipeRetur 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmAddReturBeli.frx":0204
      Left            =   7785
      List            =   "frmAddReturBeli.frx":020E
      Style           =   2  'Dropdown List
      TabIndex        =   22
      Top             =   1125
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0234
      TabIndex        =   19
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1710
      TabIndex        =   1
      Top             =   1035
      Width           =   4710
   End
   Begin VB.CommandButton cmdApprove 
      Caption         =   "&Approve"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4230
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0336
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   7200
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5805
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0438
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7200
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2700
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":053A
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   7200
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   5130
      TabIndex        =   11
      Top             =   180
      Width           =   2745
      _ExtentX        =   4842
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   60358659
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   3300
      Left            =   135
      TabIndex        =   7
      Top             =   3240
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   5821
      _Version        =   393216
      Cols            =   5
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label17 
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6165
      TabIndex        =   38
      Top             =   6660
      Width           =   735
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7110
      TabIndex        =   37
      Top             =   6660
      Width           =   1140
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4635
      TabIndex        =   36
      Top             =   6660
      Width           =   1140
   End
   Begin VB.Label Label16 
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3690
      TabIndex        =   35
      Top             =   6660
      Width           =   735
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "No Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   24
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   23
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7560
      TabIndex        =   21
      Top             =   1215
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   20
      Top             =   1215
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4410
      TabIndex        =   18
      Top             =   6030
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5490
      TabIndex        =   17
      Top             =   6030
      Width           =   1320
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4320
      TabIndex        =   16
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4995
      TabIndex        =   15
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   14
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   13
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   135
      TabIndex        =   12
      Top             =   135
      Width           =   2040
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddReturBeli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Const querybrg As String = "select [kode],[nama barang], Satuan, PriceList,Harga1,harga2,h_eceran from ms_bahan where flag='1'"
'Const queryBeli As String = "Select ID,Tanggal,kode_supplier from t_pembelianh"
'Const queryRetur As String = "Select ID,Tanggal,No_Nota,Gudang,Keterangan from t_returbeliH"
'Public Beli As Boolean
'Dim qty As Long
'Dim totalQty As Long
'Dim NoTrans2 As String
'Dim mode As Byte
'Dim detail As Boolean
'Dim currentRow As Integer
'Dim harga_beli As Currency
'Dim totalHpp As Double
'Dim totalRp As Double
'Dim NoJurnal As String
'
'
'Private Sub nomor_baru()
'Dim No As String
'Dim conn As New ADODB.Connection
'Dim rs As New ADODB.Recordset
'    conn.Open strcon
'    rs.Open "select top 1 ID from T_returbeliH where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
'    If Not rs.EOF Then
'        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
'    Else
'        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
'    End If
'    rs.Close
'    lblNoTrans = No
'    conn.Close
'End Sub
'
'Private Sub cmdKeluar_Click()
'    Unload Me
'End Sub
'
'Private Sub cmdSearch_Click()
'    frmSearch.connstr = strcon
'    frmSearch.query = queryBeli
'    frmSearch.nmform = "frmAddReturBeli"
'    frmSearch.nmctrl = "txtNoNota"
'    frmSearch.col = 0
'    frmSearch.Index = -1
'    frmSearch.proc = "cek_nota"
'    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbSort.ListIndex = 1
'    frmSearch.requery
''    frmSearch.txtSearch.text = txtNoNota.text
'    Set frmSearch.frm = Me
'    frmSearch.Show vbModal
'End Sub
'
'
'Private Sub cmdSearchBrg_Click()
'    frmSearch.query = querybrg
'    frmSearch.nmform = "frmAddReturBeli"
'    frmSearch.nmctrl = "txtKdBrg"
'    frmSearch.connstr = strcon
'    frmSearch.col = 0
'    frmSearch.Index = -1
'    frmSearch.proc = "cek_kodebarang1"
'
'    frmSearch.loadgrid frmSearch.query
''    frmSearch.cmbKey.ListIndex = 2
''    frmSearch.requery
'    Set frmSearch.frm = Me
'    frmSearch.Show vbModal
'End Sub
'
'Private Sub cmdSearchID_Click()
'    frmSearch.connstr = strcon
'    frmSearch.query = queryRetur
'    frmSearch.nmform = "frmAddReturBeli"
'    frmSearch.nmctrl = "lblNoTrans"
'
'    frmSearch.col = 0
'    frmSearch.Index = -1
'    frmSearch.proc = "cek_notrans"
'
'    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbSort.ListIndex = 1
'    frmSearch.requery
'    Set frmSearch.frm = Me
'
'    frmSearch.Show vbModal
'End Sub
'Public Sub cek_notrans()
'Dim rs As New ADODB.Recordset
'Dim row As Integer
''    reset_form
'    cmdSimpan.Enabled = True
'    cmdApprove.Enabled = False
'    conn.ConnectionString = strcon
'    conn.Open
'    rs.Open "select * from t_returbelih where id='" & lblNoTrans & "' and gudang='" & gudang & "'", conn
'    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
'        DTPicker1 = rs("tanggal")
'        txtKeterangan.text = rs("keterangan")
'        txtNoNota.text = rs("no_nota")
'        If rs("status") = 1 Then
'            cmdSimpan.Enabled = False
'        Else
'            cmdSimpan.Enabled = True
'        End If
'        rs.Close
'        qty = 0
'        flxGrid.Rows = 1
'        flxGrid.Rows = 2
'        row = 1
'        rs.Open "select distinct a.[kode],b.[nama barang],a.qty,a.harga_Beli from (t_returbelid a inner join ms_bahan b on a.[kode]=b.[kode])  where a.id='" & lblNoTrans & "'", conn
'        While Not rs.EOF
'            flxGrid.TextMatrix(row, 1) = rs(0)
'            flxGrid.TextMatrix(row, 2) = rs(1)
'            flxGrid.TextMatrix(row, 3) = rs(2)
'            flxGrid.TextMatrix(row, 4) = rs(3)
'            qty = qty + rs(2)
'            flxGrid.Rows = flxGrid.Rows + 1
'            row = row + 1
'            rs.MoveNext
'        Wend
'        lblQty = Format(qty, "#,##0")
'    End If
'    rs.Close
'    flxGrid.row = 1
'    conn.Close
'
'End Sub
'
'Private Sub cmdOK_Click()
'Dim i As Integer
'Dim row As Integer
'    row = 0
'    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
'
'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = lblKode Then row = i
'        Next
'        If row = 0 Then
'            row = flxGrid.Rows - 1
'            flxGrid.Rows = flxGrid.Rows + 1
'        End If
'        flxGrid.TextMatrix(row, 1) = lblKode
'        flxGrid.TextMatrix(row, 2) = LblNamaBarang
'        If flxGrid.TextMatrix(row, 3) = "" Then
'            flxGrid.TextMatrix(row, 3) = txtQty
'        Else
'            totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
'
'            If mode = 1 Then
'                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
'            ElseIf mode = 2 Then
'                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
'            End If
'        End If
'        flxGrid.TextMatrix(row, 4) = harga_beli
'        totalQty = totalQty + flxGrid.TextMatrix(row, 3)
'        Label9 = Format(totalQty, "#,##0")
'        'flxGrid.TextMatrix(row, 7) = lblKode
'        If row > 8 Then
'            flxGrid.TopRow = row - 7
'        Else
'            flxGrid.TopRow = 1
'        End If
'        flxGrid.row = row
'        flxGrid.col = 0
'        flxGrid.ColSel = 4
'        harga_beli = 0
'        harga_beli = 0
'        txtKdBrg.text = ""
'        lblStock = ""
'        LblNamaBarang = ""
'        txtQty.text = "1"
'        txtKdBrg.SetFocus
'        cmdClear.Enabled = False
'        cmdDelete.Enabled = False
'    End If
'    mode = 1
'    lblItem = flxGrid.Rows - 2
'End Sub
'Private Sub cmdClear_Click()
'    txtKdBrg.text = ""
'    lblStock = ""
'    LblNamaBarang = ""
'    txtQty.text = "1"
'    mode = 1
'    cmdClear.Enabled = False
'    cmdDelete.Enabled = False
'    chkGuling = 0
'    chkKirim = 0
'End Sub
'
'Private Sub cmdDelete_Click()
'Dim row, col As Integer
'    row = flxGrid.row
'    If flxGrid.Rows <= 2 Then Exit Sub
'    totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
'    lblQty = totalQty
'    For row = row To flxGrid.Rows - 2
'        If row = flxGrid.Rows Then
'            For col = 1 To flxGrid.cols - 1
'                flxGrid.TextMatrix(row, col) = ""
'            Next
'            Exit For
'        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
'            For col = 1 To flxGrid.cols - 1
'                flxGrid.TextMatrix(row, col) = ""
'            Next
'        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
'            For col = 1 To flxGrid.cols - 1
'            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
'            Next
'        End If
'    Next
'    flxGrid.Rows = flxGrid.Rows - 1
'    flxGrid.col = 0
'    'flxGrid.ColSel = 3
'    txtKdBrg.text = ""
'    lblStock = ""
'    txtQty.text = "1"
'    LblNamaBarang = ""
'    'txtKdBrg.SetFocus
'    mode = 1
'    cmdClear.Enabled = False
'    cmdDelete.Enabled = False
'
'End Sub
'Private Sub cmdSimpan_Click()
'Dim i As Integer
'Dim id As String
'Dim counter As Integer
'Dim no_urut As Integer
'Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
'
'On Error GoTo err
'
'    If flxGrid.Rows < 3 Or txtNoNota.text = "" Then
'        MsgBox "Silahkan masukkan Nota dan Barang yang akan diretur terlebih dahulu"
'        txtNoNota.SetFocus
'        Exit Sub
'    End If
'    id = lblNoTrans
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.BeginTrans
'    i = 1
'
'    If lblNoTrans = "-" Then
'        nomor_baru
'        NoJurnal = Nomor_Jurnal(DTPicker1)
'
'    Else
'        conn.Execute "delete from t_returbelih where ID='" & lblNoTrans & "'"
'        rs.Open "select * from t_returbelid where id='" & lblNoTrans & "'", conn
'        While Not rs.EOF
'            updatestock gudang, rs("kode"), rs("qty"), conn
'
'        rs.MoveNext
'        Wend
'        rs.Close
'        conn.Execute "delete from t_returbelid where ID='" & lblNoTrans & "'"
'        conn.Execute "delete from hst_hpp where ID='" & lblNoTrans & "'"
'        conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
'        conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
'    End If
'    add_dataheader
'
'    counter = 0
'    Dim J As Integer
'    totalHpp = 0: totalRp = 0
'    For J = 1 To flxGrid.Rows - 2
'        add_datadetail J
'        add_jurnaldetail J
'
'        JumlahLama = getStock2(flxGrid.TextMatrix(J, 1), gudang, strcon)
'
'        HPPLama = GetHPP(flxGrid.TextMatrix(J, 1), strcon)
'
'        jumlah = flxGrid.TextMatrix(J, 3)
'
'        updatestock gudang, flxGrid.TextMatrix(J, 1), flxGrid.TextMatrix(J, 3) * -1, conn
'        conn.Execute "insert into hst_hpp  (jenis,gudang,id,tanggal,kode,stockawal,HPPawal,Qty,Harga,HPP,StockAkhir)  " & _
'                    "values ('Retur Beli','" & gudang & "','" & lblNoTrans & " ','" & Format(Now, "yyyy/MM/dd hh:mm:ss") & "','" & flxGrid.TextMatrix(J, 1) & "'," & JumlahLama & ",0," & -jumlah & ",0," & Replace(HPPLama, ",", ".") & "," & JumlahLama - jumlah & " )"
'
'
'    Next
'
'    add_jurnal
'
'
'    counter = 0
'    conn.CommitTrans
'    i = 0
'    For J = 1 To flxGrid.Rows - 2
'        flxGrid.row = J
'    Next
'    conn.Execute "update t_returbelih set status='1' where id='" & lblNoTrans & "'"
'    DropConnection
'    i = 0
'    MsgBox "Data sudah tersimpan"
'    reset_form
'
'    Call MySendKeys("{tab}")
'    Exit Sub
'err:
'    conn.RollbackTrans
'    DropConnection
'    If id <> lblNoTrans Then lblNoTrans = id
'    MsgBox err.Description
'End Sub
'Private Sub reset_form()
'On Error Resume Next
'    lblNoTrans = "-"
'    lblQty = "0,00"
'    lblItem = "0"
'    Label9 = "0,00"
'    txtKeterangan.text = ""
'    txtNoNota.text = ""
'    DTPicker1 = Now
'    qty = 0
'    cmdApprove.Enabled = False
'
'    txtKdBrg.text = ""
'    txtQty.text = "1"
'    flxGrid.Rows = 1
'    flxGrid.Rows = 2
'    cmdSimpan.Enabled = True
'End Sub
'Private Sub add_dataheader()
'    Dim fields() As String
'    Dim nilai() As String
'    Dim table_name As String
'
'    ReDim fields(6)
'    ReDim nilai(6)
'
'    table_name = "T_returbeliH"
'    fields(0) = "id"
'    fields(1) = "tanggal"
'    fields(2) = "keterangan"
'    fields(3) = "gudang"
'    fields(4) = "no_nota"
'    fields(5) = "pk"
'
'    nilai(0) = lblNoTrans
'    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
'    nilai(2) = txtKeterangan.text
'    nilai(3) = gudang
'    nilai(4) = txtNoNota.text
'    nilai(5) = ""
'    conn.Execute tambah_data2(table_name, fields, nilai)
'End Sub
'Private Sub add_datadetail(row As Integer)
'    Dim fields() As String
'    Dim nilai() As String
'    Dim table_name As String
'
'    ReDim fields(5)
'    ReDim nilai(5)
'
'    table_name = "T_returbeliD"
'    fields(0) = "ID"
'    fields(1) = "[kode]"
'    fields(2) = "qty"
'    fields(3) = "harga_beli"
'    fields(4) = "URUT"
'
'
'    nilai(0) = lblNoTrans
'    nilai(1) = flxGrid.TextMatrix(row, 1)
'    nilai(2) = flxGrid.TextMatrix(row, 3)
'    nilai(3) = flxGrid.TextMatrix(row, 4)
'    nilai(4) = row
'    totalRp = totalRp + (CDbl(nilai(3)) * CDbl(nilai(2)))
'
'
'    conn.Execute tambah_data2(table_name, fields, nilai)
'
'End Sub
'
'Private Sub add_jurnal()
'Dim fields() As String
'    Dim nilai() As String
'    Dim table_name As String
'    Dim Acc_PiutangReturBeli As String
'
'
'    ReDim fields(7)
'    ReDim nilai(7)
'
'    table_name = "t_jurnalh"
'    fields(0) = "no_transaksi"
'    fields(1) = "no_jurnal"
'    fields(2) = "tanggal"
'    fields(3) = "keterangan"
'    fields(4) = "totaldebet"
'    fields(5) = "totalkredit"
'    fields(6) = "tipe"
'
'    nilai(0) = lblNoTrans
'    nilai(1) = NoJurnal
'    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
'    nilai(3) = "Retur Pembelian"
'    nilai(4) = totalRp
'    nilai(5) = totalRp
'    nilai(6) = "RB"
'
'    tambah_data table_name, fields, nilai
'
'
'    ReDim fields(4)
'    ReDim nilai(4)
'
'    Acc_PiutangReturBeli = getAcc("piutang retur beli")
'
'
'    table_name = "t_jurnald"
'    fields(0) = "no_transaksi"
'    fields(1) = "kode_acc"
'    fields(2) = "debet"
'    fields(3) = "urut"
'
'
'    nilai(0) = lblNoTrans
'    nilai(1) = Acc_PiutangReturBeli
'    nilai(2) = totalRp
'    nilai(3) = 1
'
'    tambah_data table_name, fields, nilai
'
'End Sub
'
'Private Sub add_jurnaldetail(row As Integer)
'    Dim fields() As String
'    Dim nilai() As String
'    Dim table_name As String
'    Dim Acc_Persediaan As String
'
'    ReDim fields(4)
'    ReDim nilai(4)
'
'    rs.Open "Select kode_acc from ms_bahan  " & _
'            "where kode='" & flxGrid.TextMatrix(row, 1) & "'", conn
'    If Not rs.EOF Then
'        Acc_Persediaan = rs("kode_acc")
'    End If
'    rs.Close
'
'    table_name = "t_jurnald"
'    fields(0) = "no_transaksi"
'    fields(1) = "kode_acc"
'    fields(2) = "kredit"
'    fields(3) = "urut"
'
'    nilai(0) = lblNoTrans
'    nilai(1) = Acc_Persediaan
'    nilai(2) = flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 4)
'    nilai(3) = 1 + row
'
'    tambah_data table_name, fields, nilai
'End Sub
'
'
'Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyDelete Then
'        cmdDelete_Click
'    ElseIf KeyCode = vbKeyReturn Then
'        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
'        mode = 2
'        cmdClear.Enabled = True
'        cmdDelete.Enabled = True
'        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
'        cek_kodebarang1
'        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
'        txtQty.SetFocus
'        End If
'    End If
'End Sub
'
'
'Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyEscape Then Unload Me
'End Sub
'
'Private Sub Form_KeyPress(KeyAscii As Integer)
'    If KeyAscii = vbKeyReturn Then Call MySendKeys("{tab}")
'End Sub
'
'Private Sub Form_Load()
'    reset_form
'    flxGrid.ColWidth(0) = 300
'    flxGrid.ColWidth(1) = 1100
'    flxGrid.ColWidth(2) = 2800
'    flxGrid.ColWidth(3) = 900
'    flxGrid.ColWidth(4) = 0
'
'
'    flxGrid.TextMatrix(0, 1) = "Kode"
'    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
'    flxGrid.TextMatrix(0, 2) = "Nama Barang"
'    flxGrid.TextMatrix(0, 3) = "Qty"
'    flxGrid.TextMatrix(0, 4) = "Harga Beli"
'End Sub
'
'Private Sub mnuKeluar_Click()
'    Unload Me
'End Sub
'
'Private Sub mnuOpen_Click()
'    cmdSearchID_Click
'End Sub
'
'Private Sub mnuReset_Click()
'    reset_form
'End Sub
'
'Private Sub mnuSimpan_Click()
'    cmdSimpan_Click
'End Sub
'Public Sub cek_nota()
''    DBGrid.RemoveAll
''    DBGrid.FieldSeparator = "#"
'    qty = 0
'
'    conn.Open strcon
'    Dim where As String
'    If lblNoTrans <> "-" Then where = " and id<>'" & lblNoTrans & "'"
'    rs.Open "select ID from t_returbelih where no_nota='" & txtNoNota & "' " & where, conn
'    If Not rs.EOF Then
'        If (MsgBox("Nota tersebut sudah diretur, apakah anda ingin melihat retur tersebut?", vbYesNo) = vbYes) Then
'            lblNoTrans = rs(0)
'            rs.Close
'        End If
'        conn.Close
'        cek_notrans
'        Exit Sub
'    End If
'    rs.Close
''    rs.Open "select d.[kode],m.[nama barang],d.qty,(d.harga-disc-disc_total) as disc,d.hpp from q_Belid d inner join ms_bahan m on d.[kode]=m.[kode] where id='" & txtNoNota.text & "' order by urut", conn
''    While Not rs.EOF
''        flxGrid.TextMatrix(row, 1) = rs(0)
''        flxGrid.TextMatrix(row, 1) = rs(1)
''        flxGrid.TextMatrix(row, 1) = 0
''        qty = qty + 1
''        rs.MoveNext
''    Wend
''    rs.Close
'    conn.Close
'    lblQty = Format(qty, "#,##0")
'End Sub
'Public Function cek_kodebarang1() As Boolean
'Dim kode As String
'
'    conn.ConnectionString = strcon
'    conn.Open
'    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
'    rs.Open "select m.[nama barang],m.[Kode],d.harga from ms_bahan m inner join t_pembeliand d on m.[kode]=d.[kode] where m.[kode]='" & txtKdBrg & "'", conn
'    If Not rs.EOF Then
'        cek_kodebarang1 = True
'        LblNamaBarang = rs(0)
'        harga_beli = rs(2)
'        'lblStock = getStock2(txtKdBrg.text, gudang, strcon)
'        lblKode = rs(1)
'    Else
'        cek_kodebarang1 = False
'        LblNamaBarang = ""
'        'lblStock = "0"
'        harga_beli = 0
'        lblKode = ""
'    End If
'    rs.Close
'    conn.Close
'End Function
'
'Private Sub fokusnota()
'On Error Resume Next
'    txtNoNota.SetFocus
'End Sub
'
'
'Private Sub txtNoNota_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyF3 Then
'        cmdSearch_Click
'    End If
'End Sub
'
'Private Sub txtNoNota_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        cek_nota
'    End If
'End Sub
'
'Private Sub txtNoNota_LostFocus()
'    cek_nota
'End Sub
'Private Sub txtKdBrg_GotFocus()
'    txtKdBrg.SelStart = 0
'    txtKdBrg.SelLength = Len(txtKdBrg.text)
'    foc = 1
'End Sub
'
'Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyF3 Then
'        cmdSearchBrg_Click
'    End If
'    If KeyCode = 13 Then
'        If InStr(1, txtKdBrg.text, "*") > 0 Then
'            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
'            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
'        End If
'        cek_kodebarang1
''        cmdOK_Click
'
''        cari_id
'    End If
'End Sub
'
'Private Sub txtKdBrg_LostFocus()
'On Error Resume Next
'    If InStr(1, txtKdBrg.text, "*") > 0 Then
'        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
'        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
'    End If
'    If Not cek_kodebarang1 And txtKdBrg <> "" Then
'        MsgBox "Kode yang anda masukkan salah"
'        txtKdBrg.SetFocus
'    End If
'    foc = 0
'End Sub
'
'Private Sub txtQty_GotFocus()
'    txtQty.SelStart = 0
'    txtQty.SelLength = Len(txtQty.text)
'End Sub
'
'Private Sub txtQty_KeyPress(KeyAscii As Integer)
'    Angka KeyAscii
'End Sub
