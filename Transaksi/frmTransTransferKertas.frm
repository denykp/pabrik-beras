VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmTransTransferKertas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Transfer Kertas"
   ClientHeight    =   5085
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9735
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5085
   ScaleWidth      =   9735
   Begin VB.CommandButton cmdSearchHistory 
      BackColor       =   &H8000000C&
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3465
      Picture         =   "frmTransTransferKertas.frx":0000
      TabIndex        =   27
      Top             =   2700
      Width           =   420
   End
   Begin VB.TextBox txtNoHistory 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      TabIndex        =   4
      Top             =   2700
      Width           =   1815
   End
   Begin VB.TextBox txtSerialHasil 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   6
      Top             =   3510
      Width           =   3750
   End
   Begin VB.TextBox txtKodehasil 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1590
      TabIndex        =   5
      Top             =   3105
      Width           =   3735
   End
   Begin VB.CommandButton cmdSearchHasil 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5400
      TabIndex        =   24
      Top             =   3150
      Width           =   375
   End
   Begin VB.TextBox txtKode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      TabIndex        =   1
      Top             =   1485
      Width           =   3735
   End
   Begin VB.TextBox txtQty 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      TabIndex        =   3
      Top             =   2310
      Width           =   1140
   End
   Begin VB.CommandButton cmdSearchBarang 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5400
      TabIndex        =   20
      Top             =   1485
      Width           =   375
   End
   Begin VB.ComboBox cmbSerial 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      TabIndex        =   2
      Text            =   "cmbSerial"
      Top             =   1890
      Width           =   2325
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4740
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   4230
      Width           =   1230
   End
   Begin VB.ComboBox cmbGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6120
      Style           =   2  'Dropdown List
      TabIndex        =   18
      Top             =   150
      Width           =   1605
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   4230
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      MaxLength       =   80
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   1005
      Width           =   5910
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   180
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7830
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   14
      Top             =   165
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5970
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   4230
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2250
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   4230
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   1620
      TabIndex        =   17
      Top             =   585
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   109182979
      CurrentDate     =   38927
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   8115
      Top             =   990
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label11 
      Caption         =   "History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   28
      Top             =   2745
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Hasil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   26
      Top             =   3150
      Width           =   1050
   End
   Begin VB.Label Label2 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "No. Serial"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   25
      Top             =   3555
      Width           =   1170
   End
   Begin VB.Label Label14 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   23
      Top             =   1530
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   22
      Top             =   2370
      Width           =   600
   End
   Begin VB.Label Label4 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "No. Serial"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   21
      Top             =   1935
      Width           =   1170
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4620
      TabIndex        =   19
      Top             =   210
      Width           =   780
   End
   Begin VB.Label Label8 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   16
      Top             =   1035
      Width           =   1275
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   15
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   13
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1620
      TabIndex        =   12
      Top             =   180
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransTransferKertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mode As Byte
Dim total As Long
Dim totalHpp As Double

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String

Private Sub cmbGudang_Click()
    cmdSearchBarang.Enabled = True
    load_serial
End Sub


Private Sub cmbSerial_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
On Error GoTo err
    conn.Open strcon
    txtQty = getStockKertas2(txtKode, cmbGudang.text, cmbSerial.text, conn)
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub cmdKeluar_Click()
    Unload Me
End Sub




Private Sub cmdPosting_Click()
    If simpan Then
        postingTransferBahan lblNoTrans, True
        reset_form
    End If
End Sub


Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearchBarang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = searchBarang & " where kategori like '%SHEET IN ROLL BLABAK%'"
    frmSearch.nmform = "frmTransTransferKertas"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub

Public Sub cari_barang()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select kode_bahan,nama_bahan from ms_bahan where kode_bahan='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        
        txtQty.text = 1
        
    Else
        
        txtQty.text = 0
        
'        txtKode.SetFocus
    End If
    rs.Close
    conn.Close
    load_serial
End Sub
Private Sub load_serial()
Dim conn As Object
Dim rs As Object
    Set conn = New ADODB.Connection
    Set rs = New ADODB.Recordset
    conn.Open strcon
    cmbSerial.Clear
    rs.Open "select nomer_serial from stock where kode_bahan='" & txtKode.text & "' and kode_gudang='" & cmbGudang.text & "' ", conn
    While Not rs.EOF
        cmbSerial.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    Set conn = Nothing
    Set rs = Nothing
End Sub

Private Sub cmdSearchHasil_Click()
    frmSearch.connstr = strcon
    frmSearch.query = searchBarang & " where kategori like '%potong%'"
    frmSearch.nmform = "frmTransTransferKertas"
    frmSearch.nmctrl = "txtkodeHasil"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub

Private Sub cmdSearchHistory_Click()
    frmSearch.query = "select * from vw_sisahistory where kode_bahan='" & txtKodeBahan & "'"
    frmSearch.nmform = "frmTransTransferKertas"
    frmSearch.nmctrl = "txtNoHistory"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "noHistory"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select t.nomer_transferbahan,t.tanggal,t.keterangan from t_transferbahan t where t.status_posting=0"
    frmSearch.nmform = "frmTransStockmutasi"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "mutasibahan"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    id = lblNoTrans
    conn.Open strcon
    
    If lblNoTrans = "-" Then lblNoTrans = newid("t_transferbahan", "nomer_transferbahan", DTPicker1, "TRF")
    
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_transferbahan where nomer_transferbahan='" & lblNoTrans & "'"
    
    add_dataheader
  
    conn.CommitTrans
    DropConnection
    simpan = True
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub reset_form()
    lblNoTrans = "-"
    lblTotal = "0"
    txtKeterangan.text = ""
    total = 0
    DTPicker1 = Now
    
    cmdPrint.Visible = False
'    cmdPosting.Enabled = False
    cmdSearchBarang.Enabled = True
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(12)
    ReDim nilai(12)
    table_name = "t_transferbahan"
    fields(0) = "nomer_transferbahan"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "kode_gudang"
    fields(4) = "kode_bahan"
    fields(5) = "nomer_serial"
    fields(6) = "kode_hasil"
    fields(7) = "nomer_serial_hasil"
    fields(8) = "qty"
    fields(9) = "userid"
    fields(10) = "hpp"
    fields(11) = "no_history"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = cmbGudang.text
    nilai(4) = txtKode
    nilai(5) = cmbSerial.text
    nilai(6) = txtKodehasil
    nilai(7) = txtSerialHasil
    nilai(8) = txtQty
    nilai(9) = User
    nilai(10) = GetHPPKertas2(txtKode, cmbSerial.text, cmbGudang.text, conn)
    nilai(11) = txtNoHistory
    tambah_data table_name, fields, nilai
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_transferbahan t " & _
            "where t.nomer_transferbahan ='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        SetComboText (rs!kode_gudang), cmbGudang
        txtNoHistory = rs!no_history
        total = 0
        txtKode = rs!kode_bahan
        load_serial
        SetComboText rs!nomer_serial, cmbSerial
        txtQty = rs!qty
        txtKodehasil = rs!kode_hasil
        txtSerialHasil = rs!nomer_serial_hasil
        If rs!status_posting = "0" Then cmdPosting.Enabled = True
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKode.SetFocus
End Sub

Private Sub cmdSimpan_Click()
 If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Command1_Click()

End Sub


Private Sub Form_Activate()
'    MySendKeys "{tab}"
End Sub



Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then

        KeyAscii = 0
        MySendKeys "{tab}"

    End If
End Sub

Private Sub Form_Load()
    
    
    lblStock = ""
    DTPicker1 = Now
    reset_form
    total = 0
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    
    rs.Open "select * from ms_gudang   order by kode_gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
    
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    
    lblGudang = gudang
    
    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    
End Sub


Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtKeterangan_GotFocus()
    txtKeterangan.SelStart = 0
    txtKeterangan.SelLength = Len(txtKeterangan.text)
    foc = 1
End Sub



Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
End Sub

Private Sub txtKode_LostFocus()
 If txtKode <> "" Then cari_barang
End Sub

Private Sub txtKodehasil_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchHasil_Click
End Sub

Private Sub txtKodehasil_LostFocus()
    txtSerialHasil = txtKodehasil
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

'Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then cmdOk.SetFocus
'End Sub

'Private Sub txtQty_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        txtQty.text = calc(txtQty.text)
'    End If
'End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub
'
'Public Sub cetaknota(no_nota As String)
'On Error GoTo err
'Dim query() As String
'Dim rs() As New ADODB.Recordset
'
'
'
'    conn.Open strcon
'
'    ReDim query(2) As String
'    query(0) = "select * from t_transferbahanD"
'    query(1) = "select * from t_transferbahan"
'    query(2) = "select * from ms_bahan"
'
'
'
'    ReDim rs(UBound(query) - 1)
'    For i = 0 To UBound(query) - 1
'        rs(i).Open query(i), conn, adOpenForwardOnly
'    Next
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\PO Stockmutasi.rpt"
'        For i = 0 To UBound(query) - 1
'            .SetTablePrivateData i, 3, rs(i)
'        Next
'        .Formulas(0) = "NamaPerusahaan='" & GNamaPerusahaan & "'"
'        .Formulas(1) = "AlamatPerusahaan='" & GAlamatPerusahaan & "'"
'        .Formulas(2) = "TeleponPerusahaan='" & GTeleponPerusahaan & "'"
'
'        .SelectionFormula = "{t_transferbahan.nomer_transferbahan}='" & no_nota & "'"
''        .PrinterSelect
'        .ProgressDialog = False
'        .WindowState = crptMaximized
''        .Destination = crptToPrinter
'        .action = 1
'    End With
'    conn.Close
'    Exit Sub
'err:
'MsgBox err.Description
'End Sub

