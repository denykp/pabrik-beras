VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{DEF7CADD-83C0-11D0-A0F1-00A024703500}#7.0#0"; "todg7.ocx"
Object = "{97069F3B-E271-4FF8-95B9-711128C70C93}#1.0#0"; "vbmaniaocx.ocx"
Begin VB.Form RptNeraca 
   BackColor       =   &H00FCCDEF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Neraca"
   ClientHeight    =   7980
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13470
   Icon            =   "RptNeraca.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7980
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      BackColor       =   &H00FCCDEF&
      Height          =   675
      Left            =   30
      TabIndex        =   0
      Top             =   -60
      Width           =   13425
      Begin VB.CommandButton bKeluar 
         BackColor       =   &H00E0E0E0&
         Caption         =   "K&eluar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   11550
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   195
         Width           =   1755
      End
      Begin VB.CommandButton bProses 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Tampilkan Data"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   7950
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   195
         Width           =   1755
      End
      Begin VB.CommandButton bPreview 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Preview"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   9750
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   195
         Width           =   1755
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FCCDEF&
      Height          =   7425
      Left            =   30
      TabIndex        =   1
      Top             =   540
      Width           =   13425
      Begin VB.CommandButton bExport 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xport ke EXCEL"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   10980
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   6720
         Width           =   2025
      End
      Begin vbmaniaOCX.LabelText sTahun 
         Height          =   300
         Left            =   240
         TabIndex        =   8
         Top             =   585
         Width           =   2040
         _ExtentX        =   3598
         _ExtentY        =   529
         BackColor       =   16567791
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Franklin Gothic Medium"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Tahun :"
         Text            =   ""
         CaptionColor    =   -2147483630
         CaptionTab      =   900
      End
      Begin VB.Frame Frame3 
         BackColor       =   &H00FCCDEF&
         Height          =   5595
         Left            =   45
         TabIndex        =   2
         Top             =   915
         Width           =   13335
         Begin TrueOleDBGrid70.TDBGrid DataGrid1 
            Height          =   5370
            Index           =   0
            Left            =   45
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   135
            Width           =   13215
            _ExtentX        =   23310
            _ExtentY        =   9472
            _LayoutType     =   4
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).Caption=   "No"
            Columns(0).DataField=   ""
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).Caption=   "Kode"
            Columns(1).DataField=   ""
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(2)._VlistStyle=   0
            Columns(2)._MaxComboItems=   5
            Columns(2).Caption=   "Nama Perkiraan"
            Columns(2).DataField=   ""
            Columns(2)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(3)._VlistStyle=   0
            Columns(3)._MaxComboItems=   5
            Columns(3).Caption=   "Jumlah"
            Columns(3).DataField=   ""
            Columns(3).NumberFormat=   "###,###,###,###,###,##0.#0"
            Columns(3)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(4)._VlistStyle=   0
            Columns(4)._MaxComboItems=   5
            Columns(4).Caption=   "Kode"
            Columns(4).DataField=   ""
            Columns(4)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(5)._VlistStyle=   0
            Columns(5)._MaxComboItems=   5
            Columns(5).Caption=   "Nama Perkiraan"
            Columns(5).DataField=   ""
            Columns(5)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(6)._VlistStyle=   0
            Columns(6)._MaxComboItems=   5
            Columns(6).Caption=   "Jumlah"
            Columns(6).DataField=   ""
            Columns(6).NumberFormat=   "###,###,###,###,###,##0.#0"
            Columns(6)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(7)._VlistStyle=   0
            Columns(7)._MaxComboItems=   5
            Columns(7).Caption=   " "
            Columns(7).DataField=   ""
            Columns(7)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   8
            Splits(0)._UserFlags=   0
            Splits(0).MarqueeStyle=   3
            Splits(0).AllowRowSizing=   0   'False
            Splits(0).RecordSelectors=   0   'False
            Splits(0).RecordSelectorWidth=   185
            Splits(0)._SavedRecordSelectors=   0   'False
            Splits(0).ScrollBars=   3
            Splits(0).AlternatingRowStyle=   -1  'True
            Splits(0).DividerColor=   12632256
            Splits(0).SpringMode=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=8"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=714"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=635"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0)._ColStyle=516"
            Splits(0)._ColumnProps(6)=   "Column(0).Visible=0"
            Splits(0)._ColumnProps(7)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(8)=   "Column(1).Width=1931"
            Splits(0)._ColumnProps(9)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(10)=   "Column(1)._WidthInPix=1852"
            Splits(0)._ColumnProps(11)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(12)=   "Column(1)._ColStyle=516"
            Splits(0)._ColumnProps(13)=   "Column(1).Order=2"
            Splits(0)._ColumnProps(14)=   "Column(2).Width=5398"
            Splits(0)._ColumnProps(15)=   "Column(2).DividerColor=0"
            Splits(0)._ColumnProps(16)=   "Column(2)._WidthInPix=5318"
            Splits(0)._ColumnProps(17)=   "Column(2)._EditAlways=0"
            Splits(0)._ColumnProps(18)=   "Column(2)._ColStyle=516"
            Splits(0)._ColumnProps(19)=   "Column(2).Order=3"
            Splits(0)._ColumnProps(20)=   "Column(3).Width=3598"
            Splits(0)._ColumnProps(21)=   "Column(3).DividerColor=0"
            Splits(0)._ColumnProps(22)=   "Column(3)._WidthInPix=3519"
            Splits(0)._ColumnProps(23)=   "Column(3)._EditAlways=0"
            Splits(0)._ColumnProps(24)=   "Column(3)._ColStyle=514"
            Splits(0)._ColumnProps(25)=   "Column(3).Order=4"
            Splits(0)._ColumnProps(26)=   "Column(4).Width=1984"
            Splits(0)._ColumnProps(27)=   "Column(4).DividerColor=0"
            Splits(0)._ColumnProps(28)=   "Column(4)._WidthInPix=1905"
            Splits(0)._ColumnProps(29)=   "Column(4)._EditAlways=0"
            Splits(0)._ColumnProps(30)=   "Column(4)._ColStyle=516"
            Splits(0)._ColumnProps(31)=   "Column(4).Order=6"
            Splits(0)._ColumnProps(32)=   "Column(5).Width=4895"
            Splits(0)._ColumnProps(33)=   "Column(5).DividerColor=0"
            Splits(0)._ColumnProps(34)=   "Column(5)._WidthInPix=4815"
            Splits(0)._ColumnProps(35)=   "Column(5)._EditAlways=0"
            Splits(0)._ColumnProps(36)=   "Column(5)._ColStyle=516"
            Splits(0)._ColumnProps(37)=   "Column(5).Order=7"
            Splits(0)._ColumnProps(38)=   "Column(6).Width=3836"
            Splits(0)._ColumnProps(39)=   "Column(6).DividerColor=0"
            Splits(0)._ColumnProps(40)=   "Column(6)._WidthInPix=3757"
            Splits(0)._ColumnProps(41)=   "Column(6)._EditAlways=0"
            Splits(0)._ColumnProps(42)=   "Column(6)._ColStyle=514"
            Splits(0)._ColumnProps(43)=   "Column(6).Order=8"
            Splits(0)._ColumnProps(44)=   "Column(7).Width=1085"
            Splits(0)._ColumnProps(45)=   "Column(7).DividerColor=0"
            Splits(0)._ColumnProps(46)=   "Column(7)._WidthInPix=1005"
            Splits(0)._ColumnProps(47)=   "Column(7)._EditAlways=0"
            Splits(0)._ColumnProps(48)=   "Column(7)._ColStyle=516"
            Splits(0)._ColumnProps(49)=   "Column(7).Order=5"
            Splits.Count    =   1
            PrintInfos(0)._StateFlags=   3
            PrintInfos(0).Name=   "piInternal 0"
            PrintInfos(0).PageHeaderFont=   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Microsoft Sans Serif"
            PrintInfos(0).PageFooterFont=   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Microsoft Sans Serif"
            PrintInfos(0).PageHeaderHeight=   0
            PrintInfos(0).PageFooterHeight=   0
            PrintInfos.Count=   1
            AllowUpdate     =   0   'False
            ColumnFooters   =   -1  'True
            DataMode        =   4
            DefColWidth     =   0
            HeadLines       =   1.5
            FootLines       =   1
            RowDividerStyle =   6
            TabAction       =   1
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTipsWidth   =   0
            MultiSelect     =   0
            DeadAreaBackColor=   16777215
            RowDividerColor =   12632256
            RowSubDividerColor=   12632256
            DirectionAfterEnter=   1
            MaxRows         =   250000
            ViewColumnCaptionWidth=   0
            ViewColumnWidth =   0
            _PropDict       =   "_ExtentX,2003,3;_ExtentY,2004,3;_LayoutType,512,2;_RowHeight,16,3;_StyleDefs,513,0;_WasPersistedAsPixels,516,2"
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(5)   =   ":id=0,.fontname=MS Sans Serif"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&,.bold=0,.fontsize=825"
            _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(8)   =   ":id=1,.fontname=Tahoma"
            _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37,.bgcolor=&HE7CFCF&"
            _StyleDefs(10)  =   ":id=4,.fgcolor=&H80000008&,.appearance=0,.ellipsis=0,.borderColor=&HFF8000&"
            _StyleDefs(11)  =   ":id=4,.bold=0,.fontsize=825,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(12)  =   ":id=4,.fontname=Microsoft Sans Serif"
            _StyleDefs(13)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34,.alignment=2,.bgcolor=&HCC9999&"
            _StyleDefs(14)  =   ":id=2,.fgcolor=&HFFFFFF&,.bold=0,.fontsize=975,.italic=0,.underline=0"
            _StyleDefs(15)  =   ":id=2,.strikethrough=0,.charset=0"
            _StyleDefs(16)  =   ":id=2,.fontname=Franklin Gothic Medium"
            _StyleDefs(17)  =   "FooterStyle:id=3,.parent=1,.namedParent=35,.wraptext=-1,.bold=0,.fontsize=825"
            _StyleDefs(18)  =   ":id=3,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(19)  =   ":id=3,.fontname=Microsoft Sans Serif"
            _StyleDefs(20)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(21)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(22)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(23)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38,.bgcolor=&HC0FFFF&"
            _StyleDefs(24)  =   ":id=8,.fgcolor=&H80000008&"
            _StyleDefs(25)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39,.bgcolor=&HF5EBEB&"
            _StyleDefs(26)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40,.bgcolor=&HFFFFFF&"
            _StyleDefs(27)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(28)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(29)  =   "Splits(0).Style:id=13,.parent=1,.bold=0,.fontsize=825,.italic=0,.underline=0"
            _StyleDefs(30)  =   ":id=13,.strikethrough=0,.charset=0"
            _StyleDefs(31)  =   ":id=13,.fontname=Tahoma"
            _StyleDefs(32)  =   "Splits(0).CaptionStyle:id=22,.parent=4,.bold=-1,.fontsize=1200,.italic=0"
            _StyleDefs(33)  =   ":id=22,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(34)  =   ":id=22,.fontname=Microsoft Sans Serif"
            _StyleDefs(35)  =   "Splits(0).HeadingStyle:id=14,.parent=2,.bgcolor=&HB6DACB&,.fgcolor=&H0&"
            _StyleDefs(36)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(37)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(38)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(39)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(40)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(41)  =   "Splits(0).EvenRowStyle:id=20,.parent=9,.bgcolor=&HFCEDFC&"
            _StyleDefs(42)  =   "Splits(0).OddRowStyle:id=21,.parent=10,.bgcolor=&HF8EFEF&"
            _StyleDefs(43)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(44)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(45)  =   "Splits(0).Columns(0).Style:id=50,.parent=13"
            _StyleDefs(46)  =   "Splits(0).Columns(0).HeadingStyle:id=47,.parent=14"
            _StyleDefs(47)  =   "Splits(0).Columns(0).FooterStyle:id=48,.parent=15"
            _StyleDefs(48)  =   "Splits(0).Columns(0).EditorStyle:id=49,.parent=17"
            _StyleDefs(49)  =   "Splits(0).Columns(1).Style:id=28,.parent=13,.alignment=3"
            _StyleDefs(50)  =   "Splits(0).Columns(1).HeadingStyle:id=25,.parent=14"
            _StyleDefs(51)  =   "Splits(0).Columns(1).FooterStyle:id=26,.parent=15"
            _StyleDefs(52)  =   "Splits(0).Columns(1).EditorStyle:id=27,.parent=17"
            _StyleDefs(53)  =   "Splits(0).Columns(2).Style:id=32,.parent=13,.alignment=3"
            _StyleDefs(54)  =   "Splits(0).Columns(2).HeadingStyle:id=29,.parent=14"
            _StyleDefs(55)  =   "Splits(0).Columns(2).FooterStyle:id=30,.parent=15"
            _StyleDefs(56)  =   "Splits(0).Columns(2).EditorStyle:id=31,.parent=17"
            _StyleDefs(57)  =   "Splits(0).Columns(3).Style:id=46,.parent=13,.alignment=1"
            _StyleDefs(58)  =   "Splits(0).Columns(3).HeadingStyle:id=43,.parent=14"
            _StyleDefs(59)  =   "Splits(0).Columns(3).FooterStyle:id=44,.parent=15"
            _StyleDefs(60)  =   "Splits(0).Columns(3).EditorStyle:id=45,.parent=17"
            _StyleDefs(61)  =   "Splits(0).Columns(4).Style:id=54,.parent=13,.alignment=3"
            _StyleDefs(62)  =   "Splits(0).Columns(4).HeadingStyle:id=51,.parent=14"
            _StyleDefs(63)  =   "Splits(0).Columns(4).FooterStyle:id=52,.parent=15"
            _StyleDefs(64)  =   "Splits(0).Columns(4).EditorStyle:id=53,.parent=17"
            _StyleDefs(65)  =   "Splits(0).Columns(5).Style:id=58,.parent=13"
            _StyleDefs(66)  =   "Splits(0).Columns(5).HeadingStyle:id=55,.parent=14"
            _StyleDefs(67)  =   "Splits(0).Columns(5).FooterStyle:id=56,.parent=15"
            _StyleDefs(68)  =   "Splits(0).Columns(5).EditorStyle:id=57,.parent=17"
            _StyleDefs(69)  =   "Splits(0).Columns(6).Style:id=62,.parent=13,.alignment=1"
            _StyleDefs(70)  =   "Splits(0).Columns(6).HeadingStyle:id=59,.parent=14"
            _StyleDefs(71)  =   "Splits(0).Columns(6).FooterStyle:id=60,.parent=15"
            _StyleDefs(72)  =   "Splits(0).Columns(6).EditorStyle:id=61,.parent=17"
            _StyleDefs(73)  =   "Splits(0).Columns(7).Style:id=66,.parent=13"
            _StyleDefs(74)  =   "Splits(0).Columns(7).HeadingStyle:id=63,.parent=14"
            _StyleDefs(75)  =   "Splits(0).Columns(7).FooterStyle:id=64,.parent=15"
            _StyleDefs(76)  =   "Splits(0).Columns(7).EditorStyle:id=65,.parent=17"
            _StyleDefs(77)  =   "Named:id=33:Normal"
            _StyleDefs(78)  =   ":id=33,.parent=0,.bold=0,.fontsize=825,.italic=0,.underline=0,.strikethrough=0"
            _StyleDefs(79)  =   ":id=33,.charset=0"
            _StyleDefs(80)  =   ":id=33,.fontname=Tahoma"
            _StyleDefs(81)  =   "Named:id=34:Heading"
            _StyleDefs(82)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&HCC9999&,.fgcolor=&H80000012&"
            _StyleDefs(83)  =   ":id=34,.wraptext=-1,.bold=-1,.fontsize=975,.italic=0,.underline=0"
            _StyleDefs(84)  =   ":id=34,.strikethrough=0,.charset=0"
            _StyleDefs(85)  =   ":id=34,.fontname=Times New Roman"
            _StyleDefs(86)  =   "Named:id=35:Footing"
            _StyleDefs(87)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(88)  =   ":id=35,.bold=0,.fontsize=825,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(89)  =   ":id=35,.fontname=Tahoma"
            _StyleDefs(90)  =   "Named:id=36:Selected"
            _StyleDefs(91)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(92)  =   "Named:id=37:Caption"
            _StyleDefs(93)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(94)  =   "Named:id=38:HighlightRow"
            _StyleDefs(95)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(96)  =   "Named:id=39:EvenRow"
            _StyleDefs(97)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(98)  =   "Named:id=40:OddRow"
            _StyleDefs(99)  =   ":id=40,.parent=33"
            _StyleDefs(100) =   "Named:id=41:RecordSelector"
            _StyleDefs(101) =   ":id=41,.parent=34"
            _StyleDefs(102) =   "Named:id=42:FilterBar"
            _StyleDefs(103) =   ":id=42,.parent=33"
         End
      End
      Begin vbmaniaOCX.Label Label2 
         Height          =   240
         Left            =   225
         TabIndex        =   3
         Top             =   300
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   423
         BackColor       =   16567791
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Franklin Gothic Medium"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Bulan :"
         ForeColor       =   -2147483630
      End
      Begin MSComCtl2.DTPicker dDate0 
         Height          =   300
         Left            =   6000
         TabIndex        =   4
         Top             =   210
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   529
         _Version        =   393216
         CalendarBackColor=   12648447
         Format          =   45809667
         CurrentDate     =   39208
      End
      Begin MSComCtl2.DTPicker dDate1 
         Height          =   300
         Left            =   7665
         TabIndex        =   5
         Top             =   210
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   529
         _Version        =   393216
         CalendarBackColor=   12648447
         Format          =   45809667
         CurrentDate     =   39208
      End
      Begin vbmaniaOCX.TextBox sBulan 
         Height          =   300
         Left            =   1140
         TabIndex        =   6
         Top             =   270
         Width           =   2040
         _ExtentX        =   3598
         _ExtentY        =   529
         Text            =   ""
         Browser         =   -1  'True
      End
      Begin vbmaniaOCX.LabelText sLokasi 
         Height          =   300
         Left            =   4710
         TabIndex        =   13
         Top             =   6825
         Visible         =   0   'False
         Width           =   4020
         _ExtentX        =   7091
         _ExtentY        =   529
         BackColor       =   16567791
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "Lokasi :"
         Text            =   "C:\NamaFile.xls"
         CaptionColor    =   -2147483630
         CaptionTab      =   600
      End
   End
End
Attribute VB_Name = "RptNeraca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim RS As New ADODB.Recordset
Dim RS2 As New ADODB.Recordset
Dim xArray As New XArrayDB
Dim i As Integer

Private Sub bExport_Click()
    Call ExportExcel(DataGrid1(0))
End Sub

Private Sub bExcell_Click()

End Sub

Private Sub bKeluar_Click()
    Unload Me
End Sub

Private Sub bPreview_Click()
    With DataGrid1(0).PrintInfo
        .SettingsMarginLeft = 500
        .SettingsMarginRight = 200
        .SettingsMarginBottom = 700
        .SettingsMarginTop = 700
        .SettingsOrientation = 2
        .PageHeader = "Neraca   Bulan : " & sBulan.Text & " Tahun : " & sTahun.Text
        .PageFooter = "Tanggal Cetak : " & Date & " Jam : " & Time
        .PreviewMaximize = True
        .PreviewInitZoom = 100
        .PrintPreview
    End With
End Sub

Private Sub bProses_Click()
    If Md.NotBlank(Array(sBulan, sTahun)) = True Then
        InputDataGrid
        bPreview.SetFocus
        bExport.Enabled = True
    End If
End Sub

Private Sub Form_Load()
Dim sDate As String
    Me.Left = (aMenu.Width - Me.Width) / 2
    Me.Top = ((aMenu.Height - Me.Height) - 1600) / 2
    Md.SetTab Array(sBulan, sTahun, bProses, bKeluar)
    sBulan.Text = MonthName(Month(Date))
    sTahun.Text = Year(Date)

    sDate = "01-" & sBulan.Text & "-" & sTahun.Text
    dDate0.Value = Format(sDate, "dd-MMM-yyyy")
    dDate1.Value = DateAdd("m", 1, dDate0.Value)        'Cari Bulan Berikutnya
    dDate1.Value = DateAdd("d", -1, dDate1.Value)        'Cari Tanggal Akhir Bulan
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set RS = Nothing
    Set RS2 = Nothing
End Sub

Private Sub sBulan_KeyDown(KeyCode As Integer, Shift As Integer)
Dim Va As Variant
    If KeyCode = 13 Then
        Va = Md.Pick("Select Nama from Bulan Order by No", _
                     Array(1, 1), sBulan)
        SetInputData Array(sBulan), Array(Va(1))
        sTahun.SetFocus
    End If
End Sub

Private Sub sBulan_Validate(Cancel As Boolean)
Dim sDate As String
    sDate = "01-" & sBulan.Text & "-" & sTahun.Text
    If IsDate(sDate) = True Then
        dDate0.Value = Format(sDate, "dd-MMM-yyyy")
        dDate1.Value = DateAdd("m", 1, dDate0.Value)        'Cari Bulan Berikutnya
        dDate1.Value = DateAdd("d", -1, dDate1.Value)       'Cari Tanggal Akhir Bulan
    End If
End Sub

Private Sub sTahun_Validate(Cancel As Boolean)
Dim sDate As String
    sDate = "01-" & sBulan.Text & "-" & sTahun.Text
    If IsDate(sDate) = True Then
        dDate0.Value = Format(sDate, "dd-MMM-yyyy")
        dDate1.Value = DateAdd("m", 1, dDate0.Value)        'Cari Bulan Berikutnya
        dDate1.Value = DateAdd("d", -1, dDate1.Value)       'Cari Tanggal Akhir Bulan
    End If
End Sub

Private Sub InputDataGrid()
Dim i As Integer, s As String
Dim X As Integer
Dim dTotalAktivaLancar As Double, dTotalAktivaTetap As Double, dTotalAkumulasi As Double, dTotalAktivaLain As Double, dTotalAktiva As Double
Dim dTotalHutang As Double, dTotalModal As Double, dTotalLaba As Double, dTotalHutangModal As Double

    'AKTIVA

    xArray.ReDim 0, -1, 0, 7
    xArray.AppendRows 3
    xArray(0, 2) = "AKTIVA"
    xArray(2, 2) = "AKTIVA LANCAR"

    s = "Select * From KodePerkiraan Where Left(Kode,5) = '1.1.0' order by Kode"

    Set RS = Md.GetData(s)

    If Not RS.EOF Then
        RS.MoveFirst
        Do While Not RS.EOF

            xArray.InsertRows xArray.UpperBound(1) + 1
            i = xArray.UpperBound(1)
            xArray(i, 1) = (RS!Kode)
            xArray(i, 2) = (RS!Nama)
                
            s = "Select Sum(j.Debet-j.Kredit) as Saldo " & _
                "from Jurnal j " & _
                "Where j.KodePerkiraan = '" & xArray(i, 1) & "' " & _
                "And j.Tanggal <= '" & Format(dDate1.Value, "yyyy-mm-dd") & "'  " & _
                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
                "Group by j.KodePerkiraan"

            Set RS2 = Md.GetData(s)
            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
                xArray(i, 3) = (RS2!Saldo)
            Else
                xArray(i, 3) = 0
            End If
            
            dTotalAktivaLancar = dTotalAktivaLancar + xArray(i, 3)
            
            RS.MoveNext
        Loop

    End If
    
    xArray.AppendRows 3
    i = xArray.UpperBound(1)
    xArray(i - 2, 2) = "TOTAL AKTIVA LANCAR"
    xArray(i - 2, 3) = dTotalAktivaLancar
    xArray(i, 2) = "AKTIVA TETAP"

    s = "Select * From KodePerkiraan Where Left(Kode,5) = '1.2.0' and Nama not like 'Akumulasi%' order by Kode"

    Set RS = Md.GetData(s)

    If Not RS.EOF Then
        RS.MoveFirst
        Do While Not RS.EOF

            xArray.InsertRows xArray.UpperBound(1) + 1
            i = xArray.UpperBound(1)
            xArray(i, 1) = (RS!Kode)
            xArray(i, 2) = (RS!Nama)
            
            s = "Select Sum(j.Debet-j.Kredit) as Saldo " & _
                "from Jurnal j " & _
                "Where j.KodePerkiraan = '" & xArray(i, 1) & "' " & _
                "And j.Tanggal <= '" & Format(dDate1.Value, "yyyy-mm-dd") & "'  " & _
                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
                "Group by j.KodePerkiraan"

            Set RS2 = Md.GetData(s)
            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
                xArray(i, 3) = (RS2!Saldo)
            Else
                xArray(i, 3) = 0
            End If
            
            dTotalAktivaTetap = dTotalAktivaTetap + xArray(i, 3)
            
            RS.MoveNext
        Loop

    End If
    
    xArray.AppendRows 3
    i = xArray.UpperBound(1)
    xArray(i - 2, 2) = "TOTAL AKTIVA TETAP"
    xArray(i - 2, 3) = dTotalAktivaTetap
    xArray(i, 2) = "AKUMULASI PENYUSUTAN"

    s = "Select * From KodePerkiraan Where Nama like 'Akumulasi%' order by Kode"

    Set RS = Md.GetData(s)

    If Not RS.EOF Then
        RS.MoveFirst
        Do While Not RS.EOF

            xArray.InsertRows xArray.UpperBound(1) + 1
            i = xArray.UpperBound(1)
            xArray(i, 1) = (RS!Kode)
            xArray(i, 2) = (RS!Nama)
            
            s = "Select Sum(j.Kredit-j.Debet) as Saldo " & _
                "from Jurnal j " & _
                "Where j.KodePerkiraan = '" & xArray(i, 1) & "' " & _
                "And j.Tanggal <= '" & Format(dDate1.Value, "yyyy-mm-dd") & "'  " & _
                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
                "Group by j.KodePerkiraan"

            Set RS2 = Md.GetData(s)
            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
                xArray(i, 3) = -(RS2!Saldo)
            Else
                xArray(i, 3) = 0
            End If
            
            dTotalAkumulasi = dTotalAkumulasi + xArray(i, 3)
            
            RS.MoveNext
        Loop

    End If
    
    xArray.AppendRows 3
    i = xArray.UpperBound(1)
    xArray(i - 2, 2) = "TOTAL AKUMULASI PENYUSUTAN"
    xArray(i - 2, 3) = dTotalAkumulasi
    xArray(i, 2) = "AKTIVA LAIN-LAIN"
    
    s = "Select * From KodePerkiraan Where Left(Kode,5) = '1.1.1' order by Kode"

    Set RS = Md.GetData(s)

    If Not RS.EOF Then
        RS.MoveFirst
        Do While Not RS.EOF

            xArray.InsertRows xArray.UpperBound(1) + 1
            i = xArray.UpperBound(1)
            xArray(i, 1) = (RS!Kode)
            xArray(i, 2) = (RS!Nama)
            
            s = "Select Sum(j.Debet-j.Kredit) as Saldo " & _
                "from Jurnal j " & _
                "Where j.KodePerkiraan = '" & xArray(i, 1) & "' " & _
                "And j.Tanggal <= '" & Format(dDate1.Value, "yyyy-mm-dd") & "'  " & _
                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
                "Group by j.KodePerkiraan"

            Set RS2 = Md.GetData(s)
            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
                xArray(i, 3) = (RS2!Saldo)
            Else
                xArray(i, 3) = 0
            End If
            
            dTotalAktivaLain = dTotalAktivaLain + xArray(i, 3)
            
            RS.MoveNext
        Loop

    End If
    
    xArray.AppendRows 3
    i = xArray.UpperBound(1)
    xArray(i - 2, 2) = "TOTAL AKTIVA LAIN-LAIN"
    xArray(i - 2, 3) = dTotalAktivaLain
    xArray(i, 2) = "TOTAL AKTIVA"
    dTotalAktiva = dTotalAktivaLancar + dTotalAktivaTetap + dTotalAkumulasi + dTotalAktivaLain
    xArray(i, 3) = dTotalAktiva
    
    'HUTANG DAN MODAL
    
    xArray(0, 5) = "HUTANG DAN MODAL"
    xArray(2, 5) = "HUTANG"
    X = 3

    s = "Select * From KodePerkiraan Where Left(Kode,4) = '2.1.' or Left(Kode,4) = '2.2.' order by Kode"

    Set RS = Md.GetData(s)

    If Not RS.EOF Then
        
        RS.MoveFirst
        Do While Not RS.EOF

            xArray(X, 4) = (RS!Kode)
            xArray(X, 5) = (RS!Nama)
            
            s = "Select Sum(j.Kredit-j.Debet) as Saldo " & _
                "from Jurnal j " & _
                "Where j.KodePerkiraan = '" & xArray(X, 4) & "' " & _
                "And j.Tanggal <= '" & Format(dDate1.Value, "yyyy-mm-dd") & "'  " & _
                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
                "Group by j.KodePerkiraan"

            Set RS2 = Md.GetData(s)
            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
                xArray(X, 6) = (RS2!Saldo)
            Else
                xArray(X, 6) = 0
            End If
            
            dTotalHutang = dTotalHutang + xArray(X, 6)
            
            RS.MoveNext
            X = X + 1
            
        Loop

    End If
    
    X = X + 3
    
    xArray(X - 3, 5) = "TOTAL HUTANG"
    xArray(X - 3, 6) = dTotalHutang
    xArray(X - 1, 5) = "MODAL"
    
    s = "Select * From KodePerkiraan Where Left(Kode,3) = '3.1' order by Kode"

    Set RS = Md.GetData(s)

    If Not RS.EOF Then

        RS.MoveFirst
        Do While Not RS.EOF

            xArray(X, 4) = (RS!Kode)
            xArray(X, 5) = (RS!Nama)

            s = "Select Sum(j.Kredit-j.Debet) as Saldo " & _
                "from Jurnal j " & _
                "Where j.KodePerkiraan = '" & xArray(X, 4) & "' " & _
                "And j.Tanggal <= '" & Format(dDate1.Value, "yyyy-mm-dd") & "'  " & _
                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
                "Group by j.KodePerkiraan"

            Set RS2 = Md.GetData(s)
            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
                xArray(X, 6) = (RS2!Saldo)
            Else
                xArray(X, 6) = 0
            End If

            dTotalModal = dTotalModal + xArray(X, 6)

            RS.MoveNext
            X = X + 1

        Loop

    End If

    X = X + 3

    xArray(X - 3, 5) = "TOTAL MODAL"
    xArray(X - 3, 6) = dTotalModal
'    xArray(x - 1, 5) = "LABA DITAHAN"
'
'    s = "Select * From KodePerkiraan Where Left(Kode,5) = '4.111' order by Kode"
'
'    Set RS = Md.GetData(s)
'
'    If Not RS.EOF Then
'
'        RS.MoveFirst
'        Do While Not RS.EOF
'
'            xArray(x, 4) = (RS!Kode)
'            xArray(x, 5) = (RS!Nama)
'
'            s = "Select Sum(j.SaldoAwal+j.Kredit-j.Debet) as Saldo " & _
'                "from Jurnal j " & _
'                "Where j.KodePerkiraan = '" & xArray(x, 4) & "' " & _
'                "And j.Tanggal <= '" & Format(ddate1.value, "yyyy-mm-dd") & "'  " & _
'                "And year(j.Tanggal) = '" & sTahun.Text & "' " & _
'                "Group by j.KodePerkiraan"
'
'            Set RS2 = Md.GetData(s)
'            If IsNull(RS2!Saldo) = False And RS2.RecordCount > 0 Then
'                xArray(x, 6) = (RS2!Saldo)
'            Else
'                xArray(x, 6) = 0
'            End If
'
'            dTotalLaba = dTotalLaba + xArray(x, 6)
'
'            RS.MoveNext
'            x = x + 1
'
'        Loop
'
'    End If
'
'    xArray(x, 5) = "TOTAL LABA DITAHAN"
'    xArray(x, 6) = dTotalLaba

    dTotalHutangModal = dTotalHutang + dTotalModal + dTotalLaba

    i = xArray.UpperBound(1)
    xArray(i, 5) = "TOTAL HUTANG DAN MODAL"
    xArray(i, 6) = dTotalHutangModal

    Set DataGrid1(0).Array = xArray
    DataGrid1(0).ReBind

End Sub




