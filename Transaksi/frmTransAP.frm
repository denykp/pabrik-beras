VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmTransAP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Account Payable"
   ClientHeight    =   8265
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9000
   DrawStyle       =   1  'Dash
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8265
   ScaleWidth      =   9000
   Begin VB.ComboBox cmbTipe 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmTransAP.frx":0000
      Left            =   5805
      List            =   "frmTransAP.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   34
      Top             =   135
      Width           =   2310
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1584
      TabIndex        =   2
      Top             =   972
      Width           =   1530
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3180
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   29
      Top             =   960
      Width           =   420
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting (F6)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3864
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   7584
      Width           =   1230
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   23
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1656
      Left            =   135
      TabIndex        =   16
      Top             =   2190
      Width           =   8565
      Begin VB.TextBox txtNilai 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   5
         Top             =   675
         Width           =   1140
      End
      Begin VB.CommandButton cmdSearchAcc 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2835
         TabIndex        =   17
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2745
         TabIndex        =   8
         Top             =   1164
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1635
         TabIndex        =   7
         Top             =   1164
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   225
         TabIndex        =   6
         Top             =   1164
         Width           =   1350
      End
      Begin VB.TextBox txtAcc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   4
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label LblNamaAcc 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Account"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   3285
         TabIndex        =   18
         Top             =   315
         Width           =   3525
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Nilai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   26
         Top             =   720
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4185
         TabIndex        =   21
         Top             =   1575
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   20
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Account"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   19
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   495
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7584
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   15
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   690
      Left            =   1572
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   1356
      Width           =   7035
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5271
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7584
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2499
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   7584
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1605
      TabIndex        =   0
      Top             =   585
      Width           =   2475
      _ExtentX        =   4366
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   100794371
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2856
      Left            =   108
      TabIndex        =   22
      Top             =   3936
      Width           =   8616
      _ExtentX        =   15187
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   4
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8460
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   336
      Left            =   5856
      TabIndex        =   1
      Top             =   612
      Width           =   1872
      _ExtentX        =   3307
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   100794371
      CurrentDate     =   38927
   End
   Begin VB.Label Label2 
      Caption         =   "Tipe"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4320
      TabIndex        =   33
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label9 
      Caption         =   "Jatuh Tempo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4368
      TabIndex        =   32
      Top             =   648
      Width           =   1320
   End
   Begin VB.Label lblNamaSupplier 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   228
      Left            =   3732
      TabIndex        =   31
      Top             =   1032
      Width           =   72
   End
   Begin VB.Label Label19 
      Caption         =   "Pemberi Hutang :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   30
      Top             =   1020
      Width           =   1365
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   27
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   6180
      TabIndex        =   25
      Top             =   6972
      Width           =   2112
   End
   Begin VB.Label Label17 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   5268
      TabIndex        =   24
      Top             =   6972
      Width           =   732
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   192
      TabIndex        =   14
      Top             =   624
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   192
      TabIndex        =   13
      Top             =   1404
      Width           =   1272
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   12
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransAP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryCOA As String = "select * from ms_coa where fg_aktif = 1"

Dim mode As Byte
Dim totalNilai As Long

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String
Public Tipe As String

Private Sub cmdClear_Click()
    txtAcc.text = ""
    debet = True
    LblNamaAcc = ""
    txtNilai.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    Row = flxGrid.Row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalNilai = totalNilai - (flxGrid.TextMatrix(Row, 3))
    lblTotal = Format(totalNilai, "#,##0")
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtAcc.text <> "" And LblNamaAcc <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtAcc.text) Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(Row, 1) = txtAcc.text
        flxGrid.TextMatrix(Row, 2) = LblNamaAcc
        
        If flxGrid.TextMatrix(Row, 3) <> "" Then totalNilai = totalNilai - (flxGrid.TextMatrix(Row, 3))
        flxGrid.TextMatrix(Row, 3) = txtNilai
        totalNilai = totalNilai + (flxGrid.TextMatrix(Row, 3))
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = 3
        
        lblTotal = Format(totalNilai, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtAcc.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub cmdPosting_Click()
    Call Posting_AP(lblNoTrans)
    reset_form
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = queryCOA
    frmSearch.nmform = "frmTransap"
    frmSearch.nmctrl = "txtAcc"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_Acc Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = searchAP
    frmSearch.nmform = "frmAddjurnal"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub




Private Sub cmdSearchSupplier_Click()
    frmSearch.connstr = strcon
    If cmbTipe.text = "Karyawan" Then
    frmSearch.query = searchKaryawan
    Else
    frmSearch.query = SearchContact
    End If
    
    frmSearch.nmform = "frmAddpembelianplat"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim Row As Integer

On Error GoTo err
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan detail terlebih dahulu"
        txtAcc.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_aph", "nomer_ap", DTPicker1, "AP")
'        NoJurnal = Nomor_Jurnal(DTPicker1)
    Else
        
'            rs.Open "select no_jurnal from t_jurnalh where nomer_ap='" & lblNoTrans & "'", conn
'            If Not rs.EOF Then
'                NoJurnal = rs("no_jurnal")
'            End If
'            rs.Close
            
            conn.Execute "delete from t_aph where nomer_ap='" & lblNoTrans & "'"
            conn.Execute "delete from t_apd where nomer_ap='" & lblNoTrans & "'"
            
'            conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
'            conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
'
            
'            conn.Execute "delete from jurnal where no_trans='" & lblNoTrans & "'"
        
    End If
    
    
    
    add_dataheader
'    add_jurnalheader
    
    For Row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(Row, 1) <> "" Then
            add_datadetail (Row)
'            add_jurnaldetail (row)
        End If
    Next
    
    
    
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub


Public Sub reset_form()
    lblNoTrans = "-"
    txtKodeSupplier.text = ""

    txtKeterangan.text = ""
    totalNilai = 0
    DTPicker1 = Now
    DTPicker2 = DateAdd("m", 1, Now)
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(9)
    ReDim nilai(9)
    table_name = "t_aph"
    fields(0) = "nomer_ap"
    fields(1) = "tanggal"
    
    fields(2) = "kode_supplier"
    fields(3) = "keterangan"
    fields(4) = "total"
    fields(6) = "userid"
    fields(5) = "tanggal_jatuhtempo"
    fields(7) = "tipe"
    fields(8) = "status_posting"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKodeSupplier.text
    nilai(3) = txtKeterangan.text
    nilai(4) = Format(totalNilai, "###0")
    nilai(6) = User
    nilai(5) = Format(DTPicker2, "yyyy/MM/dd")
    nilai(7) = cmbTipe.text
    nilai(8) = "0"
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_apd"
    fields(0) = "nomer_ap"
    fields(1) = "kode_acc"
    fields(2) = "nilai"
    fields(3) = "no_urut"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = flxGrid.TextMatrix(Row, 3)
    nilai(3) = Row
    
    tambah_data table_name, fields, nilai
End Sub




Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim StatusPosting As String
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_aph t where nomer_ap='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
        
        DTPicker1 = rs("tanggal")
        DTPicker2 = rs("tanggal_jatuhtempo")
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        SetComboText rs!Tipe, cmbTipe
        cek_supplier
        StatusPosting = rs("status_posting")
        totalNilai = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1
        
        rs.Open "select d.kode_acc,m.Nama,d.nilai from t_apd d inner join ms_coa m on d.kode_acc=m.kode_acc where d.nomer_ap='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = rs(2)
                
                Row = Row + 1
                totalNilai = totalNilai + rs(2)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(totalNilai, "#,##0")
        
        If StatusPosting = "1" Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
        End If

        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub Command1_Click()
End Sub

Private Sub DTPicker1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then

    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtAcc.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            cek_Acc
            
            txtNilai.text = flxGrid.TextMatrix(flxGrid.Row, 3)
            txtNilai.SetFocus
        End If
    End If
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyF3 Then cmdSearchAcc_Click
    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF6 And cmdPosting.Visible Then cmdPosting_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtAcc.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    LblNamaAcc = ""
    loadcombo
    reset_form
    total = 0
    
    
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    'End If
    'rs.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 1500
    
    
    flxGrid.TextMatrix(0, 1) = "Kode Acc"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Nilai"
    


End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_Acc() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where fg_aktif = 1 and kode_acc='" & txtAcc & "' ", conn
    If Not rs.EOF Then
        LblNamaAcc = rs(1)
        cek_Acc = True
        'If rs(4) = "D" Then debet = True Else debet = False
    Else
        LblNamaAcc = ""
        lblKode = ""
        
        cek_Acc = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub



Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtAcc_GotFocus()
    txtAcc.SelStart = 0
    txtAcc.SelLength = Len(txtAcc.text)
'    foc = 1
End Sub

Private Sub txtAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
'        If txtAcc.text = "" Then
'            flxGrid_Click
'        Else
        If txtAcc.text = "" Then KeyCode = 0
        
        If txtAcc.text <> "" Then
            If Not cek_Acc Then
                MsgBox "Kode yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    End If
End Sub

Public Function cek_supplier() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    cek_supplier = False
    conn.ConnectionString = strcon
    conn.Open
    If cmbTipe.text = "Karyawan" Then
        rs.Open "select [nama_karyawan] from ms_karyawan where nik='" & txtKodeSupplier & "'", conn
    Else
        rs.Open "select [nama_contact] from ms_contact where kode_contact='" & txtKodeSupplier & "'", conn
    End If
    If Not rs.EOF Then
        cek_supplier = True
        lblNamaSupplier = rs(0)
    Else
        lblNamaSupplier = ""
    End If
    rs.Close
    conn.Close
err:
End Function

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    If Not cek_supplier And txtKodeSupplier <> "" Then
        MsgBox "Kode supplier yang anda masukkan salah"
        txtKodeSupplier.SetFocus
    End If
End Sub
Private Sub txtNilai_GotFocus()
    txtNilai.SelStart = 0
    txtNilai.SelLength = Len(txtNilai.text)
End Sub

Private Sub txtNilai_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtNilai_LostFocus()
    If Not IsNumeric(txtNilai.text) Then txtNilai.text = "1"
End Sub

Private Sub txtKeterangan_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtAcc.SetFocus
End Sub

