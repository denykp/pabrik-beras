VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTransAngkut 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Transaksi Angkutan"
   ClientHeight    =   7560
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8175
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   8175
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5235
      Picture         =   "frmTransAngkut.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4140
      Picture         =   "frmTransAngkut.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.TextBox txtNoTruk 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1350
      TabIndex        =   16
      Top             =   1350
      Width           =   1815
   End
   Begin VB.ListBox listTruk 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   3705
      TabIndex        =   15
      Top             =   1350
      Width           =   3660
   End
   Begin VB.CommandButton cmdSearchPengawas 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3180
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   14
      Top             =   2745
      Width           =   420
   End
   Begin VB.ListBox listKuli 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      Left            =   1350
      Style           =   1  'Checkbox
      TabIndex        =   13
      Top             =   3660
      Width           =   3705
   End
   Begin VB.ListBox listPengawas 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   3705
      TabIndex        =   12
      Top             =   2745
      Width           =   3660
   End
   Begin VB.TextBox txtPengawas 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1350
      TabIndex        =   11
      Top             =   2745
      Width           =   1755
   End
   Begin VB.TextBox txtSupir 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1350
      TabIndex        =   10
      Top             =   2295
      Width           =   5220
   End
   Begin VB.TextBox txtKodeEkspedisi 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1350
      TabIndex        =   8
      Top             =   810
      Width           =   2025
   End
   Begin VB.CommandButton cmdSearchEkspedisi 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3450
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   7
      Top             =   795
      Width           =   420
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2955
      Picture         =   "frmTransAngkut.frx":0A64
      TabIndex        =   6
      Top             =   6630
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   300
      Picture         =   "frmTransAngkut.frx":0B66
      TabIndex        =   5
      Top             =   6630
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1605
      Picture         =   "frmTransAngkut.frx":0C68
      TabIndex        =   4
      Top             =   6645
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3495
      TabIndex        =   0
      Top             =   240
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   5100
      TabIndex        =   2
      Top             =   870
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   232062979
      CurrentDate     =   38927
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1350
      TabIndex        =   22
      Top             =   225
      Width           =   2085
   End
   Begin VB.Label Label5 
      Caption         =   "No. Truk"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   21
      Top             =   1395
      Width           =   960
   End
   Begin VB.Label lblNamaPengawas 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1365
      TabIndex        =   20
      Top             =   3150
      Width           =   2265
   End
   Begin VB.Label Label9 
      Caption         =   "Supir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   19
      Top             =   2385
      Width           =   960
   End
   Begin VB.Label Label8 
      Caption         =   "Kuli"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   18
      Top             =   3705
      Width           =   960
   End
   Begin VB.Label Label6 
      Caption         =   "Pengawas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   17
      Top             =   2790
      Width           =   960
   End
   Begin VB.Label Label10 
      Caption         =   "Ekspedisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   9
      Top             =   870
      Width           =   960
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4125
      TabIndex        =   3
      Top             =   885
      Width           =   900
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   1
      Top             =   270
      Width           =   1320
   End
End
Attribute VB_Name = "frmTransAngkut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_angkut] from t_angkut where [nomer_angkut]>'" & lblNoTrans & "' order by [nomer_angkut]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_angkut] from t_angkut where [nomer_angkut]<'" & lblNoTrans & "' order by [nomer_angkut] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearchEkspedisi_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchEkspedisi
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKodeEkspedisi"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_ekspedisi"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_ekspedisi"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = "Select * from t_Angkut"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Angkut"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmAddPenerimaanBarang"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub


Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub


Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_angkut", "nomer_angkut", DTPicker1, "BA")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1

    conn.Execute "delete from t_angkut where nomer_angkut='" & id & "'"

    add_ekspedisi
    Dim counter As Integer
    counter = 1
    conn.CommitTrans
    i = 0
    simpan = True
    conn.Close
    DropConnection
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Public Sub reset_form()
Dim J As Integer
On Error Resume Next
    lblNoTrans = "-"
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    txtKodeEkspedisi = ""
    listPengawas.Clear
    
    listTruk.Clear
    txtSupir = ""
    For J = 0 To listKuli.ListCount - 1
        listKuli.Selected(J) = False
    Next
    
    DTPicker1 = Now
    
    cmdSimpan.Enabled = True
    txtKodeEkspedisi.SetFocus
    
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
End Sub

Private Sub add_ekspedisi()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(10)
    ReDim nilai(10)

    table_name = "t_angkut"
    fields(0) = "nomer_angkut"
    fields(1) = "tanggal"
    fields(2) = "kode_ekspedisi"
    fields(3) = "fg_bayarangkut"
    fields(4) = "no_truk"
    fields(5) = "supir"
    fields(6) = "no_urut"
    fields(7) = "status"
    fields(8) = "pengawas"
    fields(9) = "kuli"
    
    Dim supir As String, notruk As String, kuli As String, pengawas As String
    pengawas = ""
    For A = 0 To listPengawas.ListCount - 1
        pengawas = pengawas & "[" & Left(listPengawas.List(A), InStr(1, listPengawas.List(A), "-") - 1) & "],"
    Next
    If pengawas <> "" Then pengawas = Left(pengawas, Len(pengawas) - 1)
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd")
    
    nilai(2) = txtKodeEkspedisi
    nilai(3) = 0
    nilai(4) = notruk
    nilai(5) = supir
    nilai(6) = 1
    nilai(7) = 1
    nilai(8) = pengawas
    nilai(9) = kuli
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_angkut where nomer_angkut='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
         
        DTPicker1 = rs("tanggal")
        
        txtKodeEkspedisi = rs!kode_ekspedisi
        cek_ekspedisi
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        
        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
        Dim pengawas() As String
        Dim kuli() As String
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
            txtNoTruk_KeyDown 13, 0
        Next
        pengawas = Split(rs!pengawas, ",")
        listPengawas.Clear
        For A = 0 To UBound(pengawas)
            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
            txtPengawas_KeyDown 13, 0
        Next
kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
        Next
    End If

    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    reset_form
    Load_ListKuli
'    conn.ConnectionString = strcon
'    conn.Open
'    listKuli.Clear
'    rs.Open "select * from ms_karyawan order by nik", conn
'    While Not rs.EOF
'        listKuli.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Close
End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Public Sub cek_pengawas()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close

err:
End Sub

Public Sub cek_kuli()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtKuli & "'", conn
    If Not rs.EOF Then
        lblNamaKuli = rs(1)
    Else
        lblNamaKuli = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyDelete Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub



Private Sub txtKodeEkspedisi_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchEkspedisi_Click
End Sub


Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPengawas_Click
    If KeyCode = 13 Then
        cek_pengawas
        listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
        txtPengawas = ""
        lblNamaPengawas = ""
        MySendKeys "+{tab}"
    End If
End Sub

Public Function cek_ekspedisi() As Boolean
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_ekspedisi] from ms_ekspedisi where kode_ekspedisi='" & txtKodeEkspedisi & "'", conn
    If Not rs.EOF Then
        lblNamaEkspedisi = rs(0)
        cek_ekspedisi = True
    Else
        lblNamaEkspedisi = ""
        cek_ekspedisi = False
    End If
    rs.Close
    conn.Close
    
err:
End Function

Private Sub txtKodeEkspedisi_LostFocus()
    If txtKodeEkspedisi.text <> "" And Not cek_ekspedisi Then
        MsgBox "Kode ekspedisi tidak ditemukan"
        txtKodeEkspedisi.SetFocus
    End If
End Sub


