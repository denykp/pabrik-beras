alter proc [dbo].[sp_stockawal]( 	
	@kode_kategori varchar(15)= '',
	@kode_bahan varchar(15)='', 
	@kode_gudang varchar(5)='',
	@periode varchar(8)=''
	)
as
	delete from tmp_stockawal
	
if (@periode ='')
	insert into tmp_stockawal
	select  ms_gudang.kode_gudang, ms_bahan.kode_bahan,nomer_serial, 0,0 as stockawal from ms_gudang,ms_bahan left join stock on ms_bahan.kode_bahan=stock.kode_bahan 
			group by ms_gudang.kode_gudang,ms_bahan.kode_bahan,nomer_serial
else
if (@kode_kategori = '')
	if (@kode_bahan ='')
		if (@kode_gudang  ='')
			insert into tmp_stockawal
			select  barang.kode_gudang, barang.kode_bahan,nomer_serial, cast(isnull(SUM(masuk-keluar),0) as numeric) as stockawal,cast(isnull(SUM(qtymasuk-qtykeluar),0) as numeric) as qtyawal from (select distinct kode_gudang,kode_bahan from stock) barang left join tmp_kartustock  	on barang.kode_bahan=tmp_kartustock.kode_bahan and barang.kode_gudang=tmp_kartustock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode
				group by barang.kode_gudang,barang.kode_bahan,nomer_serial
		else
			insert into tmp_stockawal
			select  barang.kode_gudang, barang.kode_bahan,nomer_serial, isnull(SUM(masuk-keluar),0) as stockawal,isnull(SUM(qtymasuk-qtykeluar),0) as qtyawal from (select distinct kode_gudang,kode_bahan from stock) barang left join tmp_kartustock  	on barang.kode_bahan=tmp_kartustock.kode_bahan and barang.kode_gudang=tmp_kartustock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode where barang.kode_gudang=@kode_gudang 
				group by barang.kode_gudang,barang.kode_bahan,nomer_serial
	else
		if (@kode_gudang  ='')
			insert into tmp_stockawal
			select  barang.kode_gudang, barang.kode_bahan,nomer_serial, isnull(SUM(masuk-keluar),0) as stockawal,isnull(SUM(qtymasuk-qtykeluar),0) as qtyawal from (select distinct kode_gudang,kode_bahan from stock) barang left join tmp_kartustock  	on barang.kode_bahan=tmp_kartustock.kode_bahan and barang.kode_gudang=tmp_kartustock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where barang.kode_bahan=@kode_bahan 
				group by barang.kode_gudang,barang.kode_bahan,nomer_serial
		else
			insert into tmp_stockawal
			select  barang.kode_gudang, barang.kode_bahan,nomer_serial, isnull(SUM(masuk-keluar),0) as stockawal,isnull(SUM(qtymasuk-qtykeluar),0) as qtyawal from (select distinct kode_gudang,kode_bahan from stock) barang left join tmp_kartustock  	on barang.kode_bahan=tmp_kartustock.kode_bahan and barang.kode_gudang=tmp_kartustock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where barang.kode_gudang=@kode_gudang and barang.kode_bahan=@kode_bahan 
				group by barang.kode_gudang,barang.kode_bahan,nomer_serial
else
	
		if (@kode_gudang  ='')
			insert into tmp_stockawal
			select  barang.kode_gudang, barang.kode_bahan,nomer_serial, isnull(SUM(masuk-keluar),0) as stockawal,isnull(SUM(qtymasuk-qtykeluar),0) as qtyawal from (select distinct kode_gudang,stock.kode_bahan,ms_bahan.kategori from stock,ms_bahan) barang left join tmp_kartustock  	on barang.kode_bahan=tmp_kartustock.kode_bahan and barang.kode_gudang=tmp_kartustock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where kategori=@kode_kategori 
				group by barang.kode_gudang,barang.kode_bahan,nomer_serial
		else
			insert into tmp_stockawal
			select  barang.kode_gudang, barang.kode_bahan,nomer_serial, isnull(SUM(masuk-keluar),0) as stockawal,isnull(SUM(qtymasuk-qtykeluar),0) as qtyawal from (select distinct kode_gudang,stock.kode_bahan,ms_bahan.kategori from stock,ms_bahan) barang left join tmp_kartustock  	on barang.kode_bahan=tmp_kartustock.kode_bahan and barang.kode_gudang=tmp_kartustock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where barang.kode_gudang=@kode_gudang and barang.kode_gudang=@kode_kategori 			
				group by barang.kode_gudang,barang.kode_bahan,nomer_serial
	

GO