VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmBatalCairBG 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Batal Pencairan Giro"
   ClientHeight    =   3615
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7350
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   7350
   Begin VB.TextBox txtKodeBank 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2070
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   1830
      Width           =   1530
   End
   Begin VB.TextBox txtKode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2070
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   1410
      Width           =   1530
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4230
      Picture         =   "frmBatalCairBG.frx":0000
      TabIndex        =   8
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtNominal 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2070
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   2265
      Width           =   2040
   End
   Begin VB.TextBox txtNoTransaksi 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2085
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   615
      Width           =   3480
   End
   Begin VB.TextBox txtNoBG 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2070
      MaxLength       =   10
      TabIndex        =   0
      Top             =   225
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3705
      Picture         =   "frmBatalCairBG.frx":0102
      TabIndex        =   4
      Top             =   2910
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Batalkan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2385
      Picture         =   "frmBatalCairBG.frx":0204
      TabIndex        =   3
      Top             =   2910
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   2070
      TabIndex        =   10
      Top             =   1035
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   291831811
      CurrentDate     =   38927
   End
   Begin VB.Label Label9 
      Caption         =   "Bank"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   16
      Top             =   1860
      Width           =   1320
   End
   Begin VB.Label lblNamaBank 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   3690
      TabIndex        =   15
      Top             =   1905
      Width           =   3345
   End
   Begin VB.Label Label5 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   13
      Top             =   1440
      Width           =   1320
   End
   Begin VB.Label lblNama 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   3690
      TabIndex        =   12
      Top             =   1470
      Width           =   3375
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   11
      Top             =   1065
      Width           =   1320
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Nominal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   7
      Top             =   2295
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nomer Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   6
      Top             =   675
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nomer BG "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   270
      TabIndex        =   5
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmBatalCairBG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public JenisBG As String

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    If JenisBG = "hutang" Then
        frmSearch.query = "Select l.nomer_BG,l.nomer_transaksi,l.tanggal_cair,s.nama_supplier,b.nama_bank " & _
                          "from list_BG l " & _
                          "inner join ms_supplier s on s.kode_supplier=l.kode_supplier " & _
                          "inner join ms_bank b on b.kode_bank=l.kode_bank " & _
                          "where l.status_cair='1' order by l.tanggal_cair"
    Else
        frmSearch.query = "Select l.nomer_BG,l.nomer_transaksi,l.tanggal_cair,s.nama_customer,b.nama_bank " & _
                          "from list_BG l " & _
                          "inner join ms_customer s on s.kode_customer=l.kode_customer " & _
                          "inner join ms_bank b on b.kode_bank=l.kode_bank " & _
                          "where l.status_cair='1' order by l.tanggal_cair"
    End If
    frmSearch.nmform = "frmBatalCairBG"
    frmSearch.nmctrl = "txtNoBG"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "batalCairBG"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_BG"

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub search_BG()
On Error Resume Next
    If Cek_BG Then Call MySendKeys("{tab}")
End Sub

Public Function Cek_BG() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    If JenisBG = "hutang" Then
        rs.Open "Select l.nomer_BG,l.nomer_transaksi,l.tanggal_cair,l.kode_supplier, " & _
                "s.nama_supplier,l.kode_bank,b.nama_bank,l.nominal " & _
                "from list_BG l " & _
                "inner join ms_supplier s on s.kode_supplier=l.kode_supplier " & _
                "inner join ms_bank b on b.kode_bank=l.kode_bank " & _
                "where l.nomer_BG='" & txtNoBG.text & "' and jenis='h' and l.status_cair='1' order by l.tanggal_cair", conn
    Else
        rs.Open "Select l.nomer_BG,l.nomer_transaksi,l.tanggal_cair,l.kode_customer, " & _
                "s.nama_customer,l.kode_bank,b.nama_bank,l.nominal " & _
                "from list_BG l " & _
                "inner join ms_customer s on s.kode_customer=l.kode_customer " & _
                "inner join ms_bank b on b.kode_bank=l.kode_bank " & _
                "where l.nomer_BG='" & txtNoBG.text & "' and jenis='p' and l.status_cair='1' order by l.tanggal_cair", conn
    End If
    If Not rs.EOF Then
        txtNoTransaksi.text = rs("nomer_transaksi")
        DTPicker1.value = rs("tanggal_cair")
        If JenisBG = "hutang" Then
            txtKode.text = rs("kode_supplier")
            lblNama = rs("nama_supplier")
        Else
            txtKode.text = rs("kode_customer")
            lblNama = rs("nama_customer")
        End If
        txtKodeBank.text = rs("kode_bank")
        lblNamaBank = rs("nama_bank")
        txtNominal.text = rs("nominal")
        Cek_BG = True
    Else
        txtNoTransaksi.text = ""
        If JenisBG = "hutang" Then
            txtKode.text = ""
            lblNama = "-"
        Else
            txtKode.text = ""
            lblNama = "-"
        End If
        txtKodeBank.text = ""
        lblNamaBank = "-"
        txtNominal.text = "0"
        Cek_BG = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim Acc_Bank As String, Acc_HutangGiro As String
Dim tanggal As Date, Nominal As Double, NoJurnal As String
Dim Acc_PiutangGiroDiBank  As String

On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    tanggal = Now
    NoJurnal = Nomor_Jurnal(tanggal)
    Acc_HutangGiro = getAcc("hutang giro")
    Acc_PiutangGiroDiBank = getAcc("piutang giro di bank")
    Acc_Bank = getAccBank(txtKodeBank.text)
    Nominal = CDbl(txtNominal.text)
    
    If Jenis = "hutang" Then
        JurnalD "-", NoJurnal, Acc_Bank, "d", Nominal, "Pembatalan pencairan Giro Keluar", txtNoBG
        JurnalD "-", NoJurnal, Acc_HutangGiro, "k", Nominal, "Pembatalan pencairan Giro Keluar", txtNoBG
        JurnalH "-", NoJurnal, tanggal, "Batal Pencairan Giro Keluar"
    Else
        JurnalD "-", NoJurnal, Acc_PiutangGiroDiBank, "d", Nominal, "Pembatalan pencairan Giro Masuk", txtNoBG
        JurnalD "-", NoJurnal, Acc_Bank, "k", Nominal, "Pembatalan pencairan Giro Masuk", txtNoBG
        JurnalH "-", NoJurnal, tanggal, "Batal Pencairan Giro Masuk"
    End If
    
    conn.Execute "update list_bg set status_cair='0' " & _
                 "where nomer_transaksi='" & txtNoTransaksi.text & "' " & _
                 "and nomer_bg='" & txtNoBG.text & "' "
    
    conn.CommitTrans
    conn.Close
    
    MsgBox "Data sudah disimpan ! ", vbOKOnly
    
    DBGrid.SetFocus
    SendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtNoBG.text = ""
    txtNoTransaksi.text = ""
    txtKode.text = ""
    lblNama = ""
    txtKodeBank.text = ""
    lblNamaBank = ""
    txtNominal = "0"
    txtNoBG.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    reset_form
    If JenisBG = "hutang" Then
        Label5.Caption = "Supplier"
    Else
        Label5.Caption = "Customer"
    End If
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtNoBG_GotFocus()
    txtNoBG.SelStart = 0
    txtNoBG.SelLength = Len(txtNoBG.text)
    foc = 1
End Sub

Private Sub txtNoBG_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then
        cmdSearch_Click
    End If
End Sub

Private Sub txtNoBG_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        If Not Cek_BG Then
            MsgBox "Nomer BG  yang anda masukkan salah"
            txtNoBG.SetFocus
        End If
    End If
End Sub

Private Sub txtNoBG_LostFocus()
On Error Resume Next
    If txtNoBG.text <> "" Then
        If Not Cek_BG Then
            MsgBox "Nomer BG yang anda masukkan salah"
            txtNoBG.SetFocus
        End If
    End If
    foc = 0
End Sub


