VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmAddRequestSO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Request Order Penjualan"
   ClientHeight    =   9120
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10515
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9120
   ScaleWidth      =   10515
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5280
      Picture         =   "frmAddRequestSO.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4185
      Picture         =   "frmAddRequestSO.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3060
      Picture         =   "frmAddRequestSO.frx":0A64
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   8235
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   315
      Picture         =   "frmAddRequestSO.frx":0B66
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   8235
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4455
      Picture         =   "frmAddRequestSO.frx":0C68
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   8235
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   930
      Width           =   3885
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   375
      Left            =   3690
      Picture         =   "frmAddRequestSO.frx":0D6A
      TabIndex        =   12
      Top             =   60
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchCustomer 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      Height          =   375
      Left            =   3165
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   11
      Top             =   465
      Width           =   420
   End
   Begin VB.TextBox txtKodeCustomer 
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   510
      Width           =   1530
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1665
      Picture         =   "frmAddRequestSO.frx":0E6C
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   8235
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7275
      TabIndex        =   13
      Top             =   555
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   100794371
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6315
      Index           =   0
      Left            =   90
      TabIndex        =   19
      Top             =   1395
      Width           =   10185
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         Height          =   1890
         Left            =   90
         TabIndex        =   20
         Top             =   45
         Width           =   10005
         Begin VB.TextBox txtBerat 
            Height          =   330
            Left            =   1890
            TabIndex        =   4
            Top             =   945
            Width           =   1140
         End
         Begin VB.CommandButton cmdSearchBrg 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            Height          =   330
            Left            =   5445
            Picture         =   "frmAddRequestSO.frx":0F6E
            TabIndex        =   34
            Top             =   210
            Width           =   420
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   90
            ScaleHeight     =   405
            ScaleWidth      =   4200
            TabIndex        =   21
            Top             =   1380
            Width           =   4200
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               Height          =   375
               Left            =   2790
               TabIndex        =   7
               Top             =   15
               Width           =   1320
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               Height          =   375
               Left            =   1395
               TabIndex        =   6
               Top             =   15
               Width           =   1320
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               Height          =   375
               Left            =   0
               TabIndex        =   5
               Top             =   15
               Width           =   1305
            End
         End
         Begin VB.TextBox txtQty 
            Height          =   330
            Left            =   1890
            TabIndex        =   3
            Top             =   570
            Width           =   1140
         End
         Begin VB.TextBox txtKdBrg 
            Height          =   330
            Left            =   1890
            TabIndex        =   2
            Top             =   210
            Width           =   3480
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Berat Total"
            Height          =   240
            Left            =   180
            TabIndex        =   38
            Top             =   975
            Width           =   1020
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            Height          =   240
            Left            =   3135
            TabIndex        =   37
            Top             =   1005
            Width           =   600
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            Height          =   285
            Left            =   4410
            TabIndex        =   33
            Top             =   570
            Width           =   3480
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            Height          =   240
            Left            =   165
            TabIndex        =   24
            Top             =   255
            Width           =   1050
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            Caption         =   "Qty"
            Height          =   240
            Left            =   180
            TabIndex        =   23
            Top             =   615
            Width           =   600
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3105
            TabIndex        =   22
            Top             =   585
            Width           =   1050
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   3780
         Left            =   90
         TabIndex        =   25
         Top             =   2025
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   6668
         _Version        =   393216
         Cols            =   6
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTotal 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7380
         TabIndex        =   40
         Top             =   5895
         Width           =   1680
      End
      Begin VB.Label Label5 
         Caption         =   "Berat"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6435
         TabIndex        =   39
         Top             =   5895
         Width           =   915
      End
      Begin VB.Label Label17 
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   29
         Top             =   5895
         Width           =   735
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3465
         TabIndex        =   28
         Top             =   5895
         Width           =   1140
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   27
         Top             =   5895
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   26
         Top             =   5895
         Width           =   735
      End
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   7275
      TabIndex        =   35
      Top             =   945
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   100794371
      CurrentDate     =   38927
   End
   Begin VB.Label Label2 
      Caption         =   "Deadline"
      Height          =   240
      Left            =   5805
      TabIndex        =   36
      Top             =   990
      Width           =   1320
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      Height          =   330
      Left            =   4185
      TabIndex        =   32
      Top             =   135
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label Label7 
      Caption         =   "Customer"
      Height          =   240
      Left            =   90
      TabIndex        =   30
      Top             =   555
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   18
      Top             =   60
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      Height          =   240
      Left            =   90
      TabIndex        =   17
      Top             =   930
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      Height          =   240
      Left            =   5805
      TabIndex        =   16
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label19 
      Caption         =   "Customer"
      Height          =   240
      Left            =   90
      TabIndex        =   15
      Top             =   555
      Width           =   960
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      Height          =   240
      Left            =   90
      TabIndex        =   14
      Top             =   135
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddRequestSO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Long, Komisi As Long
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim kategori As String
Private Sub cmbGudang_Click()
    cek_kodebarang2
End Sub



Public Function newfaktur(tanggal As Date) As String
'Dim No As String
'Dim conn As New ADODB.Connection
'Dim rs As New ADODB.Recordset
'Dim query As String
'Dim start As Byte
'    conn.Open strcon
'
'    query = "select top 1 nomer_faktur from t_jualkertash where substring(nomer_faktur,1,4)='" & Format(tanggal, "yyyy") & "' and tipe='" & cmbTipeTransaksi.text & "' order by nomer_faktur desc"
'
'    rs.Open query, conn
'    If Not rs.EOF Then
'        newfaktur = Format(tanggal, "yyyy") & "/" & Format((CLng(Right(rs(0), 5)) + 1), "00000")
'    Else
'        newfaktur = Format(tanggal, "yyyy") & "/" & Format("1", "00000")
'    End If
'    rs.Close
'    conn.Close
End Function
Private Sub cmbTipeTransaksi_Click()
    If cmbTipeTransaksi.text = "DUS-BUNGKUS" Then
        kategori = "('BARANG JADI DUS','ROLL DI SARASWATI','ROLL DI KM','SHEET SUPARMA','SHEET PAPIRUS','SHEET IN ROLL BLABAK','SHEET NON COAT PURA','BARANG JADI SHEET BUNGKUS','BARANG JADI BUNGKUS','ROLL KERTAS BUNGKUS DI SARASWATI','ROLL KERTAS BUNGKUS','CETAK')"
    ElseIf cmbTipeTransaksi.text = "CD-HVS" Then
        kategori = "('BARANG JADI SHEET CD','ROLL CD','BARANG JADI FOLIO HVS','BARANG JADI FOLIO CD','ROLL HVS','BARANG JADI SHEET HVS')"
    End If
    txtNoFaktur = newfaktur(DTPicker1)
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    lblNamaBarang2 = ""
    txtQty.text = "1"
    txtBerat.text = "0"
    txtBeratCustomer.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdClear2_Click()
    txtQtySerial.text = "1"
    mode2 = 1
    cmdClear2.Enabled = False
    cmdDelete2.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.Row, 0) = ""

    Row = flxGrid.Row
    totalQty = totalQty - (flxGrid.TextMatrix(Row, 4))
    total = total - (flxGrid.TextMatrix(Row, 6))
    Komisi = Komisi - ((flxGrid.TextMatrix(Row, 7) - flxGrid.TextMatrix(Row, 5)) * flxGrid.TextMatrix(Row, 4))
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")
        lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")
        txtKomisi = Komisi

    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If flxGrid.Row > 1 Then flxGrid.Row = flxGrid.Row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.Col = 0
    flxGrid.ColSel = 6
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False

End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_requestso] from t_requestsoh where [nomer_requestso]>'" & lblNoTrans & "' order by [nomer_requestso]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text And Val(flxGrid.TextMatrix(i, 5)) = Val(txtBerat.text) Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.Row, 0) = ""
        flxGrid.Row = Row
        currentRow = flxGrid.Row

        flxGrid.TextMatrix(Row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(Row, 2) = LblNamaBarang1
        
        If flxGrid.TextMatrix(Row, 4) = "" Then
            flxGrid.TextMatrix(Row, 4) = txtQty
            flxGrid.TextMatrix(Row, 5) = txtBerat
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(Row, 4))
            total = total - (flxGrid.TextMatrix(Row, 5))

            If mode = 1 Then
                flxGrid.TextMatrix(Row, 4) = CDbl(flxGrid.TextMatrix(Row, 4)) + CDbl(txtQty)
                flxGrid.TextMatrix(Row, 5) = CDbl(flxGrid.TextMatrix(Row, 5)) + CDbl(txtBerat)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(Row, 4) = CDbl(txtQty)
                flxGrid.TextMatrix(Row, 5) = CDbl(txtBerat)
            End If
        End If
        flxGrid.TextMatrix(Row, 3) = flxGrid.TextMatrix(Row, 4)
        

        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = 5
        totalQty = totalQty + flxGrid.TextMatrix(Row, 4)
        total = total + (flxGrid.TextMatrix(Row, 5))

        txtKomisi = Komisi
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")
'        lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        loadcombo
'        SetComboTextRight txtKdBrg.text, cmbItem
'        SetComboText txtKdBrg.text, cmbSerial
        txtQtySerial = txtQty
        

        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        txtQty.text = "1"
        txtBerat.text = "0"

        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
'    cmdOk2_Click
    mode = 1
    lblItem = flxGrid.Rows - 2
End Sub


Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub



Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Qty Serial tidak sama dengan jumlah yang akan dijual"
        Exit Sub
    End If
    simpan
'    If posting_requestso(lblNoTrans) Then
'        MsgBox "Proses Posting telah berhasil"
'        reset_form
'    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
        
Private Function Cek_qty() As Boolean
Dim qtycek As Double, qtycek0 As Double
Dim Z As Integer, kode As String
    Cek_qty = True
    For Z = 1 To flxGrid.Rows - 2
        qtycek = 0
        qtycek0 = 0
        kode = flxGrid.TextMatrix(Z, 1)
        
        For i = 1 To flxGrid.Rows - 2
            If flxGrid.TextMatrix(i, 1) = kode Then qtycek0 = qtycek0 + flxGrid.TextMatrix(i, 4)
        Next i
        
        
        
        If qtycek <> qtycek0 Then Cek_qty = False
    Next Z
End Function

Private Sub cmdPrev_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_requestso] from t_requestsoh where [nomer_requestso]<'" & lblNoTrans & "' order by [nomer_requestso] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = "select * from ms_coa where fg_aktif = 1"
    frmSearch.nmform = "frmAddRequestSO"
    frmSearch.nmctrl = "txtAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_acc"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub search_acc()
On Error Resume Next
    If cek_Acc Then MySendKeys "{tab}"
End Sub

Public Function cek_Acc() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where fg_aktif = 1 and kode_acc='" & txtAcc & "' ", conn
    If Not rs.EOF Then
        LblNamaAcc = rs(1)
        cek_Acc = True
        'If rs(4) = "D" Then debet = True Else debet = False
    Else
        LblNamaAcc = ""
        lblKode = ""
        
        cek_Acc = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
On Error GoTo salah
    frmSearch.connstr = strcon
'    frmSearch.query = SearchJualKertas & " where left(nomer_jualkertas,3)='PJK' and  status_posting='0'"
    frmSearch.query = SearchrequestSO
    frmSearch.nmform = "frmAddRequestSO"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "requestsoH"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 0
    frmSearch.cmbSortby.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
Exit Sub:
salah:

End Sub

Private Sub cmdSearchCustomer_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchCustomer
    frmSearch.nmform = "frmAddRequestSO"
    frmSearch.nmctrl = "txtKodeCustomer"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_customer"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_customer"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If txtKodeCustomer.text = "" Then
        MsgBox "Silahkan masukkan customer terlebih dahulu"
        txtKodeCustomer.SetFocus
        Exit Function
    End If
    
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_requestsoh", "nomer_requestso", DTPicker1, "RSO")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_requestsoh where nomer_requestso='" & id & "'"
    conn.Execute "delete from t_requestsod where nomer_requestso='" & id & "'"
    
    add_dataheader

    For Row = 1 To flxGrid.Rows - 2
        add_datadetail (Row)
    Next
    

    conn.CommitTrans
    simpan = True
    i = 0
    DropConnection

    
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    cmdClear_Click
    cmdClear2_Click
    txtKeterangan.text = ""
    txtKodeCustomer.text = ""
    txtAcc.text = ""
    txtKomisi.text = 0
    loadcombo
    totalQty = 0
    Komisi = 0
    total = 0
    lblTotal = 0
    lblGrandTotal = 0
    lblQty = 0
    lblItem = 0
'    DTPicker1 = Now
    DTPicker2 = DTPicker1 + 30
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
'    cmbGudang.text = ""
    cmbTipeTransaksi_Click
    TabStrip1.Tabs(1).Selected = True
    txtKodeCustomer.SetFocus
    
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(6)
    ReDim nilai(6)
    table_name = "t_requestsoh"
    fields(0) = "nomer_requestso"
    fields(1) = "tanggal_requestso"
    fields(2) = "kode_customer"
    fields(3) = "tanggal_expired"
    fields(4) = "keterangan"
    fields(5) = "userid"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKodeCustomer.text
    nilai(3) = Format(DTPicker2, "yyyy/MM/dd HH:mm:ss")
    nilai(4) = txtKeterangan.text
    nilai(5) = User
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_requestsod"
    fields(0) = "nomer_requestso"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "NO_URUT"
    fields(4) = "berat"

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = Replace(Format(flxGrid.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(3) = Row
    nilai(4) = Replace(Format(flxGrid.TextMatrix(Row, 5), "###0.##"), ",", ".")
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim rs As New ADODB.Recordset, gudang As String
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select t.* from t_requestsoh t where t.nomer_requestso='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        SetComboText rs!kode_gudang, cmbGudang
        DTPicker1 = rs("tanggal_requestso")
        txtKeterangan.text = rs("keterangan")
        txtKodeCustomer.text = rs("kode_customer")
        cek_customer
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        totalQty = 0
        total = 0
'        If rs("status_posting") = 1 Then
''            cmdPrint.Enabled = False
'            cmdSimpan.Enabled = False
'            mnuSave.Enabled = False
'        Else
'            cmdSimpan.Enabled = True
'            cmdPosting.Visible = True
'            mnuSave.Enabled = True
'        End If
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1
        
        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat from t_requestsod d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_requestso='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = rs(2)
                flxGrid.TextMatrix(Row, 4) = rs(2)
                flxGrid.TextMatrix(Row, 5) = Format(rs(3), "#,##0")
                Row = Row + 1
                totalQty = totalQty + rs(2)
                total = total + (rs(3))
                
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = totalQty
        lblTotal = Format(total, "#,##0.00")
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
    
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKdBrg.SetFocus
End Sub

Private Sub loaddetil()
        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.Row, 2)
                lblNamaBarang2 = flxGrid.TextMatrix(flxGrid.Row, 3)
                End If
            txtQty.text = flxGrid.TextMatrix(flxGrid.Row, 4)
            txtBerat.text = flxGrid.TextMatrix(flxGrid.Row, 5)

            txtQty.SetFocus
        End If

End Sub
Private Sub loaddetil2()

'        If flxGrid2.TextMatrix(flxGrid2.row, 1) <> "" Then
'            mode2 = 2
'            cmdClear2.Enabled = True
'            cmdDelete2.Enabled = True
'            SetComboTextRight flxGrid2.TextMatrix(flxGrid2.row, 1), cmbItem
'            SetComboText flxGrid2.TextMatrix(flxGrid2.row, 4), cmbSerial
'            txtQtySerial.text = flxGrid2.TextMatrix(flxGrid2.row, 5)
'            txtQtySerial.SetFocus
'        End If

End Sub
Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
        loaddetil
        mode = 2
         mode2 = 2
        txtQty.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        loaddetil
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub




Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    load_combo2
    'loadcombobox cmbGudang, "select kode_gudang from ms_gudang where (kode_gudang not like '%CETAK%' and kode_gudang not like '%PLONG%' and kode_gudang not like '%POTONG%') "
    reset_form
    total = 0
    
    DTPicker1 = Now
    DTPicker2 = DTPicker1 + 30
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 1200
    flxGrid.ColWidth(4) = 0
    flxGrid.ColWidth(5) = 1200


    
    
    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 4) = "Dimensi"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 5) = "Berat"
    
    
End Sub
Private Sub loadcombo()
'Dim kode As String
'    kode = Trim(Right(cmbItem.text, 50))
'    cmbItem.Clear
'    For i = 1 To flxGrid.Rows - 2
'        cmbItem.AddItem flxGrid.TextMatrix(i, 2) & "(" & flxGrid.TextMatrix(i, 3) & ")" & Space(100) & flxGrid.TextMatrix(i, 1)
'    Next
'    SetComboTextRight kode, cmbItem
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    cek_kodebarang1 = False
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        cek_kodebarang1 = True
        LblNamaBarang1 = rs!nama_bahan
        lblNamaBarang2 = rs!panjang & "x" & rs!lebar & "x" & rs!berat
        
        lblSatuan = ""
        cek_kodebarang1 = True
        If lblKategori <> "" Then
        txtBerat.text = IIf(IsNull(rs("harga_jual" & lblKategori)), 0, rs("harga_jual" & lblKategori))
        Else
        txtBerat.text = rs!harga_juala
        End If
    Else
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close

ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_kodebarang2() As Boolean

End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_customer()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_customer],kategori_harga from ms_customer where kode_customer='" & txtKodeCustomer & "'", conn
    If Not rs.EOF Then
        
        lblKategori = rs(1)
    Else
        
        lblKategori = ""
    End If
    rs.Close
    
'    If lblKategori <> "" Then
'        If flxGrid.Rows > 2 Then hitung_ulang
'    End If
err:
    conn.Close
End Sub
Private Sub hitung_ulang()
On Error GoTo err
    conn.Open strcon
    With flxGrid
    For i = 1 To .Rows - 2
        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
        If Not rs.EOF Then
            total = total - .TextMatrix(i, 6)
            .TextMatrix(i, 5) = rs("harga_jual" & lblKategori)
            .TextMatrix(i, 6) = .TextMatrix(i, 4) * .TextMatrix(i, 5)
            total = total + .TextMatrix(i, 6)
            
        End If
        rs.Close
    Next
    End With
    conn.Close
    lblTotal = Format(total, "#,##0")
    lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")
    Exit Sub
err:
    MsgBox err.Description
    If conn.State Then conn.Close
End Sub


Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub TabStrip1_Click()
    For i = 0 To frDetail.UBound
        frDetail(i).Visible = False
    Next
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    If (TabStrip1.SelectedItem.Index - 1) = 1 Then loadcombo
        
End Sub

Private Sub txtAcc_GotFocus()
    txtAcc.SelStart = 0
    txtAcc.SelLength = Len(txtAcc.text)
'    foc = 1
End Sub

Private Sub txtAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
'        If txtAcc.text = "" Then
'            flxGrid_Click
'        Else
        If txtAcc.text = "" Then KeyCode = 0
        
        If txtAcc.text <> "" Then
            If Not cek_Acc Then
                MsgBox "Kode yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    End If
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
End Sub

Private Sub txtBeratCustomer_GotFocus()
    txtBeratCustomer.SelStart = 0
    txtBeratCustomer.SelLength = Len(txtBeratCustomer.text)
End Sub

Private Sub txtBeratCustomer_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBeratCustomer_LostFocus()
    If Not IsNumeric(txtBeratCustomer.text) Then txtBeratCustomer.text = "0"
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodecustomer_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchCustomer_Click
End Sub

Private Sub txtKodecustomer_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_customer
End Sub

Private Sub txtKodecustomer_LostFocus()
    cek_customer
End Sub

Private Sub txtKomisi_GotFocus()
    txtKomisi.SelStart = 0
    txtKomisi.SelLength = Len(txtKomisi.text)
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Private Sub txtQtySerial_GotFocus()
    txtQtySerial.SelStart = 0
    txtQtySerial.SelLength = Len(txtQtySerial.text)
End Sub

Private Sub txtQtySerial_KeyPress(KeyAscii As Integer)
    Angka (KeyAscii)
End Sub

Private Sub load_combo2()
    
End Sub

