VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddAdjustment 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pengambilan Stock"
   ClientHeight    =   9090
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8940
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9090
   ScaleWidth      =   8940
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6975
      TabIndex        =   33
      Top             =   885
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save Excel "
      Height          =   330
      Left            =   7335
      TabIndex        =   31
      Top             =   1845
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel "
      Height          =   330
      Left            =   7335
      TabIndex        =   30
      Top             =   1395
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1620
      TabIndex        =   23
      Top             =   135
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1590
      Left            =   135
      TabIndex        =   14
      Top             =   1350
      Width           =   6990
      Begin VB.CommandButton cmdSearchBrg 
         Caption         =   "F3"
         Height          =   330
         Left            =   2835
         Picture         =   "frmAddAdjustment.frx":0000
         TabIndex        =   15
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2790
         TabIndex        =   6
         Top             =   1080
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1665
         TabIndex        =   5
         Top             =   1080
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   1350
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   3
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   2
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   270
         TabIndex        =   21
         Top             =   1260
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   20
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   19
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   18
         Top             =   705
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3240
         TabIndex        =   17
         Top             =   270
         Width           =   3525
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   330
         Left            =   1260
         TabIndex        =   16
         Top             =   720
         Width           =   600
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   645
      Left            =   450
      Picture         =   "frmAddAdjustment.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   8280
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   330
      Left            =   3855
      Picture         =   "frmAddAdjustment.frx":0204
      TabIndex        =   13
      Top             =   165
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   1
      Top             =   975
      Width           =   3885
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      Height          =   645
      Left            =   5235
      Picture         =   "frmAddAdjustment.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   8280
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      Height          =   645
      Left            =   2475
      Picture         =   "frmAddAdjustment.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   8280
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   0
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   60162051
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   4245
      Left            =   135
      TabIndex        =   22
      Top             =   3060
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   7488
      _Version        =   393216
      Cols            =   4
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8460
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   32
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5625
      TabIndex        =   29
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7155
      TabIndex        =   28
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label16 
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3645
      TabIndex        =   27
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4590
      TabIndex        =   26
      Top             =   7515
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7065
      TabIndex        =   25
      Top             =   7515
      Width           =   1140
   End
   Begin VB.Label Label17 
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   24
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   11
      Top             =   1005
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   10
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddAdjustment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select [kode],[nama barang],  PriceList,Harga1,Harga2,h_eceran from ms_bahan where flag='1'"
Const queryTransfer As String = "select ID,Tanggal,Keterangan,Gudang from t_adjustmenth"
Dim total As Currency
Dim NoTrans2 As String
Dim mode As Byte
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Dim foc As Byte
Dim colname() As String
Dim NoJurnal As String
Dim totalHPP As Double


Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from t_adjustmenth where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "PS" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "PS" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub chkNumbering_Click()
    If chkNumbering.Value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""

    LblNamaBarang = ""
    txtQty.text = "1"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
    lblQty = totalQty
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    txtKdBrg.text = ""

    txtQty.text = "1"
    LblNamaBarang = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub saveexcelfile(filename As String)
On Error GoTo err
Dim exapp As New Excel.Application
Dim exwb As Excel.Workbook
Dim exws As Excel.Worksheet
Dim range As String
Dim col As Byte
Dim i As Integer
'Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Add
Set exws = exwb.Sheets("Sheet1")

col = Asc("A")
range = Chr(col) & "1"

'While exapp.Workbooks.Application.range(range) <> ""
'    ReDim Preserve colname(col - 65)
'    colname(col - 65) = exws.range(range)
'    col = col + 1
'    range = Chr(col) & "1"
'Wend
exws.range(Chr(0 + 65) & "1") = "H"
exws.range(Chr(1 + 65) & "1") = "KodeBarang"
exws.range(Chr(3 + 65) & "1") = "Nama"
exws.range(Chr(2 + 65) & "1") = "Kategori"
exws.range(Chr(4 + 65) & "1") = "HargaPokok"
exws.range(Chr(5 + 65) & "1") = "HargaJual"
exws.range(Chr(6 + 65) & "1") = "Disc1"
exws.range(Chr(7 + 65) & "1") = "Disc2"
exws.range(Chr(8 + 65) & "1") = "Disc3"
exws.range(Chr(9 + 65) & "1") = "Disc4"
exws.range(Chr(10 + 65) & "1") = "Disc5"
exws.range(Chr(11 + 65) & "1") = "Stok"
i = 2
Dim hargajual As Long
Dim hargapokok As Double
Dim kategori As String
Dim disc1, disc2, disc3, disc4, disc5 As Double
conn.Open strcon
    While i < flxGrid.Rows
        disc1 = 0
        disc2 = 0
        disc3 = 0
        disc4 = 0
        disc5 = 0
        rs.Open "select m.*,h.hpp from ms_bahan m inner join hpp h on m.[kode]=h.[kode] where m.[kode]='" & flxGrid.TextMatrix(i - 1, 1) & "' and h.gudang='" & Gudang & "'", conn
        If Not rs.EOF Then
            hargajual = rs("h_eceran")
            hargapokok = rs("hpp")
            kategori = rs("kategori")
            
        Else
            hargajual = 0
            hargapokok = 0
            kategori = ""
        End If
        rs.Close
        exws.range(Chr(0 + 65) & CStr(i)) = "D"
        exws.range(Chr(1 + 65) & CStr(i)) = "'" & flxGrid.TextMatrix(i - 1, 1)
        exws.range(Chr(3 + 65) & CStr(i)) = flxGrid.TextMatrix(i - 1, 2)
        exws.range(Chr(2 + 65) & CStr(i)) = kategori
        exws.range(Chr(4 + 65) & CStr(i)) = hargapokok
        
        exws.range(Chr(9 + 65) & CStr(i)) = flxGrid.TextMatrix(i - 1, 3)
        exws.range(Chr(5 + 65) & CStr(i)) = PriceList
        exws.range(Chr(6 + 65) & CStr(i)) = harga1
        exws.range(Chr(7 + 65) & CStr(i)) = harga2
        exws.range(Chr(8 + 65) & CStr(i)) = h_eceran
        i = i + 1
    Wend
    conn.Close
err:
exwb.SaveAs filename
exwb.Close
exapp.Application.Quit
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
If conn.State Then conn.Close
End Sub
Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim exapp As Object
Dim exwb As Object
Dim exws As Object
Dim range As String
Dim col As Byte
Dim i As Integer
Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Open(filename)
Set exws = exwb.Sheets("Sheet1")

col = Asc("A")
range = Chr(col) & "1"

While exapp.Workbooks.Application.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exws.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

i = 2
    While exapp.range("A" & CStr(i)) <> ""
        txtKdBrg.text = exws.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i))
        If Not cek_kodebarang1 Then
            LblNamaBarang = exws.range(Chr(getcolindex("Nama", colname) + 65) & CStr(i))
        End If
        txtQty.text = exws.range(Chr(getcolindex("stok", colname) + 65) & CStr(i))
        cmdOK_Click
        flxGrid.TextMatrix(flxGrid.row, 1) = exws.range(Chr(getcolindex("KodeBarang", colname) + 65) & CStr(i))
        flxGrid.TextMatrix(flxGrid.row, 2) = exws.range(Chr(getcolindex("Nama", colname) + 65) & CStr(i))
        flxGrid.TextMatrix(flxGrid.row, 3) = exws.range(Chr(getcolindex("Stok", colname) + 65) & CStr(i))
        i = i + 1
    Wend
err:
exwb.Close
exapp.Application.Quit
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKdBrg.text) Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = lblKode
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 3))

            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 3
        totalQty = totalQty + flxGrid.TextMatrix(row, 3)
        lblQty = Format(totalQty, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""

        LblNamaBarang = ""
        txtQty.text = "1"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.Rows - 2
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdSave_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowSave
    If CommonDialog1.filename <> "" Then
        saveexcelfile CommonDialog1.filename
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddadjustment"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub



Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = queryTransfer
    frmSearch.nmform = "frmAddadjustment"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan Barang yang akan diambil terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
        NoJurnal = Nomor_Jurnal(DTPicker1)
    Else
        conn.Execute "delete from t_adjustmenth where ID='" & lblNoTrans & "'"
        rs.Open "select * from t_adjustmentd where ID='" & lblNoTrans & "'", conn
        While Not rs.EOF
            updatestock Gudang, rs("kode"), rs("qty"), conn
            rs.MoveNext
        Wend
        rs.Close
        conn.Execute "delete from t_adjustmentd where ID='" & lblNoTrans & "'"
        conn.Execute "delete from hst_hpp where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
        conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
    End If
    If chkNumbering = "0" Then lblNoTrans = txtID.text
    add_dataheader
    totalHPP = 0
    For row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(row, 1) <> "" Then
            add_datadetail (row)
            add_jurnaldetail row
            
            JumlahLama = getStock2(flxGrid.TextMatrix(row, 1), Gudang, strcon)

            HPPLama = GetHPP(flxGrid.TextMatrix(row, 1), strcon)
            
            jumlah = flxGrid.TextMatrix(row, 3)
            
            updatestock Gudang, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3) * -1, conn
            conn.Execute "insert into hst_hpp  (jenis,gudang,id,tanggal,kode,stockawal,HPPawal,Qty,Harga,HPP,StockAkhir)  " & _
                        "values ('Pengambilan','" & Gudang & "','" & lblNoTrans & " ','" & Format(Now, "yyyy/MM/dd hh:mm:ss") & "','" & flxGrid.TextMatrix(row, 1) & "'," & JumlahLama & ",0," & -jumlah & ",0," & Replace(HPPLama, ",", ".") & "," & JumlahLama - jumlah & " )"
           
        End If
    Next
    
    add_jurnal
    
    conn.CommitTrans
    
    
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    
    Call MySendKeys("{tab}")
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    If conn.State Then conn.Close
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    txtKeterangan.text = ""
    loadcombo
    totalQty = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "t_adjustmenth"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "gudang"
    fields(3) = "keterangan"
    fields(4) = "pk"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = Gudang
    nilai(3) = txtKeterangan.text
    nilai(4) = NoTrans2
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "t_adjustmentd"
    fields(0) = "ID"
    fields(1) = "[kode]"
    fields(2) = "qty"
    fields(3) = "URUT"
    fields(4) = "hpp"
    
    nilai(0) = NoTrans2
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = row
    nilai(4) = GetHPP(flxGrid.TextMatrix(row, 1), strcon)
    totalHPP = totalHPP + (CDbl(nilai(4)) * CDbl(nilai(2)))
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_jurnal()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim Acc_BiayaLain As String
    Dim Acc_Persediaan As String

    ReDim fields(7)
    ReDim nilai(7)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "totaldebet"
    fields(5) = "totalkredit"
    fields(6) = "tipe"

    nilai(0) = NoTrans2
    nilai(1) = NoJurnal
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = "Pemakaian"
    nilai(4) = CLng(lblTotal)
    nilai(5) = CLng(lblTotal)
    nilai(6) = "PS"

    tambah_data table_name, fields, nilai


    ReDim fields(4)
    ReDim nilai(4)

    Acc_BiayaLain = getAcc("biayalain")
    Acc_Persediaan = getAcc("persediaan")

    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "urut"


    nilai(0) = NoTrans2
    nilai(1) = Acc_BiayaLain
    nilai(2) = totalHPP
    nilai(3) = 1

    tambah_data table_name, fields, nilai
    
End Sub

Private Sub add_jurnaldetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim Acc_Persediaan As String
    Dim hpp As Double
    
    ReDim fields(4)
    ReDim nilai(4)
    
    rs.Open "Select kode_acc from ms_bahan  " & _
            "where kode='" & flxGrid.TextMatrix(row, 1) & "'", conn
    If Not rs.EOF Then
        Acc_Persediaan = rs("kode_acc")
    End If
    rs.Close
    
    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "kredit"
    fields(3) = "urut"
    
    nilai(0) = NoTrans2
    nilai(1) = Acc_Persediaan
    hpp = GetHPP(flxGrid.TextMatrix(row, 1), strcon)
    nilai(2) = flxGrid.TextMatrix(row, 3) * hpp
    nilai(3) = 1 + row
    
    
    tambah_data table_name, fields, nilai
End Sub


Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    
    conn.ConnectionString = strcon

    cmdPrint.Visible = False
    conn.Open

    rs.Open "select * from t_adjustmenth where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        NoTrans2 = rs("pk")
        lblGudang = rs("gudang")
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        totalQty = 0
        total = 0
        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.[kode],m.[nama barang], qty from t_adjustmentd d inner join ms_bahan m on d.[kode]=m.[kode] where d.id='" & lblNoTrans & "' order by urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
                
                row = row + 1
                totalQty = totalQty + rs(2)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty

        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then

    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            cek_kodebarang1
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
            txtQty.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
    Call MySendKeys("{tab}")
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    reset_form
    total = 0
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    lblGudang = Gudang
    'End If
    'rs.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 900
    
    
    flxGrid.TextMatrix(0, 1) = "kode"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.TextMatrix(0, 3) = "Qty"
    


End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_kodebarang1() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select [nama barang],[kode] from ms_bahan where [kode]='" & txtKdBrg & "' ", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(0)
        
        lblKode = rs(1)
        cek_kodebarang1 = True
    Else
        LblNamaBarang = ""
        lblKode = ""
        cek_kodebarang1 = False
    End If
    rs.Close
    conn.Close
End Function

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
'        If txtKdBrg.text = "" Then
'            flxGrid_Click
'        Else
        If txtKdBrg.text = "" Then KeyCode = 0
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If
        If txtKdBrg.text <> "" Then
            If Not cek_kodebarang1 Then
                MsgBox "Kode yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" And Not cek_kodebarang1 Then
        MsgBox "Kode yang anda masukkan salah"
        txtKdBrg.SetFocus
    End If
    foc = 0
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Private Sub UpDown1_Change()

End Sub
