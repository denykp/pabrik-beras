VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmHPP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Posting Harian"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10440
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   10440
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtNoTrans 
      Height          =   330
      Left            =   1215
      TabIndex        =   19
      Top             =   540
      Width           =   1500
   End
   Begin VB.ComboBox cmbTipe 
      Height          =   315
      ItemData        =   "frmHPP.frx":0000
      Left            =   1215
      List            =   "frmHPP.frx":0016
      TabIndex        =   16
      Top             =   945
      Width           =   1275
   End
   Begin VB.CommandButton cmd_searchhp 
      Caption         =   "..."
      Height          =   330
      Left            =   2115
      Picture         =   "frmHPP.frx":004A
      TabIndex        =   15
      Top             =   6705
      Width           =   375
   End
   Begin VB.TextBox txtSearchHarga 
      Height          =   330
      Left            =   5760
      TabIndex        =   14
      Top             =   1035
      Width           =   1500
   End
   Begin VB.ComboBox cmbSign 
      Height          =   315
      ItemData        =   "frmHPP.frx":014C
      Left            =   5040
      List            =   "frmHPP.frx":0159
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   1035
      Width           =   690
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Ubah"
      Height          =   330
      Left            =   2565
      TabIndex        =   11
      Top             =   6705
      Width           =   825
   End
   Begin VB.TextBox txtHarga 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   180
      TabIndex        =   10
      Text            =   "0"
      Top             =   6705
      Width           =   1860
   End
   Begin VB.ComboBox cmbGudang 
      Height          =   315
      Left            =   5670
      TabIndex        =   2
      Top             =   630
      Width           =   1050
   End
   Begin VB.CommandButton cmdSearchBrg 
      Caption         =   "..."
      Height          =   330
      Left            =   2835
      Picture         =   "frmHPP.frx":0166
      TabIndex        =   1
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKdBrg 
      Height          =   330
      Left            =   1215
      TabIndex        =   0
      Top             =   180
      Width           =   1500
   End
   Begin VB.CommandButton cmdApprove 
      Caption         =   "&Refresh"
      Height          =   330
      Left            =   2610
      Picture         =   "frmHPP.frx":0268
      TabIndex        =   3
      Top             =   1035
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   4320
      Picture         =   "frmHPP.frx":06AA
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6885
      Width           =   1230
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9405
      Top             =   225
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSDataGridLib.DataGrid DBGrid 
      Height          =   5010
      Left            =   135
      TabIndex        =   4
      Top             =   1530
      Width           =   10050
      _ExtentX        =   17727
      _ExtentY        =   8837
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   7965
      Top             =   1080
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   582
      ConnectMode     =   1
      CursorLocation  =   2
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   1
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "No Trans"
      Height          =   240
      Left            =   135
      TabIndex        =   20
      Top             =   585
      Width           =   735
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1080
      TabIndex        =   18
      Top             =   990
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe"
      Height          =   240
      Left            =   135
      TabIndex        =   17
      Top             =   990
      Width           =   690
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga"
      Height          =   285
      Left            =   4185
      TabIndex        =   13
      Top             =   1080
      Width           =   780
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      Height          =   240
      Left            =   4185
      TabIndex        =   9
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   5535
      TabIndex        =   8
      Top             =   675
      Width           =   105
   End
   Begin VB.Label LblNamaBarang 
      BackStyle       =   0  'Transparent
      Caption         =   "nama barang"
      Height          =   285
      Left            =   3375
      TabIndex        =   7
      Top             =   180
      Visible         =   0   'False
      Width           =   3435
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   225
      Width           =   1050
   End
End
Attribute VB_Name = "frmHPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select distinct kode_barang,nama_barang, merk, kode_jenis, satuan from v_ms_barang"
Public Sub cek_kodebarang1()
On Error Resume Next
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select nama_barang from ms_barang where kode_barang='" & txtKdBrg.text & "'", conn
    If Not rs.EOF Then
        
        LblNamaBarang = rs(0)
        Dim rs1 As New ADODB.Recordset
    Else
        LblNamaBarang = ""
    End If
    rs.Close
    conn.Close
End Sub


Private Sub cmd_searchhp_Click()
    frmSearch.query = "select distinct tanggal,tipe,no_trans,hpp as hpp from hpp_fifo where kode_barang='" & DBGrid.Columns(3).text & "' "
    frmSearch.nmform = "frmAddHargaOpname"
    frmSearch.nmctrl = "txtHarga"
    frmSearch.connstr = strcon
    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show
End Sub

Private Sub cmdApprove_Click()
Dim where As String
    Adodc1.ConnectionString = strcon
    If txtKdBrg.text <> "" Then
        If where <> "" Then where = where & " and "
        where = where & " h.kode_barang='" & txtKdBrg.text & "'"
    End If
    If txtNoTrans.text <> "" Then
        If where <> "" Then where = where & " and "
        where = where & " h.no_trans='" & txtNoTrans.text & "'"
    End If
    If cmbGudang.text <> "" Then
        If where <> "" Then where = where & " and "
        where = where & " gudang='" & cmbGudang.text & "'"
    End If
    If txtSearchHarga.text <> "" Then
        If where <> "" Then where = where & " and "
        where = where & " harga_beli" & cmbSign.text & "'" & txtSearchHarga.text & "'"
    End If
    If cmbTipe.text <> "" Then
        If where <> "" Then where = where & " and "
        where = where & " tipe='" & Left(cmbTipe.text, 1) & "'"
    End If
    If where <> "" Then where = " where " & where
    loadgrid "select tanggal,tipe,no_trans,h.kode_barang,m.nama_barang,harga_beli,qty,qtyjual,qtyreturbeli,qtyadjustment,qtytransfer,qtysisa,gudang from hpp_fifo h inner join " & dbname1 & ".dbo.ms_barang m on h.kode_barang=m.kode_barang " & where & " order by tanggal desc,no_trans desc"
End Sub

Private Sub cmdKeluar_Click()
Unload Me
End Sub

Private Sub cmdSave_Click()
    ubah_hpp txtHarga.text, DBGrid.Columns(2).text, DBGrid.Columns(1).text, DBGrid.Columns(3).text
    cmdApprove_Click
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg & " where flag='1'"
    frmSearch.nmform = "frmHPP"
    frmSearch.nmctrl = "txtkdbrg"
    frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.Show
End Sub

Private Sub DBGrid_Click()
On Error Resume Next
    txtHarga.text = DBGrid.Columns(7).text
End Sub

Private Sub DBGrid_DblClick()
    Me.Enabled = False
    frmHPPManual.tipe = DBGrid.Columns(1).text
    frmHPPManual.no_trans = DBGrid.Columns(2).text
    frmHPPManual.kode_barang = DBGrid.Columns(3).text
    frmHPPManual.warna = DBGrid.Columns(5).text
    frmHPPManual.ukuran = DBGrid.Columns(6).text
    frmHPPManual.Show
End Sub

Private Sub DBGrid_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    'txtHarga.text = DBGrid.Columns(7).text
End Sub

Private Sub Form_Load()
    conn.ConnectionString = strcon
    conn.Open
    cmbSign.ListIndex = 0
    cmbGudang.Clear
    cmbGudang.AddItem ""
    rs.Open "select * from ms_gudang order by kode_gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        ubah_hpp txtHarga.text, DBGrid.Columns(2).text, DBGrid.Columns(1).text, DBGrid.Columns(3).text
        cmdApprove_Click
    End If
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cek_kodebarang1
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    cek_kodebarang1
End Sub

Private Sub loadgrid(query As String)
Dim row, i As Long
Dim isi As String
Dim panjang As Byte
    Adodc1.RecordSource = ""
    row = 1
    Adodc1.RecordSource = query
    Set DBGrid.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid.Refresh
    
    For i = 0 To Adodc1.Recordset.fields.Count - 1
    If Adodc1.Recordset(i).DefinedSize > 50 Then
        panjang = 50
    Else
        panjang = Adodc1.Recordset(i).DefinedSize
    End If
    
    Next
    
End Sub

