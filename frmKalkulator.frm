VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmKalkulator 
   Caption         =   "Kalkulator Harga"
   ClientHeight    =   6270
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10020
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6270
   ScaleWidth      =   10020
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5460
      Index           =   3
      Left            =   270
      TabIndex        =   56
      Top             =   585
      Visible         =   0   'False
      Width           =   8970
      Begin VB.TextBox txtBungaTahun 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   58
         Text            =   "360"
         Top             =   630
         Width           =   930
      End
      Begin VB.TextBox txtBungaHarga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2700
         TabIndex        =   60
         Text            =   "0"
         Top             =   1395
         Width           =   930
      End
      Begin VB.TextBox txtBungaQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   59
         Text            =   "0"
         Top             =   1395
         Width           =   930
      End
      Begin VB.CommandButton cmdBungaHitung 
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   63
         Top             =   1845
         Width           =   780
      End
      Begin VB.TextBox txtBungaHari 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   62
         Text            =   "0"
         Top             =   1800
         Width           =   930
      End
      Begin VB.TextBox txtBungaJumlahUang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3960
         TabIndex        =   61
         Text            =   "0"
         Top             =   1395
         Width           =   2145
      End
      Begin VB.TextBox txtBungaBank 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   57
         Text            =   "9"
         Top             =   225
         Width           =   930
      End
      Begin VB.Label lblBungaKg 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1665
         TabIndex        =   74
         Top             =   2700
         Width           =   1410
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bunga /Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   23
         Left            =   225
         TabIndex        =   73
         Top             =   2655
         Width           =   870
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Jml Hari/Thn"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   22
         Left            =   225
         TabIndex        =   72
         Top             =   690
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "="
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   21
         Left            =   3735
         TabIndex        =   71
         Top             =   1440
         Width           =   135
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   17
         Left            =   2700
         TabIndex        =   70
         Top             =   1035
         Width           =   510
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Berat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   16
         Left            =   1620
         TabIndex        =   69
         Top             =   1035
         Width           =   450
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Bunga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   15
         Left            =   225
         TabIndex        =   68
         Top             =   2295
         Width           =   1020
      End
      Begin VB.Label lblBungaTotal 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1665
         TabIndex        =   67
         Top             =   2340
         Width           =   1410
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah Hari"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   20
         Left            =   225
         TabIndex        =   66
         Top             =   1860
         Width           =   1005
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah Uang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   19
         Left            =   225
         TabIndex        =   65
         Top             =   1455
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bunga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   18
         Left            =   225
         TabIndex        =   64
         Top             =   285
         Width           =   525
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5460
      Index           =   2
      Left            =   225
      TabIndex        =   38
      Top             =   495
      Visible         =   0   'False
      Width           =   8970
      Begin VB.TextBox txtcalc3pctfinal 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4185
         TabIndex        =   21
         Text            =   "0"
         Top             =   4590
         Width           =   750
      End
      Begin VB.TextBox txtcalc3KgFinal 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6525
         TabIndex        =   22
         Text            =   "0"
         Top             =   4590
         Width           =   660
      End
      Begin VB.TextBox txtcalc3NamaBarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   17
         Top             =   225
         Width           =   2505
      End
      Begin VB.TextBox txtcalc3Pct 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   18
         Top             =   630
         Width           =   930
      End
      Begin VB.TextBox txtcalc3KG 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   19
         Top             =   1035
         Width           =   930
      End
      Begin VB.CommandButton cmdAdd3 
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   20
         Top             =   1080
         Width           =   780
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid3 
         Height          =   2445
         Left            =   225
         TabIndex        =   39
         Top             =   1530
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   4313
         _Version        =   393216
         Cols            =   5
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7245
         TabIndex        =   55
         Top             =   4050
         Width           =   330
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7245
         TabIndex        =   54
         Top             =   4635
         Width           =   330
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5040
         TabIndex        =   53
         Top             =   4050
         Width           =   330
      End
      Begin VB.Label Labl2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   7290
         TabIndex        =   52
         Top             =   5085
         Width           =   210
      End
      Begin VB.Label lblCalc3BeratHasil 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "/"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7650
         TabIndex        =   51
         Top             =   5085
         Width           =   75
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   14
         Left            =   5445
         TabIndex        =   50
         Top             =   5085
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Menjadi"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   13
         Left            =   2880
         TabIndex        =   49
         Top             =   4650
         Width           =   810
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dari"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   5445
         TabIndex        =   48
         Top             =   4650
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   225
         TabIndex        =   47
         Top             =   285
         Width           =   1155
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Persentase"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   225
         TabIndex        =   46
         Top             =   690
         Width           =   945
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   225
         TabIndex        =   45
         Top             =   1095
         Width           =   210
      End
      Begin VB.Label lblCalc3Kekurangan 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6435
         TabIndex        =   44
         Top             =   5085
         Width           =   735
      End
      Begin VB.Label lblCalc3TotalKg 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3060
         TabIndex        =   43
         Top             =   4050
         Width           =   1815
      End
      Begin VB.Label lblCalc3Total 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5355
         TabIndex        =   42
         Top             =   4050
         Width           =   1815
      End
      Begin VB.Label lblCalc3Pct 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4185
         TabIndex        =   41
         Top             =   4320
         Width           =   690
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Persentase :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2880
         TabIndex        =   40
         Top             =   4320
         Width           =   1230
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5460
      Index           =   1
      Left            =   270
      TabIndex        =   15
      Top             =   495
      Visible         =   0   'False
      Width           =   8970
      Begin VB.CommandButton cmdAdd2 
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   26
         Top             =   1080
         Width           =   780
      End
      Begin VB.TextBox txtcalc2harga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   25
         Top             =   1035
         Width           =   1110
      End
      Begin VB.TextBox txtcalc2pctbarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   24
         Top             =   630
         Width           =   930
      End
      Begin VB.TextBox txtcalc2namabarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   23
         Top             =   225
         Width           =   2505
      End
      Begin VB.TextBox txtCalc2Kemasan 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3330
         TabIndex        =   16
         Text            =   "0"
         Top             =   4860
         Width           =   1650
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid2 
         Height          =   2445
         Left            =   225
         TabIndex        =   27
         Top             =   1530
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   4313
         _Version        =   393216
         Cols            =   5
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Harga per Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3060
         TabIndex        =   36
         Top             =   4455
         Width           =   1815
      End
      Begin VB.Label lblCalc2Gross 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5355
         TabIndex        =   35
         Top             =   4500
         Width           =   1815
      End
      Begin VB.Label lblCalc2Total 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5355
         TabIndex        =   34
         Top             =   4050
         Width           =   1815
      End
      Begin VB.Label lblCalc2Pct 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3060
         TabIndex        =   33
         Top             =   4050
         Width           =   1815
      End
      Begin VB.Label lblCalc2HPP 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5940
         TabIndex        =   32
         Top             =   4905
         Width           =   1230
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   225
         TabIndex        =   31
         Top             =   1095
         Width           =   510
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Persentase"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   225
         TabIndex        =   30
         Top             =   690
         Width           =   945
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   6
         Left            =   225
         TabIndex        =   29
         Top             =   285
         Width           =   1155
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Kemasan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   1980
         TabIndex        =   28
         Top             =   4905
         Width           =   780
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5460
      Index           =   0
      Left            =   315
      TabIndex        =   6
      Top             =   495
      Width           =   8970
      Begin VB.TextBox txtcalc1hargakg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   0
         Top             =   180
         Width           =   1650
      End
      Begin VB.TextBox txtcalc1NamaBarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   1
         Top             =   720
         Width           =   2505
      End
      Begin VB.TextBox txtcalc1PctBarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   2
         Top             =   1125
         Width           =   930
      End
      Begin VB.TextBox txtcalc1Harga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   3
         Top             =   1530
         Width           =   1110
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "Ok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   4
         Top             =   1575
         Width           =   780
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   225
         TabIndex        =   5
         Top             =   4590
         Width           =   1470
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid1 
         Height          =   2445
         Left            =   225
         TabIndex        =   7
         Top             =   2025
         Width           =   7395
         _ExtentX        =   13044
         _ExtentY        =   4313
         _Version        =   393216
         Cols            =   5
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Harga per Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   225
         TabIndex        =   14
         Top             =   240
         Width           =   1125
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   225
         TabIndex        =   13
         Top             =   780
         Width           =   1155
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Persentase"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   12
         Top             =   1185
         Width           =   945
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   225
         TabIndex        =   11
         Top             =   1590
         Width           =   510
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Harga per Kg"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   270
         TabIndex        =   10
         Top             =   5085
         Width           =   1125
      End
      Begin VB.Label lblCalc1pct 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1800
         TabIndex        =   9
         Top             =   4635
         Width           =   1815
      End
      Begin VB.Label lblCalc1Harga 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1800
         TabIndex        =   8
         Top             =   5085
         Width           =   1815
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   6225
      Left            =   90
      TabIndex        =   37
      Top             =   90
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   10980
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   4
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "1"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "2"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "3"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Bunga Bank"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmKalkulator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim calc1pct, calc1jumlah As Double
Dim calc2pct, calc2jumlah As Double
Dim calc3Kg, calc3Total As Double
Dim seltab As Byte
Private Sub cmdAdd_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    With flxGrid1
        If txtcalc1NamaBarang.text <> "" Then
            
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 1) = txtcalc1NamaBarang.text Then row = i
            Next
            If row = 0 Then
                row = .Rows - 1
                .Rows = .Rows + 1
            Else
                calc1pct = calc1pct - (.TextMatrix(row, 2))
                calc1jumlah = calc1jumlah - (.TextMatrix(row, 4))
            End If
            .TextMatrix(.row, 0) = ""
            .row = row
            currentRow = .row
            
            .TextMatrix(row, 1) = txtcalc1NamaBarang.text
            .TextMatrix(row, 2) = txtcalc1PctBarang
            .TextMatrix(row, 3) = txtcalc1Harga
            .TextMatrix(row, 4) = txtcalc1PctBarang * txtcalc1Harga
            
            
            calc1pct = calc1pct + (.TextMatrix(row, 2))
            calc1jumlah = calc1jumlah + (.TextMatrix(row, 4))
    
            If row > 8 Then
                .TopRow = row - 7
            Else
                .TopRow = 1
            End If
            txtcalc1NamaBarang = ""
            txtcalc1Harga = ""
            txtcalc1PctBarang = ""
            txtcalc1NamaBarang.SetFocus
            hitungulang
            
        End If
    End With
    mode = 1
    
End Sub

Private Sub hitungulang()
On Error Resume Next
    lblCalc1pct = 1 - calc1pct
    lblCalc1Harga = Format((txtcalc1hargakg - calc1jumlah) / (1 - calc1pct), "#,##0.00")
    
End Sub
Private Sub hitungulang2()
On Error Resume Next
    lblCalc2Pct = calc2pct
    lblCalc2Total = calc2jumlah
    lblCalc2Gross = Format(calc2jumlah / calc2pct, "#,##0.00")
    lblCalc2HPP = Format((calc2jumlah / calc2pct) + txtCalc2Kemasan, "#,##0.00")
End Sub
Private Sub hitungulang3()
On Error Resume Next
    lblCalc3TotalKg = calc3Kg
    lblCalc3Total = calc3Total
    lblCalc3Pct = Format((calc3Total / calc3Kg) * 100, "#0.##")
    
    lblCalc3Kekurangan = (((txtcalc3pctfinal / 100) * txtcalc3KgFinal) - ((lblCalc3Pct / 100) * txtcalc3KgFinal)) / ((100 - txtcalc3pctfinal) / 100)
    lblCalc3Kekurangan = Format(lblCalc3Kekurangan, "#,##0.00")
    'Format((calc2jumlah / calc2pct) + txtCalc2Kemasan, "#,##0.00")
End Sub
Private Sub delete_calc1()
Dim row, col As Integer
    With flxGrid1
        If flxGrid1.Rows <= 2 Then Exit Sub
        flxGrid1.TextMatrix(flxGrid1.row, 0) = ""
        row = .row
        calc1pct = calc1pct - (.TextMatrix(row, 2))
        calc1jumlah = calc1jumlah - (.TextMatrix(row, 4))
        hitungulang
        
        
        For row = row To .Rows - 1
            If row = .Rows - 1 Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(row + 1, 1) = "" Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
            ElseIf .TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To .cols - 1
                .TextMatrix(row, col) = .TextMatrix(row + 1, col)
                Next
            End If
        Next
        If .row > 1 Then .row = .row - 1
            
        .Rows = .Rows - 1
        .col = 0
        .ColSel = 4
    End With
    
End Sub
Private Sub delete_calc2()
Dim row, col As Integer
    With flxGrid2
        If .Rows <= 2 Then Exit Sub
        .TextMatrix(.row, 0) = ""
        row = .row
        calc2pct = calc2pct - (.TextMatrix(row, 2))
        calc2jumlah = calc2jumlah - (.TextMatrix(row, 4))
        hitungulang2
        
        
        For row = row To .Rows - 1
            If row = .Rows - 1 Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(row + 1, 1) = "" Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
            ElseIf .TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To .cols - 1
                .TextMatrix(row, col) = .TextMatrix(row + 1, col)
                Next
            End If
        Next
        If .row > 1 Then .row = .row - 1
            
        .Rows = .Rows - 1
        .col = 0
        .ColSel = 4
    End With
    
End Sub

Private Sub cmdAdd2_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    With flxGrid2
        If txtcalc2namabarang.text <> "" Then
            
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 1) = txtcalc2namabarang.text Then row = i
            Next
            If row = 0 Then
                row = .Rows - 1
                .Rows = .Rows + 1
            Else
                calc2pct = calc2pct - (.TextMatrix(row, 2))
                calc2jumlah = calc2jumlah - (.TextMatrix(row, 4))
            End If
            .TextMatrix(.row, 0) = ""
            .row = row
            currentRow = .row
            
            .TextMatrix(row, 1) = txtcalc2namabarang.text
            .TextMatrix(row, 2) = txtcalc2pctbarang
            .TextMatrix(row, 3) = txtcalc2harga
            .TextMatrix(row, 4) = txtcalc2pctbarang * txtcalc2harga
            
            
            calc2pct = calc2pct + (.TextMatrix(row, 2))
            calc2jumlah = calc2jumlah + (.TextMatrix(row, 4))
    
            If row > 8 Then
                .TopRow = row - 7
            Else
                .TopRow = 1
            End If
            txtcalc2namabarang = ""
            txtcalc2harga = ""
            txtcalc2pctbarang = ""
            txtcalc2namabarang.SetFocus
            hitungulang2
            
        End If
    End With
    mode = 1
    

End Sub

Private Sub Command1_Click()

End Sub

Private Sub cmdAdd3_Click()
    Dim i As Integer
Dim row As Integer
    row = 0
    With flxGrid3
        If txtcalc3NamaBarang.text <> "" Then
            
            For i = 1 To .Rows - 1
                If .TextMatrix(i, 1) = txtcalc3NamaBarang.text Then row = i
            Next
            If row = 0 Then
                row = .Rows - 1
                .Rows = .Rows + 1
            Else
                calc3Kg = calc3Kg - (.TextMatrix(row, 3))
                calc3Total = calc3Total - (.TextMatrix(row, 4))
            End If
            .TextMatrix(.row, 0) = ""
            .row = row
            currentRow = .row
            
            .TextMatrix(row, 1) = txtcalc3NamaBarang.text
            .TextMatrix(row, 2) = txtcalc3Pct
            .TextMatrix(row, 3) = txtcalc3KG
            .TextMatrix(row, 4) = txtcalc3Pct * txtcalc3KG
            
            
            calc3Kg = calc3Kg + (.TextMatrix(row, 3))
            calc3Total = calc3Total + (.TextMatrix(row, 4))
    
            If row > 8 Then
                .TopRow = row - 7
            Else
                .TopRow = 1
            End If
            txtcalc3NamaBarang = ""
            txtcalc3Harga = ""
            txtcalc3KG = ""
            txtcalc3NamaBarang.SetFocus
            hitungulang3
            
        End If
    End With
    mode = 1
    
End Sub

Private Sub cmdBungaHitung_Click()
On Error GoTo err
    lblBungaTotal = Format(((txtBungaJumlahUang * txtBungaBank) / 100 / txtBungaTahun) * txtBungaHari, "#,##0")
    lblBungaKg = Format(((txtBungaHarga * txtBungaBank) / 100 / txtBungaTahun) * txtBungaHari, "#,##0")
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub flxGrid1_DblClick()
    With flxGrid1
        txtcalc1NamaBarang = .TextMatrix(.row, 1)
        txtcalc1PctBarang = .TextMatrix(.row, 2)
        txtcalc1Harga = .TextMatrix(.row, 3)
        txtcalc1PctBarang.SetFocus
    End With
End Sub

Private Sub flxGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then delete_calc1
End Sub
Private Sub flxGrid2_DblClick()
    With flxGrid2
        txtcalc2namabarang = .TextMatrix(.row, 1)
        txtcalc2pctbarang = .TextMatrix(.row, 2)
        txtcalc2harga = .TextMatrix(.row, 3)
        txtcalc2pctbarang.SetFocus
    End With
End Sub

Private Sub flxGrid2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then delete_calc2
End Sub

Private Sub flxGrid3_DblClick()
    With flxGrid3
        txtcalc3NamaBarang = .TextMatrix(.row, 1)
        txtcalc3Pct = .TextMatrix(.row, 2)
        txtcalc3Harga = .TextMatrix(.row, 3)
        txtcalc3Pct.SetFocus
    End With
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then MySendKeys "{tab}"
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    reset_calc1
    reset_calc2
    reset_calc3
    seltab = 0
With flxGrid1
    .ColWidth(0) = 300
    .ColWidth(1) = 2500
    .ColWidth(2) = 1000
    .ColWidth(3) = 1000
    .ColWidth(4) = 1200
    
    .TextMatrix(0, 1) = "Nama Barang"
    .TextMatrix(0, 2) = "%"
    .TextMatrix(0, 3) = "Harga"
    .TextMatrix(0, 4) = "Jumlah"
End With
With flxGrid2
    .ColWidth(0) = 300
    .ColWidth(1) = 2500
    .ColWidth(2) = 1000
    .ColWidth(3) = 1000
    .ColWidth(4) = 1200
    
    .TextMatrix(0, 1) = "Nama Barang"
    .TextMatrix(0, 2) = "%"
    .TextMatrix(0, 3) = "Harga"
    .TextMatrix(0, 4) = "Jumlah"
End With
With flxGrid3
    .ColWidth(0) = 300
    .ColWidth(1) = 2500
    .ColWidth(2) = 1000
    .ColWidth(3) = 1000
    .ColWidth(4) = 1200
    
    .TextMatrix(0, 1) = "Nama Barang"
    .TextMatrix(0, 2) = "%"
    .TextMatrix(0, 3) = "Harga"
    .TextMatrix(0, 4) = "Jumlah"
End With

End Sub
Private Sub reset_calc1()
    calc1pct = 0
    calc1jumlah = 0
    txtcalc1Harga = 0
    txtcalc1hargakg = 0
    txtcalc1NamaBarang = ""
    txtcalc1PctBarang = 0
End Sub
Private Sub reset_calc2()
    calc2pct = 0
    calc2jumlah = 0
    txtcalc2harga = 0
    txtcalc2hargakg = 0
    txtcalc2namabarang = ""
    txtcalc2pctbarang = 0
End Sub
Private Sub reset_calc3()
    calc3Kg = 0
    calc3Total = 0
    txtcalc3Harga = 0
    txtcalc3KG = 0
    txtcalc3NamaBarang = ""
    txtcalc3Pct = 0
    txtcalc3pctfinal = 0
    txtcalc3KgFinal = 0
End Sub

Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1

End Sub

Private Sub Text5_Change()

End Sub

Private Sub Text2_Change()

End Sub

Private Sub txtBungaBank_GotFocus()
    txtBungaBank.SelStart = 0
    txtBungaBank.SelLength = Len(txtBungaBank)
End Sub

Private Sub txtBungaHarga_GotFocus()
    txtBungaHarga.SelStart = 0
    txtBungaHarga.SelLength = Len(txtBungaHarga)
End Sub

Private Sub txtBungaHarga_LostFocus()
On Error Resume Next
    txtBungaJumlahUang = txtBungaHarga * txtBungaQty
End Sub

Private Sub txtBungaHari_GotFocus()
    txtBungaHari.SelStart = 0
    txtBungaHari.SelLength = Len(txtBungaHari)
End Sub

Private Sub txtBungaJumlahUang_GotFocus()
    txtBungaJumlahUang.SelStart = 0
    txtBungaJumlahUang.SelLength = Len(txtBungaJumlahUang)
End Sub

Private Sub txtBungaQty_GotFocus()
    txtBungaQty.SelStart = 0
    txtBungaQty.SelLength = Len(txtBungaQty)
End Sub

Private Sub txtBungaQty_LostFocus()
On Error Resume Next
    txtBungaJumlahUang = txtBungaHarga * txtBungaQty
End Sub

Private Sub txtBungaTahun_GotFocus()
    txtBungaTahun.SelStart = 0
    txtBungaTahun.SelLength = Len(txtBungaTahun)
End Sub

Private Sub txtCalc2Kemasan_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then hitungulang2
End Sub

Private Sub txtCalc2Kemasan_LostFocus()
    hitungulang2
End Sub

Private Sub txtcalc3KgFinal_Change()
    lblCalc3BeratHasil = txtcalc3KgFinal
End Sub

Private Sub txtcalc3KgFinal_LostFocus()
    hitungulang3
End Sub

Private Sub txtcalc3pctfinal_LostFocus()
    hitungulang3
End Sub
