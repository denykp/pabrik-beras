VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmGantiHistory 
   BackColor       =   &H8000000A&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7935
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15000
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   15000
   Begin VB.CommandButton Command1 
      Caption         =   "Refresh"
      Height          =   375
      Left            =   5625
      TabIndex        =   6
      Top             =   90
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   4050
      TabIndex        =   5
      Top             =   90
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   661
      _Version        =   393216
      Format          =   55377921
      CurrentDate     =   40612
   End
   Begin MSDataGridLib.DataGrid DBGrid 
      Height          =   6270
      Left            =   225
      TabIndex        =   4
      Top             =   675
      Width           =   14685
      _ExtentX        =   25903
      _ExtentY        =   11060
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   17
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   5
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   "No Transaksi"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   "Tanggal"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   ""
         Caption         =   "Kode Bahan"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   ""
         Caption         =   "Qty"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   ""
         Caption         =   "No Ref"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            Locked          =   -1  'True
         EndProperty
         BeginProperty Column01 
            Locked          =   -1  'True
            ColumnWidth     =   1709.858
         EndProperty
         BeginProperty Column02 
            Locked          =   -1  'True
            ColumnWidth     =   3209.953
         EndProperty
         BeginProperty Column03 
            Locked          =   -1  'True
         EndProperty
         BeginProperty Column04 
            Button          =   -1  'True
            WrapText        =   -1  'True
            ColumnWidth     =   1920.189
         EndProperty
      EndProperty
   End
   Begin VB.ComboBox Combo1 
      Height          =   360
      ItemData        =   "frmGantiHistory.frx":0000
      Left            =   1845
      List            =   "frmGantiHistory.frx":000A
      TabIndex        =   3
      Text            =   "Combo1"
      Top             =   90
      Width           =   2040
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   90
      Top             =   90
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   585
      Left            =   12300
      TabIndex        =   2
      Top             =   7125
      Width           =   1935
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   585
      Left            =   3300
      TabIndex        =   1
      Top             =   8010
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   585
      Left            =   1350
      TabIndex        =   0
      Top             =   8010
      Visible         =   0   'False
      Width           =   1935
   End
End
Attribute VB_Name = "frmGantiHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
'Dim Cols As TrueOleDBGrid70.Columns

Private Sub cmdKeluar_Click()
    Unload Me
End Sub



Private Sub Combo1_Click()
    refresh_grid
End Sub
Private Sub refresh_grid()
    Adodc1.ConnectionString = strcon
    Adodc1.RecordSource = "select nomer_" & Combo1.text & " as nomer,convert(varchar(13),tanggal,101) as tanggal,kode_bahan,qty,no_history from t_" & Combo1.text & "h where convert(varchar(12),tanggal,101)='" & Format(DTPicker1, "MM/dd/yyyy") & "'"
    Set DBGrid.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid.Refresh
    DBGrid.Columns(0).DataField = "nomer"
    DBGrid.Columns(1).DataField = "tanggal"
    DBGrid.Columns(2).DataField = "kode_bahan"
    DBGrid.Columns(3).DataField = "qty"
    DBGrid.Columns(4).DataField = "no_history"
End Sub

Private Sub Command1_Click()
    refresh_grid
End Sub

Private Sub dbgrid_ButtonClick(ByVal ColIndex As Integer)

    frmSearch.query = "select * from vw_pemakaianbahandus where kode_bahan='" & DBGrid.Columns(2).text & "'"
    frmSearch.nmform = "frmAddgantihistory"
    frmSearch.nmctrl = "dbgrid"
    frmSearch.Index = "4"
    frmSearch.col = 0
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "pemakaian"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub
Private Sub cmdSearchPerkiraan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa"
    frmSearch.nmform = "frmMasterSupplier"
    frmSearch.nmctrl = "SSDBGrid1"
    frmSearch.nmctrl2 = "SSDBGrid1"
    
    frmSearch.proc = ""
    frmSearch.col = 0
    frmSearch.col2 = 1
    frmSearch.Index = 3
    frmSearch.Index2 = 4
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub Form_Load()
    
    
    Select Case filename
    Case "Tinta"
        lblKode = "Kode Tinta"
        SSDBGrid1.Columns(0).Caption = "Gudang"
        SSDBGrid1.Columns(1).Caption = "Tinta"
        SSDBGrid1.Columns(2).Caption = "Nama Tinta"
        SSDBGrid1.Columns(3).Caption = "Perkiraan"
        SSDBGrid1.Columns(4).Caption = "Nama Perkiraan"
        SSDBGrid1.Columns(5).Visible = False
        SSDBGrid1.Columns(6).Visible = False
        
    Case "Bahan"
        lblKode = "Kode Bahan"
        SSDBGrid1.Columns(0).Visible = False
        SSDBGrid1.Columns(1).Caption = "Kode Bahan"
        SSDBGrid1.Columns(2).Caption = "Nama Bahan"
        SSDBGrid1.Columns(3).Caption = "Perkiraan Persediaan"
        SSDBGrid1.Columns(4).Caption = "Ket.Persediaan "
        SSDBGrid1.Columns(5).Visible = True
        SSDBGrid1.Columns(5).Caption = "Perkiraan HPP"
        SSDBGrid1.Columns(6).Visible = True
        SSDBGrid1.Columns(6).Caption = "Ket.Hpp "
    Case "Customer"
        lblKode = "Kode Customer"
        SSDBGrid1.Columns(0).Visible = False
        SSDBGrid1.Columns(1).Caption = "Kode Customer"
        SSDBGrid1.Columns(2).Caption = "Nama Customer"
        SSDBGrid1.Columns(3).Caption = "Perkiraan Piutang"
        SSDBGrid1.Columns(4).Visible = True
        SSDBGrid1.Columns(4).Caption = "Perkiraan Hutang Retur Jual"

    Case "Supplier"
        lblKode = "Kode Supplier"
        SSDBGrid1.Columns(0).Visible = False
        SSDBGrid1.Columns(1).Caption = "Kode Supplier"
        SSDBGrid1.Columns(2).Caption = "Nama Supplier"
        SSDBGrid1.Columns(3).Caption = "Perkiraan Hutang"
        SSDBGrid1.Columns(4).Visible = True
        SSDBGrid1.Columns(4).Caption = "Perkiraan Piutang Retur Beli"
    End Select
    reset_form
'    load_data
'    cari_perkiraan
End Sub



Private Sub reset_form()
    cari_data
End Sub

Private Sub add_data(i As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)
    Select Case filename
        Case "Tinta"
            table_name = "setting_accTinta"
            fields(0) = "kode_tinta"
            fields(1) = "kode_acc"
            fields(2) = "kode_gudang"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(0).text
        Case "Bahan"
            table_name = "setting_accbahan"
            fields(0) = "kode_bahan"
            fields(1) = "acc_persediaan"
            fields(2) = "acc_hpp"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(5).text
        Case "Customer"
            table_name = "setting_accCustomer"
            fields(0) = "kode_customer"
            fields(1) = "acc_piutang"
            fields(2) = "acc_hutangRetur"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(5).text
        Case "Supplier"
            table_name = "setting_accSupplier"
            fields(0) = "kode_supplier"
            fields(1) = "acc_hutang"
            fields(2) = "acc_piutangRetur"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(5).text
    End Select
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub SSDBGrid1_AfterColUpdate(ByVal ColIndex As Integer)
Dim i As Byte
On Error GoTo err
i = 0
If SSDBGrid1.Columns(1).text = "" Then Exit Sub
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
i = 1
    Select Case filename
    Case "Tinta"
        rs.Open "select * from setting_accTinta where kode_gudang='" & SSDBGrid1.Columns(0).text & "' and kode_tinta='" & SSDBGrid1.Columns(1).text & "'", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accTinta where kode_gudang='" & SSDBGrid1.Columns(0).text & "' and kode_tinta='" & SSDBGrid1.Columns(1).text & "' "
        End If
    Case "Bahan"
        rs.Open "select * from setting_accbahan where kode_bahan='" & SSDBGrid1.Columns(1).text & "' ", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accbahan where kode_bahan='" & SSDBGrid1.Columns(1).text & "' "
        End If
    Case "Customer"
        rs.Open "select * from setting_accCustomer where kode_customer='" & SSDBGrid1.Columns(1).text & "' ", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accCustomer where kode_customer='" & SSDBGrid1.Columns(1).text & "' "
        End If
    Case "Supplier"
        rs.Open "select * from setting_accSupplier where kode_supplier='" & SSDBGrid1.Columns(1).text & "' ", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accSupplier where kode_supplier='" & SSDBGrid1.Columns(1).text & "' "
        End If
    End Select
    
    add_data SSDBGrid1.col
    conn.CommitTrans
i = 0
    DropConnection
Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub



Private Sub txtKode_LostFocus()
'    If txtKode <> "" Then cari_data
End Sub
Private Sub cari_data()

End Sub

Private Sub SSDBGrid1_KeyDown(KeyCode As Integer, Shift As Integer)

'If KeyCode = vbKeyF3 Then
'    frmMasterAcc.tutup = True
'    frmMasterAcc.Show
' Select Case filename
'        Case "Tinta"
'         SSDBGrid1.Columns(3).text = frmMasterAcc.txtField(1).text
'        Case "Bahan"
'
'        Case "Customer"
'
'        Case "Supplier"
'
'    End Select
'
'
'End If
'frmMasterAcc.tutup = False
End Sub
'
'Private Sub SSDBGrid1_LostFocus()
'    SSDBGrid1.Refresh
'    SSDBGrid1.Update
'
'End Sub
