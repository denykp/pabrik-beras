VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddReturSuratJalan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Retur Pengiriman Barang"
   ClientHeight    =   9675
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   13215
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9675
   ScaleWidth      =   13215
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5760
      Picture         =   "frmAddReturSuratJalan.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   53
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6855
      Picture         =   "frmAddReturSuratJalan.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   52
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdAddOrder 
      Caption         =   "Add"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4500
      Picture         =   "frmAddReturSuratJalan.frx":0A64
      TabIndex        =   49
      Top             =   1755
      Width           =   825
   End
   Begin VB.ComboBox cmbNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1665
      Style           =   2  'Dropdown List
      TabIndex        =   48
      Top             =   1755
      Width           =   2805
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3030
      Left            =   7200
      TabIndex        =   46
      Top             =   4365
      Visible         =   0   'False
      Width           =   3210
      _ExtentX        =   5662
      _ExtentY        =   5345
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   6
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ComboBox cmbGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   9585
      Style           =   2  'Dropdown List
      TabIndex        =   44
      Top             =   9045
      Visible         =   0   'False
      Width           =   2805
   End
   Begin VB.CommandButton cmdopentreeview 
      Appearance      =   0  'Flat
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   12420
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   43
      Top             =   9090
      Visible         =   0   'False
      Width           =   330
   End
   Begin VB.TextBox txtNoSuratJalan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1665
      TabIndex        =   40
      Top             =   495
      Width           =   1875
   End
   Begin VB.CommandButton cmdSearchSuratJalan 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   39
      Top             =   495
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchPosting 
      Caption         =   "Search Post"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4365
      Picture         =   "frmAddReturSuratJalan.frx":0B66
      TabIndex        =   37
      Top             =   45
      Width           =   1320
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   1665
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   900
      Width           =   7035
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      Picture         =   "frmAddReturSuratJalan.frx":0C68
      TabIndex        =   28
      Top             =   45
      Width           =   420
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9990
      Top             =   8955
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Simpan + &Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5940
      Picture         =   "frmAddReturSuratJalan.frx":0D6A
      TabIndex        =   23
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3105
      Picture         =   "frmAddReturSuratJalan.frx":0E6C
      TabIndex        =   8
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   315
      Picture         =   "frmAddReturSuratJalan.frx":0F6E
      TabIndex        =   5
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4500
      Picture         =   "frmAddReturSuratJalan.frx":1070
      TabIndex        =   7
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1710
      Picture         =   "frmAddReturSuratJalan.frx":1172
      TabIndex        =   6
      Top             =   9045
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   12105
      Top             =   495
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10980
      TabIndex        =   27
      Top             =   630
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   8505
      TabIndex        =   0
      Top             =   510
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   99221507
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   10215
      TabIndex        =   33
      Top             =   900
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   99221507
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker3 
      Height          =   330
      Left            =   10215
      TabIndex        =   35
      Top             =   1260
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   99221507
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5700
      Index           =   1
      Left            =   315
      TabIndex        =   15
      Top             =   3060
      Visible         =   0   'False
      Width           =   10215
      Begin VB.ListBox listKuli 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2760
         Left            =   1575
         Style           =   1  'Checkbox
         TabIndex        =   38
         Top             =   2880
         Width           =   3705
      End
      Begin VB.ListBox listTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   3960
         TabIndex        =   26
         Top             =   270
         Width           =   3660
      End
      Begin VB.TextBox txtNoTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   24
         Top             =   270
         Width           =   1755
      End
      Begin VB.CommandButton cmdSearchPengawas 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3420
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   21
         Top             =   1755
         Width           =   420
      End
      Begin VB.ListBox listPengawas 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   3960
         TabIndex        =   4
         Top             =   1755
         Width           =   3660
      End
      Begin VB.TextBox txtPengawas 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   3
         Top             =   1755
         Width           =   1755
      End
      Begin VB.TextBox txtSupir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   2
         Top             =   1305
         Width           =   5220
      End
      Begin VB.Label Label5 
         Caption         =   "No. Truk"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   25
         Top             =   315
         Width           =   960
      End
      Begin VB.Label lblNamaPengawas 
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1575
         TabIndex        =   22
         Top             =   2160
         Width           =   2265
      End
      Begin VB.Label Label9 
         Caption         =   "Supir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   18
         Top             =   1395
         Width           =   960
      End
      Begin VB.Label Label8 
         Caption         =   "Kuli"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   17
         Top             =   2880
         Width           =   960
      End
      Begin VB.Label Label6 
         Caption         =   "Pengawas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   16
         Top             =   1800
         Width           =   960
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5700
      Index           =   0
      Left            =   270
      TabIndex        =   10
      Top             =   3060
      Width           =   12645
      Begin SSDataWidgets_B.SSDBGrid dbgrid 
         Height          =   4965
         Left            =   180
         TabIndex        =   47
         Top             =   135
         Width           =   12165
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         MultiLine       =   0   'False
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   450
         ExtraHeight     =   238
         Columns.Count   =   8
         Columns(0).Width=   2566
         Columns(0).Caption=   "No Order"
         Columns(0).Name =   "No Order"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   2646
         Columns(1).Caption=   "Kode Bahan"
         Columns(1).Name =   "Kode Bahan"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   5398
         Columns(2).Caption=   "Nama Bahan"
         Columns(2).Name =   "Nama Bahan"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Nomer Serial"
         Columns(3).Name =   "Nomer Serial"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1349
         Columns(4).Caption=   "Qty"
         Columns(4).Name =   "Qty"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1773
         Columns(5).Caption=   "Berat"
         Columns(5).Name =   "Berat"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   2858
         Columns(6).Caption=   "Gudang"
         Columns(6).Name =   "Gudang"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).Style=   1
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "Hpp"
         Columns(7).Name =   "Hpp"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   21458
         _ExtentY        =   8758
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label24 
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6210
         TabIndex        =   20
         Top             =   5175
         Width           =   960
      End
      Begin VB.Label lblBerat 
         Alignment       =   1  'Right Justify
         Caption         =   "lblBerat"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7155
         TabIndex        =   19
         Top             =   5175
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   14
         Top             =   5175
         Width           =   735
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   13
         Top             =   5175
         Width           =   1140
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4320
         TabIndex        =   12
         Top             =   5175
         Width           =   1140
      End
      Begin VB.Label Label17 
         Caption         =   "Jml Kemasan"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   11
         Top             =   5175
         Width           =   1770
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   6225
      Left            =   180
      TabIndex        =   9
      Top             =   2610
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   10980
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Barang"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Pekerja"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblNamaCustomer 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Customer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5400
      TabIndex        =   51
      Top             =   2160
      Width           =   1050
   End
   Begin VB.Label lblKodeCustomer 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Customer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5400
      TabIndex        =   50
      Top             =   1800
      Width           =   1050
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8415
      TabIndex        =   45
      Top             =   9105
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "No.Order"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   180
      TabIndex        =   42
      Top             =   1800
      Width           =   1050
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "No.Pengeluaran"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   41
      Top             =   555
      Width           =   1410
   End
   Begin VB.Label Label15 
      Caption         =   "Selesai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9000
      TabIndex        =   36
      Top             =   1305
      Width           =   1050
   End
   Begin VB.Label Label11 
      Caption         =   "Mulai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9000
      TabIndex        =   34
      Top             =   945
      Width           =   1050
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   32
      Top             =   885
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7020
      TabIndex        =   31
      Top             =   555
      Width           =   1320
   End
   Begin VB.Label Label22 
      Caption         =   "No.Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   30
      Top             =   135
      Width           =   1410
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1710
      TabIndex        =   29
      Top             =   45
      Width           =   2085
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddReturSuratJalan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte
Public suratjalan As Boolean

Private Sub chkAngkut_Click()
    If chkAngkut.value = False Then frAngkut.Visible = False Else frAngkut.Visible = True
End Sub


Private Sub cmbNoOrder_Click()
    cek_order
End Sub

Private Sub cmdAddOrder_Click()
    add_order
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdopentreeview_Click()
On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    If simpan Then
    If posting_retursuratjalan(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    If suratjalan Then
    frmSearch.query = Searchretursuratjalan & " where status_posting='1'"
    Else
    frmSearch.query = Searchretursuratjalan
    End If
    frmSearch.nmform = "frmAddSuratJalan"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "so"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKuli_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmAddSuratJalan"
        .nmctrl = "txtkuli"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_kuli"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        .query = searchKaryawan
        .nmform = "frmAddSuratJalan"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPosting_Click()
    frmSearch.connstr = strcon
    frmSearch.query = Searchretursuratjalan & " where status_posting='0'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Suratjalan"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub


Public Function cek_suratjalan() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cek_suratjalan = False

cmbNoOrder.Clear
    conn.Open strcon
    rs.Open "select distinct nomer_order from t_suratjalan_timbang where nomer_suratjalan='" & txtNoSuratJalan & "' order by nomer_order", conn
    While Not rs.EOF
        cmbNoOrder.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
Exit Function
err:
MsgBox err.Description
If rs.State Then rs.Close
If conn.State Then conn.Close
Set conn = Nothing
End Function

Private Sub cmdSearchSuratJalan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select * from vw_suratjalanbelumtagih where status_posting='1'"
    
    frmSearch.nmform = "frmAddReturSuratJalan"
    frmSearch.nmctrl = "txtNoSuratJalan"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "suratjalan"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_suratjalan"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If validasi Then
    If simpan Then
        MsgBox "Data sudah tersimpan"
'        reset_form
        cmdPosting.Visible = True
        Call MySendKeys("{tab}")
    End If
    End If
End Sub
Private Function validasi() As Boolean
On Error GoTo err
Dim item As Integer
    validasi = False
    DBGrid.Update
    item = 0
    For row = 0 To DBGrid.Rows - 1
        If (DBGrid.Columns(5).CellText(row) > 0 Or DBGrid.Columns(4).CellText(row) > 0) Then
        If DBGrid.Columns(6).CellText(row) = "" Then
            MsgBox "Pilih Gudang terlebih dahulu"
            Exit Function
        Else
        item = item + 1
        End If
        
        End If
    Next
    
    If item = 0 Then
        MsgBox "Silahkan masukkan barang yang ingin disimpan terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If cmbGudang.text = "" Then
        MsgBox "Pilih gudang terlebih dahulu"
        cmbGudang.SetFocus
        Exit Function
    End If
    validasi = True
err:
End Function
Private Function simpan() As Boolean

Dim i As Integer
Dim id As String
Dim row As Integer
Dim rs As New ADODB.Recordset
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False

   id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_retursuratjalanh", "nomer_retursuratjalan", DTPicker1, "RKB")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_retursuratjalanh where nomer_retursuratjalan='" & id & "'"
    conn.Execute "delete from t_retursuratjaland where nomer_retursuratjalan='" & id & "'"
    

    
    add_dataheader

    For row = 0 To DBGrid.Rows - 1
        If DBGrid.Columns(5).CellText(row) > 0 Or DBGrid.Columns(4).CellText(row) > 0 Then add_datadetail (row)
    Next
    
    conn.CommitTrans
    i = 0
    simpan = True
    conn.Close
    DropConnection
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub cek_order()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
conn.Open strcon
rs.Open "select m.* from t_soh s inner join ms_customer m on s.kode_customer=m.kode_customer where nomer_so='" & cmbNoOrder.text & "'", conn
If Not rs.EOF Then
    lblKodeCustomer = rs!kode_customer
    lblNamaCustomer = rs!Nama_Customer
End If
conn.Close
Set conn = Nothing

End Sub
Public Sub add_order()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim counter As Integer
counter = 0
DBGrid.MoveFirst
While counter < DBGrid.Rows
    If DBGrid.Columns(0).CellText(counter) = cmbNoOrder.text Then Exit Sub
    counter = counter + 1
Wend
conn.Open strcon

DBGrid.FieldSeparator = "#"
rs.Open "select distinct nomer_order,s.kode_bahan,m.nama_bahan,s.nomer_serial,hpp from t_suratjalan_timbang s " & _
    "inner join ms_bahan m on s.kode_bahan=m.kode_bahan where nomer_suratjalan='" & txtNoSuratJalan.text & "' and nomer_order='" & cmbNoOrder.text & "'", conn
While Not rs.EOF
    DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#0#0##" & rs!hpp
    rs.MoveNext
Wend
conn.Close
Set conn = Nothing
End Sub
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    If Len(lblNoTrans) > 1 Then
        lblNoSuratJalan = "SJ" & Mid(lblNoTrans, 3, Len(lblNoTrans))
    Else
        lblNoSuratJalan = lblNoTrans
    End If
    txtNoOrder.text = ""
    txtKeterangan.text = ""
    chkAngkut.value = True
    
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    listPengawas.Clear
   
    txtSupir = ""
    status_posting = False
    totalQty = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblBerat = 0
    lblItem = 0
    
    txtNamaQCFisik = ""
    txtNamaQCLab = ""
    txtQCFisik = ""
    txtQCLab = ""
    txtQCKeterangan = ""
    
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    cmdPrint.Enabled = False
    
    TabStrip1.Tabs(1).Selected = True
    
    txtNoOrder.SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(12)
    ReDim nilai(12)
    table_name = "t_retursuratjalanh"
    fields(0) = "nomer_retursuratjalan"
    fields(1) = "tanggal_retursuratjalan"
    
    fields(2) = "nomer_suratjalan"
    fields(3) = "keterangan"
    fields(4) = "userid"
    fields(5) = "status_posting"
    
    fields(6) = "no_truk"
    fields(7) = "pengawas"
    fields(8) = "kuli"
    fields(9) = "supir"
    fields(10) = "jam_mulai"
    fields(11) = "jam_selesai"
    

    
    Dim pengawas, supir, kuli, notruk As String
    pengawas = ""
    For A = 0 To listPengawas.ListCount - 1
        pengawas = pengawas & "[" & Left(listPengawas.List(A), InStr(1, listPengawas.List(A), "-") - 1) & "],"
    Next
    If pengawas <> "" Then pengawas = Left(pengawas, Len(pengawas) - 1)
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    
    nilai(2) = txtNoSuratJalan.text
    nilai(3) = txtKeterangan.text
    nilai(4) = User
    nilai(5) = 0
    nilai(6) = notruk
    nilai(7) = pengawas
    nilai(8) = kuli
    nilai(9) = supir
    nilai(10) = Format(DTPicker2, "HH:mm")
    nilai(11) = Format(DTPicker3, "HH:mm")
    
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_retursuratjaland"
    fields(0) = "nomer_retursuratjalan"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "nomer_serial"
    fields(5) = "NO_URUT"
    fields(6) = "nomer_order"
    fields(7) = "kode_gudang"
    fields(8) = "hpp"

    
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(1).CellText(row)
    nilai(2) = Replace(Format(DBGrid.Columns(4).CellText(row), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(DBGrid.Columns(5).CellText(row), "###0.##"), ",", ".")
    nilai(4) = DBGrid.Columns(3).CellText(row)
    nilai(5) = row
    
    nilai(6) = DBGrid.Columns(0).CellText(row)
    nilai(7) = Trim(Right(DBGrid.Columns(6).CellText(row), 10))
    nilai(8) = DBGrid.Columns(7).CellText(row)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
cmdSimpan.Enabled = True
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_retursuratjalanh where nomer_retursuratjalan='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        lblNoSuratJalan = "SJ" & Mid(lblNoTrans, 3, Len(lblNoTrans))
        DTPicker1 = rs("tanggal_retursuratjalan")
        txtKeterangan.text = rs("keterangan")
        txtNoSuratJalan.text = rs("nomer_suratjalan")
        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
        Dim pengawas() As String
        Dim kuli() As String
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
            txtNoTruk_KeyDown 13, 0
        Next
        
        pengawas = Split(rs!pengawas, ",")
        listPengawas.Clear
        For A = 0 To UBound(pengawas)
            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
            txtPengawas_KeyDown 13, 0
        Next
        
        kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
'            txtKuli = Replace(Replace(kuli(A), "[", ""), "]", "")
'            txtKuli_KeyDown 13, 0
        Next
        If rs!status_posting = 0 Then
            cmdPosting.Visible = True
        Else
            cmdSimpan.Enabled = False
        End If
       
        totalQty = 0
        totalberat = 0
        total = 0
        
        If rs.State Then rs.Close
        
        DBGrid.RemoveAll
        row = 1
        DBGrid.FieldSeparator = "#"
        rs.Open "select d.nomer_order,d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat,kode_gudang from t_retursuratjaland d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_retursuratjalan='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                SetComboTextRight rs!kode_gudang, cmbGudang
                DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(5) & "#" & cmbGudang.text
                
                row = row + 1
'                totalQty = totalQty + rs(4)
'                totalberat = totalberat + rs(5)
'
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty
        lblBerat = totalberat
        
        lblItem = DBGrid.Rows
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_noso()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_soh where nomer_so='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        
        If rs.State Then rs.Close
        txtKdBrg.Clear
        rs.Open "select distinct(d.[kode_bahan]) from t_sod d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_so='" & txtNoOrder & "' order by d.kode_bahan", conn
        While Not rs.EOF
            txtKdBrg.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub loaddetil()

'        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
'            mode = 2
'            cmdClear.Enabled = True
'            cmdDelete.Enabled = True
'            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
'            If Not cek_kodebarang1 Then
'                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
'
'            End If
'            txtNomerSerial = flxGrid.TextMatrix(flxGrid.row, 3)
'            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
'            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 5)
'            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 7)
'            txtBerat.SetFocus
'        End If
'
End Sub

Private Sub Command1_Click()

End Sub



Private Sub dbgrid_BtnClick()
On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click

    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
        
    End If
End Sub

Private Sub Form_Load()
    load_combo
    
    DTPicker1 = Now
    DTPicker2 = Now
    DTPicker3 = Now
    Load_ListKuli
    loadgrup "0", tvwMain
    addserialsj
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    seltab = 0
'    conn.ConnectionString = strcon
'    conn.Open
'    listKuli.Clear
'    rs.Open "select * from ms_karyawan order by nik", conn
'    While Not rs.EOF
'        listKuli.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Close
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Public Sub cek_pengawas()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close

err:
End Sub
Public Sub cek_kuli()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtKuli & "'", conn
    If Not rs.EOF Then
        lblNamaKuli = rs(1)
    Else
        lblNamaKuli = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub

Private Sub hitung_ulang()
'On Error GoTo err
'    conn.Open strcon
'    With flxGrid
'    For i = 1 To .Rows - 2
'        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
'        If Not rs.EOF Then
'            total = total - .TextMatrix(i, 8)
'            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
'            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
'            total = total + .TextMatrix(i, 8)
'        End If
'        rs.Close
'    Next
'    End With
'    conn.Close
'    lblTotal = Format(total, "#,##0")
'    Exit Sub
'err:
'    MsgBox err.Description
'    If conn.State Then conn.Close
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyDelete And listPengawas.ListIndex >= 0 Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And listTruk.ListIndex >= 0 Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub
Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    DBGrid.Columns(6).text = cmbGudang.text
    tvwMain.Visible = False
End Sub




Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPengawas_Click
    If KeyCode = 13 Then
        cek_pengawas
        listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
        txtPengawas = ""
        lblNamaPengawas = ""
        MySendKeys "+{tab}"
    End If
End Sub
