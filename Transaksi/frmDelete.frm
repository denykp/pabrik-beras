VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmDelete 
   Caption         =   "Transaksi"
   ClientHeight    =   8055
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14190
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   14190
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdrefresh 
      Caption         =   "&Refresh"
      Height          =   375
      Left            =   5280
      TabIndex        =   4
      Top             =   120
      Width           =   1095
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   5280
      Top             =   120
      Visible         =   0   'False
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   120
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      CheckBox        =   -1  'True
      CustomFormat    =   "dd/MM/YYYYY"
      Format          =   60227585
      CurrentDate     =   40066
   End
   Begin VB.ComboBox cmb_transaksi 
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      Top             =   120
      Width           =   1935
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid flxgrid 
      Height          =   6735
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   13935
      _ExtentX        =   24580
      _ExtentY        =   11880
      _Version        =   393216
      FixedCols       =   0
      FocusRect       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin VB.Label Label1 
      Caption         =   "Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmDelete"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub refresh_data()
If cmb_transaksi = "Penjualan" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'yyyy/MM/dd') AS Tanggal,[KODE CUSTOMER]from t_penjualanh"
  ElseIf cmb_transaksi = "Pembelian" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'yyyy/MM/dd') AS Tanggal,kode_supplierfrom t_pembelianh"
 ElseIf cmb_transaksi = "Pengambilan Barang" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'yyyy/MM/dd') AS Tanggal from t_adjustmenth"
 ElseIf cmb_transaksi = "Stock Opname" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'yyyy/MM/dd')as tanggal,keterangan from t_stockopnameh"
End If
 Adodc1.RecordSource = Adodc1.RecordSource & IIf(IsNull(DTPicker1.Value), "", " where format(tanggal, 'yyyy/MM/dd')='" & Format(DTPicker1.Value, "yyyy/MM/dd") & "'")
 Set flxGrid.DataSource = Adodc1
    Adodc1.Refresh
    flxGrid.Refresh
End Sub
Private Sub cmb_transaksi_Click()
    refresh_data
End Sub

Private Sub cmdrefresh_Click()
    refresh_data
End Sub


Private Sub DTPicker1_Click()
refresh_data
End Sub

Private Sub flxgrid_DblClick()
    hapus
    Adodc1.Refresh
    flxGrid.Refresh
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 27 Then Unload Me

End Sub

Private Sub flxGrid_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

hapus
Adodc1.Refresh
flxGrid.Refresh
End If

End Sub
Private Sub hapus()
On Error GoTo err
Dim mode As Byte
mode = 1

conn.Open strcon
conn.BeginTrans
mode = 2
i = flxGrid.row
j = flxGrid.RowSel

For a = IIf(i < j, i, j) To IIf(i < j, j, i)
If cmb_transaksi = "Penjualan" Then
    With conn
        .Execute "delete from t_penjualanh where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_penjualand where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_pembayaran where NO_JUAL='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete t_returjuald.* from t_returjualh inner join t_returjuald on t_returjuald.id = t_returjualh.id where No_nota ='" & flxGrid.TextMatrix(a, 0) & "' "
        rs.Open "select id from t_returjualh where No_nota='" & flxGrid.TextMatrix(a, 0) & "'", conn
        If Not rs.EOF Then
            conn.Execute "update t_returjualh set pk='' where pk='" & rs(0) & "'"
        End If
        rs.Close
        .Execute "delete from t_returjualh where No_nota='" & flxGrid.TextMatrix(a, 0) & "'"
   End With
   conn.Execute "update t_penjualanh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
ElseIf cmb_transaksi = "Pembelian" Then
    With conn
        .Execute "delete from t_pembelianh where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_pembeliand where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_bayarhutang where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute " delete t_returbelid.* from t_returbelih inner join t_returbelid on t_returbelid.id = t_returbelih.id where No_nota ='" & flxGrid.TextMatrix(a, 0) & "' "
         rs.Open "select id from t_returbelih where No_nota='" & flxGrid.TextMatrix(a, 0) & "'", conn
             If Not rs.EOF Then
                 conn.Execute "update t_returbelih set pk='' where pk='" & rs(0) & "'"
             End If
             rs.Close
        .Execute "delete from t_returbelih where No_nota='" & flxGrid.TextMatrix(a, 0) & "'"
   End With
   conn.Execute "update t_pembelianh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
 ElseIf cmb_transaksi = "Pengambilan Barang" Then
    With conn
        .Execute "delete from t_adjustmentd where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_adjustmenth where ID='" & flxGrid.TextMatrix(a, 0) & "'"
    End With
    conn.Execute "update t_adjustmenth set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
 Else
    With conn
        .Execute "delete from t_stockopnamed where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_stockopnameh where ID='" & flxGrid.TextMatrix(a, 0) & "'"
    End With
   
   conn.Execute "update t_stockopnameh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
  End If

Next a
conn.CommitTrans
conn.CommitTrans
mode = 1
conn.Close
err:
If mode = 2 Then conn.RollbackTrans
DropConnection
End Sub
Private Sub Form_Activate()
Adodc1.ConnectionString = strcon
cmb_transaksi.AddItem "Penjualan"
cmb_transaksi.AddItem "Pembelian"
cmb_transaksi.AddItem "Pengambilan Barang"
cmb_transaksi.AddItem "Stock Opname"

cmb_transaksi.SetFocus
cmb_transaksi = "Penjualan"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()

With flxGrid
    .ColWidth(0) = 2500
    .ColWidth(1) = 1800
    .ColWidth(2) = 1800
    
    End With

End Sub

Private Sub Command2_Click()

'Adodc1.RecordSource = "select ID from t_penjualan"
'MsgBox Adodc1.Recordset!id
'Adodc1.Recordset.MoveNext
End Sub



