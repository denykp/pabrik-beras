alter table ms_coa
add fg_aktif varchar(1)
go
update ms_coa set fg_aktif = 1 where fg_aktif is null
go
update ms_coa set fg_aktif = 0 where nama like '%(OLD)%'