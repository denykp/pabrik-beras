VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSettingPesan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Setting Pesan"
   ClientHeight    =   4965
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8025
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleWidth      =   8025
   Begin VB.ComboBox txtUser 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1515
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   1200
      Width           =   2580
   End
   Begin VB.TextBox txtPesan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   1515
      MaxLength       =   300
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   1620
      Width           =   6165
   End
   Begin VB.ComboBox txtDari 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1515
      Locked          =   -1  'True
      Style           =   1  'Simple Combo
      TabIndex        =   17
      Top             =   795
      Width           =   1905
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Berlaku setiap bulan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   225
      TabIndex        =   9
      Top             =   3720
      Width           =   3675
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3315
      Picture         =   "frmSettingPesan.frx":0000
      TabIndex        =   5
      Top             =   4095
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4650
      Picture         =   "frmSettingPesan.frx":0102
      TabIndex        =   6
      Top             =   4095
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1995
      Picture         =   "frmSettingPesan.frx":0204
      TabIndex        =   4
      Top             =   4095
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3630
      Picture         =   "frmSettingPesan.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   375
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtNomer 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1515
      TabIndex        =   16
      Top             =   390
      Width           =   1905
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   630
      Picture         =   "frmSettingPesan.frx":0408
      TabIndex        =   2
      Top             =   4095
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1515
      TabIndex        =   3
      Top             =   3195
      Width           =   2475
      _ExtentX        =   4366
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   54919171
      CurrentDate     =   38927
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Untuk User :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   240
      TabIndex        =   15
      Top             =   1215
      Width           =   1140
   End
   Begin VB.Label lblStatus 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   6600
      TabIndex        =   14
      Top             =   225
      Width           =   840
   End
   Begin VB.Label Label5 
      Caption         =   "Status :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   5715
      TabIndex        =   13
      Top             =   225
      Width           =   840
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Dari User :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   240
      TabIndex        =   12
      Top             =   810
      Width           =   1140
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Pesan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   240
      TabIndex        =   11
      Top             =   1590
      Width           =   1005
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   240
      TabIndex        =   10
      Top             =   3225
      Width           =   1005
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   7
      Top             =   420
      Width           =   885
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmSettingPesan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0

    If txtNomer.text = "" Then
        MsgBox "Silahkan masukkan nomer terlebih dahulu!", vbCritical
        txtNomer.SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    
    
        conn.Execute "delete from alert where nomer='" & txtNomer.text & "'"

        MsgBox "Data sudah dihapus"
    
    
    conn.CommitTrans
    i = 0
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = "select * from alert"
    frmSearch.nmform = "frmSettingPesan"
    frmSearch.nmctrl = "txtnomer"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "alert"
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub
Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    
    If txtPesan.text = "" Then
        MsgBox "Pesan harus diisi"
        txtPesan.SetFocus
        Exit Sub
    End If
    
    
        
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    
    txtNomer = newid("alert", "nomer", Date, "RM")
    
    If txtNomer <> "" Then
        
        If lblStatus.Caption = "1" Then
            MsgBox "Data tidak bisa dikoreksi karena sudah dikerjakan !", vbExclamation
            Exit Sub
        End If
    
        conn.Execute "delete from alert where nomer='" & txtNomer & "'"
    End If
    add_dataheader
    
    
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtNomer.text = ""
    txtUser.text = ""
    txtPesan.text = ""
    txtUser.SetFocus
    Check1.value = 0
End Sub
Public Sub cari_data()
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select userid,pesan,tanggal,bulanan,status,dari from alert where nomer='" & txtNomer.text & "'", conn
    If Not rs.EOF Then
        txtUser.text = rs(0)
        txtPesan.text = rs(1)
        DTPicker1.value = rs(2)
        Check1.value = IIf(rs(3) = "y", 1, 0)
        lblStatus.Caption = rs(4)
        txtDari.text = rs(5)
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        txtPesan.text = ""
        txtUser.text = ""
        txtDari.text = ""
        Check1.value = 0
        lblStatus.Caption = "0"
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    reset_form
    DTPicker1.value = Now
    txtDari.text = User
    load_combo
End Sub

Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    txtUser.Clear
    rs.Open "select userid from userid  order by userid", conn
    While Not rs.EOF
        txtUser.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub


Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(7)
    ReDim nilai(7)
    
    table_name = "alert"
    fields(0) = "nomer"
    fields(1) = "userid"
    fields(2) = "pesan"
    fields(3) = "tanggal"
    fields(4) = "bulanan"
    fields(5) = "tanggal_alert"
    fields(6) = "dari"
    
    nilai(0) = txtNomer.text
    nilai(1) = txtUser.text
    nilai(2) = txtPesan.text
    nilai(3) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(4) = IIf(Check1.value = 1, "y", "n")
    nilai(5) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(6) = txtDari.text
    conn.Execute tambah_data2(table_name, fields, nilai)


End Sub


'Private Sub txtNomer_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then cari_data
'End Sub

Private Sub txtnomer_LostFocus()
    If txtNomer <> "" Then cari_data
End Sub
