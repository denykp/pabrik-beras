VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Begin VB.Form frmProsesHPP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Proses HPP"
   ClientHeight    =   1785
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5730
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1785
   ScaleWidth      =   5730
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command3 
      Caption         =   "Hitung Ulang Kartu Piutang && Hutang"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   270
      TabIndex        =   19
      Top             =   3465
      Width           =   5295
   End
   Begin VB.Frame Frame1 
      Caption         =   "Edit HPP"
      Height          =   1140
      Left            =   150
      TabIndex        =   14
      Top             =   1800
      Width           =   5340
      Begin VB.CommandButton cmdopentreeview 
         Appearance      =   0  'Flat
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4065
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   16
         Top             =   450
         Width           =   330
      End
      Begin VB.ComboBox cmbGudang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1545
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   405
         Width           =   2505
      End
      Begin MSComctlLib.TreeView tvwMain 
         Height          =   3030
         Left            =   1530
         TabIndex        =   18
         Top             =   780
         Visible         =   0   'False
         Width           =   3210
         _ExtentX        =   5662
         _ExtentY        =   5345
         _Version        =   393217
         HideSelection   =   0   'False
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Gudang :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   195
         TabIndex        =   17
         Top             =   450
         Width           =   1320
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Proses Kartu Hutang"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   2790
      TabIndex        =   13
      Top             =   6915
      Width           =   2775
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Proses Kartu Piutang"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   150
      TabIndex        =   12
      Top             =   6915
      Width           =   2520
   End
   Begin MSComctlLib.ProgressBar P1 
      Height          =   225
      Left            =   75
      TabIndex        =   11
      Top             =   1455
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   397
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1395
      Picture         =   "frmProsesHPP.frx":0000
      TabIndex        =   3
      Top             =   870
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2910
      Picture         =   "frmProsesHPP.frx":0102
      TabIndex        =   2
      Top             =   885
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker0 
      Height          =   375
      Left            =   2385
      TabIndex        =   0
      Top             =   210
      Width           =   1860
      _ExtentX        =   3281
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "MMMM yyyy"
      Format          =   97976323
      CurrentDate     =   40960
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1155
      TabIndex        =   4
      Top             =   6255
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   97976323
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   3435
      TabIndex        =   5
      Top             =   6270
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   97976323
      CurrentDate     =   38927
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Label1"
      Height          =   255
      Left            =   645
      TabIndex        =   10
      Top             =   6705
      Width           =   720
   End
   Begin VB.Label Label2 
      Caption         =   "Label1"
      Height          =   255
      Left            =   2175
      TabIndex        =   9
      Top             =   6660
      Width           =   810
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "dari"
      Height          =   255
      Left            =   1515
      TabIndex        =   8
      Top             =   6675
      Width           =   345
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   7
      Top             =   6300
      Width           =   915
   End
   Begin VB.Label Label3 
      Caption         =   "s/d"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   2850
      TabIndex        =   6
      Top             =   6300
      Width           =   450
   End
   Begin VB.Label Label7 
      Caption         =   "Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1260
      TabIndex        =   1
      Top             =   255
      Width           =   1050
   End
End
Attribute VB_Name = "frmProsesHPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim conn As New ADODB.Connection


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSimpan_Click()
Dim rs As New ADODB.Recordset
Dim noproduksi As String
Dim rs1 As New ADODB.Recordset
Dim conn1 As New ADODB.Connection

Dim no_transaksi As String
Dim nomer As String
P1.value = 0
P1.Max = 100
'On Error GoTo err

    conn.Open strcon
    P1.value = 10
    
    conn.Execute "update k set k.hpp=d.harga " & _
                 "from tmp_kartustock k  " & _
                 "left join t_belih b on b.nomer_terimabarang=k.id " & _
                 "left join t_belid d on d.nomer_beli=b.nomer_beli and d.kode_bahan=k.kode_bahan " & _
                 "and d.nomer_serial=k.nomer_serial " & _
                 "where left(k.id,2)='TB' and k.HPP=0 and d.harga<>0 and d.harga is not null"
    
    conn.Execute "update stock set hpp=d.harga from stock s inner join " & _
            "t_belid d on s.kode_bahan=d.kode_bahan and s.nomer_serial=d.nomer_serial " & _
            " where d.kode_bahan+d.nomer_serial in (select kode_bahan+nomer_serial from t_belid where substring(nomer_beli,3,4)='" & Format(DTPicker0, "yyMM") & "')"

    conn.Execute "update stock set hpp=0 where hpp=0 or hpp is null"
    conn.Execute "update t_produksi_bahan set hpp=isnull((select hpp from stock where kode_bahan=t_produksi_bahan.kode_bahan and nomer_serial=t_produksi_bahan.nomer_serial and kode_gudang=t_produksi_bahan.kode_gudang),0)"
    rs.Open "select * from t_belid where substring(nomer_beli,3,4)='" & Format(DTPicker0, "yyMM") & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        prosesproduksi rs!kode_bahan, rs!nomer_serial
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select kode_bahan,nomer_serial from t_produksi_hasil where substring(nomer_produksi,3,4)='" & Format(DTPicker0, "yyMM") & "' order by nomer_produksi", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        prosesproduksi rs!kode_bahan, rs!nomer_serial
        rs.MoveNext
    Wend
    rs.Close
    
'    conn.Execute "update tmp_kartustock set hpp=(select hpp from t_produksi_hasil where kode_bahan=tmp_kartustock.kode_bahan and nomer_produksi=tmp_kartustock.id and kode_gudang=tmp_kartustock.kode_gudang)" & _
'        "where left(id,6)='PR" & Format(DTPicker0, "yyMM") & "' and tipe='Hasil Produksi'"
        
    conn.Execute "update k set k.hpp=t.hpp " & _
                 "from tmp_kartustock k " & _
                 "left join t_produksi_hasil t on t.nomer_produksi=k.id " & _
                 "and t.kode_bahan=k.kode_bahan " & _
                 "and t.kode_gudang=k.kode_gudang " & _
                 "where left(id,6)='PR" & Format(DTPicker0, "yyMM") & "' and tipe='Hasil Produksi'"
    conn.Execute "update stock set hpp=d.hpp from stock s inner join  " & _
        "tmp_kartustock d on s.kode_bahan=d.kode_bahan and s.nomer_serial=d.nomer_serial " & _
        "where left(id,6)='PR" & Format(DTPicker0, "yyMM") & "' and tipe='Hasil Produksi'"
        
    'untuk gula -- tidak ada hasil produksi
    conn.Execute "update stock set hpp=d.hpp from stock s inner join  " & _
        "tmp_kartustock d on s.kode_bahan=d.kode_bahan and s.nomer_serial=d.nomer_serial " & _
        "where s.hpp=0 and d.tipe='Penerimaan Barang'"
        
    
    ''
        
'    conn.Execute "update t_suratjalan_timbang set hpp=(select hpp from stock where kode_bahan=t_suratjalan_timbang.kode_bahan and nomer_serial=t_suratjalan_timbang.nomer_serial and kode_gudang=t_suratjalan_timbang.kode_gudang) where substring(nomer_suratjalan,3,4)='" & Format(DTPicker0, "yyMM") & "'"
    
'    conn.Execute "update s set s.hpp= t.hpp " & _
'                "from t_suratjalan_timbang s " & _
'                "left join t_jualh h on h.nomer_order=s.nomer_order " & _
'                "left join stock t on t.kode_bahan=s.kode_bahan and t.nomer_serial=s.nomer_serial " & _
'                "and t.kode_gudang=s.kode_gudang where " & _
'                "Month(h.tanggal_jual) = '" & Format(DTPicker0, "M") & "' And Year(h.tanggal_jual) = '" & Format(DTPicker0, "YYYY") & "' "
   conn.Execute "update t_suratjalan_timbang set hpp=isnull((select hpp from stock where kode_bahan=t_suratjalan_timbang.kode_bahan and nomer_serial=t_suratjalan_timbang.nomer_serial and kode_gudang=t_suratjalan_timbang.kode_gudang),0) " & _
        "from t_suratjalan_timbang inner join t_suratjalanh on t_suratjalan_timbang.nomer_suratjalan=t_suratjalanh.nomer_suratjalan " & _
        "where Month(tanggal_suratjalan) = '" & Format(DTPicker0, "M") & "' And Year(tanggal_suratjalan) = '" & Format(DTPicker0, "YYYY") & "' "
    conn.Execute "update t_suratjalan_timbang set hpp=0 where hpp is null"
    
    
    P1.value = 15
    ''tambahan
        
    ''insert into history_posting_tmp
    conn.Execute "delete from history_posting_tmp"

    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_jual,tanggal_jual,'0' from t_jualh " & _
                 "where CONVERT(VARCHAR(10), tanggal_jual, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_jual, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
                 
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_suratjalan,tanggal_suratjalan,'0' from t_suratjalanh " & _
                 "where CONVERT(VARCHAR(10), tanggal_suratjalan, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_suratjalan, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
    
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_beli,tanggal_beli,'0' from t_belih " & _
                 "where CONVERT(VARCHAR(10), tanggal_beli, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_beli, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
    
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_returbeli,tanggal_returbeli,'0' from t_returbelih " & _
                 "where CONVERT(VARCHAR(10), tanggal_returbeli, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_returbeli, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
                 
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_returjual,tanggal_returjual,'0' from t_returjualh " & _
                 "where CONVERT(VARCHAR(10), tanggal_returjual, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_returjual, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
    
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select no_transaksi,tanggal,'0' from t_kasbankh " & _
                 "where CONVERT(VARCHAR(10), tanggal, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
    
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_bayarpiutang,tanggal_bayarpiutang,'0' from t_bayarpiutangh " & _
                 "where CONVERT(VARCHAR(10), tanggal_bayarpiutang, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_bayarpiutang, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
                 
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_bayarhutang,tanggal_bayarhutang,'0' from t_bayarhutangh " & _
                 "where CONVERT(VARCHAR(10), tanggal_bayarhutang, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal_bayarhutang, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
    
    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select nomer_bayarkuli,tanggal,'0' from t_bayarkulih " & _
                 "where CONVERT(VARCHAR(10), tanggal, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "
    
'    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
'                 "select nomer_ongkostransport,tanggal,'0' from t_ongkostransporth " & _
'                 "where CONVERT(VARCHAR(10), tanggal, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
'                 "and CONVERT(VARCHAR(10), tanggal, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "' and status_posting<>'0' "

    conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
                 "select id,tanggal,0 from t_pencairangiroh " & _
                 "where CONVERT(VARCHAR(10), tanggal, 112)>='" & Format(DTPicker1.value, "yyyyMMdd") & "' " & _
                 "and CONVERT(VARCHAR(10), tanggal, 112)<='" & Format(DTPicker2.value, "yyyyMMdd") & "'"
    
    ''tambahan
    
    P1.value = 20
    conn.Execute "Delete from t_jurnald_Tmp where no_transaksi in (select no_transaksi  from t_jurnalh_Tmp where convert(varchar(6),tanggal,112)='" & Format(DTPicker1.value, "yyyyMM") & "')"
    conn.Execute "Delete from t_jurnalh_Tmp where convert(varchar(6),tanggal,112)='" & Format(DTPicker1.value, "yyyyMM") & "'"
    
    
    
    
    
    conn.Close
    
    
    conn1.Open strcon
    rs.Open "select count(*) from history_posting_tmp  where status='0';select * from history_posting_tmp where status='0' order by tanggal asc", conn1, adOpenDynamic, adLockOptimistic
    Label2 = rs(0)
    Set rs = rs.NextRecordset
    Dim counter As Long
    Dim break As Boolean
    counter = 0
    
    While Not rs.EOF
        Label1 = counter
        no_transaksi = rs(0)
        P1.value = 20 + (counter * 80 / Val(Label2))
    If IsNumeric(Mid(no_transaksi, 3, 1)) Then
        Select Case Left(no_transaksi, 2)
            Case "PH"
                PostingJurnal_BayarHutang_Tmp no_transaksi
            Case "PP"
                PostingJurnal_BayarPiutang_Tmp no_transaksi
            Case "BK", "BM", "KM", "KK"
                PostingJurnal_KasBank_Tmp no_transaksi
            Case "PB"
                PostingJurnal_Pembelian_Tmp no_transaksi
            Case "PJ"
'                If no_transaksi = "PJ120600065" Then
'                    MsgBox no_transaksi
'                End If
                PostingJurnal_Penjualan_Tmp no_transaksi
            Case "PO", "RJ"
                postingjurnal_returjual_Tmp no_transaksi
            Case "KB"
                PostingJurnal_SuratJalan_Tmp no_transaksi
'            Case "OT"
'                postingjurnal_transport_Tmp no_transaksi
            Case "MN"
                postingjurnal_manol_Tmp no_transaksi
            Case "GM", "GS"
                postingjurnal_pencairangiro no_transaksi
        End Select
    
    End If
        conn1.Execute "update history_posting_tmp set status='1' where nomer_transaksi='" & no_transaksi & "'"
        rs.MoveNext
        counter = counter + 1
        DoEvents
    Wend
    
    conn1.Execute "insert into t_jurnalh_Tmp " & _
                 "select * from t_jurnalh where Month(tanggal) = '" & Format(DTPicker0, "M") & "' " & _
                 "And Year(tanggal) = '" & Format(DTPicker0, "YYYY") & "' and kode_pc='s'"
                 
    conn1.Execute "insert into t_jurnald_Tmp " & _
                 "select * from t_jurnald where no_transaksi in (select no_transaksi from t_jurnalh where Month(tanggal) = '" & Format(DTPicker0, "M") & "' " & _
                 "And Year(tanggal) = '" & Format(DTPicker0, "YYYY") & "' and kode_pc='s')"
    
    conn1.Close
    Set conn1 = Nothing
MsgBox "posting selesai"
    
    
    Exit Sub
err:
MsgBox err.Description
MsgBox no_transaksi
If conn.State Then conn.Close
End Sub

Private Sub prosesproduksi(kode As String, noseri As String)
Dim rs As New ADODB.Recordset
    rs.Open "select distinct nomer_produksi from t_produksi_bahan where kode_bahan+nomer_serial='" & kode & noseri & "' ", conn
    While Not rs.EOF
        hitunghpp_produksi rs(0)
        rs.MoveNext
    Wend
    rs.Close
End Sub
Private Sub hitunghpp_produksi(noproduksi As String)
Dim rs As New ADODB.Recordset
    

Dim totalhppbahan As Double
Dim totalhpphasil As Double
Dim totalhppbiaya As Double
Dim totalhppkemasan As Double
Dim hpp As Double
Dim tanggal As Date
hitunghpp = False
totalhppbahan = 0
totalhpphasil = 0
totalhppbiaya = 0
rs.Open "select tanggal_produksi from t_produksih where nomer_produksi='" & noproduksi & "'", conn
tanggal = rs(0)
rs.Close
rs.Open "select isnull(sum(berat*hpp),0) from t_produksi_bahan where nomer_produksi='" & noproduksi & "'", conn
totalhppbahan = rs(0)
rs.Close
rs.Open "select isnull(sum(biaya),0) from t_produksi_biaya where nomer_produksi='" & noproduksi & "'", conn
totalhppbiaya = rs(0)
rs.Close
rs.Open "select isnull(sum(qty*hpp),0) from t_produksi_kemasan where nomer_produksi='" & noproduksi & "'", conn
totalhppkemasan = rs(0)
rs.Close

conn.Execute "update t_produksi_hasil set hpp=0 where  nomer_produksi='" & noproduksi & "'"
rs.Open "select * from t_produksi_hasil where  nomer_produksi='" & noproduksi & "'", conn
While Not rs.EOF
    conn.Execute "update t_produksi_hasil set hpp='" & gethisthpp(rs!kode_bahan, tanggal) & "' where nomer_produksi='" & noproduksi & "' and  kode_bahan='" & rs!kode_bahan & "'"
    rs.MoveNext
Wend
rs.Close

Dim hppnol As Byte
Dim totalberathppnol As Double
hppnol = 0
rs.Open "select count(*),isnull(sum(berat),0) from t_produksi_hasil where nomer_produksi='" & noproduksi & "' and hpp=0", conn
hppnol = rs(0)
totalberathppnol = rs(1)
rs.Close
rs.Open "select isnull(sum(berat*hpp),0) from t_produksi_hasil where nomer_produksi='" & noproduksi & "'", conn
totalhpphasil = rs(0)
rs.Close
If totalberathppnol > 0 Then
    hpp = (totalhppbahan + totalhppbiaya - totalhpphasil) / totalberathppnol
    conn.Execute "update t_produksi_hasil set hpp='" & hpp & "'  where nomer_produksi='" & noproduksi & "' and hpp=0"
    rs.Open "select * from t_produksi_hasil h left join t_produksi_kemasan k on h.nomer_produksi = k.nomer_produksi where h.nomer_produksi = '" & noproduksi & "' and h.kode_bahan = k.kode_hasil", conn, adOpenKeyset
    If Not rs.EOF Then
        conn.Execute "update t_produksi_hasil set hpp=((berat*hpp)+(select isnull(sum(berat*hpp),0) from t_produksi_kemasan where nomer_produksi='" & noproduksi & "' and kode_hasil = t_produksi_hasil.kode_bahan))/berat " & _
                " where nomer_produksi='" & noproduksi & "'"
    End If
    rs.Close
End If
rs.Open "select * from t_produksi_hasil where nomer_produksi='" & noproduksi & "'", conn, adOpenKeyset
While Not rs.EOF
    conn.Execute "update stock set hpp='" & rs!hpp & "'  where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "'"
    conn.Execute "update tmp_kartustock set hpp='" & rs!hpp & "'  where id='" & noproduksi & "' and tipe='Hasil Produksi' " & _
                 "and kode_bahan='" & rs!kode_bahan & "'"
    rs.MoveNext
Wend
rs.Close
'hitunghpp = True
Exit Sub
err:
MsgBox err.Description

End Sub
Private Function gethisthpp(KodeBahan As String, tanggal As Date) As Double
Dim rs As New ADODB.Recordset
    rs.Open "select top 1 hpp from hst_hppbahan where kode_bahan='" & KodeBahan & "' and convert(varchar(20),tanggal,120)<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' order by tanggal desc", conn
    If Not rs.EOF Then
        gethisthpp = rs(0)
    Else
        gethisthpp = 0
    End If
    If rs.State Then rs.Close
End Function

Private Sub Command1_Click()
Dim rs As New ADODB.Recordset
Dim rs1 As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim SaldoAwal As Double
    conn.Open strcon
    conn.BeginTrans
'    conn.Execute "delete from tmp_kartu"
    rs.Open "select distinct kode_customer from list_piutang", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        SaldoAwal = 0
        rs1.Open "Select * from tmp_kartu where kode_customer='" & (rs!kode_customer) & "' order by tanggal", conn, adOpenDynamic, adLockOptimistic
        While Not rs1.EOF
            If (rs1!Tipe) = "Penjualan" Then
                conn.Execute "insert into tmp_kartupiutang(kode_customer, tipe, id, tanggal, debet, kredit, saldoawal) " & _
                             "values ('" & (rs1!kode_customer) & "', 'Penjualan', '" & (rs1!nomer_transaksi) & "', '" & Format((rs1!tanggal), "yyyy/MM/dd HH:mm:ss") & "', " & (rs1!jumlah) & ",0," & SaldoAwal & ")"
            Else
                conn.Execute "insert into tmp_kartupiutang(kode_customer, tipe, id, tanggal, debet, kredit, saldoawal) " & _
                             "values ('" & (rs1!kode_customer) & "', 'Bayar Piutang', '" & (rs1!nomer_transaksi) & "', '" & Format((rs1!tanggal), "yyyy/MM/dd HH:mm:ss") & "', 0," & (rs1!bayar) & "," & SaldoAwal & ")"
            End If
            
            SaldoAwal = SaldoAwal + (rs1!jumlah) - (rs1!bayar)
            
            rs1.MoveNext
        Wend
        rs1.Close
        
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    conn.Close
    MsgBox "Proses Selesai !"
End Sub

Private Sub Command2_Click()
Dim rs As New ADODB.Recordset
Dim rs1 As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim SaldoAwal As Double
    conn.Open strcon
    conn.BeginTrans
    rs.Open "select distinct kode_supplier from list_hutang", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        SaldoAwal = 0
        rs1.Open "Select * from tmp_kartu where kode_customer='" & (rs!kode_supplier) & "' order by tanggal", conn, adOpenDynamic, adLockOptimistic
        While Not rs1.EOF
            If (rs1!Tipe) = "Pembelian" Then
                conn.Execute "insert into tmp_kartuhutang(kode_supplier, tipe, id, tanggal,  kredit, debet, saldoawal) " & _
                             "values ('" & (rs1!kode_customer) & "', 'Pembelian', '" & (rs1!nomer_transaksi) & "', '" & Format((rs1!tanggal), "yyyy/MM/dd HH:mm:ss") & "', " & (rs1!jumlah) & ",0," & SaldoAwal & ")"
            Else
                conn.Execute "insert into tmp_kartuhutang(kode_supplier, tipe, id, tanggal,  kredit, debet, saldoawal) " & _
                             "values ('" & (rs1!kode_customer) & "', 'Bayar Hutang', '" & (rs1!nomer_transaksi) & "', '" & Format((rs1!tanggal), "yyyy/MM/dd HH:mm:ss") & "', 0," & (rs1!bayar) & "," & SaldoAwal & ")"
            End If
            
            SaldoAwal = SaldoAwal + (rs1!jumlah) - (rs1!bayar)
            
            rs1.MoveNext
        Wend
        rs1.Close
        
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    conn.Close
    MsgBox "Proses Selesai !"
End Sub



Private Sub Command4_Click()

End Sub

Private Sub DTPicker0_Change()
    Call DTPicker0_Validate(False)
End Sub

Private Sub DTPicker0_Validate(Cancel As Boolean)
Dim sDate As String
    sDate = "01-" & Format(DTPicker0.value, "MM") & "-" & Format(DTPicker0.value, "YYYY")
    If IsDate(sDate) = True Then
        DTPicker1.value = Format(sDate, "dd-MM-yyyy")
        DTPicker2.value = DateAdd("m", 1, DTPicker1.value)        'Cari Bulan Berikutnya
        DTPicker2.value = DateAdd("d", -1, DTPicker2.value)       'Cari Tanggal Akhir Bulan
    End If
End Sub

Private Sub Form_Load()
    Dim sDate As String
    DTPicker0.value = Date
    sDate = "01-" & Format(DTPicker0.value, "MM") & "-" & Format(DTPicker0.value, "YYYY")
    If IsDate(sDate) = True Then
        DTPicker1.value = Format(sDate, "dd-MM-yyyy")
        DTPicker2.value = DateAdd("m", 1, DTPicker1.value)        'Cari Bulan Berikutnya
        DTPicker2.value = DateAdd("d", -1, DTPicker2.value)       'Cari Tanggal Akhir Bulan
    End If
End Sub

Private Function PostingJurnal_BayarHutang_Tmp(ByRef NoTransaksi As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Currency, NilaiJurnalH As Currency
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_Kas As String, Acc_Bank As String, Acc_PiutangReturBeli As String
Dim Acc_PendapatanLain As String, Acc_BiayaLain As String
Dim Acc_UMBeli As String
Dim Acc_Hutang As String
Dim CaraBayar As String
Dim JumlahBayar As Currency, NilaiTransfer As Currency, UangMuka As Currency
Dim TotalRetur As Currency
Dim KodeBank As String
Dim selisih As Currency
Dim TotalHutang As Currency
Dim Kodesupplier As String
Dim Acc_HutangGiro As String, NilaiBG As Currency
Dim NoBG As String, TanggalCair As Date

'On Error GoTo err
    PostingJurnal_BayarHutang_Tmp = False
    conn.Open strcon
    conn.BeginTrans
    
    Acc_Kas = getAcc("kas")
    Acc_PiutangReturBeli = getAcc("piutang retur beli")
    Acc_PendapatanLain = getAcc("pendapatanlain")
    Acc_BiayaLain = getAcc("biayalain")
    Acc_UMBeli = getAcc("um pembelian")
    Acc_HutangGiro = getAcc("hutang giro")
        
    rs.Open "select tanggal_bayarhutang as tanggal,kode_supplier,jumlah_tunai, uang_muka,selisih from t_bayarhutangh where nomer_bayarhutang='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    tanggal = rs("tanggal")
    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    UangMuka = rs("uang_muka")
    Kodesupplier = rs("kode_supplier")
    rs.Close
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
    rs.Open "select sum(jumlah) as TotalRetur from t_bayarhutang_retur where nomer_bayarhutang='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF And IsNull(rs("TotalRetur")) = False Then
        TotalRetur = rs("TotalRetur")
    End If
    rs.Close
    y = 1
    NilaiTransfer = 0
    Dim rs1 As New ADODB.Recordset
    rs.Open "select * from t_bayarhutang_transfer where nomer_bayarhutang='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        
        rs1.Open "Select kode_acc from ms_bank " & _
                "where kode_bank='" & rs!Kode_Bank & "'", conn, adOpenDynamic, adLockOptimistic
        If Not rs1.EOF Then
            Acc_Bank = rs1("kode_acc")
        Else
            MsgBox "Kode Bank " & rs!Kode_Bank & " belum mempunyai Account"
            GoTo err
        End If
        rs1.Close
        NilaiTransfer = NilaiTransfer + rs!Nilai_Transfer
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_Bank, "k", rs!Nilai_Transfer, conn, "Pembayaran Hutang", Kodesupplier, , , y
        
        y = y + 1
        rs.MoveNext
    Wend
    rs.Close
    NilaiBG = 0
'    rs.Open "select * from t_bayarhutang_cek where nomer_bayarhutang='" & NoTransaksi & "'", conn
'    While Not rs.EOF
'        conn.Execute "insert into list_bg(nomer_transaksi,tanggal,nomer_bg,tanggal_cair, " & _
'                     "jenis,kode_supplier,keterangan_bank,nominal) " & _
'                    "values ('" & NoTransaksi & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & rs!no_cek & "','" & Format(rs!Tanggal_Cair, "yyyy/MM/dd HH:mm:ss") & "', " & _
'                    "'h','" & Kodesupplier & "','" & rs!Kode_Bank & "'," & Replace(rs!nilai_cek, ",", ".") & " )"
'        NilaiBG = NilaiBG + rs("nilai_cek")
'        conn.Execute "update t_belibukud set status=0 where jenis='" & rs!Jenis & "' and nomer='" & rs!no_cek & "'"
'        rs.MoveNext
'    Wend
'    rs.Close
'
    TotalHutang = JumlahBayar + UangMuka + TotalRetur + selisih + NilaiBG + NilaiTransfer
    
    JurnalD_Tmp NoTransaksi, NoJurnal, Acc_HutangGiro, "k", NilaiBG, conn, "Pembayaran Hutang", Kodesupplier, , , y
    y = y + 1
    
    rs.Open "Select acc_hutang from setting_accSupplier  " & _
            "where kode_supplier='" & Kodesupplier & "'", conn
    If Not rs.EOF Then
        Acc_Hutang = rs("acc_hutang")
    End If
    rs.Close
    
    
    
    JurnalD_Tmp NoTransaksi, NoJurnal, Acc_Hutang, "d", TotalHutang, conn, "Pembayaran Hutang", Kodesupplier, , , y
    y = y + 1

    
    If JumlahBayar > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_Kas, "k", JumlahBayar, conn, "Pembayaran Hutang", Kodesupplier, , , y
    End If
    y = y + 1
    
    If TotalRetur > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_PiutangReturBeli, "k", TotalRetur, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
        y = y + 1
    End If
    If selisih > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_BiayaLain, "k", selisih, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
        y = y + 1
    ElseIf selisih < 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_PendapatanLain, "d", selisih * -1, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
        y = y + 1
    End If
    
    If UangMuka > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_UMBeli, "k", UangMuka, conn, "Pembayaran Hutang", Kodesupplier, , , y + 1
    End If
    
    JurnalH_Tmp NoTransaksi, NoJurnal, tanggal, "Pembayaran Hutang", conn
    
'    table_name = "t_bayarhutang_beli"
'    fields(0) = "nomer_bayarhutang"
'    fields(1) = "nomer_beli"
'    fields(2) = "jumlah_bayar"
'    fields(3) = "no_urut"
'
    
'    rs.Open "select nomer_beli,jumlah_bayar from t_bayarhutang_beli " & _
'            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
'    While Not rs.EOF
'        conn.Execute "update list_hutang set total_bayar=total_bayar + " & rs("jumlah_bayar") & " " & _
'                     "where nomer_transaksi='" & rs("nomer_beli") & "'"
'
'        insertkartuHutang "Bayar Hutang", NoTransaksi, tanggal, Kodesupplier, rs("jumlah_bayar"), conn
'
'        rs.MoveNext
'    Wend
'    rs.Close
    
'    rs.Open "select nomer_retur,jumlah from t_bayarhutang_retur " & _
'            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
'    While Not rs.EOF
'        conn.Execute "update list_piutang_retur set total_bayar=total_bayar + " & rs("jumlah") & " " & _
'                     "where nomer_transaksi='" & rs("nomer_retur") & "'"
'
'        insertkartuHutang "Bayar Hutang (Retur Beli)", NoTransaksi, tanggal, Kodesupplier, rs("jumlah"), conn
'
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Execute "update ms_supplier set deposit=deposit-" & Replace(UangMuka, ",", ".") & " where kode_supplier='" & Kodesupplier & "'"
'    conn.Execute "update t_bayarhutangh set status_posting='3' where nomer_bayarhutang='" & NoTransaksi & "'"

    conn.CommitTrans
    
    DropConnection

    PostingJurnal_BayarHutang_Tmp = True

    Exit Function
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Function


Private Function PostingJurnal_BayarPiutang_Tmp(ByRef NoTransaksi As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Currency, NilaiJurnalH As Currency
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_Kas As String, Acc_Bank As String, Acc_HutangReturJual As String
Dim Acc_PendapatanLain As String, Acc_BiayaLain As String
Dim Acc_UMJual As String
Dim Acc_piutang As String
Dim CaraBayar As String
Dim JumlahBayar As Currency, NilaiTransfer As Currency, UangMuka As Currency
Dim TotalRetur As Currency
Dim KodeBank As String
Dim selisih As Currency
Dim TotalPiutang As Currency
Dim KodeCustomer As String
Dim Acc_PiutangGiroDiTangan As String, NilaiBG As Currency
Dim NoBG As String, TanggalCair As Date

'On Error GoTo err
    
    PostingJurnal_BayarPiutang_Tmp = False

    conn.Open strcon
    conn.BeginTrans
    
    Acc_Kas = getAcc("kas")
    Acc_HutangReturJual = getAcc("Hutang Retur Jual")
    Acc_PendapatanLain = getAcc("pendapatanlain")
    Acc_BiayaLain = getAcc("biayalain")
    Acc_UMJual = getAcc("um Penjualan")
    Acc_PiutangGiroDiTangan = getAcc("piutang giro")
    
    rs.Open "select tanggal_bayarPiutang as tanggal,kode_Customer,jumlah_tunai,selisih, " & _
            "uang_muka from t_bayarPiutangh where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    
    
    UangMuka = rs("uang_muka")
    
    KodeCustomer = rs("kode_Customer")
    rs.Close
    
    rs.Open "select sum(jumlah) as TotalRetur from t_bayarPiutang_retur where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    If Not rs.EOF And IsNull(rs("TotalRetur")) = False Then
        TotalRetur = rs("TotalRetur")
    End If
    rs.Close
    
    NilaiTransfer = 0
    Dim rs1 As New ADODB.Recordset
    rs.Open "select * from t_bayarpiutang_transfer where nomer_bayarpiutang='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        
        rs1.Open "Select kode_acc from ms_bank " & _
                "where kode_bank='" & rs!Kode_Bank & "'", conn
        If Not rs1.EOF Then
            Acc_Bank = rs1("kode_acc")
        Else
            MsgBox "Kode Bank " & rs!Kode_Bank & " belum mempunyai Account"
            GoTo err
        End If
        rs1.Close
        NilaiTransfer = NilaiTransfer + rs!Nilai_Transfer
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_Bank, "d", rs!Nilai_Transfer, conn, "Pembayaran Piutang", KodeCustomer, , , y
        rs.MoveNext
    Wend
    rs.Close
    NilaiBG = 0
    rs.Open "select * from t_bayarpiutang_cek where nomer_bayarpiutang='" & NoTransaksi & "'", conn
    While Not rs.EOF
        NilaiBG = NilaiBG + rs("nilai_cek")
        rs.MoveNext
    Wend
    rs.Close
    JurnalD_Tmp NoTransaksi, NoJurnal, Acc_PiutangGiroDiTangan, "d", NilaiBG, conn, "Pembayaran Piutang", KodeCustomer, , , y
    TotalPiutang = JumlahBayar + UangMuka + TotalRetur + selisih + NilaiBG + NilaiTransfer
    rs.Open "Select acc_Piutang from setting_accCustomer  " & _
            "where kode_Customer='" & KodeCustomer & "'", conn
    If Not rs.EOF Then
        Acc_piutang = rs("acc_Piutang")
    End If
    rs.Close
    
    
    
    y = 1
    
    If JumlahBayar > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_Kas, "d", JumlahBayar, conn, "Pembayaran Piutang", KodeCustomer, , , y
    End If
    
    y = y + 1
    If TotalRetur > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_HutangReturJual, "d", TotalRetur, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
        y = y + 1
    End If
    
    If UangMuka > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_UMJual, "d", UangMuka, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
        y = y + 1
    End If
    
    
    JurnalD_Tmp NoTransaksi, NoJurnal, Acc_piutang, "k", TotalPiutang, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
    y = y + 1
    
    
    If selisih < 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_PendapatanLain, "k", selisih * -1, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
    ElseIf selisih > 0 Then
        JurnalD_Tmp NoTransaksi, NoJurnal, Acc_BiayaLain, "k", selisih, conn, "Pembayaran Piutang", KodeCustomer, , , y + 1
    End If

    
    JurnalH_Tmp NoTransaksi, NoJurnal, tanggal, "Pembayaran Piutang", conn
    
'    conn.Execute "update t_bayarPiutangh set status_posting='3' where nomer_bayarPiutang='" & NoTransaksi & "'"

    conn.CommitTrans
    
    DropConnection

    PostingJurnal_BayarPiutang_Tmp = True
    
    Exit Function
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Function

Private Function PostingJurnal_KasBank_Tmp(ByRef NoTransaksi As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim AccKasBank As String
Dim NilaiJurnalD As Currency, NilaiJurnalH As Currency, NilaiJurnalD2 As Currency
Dim keterangan As String, KetBG As String
Dim NoJurnal As String
Dim ket As String

'On Error GoTo err

    PostingJurnal_KasBank_Tmp = False

    conn.Open strcon
    
    conn.BeginTrans

    Tipe = Left(NoTransaksi, 2)

    rs.Open "select tanggal,acc_kasbank,total,keterangan,ket_BG from t_kasbankh where no_transaksi='" & NoTransaksi & "'", conn
    If rs.EOF Then GoTo err
    tanggal = rs("tanggal")
    NilaiJurnalH = rs("total")
    ket = rs!keterangan
    KetBG = rs!ket_bg
    rs.Close
    
    Select Case Tipe
        Case "KM"
            keterangan = "Kas Masuk"
        Case "KK"
            keterangan = "Kas Keluar"
        Case "BM"
            keterangan = "Bank Masuk"
        Case "BK"
            keterangan = "Bank Keluar"
    End Select
    
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
    
    If Tipe = "KM" Or Tipe = "BM" Then
        
    End If
    
    rs.Open "select d.kode_acc,d.nilai,keterangan from t_kasbankd d " & _
            "where d.no_transaksi='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        y = y + 1
        NilaiJurnalD = rs("nilai")
                
        If Tipe = "KM" Or Tipe = "BM" Then
            JurnalD_Tmp NoTransaksi, NoJurnal, rs("kode_acc"), "k", NilaiJurnalD, conn, rs!keterangan, , , , y
        Else
            JurnalD_Tmp NoTransaksi, NoJurnal, rs("kode_acc"), "d", NilaiJurnalD, conn, rs!keterangan, , , , y
        End If
        
        rs.MoveNext
    Wend
    rs.Close
    y = 1
    rs.Open "select * from t_kasbankd2 where no_transaksi = '" & NoTransaksi & "'", conn, adOpenKeyset
    While Not rs.EOF
        NilaiJurnalD2 = rs!nilai
        If Tipe = "KM" Or Tipe = "BM" Then
            JurnalD_Tmp NoTransaksi, NoJurnal, rs("acc_kasbank"), "d", NilaiJurnalD2, conn, ket, KetBG, , , y
        Else
            JurnalD_Tmp NoTransaksi, NoJurnal, rs("acc_kasbank"), "k", NilaiJurnalD2, conn, ket, , , , y
        End If
        y = y + 1
        rs.MoveNext
    Wend
    
    JurnalH_Tmp NoTransaksi, NoJurnal, tanggal, keterangan, conn
    
'    conn.Execute "update t_kasbankh set status_posting='3' where no_transaksi='" & NoTransaksi & "'"

    
    conn.CommitTrans
    
    DropConnection

    PostingJurnal_KasBank_Tmp = True
    
    Exit Function
err:
    conn.RollbackTrans
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
    Debug.Print no_transaksi & " " & err.Description
End Function
'Private Function PostingJurnal_produksi_Tmp(ByRef NoTransaksi As String) As Boolean
'Dim conn As New ADODB.Connection
'Dim rs As New ADODB.Recordset
'Dim i As Integer, y As Integer
'Dim Tipe As String
'Dim row As Integer
'Dim tanggal As Date
'Dim AccKasBank As String
'Dim NilaiJurnalD As Double, NilaiJurnalH As Double
'Dim keterangan As String, KetBG As String
'Dim NoJurnal As String
'Dim ket As String
'
''On Error GoTo err
'
'    PostingJurnal_KasBank_Tmp = False
'
'    conn.Open strcon
'
'    conn.BeginTrans
'
'    Tipe = Left(NoTransaksi, 2)
'
'    rs.Open "select tanggal,acc_kasbank,total,keterangan,ket_BG from t_kasbankh where no_transaksi='" & NoTransaksi & "'", conn
'    If rs.EOF Then GoTo err
'    tanggal = rs("tanggal")
'    AccKasBank = rs("acc_kasbank")
'    NilaiJurnalH = rs("total")
'    ket = rs!keterangan
'    KetBG = rs!ket_bg
'    rs.Close
'
'    Select Case Tipe
'        Case "KM"
'            keterangan = "Kas Masuk"
'        Case "KK"
'            keterangan = "Kas Keluar"
'        Case "BM"
'            keterangan = "Bank Masuk"
'        Case "BK"
'            keterangan = "Bank Keluar"
'    End Select
'
'    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
'
'    If Tipe = "KM" Or Tipe = "BM" Then
'        JurnalD_Tmp NoTransaksi, NoJurnal, AccKasBank, "d", NilaiJurnalH, conn, ket, KetBG, , , y + 1
'    End If
'
'    rs.Open "select d.kode_acc,d.nilai,keterangan from t_kasbankd d " & _
'            "where d.no_transaksi='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
'    While Not rs.EOF
'        y = y + 1
'        NilaiJurnalD = rs("nilai")
'
'        If Tipe = "KM" Or Tipe = "BM" Then
'            JurnalD_Tmp NoTransaksi, NoJurnal, rs("kode_acc"), "k", NilaiJurnalD, conn, rs!keterangan, , , , y
'        Else
'            JurnalD_Tmp NoTransaksi, NoJurnal, rs("kode_acc"), "d", NilaiJurnalD, conn, rs!keterangan, , , , y
'        End If
'
'        rs.MoveNext
'    Wend
'    rs.Close
'
'    If Tipe = "KK" Or Tipe = "BK" Then
'        JurnalD_Tmp NoTransaksi, NoJurnal, AccKasBank, "k", NilaiJurnalH, conn, ket, , , , y + 1
'    End If
'
'    JurnalH_Tmp NoTransaksi, NoJurnal, tanggal, keterangan, conn
'
''    conn.Execute "update t_kasbankh set status_posting='3' where no_transaksi='" & NoTransaksi & "'"
'
'
'    conn.CommitTrans
'
'    DropConnection
'
'    PostingJurnal_KasBank_Tmp = True
'
'    Exit Function
'err:
'    conn.RollbackTrans
'    DropConnection
'    Debug.Print no_transaksi & " " & err.Description
'End Function
'
'Private Function PostingJurnal_manol_Tmp(ByRef NoTransaksi As String) As Boolean
'Dim conn As New ADODB.Connection
'Dim rs As New ADODB.Recordset
'Dim i As Integer, y As Integer
'Dim Tipe As String
'Dim row As Integer
'Dim tanggal As Date
'Dim AccKasBank As String
'Dim NilaiJurnalD As Double, NilaiJurnalH As Double
'Dim keterangan As String, KetBG As String
'Dim NoJurnal As String
'Dim ket As String
'
''On Error GoTo err
'
'    PostingJurnal_KasBank_Tmp = False
'
'    conn.Open strcon
'
'    conn.BeginTrans
'
'    Tipe = Left(NoTransaksi, 2)
'
'    rs.Open "select tanggal,acc_kasbank,total,keterangan,ket_BG from t_kasbankh where no_transaksi='" & NoTransaksi & "'", conn
'    If rs.EOF Then GoTo err
'    tanggal = rs("tanggal")
'    AccKasBank = rs("acc_kasbank")
'    NilaiJurnalH = rs("total")
'    ket = rs!keterangan
'    KetBG = rs!ket_bg
'    rs.Close
'
'    Select Case Tipe
'        Case "KM"
'            keterangan = "Kas Masuk"
'        Case "KK"
'            keterangan = "Kas Keluar"
'        Case "BM"
'            keterangan = "Bank Masuk"
'        Case "BK"
'            keterangan = "Bank Keluar"
'    End Select
'
'    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
'
'    If Tipe = "KM" Or Tipe = "BM" Then
'        JurnalD_Tmp NoTransaksi, NoJurnal, AccKasBank, "d", NilaiJurnalH, conn, ket, KetBG, , , y + 1
'    End If
'
'    rs.Open "select d.kode_acc,d.nilai,keterangan from t_kasbankd d " & _
'            "where d.no_transaksi='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
'    While Not rs.EOF
'        y = y + 1
'        NilaiJurnalD = rs("nilai")
'
'        If Tipe = "KM" Or Tipe = "BM" Then
'            JurnalD_Tmp NoTransaksi, NoJurnal, rs("kode_acc"), "k", NilaiJurnalD, conn, rs!keterangan, , , , y
'        Else
'            JurnalD_Tmp NoTransaksi, NoJurnal, rs("kode_acc"), "d", NilaiJurnalD, conn, rs!keterangan, , , , y
'        End If
'
'        rs.MoveNext
'    Wend
'    rs.Close
'
'    If Tipe = "KK" Or Tipe = "BK" Then
'        JurnalD_Tmp NoTransaksi, NoJurnal, AccKasBank, "k", NilaiJurnalH, conn, ket, , , , y + 1
'    End If
'
'    JurnalH_Tmp NoTransaksi, NoJurnal, tanggal, keterangan, conn
'
''    conn.Execute "update t_kasbankh set status_posting='3' where no_transaksi='" & NoTransaksi & "'"
'
'
'    conn.CommitTrans
'
'    DropConnection
'
'    PostingJurnal_KasBank_Tmp = True
'
'    Exit Function
'err:
'    conn.RollbackTrans
'    DropConnection
'    Debug.Print no_transaksi & " " & err.Description
'End Function

Private Function PostingJurnal_Pembelian_Tmp(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim i As Byte
Dim NoJurnal As String
Dim NoSuratJalan As String
Dim kode_supplier As String
Dim total As Currency
Dim gudang As String
Dim pajak As Currency
Dim Row As Integer
i = 0
PostingJurnal_Pembelian_Tmp = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select * from t_belih where nomer_beli='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    tanggal = rs!tanggal_beli
    tanggal_jatuhtempo = rs!tanggal_jatuhtempo
    kode_supplier = rs!kode_supplier
    NoSuratJalan = rs!nomer_invoice
    total = rs!total
    pajak = 0
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
    add_jurnal_Tmp NoTransaksi, NoJurnal, tanggal, "Pembelian", rs!total, "PB", conntrans

    If rs!cara_bayar = "T" Then
'        insertbayarhutangtunai NoTransaksi, rs!tanggal_beli, kode_supplier, rs!total, conntrans
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_acckas), 0, CCur(rs!total), Row, conntrans, "Pembelian", kode_supplier
    Else
'        insertkartuHutang "Pembelian", NoTransaksi, Tanggal, kode_supplier, rs!total * -1, conntrans
'        inserthutang kode_supplier, NoTransaksi, NoSuratJalan, rs!total, tanggal_jatuhtempo, conntrans

        rs.Close
        rs.Open "select acc_hutang from setting_accsupplier where kode_supplier='" & kode_supplier & "'"
        If Not rs.EOF Then add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs(0), 0, total, Row, conntrans, "Pembelian", kode_supplier
    End If
    rs.Close
    
    rs.Open "select * from t_belid d inner join setting_accbahan s on d.kode_bahan=s.kode_bahan where nomer_beli='" & NoTransaksi & "'", conn, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        'updatehpp NoTransaksi, rs!kode_bahan, gudang, rs!nomer_serial, rs!berat, rs!harga, conntrans
'        conntrans.Execute "update stock set hpp='" & rs!harga & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "'"
'        conntrans.Execute "update tmp_kartustock set hpp='" & rs!harga & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "' and id='" & NoSuratJalan & "'"
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!Acc_Persediaan, (rs!berat * rs!harga), 0, Row, conntrans, "Pembelian", rs!kode_bahan
        rs.MoveNext
    Wend
    Row = Row + 1
    
'    conntrans.Execute "update t_belih set status_posting='3' where nomer_beli='" & NoTransaksi & "'"
    PostingJurnal_Pembelian_Tmp = True
Else
    GoTo err
End If
rs.Close
conntrans.CommitTrans
PostingJurnal_Pembelian_Tmp = True
i = 0
conn.Close
conntrans.Close
Exit Function
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function

Private Function PostingJurnal_Penjualan_Tmp(NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte, hpp As Double
Dim kode_customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim noFaktur As String
Dim pajak As Double

Dim Acc_Penjualan_Lain As String
Dim Acc_Pendapatan As String

i = 0
PostingJurnal_Penjualan_Tmp = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1

rs.Open "select kode_acc from setting_coa where kode='jual lain'", conn
If Not rs.EOF Then Acc_Penjualan_Lain = rs(0)
rs.Close

If Left(NoTransaksi, 2) = "PJ" Then
    Acc_Pendapatan = var_accpendapatan
Else
    Acc_Pendapatan = Acc_Penjualan_Lain
End If

rs.Open "select * from t_jualh where nomer_jual='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    
    tanggal = rs!tanggal_jual
    kode_customer = rs!kode_customer
    noFaktur = rs!nomer_invoice
    pajak = rs!tax
    total = rs!total
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
'    insertpiutang rs!kode_customer, NoTransaksi, noFaktur, (total * (100 + pajak) / 100), rs!tanggal_jatuhtempo, conntrans
    add_jurnal_Tmp NoTransaksi, NoJurnal, tanggal, "Penjualan", (total * (100 + pajak) / 100), "PJ", conntrans
    If rs!cara_bayar = "T" Then
        
'        insertbayarpiutangtunai NoTransaksi, rs!tanggal_jual, rs!kode_customer, (total * (100 + pajak) / 100), conntrans
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_acckas), CCur((total * (100 + pajak) / 100)), 0, Row, conntrans, "Penjualan", kode_customer
    Else
        rs.Close
        rs.Open "select acc_piutang from setting_acccustomer where kode_customer='" & kode_customer & "'"
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs(0), (total * (100 + pajak) / 100), 0, Row, conntrans, "Penjualan", kode_customer
    End If
    Row = Row + 1
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(Acc_Pendapatan), 0, total, Row, conntrans, "Penjualan", kode_customer
    
    Row = Row + 1
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_accpajakjual), 0, (total * pajak / 100), Row, conntrans, "Penjualan", noFaktur
    rs.Close
    
    
    rs.Open "select d.kode_bahan,s.total,m.acc_hppjual,m.acc_persediaan,s.qty from t_juald d " & _
            "left join t_jualh t on t.nomer_jual=d.nomer_jual " & _
            "left join (select nomer_order,kode_bahan,sum(berat) as qty,sum(berat*hpp) as Total from t_suratjalan_timbang " & _
            "group by nomer_order,kode_bahan) s on s.nomer_order=t.nomer_order and s.kode_bahan=d.kode_bahan " & _
            "left join setting_accbahan m on d.kode_bahan=m.kode_bahan " & _
            "where d.nomer_jual='" & NoTransaksi & "'"
    While Not rs.EOF
        Row = Row + 1
        If rs!Acc_Persediaan <> "" Then
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs(1), Row, conntrans, "Surat Jalan", rs!kode_bahan
    '        JurnalD_Tmp NoTransaksi, NoJurnal, rs!Acc_Persediaan, "k", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
            Row = Row + 1
            
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, var_acchpp, rs(1), 0, Row, conntrans, "Surat Jalan", rs!kode_bahan
    '        JurnalD_Tmp NoTransaksi, NoJurnal, rs!acc_hppjual, "d", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
            conntrans.Execute "update t_JurnalD_Tmp set nilai='" & Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##") & "' where no_transaksi='" & NoTransaksi & "' and kode1='" & rs!kode_bahan & "'"
        Else
            MsgBox "Kode persediaan dan hpp untuk barang " & rs!kode_bahan & " belum diisi", vbCritical
        End If
        rs.MoveNext
    Wend
    
    
'    rs.Open "select s.kode_bahan,sum(s.berat*s.hpp) as total,acc_hppjual,acc_persediaan,sum(s.berat) as qty from t_suratjalan_timbang s " & _
'        " inner join setting_accbahan m on s.kode_bahan=m.kode_bahan where s.nomer_suratjalan='" & NoTransaksi & "' group by s.kode_bahan,acc_hppjual,acc_persediaan", conn, adOpenForwardOnly
'    While Not rs.EOF
'        row = row + 1
'
'        If rs!Acc_Persediaan <> "" Then
'            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs(1), row, conntrans, "Surat Jalan", rs!kode_bahan
'    '        JurnalD_Tmp NoTransaksi, NoJurnal, rs!Acc_Persediaan, "k", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
'            row = row + 1
'            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!acc_hppjual, rs(1), 0, row, conntrans, "Surat Jalan", rs!kode_bahan
'    '        JurnalD_Tmp NoTransaksi, NoJurnal, rs!acc_hppjual, "d", rs(1), "Penjualan", rs!kode_bahan, , Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##"), row
'            conntrans.Execute "update t_JurnalD_Tmp set nilai='" & Format(rs!qty, "###0.##") & " X " & Format(rs!total / rs!qty, "###0.##") & "' where no_transaksi='" & NoTransaksi & "' and kode1='" & rs!kode_bahan & "'"
'        End If
'        rs.MoveNext
'    Wend
    
    
    
    
'    conntrans.Execute "update t_jualh set status_posting='3' where nomer_jual='" & NoTransaksi & "'"
    PostingJurnal_Penjualan_Tmp = True
Else
    GoTo err
End If
If rs.State Then rs.Close
conn.Close
conntrans.CommitTrans
i = 0

Exit Function
err:
'    MsgBox err.Description
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function

Public Function PostingJurnal_SuratJalan_Tmp(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim NoJurnal As String
Dim rs As New ADODB.Recordset
Dim i As Byte
    i = 0
    PostingJurnal_SuratJalan_Tmp = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    
    Dim biayaangkut As Currency
    Dim ekspedisi As String
    Dim Row As Integer
    Row = 0
    rs.Open "select biaya_angkut,biaya_lain,kode_ekspedisi,cara_bayar,tanggal from t_suratjalanh h inner join t_suratjalan_angkut t on h.nomer_suratjalan=t.nomer_suratjalan where  h.nomer_suratjalan='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
        ekspedisi = rs!kode_ekspedisi
        biayaangkut = rs!biaya_angkut + rs!biaya_lain
        NoJurnal = newid("t_jurnalh", "no_jurnal", rs!tanggal, "JU")
        add_jurnal_Tmp NoTransaksi, NoJurnal, rs!tanggal, "Surat Jalan", biayaangkut, "SJ", conntrans
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_accbiayaangkut), CCur(biayaangkut), 0, Row, conntrans, "Biaya Angkut", ekspedisi
        Row = Row + 1
        If rs!cara_bayar = "T" Then
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_acckasangkut), 0, CCur(biayaangkut), Row, conntrans, "Biaya Angkut", ekspedisi
        Else
    
            rs.Close
            rs.Open "select acc_hutang from setting_accsupplier where kode_supplier='" & ekspedisi & "'", conntrans
            If Not rs.EOF Then add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs(0), 0, biayaangkut, Row, conntrans, "Ongkos Angkutan", ekspedisi
        End If
        If rs.State Then rs.Close

    End If
    If rs.State Then rs.Close
    
    conntrans.CommitTrans
    PostingJurnal_SuratJalan_Tmp = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    'MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function
Private Sub postingjurnal_manol_Tmp(ByRef NoTransaksi As String)
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Currency, NilaiJurnalH As Currency
Dim keterangan As String, KetBG As String
Dim NoJurnal As String
Dim AccKas As String, AccBiaya As String
Dim ket As String
Dim conn As New ADODB.Connection
On Error GoTo err

    conn.Open strcon
    
    conn.BeginTrans

    Tipe = Left(NoTransaksi, 2)
    AccKas = getAcc("kas kecil") '"110.010.0200"
    AccBiaya = getAcc("biaya manol") '"520.100.0100"

    rs.Open "select * from t_bayarkulih where nomer_bayarkuli='" & NoTransaksi & "'", conn
'    If rs.EOF Then GoTo err
    tanggal = rs("tanggal")
    NilaiJurnalH = rs("total")
    ket = "Bayar Kuli"
    rs.Close
    
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
            
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, AccBiaya, NilaiJurnalH, 0, 0, conn, ket
    
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, AccKas, 0, NilaiJurnalH, 1, conn, ket
    
    add_jurnal_Tmp NoTransaksi, NoJurnal, tanggal, ket, NilaiJurnalH, "MN", conn
    
    conn.CommitTrans
    
    DropConnection

    'MsgBox "Data sudah diposting"

    Exit Sub
err:
    conn.RollbackTrans
    MsgBox err.Description
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub
Public Function postingjurnal_transport_Tmp(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim kode_supplier As String
Dim CaraBayar As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim Nama_Supplier As String
i = 0
postingjurnal_transport_Tmp = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1
rs.Open "select h.*,m.nama_ekspedisi from t_ongkostransporth h left join ms_ekspedisi m on h.kode_ekspedisi=m.kode_ekspedisi where nomer_ongkostransport='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    tanggal = rs!tanggal
    'kode_supplier = rs!kode_ekspedisi
    total = rs!total
    CaraBayar = rs!cara_bayar
    'Nama_Supplier = rs!nama_ekspedisi
    rs.Close
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    add_jurnal_Tmp NoTransaksi, NoJurnal, tanggal, "Transport", total, "OT", conntrans
    
    If CaraBayar = "Kredit" Then
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_accHutang), 0, CCur(total), Row, conntrans, "Transport", Nama_Supplier
    Else
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_acckas), 0, CCur(total), Row, conntrans, "Transport", Nama_Supplier
    End If
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_accbiayaangkut), CCur(total), 0, Row, conntrans, "Transport", Nama_Supplier
    conntrans.Execute "update t_ongkostransporth set status_posting='1' where nomer_ongkostransport='" & NoTransaksi & "'"
    postingjurnal_transport_Tmp = True
Else
    GoTo err
End If
If rs.State Then rs.Close
connrs.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function
Private Function postingjurnal_returjual_Tmp(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim kode_customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim pajak As Double
i = 0
postingjurnal_returjual_Tmp = False
conn.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1
rs.Open "select * from t_returjualh where nomer_returjual='" & NoTransaksi & "'", conn
If Not rs.EOF Then
    'gudang = rs!kode_gudang
    tanggal = rs!tanggal_returjual
    kode_customer = rs!kode_customer
'    pajak = rs!tax
    total = rs!total
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
'    insertpiutang rs!kode_customer, NoTransaksi, rs!total, conntrans

'    inserthutangretur rs!kode_customer, NoTransaksi, rs!total, conntrans

    add_jurnal_Tmp NoTransaksi, NoJurnal, tanggal, "Penjualan", rs!total, "RJ", conntrans
    rs.Close
    rs.Open "select acc_hutangretur from setting_acccustomer where kode_customer='" & kode_customer & "'"
    If rs.EOF Then
        MsgBox "Perkiraan hutang retur untuk customer " & kode_customer & " belum diisi"
        GoTo err
    End If
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs(0), 0, total, Row, conntrans, "Retur", kode_customer
    Row = Row + 1
    add_jurnaldetail_Tmp NoTransaksi, NoJurnal, CStr(var_accreturjual), total, 0, Row, conntrans, "", kode_customer

    rs.Close
    

    rs.Open "select s.kode_bahan,sum(s.berat*s.hpp),acc_hpp,acc_persediaan from t_returjuald s " & _
        " inner join setting_accbahan m on s.kode_bahan=m.kode_bahan where s.nomer_returjual='" & NoTransaksi & "' group by s.kode_bahan,acc_hpp,acc_persediaan", conn, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!Acc_Persediaan, rs(1), 0, Row, conntrans, "Retur", rs!kode_bahan
        Row = Row + 1
        add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!acc_hpp, 0, rs(1), Row, conntrans, , "Retur", rs!kode_bahan
        rs.MoveNext
    Wend
'    conntrans.Execute "update t_returjualh set status_posting='3' where nomer_returjual='" & NoTransaksi & "'"
    postingjurnal_returjual_Tmp = True
Else
    GoTo err
End If
If rs.State Then rs.Close
conn.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If conn.State Then conn.Close
If conntrans.State Then conntrans.Close
End Function

Private Sub postingjurnal_pencairangiro(NoTransaksi As String)
    Dim trans As Boolean
    Dim kode_customer As String
    Dim tanggal As Date
    Dim total As Currency
    Dim NoJurnal As String
    On Error GoTo err
    conn.Open strcon
    conn.BeginTrans
    trans = False
    rs.Open "select * from t_pencairangiroh where id = '" & NoTransaksi & "'", conn
    If Not rs.EOF Then
        tanggal = rs!tanggal
        total = rs!total_nominalgiro
    End If
    rs.Close
    NoJurnal = newid("t_JurnalH_Tmp", "no_jurnal", tanggal, "JU")
    add_jurnal_Tmp NoTransaksi, NoJurnal, tanggal, "", total, "GM", conn
    Dim acc_giro As String
    rs.Open "select * from setting_coa where kode='" & IIf(Left(NoTransaksi, 2) = "GM", "piutang giro", "hutang giro") & "'", conn
    If Not rs.EOF Then
        acc_giro = rs!kode_acc
    End If
    rs.Close
    Dim X As Integer
    X = 0
    rs.Open "select * from t_pencairangirod where id = '" & NoTransaksi & "'", conn
    While Not rs.EOF
        If Left(NoTransaksi, 2) = "GM" Then
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, acc_giro, 0, rs!Nominal, X, conn, rs!no_giro
        Else
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, acc_giro, rs!Nominal, 0, X, conn, rs!no_giro
        End If
        X = X + 1
        rs.MoveNext
    Wend
    rs.Close
    X = 0
    rs.Open "select * from t_pencairangiro_acc where id = '" & NoTransaksi & "'", conn
    While Not rs.EOF
        If rs!Nominal > 0 Then
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!kode_acc, rs!Nominal, 0, X, conn, rs!keterangan
        Else
            add_jurnaldetail_Tmp NoTransaksi, NoJurnal, rs!kode_acc, 0, rs!Nominal, X, conn, rs!keterangan
        End If
        X = X + 1
        rs.MoveNext
    Wend
    conn.CommitTrans
    trans = True
    conn.Close
    Exit Sub
err:
    If trans = False Then conn.RollbackTrans
    If conn.State Then conn.Close
    Debug.Print NoTransaksi & " " & err.Description
End Sub


Private Sub JurnalD_Tmp(ByRef NoTransaksi As String, NoJurnal As String, kodeAcc As String, Posisi As String, nilaiJurnal As Currency, ByRef cn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String, Optional KeteranganNilai As String, Optional NomorUrut As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    
    table_name = "t_jurnald_Tmp"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    If UCase(Posisi) = "D" Then
        fields(2) = "debet"
    Else
        fields(2) = "kredit"
    End If
    fields(3) = "no_jurnal"
    fields(4) = "keterangan"
    fields(5) = "kode1"
    fields(6) = "kode2"
    fields(7) = "nilai"
    fields(8) = "no_urut"
    

    nilai(0) = NoTransaksi
    nilai(1) = kodeAcc
    nilai(2) = Replace(nilaiJurnal, ",", ".")
    nilai(3) = NoJurnal
    nilai(4) = keterangan
    nilai(5) = kode1
    nilai(6) = kode2
    nilai(7) = Replace(KeteranganNilai, ",", ".")
    nilai(8) = NomorUrut

'    tambah_data table_name, fields, nilai
    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub JurnalH_Tmp(ByRef NoTransaksi As String, NoJurnal As String, tanggal As Date, keterangan As String, ByRef cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_jurnalh_Tmp"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "tipe"
    fields(5) = "userid"

    nilai(0) = NoTransaksi
    nilai(1) = NoJurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
    nilai(4) = Left(NoTransaksi, 2)
    nilai(5) = User

'    tambah_data table_name, fields, nilai
    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub


Private Sub add_jurnal_Tmp(ByRef NoTransaksi As String, no_jurnal As String, tanggal As Date, keterangan As String, Nominal As Currency, Tipe As String, ByRef cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_jurnalh_Tmp"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
'    fields(4) = "totaldebet"
'    fields(5) = "totalkredit"
    fields(4) = "tipe"

    nilai(0) = NoTransaksi
    nilai(1) = no_jurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
'    nilai(4) = CCur(nominal)
'    nilai(5) = CCur(nominal)
    nilai(4) = Tipe

    cn.Execute tambah_data2(table_name, fields, nilai)

End Sub

Private Sub add_jurnaldetail_Tmp(ByRef NoTransaksi As String, NoJurnal As String, kode_acc As String, debet As Currency, kredit As Currency, Row As Integer, ByRef cn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_jurnald_Tmp"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "kredit"
    fields(4) = "no_urut"
    fields(5) = "no_jurnal"
    fields(6) = "keterangan"
    fields(7) = "kode1"
    fields(8) = "kode2"

    nilai(0) = NoTransaksi
    nilai(1) = kode_acc
    nilai(2) = Replace(Format(debet, "###0.##"), ",", ".")
    nilai(3) = Replace(Format(kredit, "###0.##"), ",", ".")
    nilai(4) = Row
    nilai(5) = NoJurnal
    nilai(6) = keterangan
    nilai(7) = kode1
    nilai(8) = kode2

    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub Command3_Click()
    conn.Open strcon
    conn.BeginTrans
    HitungUlang_KartuPiutang conn
    HitungUlang_KartuHutang conn
    conn.CommitTrans
    conn.Close
    MsgBox "Proses Selesai !", vbInformation
End Sub

Public Sub HitungUlang_KartuPiutang(conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset
Dim customer As String
Dim Awal As Double
        
    rs.Open "Select distinct kode_customer from tmp_kartupiutang", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        customer = rs!kode_customer
        rs2.Open "Select top 1 saldoawal from tmp_kartupiutang where kode_customer='" & rs!kode_customer & "' order by no_urut", conn, adOpenDynamic, adLockOptimistic
        Awal = rs2!SaldoAwal
        rs2.Close
        
        rs2.Open "Select * from tmp_kartupiutang where kode_customer='" & rs!kode_customer & "' order by no_urut", conn, adOpenDynamic, adLockOptimistic
        While Not rs2.EOF
            conn.Execute "Update tmp_kartupiutang set saldoawal=" & Replace(Awal, ",", ".") & " where kode_customer='" & customer & "' and no_urut='" & rs2!no_urut & "'"
            Awal = Awal + (rs2!debet) - (rs2!kredit)
            rs2.MoveNext
        Wend
        rs2.Close
        rs.MoveNext
    Wend
    rs.Close
End Sub

Public Sub HitungUlang_KartuHutang(conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset
Dim supplier As String
Dim Awal As Double
        
    rs.Open "Select distinct kode_supplier from tmp_kartuHutang", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        supplier = rs!kode_supplier
        rs2.Open "Select top 1 saldoawal from tmp_kartuHutang where kode_supplier='" & rs!kode_supplier & "' order by no_urut", conn, adOpenDynamic, adLockOptimistic
        Awal = rs2!SaldoAwal
        rs2.Close
        
        rs2.Open "Select * from tmp_kartuHutang where kode_supplier='" & rs!kode_supplier & "' order by no_urut", conn, adOpenDynamic, adLockOptimistic
        While Not rs2.EOF
            conn.Execute "Update tmp_kartuHutang set saldoawal=" & Replace(Awal, ",", ".") & " where kode_supplier='" & supplier & "' and no_urut='" & rs2!no_urut & "'"
            Awal = Awal + (rs2!kredit) - (rs2!debet)
            rs2.MoveNext
        Wend
        rs2.Close
        rs.MoveNext
    Wend
    rs.Close
End Sub

