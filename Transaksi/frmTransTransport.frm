VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmTransTransport 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ongkos Transport"
   ClientHeight    =   8640
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11550
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8640
   ScaleWidth      =   11550
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4410
      Picture         =   "frmTransTransport.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5505
      Picture         =   "frmTransTransport.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   825
      Left            =   180
      TabIndex        =   20
      Top             =   1530
      Width           =   10905
      Begin VB.TextBox txtNoTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   27
         Top             =   405
         Width           =   2025
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4140
         Picture         =   "frmTransTransport.frx":0A64
         TabIndex        =   26
         Top             =   405
         Width           =   1635
      End
      Begin VB.CommandButton cmdSearchEkspedisi 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3735
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   22
         Top             =   0
         Width           =   420
      End
      Begin VB.TextBox txtKodeEkspedisi 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   21
         Top             =   15
         Width           =   2025
      End
      Begin VB.Label Label3 
         Caption         =   "No Truk"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   25
         Top             =   450
         Width           =   960
      End
      Begin VB.Label Label19 
         Caption         =   "Ekspedisi"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   24
         Top             =   60
         Width           =   960
      End
      Begin VB.Label lblNamaEkspedisi 
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4275
         TabIndex        =   23
         Top             =   0
         Width           =   3120
      End
   End
   Begin VB.ComboBox cmbCaraBayar 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmTransTransport.frx":0B66
      Left            =   1800
      List            =   "frmTransTransport.frx":0B70
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   2430
      Width           =   2445
   End
   Begin VB.Frame frPeriode 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   465
      Left            =   180
      TabIndex        =   16
      Top             =   1035
      Width           =   7755
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Left            =   1620
         TabIndex        =   1
         Top             =   45
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   306380803
         CurrentDate     =   38927
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   3375
         TabIndex        =   2
         Top             =   45
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   306380803
         CurrentDate     =   38927
      End
      Begin VB.Label Label9 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1440
         TabIndex        =   18
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label8 
         Caption         =   "Periode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   17
         Top             =   90
         Width           =   1320
      End
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2970
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   8055
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6690
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   8055
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4230
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   8055
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5460
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   8055
      Width           =   1230
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   4425
      Left            =   225
      TabIndex        =   10
      Top             =   2880
      Width           =   11025
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   6
      AllowDelete     =   -1  'True
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   185
      Columns.Count   =   6
      Columns(0).Width=   2858
      Columns(0).Caption=   "Tanggal"
      Columns(0).Name =   "Kuli"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   4974
      Columns(1).Caption=   "Keterangan"
      Columns(1).Name =   "Keterangan"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   2461
      Columns(2).Caption=   "No Bukti"
      Columns(2).Name =   "No Bukti"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   2540
      Columns(3).Caption=   "Berat"
      Columns(3).Name =   "Berat"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   2487
      Columns(4).Caption=   "Ongkos"
      Columns(4).Name =   "Ongkos"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2434
      Columns(5).Caption=   "Ongkos lain"
      Columns(5).Name =   "Ongkos lain"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   19447
      _ExtentY        =   7805
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   3975
      Picture         =   "frmTransTransport.frx":0B83
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   165
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTPicker3 
      Height          =   345
      Left            =   1815
      TabIndex        =   0
      Top             =   630
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   306380803
      CurrentDate     =   38927
   End
   Begin VB.Label Label2 
      Caption         =   "Cara Bayar :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   19
      Top             =   2475
      Width           =   1410
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   11
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   7515
      Width           =   2040
   End
   Begin VB.Label lblTotalBerat 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5355
      TabIndex        =   8
      Top             =   7515
      Width           =   2040
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7605
      TabIndex        =   7
      Top             =   7515
      Width           =   2040
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1815
      TabIndex        =   6
      Top             =   135
      Width           =   2040
   End
   Begin VB.Label Label31 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   5
      Top             =   195
      Width           =   1320
   End
End
Attribute VB_Name = "frmTransTransport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Double
Dim totalQty, totalberat As Double
Dim berathapus, ongkoshapus As Double

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_mutasibahan] from t_stockmutasibahanH where [nomer_mutasibahan]>'" & lblNoTrans & "' order by [nomer_mutasibahan]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
    
    simpan
    If posting_transport(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
    conn.Open strcon
        total = 0
        totalberat = 0
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select * from vw_rekaptransport where convert(varchar(12),tanggal,111) between '" & Format(DTPicker1, "yyyy/MM/dd") & "' and '" & Format(DTPicker2, "yyyy/MM/dd") & "' and fg_bayarangkut=0 " & _
            " and kode_ekspedisi='" & txtKodeEkspedisi & "' " & IIf(txtNoTruk <> "", " and no_truk='" & txtNoTruk & "'", "") & " order by tanggal", conn
        While Not rs.EOF
            DBGrid.AddItem Format(rs!tanggal, "dd/MM/yyyy") & "#" & rs!nama & "#" & rs!nomer & "#" & rs!berat & "#0#0"
            totalberat = totalberat + rs!berat
            rs.MoveNext
        Wend
        rs.Close
    conn.Close
    lblTotal = 0
    lblTotalBerat = totalberat
End Sub
Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select t.* from t_ongkostransportH t " & _
            "where t.nomer_ongkostransport ='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker3 = rs("tanggal")
        DTPicker1 = rs("periode_awal")
        DTPicker2 = rs("periode_akhir")
        frPeriode.Visible = False
        SetComboText rs!cara_bayar, cmbCaraBayar
        txtKodeEkspedisi = rs!kode_ekspedisi
        total = 0
        totalberat = 0
        
        If rs!status_posting = 1 Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
        End If
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select tanggal,nama,d.nomer,berat,ongkos,ongkoslain " & _
                "from t_ongkostransportD d inner join vw_rekaptransport v on d.nomer=v.nomer " & _
                "where d.nomer_ongkostransport='" & lblNoTrans & "' order by d.no_urut", conn, adOpenDynamic, adLockOptimistic
        While Not rs.EOF
            DBGrid.AddItem Format(rs(0), "dd/MM/yyyy") & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & Format(rs(4), "#,##0") & "#" & Format(rs(5), "#,##0")
            totalberat = totalberat + rs(3)
            total = total + rs(4)
            rs.MoveNext
        Wend
        rs.Close
        
        lblTotalBerat = totalberat
        lblTotal = total
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    id = lblNoTrans
    If cmbCaraBayar.text = "" Then
        MsgBox "Pilih cara pembayaran terlebih dahulu"
        Exit Function
    End If
    If txtKodeEkspedisi.text = "" Or lblNamaEkspedisi = "" Then
        MsgBox "Pilih ekspedisi terlebih dahulu"
        Exit Function
    End If
    conn.Open strcon
    
    conn.BeginTrans
    DBGrid.Update
    i = 1
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_ongkostransportH", "nomer_ongkostransport", DTPicker3, "OT")
    Else
        conn.Execute "delete from t_ongkostransportH where nomer_ongkostransport='" & lblNoTrans & "'"
        conn.Execute "delete from t_ongkostransportD where nomer_ongkostransport='" & lblNoTrans & "'"
    End If
    add_dataheader
    
    For row = 1 To DBGrid.Rows
        add_datadetail (row)
        Select Case Left(DBGrid.Columns(2).CellText(row - 1), 2)
        Case "TB": conn.Execute "update t_terimabarangh set fg_bayarangkut=1 where nomer_terimabarang='" & DBGrid.Columns(1).CellText(row - 1) & "'"
        Case "SJ": conn.Execute "update t_suratjalanh set fg_bayarangkut=1 where nomer_suratjalan='" & DBGrid.Columns(1).CellText(row - 1) & "'"
        Case "KR": conn.Execute "update t_konsireturh set fg_bayarangkut=1 where nomer_konsiretur='" & DBGrid.Columns(1).CellText(row - 1) & "'"
        Case "KT": conn.Execute "update t_konsitambahh set fg_bayarangkut=1 where nomer_konsitambah='" & DBGrid.Columns(1).CellText(row - 1) & "'"
        Case "RB": conn.Execute "update t_returbelih set fg_bayarangkut=1 where nomer_returbeli='" & DBGrid.Columns(1).CellText(row - 1) & "'"
        Case "RJ": conn.Execute "update t_returjualh set fg_bayarangkut=1 where nomer_returjual='" & DBGrid.Columns(1).CellText(row - 1) & "'"
        
        End Select
        
    Next
    
    conn.CommitTrans
    DropConnection
    simpan = True
  
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(8)
    ReDim nilai(8)
    table_name = "t_ongkostransportH"
    fields(0) = "nomer_ongkostransport"
    fields(1) = "tanggal"
    fields(2) = "periode_awal"
    fields(3) = "periode_akhir"
    
    fields(4) = "Total"
    fields(5) = "Userid"
    fields(6) = "cara_bayar"
    fields(7) = "kode_ekspedisi"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker3, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = Format(DTPicker2, "yyyy/MM/dd HH:mm:ss")
    
    nilai(4) = total
    nilai(5) = User
    nilai(6) = cmbCaraBayar.text
    nilai(7) = txtKodeEkspedisi
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim selisih As Double
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "t_ongkostransportD"
    fields(0) = "nomer_ongkostransport"
    fields(1) = "nomer"
    fields(2) = "ongkos"
    fields(4) = "ongkoslain"
    fields(3) = "no_urut"
    
       
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(2).CellText(row - 1)
    nilai(2) = Format(DBGrid.Columns(4).CellText(row - 1), "###0")
    nilai(3) = row
    nilai(4) = Format(DBGrid.Columns(5).CellText(row - 1), "###0")
    tambah_data table_name, fields, nilai
End Sub

Private Sub cmdSearchEkspedisi_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchEkspedisi
    frmSearch.nmform = "frmTransTransport"
    frmSearch.nmctrl = "txtKodeEkspedisi"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_ekspedisi"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_ekspedisi"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchOngkosTransport & " where status_posting=0"
    frmSearch.nmform = "frmTransManol"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "manol"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub dbgrid_AfterColUpdate(ByVal ColIndex As Integer)
    DBGrid.Columns(ColIndex).text = Format(DBGrid.Columns(ColIndex).text, "#,##0")
    DBGrid.Update
End Sub

Private Sub DBGrid_AfterDelete(RtnDispErrMsg As Integer)
    hitungtotal
    berathapus = 0
    ongkoshapus = 0
End Sub

Private Sub DBGrid_AfterUpdate(RtnDispErrMsg As Integer)
    hitungtotal
End Sub

Private Sub reset_form()
    total = 0
    totalberat = 0
    lblNoTrans = "-"
    txtKodeEkspedisi = ""
    DBGrid.RemoveAll
    lblTotalBerat = totalberat
    lblTotal = total
    frPeriode.Visible = True
End Sub

Private Sub DBGrid_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    berathapus = DBGrid.Columns(3).text
    ongkoshapus = DBGrid.Columns(4).text
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
        
    End If
End Sub

Private Sub Form_Load()
    DTPicker1 = Now
    DTPicker2 = Now
    total = 0
    totalberat = 0
    DTPicker3 = Now
End Sub
Public Sub cek_ekspedisi()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_ekspedisi] from ms_ekspedisi where kode_ekspedisi='" & txtKodeEkspedisi & "'", conn
    If Not rs.EOF Then
        lblNamaEkspedisi = rs(0)
    Else
        lblNamaEkspedisi = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub


Private Sub txtKodeEkspedisi_LostFocus()
    cek_ekspedisi
End Sub
Private Sub hitungtotal()
    total = 0
    For i = 0 To DBGrid.Rows - 1
        total = total + CDbl(DBGrid.Columns(4).CellValue(i)) + CDbl(DBGrid.Columns(5).CellValue(i))
        
    Next
    lblTotal = Format(total, "#,##0")
End Sub
