VERSION 5.00
Begin VB.Form frmMasterKategoriSupplier 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Kategori"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6150
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   6150
   Begin VB.TextBox txtAccHutangRetur 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2025
      MaxLength       =   25
      TabIndex        =   2
      Top             =   1080
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearchAccHutangRetur 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      TabIndex        =   12
      Top             =   1080
      Width           =   465
   End
   Begin VB.TextBox txtAccPiutang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2025
      MaxLength       =   25
      TabIndex        =   1
      Top             =   675
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearchAccPiutang 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      TabIndex        =   11
      Top             =   675
      Width           =   465
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3593
      Picture         =   "frmMasterKategoriSupplier.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2595
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1973
      Picture         =   "frmMasterKategoriSupplier.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2595
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5355
      Picture         =   "frmMasterKategoriSupplier.frx":0204
      TabIndex        =   8
      Top             =   270
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   2025
      TabIndex        =   0
      Top             =   270
      Width           =   3255
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   488
      Picture         =   "frmMasterKategoriSupplier.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2595
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label24 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Piutang Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1125
      Width           =   1515
   End
   Begin VB.Label Label20 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Hutang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   720
      Width           =   960
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   10
      Top             =   315
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   2235
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1845
      TabIndex        =   7
      Top             =   315
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   300
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterKategoriSupplier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_supplier where kategori='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        DropConnection
        Exit Sub
    End If
    rs.Close
    
    
    conn.BeginTrans
    conn.Execute "delete from var_kategorisupplier where kategori='" & txtField(0).text & "'"
    conn.CommitTrans
    
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = "select kategori from var_kategorisupplier" ' order by kategori"
    frmSearch.nmform = "frmMasterkategori"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "kategori"
    frmSearch.Col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchAccHutangRetur_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1"
    frmSearch.nmform = "frmMasterKategorisupplier"
    frmSearch.nmctrl = "txtAccHutangRetur"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccPiutang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and nama like '%HUTANG%'"
    frmSearch.nmform = "frmMasterKategorisupplier"
    frmSearch.nmctrl = "txtAccPiutang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal

End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        txtField(0).SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    conn.Execute "delete from var_kategorisupplier where kategori='" & txtField(0).text & "'"
    conn.Execute "insert into var_kategorisupplier values ('" & txtField(0).text & "','" & txtAccPiutang & "','" & txtAccHutangRetur & "')"
    If MsgBox("Hendak update Setting Acc semua barang dan subkategori untuk Kategori " & txtField(0).text & "?", vbYesNo) = vbYes Then
    conn.Execute "update setting_accsupplier set acc_hutang='" & txtAccPiutang & "',acc_piutangretur='" & txtAccHutangRetur & "' from ms_supplier m inner join setting_accsupplier s on m.kode_supplier=s.kode_supplier where m.kategori='" & txtField(0) & "'"
    End If
    
    conn.CommitTrans
    
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(0).SetFocus
    txtAccPiutang = var_accHutang
    txtAccHutangRetur = var_accpiutangretur
End Sub
Public Sub cari_data()
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from var_kategorisupplier where kategori='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        ttxtAccPiutang = rs!Acc_Hutang
        txtAccHutangRetur = rs!acc_piutangretur
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub


Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    reset_form
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtAccHutangRetur_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchAccHutangRetur_Click
End Sub
Private Sub txtAccPiutang_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchAccPiutang_Click
End Sub
Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub



