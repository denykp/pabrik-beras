VERSION 5.00
Begin VB.Form frmTutupBukuBulanan 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tutup Buku Bulanan"
   ClientHeight    =   2715
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtTahun 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2145
      TabIndex        =   4
      Top             =   945
      Width           =   1035
   End
   Begin VB.TextBox txtBulan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2145
      TabIndex        =   3
      Top             =   480
      Width           =   1035
   End
   Begin VB.CommandButton cmdProses 
      Caption         =   "Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1425
      TabIndex        =   2
      Top             =   1815
      Width           =   1770
   End
   Begin VB.Label Label2 
      Caption         =   "Tahun"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1395
      TabIndex        =   1
      Top             =   1005
      Width           =   675
   End
   Begin VB.Label Label1 
      Caption         =   "Bulan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1395
      TabIndex        =   0
      Top             =   555
      Width           =   615
   End
End
Attribute VB_Name = "frmTutupBukuBulanan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdProses_Click()
Dim sBulanPrev As String, sTahunPrev As String
Dim sBulanNext As String, sTahunNext As String
Dim SaldoAwal As Long, BulanAwal As String, TahunAwal As String
Dim AccLaba As String, Laba As Double
    
    If MsgBox("Setelah dilakukan proses tutup buku, maka tidak boleh ada transaksi lagi dalam periode tersebut. " & vbCrLf & _
              "Apakah Anda yakin akan melakukan proses tutup buku ?", vbYesNo + vbQuestion, "Konfirmasi Proses Tutup Buku") = vbNo Then
        Exit Sub
    End If
    
    If txtBulan.text = "" Or txtTahun.text = "" Then
        MsgBox "Bulan dan Tahun harus diisi !", vbExclamation
        Exit Sub
    End If
    
    If Val(txtBulan.text) <= 0 Or Val(txtBulan.text) > 12 Then
        MsgBox "Bulan tidak valid !", vbExclamation
        Exit Sub
    End If
    
    txtBulan.text = Val(txtBulan.text)
    txtTahun.text = Val(txtTahun.text)
    
    sBulanPrev = BulanPrev(txtBulan.text, txtTahun.text)
    sTahunPrev = TahunPrev(txtBulan.text, txtTahun.text)
    sBulanNext = BulanNext(txtBulan.text, txtTahun.text)
    sTahunNext = TahunNext(txtBulan.text, txtTahun.text)
    
    conn.ConnectionString = strcon
    
    conn.Open
    
    AccLaba = getAcc("laba berjalan")
    
    rs.Open "select sum(d.kredit-d.debet) as Laba from (t_jurnald d " & _
            "inner join t_jurnalh t on t.no_transaksi=d.no_transaksi) " & _
            "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
            "where CONVERT(VARCHAR(7), t.tanggal, 111)='" & txtBulan & "/" & txtTahun & "' " & _
            "and (p.kategori<>'HT' and p.kategori<>'KW' and p.kategori<>'MD')", conn
    Laba = 0
    If Not rs.EOF And IsNull(rs!Laba) = False Then
        Laba = (rs!Laba)
    End If
    rs.Close
'
'    rs.Open "select SaldoAkhir as LabaSebelumnya from mutasi_coa " & _
'            "where kode_acc='" & AccLaba & "' and bulan='" & sBulanPrev & "' and tahun='" & sTahunPrev & "' ", conn
'    If Not rs.EOF And IsNull(rs!LabaSebelumnya) = False Then
'        Laba = Laba + (rs!LabaSebelumnya)
'    End If
'    rs.Close
    
    rs.Open "Select * from t_jurnalh where month(tanggal)='" & txtBulan.text & "' and  year(tanggal)='" & txtTahun.text & "'", conn
    If rs.EOF Then
        rs.Close
        conn.Close
        MsgBox "Belum ada transaksi dalam periode ini.  " & vbCrLf & _
               "Proses tutup buku tidak dapat dilanjutkan !", vbExclamation
        Exit Sub
    End If
    rs.Close
    
    
    rs.Open "select * from mutasi_coa ", conn
    If rs.EOF Then
        conn.BeginTrans
        conn.Execute "insert into mutasi_coa (bulan,tahun,kode_acc,saldoawal,debet,kredit,saldoakhir) " & _
                     "SELECT '" & txtBulan.text & "','" & txtTahun.text & "', i.kode_acc, 0, IsNull(b.totaldebet,0) AS debet, IsNull(b.totalkredit,0) AS kredit, " & _
                    "(CASE WHEN p.kategori='HT' or p.kategori='HPP' or p.kategori='BO' or p.kategori='BNO' or p.kategori='BL' THEN IsNull(b.totaldebet,0)-IsNull(b.totalkredit,0) ELSE IsNull(b.totalkredit,0)-IsNull(b.totaldebet,0) END) AS akhir " & _
                    "FROM (ms_coa AS i  " & _
                    "LEFT JOIN ms_parentAcc p on p.kode_parent=i.kode_parent) " & _
                    "LEFT JOIN (select  d.kode_acc,sum(d.debet) as totaldebet,sum(d.kredit) as totalkredit from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi where month(h.tanggal)='" & txtBulan.text & "' and  year(h.tanggal)='" & txtTahun.text & "' group by d.kode_acc) AS b ON i.kode_acc = b.kode_acc "
        rs.Close
        
        conn.Execute "Update mutasi_coa set kredit=" & Replace(Laba, ",", ".") & ",saldoakhir=" & Replace(Laba, ",", ".") & "  where kode_acc='" & AccLaba & "' and bulan='" & txtBulan.text & "' and tahun='" & txtTahun.text & "'"
        conn.Execute "Update periode_aktif set bulan='" & sBulanNext & "',tahun='" & sTahunNext & "'"
        conn.CommitTrans
        
        conn.Close
        MsgBox "Proses Selesai !", vbInformation
        Exit Sub
    End If
    rs.Close
    
    
        
    If txtBulan.text <> gBulanAwal Or txtTahun.text <> gTahunAwal Then
        rs.Open "select * from mutasi_coa where bulan='" & sBulanPrev & "' and tahun='" & sTahunPrev & "' ", conn
        If rs.EOF Then
            MsgBox "Bulan sebelumnya belum ditutup !", vbExclamation
            rs.Close
            conn.Close
            Exit Sub
        End If
        rs.Close
    End If
    
    rs.Open "select * from mutasi_coa where bulan='" & txtBulan.text & "' and tahun='" & txtTahun.text & "' ", conn
    If Not rs.EOF Then
        If MsgBox("Bulan ini sudah ditutup, apakah mau diproses ulang?", vbYesNo + vbQuestion) = vbNo Then
            rs.Close
            conn.Close
            Exit Sub
        Else
            conn.BeginTrans
            conn.Execute "Delete from mutasi_coa where bulan='" & txtBulan.text & "' and tahun='" & txtTahun.text & "' "
        End If
    Else
        conn.BeginTrans
    End If
    rs.Close
    
    conn.Execute "insert into mutasi_coa (bulan,tahun,kode_acc,saldoawal,debet,kredit,saldoakhir) " & _
                 "SELECT '" & txtBulan.text & "','" & txtTahun.text & "', i.kode_acc, IsNull(m.saldoakhir,0) AS awal, IsNull(b.totaldebet,0) AS debet, IsNull(b.totalkredit,0) AS kredit, " & _
                 "(CASE WHEN p.kategori='HT' or p.kategori='HPP' or p.kategori='BO' or p.kategori='BNO' or p.kategori='BL' THEN IsNull(m.saldoakhir,0)+IsNull(b.totaldebet,0)-IsNull(b.totalkredit,0) ELSE IsNull(m.saldoakhir,0)+IsNull(b.totalkredit,0)-IsNull(b.totaldebet,0) END ) AS akhir " & _
                 "FROM ((ms_coa AS i  " & _
                 "LEFT JOIN ms_parentAcc p on p.kode_parent=i.kode_parent) " & _
                 "LEFT JOIN (select  d.kode_acc,sum(d.debet) as totaldebet,sum(d.kredit) as totalkredit from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi where month(h.tanggal)='" & txtBulan.text & "' and  year(h.tanggal)='" & txtTahun.text & "' group by d.kode_acc) AS b ON i.kode_acc = b.kode_acc) " & _
                 "LEFT JOIN (select  kode_acc,saldoakhir  from mutasi_coa where bulan = '" & sBulanPrev & "' and tahun='" & sTahunPrev & "') AS m ON i.kode_acc = m.kode_acc "
    
    conn.Execute "Update mutasi_coa set kredit=" & Replace(Laba, ",", ".") & ",saldoakhir=saldoawal+" & Replace(Laba, ",", ".") & " where kode_acc='" & AccLaba & "' and bulan='" & txtBulan.text & "' and tahun='" & txtTahun.text & "'"
    conn.Execute "Update periode_aktif set bulan='" & sBulanNext & "',tahun='" & sTahunNext & "'"
    
    conn.CommitTrans
    
    conn.Close
    
    
    
    MsgBox "Proses Selesai !", vbInformation
    Unload Me
End Sub

Private Sub Form_Load()
Dim i As Integer

    conn.ConnectionString = strcon
    
    conn.Open

    rs.Open "Select * from periode_aktif", conn
    If Not rs.EOF Then
        txtBulan.text = rs("bulan")
        txtTahun.text = rs("tahun")
    End If
    rs.Close
    
    conn.Close

End Sub
