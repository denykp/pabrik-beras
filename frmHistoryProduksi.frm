VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{DEF7CADD-83C0-11D0-A0F1-00A024703500}#7.0#0"; "todg7.ocx"
Begin VB.Form frmHistoryProduksi 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " History Produksi"
   ClientHeight    =   7980
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15135
   Icon            =   "frmHistoryProduksi.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7980
   ScaleWidth      =   15135
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   675
      Left            =   30
      TabIndex        =   0
      Top             =   -60
      Width           =   15090
      Begin VB.CommandButton bKeluar 
         BackColor       =   &H00E0E0E0&
         Caption         =   "K&eluar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   13125
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   195
         Width           =   1755
      End
      Begin VB.CommandButton bProses 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Tampilkan Data"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   9525
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   195
         Width           =   1755
      End
      Begin VB.CommandButton bPreview 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&Preview"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   11325
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   195
         Width           =   1755
      End
   End
   Begin VB.Frame Frame2 
      Height          =   7425
      Left            =   30
      TabIndex        =   1
      Top             =   540
      Width           =   15090
      Begin MSComDlg.CommonDialog CD 
         Left            =   9675
         Top             =   405
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton bExport 
         BackColor       =   &H00E0E0E0&
         Caption         =   "E&xport ke EXCEL"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   10980
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   6720
         Width           =   2025
      End
      Begin VB.Frame Frame3 
         Height          =   5595
         Left            =   45
         TabIndex        =   2
         Top             =   915
         Width           =   14985
         Begin TrueOleDBGrid70.TDBGrid DataGrid1 
            Height          =   5280
            Index           =   0
            Left            =   45
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   135
            Width           =   14880
            _ExtentX        =   26247
            _ExtentY        =   9313
            _LayoutType     =   4
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).Caption=   "No"
            Columns(0).DataField=   ""
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).Caption=   "Tanggal"
            Columns(1).DataField=   ""
            Columns(1).NumberFormat=   "dd/MM/yy"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(2)._VlistStyle=   0
            Columns(2)._MaxComboItems=   5
            Columns(2).Caption=   "No. Potong"
            Columns(2).DataField=   ""
            Columns(2)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(3)._VlistStyle=   0
            Columns(3)._MaxComboItems=   5
            Columns(3).Caption=   "Proses Potong "
            Columns(3).DataField=   ""
            Columns(3)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(4)._VlistStyle=   0
            Columns(4)._MaxComboItems=   5
            Columns(4).Caption=   "Qty"
            Columns(4).DataField=   ""
            Columns(4)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(5)._VlistStyle=   0
            Columns(5)._MaxComboItems=   5
            Columns(5).DataField=   ""
            Columns(5)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(6)._VlistStyle=   0
            Columns(6)._MaxComboItems=   5
            Columns(6).Caption=   "Tanggal"
            Columns(6).DataField=   ""
            Columns(6).NumberFormat=   "dd/MM/yy"
            Columns(6)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(7)._VlistStyle=   0
            Columns(7)._MaxComboItems=   5
            Columns(7).Caption=   "No. Cetak"
            Columns(7).DataField=   ""
            Columns(7)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(8)._VlistStyle=   0
            Columns(8)._MaxComboItems=   5
            Columns(8).Caption=   "Proses Cetak"
            Columns(8).DataField=   ""
            Columns(8)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(9)._VlistStyle=   0
            Columns(9)._MaxComboItems=   5
            Columns(9).Caption=   "Qty"
            Columns(9).DataField=   ""
            Columns(9)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(10)._VlistStyle=   0
            Columns(10)._MaxComboItems=   5
            Columns(10).Caption=   " "
            Columns(10).DataField=   ""
            Columns(10)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(11)._VlistStyle=   0
            Columns(11)._MaxComboItems=   5
            Columns(11).Caption=   "Tanggal"
            Columns(11).DataField=   ""
            Columns(11).NumberFormat=   "dd/MM/yy"
            Columns(11)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(12)._VlistStyle=   0
            Columns(12)._MaxComboItems=   5
            Columns(12).Caption=   "No. Plong"
            Columns(12).DataField=   ""
            Columns(12)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(13)._VlistStyle=   0
            Columns(13)._MaxComboItems=   5
            Columns(13).Caption=   "Proses Plong"
            Columns(13).DataField=   ""
            Columns(13)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(14)._VlistStyle=   0
            Columns(14)._MaxComboItems=   5
            Columns(14).Caption=   "Qty"
            Columns(14).DataField=   ""
            Columns(14)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   15
            Splits(0)._UserFlags=   0
            Splits(0).MarqueeStyle=   3
            Splits(0).AllowRowSizing=   0   'False
            Splits(0).RecordSelectors=   0   'False
            Splits(0).RecordSelectorWidth=   185
            Splits(0)._SavedRecordSelectors=   0   'False
            Splits(0).ScrollBars=   3
            Splits(0).AlternatingRowStyle=   -1  'True
            Splits(0).DividerColor=   12632256
            Splits(0).SpringMode=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=15"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=714"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=635"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0)._ColStyle=516"
            Splits(0)._ColumnProps(6)=   "Column(0).Visible=0"
            Splits(0)._ColumnProps(7)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(8)=   "Column(1).Width=1349"
            Splits(0)._ColumnProps(9)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(10)=   "Column(1)._WidthInPix=1270"
            Splits(0)._ColumnProps(11)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(12)=   "Column(1)._ColStyle=516"
            Splits(0)._ColumnProps(13)=   "Column(1).Order=2"
            Splits(0)._ColumnProps(14)=   "Column(2).Width=2196"
            Splits(0)._ColumnProps(15)=   "Column(2).DividerColor=0"
            Splits(0)._ColumnProps(16)=   "Column(2)._WidthInPix=2117"
            Splits(0)._ColumnProps(17)=   "Column(2)._EditAlways=0"
            Splits(0)._ColumnProps(18)=   "Column(2)._ColStyle=516"
            Splits(0)._ColumnProps(19)=   "Column(2).Order=3"
            Splits(0)._ColumnProps(20)=   "Column(3).Width=4392"
            Splits(0)._ColumnProps(21)=   "Column(3).DividerColor=0"
            Splits(0)._ColumnProps(22)=   "Column(3)._WidthInPix=4313"
            Splits(0)._ColumnProps(23)=   "Column(3)._EditAlways=0"
            Splits(0)._ColumnProps(24)=   "Column(3)._ColStyle=516"
            Splits(0)._ColumnProps(25)=   "Column(3).Order=4"
            Splits(0)._ColumnProps(26)=   "Column(4).Width=2778"
            Splits(0)._ColumnProps(27)=   "Column(4).DividerColor=0"
            Splits(0)._ColumnProps(28)=   "Column(4)._WidthInPix=2699"
            Splits(0)._ColumnProps(29)=   "Column(4)._EditAlways=0"
            Splits(0)._ColumnProps(30)=   "Column(4)._ColStyle=516"
            Splits(0)._ColumnProps(31)=   "Column(4).Order=5"
            Splits(0)._ColumnProps(32)=   "Column(5).Width=582"
            Splits(0)._ColumnProps(33)=   "Column(5).DividerColor=0"
            Splits(0)._ColumnProps(34)=   "Column(5)._WidthInPix=503"
            Splits(0)._ColumnProps(35)=   "Column(5)._EditAlways=0"
            Splits(0)._ColumnProps(36)=   "Column(5)._ColStyle=516"
            Splits(0)._ColumnProps(37)=   "Column(5).Order=6"
            Splits(0)._ColumnProps(38)=   "Column(6).Width=1429"
            Splits(0)._ColumnProps(39)=   "Column(6).DividerColor=0"
            Splits(0)._ColumnProps(40)=   "Column(6)._WidthInPix=1349"
            Splits(0)._ColumnProps(41)=   "Column(6)._EditAlways=0"
            Splits(0)._ColumnProps(42)=   "Column(6)._ColStyle=516"
            Splits(0)._ColumnProps(43)=   "Column(6).Order=7"
            Splits(0)._ColumnProps(44)=   "Column(7).Width=2090"
            Splits(0)._ColumnProps(45)=   "Column(7).DividerColor=0"
            Splits(0)._ColumnProps(46)=   "Column(7)._WidthInPix=2011"
            Splits(0)._ColumnProps(47)=   "Column(7)._EditAlways=0"
            Splits(0)._ColumnProps(48)=   "Column(7)._ColStyle=516"
            Splits(0)._ColumnProps(49)=   "Column(7).Order=8"
            Splits(0)._ColumnProps(50)=   "Column(8).Width=4551"
            Splits(0)._ColumnProps(51)=   "Column(8).DividerColor=0"
            Splits(0)._ColumnProps(52)=   "Column(8)._WidthInPix=4471"
            Splits(0)._ColumnProps(53)=   "Column(8)._EditAlways=0"
            Splits(0)._ColumnProps(54)=   "Column(8)._ColStyle=516"
            Splits(0)._ColumnProps(55)=   "Column(8).Order=9"
            Splits(0)._ColumnProps(56)=   "Column(9).Width=2831"
            Splits(0)._ColumnProps(57)=   "Column(9).DividerColor=0"
            Splits(0)._ColumnProps(58)=   "Column(9)._WidthInPix=2752"
            Splits(0)._ColumnProps(59)=   "Column(9)._EditAlways=0"
            Splits(0)._ColumnProps(60)=   "Column(9)._ColStyle=516"
            Splits(0)._ColumnProps(61)=   "Column(9).Order=10"
            Splits(0)._ColumnProps(62)=   "Column(10).Width=661"
            Splits(0)._ColumnProps(63)=   "Column(10).DividerColor=0"
            Splits(0)._ColumnProps(64)=   "Column(10)._WidthInPix=582"
            Splits(0)._ColumnProps(65)=   "Column(10)._EditAlways=0"
            Splits(0)._ColumnProps(66)=   "Column(10)._ColStyle=516"
            Splits(0)._ColumnProps(67)=   "Column(10).Order=11"
            Splits(0)._ColumnProps(68)=   "Column(11).Width=1482"
            Splits(0)._ColumnProps(69)=   "Column(11).DividerColor=0"
            Splits(0)._ColumnProps(70)=   "Column(11)._WidthInPix=1402"
            Splits(0)._ColumnProps(71)=   "Column(11)._EditAlways=0"
            Splits(0)._ColumnProps(72)=   "Column(11)._ColStyle=516"
            Splits(0)._ColumnProps(73)=   "Column(11).Order=12"
            Splits(0)._ColumnProps(74)=   "Column(12).Width=2090"
            Splits(0)._ColumnProps(75)=   "Column(12).DividerColor=0"
            Splits(0)._ColumnProps(76)=   "Column(12)._WidthInPix=2011"
            Splits(0)._ColumnProps(77)=   "Column(12)._EditAlways=0"
            Splits(0)._ColumnProps(78)=   "Column(12)._ColStyle=516"
            Splits(0)._ColumnProps(79)=   "Column(12).Order=13"
            Splits(0)._ColumnProps(80)=   "Column(13).Width=4498"
            Splits(0)._ColumnProps(81)=   "Column(13).DividerColor=0"
            Splits(0)._ColumnProps(82)=   "Column(13)._WidthInPix=4419"
            Splits(0)._ColumnProps(83)=   "Column(13)._EditAlways=0"
            Splits(0)._ColumnProps(84)=   "Column(13)._ColStyle=516"
            Splits(0)._ColumnProps(85)=   "Column(13).Order=14"
            Splits(0)._ColumnProps(86)=   "Column(14).Width=2725"
            Splits(0)._ColumnProps(87)=   "Column(14).DividerColor=0"
            Splits(0)._ColumnProps(88)=   "Column(14)._WidthInPix=2646"
            Splits(0)._ColumnProps(89)=   "Column(14)._EditAlways=0"
            Splits(0)._ColumnProps(90)=   "Column(14)._ColStyle=516"
            Splits(0)._ColumnProps(91)=   "Column(14).Order=15"
            Splits.Count    =   1
            PrintInfos(0)._StateFlags=   3
            PrintInfos(0).Name=   "piInternal 0"
            PrintInfos(0).PageHeaderFont=   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Microsoft Sans Serif"
            PrintInfos(0).PageFooterFont=   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Microsoft Sans Serif"
            PrintInfos(0).PageHeaderHeight=   0
            PrintInfos(0).PageFooterHeight=   0
            PrintInfos.Count=   1
            AllowUpdate     =   0   'False
            ColumnFooters   =   -1  'True
            DataMode        =   4
            DefColWidth     =   0
            HeadLines       =   1,5
            FootLines       =   1
            RowDividerStyle =   6
            TabAction       =   1
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTipsWidth   =   0
            MultiSelect     =   0
            DeadAreaBackColor=   16777215
            RowDividerColor =   12632256
            RowSubDividerColor=   12632256
            DirectionAfterEnter=   1
            MaxRows         =   250000
            ViewColumnCaptionWidth=   0
            ViewColumnWidth =   0
            _PropDict       =   "_ExtentX,2003,3;_ExtentY,2004,3;_LayoutType,512,2;_RowHeight,16,3;_StyleDefs,513,0;_WasPersistedAsPixels,516,2"
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(5)   =   ":id=0,.fontname=MS Sans Serif"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HFFFFFF&,.bold=0,.fontsize=825"
            _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(8)   =   ":id=1,.fontname=Tahoma"
            _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37,.bgcolor=&HE7CFCF&"
            _StyleDefs(10)  =   ":id=4,.fgcolor=&H80000008&,.appearance=0,.ellipsis=0,.borderColor=&HFF8000&"
            _StyleDefs(11)  =   ":id=4,.bold=0,.fontsize=825,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(12)  =   ":id=4,.fontname=Microsoft Sans Serif"
            _StyleDefs(13)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34,.alignment=2,.bgcolor=&HCC9999&"
            _StyleDefs(14)  =   ":id=2,.fgcolor=&HFFFFFF&,.bold=0,.fontsize=975,.italic=0,.underline=0"
            _StyleDefs(15)  =   ":id=2,.strikethrough=0,.charset=0"
            _StyleDefs(16)  =   ":id=2,.fontname=Franklin Gothic Medium"
            _StyleDefs(17)  =   "FooterStyle:id=3,.parent=1,.namedParent=35,.wraptext=-1,.bold=0,.fontsize=825"
            _StyleDefs(18)  =   ":id=3,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(19)  =   ":id=3,.fontname=Microsoft Sans Serif"
            _StyleDefs(20)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(21)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(22)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(23)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38,.bgcolor=&HC0FFFF&"
            _StyleDefs(24)  =   ":id=8,.fgcolor=&H80000008&"
            _StyleDefs(25)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39,.bgcolor=&HF5EBEB&"
            _StyleDefs(26)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40,.bgcolor=&HFFFFFF&"
            _StyleDefs(27)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(28)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(29)  =   "Splits(0).Style:id=13,.parent=1,.bold=0,.fontsize=825,.italic=0,.underline=0"
            _StyleDefs(30)  =   ":id=13,.strikethrough=0,.charset=0"
            _StyleDefs(31)  =   ":id=13,.fontname=Tahoma"
            _StyleDefs(32)  =   "Splits(0).CaptionStyle:id=22,.parent=4,.bold=-1,.fontsize=1200,.italic=0"
            _StyleDefs(33)  =   ":id=22,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(34)  =   ":id=22,.fontname=Microsoft Sans Serif"
            _StyleDefs(35)  =   "Splits(0).HeadingStyle:id=14,.parent=2,.bgcolor=&HFFFFFF&,.fgcolor=&H0&"
            _StyleDefs(36)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(37)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(38)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(39)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(40)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(41)  =   "Splits(0).EvenRowStyle:id=20,.parent=9,.bgcolor=&HFFFFFF&"
            _StyleDefs(42)  =   "Splits(0).OddRowStyle:id=21,.parent=10,.bgcolor=&HFFFFFF&"
            _StyleDefs(43)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(44)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(45)  =   "Splits(0).Columns(0).Style:id=50,.parent=13"
            _StyleDefs(46)  =   "Splits(0).Columns(0).HeadingStyle:id=47,.parent=14"
            _StyleDefs(47)  =   "Splits(0).Columns(0).FooterStyle:id=48,.parent=15"
            _StyleDefs(48)  =   "Splits(0).Columns(0).EditorStyle:id=49,.parent=17"
            _StyleDefs(49)  =   "Splits(0).Columns(1).Style:id=28,.parent=13,.alignment=3"
            _StyleDefs(50)  =   "Splits(0).Columns(1).HeadingStyle:id=25,.parent=14"
            _StyleDefs(51)  =   "Splits(0).Columns(1).FooterStyle:id=26,.parent=15"
            _StyleDefs(52)  =   "Splits(0).Columns(1).EditorStyle:id=27,.parent=17"
            _StyleDefs(53)  =   "Splits(0).Columns(2).Style:id=86,.parent=13"
            _StyleDefs(54)  =   "Splits(0).Columns(2).HeadingStyle:id=83,.parent=14"
            _StyleDefs(55)  =   "Splits(0).Columns(2).FooterStyle:id=84,.parent=15"
            _StyleDefs(56)  =   "Splits(0).Columns(2).EditorStyle:id=85,.parent=17"
            _StyleDefs(57)  =   "Splits(0).Columns(3).Style:id=32,.parent=13,.alignment=3"
            _StyleDefs(58)  =   "Splits(0).Columns(3).HeadingStyle:id=29,.parent=14"
            _StyleDefs(59)  =   "Splits(0).Columns(3).FooterStyle:id=30,.parent=15"
            _StyleDefs(60)  =   "Splits(0).Columns(3).EditorStyle:id=31,.parent=17"
            _StyleDefs(61)  =   "Splits(0).Columns(4).Style:id=46,.parent=13,.alignment=3"
            _StyleDefs(62)  =   "Splits(0).Columns(4).HeadingStyle:id=43,.parent=14"
            _StyleDefs(63)  =   "Splits(0).Columns(4).FooterStyle:id=44,.parent=15"
            _StyleDefs(64)  =   "Splits(0).Columns(4).EditorStyle:id=45,.parent=17"
            _StyleDefs(65)  =   "Splits(0).Columns(5).Style:id=70,.parent=13"
            _StyleDefs(66)  =   "Splits(0).Columns(5).HeadingStyle:id=67,.parent=14"
            _StyleDefs(67)  =   "Splits(0).Columns(5).FooterStyle:id=68,.parent=15"
            _StyleDefs(68)  =   "Splits(0).Columns(5).EditorStyle:id=69,.parent=17"
            _StyleDefs(69)  =   "Splits(0).Columns(6).Style:id=54,.parent=13,.alignment=3"
            _StyleDefs(70)  =   "Splits(0).Columns(6).HeadingStyle:id=51,.parent=14"
            _StyleDefs(71)  =   "Splits(0).Columns(6).FooterStyle:id=52,.parent=15"
            _StyleDefs(72)  =   "Splits(0).Columns(6).EditorStyle:id=53,.parent=17"
            _StyleDefs(73)  =   "Splits(0).Columns(7).Style:id=90,.parent=13"
            _StyleDefs(74)  =   "Splits(0).Columns(7).HeadingStyle:id=87,.parent=14"
            _StyleDefs(75)  =   "Splits(0).Columns(7).FooterStyle:id=88,.parent=15"
            _StyleDefs(76)  =   "Splits(0).Columns(7).EditorStyle:id=89,.parent=17"
            _StyleDefs(77)  =   "Splits(0).Columns(8).Style:id=58,.parent=13"
            _StyleDefs(78)  =   "Splits(0).Columns(8).HeadingStyle:id=55,.parent=14"
            _StyleDefs(79)  =   "Splits(0).Columns(8).FooterStyle:id=56,.parent=15"
            _StyleDefs(80)  =   "Splits(0).Columns(8).EditorStyle:id=57,.parent=17"
            _StyleDefs(81)  =   "Splits(0).Columns(9).Style:id=62,.parent=13,.alignment=3"
            _StyleDefs(82)  =   "Splits(0).Columns(9).HeadingStyle:id=59,.parent=14"
            _StyleDefs(83)  =   "Splits(0).Columns(9).FooterStyle:id=60,.parent=15"
            _StyleDefs(84)  =   "Splits(0).Columns(9).EditorStyle:id=61,.parent=17"
            _StyleDefs(85)  =   "Splits(0).Columns(10).Style:id=66,.parent=13"
            _StyleDefs(86)  =   "Splits(0).Columns(10).HeadingStyle:id=63,.parent=14"
            _StyleDefs(87)  =   "Splits(0).Columns(10).FooterStyle:id=64,.parent=15"
            _StyleDefs(88)  =   "Splits(0).Columns(10).EditorStyle:id=65,.parent=17"
            _StyleDefs(89)  =   "Splits(0).Columns(11).Style:id=74,.parent=13"
            _StyleDefs(90)  =   "Splits(0).Columns(11).HeadingStyle:id=71,.parent=14"
            _StyleDefs(91)  =   "Splits(0).Columns(11).FooterStyle:id=72,.parent=15"
            _StyleDefs(92)  =   "Splits(0).Columns(11).EditorStyle:id=73,.parent=17"
            _StyleDefs(93)  =   "Splits(0).Columns(12).Style:id=94,.parent=13"
            _StyleDefs(94)  =   "Splits(0).Columns(12).HeadingStyle:id=91,.parent=14"
            _StyleDefs(95)  =   "Splits(0).Columns(12).FooterStyle:id=92,.parent=15"
            _StyleDefs(96)  =   "Splits(0).Columns(12).EditorStyle:id=93,.parent=17"
            _StyleDefs(97)  =   "Splits(0).Columns(13).Style:id=78,.parent=13"
            _StyleDefs(98)  =   "Splits(0).Columns(13).HeadingStyle:id=75,.parent=14"
            _StyleDefs(99)  =   "Splits(0).Columns(13).FooterStyle:id=76,.parent=15"
            _StyleDefs(100) =   "Splits(0).Columns(13).EditorStyle:id=77,.parent=17"
            _StyleDefs(101) =   "Splits(0).Columns(14).Style:id=82,.parent=13"
            _StyleDefs(102) =   "Splits(0).Columns(14).HeadingStyle:id=79,.parent=14"
            _StyleDefs(103) =   "Splits(0).Columns(14).FooterStyle:id=80,.parent=15"
            _StyleDefs(104) =   "Splits(0).Columns(14).EditorStyle:id=81,.parent=17"
            _StyleDefs(105) =   "Named:id=33:Normal"
            _StyleDefs(106) =   ":id=33,.parent=0,.bold=0,.fontsize=825,.italic=0,.underline=0,.strikethrough=0"
            _StyleDefs(107) =   ":id=33,.charset=0"
            _StyleDefs(108) =   ":id=33,.fontname=Tahoma"
            _StyleDefs(109) =   "Named:id=34:Heading"
            _StyleDefs(110) =   ":id=34,.parent=33,.valignment=2,.bgcolor=&HCC9999&,.fgcolor=&H80000012&"
            _StyleDefs(111) =   ":id=34,.wraptext=-1,.bold=-1,.fontsize=975,.italic=0,.underline=0"
            _StyleDefs(112) =   ":id=34,.strikethrough=0,.charset=0"
            _StyleDefs(113) =   ":id=34,.fontname=Times New Roman"
            _StyleDefs(114) =   "Named:id=35:Footing"
            _StyleDefs(115) =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(116) =   ":id=35,.bold=0,.fontsize=825,.italic=0,.underline=0,.strikethrough=0,.charset=0"
            _StyleDefs(117) =   ":id=35,.fontname=Tahoma"
            _StyleDefs(118) =   "Named:id=36:Selected"
            _StyleDefs(119) =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(120) =   "Named:id=37:Caption"
            _StyleDefs(121) =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(122) =   "Named:id=38:HighlightRow"
            _StyleDefs(123) =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(124) =   "Named:id=39:EvenRow"
            _StyleDefs(125) =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(126) =   "Named:id=40:OddRow"
            _StyleDefs(127) =   ":id=40,.parent=33"
            _StyleDefs(128) =   "Named:id=41:RecordSelector"
            _StyleDefs(129) =   ":id=41,.parent=34"
            _StyleDefs(130) =   "Named:id=42:FilterBar"
            _StyleDefs(131) =   ":id=42,.parent=33"
         End
      End
      Begin MSComCtl2.DTPicker dDate0 
         Height          =   300
         Left            =   1305
         TabIndex        =   3
         Top             =   390
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   529
         _Version        =   393216
         CalendarBackColor=   12648447
         Format          =   49020931
         CurrentDate     =   39208
      End
      Begin MSComCtl2.DTPicker dDate1 
         Height          =   300
         Left            =   3390
         TabIndex        =   4
         Top             =   390
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   529
         _Version        =   393216
         CalendarBackColor=   12648447
         Format          =   49020931
         CurrentDate     =   39208
      End
      Begin VB.Label Label1 
         Caption         =   "s/d"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2940
         TabIndex        =   11
         Top             =   420
         Width           =   465
      End
      Begin VB.Label Label14 
         Caption         =   "Tanggal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   405
         TabIndex        =   10
         Top             =   420
         Width           =   870
      End
   End
End
Attribute VB_Name = "frmHistoryProduksi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim xArray As New XArrayDB


Private Sub bExport_Click()
    Call ExportExcel(DataGrid1(0))
End Sub

Private Sub bExcell_Click()

End Sub

Private Sub bKeluar_Click()
    Unload Me
End Sub

Private Sub bPreview_Click()
    With DataGrid1(0).PrintInfo
        .SettingsMarginLeft = 500
        .SettingsMarginRight = 200
        .SettingsMarginBottom = 700
        .SettingsMarginTop = 700
        .SettingsOrientation = 2
        .PageHeader = "Laporan History Produksi"
        .PageFooter = "Tanggal Cetak : " & Date & " Jam : " & Time
        .PreviewMaximize = True
        .SettingsPaperSize = 5
        .PreviewInitZoom = 100
        .PrintPreview
    End With
End Sub

Private Sub bProses_Click()
    InputDataGrid
    bPreview.SetFocus
    bExport.Enabled = True
End Sub


Private Sub InputDataGrid()
Dim X As Integer
Dim conn As New ADODB.Connection
Dim RS As New ADODB.Recordset
Dim RS2 As New ADODB.Recordset
Dim RS3 As New ADODB.Recordset
Dim i As Integer, j As Integer, k As Integer

    xArray.ReDim 0, -1, 0, 16
    conn.ConnectionString = strcon
    conn.Open

    RS.Open "select t.tanggal, t.nomer_potong, " & _
            "t.kode_bahan, t.qty,b1.satuan,d.kode_bahan as kode_output, " & _
            "d.qty_manual,b2.satuan as satuan_output " & _
            "from t_potongh t " & _
            "inner join t_potongd d on d.nomer_potong=t.nomer_potong " & _
            "inner join ms_bahan b1 on b1.kode_bahan=t.kode_bahan " & _
            "inner join ms_bahan b2 on b2.kode_bahan=d.kode_bahan ", conn
    If Not RS.EOF Then
        RS.MoveFirst
        Do While Not RS.EOF

            xArray.InsertRows xArray.UpperBound(1) + 1
            i = xArray.UpperBound(1)
            j = i: k = i
            xArray(i, 1) = (RS!tanggal)
            xArray(i, 2) = (RS!nomer_potong)
            xArray(i, 3) = (RS!kode_output) & "  (D)"
            xArray(i, 4) = (RS!qty_manual) & " " & (RS!satuan_output)
            
            xArray.InsertRows xArray.UpperBound(1) + 1
            i = xArray.UpperBound(1)
            xArray(i, 3) = (RS!kode_bahan) & "  (K)"
            xArray(i, 4) = String(10, " ") & (RS!qty) & " " & (RS!satuan)
            
            
            RS2.Open "select t.tanggal, t.nomer_cetak, " & _
                    "t.kode_bahan, t.qty,b1.satuan,d.kode_bahan as kode_output, " & _
                    "d.qty_manual,b2.satuan as satuan_output " & _
                    "from t_cetakh t " & _
                    "inner join t_cetakd d on d.nomer_cetak=t.nomer_cetak " & _
                    "inner join ms_bahan b1 on b1.kode_bahan=t.kode_bahan " & _
                    "inner join ms_bahan b2 on b2.kode_bahan=d.kode_bahan " & _
                    "where t.no_history='" & (RS!nomer_potong) & "'", conn
            
            Do While Not RS2.EOF
                If j > xArray.UpperBound(1) Then
                    xArray.InsertRows xArray.UpperBound(1) + 1
                End If
                xArray(j, 6) = (RS2!tanggal)
                xArray(j, 7) = (RS2!nomer_cetak)
                xArray(j, 8) = (RS2!kode_output) & "  (D)"
                xArray(j, 9) = (RS2!qty_manual) & " " & (RS2!satuan_output)
                
                j = j + 1
                If j > xArray.UpperBound(1) Then
                    xArray.InsertRows xArray.UpperBound(1) + 1
                End If
                xArray(j, 8) = (RS2!kode_bahan) & "  (K)"
                xArray(j, 9) = String(10, " ") & (RS2!qty) & " " & (RS2!satuan)
                j = j + 1
                RS2.MoveNext
            Loop
            RS2.Close
            
            RS2.Open "select t.tanggal, t.nomer_plong, " & _
                    "t.kode_bahan, t.qty,b1.satuan,d.kode_bahan as kode_output, " & _
                    "d.qty_manual,b2.satuan as satuan_output " & _
                    "from t_plongh t " & _
                    "inner join t_plongd d on d.nomer_plong=t.nomer_plong " & _
                    "inner join ms_bahan b1 on b1.kode_bahan=t.kode_bahan " & _
                    "inner join ms_bahan b2 on b2.kode_bahan=d.kode_bahan " & _
                    "where t.no_history='" & (RS!nomer_potong) & "'", conn
            
            Do While Not RS2.EOF
                If k > xArray.UpperBound(1) Then
                    xArray.InsertRows xArray.UpperBound(1) + 1
                End If
                xArray(k, 11) = (RS2!tanggal)
                xArray(k, 12) = (RS2!nomer_plong)
                xArray(k, 13) = (RS2!kode_output) & "  (D)"
                xArray(k, 14) = (RS2!qty_manual) & " " & (RS2!satuan_output)
                
                k = k + 1
                If k > xArray.UpperBound(1) Then
                    xArray.InsertRows xArray.UpperBound(1) + 1
                End If
                xArray(k, 13) = (RS2!kode_bahan) & "  (K)"
                xArray(k, 14) = String(10, " ") & (RS2!qty) & " " & (RS2!satuan)
                k = k + 1
                RS2.MoveNext
            Loop
            RS2.Close
            

            RS.MoveNext
        Loop
    Else

    End If
    RS.Close
    conn.Close


    Set DataGrid1(0).Array = xArray
    DataGrid1(0).ReBind

End Sub


Sub ExportExcel(DG As Variant)
Dim nHeadBackColor As Long, nHeadForeColor As Long
Dim strLokasi As String
On Error GoTo salah
    CD.filter = "Excel files (*.xls)|*.xls"
    CD.CancelError = True
    CD.ShowSave
    If err Then
        MsgBox "Proses export gagal !", vbExclamation
        Exit Sub
    End If
    strLokasi = CD.filename
    If Right(strLokasi, 4) <> ".xls" Then strLokasi = strLokasi & ".xls"
    
    With DG
        nHeadForeColor = .HeadForeColor
        .HeadForeColor = RGB(0, 0, 0)
        nHeadBackColor = .HeadBackColor
        .HeadBackColor = RGB(255, 255, 255)
    End With
    
    DG.ExportToFile strLokasi, True
    
    With DG
        .HeadForeColor = nHeadForeColor
        .HeadBackColor = nHeadBackColor
    End With
    
    MsgBox "Data berhasil di-export !"
Exit Sub
salah:
If err.Number = 6277 Then
    MsgBox "Lokasi folder tidak ada !", vbCritical
Else
    MsgBox "Proses Export Gagal !", vbCritical
    MsgBox err.Description, vbExclamation
End If
End Sub

