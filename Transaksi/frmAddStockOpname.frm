VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmAddStockOpname 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stock Opname"
   ClientHeight    =   9855
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8940
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9855
   ScaleWidth      =   8940
   Begin VB.CheckBox chkPrintList 
      Caption         =   "Load From Print List"
      Height          =   330
      Left            =   5895
      TabIndex        =   39
      Top             =   990
      Width           =   1905
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      Height          =   645
      Left            =   3150
      Picture         =   "frmAddStockOpname.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   36
      Top             =   9045
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   27
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   26
      Top             =   180
      Value           =   1  'Checked
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1590
      Left            =   135
      TabIndex        =   17
      Top             =   1350
      Width           =   6990
      Begin VB.CommandButton cmdSearchBrg 
         Caption         =   "F3"
         Height          =   330
         Left            =   3150
         Picture         =   "frmAddStockOpname.frx":0102
         TabIndex        =   18
         Top             =   225
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2745
         TabIndex        =   6
         Top             =   1080
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1530
         TabIndex        =   5
         Top             =   1080
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   315
         TabIndex        =   4
         Top             =   1080
         Width           =   1050
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   3
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   2
         Top             =   225
         Width           =   1770
      End
      Begin VB.Label lblStock 
         BackStyle       =   0  'Transparent
         Height          =   330
         Left            =   2520
         TabIndex        =   35
         Top             =   675
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   270
         TabIndex        =   24
         Top             =   1260
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   23
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   22
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   21
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3240
         TabIndex        =   20
         Top             =   270
         Width           =   3525
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   330
         Left            =   1260
         TabIndex        =   19
         Top             =   720
         Width           =   600
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   645
      Left            =   180
      Picture         =   "frmAddStockOpname.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   9045
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   330
      Left            =   3780
      Picture         =   "frmAddStockOpname.frx":0306
      TabIndex        =   16
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   1
      Top             =   990
      Width           =   3885
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   645
      Left            =   4515
      Picture         =   "frmAddStockOpname.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   9045
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      Height          =   645
      Left            =   5985
      Picture         =   "frmAddStockOpname.frx":050A
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      Height          =   645
      Left            =   1755
      Picture         =   "frmAddStockOpname.frx":060C
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   9045
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   0
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   59703299
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   5010
      Left            =   135
      TabIndex        =   25
      Top             =   3060
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   8837
      _Version        =   393216
      Cols            =   5
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   6765
      Left            =   90
      TabIndex        =   37
      Top             =   1350
      Visible         =   0   'False
      Width           =   8700
      Begin SSDataWidgets_B.SSDBGrid DBGrid 
         Height          =   6405
         Left            =   45
         TabIndex        =   38
         Top             =   90
         Width           =   8340
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   "#"
         Col.Count       =   4
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         RowSelectionStyle=   2
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   450
         ExtraHeight     =   212
         Columns.Count   =   4
         Columns(0).Width=   2805
         Columns(0).Caption=   "Kd Barang"
         Columns(0).Name =   "Kode Barang"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   7276
         Columns(1).Caption=   "Nama Barang"
         Columns(1).Name =   "Nama Barang"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1561
         Columns(2).Caption=   "Qty"
         Columns(2).Name =   "Qty"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "#,##0"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1429
         Columns(3).Caption=   "Stock"
         Columns(3).Name =   "Stock"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         _ExtentX        =   14711
         _ExtentY        =   11298
         _StockProps     =   79
         BackColor       =   -2147483636
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5625
      TabIndex        =   34
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6975
      TabIndex        =   33
      Top             =   225
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7155
      TabIndex        =   32
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label16 
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3645
      TabIndex        =   31
      Top             =   8280
      Width           =   735
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4590
      TabIndex        =   30
      Top             =   8280
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7065
      TabIndex        =   29
      Top             =   8280
      Width           =   1140
   End
   Begin VB.Label Label17 
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   28
      Top             =   8280
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label11 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   14
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   13
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   1035
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   11
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddStockOpname"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select Lokasi,[Kode],Kategori,Merk,[nama barang], Satuan, PriceList,Harga1,Harga2,h_eceran from ms_barang where flag='1'"
Const queryTransfer As String = "select ID,Tanggal,Keterangan,Gudang from t_stockopnameh"
Dim total As Currency
Dim NoTrans2 As String
Dim foc As Byte
Dim mode As Byte
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Private Sub nomor_baru()
Dim No As String
    If db2 Then
    rs.Open "select top 1 ID from t_stockopnameh where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "SO" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "SO" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    End If
No = ""
    If db1 Then
    rs.Open "select top 1 ID from t_stockopnameh where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn_fake
    If Not rs.EOF Then
        No = "SO" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "SO" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    NoTrans2 = No
    If Not db2 Then lblNoTrans = NoTrans2
    End If
End Sub


Private Sub chkNumbering_Click()
If chkNumbering.Value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub


Private Sub chkPrintList_Click()
    If chkPrintList.Value Then
    Frame1.Visible = True
    Frame2.Visible = False
    flxGrid.Visible = False
    loaditemlist
    Else
    Frame1.Visible = False
    Frame2.Visible = True
    flxGrid.Visible = True
    End If
End Sub
Private Sub loaditemlist()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    conn.Open strcon
    rs.Open "select i.[kode],i.[nama barang],0,s.stock from itemlist i inner join stock s on i.[kode]=s.[kode] where s.gudang='" & gudang & "'", conn
    While Not rs.EOF
        DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(3) & "#" & rs(3)
    rs.MoveNext
    Wend
    rs.Close
End Sub
Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblStock = ""
    LblNamaBarang = ""
    txtQty.text = "1"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
    lblQty = totalQty
    For row = row To flxGrid.Rows - 2
        If row = flxGrid.Rows Then
            For col = 1 To 4
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To 4
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To 4
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = 4
    txtKdBrg.text = ""
    lblStock = ""
    txtQty.text = "1"
    LblNamaBarang = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKdBrg.text) Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 3))

            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        flxGrid.TextMatrix(row, 4) = lblStock
        totalQty = totalQty + flxGrid.TextMatrix(row, 3)
        lblQty = Format(totalQty, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 4
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""
        lblStock = ""
        LblNamaBarang = ""
        txtQty.text = "1"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.Rows - 1
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
Dim i As Byte
    i = 0
    conn.Open
    conn.BeginTrans
    i = 1
    For row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(row, 1) <> "" Then
            rs.Open "select stock from stock where [kode]='" & flxGrid.TextMatrix(row, 1) & "'", conn
            If Not rs.EOF Then
                flxGrid.TextMatrix(row, 4) = rs(0)
            Else
                flxGrid.TextMatrix(row, 4) = "0"
            End If
            rs.Close
            conn.Execute "update t_stockopnamed set stock=" & flxGrid.TextMatrix(row, 4) & ",hpp='" & getHpp(flxGrid.TextMatrix(row, 1), gudang, IIf(db2, strconasli, strcon)) & "' where [kode]='" & flxGrid.TextMatrix(row, 1) & "' and id='" & lblNoTrans & "'"
        End If
    Next
    conn.Execute "update t_stockopnamed set selisih=qty-stock where id='" & lblNoTrans & "'"
    conn.Execute "update t_stockopnameh set status='1' where id='" & lblNoTrans & "'"
    conn.CommitTrans
    i = 0
    conn.Close
    MsgBox "Posting berhasil"
    reset_form
    Exit Sub
err:
MsgBox err.Description
If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
DropConnection
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddstockopname"
    frmSearch.nmctrl = "txtKdBrg"
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.col = 1
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.cmbSort.ListIndex = 1
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub



Private Sub cmdSearchID_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.query = queryTransfer
    frmSearch.nmform = "frmAddstockopname"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer
On Error GoTo err
    i = 0
    id = lblNoTrans
    If flxGrid.Rows <= 1 Then
        MsgBox "Silahkan masukkan Barang yang akan disesuaikan terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Sub
    End If
    If db2 Then
    conn.ConnectionString = strconasli
    conn.Open
    conn.BeginTrans
    End If
    If db1 Then
    conn_fake.ConnectionString = strcon
    conn_fake.Open
    conn_fake.BeginTrans
    End If

    
    i = 1
    
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
    Else
        If db2 Then
        conn.Execute "delete from t_stockopnameh where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_stockopnamed where ID='" & lblNoTrans & "'"
        End If
        If db1 And NoTrans2 <> "" Then
        conn_fake.Execute "delete from t_stockopnameh where ID='" & NoTrans2 & "'"
        conn_fake.Execute "delete from t_stockopnamed where ID='" & NoTrans2 & "'"
        End If
    End If
    If chkNumbering = "0" Then lblNoTrans = txtID.text
    add_dataheader
    If chkPrintList.Value Then
    DBGrid.MoveFirst
    Dim counter As Integer
    counter = 0
    flxGrid.Rows = DBGrid.Rows + 2
    
    While counter < DBGrid.Rows
        flxGrid.TextMatrix(counter + 1, 1) = DBGrid.Columns(0).text
        flxGrid.TextMatrix(counter + 1, 2) = DBGrid.Columns(1).text
        flxGrid.TextMatrix(counter + 1, 3) = DBGrid.Columns(2).text
        flxGrid.TextMatrix(counter + 1, 4) = DBGrid.Columns(3).text
        counter = counter + 1
        DBGrid.MoveNext
    Wend
    End If
    For row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(row, 1) <> "" Then
            add_datadetail (row)
        End If
    Next
    Dim kodebarang As String
    kodebarang = ""
    For row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(row, 1) <> "" Then
            kodebarang = ""
            rs.Open "select [kode] from ms_barang where kode='" & flxGrid.TextMatrix(row, 1) & "'", conn_fake
            If Not rs.EOF Then
                kodebarang = rs(0)
            End If
            rs.Close
            flxGrid.TextMatrix(row, 4) = getStock2(flxGrid.TextMatrix(row, 1), gudang, strcon)
        
            If db2 Then conn.Execute "update t_stockopnamed set stock=" & flxGrid.TextMatrix(row, 4) & ",hpp='" & getHpp(flxGrid.TextMatrix(row, 1), gudang, IIf(db2, strconasli, strcon)) & "' where [kode]='" & flxGrid.TextMatrix(row, 1) & "' and id='" & lblNoTrans & "'"
            If db1 Then conn_fake.Execute "update t_stockopnamed set stock=" & flxGrid.TextMatrix(row, 4) & ",hpp='" & getHpp(flxGrid.TextMatrix(row, 1), gudang, IIf(db2, strconasli, strcon)) & "' where [kode]='" & flxGrid.TextMatrix(row, 1) & "' and id='" & lblNoTrans & "'"
            If db2 Then updatestock2 gudang, kodebarang, flxGrid.TextMatrix(row, 3), conn
            If db1 Then updatestock2 gudang, kodebarang, flxGrid.TextMatrix(row, 3), conn_fake
        End If
    Next
    If db2 Then
    conn.Execute "update t_stockopnamed set selisih=qty-stock where id='" & lblNoTrans & "'"
    conn.Execute "update t_stockopnameh set status='1' where id='" & lblNoTrans & "'"
    End If
    If db1 Then
    conn_fake.Execute "update t_stockopnamed set selisih=qty-stock where id='" & lblNoTrans & "'"
    conn_fake.Execute "update t_stockopnameh set status='1' where id='" & lblNoTrans & "'"
    End If
    If db2 Then conn.CommitTrans
    If db1 Then conn_fake.CommitTrans
    i = 0
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    
    Call MySendKeys("{tab}")
    Exit Sub
err:
    If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    txtKeterangan.text = ""
    loadcombo
    totalQty = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    chkPrintList.Value = False
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "t_stockopnameh"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "gudang"
    fields(3) = "keterangan"
    fields(4) = "pk"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = gudang
    nilai(3) = txtKeterangan.text
    nilai(4) = NoTrans2
    
    If db2 Then conn.Execute tambah_data2(table_name, fields, nilai)
    If db1 And NoTrans2 <> "" Then
        nilai(0) = NoTrans2
        conn_fake.Execute tambah_data2(table_name, fields, nilai)
    End If
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_stockopnamed"
    fields(0) = "ID"
    fields(1) = "[kode]"
    fields(2) = "qty"
    fields(3) = "URUT"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = row
        
    If db2 Then conn.Execute tambah_data2(table_name, fields, nilai)
    If db1 And NoTrans2 <> "" Then
    nilai(0) = NoTrans2
    conn_fake.Execute tambah_data2(table_name, fields, nilai)
    End If
End Sub

Public Sub cek_notrans()
    cmdPrint.Visible = False
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open

    rs.Open "select * from t_stockopnameh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
    
        lblGudang = rs("gudang")
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        totalQty = 0
        total = 0
        If rs("status") = 1 Then
            cmdPosting.Enabled = False
            
            cmdSimpan.Enabled = False
            mnuSave.Enabled = False
            cmdHapus.Enabled = False
            mnuDelete.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
            mnuSave.Enabled = True
            cmdHapus.Enabled = True
            mnuDelete.Enabled = True
        End If
'        cmdPrint.Visible = True
        
        If rs.State Then rs.Close
        
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.[kode],m.[nama barang], qty from t_stockopnamed d inner join ms_barang m on d.[kode]=m.[kode] where d.id='" & lblNoTrans & "' order by urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
                flxGrid.TextMatrix(row, 4) = getStock2(flxGrid.TextMatrix(row, 1), gudang, strcon)
                row = row + 1
                totalQty = totalQty + rs(2)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty

        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub



Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
        cek_kodebarang1
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
        txtQty.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
    Call MySendKeys("{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_Load()
    loadcombo
    reset_form
    total = 0
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    lblGudang = gudang
    'End If
    'rs.Close
    conn.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 900
    flxGrid.ColWidth(4) = 0
    If right_stock = True Then flxGrid.ColWidth(4) = 1000
    
    flxGrid.TextMatrix(0, 1) = "Kode"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 4) = "Stock"
End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select m.[nama barang],m.[Kode] from ms_barang m where m.[Kode]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        cek_kodebarang1 = True
        LblNamaBarang = rs(0)
        lblStock = getStock2(rs(1), gudang, strcon)
        lblKode = txtKdBrg
    Else
        cek_kodebarang1 = False
        LblNamaBarang = ""
        lblStock = "0"
        lblKode = ""
    End If
    rs.Close
    conn.Close
Exit Function
err:
    MsgBox err.Description
    If conn.State Then conn.Close
End Function

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If
        cek_kodebarang1
'        cmdOK_Click

'        cari_id
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If Not cek_kodebarang1 And txtKdBrg <> "" Then
        MsgBox "Kode yang anda masukkan salah"
        txtKdBrg.SetFocus
    End If
    foc = 0
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtQty.text = calc(txtQty.text)
    End If
End Sub

