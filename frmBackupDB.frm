VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmBackupDB 
   BorderStyle     =   0  'None
   Caption         =   "Backup Database"
   ClientHeight    =   6780
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7125
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   7125
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command2 
      Caption         =   "Create table terimabarang"
      Height          =   420
      Left            =   90
      TabIndex        =   16
      Top             =   3420
      Width           =   2220
   End
   Begin VB.Frame frRestore 
      Height          =   1680
      Left            =   180
      TabIndex        =   7
      Top             =   765
      Visible         =   0   'False
      Width           =   5370
      Begin VB.TextBox txtPassword 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   1125
         PasswordChar    =   "*"
         TabIndex        =   14
         Top             =   1125
         Width           =   1500
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Save as"
         Height          =   330
         Left            =   4455
         TabIndex        =   11
         Top             =   720
         Width           =   780
      End
      Begin VB.TextBox txtFileSave 
         Height          =   330
         Left            =   1125
         TabIndex        =   10
         Top             =   720
         Width           =   3120
      End
      Begin VB.CommandButton cmdOpen 
         Caption         =   "Browse"
         Height          =   330
         Left            =   4455
         TabIndex        =   9
         Top             =   225
         Width           =   780
      End
      Begin VB.TextBox txtFileOpen 
         Height          =   330
         Left            =   1125
         TabIndex        =   8
         Top             =   225
         Width           =   3120
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "sa Password"
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   45
         TabIndex        =   13
         Top             =   1170
         Width           =   960
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Backup File"
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   45
         TabIndex        =   12
         Top             =   270
         Width           =   960
      End
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "Process"
      Enabled         =   0   'False
      Height          =   420
      Left            =   1530
      TabIndex        =   6
      Top             =   2655
      Width           =   1545
   End
   Begin VB.Frame frBackup 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   825
      Left            =   225
      TabIndex        =   3
      Top             =   855
      Visible         =   0   'False
      Width           =   4695
      Begin VB.CommandButton cmdSave 
         Caption         =   "Browse"
         Height          =   330
         Left            =   3510
         TabIndex        =   5
         Top             =   225
         Width           =   1005
      End
      Begin VB.TextBox txtFileBackup 
         Height          =   330
         Left            =   135
         TabIndex        =   4
         Top             =   225
         Width           =   3300
      End
   End
   Begin VB.CommandButton cmdRestore 
      Caption         =   "&Restore"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1890
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   225
      Width           =   1065
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   45
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3015
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   225
      Width           =   1065
   End
   Begin VB.CommandButton cmdBackup 
      Caption         =   "&Backup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   795
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   225
      Width           =   1005
   End
   Begin VB.Label lbltgl 
      Height          =   600
      Left            =   5625
      TabIndex        =   15
      Top             =   990
      Visible         =   0   'False
      Width           =   1275
   End
End
Attribute VB_Name = "frmBackupDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public kode As String

Private Sub cmdBackup_Click()

    frBackup.Visible = True
    frRestore.Visible = False
    cmdProcess.Enabled = True
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOpen_Click()
    CommonDialog1.DialogTitle = "Open File as"
    
    CommonDialog1.ShowOpen
    txtFileOpen.text = CommonDialog1.filename
End Sub

Private Sub cmdProcess_Click()

On Error GoTo err
Dim filename As String

If frBackup.Visible = True Then
    conn.ConnectionString = strcon
    conn.Open
    filename = txtFileBackup ' & "\" & lbltgl & "pabrik_kertas.bak"
    

     conn.Execute " BACKUP DATABASE " & dbname1 & " TO  DISK = N'" & filename & "' WITH NOFORMAT, NOINIT,  NAME = N'" & dbname1 & "-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10"
    
    conn.Close
    MsgBox "Proses backup telah berhasil"
End If

If frRestore.Visible = True Then
    
    Dim filename1 As String
    filename = txtFileOpen.text

    If filename <> "" Then
    
    If conn.State Then conn.Close
    Set conn = Nothing
    Call SetConnection(CStr(servname), "sa", txtPassword.text, dbname1)
    conn.Close
    conn.ConnectionString = Replace(strcon, dbname1, "master")
    conn.Open
    conn.Execute "RESTORE DATABASE " & dbname1 & " FROM  DISK = N'" & filename & "' WITH  FILE = 1,  NOUNLOAD,  STATS = 10"

    conn.Close
    End If
    Set conn = Nothing
    Call SetConnection(CStr(servname), CStr(User), CStr(pwd), dbname1)
    
    conn.Close
    MsgBox "Proses RESTORE telah berhasil"

End If


Exit Sub
err:
    If conn.State Then conn.Close
    
    MsgBox err.Description
End Sub

Private Sub cmdRestore_Click()
    frRestore.Visible = True
    frBackup.Visible = False
    cmdProcess.Enabled = True
End Sub

Private Sub cmdSave_Click()
    CommonDialog1.DialogTitle = "Save File as"
    CommonDialog1.filter = "*.bak|*.bak"
    CommonDialog1.filename = lbltgl + "backup.bak"
    CommonDialog1.ShowSave
   
    txtFileBackup.text = CommonDialog1.filename ' & lbltgl & ".bak"
    
End Sub

Private Sub cmdSave2_Click()
    CommonDialog1.DialogTitle = "Save File as"
    CommonDialog1.ShowSave
    txtFileBackup2.text = CommonDialog1.filename
End Sub

Private Sub Command1_Click()
    CommonDialog1.DialogTitle = "Save File as"
    CommonDialog1.ShowSave
    txtFileSave.text = CommonDialog1.filename
End Sub

Private Sub Command2_Click()
    createterima
End Sub

Private Sub Form_Load()
 lbltgl = Format(Now, "ddMMyyyy")
End Sub
