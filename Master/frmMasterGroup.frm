VERSION 5.00
Begin VB.Form frmMasterGroup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Jenis Barang"
   ClientHeight    =   2415
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   5310
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2415
   ScaleWidth      =   5310
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   3600
      Picture         =   "frmMasterGroup.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1440
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      Height          =   645
      Left            =   1980
      Picture         =   "frmMasterGroup.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1440
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3195
      Picture         =   "frmMasterGroup.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   1
      Left            =   1710
      MaxLength       =   50
      TabIndex        =   0
      Top             =   585
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1710
      Locked          =   -1  'True
      MaxLength       =   2
      TabIndex        =   3
      Top             =   180
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   645
      Left            =   495
      Picture         =   "frmMasterGroup.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1440
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   990
      Width           =   2130
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1035
      TabIndex        =   11
      Top             =   630
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   990
      TabIndex        =   10
      Top             =   225
      Width           =   150
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   9
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   8
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Jenis"
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Jenis"
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   225
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_barang where kode_tipe='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
        
    conn.BeginTrans
    i = 1
    conn.Execute "delete from ms_tipe where kode_tipe='" & txtField(0).text & "'"
    conn.CommitTrans
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.query = "select kode_tipe as [Kode_tipe],nama_tipe as [Nama_tipe] from ms_tipe"
    frmSearch.nmform = "frmMasterGroup"
    frmSearch.nmctrl = "txtField"
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSimpan_Click()
Dim i, j As Byte
On Error GoTo err
    i = 0
    For j = 1 To 1
        If txtField(j).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(j).SetFocus
            Exit Sub
        End If
    Next
    conn.ConnectionString = strcon
    conn.Open
'    If txtField(0).text = "" Then
'        rs.Open "select top 1 kode_tipe from ms_tipe order by kode_tipe desc", conn
'        If Not rs.EOF Then
'            txtField(0).text = Format((CLng(rs(0)) + 1), "00")
'        Else
'            txtField(0).text = Format("1", "00")
'        End If
'        rs.Close
'    End If
    
    'rs.Open "select * from ms_tipe where kode_tipe='" & txtField(0).text & "'", conn
    
    'rs.Close
    conn.BeginTrans
    i = 1
    If txtField(0) <> "" Then
        conn.Execute "update ms_tipe set nama_tipe='" & txtField(1).text & "' where kode_tipe='" & txtField(0).text & "'"
    Else
        conn.Execute "insert into MS_tipe values ('" & txtField(1).text & "')"
    End If
    conn.CommitTrans
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Private Sub reset_form()
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(1).SetFocus
End Sub
Public Sub cari_data(ByVal key As String)
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_tipe where kode_tipe='" & key & "'", conn
    If Not rs.EOF Then
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        txtField(1).text = rs(1)
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        txtField(1).text = ""
    End If
    rs.Close
    conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then cmdSearch_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{Tab}"
    End If
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data (txtField(0).text)
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data (txtField(0).text)
End Sub
