VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPenjualanLain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Penjualan"
   ClientHeight    =   9555
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10515
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9555
   ScaleWidth      =   10515
   Begin VB.ComboBox cmbTipeTransaksi 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmAddPenjualanLain.frx":0000
      Left            =   1575
      List            =   "frmAddPenjualanLain.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   61
      Top             =   945
      Width           =   3030
   End
   Begin VB.TextBox txtNoFaktur 
      Height          =   360
      Left            =   1575
      TabIndex        =   59
      Top             =   1305
      Width           =   2220
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3060
      Picture         =   "frmAddPenjualanLain.frx":0023
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   7980
      Width           =   1230
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5760
      ScaleHeight     =   345
      ScaleWidth      =   3090
      TabIndex        =   51
      Top             =   900
      Width           =   3120
      Begin VB.OptionButton OptLunas 
         Caption         =   "Kredit"
         Height          =   285
         Index           =   1
         Left            =   1575
         TabIndex        =   3
         Top             =   45
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.OptionButton OptLunas 
         Caption         =   "Tunai"
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   2
         Top             =   45
         Width           =   915
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah bayar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   52
         Top             =   1035
         Visible         =   0   'False
         Width           =   1725
      End
   End
   Begin VB.Frame frJT 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5805
      TabIndex        =   49
      Top             =   1305
      Width           =   3480
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   1530
         TabIndex        =   4
         Top             =   45
         Width           =   1875
         _ExtentX        =   3307
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   215416835
         CurrentDate     =   38927
      End
      Begin VB.Label Label9 
         Caption         =   "Jatuh Tempo"
         Height          =   240
         Left            =   45
         TabIndex        =   50
         Top             =   75
         Width           =   1320
      End
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   285
      Picture         =   "frmAddPenjualanLain.frx":0125
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7980
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4455
      Picture         =   "frmAddPenjualanLain.frx":0227
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   7980
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   1695
      Width           =   3885
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   375
      Left            =   3690
      Picture         =   "frmAddPenjualanLain.frx":0329
      TabIndex        =   21
      Top             =   60
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      Height          =   285
      Left            =   8685
      TabIndex        =   20
      Top             =   165
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      Height          =   375
      Left            =   10170
      TabIndex        =   19
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.CommandButton cmdSearchCustomer 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      Height          =   375
      Left            =   3165
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   18
      Top             =   465
      Width           =   420
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8010
      TabIndex        =   17
      Top             =   1770
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdLoad2 
      Caption         =   "&Load Excel 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8730
      TabIndex        =   16
      Top             =   1770
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save Excel "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9225
      TabIndex        =   15
      Top             =   1770
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.TextBox txtPPN 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   9285
      TabIndex        =   14
      Text            =   "10"
      Top             =   8130
      Width           =   735
   End
   Begin VB.TextBox txtKodeCustomer 
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   510
      Width           =   1530
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1665
      Picture         =   "frmAddPenjualanLain.frx":042B
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   7980
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7275
      TabIndex        =   22
      Top             =   555
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   215416835
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5280
      Index           =   0
      Left            =   135
      TabIndex        =   33
      Top             =   2250
      Width           =   10185
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         Height          =   1995
         Left            =   90
         TabIndex        =   34
         Top             =   180
         Width           =   8385
         Begin VB.CommandButton cmdSearchBrg 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            Height          =   330
            Left            =   4815
            Picture         =   "frmAddPenjualanLain.frx":052D
            TabIndex        =   58
            Top             =   270
            Width           =   420
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   510
            Left            =   90
            ScaleHeight     =   510
            ScaleWidth      =   4200
            TabIndex        =   35
            Top             =   1440
            Width           =   4200
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               Height          =   375
               Left            =   2790
               TabIndex        =   10
               Top             =   45
               Width           =   1320
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               Height          =   375
               Left            =   1395
               TabIndex        =   9
               Top             =   45
               Width           =   1320
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               Height          =   375
               Left            =   0
               TabIndex        =   8
               Top             =   45
               Width           =   1305
            End
         End
         Begin VB.TextBox txtHarga 
            Height          =   330
            Left            =   1260
            TabIndex        =   7
            Top             =   1080
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.TextBox txtQty 
            Height          =   330
            Left            =   1260
            TabIndex        =   6
            Top             =   675
            Width           =   1140
         End
         Begin VB.TextBox txtKdBrg 
            Height          =   330
            Left            =   1260
            TabIndex        =   5
            Top             =   270
            Width           =   3480
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            Height          =   285
            Left            =   3285
            TabIndex        =   57
            Top             =   675
            Width           =   3480
         End
         Begin VB.Label lblNamaBarang2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            Height          =   285
            Left            =   3285
            TabIndex        =   48
            Top             =   1080
            Width           =   3480
         End
         Begin VB.Label Label8 
            BackColor       =   &H8000000C&
            Caption         =   "Harga"
            Height          =   240
            Left            =   180
            TabIndex        =   42
            Top             =   1125
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblDefQtyJual 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   270
            TabIndex        =   41
            Top             =   1395
            Width           =   870
         End
         Begin VB.Label lblID 
            Caption         =   "Label12"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000F&
            Height          =   240
            Left            =   270
            TabIndex        =   40
            Top             =   1395
            Visible         =   0   'False
            Width           =   870
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            Height          =   240
            Left            =   180
            TabIndex        =   39
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            Caption         =   "Qty"
            Height          =   240
            Left            =   180
            TabIndex        =   38
            Top             =   720
            Width           =   600
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   37
            Top             =   1305
            Width           =   600
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2520
            TabIndex        =   36
            Top             =   675
            Width           =   1050
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2565
         Left            =   90
         TabIndex        =   43
         Top             =   2265
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   4524
         _Version        =   393216
         Cols            =   7
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label17 
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   47
         Top             =   4860
         Width           =   735
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3465
         TabIndex        =   46
         Top             =   4860
         Width           =   1140
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   45
         Top             =   4860
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   44
         Top             =   4860
         Width           =   735
      End
   End
   Begin VB.Label Label13 
      Caption         =   "Tipe"
      Height          =   240
      Left            =   90
      TabIndex        =   62
      Top             =   945
      Width           =   1275
   End
   Begin VB.Label Label5 
      Caption         =   "No. Faktur"
      Height          =   240
      Left            =   90
      TabIndex        =   60
      Top             =   1305
      Width           =   1275
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      Height          =   330
      Left            =   4185
      TabIndex        =   56
      Top             =   135
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   10080
      TabIndex        =   55
      Top             =   8145
      Width           =   315
   End
   Begin VB.Label Label7 
      Caption         =   "Customer"
      Height          =   240
      Left            =   90
      TabIndex        =   53
      Top             =   555
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   32
      Top             =   60
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      Height          =   240
      Left            =   90
      TabIndex        =   31
      Top             =   1695
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      Height          =   240
      Left            =   5805
      TabIndex        =   30
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label19 
      Caption         =   "Customer"
      Height          =   240
      Left            =   90
      TabIndex        =   29
      Top             =   555
      Width           =   960
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   28
      Top             =   7665
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6975
      TabIndex        =   27
      Top             =   7665
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "PPN"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   6900
      TabIndex        =   26
      Top             =   8175
      Width           =   945
   End
   Begin VB.Label Label20 
      Alignment       =   1  'Right Justify
      Caption         =   "Grand Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6165
      TabIndex        =   25
      Top             =   8715
      Width           =   1680
   End
   Begin VB.Label lblGrandTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7980
      TabIndex        =   24
      Top             =   8700
      Width           =   2115
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      Height          =   240
      Left            =   90
      TabIndex        =   23
      Top             =   135
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPenjualanLain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Long
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim kategori As String

Public Function newfaktur(tanggal As Date) As String
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim query As String
Dim start As Byte
    conn.Open strcon
    
    query = "select top 1 nomer_faktur from t_jualkertash where substring(nomer_faktur,1,4)='" & Format(tanggal, "yyyy") & "' and tipe='" & cmbTipeTransaksi.text & "' order by nomer_faktur desc"
    
    rs.Open query, conn
    If Not rs.EOF Then
        newfaktur = Format(tanggal, "yyyy") & "/" & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        newfaktur = Format(tanggal, "yyyy") & "/" & Format("1", "00000")
    End If
    rs.Close
    conn.Close
End Function
Private Sub cmbTipeTransaksi_Click()
    If cmbTipeTransaksi.text = "DUS-BUNGKUS" Then
        kategori = "('BARANG JADI DUS','ROLL DI SARASWATI','ROLL DI KM','SHEET SUPARMA','SHEET PAPIRUS','SHEET IN ROLL BLABAK','SHEET NON COAT PURA','BARANG JADI SHEET BUNGKUS','BARANG JADI BUNGKUS','ROLL KERTAS BUNGKUS DI SARASWATI','ROLL KERTAS BUNGKUS')"
    ElseIf cmbTipeTransaksi.text = "CD-HVS" Then
        kategori = "('BARANG JADI SHEET CD','ROLL CD','BARANG JADI FOLIO HVS','BARANG JADI FOLIO CD','ROLL HVS','BARANG JADI SHEET HVS')"
    End If
    txtNoFaktur = newfaktur(DTPicker1)
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    lblNamaBarang2 = ""
    txtQty.text = "1"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub


Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""

    row = flxGrid.row
    totalQty = totalQty - (flxGrid.TextMatrix(row, 4))
    total = total - (flxGrid.TextMatrix(row, 6))
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")
        lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")

    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = 6
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False

End Sub


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang1
        flxGrid.TextMatrix(row, 3) = lblNamaBarang2
        If flxGrid.TextMatrix(row, 4) = "" Then
            flxGrid.TextMatrix(row, 4) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 4))
            total = total - (flxGrid.TextMatrix(row, 6))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 4) = CDbl(flxGrid.TextMatrix(row, 4)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 4) = CDbl(txtQty)
            End If
        End If
        
        flxGrid.TextMatrix(row, 5) = txtHarga.text
        flxGrid.TextMatrix(row, 6) = flxGrid.TextMatrix(row, 4) * flxGrid.TextMatrix(row, 5)
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 6
        totalQty = totalQty + flxGrid.TextMatrix(row, 4)
        total = total + (flxGrid.TextMatrix(row, 6))
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")
        lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        
        
        
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        txtQty.text = "1"
        txtHarga.text = "0"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    
    mode = 1
    lblItem = flxGrid.Rows - 2
End Sub


Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdPosting_Click()
On Error GoTo err
    
'    If Not Cek_serial Then
'        MsgBox "Ada Serial Number yang sudah terpakai"
'        Exit Sub
'    End If
    simpan
    If posting_jual(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = "select * from ms_bahan where  tipe='2'"
    frmSearch.nmform = "frmAddpenjualankertasKertas"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchJualKertas & " where left(nomer_jualkertas,3)='PJL' and status_posting='0'"
    frmSearch.nmform = "frmAddpenjualankertas"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "jualKertasH"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchCustomer_Click()
    frmSearch.connstr = strcon
    frmSearch.query = searchcustomer
    frmSearch.nmform = "frmAddpenjualankertas"
    frmSearch.nmctrl = "txtKodeCustomer"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_customer"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_customer"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If txtKodeCustomer.text = "" Then
        MsgBox "Silahkan masukkan customer terlebih dahulu"
        txtKodeCustomer.SetFocus
        Exit Function
    End If
    
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_jualkertash", "nomer_jualkertas", DTPicker1, "PJL")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_jualkertash where nomer_jualkertas='" & id & "'"
    conn.Execute "delete from t_jualkertasd where nomer_jualkertas='" & id & "'"
    conn.Execute "delete from t_jualkertas_serial where nomer_jualkertas='" & id & "'"
    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    


    conn.CommitTrans
    simpan = True
    i = 0
    DropConnection

    
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    cmdClear_Click
    
    txtKeterangan.text = ""
    txtKodeCustomer.text = ""
    
    totalQty = 0
    total = 0
    lblTotal = 0
    lblGrandTotal = 0
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = DTPicker1 + 30
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    
    
    cmbTipeTransaksi_Click
    
    txtKodeCustomer.SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(12)
    ReDim nilai(12)
    table_name = "t_jualkertash"
    fields(0) = "nomer_jualkertas"
    fields(1) = "tanggal_jualkertas"
    fields(2) = "tanggal_jatuhtempo"
    fields(3) = "kode_customer"
    fields(4) = "total"
    fields(5) = "keterangan"
    fields(6) = "userid"
    fields(7) = "cara_bayar"
    fields(8) = "kode_gudang"
    fields(9) = "tax"
    fields(10) = "tipe"
    fields(11) = "nomer_faktur"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = Format(DTPicker2, "yyyy/MM/dd")
    nilai(3) = txtKodeCustomer.text
    
    nilai(4) = Replace(Format(lblTotal, "###0.##"), ",", ".")
    nilai(5) = txtKeterangan.text
    nilai(6) = User
    nilai(8) = ""
    nilai(9) = Replace(Format(txtPPN.text, "###0.##"), ",", ".")
    nilai(10) = cmbTipeTransaksi.text
    nilai(11) = txtNoFaktur.text
    If OptLunas(0).value = True Then
        nilai(7) = "T"
    Else
        nilai(7) = "K"
    End If
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_jualkertasd"
    fields(0) = "nomer_jualkertas"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "NO_URUT"
    fields(4) = "HARGA"
    

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = Replace(Format(flxGrid.TextMatrix(row, 4), "###0.##"), ",", ".")
    nilai(3) = row
    nilai(4) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datadetail2(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_jualkertas_serial"
    fields(0) = "nomer_jualkertas"
    fields(1) = "kode_bahan"
    fields(2) = "nomer_serial"
    fields(3) = "NO_URUT"
    fields(4) = "qty"
    fields(5) = "hpp"
    

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 1)
    nilai(3) = row
    nilai(4) = Replace(Format(flxGrid.TextMatrix(row, 4), "###0.##"), ",", ".")
    nilai(5) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub


'Private Sub add_datadetail2(row As Integer)
'    Dim fields() As String
'    Dim nilai() As String
'    Dim table_name As String
'
'    ReDim fields(6)
'    ReDim nilai(6)
'
'    table_name = "t_jualkertas_serial"
'    fields(0) = "nomer_jualkertas"
'    fields(1) = "kode_bahan"
'    fields(2) = "nomer_serial"
'    fields(3) = "NO_URUT"
'    fields(4) = "qty"
'    fields(5) = "hpp"
'
'
'    nilai(0) = lblNoTrans
'    nilai(1) = flxGrid2.TextMatrix(row, 1)
'    nilai(2) = flxGrid2.TextMatrix(row, 4)
'    nilai(3) = row
'    nilai(4) = Replace(Format(flxGrid2.TextMatrix(row, 5), "###0.##"), ",", ".")
'    nilai(5) = Replace(Format(flxGrid2.TextMatrix(row, 6), "###0.##"), ",", ".")
'
'
'    conn.Execute tambah_data2(table_name, fields, nilai)
'
'End Sub


Public Sub cek_notrans()
Dim rs As New ADODB.Recordset, gudang As String

cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_jualkertash where nomer_jualkertas='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        SetComboText rs!kode_gudang, cmbGudang
        gudang = rs("kode_gudang")
        DTPicker1 = rs("tanggal_jualkertas")
        DTPicker2 = rs("tanggal_jatuhtempo")
        txtKeterangan.text = rs("keterangan")
        txtKodeCustomer.text = rs("kode_customer")
        txtPPN.text = rs("tax")
        SetComboText rs!Tipe, cmbTipeTransaksi
        txtNoFaktur = rs!nomer_faktur
        totalQty = 0
        total = 0
'        cmdPrint.Visible = True
        If rs("status_posting") = 1 Then
'            cmdPrint.Enabled = False
            cmdSimpan.Enabled = False
            mnuSave.Enabled = False
        Else
            cmdSimpan.Enabled = True
            cmdPosting.Visible = True
            mnuSave.Enabled = True
        End If
        
        If rs("cara_bayar") = "T" Then
            OptLunas(0).value = True
        Else
            OptLunas(1).value = True
        End If
        
        
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.harga,panjang,lebar,berat from t_jualkertasd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_jualkertas='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(4) & "x" & rs(5) & "x" & rs(6)
                flxGrid.TextMatrix(row, 4) = rs(2)
                flxGrid.TextMatrix(row, 5) = Format(rs(3), "#,##0")
                flxGrid.TextMatrix(row, 6) = Format(rs(2) * rs(3), "#,##0")
                row = row + 1
                totalQty = totalQty + rs(2)
                total = total + (rs(2) * rs(3))
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = totalQty
        lblTotal = Format(total, "#,##0.00")
        lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
    
End Sub

Private Sub Command1_Click()

End Sub


Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKdBrg.SetFocus
End Sub

Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
                lblNamaBarang2 = flxGrid.TextMatrix(flxGrid.row, 3)
                End If
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 4)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtQty.SetFocus
        End If

End Sub
Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
         mode2 = 2
        txtQty.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        loaddetil
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub




Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    
    
    reset_form
    total = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 1200
    flxGrid.ColWidth(4) = 900
    flxGrid.ColWidth(5) = 0
    flxGrid.ColWidth(6) = 0
    
    
'    If right_hpp = True Then
        flxGrid.ColWidth(5) = 1500
        flxGrid.ColWidth(6) = 1500
        txtHarga.Visible = True
        Label8.Visible = True
        lblTotal.Visible = True
        Label10.Visible = True
'    End If

    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Dimensi"
    flxGrid.TextMatrix(0, 4) = "Qty"
    flxGrid.TextMatrix(0, 5) = "Harga"
    flxGrid.TextMatrix(0, 6) = "Total"
    
    
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "' and tipe='2'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        
        
        lblSatuan = ""
        cek_kodebarang1 = True
        If lblKategori <> "" Then
        txtHarga.text = IIf(IsNull(rs("harga_jual" & lblKategori)), 0, rs("harga_jual" & lblKategori))
        Else
        txtHarga.text = rs!harga_juala
        End If
    Else
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close

ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_customer()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_customer],kategori_harga from ms_customer where kode_customer='" & txtKodeCustomer & "'", conn
    If Not rs.EOF Then
        
        lblKategori = rs(1)
    Else
        
        lblKategori = ""
    End If
    rs.Close
    conn.Close
    If lblKategori <> "" Then
        If flxGrid.Rows > 2 Then hitung_ulang
    End If
err:
End Sub
Private Sub hitung_ulang()
On Error GoTo err
    conn.Open strcon
    With flxGrid
    For i = 1 To .Rows - 2
        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
        If Not rs.EOF Then
            total = total - .TextMatrix(i, 6)
            .TextMatrix(i, 5) = rs("harga_jual" & lblKategori)
            .TextMatrix(i, 6) = .TextMatrix(i, 4) * .TextMatrix(i, 5)
            total = total + .TextMatrix(i, 6)
        End If
        rs.Close
    Next
    End With
    conn.Close
    lblTotal = Format(total, "#,##0")
    lblGrandTotal = Format((total * (100 + txtPPN.text) / 100), "#,##0.00")
    Exit Sub
err:
    MsgBox err.Description
    If conn.State Then conn.Close
End Sub


Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub OptLunas_Click(Index As Integer)
    If Index = 1 Then frJT.Visible = True Else frJT.Visible = False
End Sub


Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodecustomer_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchCustomer_Click
End Sub

Private Sub txtKodecustomer_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_customer
End Sub

Private Sub txtKodecustomer_LostFocus()
    cek_customer
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub



