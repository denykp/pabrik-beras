Attribute VB_Name = "KeyCode"

Public Function GenKeyString(ByVal UserName, ProdName As String, F_Code As Long) As String

  Dim TempStr As String
  Dim KeyStr As String
  Dim KeyVal As String
  Dim CodeVal As Long
  Dim CodeLow As Byte
  Dim CodeHigh As Byte
  Dim KeyLowV1 As Byte
  Dim KeyLowV2 As Byte
  
   
  TempStr = LCase(UserName) & LCase(ProdName)
  KeyStr = DigestStrToHexStr(TempStr)
  KeyVal = HexStrToBinStr(KeyStr)
   
  CodeVal = F_Code And &HFFFF
  CodeLow = CodeVal And &HFF
  CodeHigh = (((CodeVal And &HFF00) / 256) And &HFF)
  
  KeyLow1 = Mid(KeyVal, Len(KeyVal), 1)
  KeyLow2 = Mid(KeyVal, Len(KeyVal) - 1, 1)
  
  KeyLowV1 = Asc(KeyLow1)
  KeyLowV2 = Asc(KeyLow2)
  
  KeyLowV1 = (KeyLowV1 Xor CodeLow)
  KeyLowV2 = (KeyLowV2 Xor CodeHigh)
  
    ChrV1 = Chr(KeyLowV1)
  ChrV2 = Chr(KeyLowV2)
  

  KeyVal = Mid(KeyVal, 1, Len(KeyVal) - 2)
  
  
  KeyVal = KeyVal & ChrV2 & ChrV1
  
  KeyVal = Mid(KeyVal, 3, Len(KeyVal) - 2)
  
  RawChk = DigestStrToHexStr(KeyVal)
  
  RC1 = Mid(RawChk, 1, 2)
  RC2 = Mid(RawChk, Len(RawChk) - 1, 2)
  
  StubStr = BinStrToHexStr(KeyVal)
  
  GenKeyString = RC1 & RC2 & StubStr
  
End Function


Public Function ValidateKeyCode(ByVal KeyCode, UserName, ProjName As String) As Boolean
  Dim ActiveBytes As String
  Dim LUNameHash As String
  Dim LUName As String
  Dim ValidKey As Boolean
  Dim KeyMD5 As String
  Dim KeySig As String
  
  ValidKey = False
  
  
  If Len(KeyCode) = 32 Then
    BinKeyCode = HexStrToBinStr(KeyCode)
    ActiveBytes = Right(BinKeyCode, 14)
    KeyMD5 = DigestStrToHexStr(ActiveBytes)
    ValidSig = Left(KeyMD5, 2) & Right(KeyMD5, 2)
    KeySig = Left(KeyCode, 4)
    
    If KeySig = ValidSig Then
      ValidKey = True
    Else
      ValidKey = False
    End If
    
    If ValidKey Then
      LUName = LCase(UserName) & LCase(ProjName)
      LUNameHash = DigestStrToHexStr(LUName)
      
      ActiveBytes = Mid(KeyCode, 5, 24)
      LUNameHash = Mid(LUNameHash, 5, 24)
      
      If ActiveBytes = LUNameHash Then
        ValidKey = True
      Else
        ValidKey = False
      End If
    End If
    
  Else
    ValidKey = False
  End If
  
  ValidateKeyCode = ValidKey
  
End Function



Public Function ExtractKeyFBits(ByVal KeyCode, UserName, ProjName As String)
  Dim PermVal As Long
  Dim RealHash As String
  Dim LUser As String
  Dim Perms As Long
  Dim BinCodePerm As String
  Dim BinUHashPerm As String
  Dim HiCodePerm As Byte
  Dim HIUMask As Byte
  Dim LoUMask As Byte
  Dim HiPerm As Long
  Dim LoPerm As Long
  
  PermVal = 0

  If ValidateKeyCode(KeyCode, UserName, ProjName) Then
  
    LUser = LCase(UserName) & LCase(ProjName)
    UserHash = DigestStrToHexStr(LUser)
    KCodedPerm = Right(KeyCode, 4)
    UHashPerm = Right(UserHash, 4)
    
    BinCodePerm = HexStrToBinStr(KCodedPerm)
    BinUHashPerm = HexStrToBinStr(UHashPerm)
    
    HiCodePerm = Asc(Mid(BinCodePerm, 1, 1))
    LoCodePerm = Asc(Mid(BinCodePerm, 2, 1))
    
    HIUMask = Asc(Mid(BinUHashPerm, 1, 1))
    LoUMask = Asc(Mid(BinUHashPerm, 2, 1))
    
    HiPerm = HiCodePerm Xor HIUMask
    LoPerm = LoCodePerm Xor LoUMask
    PermVal = (HiPerm * 256) Or LoPerm
     
  Else
    PermVal = 0
  End If
  
  ExtractKeyFBits = PermVal

End Function

Public Function FormatKeyCode(ByVal StrIn As String, ByVal GrpLen As Long) As String
  Dim StrLen As Long
  Dim CurGrp As Long
  Dim OutStr As String
  Dim GrpStr As String
  Dim GrpStart As Long
  
  StrLen = Len(StrIn)
   
  StrGroups = Int(StrLen / GrpLen)
  StrLeftOver = StrLen Mod GrpLen
  
  
  For CurGrp = 0 To (StrGroups - 1)
    GrpStart = (CurGrp * GrpLen) + 1
    GrpStr = Mid(StrIn, GrpStart, GrpLen)
    
    If CurGrp > 0 Then
      OutStr = OutStr & "-" & GrpStr
    Else
      OutStr = OutStr & GrpStr
    End If
    
  Next CurGrp
  
  
  If StrLeftOver > 0 Then
    OutStr = OutStr & "-" & Right(StrIn, StrLeftOver)
  End If
  
  FormatKeyCode = OutStr
End Function


