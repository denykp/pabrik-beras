VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmGroupUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New User"
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   7095
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   7095
   Begin VB.CommandButton cmdDel 
      BackColor       =   &H80000003&
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2970
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   4455
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   4185
      Picture         =   "frmGroupUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H80000003&
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4545
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   4455
      Width           =   1125
   End
   Begin VB.CommandButton cmdOK 
      BackColor       =   &H80000003&
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1350
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4455
      Width           =   1125
   End
   Begin VB.TextBox txtNamaGroup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1200
      TabIndex        =   1
      Top             =   180
      Width           =   2895
   End
   Begin SSDataWidgets_B.SSDBGrid DBMenu 
      Height          =   3345
      Left            =   180
      TabIndex        =   6
      Top             =   810
      Width           =   6225
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   3
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1535
      Columns(0).Caption=   "Access"
      Columns(0).Name =   "Access"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   5212
      Columns(1).Caption=   "Menu"
      Columns(1).Name =   "Menu"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   10980
      _ExtentY        =   5900
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Group :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   210
      Width           =   1200
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmGroupUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim seltab As Byte
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
On Error GoTo err
Dim result, result1, result2 As String
Dim counter As Byte
    If txtNamaGroup.text <> "" Then
        conn.ConnectionString = strcon
        conn.Open
        conn.Execute "delete from group_menu where nama_group='" & txtNamaGroup.text & "'"
        counter = 0
        DBMenu.MoveFirst
        While counter < DBMenu.Rows
            If DBMenu.Columns(0).Value = True Then
                conn.Execute "insert into group_menu values('" & DBMenu.Columns(2).text & "','" & txtNamaGroup.text & "','1')"
            Else
                conn.Execute "insert into group_menu values('" & DBMenu.Columns(2).text & "','" & txtNamaGroup.text & "','0')"
            End If
            DBMenu.MoveNext
            counter = counter + 1
        Wend
        conn.Close
        MsgBox "Data sudah tersimpan"
    Else
        MsgBox "semua field harus diisi"
    End If
    Exit Sub
err:
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub reset()
    txtNamaGroup.text = ""
    
End Sub


Private Sub cmdSearchID_Click()
    frmSearch.query = "Select distinct nama_group from group_menu"
    frmSearch.nmform = "frmGroupUser"
    frmSearch.nmctrl = "txtNamaGroup"
    frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub Form_Load()
    
    DBMenu.RemoveAll
    DBMenu.FieldSeparator = "#"
    conn.ConnectionString = strcon
    conn.Open
    
'    rs.Open "select * from ms_menu order by urut", conn
'    While Not rs.EOF
'        DBMenu.AddItem "#" & rs(1) & "#" & rs(0)
'        rs.MoveNext
'    Wend
'    rs.Close
    conn.Close
    seltab = 0
End Sub

Private Sub mnuCancel_Click()
Unload Me
End Sub

Private Sub mnuok_Click()
cmdOK_Click
End Sub

Public Sub cari_data(ByVal key As String)
    conn.ConnectionString = strcon
    conn.Open
    If key <> "" Then
    rs.Open "select * from group_menu where nama_group='" & key & "'", conn
    If Not rs.EOF Then
        rs.Close
        
        counter = 0
        DBMenu.MoveFirst
        While counter < DBMenu.Rows
            rs.Open "select * from group_menu where nama_group='" & key & "' and menu_id='" & DBMenu.Columns(2).text & "'", conn
            If Not rs.EOF Then
                DBMenu.Columns(0).Value = rs(2)
            Else
                DBMenu.Columns(0).Value = True
            End If
            
            rs.Close
            DBMenu.MoveNext
            counter = counter + 1
        Wend
        DBMenu.MoveFirst
    Else
        
        DBMenu.MoveFirst
        While counter < DBMenu.Rows
            DBMenu.Columns(0).Value = 1
            DBMenu.MoveNext
            counter = counter + 1
        Wend
        DBMenu.MoveFirst
    End If
    If rs.State Then rs.Close
    End If
    conn.Close
End Sub

Private Sub txtNamaGroup_KeyUp(KeyCode As Integer, Shift As Integer)
     If KeyCode = 13 Then cari_data txtNamaGroup.text
End Sub

Private Sub txtNamaGroup_LostFocus()
    cari_data txtNamaGroup.text
End Sub

