VERSION 5.00
Begin VB.Form frmMasterCustomer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Customer"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7110
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   7110
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0000
      Left            =   1890
      List            =   "frmMasterCustomer.frx":0013
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   2250
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterCustomer.frx":0026
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   3
      Left            =   1890
      TabIndex        =   3
      Top             =   1845
      Width           =   2040
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   2
      Left            =   1890
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1035
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      TabIndex        =   1
      Top             =   630
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      TabIndex        =   0
      Top             =   225
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4538
      Picture         =   "frmMasterCustomer.frx":0128
      TabIndex        =   6
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2918
      Picture         =   "frmMasterCustomer.frx":022A
      TabIndex        =   5
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1343
      Picture         =   "frmMasterCustomer.frx":032C
      TabIndex        =   4
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1350
      TabIndex        =   22
      Top             =   2295
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   21
      Top             =   2295
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Discount"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   20
      Top             =   2295
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   17
      Top             =   675
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   16
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   2880
      Width           =   2130
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1890
      Width           =   1320
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   13
      Top             =   1890
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   11
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   10
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   9
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Member"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Customer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterCustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If db2 Then
    conn.ConnectionString = strconasli
    conn.Open
    End If
    If db1 Then
    conn_fake.ConnectionString = strcon
    conn_fake.Open
    End If
    rs.Open "select * from t_penjualanh where [kode customer]='" & txtField(0).text & "'", IIf(db2, conn, conn_fake)
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        DropConnection
        Exit Sub
    End If
    rs.Close
   
    If db2 Then
    conn.BeginTrans
    conn.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
    conn.CommitTrans
    End If
    If db1 Then
    conn_fake.BeginTrans
    conn_fake.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
    conn_fake.CommitTrans
    End If
    i = 1
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [kode customer], [Nama Customer] , [Alamat], [Telp],Harga from ms_customer " ' order by kode_Member"
    frmSearch.nmform = "frmMasterCustomer"
    frmSearch.nmctrl = "txtField"
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, j As Byte
On Error GoTo err
    i = 0
    For j = 0 To 1
        If txtField(j).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(j).SetFocus
            Exit Sub
        End If
    Next
    If Combo1.text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        Combo1.SetFocus
        Exit Sub
    End If
    If db2 Then
    conn.ConnectionString = strconasli
    conn.Open
    conn.BeginTrans
    End If
    If db1 Then
    conn_fake.ConnectionString = strcon
    conn_fake.Open
    conn_fake.BeginTrans
    End If
    i = 1
    If db2 Then
    Set rs = conn.Execute("select * from ms_customer where [kode customer]='" & txtField(0).text & "'")
    Else
    Set rs = conn_fake.Execute("select * from ms_customer where [kode customer]='" & txtField(0).text & "'")
    End If
    If Not rs.EOF Then
        If db2 Then conn.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
        If db1 Then conn_fake.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
    End If
    rs.Close
    If db2 Then
        conn.Execute "insert into MS_customer values ('" & txtField(0).text & "','" & txtField(1).text & "','" & txtField(2).text & "','" & txtField(3).text & "','" & Combo1.text & "')"
        conn.CommitTrans
    End If
    If db1 Then
        conn_fake.Execute "insert into MS_customer values ('" & txtField(0).text & "','" & txtField(1).text & "','" & txtField(2).text & "','" & txtField(3).text & "','" & Combo1.text & "')"
        conn_fake.CommitTrans
    End If
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(0).SetFocus
    
End Sub
Public Sub cari_data()
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    rs.Open "select * from ms_customer where [kode customer]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        SetComboText rs(4), Combo1
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        
        txtField(1).text = ""
        txtField(2).text = ""
        txtField(3).text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub
Private Sub load_combo()
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    Combo1.ListIndex = 0
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
