VERSION 5.00
Begin VB.Form frmBatalTutupBukuBulanan 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Batal Tutup Buku Bulanan"
   ClientHeight    =   2715
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtTahun 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2145
      TabIndex        =   4
      Top             =   945
      Width           =   1035
   End
   Begin VB.TextBox txtBulan 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2145
      TabIndex        =   3
      Top             =   480
      Width           =   1035
   End
   Begin VB.CommandButton cmdProses 
      Caption         =   "Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1425
      TabIndex        =   2
      Top             =   1815
      Width           =   1770
   End
   Begin VB.Label Label2 
      Caption         =   "Tahun"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1395
      TabIndex        =   1
      Top             =   1005
      Width           =   675
   End
   Begin VB.Label Label1 
      Caption         =   "Bulan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1395
      TabIndex        =   0
      Top             =   555
      Width           =   615
   End
End
Attribute VB_Name = "frmBatalTutupBukuBulanan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdProses_Click()
Dim sBulanPrev As String, sTahunPrev As String
Dim sBulanNext As String, sTahunNext As String
Dim SaldoAwal As Long, BulanAwal As String, TahunAwal As String

    If MsgBox("Setelah dilakukan proses batal tutup buku, maka periode aktif akan dikembalikan ke periode sebelumnya. " & vbCrLf & _
              "Apakah Anda yakin akan melakukan proses batal tutup buku ?", vbYesNo + vbQuestion, "Konfirmasi Proses Tutup Buku") = vbNo Then
        Exit Sub
    End If
    
    txtBulan.text = Val(txtBulan.text)
    txtTahun.text = Val(txtTahun.text)
    
    conn.ConnectionString = strcon
    
    conn.Open
    
    conn.BeginTrans
    conn.Execute "Delete from mutasi_coa where bulan='" & txtBulan.text & "' and tahun='" & txtTahun.text & "' "
    conn.Execute "Update periode_aktif set bulan='" & txtBulan.text & "',tahun='" & txtTahun.text & "'"
    conn.CommitTrans
    conn.Close
    
    MsgBox "Proses Selesai !", vbInformation
    Unload Me
End Sub

Private Sub Form_Load()
Dim i As Integer

    conn.ConnectionString = strcon
    
    conn.Open

    rs.Open "Select * from periode_aktif", conn
    txtBulan.text = BulanPrev(rs("bulan"), rs("tahun"))
    txtTahun.text = TahunPrev(rs("bulan"), rs("tahun"))
    rs.Close
    
    conn.Close

End Sub
