VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmTransDeposit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Transaksi Deposit"
   ClientHeight    =   2790
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6690
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2790
   ScaleWidth      =   6690
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   1890
      TabIndex        =   1
      Top             =   570
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   63897601
      CurrentDate     =   40492
   End
   Begin VB.TextBox txtTotal 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1905
      MaxLength       =   50
      TabIndex        =   3
      Top             =   1380
      Width           =   1185
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3420
      Picture         =   "frmMasterSA.frx":0000
      TabIndex        =   6
      Top             =   2130
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2190
      Picture         =   "frmMasterSA.frx":0102
      TabIndex        =   5
      Top             =   2130
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3810
      TabIndex        =   15
      Top             =   150
      Width           =   375
   End
   Begin VB.TextBox txtNoTrans 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1890
      Locked          =   -1  'True
      MaxLength       =   12
      TabIndex        =   0
      Top             =   180
      Width           =   1575
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3840
      Picture         =   "frmMasterSA.frx":0204
      TabIndex        =   11
      Top             =   960
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtKode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1890
      MaxLength       =   25
      TabIndex        =   2
      Top             =   990
      Width           =   1875
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   960
      Picture         =   "frmTransDeposit.frx":0306
      TabIndex        =   4
      Top             =   2130
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label lblNama 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4290
      TabIndex        =   18
      Top             =   1020
      Width           =   495
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1650
      TabIndex        =   17
      Top             =   1440
      Width           =   105
   End
   Begin VB.Label lblTotal 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   210
      TabIndex        =   16
      Top             =   1410
      Width           =   1320
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   14
      Top             =   195
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "No.Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   13
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label lblParent 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3405
      TabIndex        =   12
      Top             =   270
      Width           =   60
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   10
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   9
      Top             =   630
      Width           =   105
   End
   Begin VB.Label lblKode 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   8
      Top             =   1020
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   7
      Top             =   615
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransDeposit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
If filename = "Hutang" Then
    frmSearch.query = "select * from ms_supplier "
Else
    frmSearch.query = "select * from ms_customer "
End If
    frmSearch.nmform = "frmTransDeposit"
    frmSearch.nmctrl = "txtKode"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchID_Click()
If filename = "Hutang" Then
    frmSearch.query = "select * from sa_hutang "
Else
    frmSearch.query = "select * from sa_piutang "
End If
    frmSearch.nmform = "frmTransDeposit"
    frmSearch.nmctrl = "txtNoTrans"
    frmSearch.connstr = strcon
    frmSearch.proc = "cek_notrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte, strFormat As String
On Error GoTo err
    i = 0
    If txtKode = "" Then
       If filename = "Hutang" Then
            MsgBox "Data Supplier harus diisi!"
       Else
            MsgBox "Data Customer harus diisi!"
       End If
        Exit Sub
    End If
    
    
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    If txtNoTrans = "-" Then
        Select Case filename
         Case "Hutang"
            nomor_baruHutang
            add_dataHutang
            add_dataHutangSupplier
          Case Else
            nomor_baruPiutang
            add_dataPiutang
            add_dataPiutangCustomer
        End Select
    End If
    

    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub add_dataHutang()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(4)
    ReDim nilai(4)
    table_name = "sa_hutang"
    
    fields(0) = "nomer_transaksi"
    fields(1) = "kode_supplier"
    fields(2) = "tanggal"
    fields(3) = "total_hutang"
    
    nilai(0) = txtNoTrans.text
    nilai(1) = txtKode.text
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = Format(txtTotal.text, "###0")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_dataPiutang()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(4)
    ReDim nilai(4)
    table_name = "sa_piutang"
    
    fields(0) = "nomer_transaksi"
    fields(1) = "kode_customer"
    fields(2) = "tanggal"
    fields(3) = "total_piutang"
    
    nilai(0) = txtNoTrans.text
    nilai(1) = txtKode.text
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = Format(txtTotal, "###0")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_dataHutangSupplier()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(3)
    ReDim nilai(3)
    table_name = "ms_supplier_hutang"
    
    fields(0) = "kode_supplier"
    fields(1) = "nomer_transaksi"
    fields(2) = "hutang"
    
    nilai(0) = txtKode.text
    nilai(1) = txtNoTrans.text
    nilai(2) = Format(txtTotal, "###0")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_dataPiutangCustomer()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(3)
    ReDim nilai(3)
    table_name = "ms_customer_piutang"
    
    fields(0) = "kode_customer"
    fields(1) = "nomer_transaksi"
    fields(2) = "piutang"
    
    nilai(0) = txtKode.text
    nilai(1) = txtNoTrans.text
    nilai(2) = Format(txtTotal, "###0")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub reset_form()
On Error Resume Next
    txtNoTrans.text = "-"
    txtKode.text = ""
    txtTotal.text = 0
    txtKode.SetFocus
    lblNama.Caption = ""
    DTPicker1 = Format(Now, "dd/MM/yyyy hh:mm:ss")
    cmdSimpan.Enabled = True
    End Sub

Public Sub cari_data()

    conn.ConnectionString = strcon
    
    conn.Open
    If filename = "Hutang" Then
        rs.Open "select * from ms_supplier where kode_supplier='" & txtKode.text & "'", conn
    Else
        rs.Open "select * from ms_customer where kode_customer='" & txtKode.text & "'", conn
    End If
   
    If Not rs.EOF Then
        lblNama.Caption = rs(1)
       
    Else
        lblNama.Caption = ""

    End If
    rs.Close
    conn.Close
End Sub

Public Sub cek_notrans()
 conn.ConnectionString = strcon
    
    conn.Open
    If filename = "Hutang" Then
        rs.Open "select * from sa_hutang where nomer_transaksi='" & txtNoTrans.text & "'", conn
    Else
        rs.Open "select * from sa_piutang where nomer_transaksi='" & txtNoTrans.text & "'", conn
    End If
   
    If Not rs.EOF Then
        DTPicker1 = rs!tanggal
        txtKode = rs(2)
        txtTotal = rs(3)

        cmdSimpan.Enabled = False
    End If
    rs.Close
    conn.Close
    cari_data
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    reset_form
    Select Case filename
    Case "Hutang"
        lblKode.Caption = "Kode Supplier"
        lblTotal.Caption = "Total Hutang"
    Case Else
        lblKode.Caption = "Kode Customer"
        lblTotal.Caption = "Total Piutang"
    End Select
End Sub


Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearch_Click
End Sub

Private Sub txtkode_KeyPress(KeyAscii As Integer)
    

    If KeyAscii = 13 And Index = 1 Then
        cari_data
    End If
 
    
End Sub

Private Sub txtKode_LostFocus()
    If txtKode <> "" Then cari_data
End Sub
Private Sub nomor_baruHutang()
Dim No As String
    rs.Open "select top 1 nomer_transaksi from sa_hutang where substring(nomer_transaksi,3,4)='" & Format(Now, "yyMM") & "' order by nomer_transaksi desc", conn
    If Not rs.EOF Then
        No = "AH" & Format(DTPicker1, "yyMM") & Format((CLng(Right(rs(0), 6)) + 1), "000000")
    Else
        No = "AH" & Format(DTPicker1, "yyMM") & Format("1", "000000")
    End If
    rs.Close
    txtNoTrans = No
End Sub
Private Sub nomor_baruPiutang()
Dim No As String
    rs.Open "select top 1 nomer_transaksi from sa_piutang where substring(nomer_transaksi,3,4)='" & Format(Now, "yyMM") & "' order by nomer_transaksi desc", conn
    If Not rs.EOF Then
        No = "AP" & Format(DTPicker1, "yyMM") & Format((CLng(Right(rs(0), 6)) + 1), "000000")
    Else
        No = "AP" & Format(DTPicker1, "yyMM") & Format("1", "000000")
    End If
    rs.Close
    txtNoTrans = No
End Sub

Private Sub txtTotal_GotFocus()
txtTotal.SelStart = 0
txtTotal.SelLength = Len(txtTotal)
End Sub

Private Sub txtTotal_KeyPress(KeyAscii As Integer)
   NumberOnly KeyAscii
End Sub
