alter table t_produksih
add primary key (nomer_produksi)
go
alter table t_produksi_pengawas
alter column nomer_produksi varchar(12) not null
go
alter table t_produksi_pengawas
alter column nik varchar(50) not null
go
alter table t_produksi_pengawas
add primary key (nomer_produksi,nik)
go
alter table t_produksi_kuli
alter column nomer_produksi varchar(12) not null
go
alter table t_produksi_kuli
alter column kuli varchar(30) not null
go
alter table t_produksi_kuli
add primary key (nomer_produksi,kuli)