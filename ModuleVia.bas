Attribute VB_Name = "ModuleInventory"
Dim totalHpp As Double
Dim NoJurnal As String
Public var_namaPerusahaan As String
Public var_alamtPerusahaan As String
Public var_telpPerusahaan As String
Public var_NPWPperusahaan As String

Public Sub load_IDperusahaan()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    var_namaPerusahaan = ""
    var_alamtPerusahaan = ""
    var_telpPerusahaan = ""
    var_NPWPperusahaan = ""
    
    conn.Open strcon
    rs.Open "select * from setting_perusahaan", conn
    If Not rs.EOF Then
       If Not IsNull(rs(0)) Then var_namaPerusahaan = rs(0) Else var_namaPerusahaan = ""
       If Not IsNull(rs(1)) Then var_alamtPerusahaan = rs(1) Else var_alamtPerusahaan = ""
       If Not IsNull(rs(2)) Then var_telpPerusahaan = rs(2) Else var_telpPerusahaan = ""
       If Not IsNull(rs(3)) Then var_NPWPperusahaan = rs(3) Else var_NPWPperusahaan = ""
    End If
    rs.Close
End Sub

Public Function getStockTinta(ByVal kdBrg As String, ByVal kdgdg As String, conn As ADODB.Connection) As Double
Dim rs As New ADODB.Recordset

rs.Open "select stock from stock_tinta s  where kode_tinta='" & kdBrg & "' and kode_gudang='" & kdgdg & "'", conn
If Not rs.EOF Then
    getStockTinta = rs(0)
Else
    getStockTinta = 0
End If
rs.Close
'conn.Close

End Function
Public Function getStockPlat(ByVal kdPlat As String, ByRef cn As ADODB.Connection) As Long
Dim rs As New ADODB.Recordset
rs.Open "select stock from stock_plat where kode_plat='" & kdPlat & "'", cn
If Not rs.EOF Then
    getStockPlat = rs(0)
Else
    getStockPlat = 0
End If
rs.Close
End Function


Public Function GetHPPKertasVia(ByVal kdBrg As String, ByVal noseri As String, ByVal kode_gudang As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select hpp from stock where kode_bahan='" & kdBrg & "' and  [nomer_serial]='" & noseri & "'  and kode_gudang='" & kode_gudang & "'", cn
If Not rs.EOF And IsNull(rs!hpp) = False Then
    GetHPPKertasVia = rs(0)
Else
    GetHPPKertasVia = 0
End If
rs.Close

End Function
Public Function GetHPPPlatVia(ByVal kdBrg As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select hpp from hpp_plat where kode_plat='" & kdBrg & "'", cn
If Not rs.EOF And IsNull(rs!hpp) = False Then
    GetHPPPlatVia = rs(0)
Else
    GetHPPPlatVia = 0
End If
rs.Close

End Function


Public Sub updateOpnameStock(ByVal kdBrg As String, qty As Double, berat As Double, hpp As Double, Serial As String, gudang As String, ByRef cn As ADODB.Connection)
'inser stock
    cn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kdBrg & "'and nomer_serial='" & Serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,qty,hpp,nomer_serial,kode_gudang) values ('" & kdBrg & "'," & berat & "," & qty & "," & hpp & ",'" & Serial & "','" & gudang & "') else " & _
     "update stock set stock=" & berat & ",qty=" & qty & ",hpp='" & hpp & "' where kode_bahan='" & kdBrg & "' and nomer_serial='" & Serial & "'  and kode_gudang='" & gudang & "' "

End Sub

Public Sub updateOpnameStockKertas2(ByVal kdBrg As String, qty As Double, Serial As String, gudang As String, ByRef cn As ADODB.Connection)
'inser stock
'    cn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kdBrg & "'and nomer_serial='" & serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & serial & "','" & gudang & "') else " & _
'     "update stock set stock=" & Replace(qty, ",", ".") & " where kode_bahan='" & kdBrg & "' and nomer_serial='" & serial & "'"

    cn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kdBrg & "'and nomer_serial='" & Serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & Serial & "','" & gudang & "') else " & _
     "update stock set stock=stock+" & Replace(qty, ",", ".") & " where kode_bahan='" & kdBrg & "' and nomer_serial='" & Serial & "'"

'insert bahan
    cn.Execute "if not exists (select kode_bahan from ms_bahan where kode_bahan='" & kdBrg & "') insert into ms_bahan (kode_bahan) values ('" & kdBrg & "') "
End Sub

Private Sub updatehpp(NoTransaksi As String, tanggal As Date, gudang As String, kode_bahan As String, qty As Double, ByRef cn As ADODB.Connection, Optional hpp As Double)

'   cn.Execute "if not exists(select * from hpp_bahan where kode_bahan='" & kode_bahan & "') " & _
'                "insert into hpp_bahan values ('" & kode_bahan & "',0)"
    cn.Execute "insert into hst_hpp (jenis,nomer_transaksi,tanggal,kode_gudang,kode_bahan,stockawal,hppawal,qty,harga,hpp,stockakhir) values " & _
    "('OpnameBahan','" & NoTransaksi & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & gudang & "','" & kode_bahan & "',0,0,'" & Replace(qty, ",", ".") & "'," & hpp & "," & hpp & ",'" & Replace((StockAwal + qty), ",", ".") & "')"

End Sub

Public Sub updateOpnameStockTinta(ByVal gudang As String, ByVal kdBrg As String, qty As Double, ByRef cn As ADODB.Connection)
'insert stock tinta
    cn.Execute "if not exists (select kode_tinta from stock_tinta where kode_tinta='" & kdBrg & "' and kode_gudang='" & gudang & "') insert into stock_tinta (kode_tinta,stock,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & gudang & "') else " & _
    "update stock_tinta set stock=" & Replace(qty, ",", ".") & " where kode_gudang='" & gudang & "' and kode_tinta='" & kdBrg & "'"
'insert ms_tinta
    cn.Execute "if not exists (select kode_tinta from ms_tinta where kode_tinta='" & kdBrg & "') insert into ms_tinta (kode_tinta) values ('" & kdBrg & "') "
    
End Sub



Private Sub updatehpptinta(NoTransaksi As String, tanggal As Date, gudang As String, kode_tinta As String, qty As Double, hpp As Double, ByRef cn As ADODB.Connection)

    cn.Execute "if not exists(select * from hpp_tinta where kode_tinta='" & kode_tinta & "') " & _
                "insert into hpp_tinta values ('" & kode_tinta & "',0)"
    cn.Execute "insert into hst_hpptinta (jenis,nomer_transaksi,tanggal,kode_gudang,kode_tinta,stockawal,hppawal,qty,harga,hpp,stockakhir) values " & _
    "('OpnameTinta','" & NoTransaksi & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & gudang & "','" & kode_tinta & "',0,0,'" & Replace(qty, ",", ".") & "',0," & Replace(hpp, ",", ".") & ",'" & Replace((StockAwal + qty), ",", ".") & "')"

End Sub
Public Sub updateOpnameStockPlat(ByVal kdBrg As String, qty As Long, ByRef cn As ADODB.Connection)
    
    cn.Execute "if not exists (select kode_plat from stock_plat where kode_plat='" & kdBrg & "' ) insert into stock_plat (kode_plat,stock) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ") else " & _
    "update stock_plat set stock=" & Replace(qty, ",", ".") & " where kode_plat='" & kdBrg & "'"
'insert bahan
    cn.Execute "if not exists (select kode_plat from ms_plat where kode_plat='" & kdBrg & "') insert into ms_plat (kode_plat) values ('" & kdBrg & "') "
    cn.Execute "if not exists (select kode_plat from hpp_plat where kode_plat='" & kdBrg & "') insert into hpp_plat (kode_plat,hpp) values ('" & kdBrg & "',0) "
        
   
End Sub

Public Sub updateOpnameStockPlat2(ByVal gudang As String, ByVal kdBrg As String, qty As Long, ByRef cn As ADODB.Connection)
    
    cn.Execute "if not exists (select kode_plat from stock_plat where kode_plat='" & kdBrg & "' and kode_gudang='" & gudang & "') insert into stock_plat (kode_plat,stock,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & gudang & "') else " & _
    "update stock_plat set stock=stock+" & Replace(qty, ",", ".") & " where  kode_gudang='" & gudang & "' and kode_plat='" & kdBrg & "'"
'insert bahan
    cn.Execute "if not exists (select kode_plat from ms_plat where kode_plat='" & kdBrg & "') insert into ms_plat (kode_plat) values ('" & kdBrg & "') "
        
   
End Sub

Public Sub kartuStockPlat(NoTrans As String, kode As String, qty As Long, tanggal As Date, Tipe As String, hpp As Double, ByRef cn As ADODB.Connection)
Dim StockAwal As Long
Dim stockInput As Long
    StockAwal = getStockPlat(kode, cn)
    stockInput = qty - StockAwal
    If stockInput > 0 Then
        masuk = stockInput
        keluar = 0
    Else
        keluar = (stockInput) * -1
        masuk = 0
    End If
    cn.Execute "insert into tmp_kartuStockPlat(kode_plat,tipe,gudang,id,tanggal,masuk,keluar,stockawal,hpp) values " & _
    "('" & kode & "','" & Tipe & "','" & gudang & "','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & masuk & "','" & keluar & "','" & StockAwal & "','" & hpp & "')"

End Sub
'Public Sub kartuStockKertas(NoTrans As String, kode As String, qty As Long, tanggal As Date, tipe As String, serial As String, gudang As String, byref cn As ADODB.Connection)
'Dim stockawal As Long
'Dim stockInput As Long
'    stockawal = GetStockKertas(serial, gudang, cn)
'    stockInput = qty - stockawal
'    If stockInput > 0 Then
'        masuk = stockInput
'        keluar = 0
'    Else
'        keluar = (stockInput) * -1
'        masuk = 0
'    End If
'    cn.Execute "insert into tmp_kartustock(kode_bahan,tipe,gudang,nomer_serial,id,tanggal,masuk,keluar,stockawal) values " & _
'    "('" & kode & "','" & tipe & "','" & gudang & "','" & serial & "','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & masuk & "','" & keluar & "','" & stockawal & "')"
'
'End Sub

Public Sub updateKoreksiStockTinta(ByVal gudang As String, ByVal kdBrg As String, qty As Double, ByRef cn As ADODB.Connection)
     
     cn.Execute "if not exists (select kode_tinta from stock_tinta where kode_tinta='" & kdBrg & "'  and kode_gudang='" & gudang & "') insert into stock_tinta (kode_tinta,stock,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & gudang & "') else " & _
     "update stock_tinta set stock=stock + " & Replace(qty, ",", ".") & " where kode_gudang='" & gudang & "' and kode_tinta='" & kdBrg & "'"
'insert tinta
    cn.Execute "if not exists (select kode_tinta from ms_tinta where kode_tinta='" & kdBrg & "') insert into ms_tinta (kode_tinta) values ('" & kdBrg & "') "

End Sub
Public Sub updateKoreksiStockKertas(ByVal kdBrg As String, qty As Double, Serial As String, gudang As String, ByRef cn As ADODB.Connection)
    cn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kdBrg & "'and nomer_serial='" & Serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & Serial & "','" & gudang & "') else " & _
    "update stock set stock=stock+ " & Replace(qty, ",", ".") & " where  kode_bahan='" & kdBrg & "' and nomer_serial='" & Serial & "' and kode_gudang='" & gudang & "'"
'insert bahan
    'cn.Execute "if not exists (select kode_bahan from ms_bahan where kode_bahan='" & kdBrg & "') insert into ms_bahan (kode_bahan) values ('" & kdBrg & "') "

End Sub
'Public Sub updateKoreksiStockPlat(ByVal kdBrg As String, qty As Long, byref cn As ADODB.Connection)
'    cn.Execute "update stock_plat set stock=stock + " & qty & " where kode_plat='" & kdBrg & "'"
'End Sub
Public Sub updateMutasiStockTinta(ByVal gudangAsal As String, ByVal gudangTujuan As String, ByVal kdBrg As String, qty As Double, ByRef cn As ADODB.Connection)
   cn.Execute "update stock_tinta set stock=stock - " & Replace(qty, ",", ".") & " where kode_gudang='" & gudangAsal & "' and kode_tinta='" & kdBrg & "'"
   cn.Execute "if not exists (select kode_tinta from stock_tinta where kode_tinta='" & kdBrg & "' and kode_gudang='" & gudangTujuan & "') insert into stock_tinta (kode_tinta,stock,kode_gudang) values ('" & kdBrg & "'," & Replace(qty, ",", ".") & ",'" & gudangTujuan & "') else " & _
   "update stock_tinta set stock=stock+" & Replace(qty, ",", ".") & " where kode_gudang='" & gudangTujuan & "' and kode_tinta='" & kdBrg & "'"
 
End Sub
Private Sub add_jurnal(NoTransaksi As String, NoJurnal As String, tanggal As Date, keterangan As String, conn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "tipe"
    fields(5) = "userid"

    nilai(0) = NoTransaksi
    nilai(1) = NoJurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
    nilai(4) = Left(NoTransaksi, 3)
    nilai(5) = User
    
   conn.Execute tambah_data2(table_name, fields, nilai)
   End Sub
Public Sub postingKoreksiTinta(NoTransaksi As String, pesan As Boolean)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
'On Error GoTo err
conn.Open strcon
conn.BeginTrans
i = 1
totalHpp = 0

   rs.Open "select a.*,b.kode_tinta,b.qty,b.no_urut,b.hpp from t_stockkoreksiTintaH a inner join t_stockkoreksiTintaD b on a.nomer_koreksiTinta=b.nomer_koreksiTinta  where a.nomer_koreksiTinta='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
   tanggal1 = rs!tanggal
        NoJurnal = Nomor_Jurnal2(tanggal1)
        add_jurnal NoTransaksi, NoJurnal, tanggal1, "Koreksi", conn

        While Not rs.EOF
            Row = Row + 1
            add_jurnaldetailKoreksiTinta rs!kode_tinta, rs!qty, rs!kode_gudang, NoTransaksi, NoJurnal, Row, rs!hpp, conn
            insertkartustockTintaGlobal NoTransaksi, rs!kode_tinta, rs!qty, tanggal1, "Koreksi", rs!kode_gudang, rs!hpp, conn
            updatehpptinta NoTransaksi, tanggal1, rs!kode_gudang, rs!kode_tinta, rs!qty, rs!hpp, conn
        rs.MoveNext
        Wend

    End If
    rs.Close

    conn.Execute "update t_stockkoreksiTintaH set status_posting=1 where nomer_koreksiTinta='" & NoTransaksi & "'"

conn.CommitTrans
i = 0
DropConnection
If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description



End Sub



Public Sub add_jurnaldetailKoreksiTinta(kode As String, qty As Double, gudang As String, NoTransaksi As String, NoJurnal As String, Row As Integer, hpp As Currency, ByRef cn As ADODB.Connection)
Dim Acc_Persediaan As String
Dim selisih As Double
Dim rs2 As New ADODB.Recordset
'On Error GoTo err
   rs2.Open "Select kode_acc from setting_accTinta  " & _
            "where kode_tinta='" & kode & "' and kode_gudang='" & gudang & "'", cn
    If Not rs2.EOF Then
        Acc_Persediaan = rs2("kode_acc")
    End If
    rs2.Close

    selisih = qty
    totalHpp = hpp * selisih

    If selisih < 0 Then
        JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", -totalHpp, cn, "Koreksi", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", -totalHpp, cn, "Koreksi", kode, , , 2
    Else
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "D", totalHpp, cn, "Koreksi", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, var_accpendapatanlain, "K", totalHpp, cn, "Koreksi", kode, , , 2
    End If
    Exit Sub
err:
  Debug.Print NoTransaksi & " " & err.Description
End Sub


Public Sub postingKoreksiKertas(NoTransaksi As String, pesan As Boolean)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
Dim currentstock As Double, currenthpp As Double, newHPP As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
'On Error GoTo err
conn.Open strcon
conn.BeginTrans
i = 1
totalHpp = 0
   rs.Open "select a.*,b.kode_bahan,b.qty,b.nomer_serial,b.no_urut,b.hpp,b.berat from t_stockkoreksiH a inner join t_stockkoreksiD b on a.nomer_koreksi=b.nomer_koreksi where a.nomer_koreksi='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
    tanggal1 = rs!tanggal
    NoJurnal = Nomor_Jurnal2(tanggal1)
    add_jurnal NoTransaksi, NoJurnal, tanggal1, "Koreksi", conn
        While Not rs.EOF
            Row = Row + 1
            add_jurnaldetailKoreksiKertas rs!kode_bahan, rs!berat, rs!kode_gudang, NoTransaksi, NoJurnal, rs!nomer_serial, Row, rs!hpp, conn
            
            insertkartustockBeras "Koreksi", NoTransaksi, tanggal1, rs!kode_bahan, rs!qty, rs!berat, rs!kode_gudang, rs!nomer_serial, rs!hpp, conn
            
        rs.MoveNext
        Wend

    End If
    rs.Close

    conn.Execute "update t_stockkoreksiH set status_posting=1 where nomer_koreksi='" & NoTransaksi & "'"

conn.CommitTrans
i = 0
DropConnection
If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description

End Sub
Public Sub add_jurnaldetailKoreksiKertas(kode As String, qty As Double, gudang As String, NoTransaksi As String, NoJurnal As String, Serial As String, Row As Integer, hpp As Currency, ByRef cn As ADODB.Connection)
Dim Acc_Persediaan As String
Dim selisih As Double
Dim rs2 As New ADODB.Recordset
'On Error GoTo err

     rs2.Open "Select a.acc_persediaan from setting_accbahan a inner join stock b on a.kode_bahan=b.kode_bahan  " & _
            "where a.kode_bahan='" & kode & "' and b.kode_gudang='" & gudang & "' and b.nomer_serial='" & Serial & "'", cn
    If Not rs2.EOF Then
        Acc_Persediaan = rs2("acc_persediaan")
    End If
    rs2.Close

    selisih = qty
    totalHpp = hpp * selisih

       If selisih < 0 Then
        JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", -totalHpp, cn, "Koreksi", kode, Serial, , 1
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", -totalHpp, cn, "Koreksi", kode, Serial, , 2
    Else
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "D", totalHpp, cn, "Koreksi", kode, Serial, , 1
        JurnalD NoTransaksi, NoJurnal, var_accpendapatanlain, "K", totalHpp, cn, "Koreksi", kode, Serial, , 2
    End If
  Exit Sub
err:
Debug.Print NoTransaksi & " " & err.Description
End Sub

'Public Sub postingOpnameKertas(NoTransaksi As String, pesan As Boolean)  ', conn As ADODB.Connection)
'Dim i As Integer
'Dim id As String
'Dim row As Integer
'Dim tanggal1 As Date
'Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
'Dim rs As New ADODB.Recordset
'Dim conn As New ADODB.Connection
''On Error GoTo err
'conn.Open strcon
'conn.BeginTrans
'i = 1
'
'   totalHpp = 0
'
'   rs.Open "select a.*,b.kode_bahan,b.qty,b.nomer_serial,b.selisih,b.no_urut,b.hpp from t_stockopnameh a inner join t_stockopnamed b on a.nomer_opname=b.nomer_opname where a.nomer_opname='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
'   If Not rs.EOF Then
'   tanggal1 = rs!tanggal
'        NoJurnal = Nomor_Jurnal2(tanggal1)
'        add_jurnal NoTransaksi, NoJurnal, tanggal1, "Opname", conn
'
'        While Not rs.EOF
'            row = row + 1
'            add_jurnaldetailOpnameKertas rs!kode_bahan, rs!selisih, rs!kode_gudang, NoTransaksi, NoJurnal, rs!nomer_serial, row, rs!hpp, conn
'            insertkartustockOpname "Opname", NoTransaksi, tanggal1, rs!kode_bahan, rs!qty, rs!selisih, rs!kode_gudang, rs!nomer_serial, rs!hpp, conn
'
''            updateOpnameStockKertas rs!kode_bahan, rs!qty, rs!nomer_serial, rs!kode_gudang, conn
'            updatehpp NoTransaksi, tanggal1, rs!kode_gudang, rs!kode_bahan, rs!qty, conn
'
'        rs.MoveNext
'        Wend
'
'    End If
'    rs.Close
'
'    conn.Execute "update t_stockopnameh set status_posting=1 where nomer_opname='" & NoTransaksi & "'"
'
'conn.CommitTrans
'i = 0
'DropConnection
'If pesan = True Then MsgBox "Proses Posting telah berhasil"
'Exit Sub
'err:
'    If i = 1 Then
'        conn.RollbackTrans
'    End If
'    DropConnection
'    debug.print no_transaksi &" "& err.description
'
'End Sub
'Public Sub add_jurnaldetailOpnameKertas(kode As String, qty As Double, gudang As String, NoTransaksi As String, NoJurnal As String, Serial As String, row As Integer, hpp As Currency, byref cn As ADODB.Connection)
'Dim Acc_Persediaan As String
'Dim selisih As Double
'
'Dim qty_stock As Double
'Dim rs2 As New ADODB.Recordset
''Dim cn As New ADODB.Connection
'
''On Error GoTo err
''cn.Open strcon
'    rs2.Open "Select a.acc_persediaan from setting_accbahan a inner join stock b on a.kode_bahan=b.kode_bahan  " & _
'            "where a.kode_bahan='" & kode & "' and b.kode_gudang='" & gudang & "' and b.nomer_serial='" & Serial & "'", cn
'    If Not rs2.EOF Then
'        Acc_Persediaan = rs2("acc_persediaan")
'    End If
'    rs2.Close
'
'
'    totalHpp = hpp * qty
'
'    If qty < 0 Then
'        JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", -totalHpp, cn, "Opname", kode, Serial, , 1
'        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", -totalHpp, cn, "Opname", kode, Serial, , 2
'    Else
'        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "D", totalHpp, cn, "Opname", kode, Serial, , 1
'        JurnalD NoTransaksi, NoJurnal, var_accpendapatanlain, "K", totalHpp, cn, "Opname", kode, Serial, , 2
'    End If
''cn.Close
'  Exit Sub
'err:
'debug.print no_transaksi &" "& err.description
'End Sub

Public Sub postingOpname(NoTransaksi As String, pesan As Boolean)  ', conn As ADODB.Connection)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
Dim stockgudanglain As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim StockAwal As Double, qtyawal As Double
Dim SelisihQty As Double, SelisihBerat As Double

On Error GoTo err
conn.Open strcon
conn.BeginTrans
i = 1

   totalHpp = 0
        
   rs.Open "select a.*,b.kode_bahan,b.qty,b.berat,b.nomer_serial,b.stock_qty,b.stock_berat,b.selisih_qty,b.selisih_berat,b.no_urut,b.hpp,hpp_baru,b.berat,kode_gudang from t_stockopnameh a inner join t_stockopnamed b on a.nomer_opname=b.nomer_opname where a.nomer_opname='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
   tanggal1 = rs!tanggal
        
        Dim rs1 As New ADODB.Recordset
        While Not rs.EOF
            Row = Row + 1

            
            StockAwal = getStockKertas2(rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn)
            qtyawal = getQty(rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn)
            SelisihQty = rs!qty - qtyawal
            SelisihBerat = rs!berat - StockAwal
            conn.Execute "update t_stockopnamed set stock_qty='" & qtyawal & "',stock_berat='" & StockAwal & "',selisih_qty='" & SelisihQty & "',selisih_berat='" & SelisihBerat & "' where nomer_opname='" & NoTransaksi & "' and kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "' and kode_gudang='" & rs!kode_gudang & "'"
            insertkartustock "Opname", NoTransaksi, tanggal1, rs!kode_bahan, SelisihQty, SelisihBerat, rs!kode_gudang, rs!nomer_serial, rs!hpp, conn
'            insertkartustockOpname "Opname", NoTransaksi, tanggal1, rs!kode_bahan, rs!qty, rs!selisih, rs!kode_gudang, rs!nomer_serial, rs!hpp_baru, conn
            updateOpnameStock rs!kode_bahan, rs!qty, rs!berat, rs!hpp_baru, rs!nomer_serial, rs!kode_gudang, conn
'            conn.Execute "update stock set hpp='" & Replace(rs!hpp_baru, ",", ".") & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "'"
'            updatehpp NoTransaksi, tanggal1, rs!kode_gudang, rs!kode_bahan, rs!qty, conn, rs!hpp_baru

        rs.MoveNext
        Wend

    End If
    rs.Close
    conn.Execute "update t_stockopnameh set status_posting=1 where nomer_opname='" & NoTransaksi & "'"
conn.CommitTrans
i = 0
DropConnection
If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description

End Sub
Public Sub add_jurnaldetailOpnameKertas(kode As String, qty As Double, gudang As String, NoTransaksi As String, NoJurnal As String, Serial As String, Row As Integer, hpp As Currency, ByRef cn As ADODB.Connection)
Dim Acc_Persediaan As String
Dim selisih As Double

Dim qty_stock As Double
Dim rs2 As New ADODB.Recordset
'Dim cn As New ADODB.Connection

'On Error GoTo err
'cn.Open strcon
    rs2.Open "Select a.acc_persediaan from setting_accbahan a " & _
            "where a.kode_bahan='" & kode & "'", cn
    If Not rs2.EOF Then
        Acc_Persediaan = rs2("acc_persediaan")
    End If
    rs2.Close
    
    
    totalHpp = hpp * qty

    If totalHpp < 0 Then
        JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", -totalHpp, cn, "Opname", kode, Serial, , 1
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", -totalHpp, cn, "Opname", kode, Serial, , 2
    Else
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "D", totalHpp, cn, "Opname", kode, Serial, , 1
        JurnalD NoTransaksi, NoJurnal, var_accpendapatanlain, "K", totalHpp, cn, "Opname", kode, Serial, , 2
    End If
'cn.Close
  Exit Sub
err:
Debug.Print NoTransaksi & " " & err.Description
End Sub


Public Sub postingOpnameTinta(NoTransaksi As String, pesan As Boolean)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
''On Error GoTo err
conn.Open strcon
conn.BeginTrans
i = 1
totalHpp = 0
   rs.Open "select a.*,b.kode_tinta,b.qty,b.no_urut,b.hpp,b.selisih from t_stockOpnameTintaH a inner join t_stockOpnameTintaD b on a.nomer_opnameTinta=b.nomer_opnameTinta  where a.nomer_opnameTinta='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
   tanggal1 = rs!tanggal
        NoJurnal = Nomor_Jurnal2(tanggal1)
        add_jurnal NoTransaksi, NoJurnal, tanggal1, "Opname", conn

        While Not rs.EOF
            Row = Row + 1
            add_jurnaldetailOpnameTinta rs!kode_tinta, rs!selisih, rs!kode_gudang, NoTransaksi, NoJurnal, Row, IIf(IsNull(rs!hpp), 0, rs!hpp), conn
            insertkartustockTintaOpname NoTransaksi, rs!kode_tinta, rs!qty, rs!selisih, tanggal1, "Opname", rs!kode_gudang, IIf(IsNull(rs!hpp), 0, rs!hpp), conn
'            updateOpnameStockTinta rs!kode_gudang, rs!kode_tinta, rs!qty, conn
            updatehpptinta NoTransaksi, tanggal1, rs!kode_gudang, rs!kode_tinta, rs!qty, rs!hpp, conn
            
        rs.MoveNext
        Wend

    End If
    rs.Close
    
    conn.Execute "update t_stockopnameTintaH set status_posting=1 where nomer_opnameTinta='" & NoTransaksi & "'"

conn.CommitTrans
i = 0
DropConnection
If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description
End Sub



Public Sub add_jurnaldetailOpnameTinta(kode As String, qty As Double, gudang As String, NoTransaksi As String, NoJurnal As String, Row As Integer, hpp As Currency, ByRef cn As ADODB.Connection)
Dim Acc_Persediaan As String
Dim selisih As Double
Dim qty_stock As Double

Dim rs2 As New ADODB.Recordset
'Dim cn As New ADODB.Connection
'cn.Open strcon
'On Error GoTo err
    rs2.Open "Select kode_acc from setting_accTinta  " & _
            "where kode_tinta='" & kode & "' and kode_gudang='" & gudang & "'", cn
    If Not rs2.EOF Then
        Acc_Persediaan = rs2("kode_acc")
    End If
    rs2.Close
    
    selisih = qty
    totalHpp = hpp * selisih

    If selisih < 0 Then
        JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", -totalHpp, cn, "Opname", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", -totalHpp, cn, "Opname", kode, , , 2
    Else
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "D", totalHpp, cn, "Opname", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, var_accpendapatanlain, "K", totalHpp, cn, "Opname", kode, , , 2
    End If
'cn.Close
'    add_jurnaldetail totalHpp, Acc_Persediaan, selisih, NoTransaksi
    Exit Sub
err:
  Debug.Print NoTransaksi & " " & err.Description
End Sub




Public Sub postingOpnamePlat(NoTransaksi As String, pesan As Boolean) ', conn As ADODB.Connection)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
'On Error GoTo err
conn.Open strcon
conn.BeginTrans
i = 1
    totalHpp = 0
   rs.Open "select a.*,b.kode_plat,b.qty,b.no_urut,b.selisih,b.hpp from t_stockopnamePlath a inner join t_stockopnamePlatd b on a.nomer_opnamePlat=b.nomer_opnamePlat where a.nomer_opnamePlat='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
   tanggal1 = rs!tanggal
        NoJurnal = Nomor_Jurnal2(tanggal1)
        add_jurnal NoTransaksi, NoJurnal, tanggal1, "Opname", conn
   
        While Not rs.EOF
            Row = Row + 1
            add_jurnaldetailOpnamePlat rs!kode_plat, rs!selisih, NoTransaksi, NoJurnal, Row, rs!hpp, conn
            insertkartustockPlatOpname NoTransaksi, rs!kode_plat, rs!qty, rs!selisih, tanggal1, "Opname", "GTINTA", rs!hpp, conn
'            updateOpnameStockPlat rs!kode_plat, rs!qty, conntanggal1

        rs.MoveNext
        Wend
    End If
    rs.Close

    conn.Execute "update t_stockopnamePlath set status_posting=1 where nomer_opnamePlat='" & NoTransaksi & "'"

conn.CommitTrans
i = 0
DropConnection
If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description

End Sub
Public Sub add_jurnaldetailOpnamePlat(kode As String, qty As Double, NoTransaksi As String, NoJurnal As String, Row As Integer, hpp As Currency, ByRef cn As ADODB.Connection)
Dim Acc_Persediaan As String
Dim selisih As Double

Dim qty_stock As Double
Dim rs2 As New ADODB.Recordset
'Dim cn As New ADODB.Connection

'On Error GoTo err
'cn.Open strcon
    rs2.Open "Select kode_acc from ms_plat " & _
            "where kode_plat='" & kode & "' ", cn
    If Not rs2.EOF Then
        Acc_Persediaan = rs2("kode_acc")
    End If
    rs2.Close
    
    selisih = qty
    totalHpp = hpp * selisih

    If selisih < 0 Then
        JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", -totalHpp, cn, "Opname", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", -totalHpp, cn, "Opname", kode, , , 2
    Else
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "D", totalHpp, cn, "Opname", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, var_accpendapatanlain, "K", totalHpp, cn, "Opname", kode, , , 2
    End If
'cn.Close
  Exit Sub
err:
Debug.Print NoTransaksi & " " & err.Description
End Sub

Public Sub postingMutasiBahan(NoTransaksi As String, pesan As Boolean)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
'On Error GoTo err
conn.Open strcon
conn.BeginTrans
i = 1
totalHpp = 0
   rs.Open "select * from t_stockMutasibahanH a inner join t_stockMutasibahanD b on a.nomer_Mutasibahan=b.nomer_Mutasibahan  where a.nomer_Mutasibahan='" & NoTransaksi & "' order by b.no_urut", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
   tanggal1 = rs!tanggal
        Dim currentstock As Double
        Dim currenthpp As Currency
        Dim newHPP As Currency
        While Not rs.EOF
             Row = Row + 1
            currenthpp = GetHPPKertas2(rs!kode_bahan, rs!nomer_serial, rs!kode_gudangTujuan, conn)
            insertkartustock "Mutasi", NoTransaksi, tanggal1, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_gudangAsal, rs!nomer_serial, CDbl(currenthpp), conn
            insertkartustock "Mutasi", NoTransaksi, tanggal1, rs!kode_bahan, rs!qty, rs!berat, rs!kode_gudangTujuan, rs!nomer_serial, CDbl(currenthpp), conn
            currentstock = getStockKertas2(rs!kode_bahan, rs!kode_gudangTujuan, rs!nomer_serial, conn)
'            If currentstock > 0 Then
'            newHPP = ((currentstock * currenthpp) + (rs!qty * rs!hpp)) / (currentstock + rs!qty)
'            Else
'            newHPP = rs!hpp
'            End If
            updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudangAsal, rs!nomer_serial, rs!qty * -1, rs!berat * -1, rs!hpp, conn
            updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudangTujuan, rs!nomer_serial, rs!qty, rs!berat, rs!hpp, conn
'            conn.Execute "update stock set hpp='" & Replace(newHPP, ",", ".") & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "'"

            'updateMutasiStockTinta rs!kode_gudangasal, rs!kode_gudangtujuan, rs!kode_bahan, rs!nomer_serial, rs!qty, conn
            
        rs.MoveNext
        Wend

    End If
    rs.Close
    conn.Execute "update t_stockMutasibahanH set status_posting=1 where nomer_Mutasibahan='" & NoTransaksi & "'"
conn.CommitTrans
i = 0
DropConnection
If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description
End Sub
'Public Sub postingTransferBahan(NoTransaksi As String, pesan As Boolean)
'Dim i As Integer
'Dim id As String
'Dim row As Integer
'Dim tanggal1 As Date
'Dim JumlahLama As Double, jumlah As Double, HPPLama As Double
'Dim NoJurnal As String
'Dim rs As New ADODB.Recordset
'Dim conn As New ADODB.Connection
''On Error GoTo err
'conn.Open strcon
'conn.BeginTrans
'i = 1
'totalHpp = 0
'   rs.Open "select a.*,b.acc_persediaan as acc_bahan,c.acc_persediaan as acc_hasil from t_transferbahan a left join setting_accbahan b on a.kode_bahan=b.kode_bahan left join setting_accbahan c on a.kode_hasil=c.kode_bahan where a.nomer_transferbahan='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
'   If Not rs.EOF Then
'   tanggal1 = rs!tanggal
'   If rs!acc_bahan = "" Or rs!acc_hasil = "" Then
'    MsgBox "Setting Account persediaan bahan belum lengkap"
'    GoTo err
'   End If
'   NoJurnal = Nomor_Jurnal2(tanggal1)
'   add_jurnal NoTransaksi, NoJurnal, tanggal1, "Ubah Bahan", conn
'
'        Dim currentstock As Double
'        Dim currenthpp As Currency
'        Dim newHPP As Currency
'        While Not rs.EOF
'             row = row + 1
'            currenthpp = GetHPPKertas2(rs!kode_bahan, rs!nomer_serial, rs!kode_gudang, conn)
'            insertkartustock "Ubah Bahan", NoTransaksi, tanggal1, rs!kode_bahan, rs!qty * -1, rs!kode_gudang, rs!nomer_serial, CDbl(currenthpp), conn
'            insertkartustock "Ubah Bahan", NoTransaksi, tanggal1, rs!kode_hasil, rs!qty, rs!kode_gudang, rs!nomer_serial_hasil, CDbl(currenthpp), conn
'            currentstock = getStockKertas2(rs!kode_hasil, rs!kode_gudang, rs!nomer_serial_hasil, conn)
'            currenthpp = GetHPPKertas2(rs!kode_hasil, rs!nomer_serial_hasil, rs!kode_gudang, conn)
'            'newHPP = ((currentstock * currenthpp) + (rs!qty * rs!hpp)) / (currentstock + rs!qty)
'            'conn.Execute "update stock set hpp='" & Replace(newHPP, ",", ".") & "',no_history='" & rs!no_history & "' where kode_bahan='" & rs!kode_hasil & "' and nomer_serial='" & rs!nomer_serial_hasil & "' and kode_gudang='" & rs!kode_gudang & "'"
'            'cn.Execute "insert into hst_hpp (jenis,nomer_transaksi,kode_bahan,stockawal,hppawal,qty,harga,hpp,stockakhir) values " & _
'                "('Koreksi','" & NoTransaksi & "','" & rs!kode_hasil & "','" & Replace(currentstock, ",", ".") & "','" & Replace(currenthpp, ",", ".") & "','" & Replace(Format(rs!qty, "###0.##"), ",", ".") & "','" & Replace(Format(rs!hpp, "###0.##"), ",", ".") & "','" & Replace(Format(newHPP, "###0.##"), ",", ".") & "','" & Replace(Format(currentstock + rs!qty, "###0.##"), ",", ".") & "')"
'
'            updateKoreksiStockKertas rs!kode_bahan, rs!qty * -1, rs!nomer_serial, rs!kode_gudang, conn
'            updateKoreksiStockKertas rs!kode_hasil, rs!qty, rs!nomer_serial_hasil, rs!kode_gudang, conn
'            'conn.Execute "update stock set hpp='" & Replace(newHPP, ",", ".") & "',no_history='" & rs!no_history & "' where kode_bahan='" & rs!kode_hasil & "' and nomer_serial='" & rs!nomer_serial_hasil & "' and kode_gudang='" & rs!kode_gudang & "'"
'
'            JurnalD NoTransaksi, NoJurnal, rs!acc_bahan, "k", rs!qty * rs!hpp, conn, "Ubah bahan", rs!kode_bahan, rs!nomer_serial, rs!qty & "X" & rs!hpp, 1
'            JurnalD NoTransaksi, NoJurnal, rs!acc_bahan, "d", rs!qty * rs!hpp, conn, "Ubah bahan", rs!kode_hasil, rs!nomer_serial_hasil, rs!qty & "X" & rs!hpp, 2
'
'            'updateMutasiStockTinta rs!kode_gudangasal, rs!kode_gudangtujuan, rs!kode_bahan, rs!nomer_serial, rs!qty, conn
'
'        rs.MoveNext
'        Wend
'
'    End If
'    rs.Close
'    conn.Execute "update t_transferbahan set status_posting=1 where nomer_Transferbahan='" & NoTransaksi & "'"
'conn.CommitTrans
'i = 0
'DropConnection
'If pesan = True Then MsgBox "Proses Posting telah berhasil"
'Exit Sub
'err:
'    If i = 1 Then
'        conn.RollbackTrans
'    End If
'    DropConnection
'    Debug.Print NoTransaksi & " " & err.Description
'End Sub
Public Sub add_jurnaldetailMutasiTinta(kode As String, qty As Double, gudang As String, gudangTujuan As String, NoTransaksi As String, NoJurnal As String, Row As Integer, ByRef cn As ADODB.Connection)
Dim Acc_Persediaan As String
Dim Acc_PersediaanTujuan As String
Dim selisih As Double
Dim qty_stock As Double

Dim rs2 As New ADODB.Recordset
'Dim cn As New ADODB.Connection
'cn.Open strcon
'On Error GoTo err
    rs2.Open "Select kode_acc from setting_accTinta  " & _
            "where kode_tinta='" & kode & "' and kode_gudang='" & gudang & "'", cn
    If Not rs2.EOF Then
        Acc_Persediaan = rs2("kode_acc")
    End If
    rs2.Close
    rs2.Open "Select kode_acc from setting_accTinta  " & _
            "where kode_tinta='" & kode & "' and kode_gudang='" & gudangTujuan & "'", cn
    If Not rs2.EOF Then
        Acc_PersediaanTujuan = rs2("kode_acc")
    End If
    rs2.Close
    
    qty_stock = qty
    hpp = GetHPPTinta(kode, cn)
    totalHpp = hpp * qty_stock
        
        JurnalD NoTransaksi, NoJurnal, Acc_PersediaanTujuan, "D", totalHpp, cn, "Mutasi", kode, , , 1
        JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", totalHpp, cn, "Mutasi", kode, , , 2

    Exit Sub
err:
  Debug.Print NoTransaksi & " " & err.Description
End Sub






Public Sub posting_PlatBaru(NoTransaksi As String, pesan As Boolean)
'On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim i As Byte
i = 0
conn.Open strcon
conn.BeginTrans
i = 1
    rs.Open "select * from t_plat_baru where nomer_transaksi='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
         conn.Execute "If Exists(select * from serial_plat where kode_plat='" & rs!kode_plat & "' and nomer_serial='" & rs!nomer_serial & "' ) update serial_plat set status=1 else insert into serial_plat (kode_plat,nomer_serial,status) values('" & rs!kode_plat & "','" & rs!nomer_serial & "',1) "
    End If
    rs.Close
        conn.Execute "update t_plat_baru set status_posting=1 where nomer_transaksi='" & NoTransaksi & "'"
conn.CommitTrans
i = 0
  If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    Debug.Print NoTransaksi & " " & err.Description
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub

Public Sub posting_PlatRusak(NoTransaksi As String)
'On Error GoTo err
Dim Acc_Persediaan As String
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim i As Byte, KodePlat As String
Dim hpp As Double, totalHpp As Double, qty As Double
Dim NoJurnal As String
Dim tanggal As Date
Dim Serial As String
i = 0
conn.Open strcon
conn.BeginTrans
i = 1
    rs.Open "select * from t_plat_rusak where nomer_transaksi='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
         tanggal = rs("tanggal")
         KodePlat = rs("kode_plat")
         Serial = rs!nomer_serial
         qty = rs("Qty")
         conn.Execute "update serial_plat set status=0 where kode_plat='" & KodePlat & "' and nomer_serial='" & rs!nomer_serial & "'"
         'Kurangi stock plat
         conn.Execute "update stock_plat set stock=stock - " & Replace(qty, ",", ".") & " where kode_plat='" & KodePlat & "' "
        
    End If
    rs.Close
    
    
    rs.Open "Select kode_acc from ms_plat " & _
            "where kode_plat='" & KodePlat & "' ", conn
    If Not rs.EOF Then
        Acc_Persediaan = rs("kode_acc")
    End If
    rs.Close
    hpp = GetHPPPlatVia(KodePlat, conn)
    totalHpp = hpp * qty
    
    NoJurnal = Nomor_Jurnal2(tanggal)
    
    
    JurnalD NoTransaksi, NoJurnal, var_accbiayalain, "D", totalHpp, conn, "Rusak", KodePlat, Serial, , 1
    JurnalD NoTransaksi, NoJurnal, Acc_Persediaan, "K", totalHpp, conn, "Rusak", KodePlat, Serial, , 2
    
    JurnalH NoTransaksi, NoJurnal, tanggal, "Kerusakan Plat", conn
    
    
    conn.Execute "update t_plat_rusak set status_posting=1 where nomer_transaksi='" & NoTransaksi & "'"
    conn.CommitTrans
i = 0
    MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    Debug.Print NoTransaksi & " " & err.Description
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub

Private Sub JurnalH(NoTransaksi As String, NoJurnal As String, tanggal As Date, keterangan As String, conn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "tipe"
    fields(5) = "userid"

    nilai(0) = NoTransaksi
    nilai(1) = NoJurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
    nilai(4) = Left(NoTransaksi, 2)
    nilai(5) = User

    conn.Execute tambah_data2(table_name, fields, nilai)

End Sub

Private Sub JurnalD(NoTransaksi As String, NoJurnal As String, kodeAcc As String, Posisi As String, nilaiJurnal As Double, conn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String, Optional KeteranganNilai As String, Optional NomorUrut As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(9)
    ReDim nilai(9)
        
    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    If UCase(Posisi) = "D" Then
        fields(2) = "debet"
    Else
        fields(2) = "kredit"
    End If
    fields(3) = "no_jurnal"
    fields(4) = "keterangan"
    fields(5) = "kode1"
    fields(6) = "kode2"
    fields(7) = "nilai"
    fields(8) = "no_urut"
    
    nilai(0) = NoTransaksi
    nilai(1) = kodeAcc
    nilai(2) = Replace(nilaiJurnal, ",", ".")
    nilai(3) = NoJurnal
    nilai(4) = keterangan
    nilai(5) = kode1
    nilai(6) = kode2
    nilai(7) = Replace(KeteranganNilai, ",", ".")
    nilai(8) = NomorUrut
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Public Sub posting_campurTinta(NoTransaksi As String, pesan As Boolean)
'On Error GoTo err
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim i As Byte, hpp As Double
i = 0
conn.Open strcon
conn.BeginTrans
i = 1
    rs.Open "select * from t_campuranTintah where nomer_campur='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
    'menambah stock tinta yang d campur
        hpp = GetHPPTinta(rs!kode_tinta, conn)
        insertkartustockTintaGlobal NoTransaksi, rs!kode_tinta, rs!qty, rs!tanggal_campur, "HslCampurTinta", rs!kode_gudang, hpp, conn
'        conn.Execute "update stock_tinta set stock=stock+'" & Replace(rs!qty, ",", ".") & "' where kode_tinta='" & rs!kode_tinta & "' and kode_gudang='" & rs!kode_gudang & "' "
        rs.Close
    'mengurangi bahan tinta
        rs.Open "select a.kode_tinta,a.qty,b.kode_gudang,b.tanggal_campur from t_campuranTintad a inner join t_campuranTintaH b on a.nomer_campur=b.nomer_campur  where b.nomer_campur='" & NoTransaksi & "'", conn
        While Not rs.EOF
            hpp = GetHPPTinta(rs!kode_tinta, conn)
            insertkartustockTintaGlobal NoTransaksi, rs!kode_tinta, rs!qty * -1, rs!tanggal_campur, "BhnCampurTinta", rs!kode_gudang, hpp, conn
'            conn.Execute "update stock_tinta set stock=stock-'" & Replace(rs!qty, ",", ".") & "' where kode_tinta='" & rs!kode_tinta & "' and kode_gudang='" & rs!kode_gudang & "' "
            rs.MoveNext
        Wend
    End If
    rs.Close
        
    
    conn.Execute "update t_campuranTintah set status_posting=1 where nomer_campur='" & NoTransaksi & "'"
conn.CommitTrans
i = 0
 If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    Debug.Print NoTransaksi & " " & err.Description
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub

'Public Sub insertkartustockGlobal(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, gudang As String, nomer_serial As String, conn As ADODB.Connection)
'Dim rs As New ADODB.Recordset
'Dim stockawal As Double
'Dim masuk, keluar As Double
'
'rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc , nomer_urut desc", conn
'If Not rs.EOF Then
'stockawal = rs(0)
'Else
'stockawal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
'End If
'rs.Close
'masuk = 0
'keluar = 0
'If qty > 0 Then masuk = qty
'If qty < 0 Then keluar = qty * -1
'
'conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(stockawal, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
'
'
'conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kode_bahan & "'and nomer_serial='" & nomer_serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kode_bahan & "'," & Replace(qty, ",", ".") & ",'" & serial & "','" & gudang & "') else " & _
'     "update stock set stock=stock+" & Replace(qty, ",", ".") & " where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "'"
'
''insert bahan
'conn.Execute "if not exists (select kode_bahan from ms_bahan where kode_bahan='" & kode_bahan & "') insert into ms_bahan (kode_bahan) values ('" & kode_bahan & "') "
'
'
'End Sub
'Public Sub insertkartustockOpname(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, selisih As Double, gudang As String, nomer_serial As String, conn As ADODB.Connection)
'Dim rs As New ADODB.Recordset
'Dim stockawal As Double
'Dim masuk, keluar As Double
'
'rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc , nomer_urut desc", conn
'If Not rs.EOF Then
'stockawal = rs(0)
'Else
'stockawal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
'End If
'rs.Close
'masuk = 0
'keluar = 0
'If selisih > 0 Then masuk = selisih
'If selisih < 0 Then keluar = selisih * -1
'
'conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(stockawal, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
'
'
'conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kode_bahan & "'and nomer_serial='" & nomer_serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kode_bahan & "'," & Replace(qty, ",", ".") & ",'" & serial & "','" & gudang & "') else " & _
'     "update stock set stock=" & Replace(qty, ",", ".") & " where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "'"
'
''insert bahan
'conn.Execute "if not exists (select kode_bahan from ms_bahan where kode_bahan='" & kode_bahan & "') insert into ms_bahan (kode_bahan) values ('" & kode_bahan & "') "
'
'
'End Sub

Public Sub insertkartustockGlobal(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, gudang As String, nomer_serial As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double

rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "'  order by nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
End If
rs.Close
masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1

conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"


conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kode_bahan & "'and nomer_serial='" & nomer_serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kode_bahan & "'," & Replace(qty, ",", ".") & ",'" & nomer_serial & "','" & gudang & "') else " & _
     "update stock set stock=stock+" & Replace(qty, ",", ".") & " where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "'"

'insert bahan
conn.Execute "if not exists (select kode_bahan from ms_bahan where kode_bahan='" & kode_bahan & "') insert into ms_bahan (kode_bahan) values ('" & kode_bahan & "') "


End Sub
Public Sub insertkartustockOpname(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, selisih As Double, gudang As String, nomer_serial As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double

rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "'  order by  nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
End If
rs.Close
masuk = 0
keluar = 0
If selisih > 0 Then masuk = selisih
If selisih < 0 Then keluar = selisih * -1

conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"


conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kode_bahan & "'and nomer_serial='" & nomer_serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & kode_bahan & "'," & Replace(qty, ",", ".") & ",'" & nomer_serial & "','" & gudang & "') else " & _
     "update stock set stock=" & Replace(qty, ",", ".") & " where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "'"

'insert bahan
conn.Execute "if not exists (select kode_bahan from ms_bahan where kode_bahan='" & kode_bahan & "') insert into ms_bahan (kode_bahan) values ('" & kode_bahan & "') "


End Sub
Public Function getstockKartu(kode_bahan As String, gudang As String, nomer_serial As String, conn As ADODB.Connection) As Double
Dim rs As New ADODB.Recordset
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "'  order by  nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
End If
rs.Close
getstockKartu = StockAwal
End Function



Public Sub insertkartustock(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, berat As Double, gudang As String, nomer_serial As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim qtyawal As Double
Dim masuk, keluar As Double
Dim qtymasuk, qtykeluar As Double

rs.Open "select top 1 stockawal+masuk-keluar,qtyawal+qtymasuk-qtykeluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "' order by nomer_urut desc", conn, adOpenForwardOnly
If Not rs.EOF Then
StockAwal = rs(0)
qtyawal = rs(1)
Else
StockAwal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
qtyawal = getQty(kode_bahan, gudang, nomer_serial, conn)
End If
rs.Close
masuk = 0
keluar = 0
qtymasuk = 0
qtykeluar = 0
If berat > 0 Then masuk = berat
If berat < 0 Then keluar = berat * -1
If qty > 0 Then qtymasuk = qty
If qty < 0 Then qtykeluar = qty * -1
'Debug.Print kode_bahan & "=" & stockawal & " ->" & qty
conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,qtyawal,qtymasuk,qtykeluar,hpp) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(qtyawal, ",", ".") & "," & Replace(qtymasuk, ",", ".") & "," & Replace(qtykeluar, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
End Sub
Public Sub insertkartustockcustomer(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, berat As Double, toko As String, nomer_serial As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double

rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustocktoko where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_toko='" & gudang & "' order by nomer_urut desc", conn, adOpenForwardOnly
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockToko(kode_bahan, toko, nomer_serial, conn)
End If
rs.Close
masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1
'Debug.Print kode_bahan & "=" & stockawal & " ->" & qty
conn.Execute "insert into tmp_kartustocktoko (kode_bahan,nomer_serial,tipe,kode_toko,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & toko & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
End Sub


'Public Sub insertkartustock(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, gudang As String, nomer_serial As String, conn As ADODB.Connection)
'Dim rs As New ADODB.Recordset
'Dim stockawal As Double
'Dim masuk, keluar As Double
'
'rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc , nomer_urut desc", conn
'If Not rs.EOF Then
'stockawal = rs(0)
'Else
'stockawal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
'End If
'rs.Close
'masuk = 0
'keluar = 0
'If qty > 0 Then masuk = qty
'If qty < 0 Then keluar = qty * -1
'
'conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(stockawal, ",", ".") & ")"
'conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
'
'End Sub



Public Sub generatekartustockKertas(tanggal1 As Date, tanggal2 As Date, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double
    conn.Execute "delete from tmp_kartustock where tanggal >='" & Format(tanggal1, "yyyy/MM/dd HH:mm:ss") & "' and tanggal <= '" & Format(tanggal2, "yyyy/MM/dd HH:mm:ss") & "'"
    conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal) " & _
    "select rpt_ms_bahan.kode_bahan,stock.nomer_serial,stock.kode_gudang,(select) " & _
    " from "
        
   conn.Execute "insert into tmp_stockharian (tahun,bulan,tanggal,[kode],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "','" & Format(tanggal, "MM") & "','" & Format(tanggal, "dd") & "',ms_bahan.[kode],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' and [kode]=ms_bahan.[kode])),0," & _
    "(select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' and [kode]=ms_bahan.[kode])) as stockawal from ms_bahan"

    conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & ")"
    conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
End Sub


Public Sub deletekartustock(Tipe As String, No As String, conn As ADODB.Connection)

Dim tanggal As Date
Dim masuk, keluar As Double
Dim qtymasuk, qtykeluar As Double
Dim kode_bahan As String

rs.Open "select * from  tmp_kartustock where id='" & No & "'", conn, adOpenKeyset
While Not rs.EOF
    tanggal = rs!tanggal
    masuk = rs!masuk
    keluar = rs!keluar
    qtymasuk = rs!qtymasuk
    qtykeluar = rs!qtykeluar
    kode_bahan = rs!kode_bahan
    Serial = rs!nomer_serial
    gudang = rs!kode_gudang
    conn.Execute "update tmp_kartustock set stockawal=(stockawal-" & Replace(masuk, ",", ".") & "+" & Replace(keluar, ",", ".") & "), " & _
        " qtyawal=(qtyawal-" & Replace(qtymasuk, ",", ".") & "+" & Replace(qtykeluar, ",", ".") & ") " & _
            "where kode_bahan='" & kode_bahan & "' and kode_gudang='" & gudang & "'and nomer_serial='" & Serial & "' and ((tanggal >='" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and id>'" & No & "') or tanggal >='" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "')"
    
    rs.MoveNext
    DoEvents
Wend
rs.Close
conn.Execute "delete from tmp_kartustock where id='" & No & "' "

End Sub
Public Sub insertkartustockTinta(NoTrans As String, kode_tinta As String, qty As Double, tanggal As Date, Tipe As String, gudang As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustockTinta where kode_tinta='" & kode_tinta & "' and kode_gudang='" & gudang & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc, nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockTinta(kode_tinta, gudang, conn)
End If
rs.Close
masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1

conn.Execute "insert into tmp_kartustockTinta (kode_tinta,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_tinta & "','" & Tipe & "','" & gudang & "','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
conn.Execute "update tmp_kartustockTinta set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_tinta='" & kode_tinta & "'  and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"

End Sub
Public Sub insertkartustockTintaGlobal(NoTrans As String, kode_tinta As String, qty As Double, tanggal As Date, Tipe As String, gudang As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk As Double, keluar As Double
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustockTinta where kode_tinta='" & kode_tinta & "' and kode_gudang='" & gudang & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc,nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockTinta(kode_tinta, gudang, conn)
End If
rs.Close
masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1

conn.Execute "insert into tmp_kartustockTinta (kode_tinta,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_tinta & "','" & Tipe & "','" & gudang & "','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
conn.Execute "update tmp_kartustockTinta set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_tinta='" & kode_tinta & "'  and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"

conn.Execute "if not exists (select kode_tinta from stock_tinta where kode_tinta='" & kode_tinta & "'  and kode_gudang='" & gudang & "') insert into stock_tinta (kode_tinta,stock,kode_gudang) values ('" & kode_tinta & "'," & Replace(qty, ",", ".") & ",'" & gudang & "') else " & _
     "update stock_tinta set stock=stock + " & Replace(qty, ",", ".") & " where kode_gudang='" & gudang & "' and kode_tinta='" & kode_tinta & "'"
'insert tinta
conn.Execute "if not exists (select kode_tinta from ms_tinta where kode_tinta='" & kode_tinta & "') insert into ms_tinta (kode_tinta) values ('" & kode_tinta & "') "

End Sub
Public Sub insertkartustockTintaOpname(NoTrans As String, kode_tinta As String, qty As Double, selisih As Double, tanggal As Date, Tipe As String, gudang As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustockTinta where kode_tinta='" & kode_tinta & "' and kode_gudang='" & gudang & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc,nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockTinta(kode_tinta, gudang, conn)
End If
rs.Close
masuk = 0
keluar = 0
If selisih > 0 Then masuk = selisih
If selisih < 0 Then keluar = selisih * -1

conn.Execute "insert into tmp_kartustockTinta (kode_tinta,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_tinta & "','" & Tipe & "','" & gudang & "','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(hpp, ",", ".") & ")"
conn.Execute "update tmp_kartustockTinta set stockawal=(stockawal+" & Replace(masuk, ",", ".") & "-" & Replace(keluar, ",", ".") & ") where kode_tinta='" & kode_tinta & "'  and kode_gudang='" & gudang & "' and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"

conn.Execute "if not exists (select kode_tinta from stock_tinta where kode_tinta='" & kode_tinta & "'  and kode_gudang='" & gudang & "') insert into stock_tinta (kode_tinta,stock,kode_gudang) values ('" & kode_tinta & "'," & Replace(qty, ",", ".") & ",'" & gudang & "') else " & _
     "update stock_tinta set stock=" & Replace(qty, ",", ".") & " where kode_gudang='" & gudang & "' and kode_tinta='" & kode_tinta & "'"
'insert tinta
conn.Execute "if not exists (select kode_tinta from ms_tinta where kode_tinta='" & kode_tinta & "') insert into ms_tinta (kode_tinta) values ('" & kode_tinta & "') "

End Sub
Public Sub deletekartustockTinta(Tipe As String, No As String, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim tanggal As Date
Dim masuk, keluar As Double
Dim kode_bahan As String

rs.Open "select * from  tmp_kartustockTinta where id='" & No & "' and tipe='" & Tipe & "'", conn
While Not rs.EOF
    tanggal = rs!tanggal
    masuk = rs!masuk
    keluar = rs!keluar
    kode_tinta = rs!kode_tinta
    gudang = rs!gudang
    conn.Execute "update tmp_kartustockTinta set stockawal=(stockawal-" & masuk & "+" & keluar & ") where kode_tinta='" & kode_tinta & "' and gudang='" & gudang & "' and tanggal>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
    
    rs.MoveNext
Wend
rs.Close
conn.Execute "delete from tmp_kartustockTinta where id='" & No & "' and tipe='" & Tipe & "' "

End Sub

Public Sub insertkartustockPlat(NoTrans As String, kode_plat As String, qty As Long, tanggal As Date, Tipe As String, gudang As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustockPlat where kode_plat='" & kode_plat & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc,nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockPlat(kode_plat, conn)
End If
rs.Close
masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1

conn.Execute "insert into tmp_kartustockPlat (kode_plat,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_plat & "','" & Tipe & "','" & gudang & " ','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & masuk & "," & keluar & "," & StockAwal & "," & Replace(hpp, ",", ".") & ")"
conn.Execute "update tmp_kartustockPlat set stockawal=(stockawal+" & masuk & "-" & keluar & ") where kode_plat='" & kode_plat & "' and kode_gudang='" & gudang & "'  and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"

End Sub
Public Sub insertkartustockPlatGlobal(NoTrans As String, kode_plat As String, qty As Long, tanggal As Date, Tipe As String, gudang As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Double
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustockPlat where kode_plat='" & kode_plat & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc,nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockPlat(kode_plat, conn)
End If
rs.Close
masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1

conn.Execute "insert into tmp_kartustockPlat (kode_plat,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_plat & "','" & Tipe & "','" & gudang & " ','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & masuk & "," & keluar & "," & StockAwal & "," & Replace(hpp, ",", ".") & ")"
conn.Execute "update tmp_kartustockPlat set stockawal=(stockawal+" & masuk & "-" & keluar & ") where kode_plat='" & kode_plat & "' and kode_gudang='" & gudang & "'  and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"

conn.Execute "if exists(select * from stock_plat where kode_plat='" & kode_plat & "') " & _
        "update stock_plat set stock=stock+'" & Replace(Format(qty, "###0.##"), ",", ".") & "' where kode_plat='" & kode_plat & "'  else " & _
        "insert into stock_plat (kode_plat,stock) values ('" & kode_plat & "','" & qty & "')"

End Sub

Public Sub insertkartustockPlatOpname(NoTrans As String, kode_plat As String, qty As Long, selisih As Long, tanggal As Date, Tipe As String, gudang As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim masuk, keluar As Long
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustockPlat where kode_plat='" & kode_plat & "' and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by tanggal desc,id desc,nomer_urut desc", conn
If Not rs.EOF Then
StockAwal = rs(0)
Else
StockAwal = getStockPlat(kode_plat, conn)
End If
rs.Close
masuk = 0
keluar = 0
If selisih > 0 Then masuk = selisih
If selisih < 0 Then keluar = selisih * -1

conn.Execute "insert into tmp_kartustockPlat (kode_plat,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,hpp) values ('" & kode_plat & "','" & Tipe & "','" & gudang & " ','" & NoTrans & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & masuk & "," & keluar & "," & StockAwal & "," & Replace(hpp, ",", ".") & ")"
conn.Execute "update tmp_kartustockPlat set stockawal=(stockawal+" & masuk & "-" & keluar & ") where kode_plat='" & kode_plat & "' and kode_gudang='" & gudang & "'  and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"

conn.Execute "if not exists (select kode_plat from stock_plat where kode_plat='" & kode_plat & "' ) insert into stock_plat (kode_plat,stock) values ('" & kode_plat & "'," & Replace(qty, ",", ".") & ") else " & _
    "update stock_plat set stock=" & Replace(qty, ",", ".") & " where kode_plat='" & kode_plat & "'"
'insert bahan
    conn.Execute "if not exists (select kode_plat from ms_plat where kode_plat='" & kode_plat & "') insert into ms_plat (kode_plat) values ('" & kode_plat & "') "
    conn.Execute "if not exists (select kode_plat from hpp_plat where kode_plat='" & kode_plat & "') insert into hpp_plat (kode_plat,hpp) values ('" & kode_plat & "',0) "

End Sub



Public Sub deletekartustockPlat(Tipe As String, No As String, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim tanggal As Date
Dim masuk, keluar As Double
Dim kode_bahan As String

rs.Open "select * from  tmp_kartustockPlat where id='" & No & "' and tipe='" & Tipe & "'", conn
While Not rs.EOF
    tanggal = rs!tanggal
    masuk = rs!masuk
    keluar = rs!keluar
    kode_plat = rs!kode_plat
    gudang = rs!gudang
    conn.Execute "update tmp_kartustockPlat set stockawal=(stockawal-" & masuk & "+" & keluar & ") where kode_plat='" & kode_plat & "' and gudang='" & gudang & "' and tanggal>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
    
    rs.MoveNext
Wend
rs.Close
conn.Execute "delete from tmp_kartustockPlat where id='" & No & "' and tipe='" & Tipe & "' "

End Sub



Public Sub insertkartuHutang(Tipe As String, No As String, tanggal As Date, kode_supplier As String, Nominal As Currency, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim HutangAwal As Currency
Dim debet, kredit As Currency
rs.Open "select top 1 saldoawal-debet+kredit from tmp_kartuHutang where kode_supplier='" & kode_supplier & "'  and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by no_urut desc", conn
If Not rs.EOF Then
    HutangAwal = rs(0)
Else
    HutangAwal = GetTotalHutang(kode_supplier, conn)
End If
rs.Close
debet = 0
kredit = 0
If Nominal > 0 Then debet = Nominal
If Nominal < 0 Then kredit = Nominal * -1

conn.Execute "insert into tmp_kartuHutang (kode_supplier,tipe,id,tanggal,debet,kredit,saldoawal) values ('" & kode_supplier & "','" & Tipe & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(debet, ",", ".") & "," & Replace(kredit, ",", ".") & "," & Replace(HutangAwal, ",", ".") & ")"
conn.Execute "update tmp_kartuHutang set saldoawal=(saldoawal-" & Replace(debet, ",", ".") & "+" & Replace(kredit, ",", ".") & ") where kode_supplier='" & kode_supplier & "'  and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
End Sub


Public Function GetTotalHutang(ByVal kode_supplier As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select isnull((sum(hutang)-sum(total_bayar)),0) as hutang from list_hutang where kode_supplier='" & kode_supplier & "'", cn
If Not rs.EOF Then
    GetTotalHutang = rs(0)
Else
    GetTotalHutang = 0
End If
rs.Close

End Function
Public Sub insertkartuPiutang(Tipe As String, No As String, tanggal As Date, kode_customer As String, Nominal As Currency, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim piutangAwal As Currency
Dim debet, kredit As Currency
rs.Open "select top 1 saldoawal+debet-kredit from tmp_kartuPiutang where kode_customer='" & kode_customer & "'  and  tanggal <'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' order by no_urut desc", conn
If Not rs.EOF Then
    piutangAwal = rs(0)
Else
    piutangAwal = GetTotalPiutang(kode_customer, conn)
End If
rs.Close
debet = 0
kredit = 0
If Nominal > 0 Then debet = Nominal
If Nominal < 0 Then kredit = Nominal * -1

conn.Execute "insert into tmp_kartuPiutang (kode_customer,tipe,id,tanggal,debet,kredit,saldoawal) values ('" & kode_customer & "','" & Tipe & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & debet & "," & kredit & "," & piutangAwal & ")"
conn.Execute "update tmp_kartuPiutang set saldoawal=(saldoawal-" & debet & "+" & kredit & ") where kode_customer='" & kode_customer & "'  and tanggal >'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'"
End Sub
Public Function GetTotalPiutang(ByVal kode_customer As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select isnull((sum(piutang)-sum(total_bayar)),0) as piutang from list_piutang where kode_customer='" & kode_customer & "'", cn
If Not rs.EOF Then
    GetTotalPiutang = rs(0)
Else
    GetTotalPiutang = 0
End If
rs.Close

End Function

Public Sub postingTrukBerangkat(NoTransaksi As String, pesan As Boolean)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim Acc_biaya As String
Dim biaya As Double, nopol As String
Dim UangSaku As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
'On Error GoTo err
conn.Open strcon

conn.BeginTrans
    i = 1
    
   rs.Open "select * from t_trukAngkut  where nomer_truk_keluar='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
        tanggal1 = rs!tanggal_keluar
        NoJurnal = Nomor_Jurnal2(tanggal1)
        UangSaku = rs!uang_saku
        nopol = rs!nopol
'        biaya = rs!biaya_solar + rs!biaya_makan + rs!biaya_timbangan + rs!biaya_forklift + rs!biaya_jalan + rs!biaya_tol + rs!biaya_ijin + rs!biaya_kuli
    End If
    rs.Close
    
    
    rs.Open "select * from ms_truk where nopol='" & nopol & "'", conn
    If Not rs.EOF Then
        Acc_biaya = rs!kode_acc
    End If
    rs.Close
    add_jurnal NoTransaksi, NoJurnal, tanggal1, "Pengiriman", conn
        JurnalD NoTransaksi, NoJurnal, Acc_biaya, "D", UangSaku, conn, "Biaya Pengiriman", nopol, , , 1
        JurnalD NoTransaksi, NoJurnal, var_acckas, "K", UangSaku, conn, "Biaya Pengiriman", nopol, , , 2
   
    conn.Execute "update t_trukAngkut set status_posting=1 where nomer_truk_keluar='" & NoTransaksi & "'"

conn.CommitTrans
conn.Close
i = 0

If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description
End Sub


Public Sub postingTrukKembali(NoTransaksi As String, pesan As Boolean)
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim tanggal1 As Date
Dim Acc_biaya As String
Dim biaya As Double, nopol As String
Dim SisaUangSaku As Double
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
'On Error GoTo err
conn.Open strcon

conn.BeginTrans
    i = 1
    
   rs.Open "select * from t_trukAngkut  where nomer_truk_keluar='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
   If Not rs.EOF Then
        tanggal1 = rs!tanggal_kembali
        NoJurnal = Nomor_Jurnal2(tanggal1)
        SisaUangSaku = rs!sisa_uang_saku
        nopol = rs!nopol
'        biaya = rs!biaya_solar + rs!biaya_makan + rs!biaya_timbangan + rs!biaya_forklift + rs!biaya_jalan + rs!biaya_tol + rs!biaya_ijin + rs!biaya_kuli
    End If
    rs.Close
    
    If SisaUangSaku <> 0 Then
    
        rs.Open "select * from ms_truk where nopol='" & nopol & "'", conn
        If Not rs.EOF Then
            Acc_biaya = rs!kode_acc
        End If
        rs.Close
        add_jurnal NoTransaksi, NoJurnal, tanggal1, "Pengiriman", conn
        If SisaUangSaku > 0 Then
            JurnalD NoTransaksi, NoJurnal, var_acckas, "D", SisaUangSaku, conn, "Sisa Uang Saku", nopol, , , 1
            JurnalD NoTransaksi, NoJurnal, Acc_biaya, "K", SisaUangSaku, conn, "Sisa Uang Saku", nopol, , , 2
        Else
            JurnalD NoTransaksi, NoJurnal, Acc_biaya, "D", -SisaUangSaku, conn, "Kekurangan Uang Saku", nopol, , , 1
            JurnalD NoTransaksi, NoJurnal, var_acckas, "K", -SisaUangSaku, conn, "Kekurangan Uang Saku", nopol, , , 2
        End If
    
    End If
    conn.Execute "update t_trukAngkut set status_kembali=1 where nomer_truk_keluar='" & NoTransaksi & "'"

conn.CommitTrans
conn.Close
i = 0

If pesan = True Then MsgBox "Proses Posting telah berhasil"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description
End Sub



