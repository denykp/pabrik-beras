VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddQCPenerimaanbarang2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Komposisi Barang datang"
   ClientHeight    =   9405
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   11565
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9405
   ScaleWidth      =   11565
   Begin VB.TextBox txtNoTruk 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4950
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   1395
      Width           =   2205
   End
   Begin VB.TextBox txtNoSJ 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   1395
      Width           =   2205
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3105
      Picture         =   "frmAddQCPenerimaanBarang2.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   8640
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4500
      Picture         =   "frmAddQCPenerimaanBarang2.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   8640
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   1785
      Width           =   8250
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3708
      Picture         =   "frmAddQCPenerimaanBarang2.frx":0204
      TabIndex        =   6
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9270
      TabIndex        =   5
      Top             =   165
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10170
      TabIndex        =   4
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   1005
      Width           =   1530
   End
   Begin VB.TextBox txtNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   585
      Width           =   1875
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7020
      TabIndex        =   7
      Top             =   555
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   93913091
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   5490
      Left            =   450
      TabIndex        =   21
      Top             =   2430
      Width           =   9960
      _ExtentX        =   17568
      _ExtentY        =   9684
      _Version        =   393216
      Cols            =   9
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label5 
      Caption         =   "No. Truk"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3915
      TabIndex        =   20
      Top             =   1455
      Width           =   960
   End
   Begin VB.Label Label1 
      Caption         =   "No SJ"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   18
      Top             =   1440
      Width           =   960
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   150
      TabIndex        =   16
      Top             =   1335
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   14
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   13
      Top             =   1785
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5535
      TabIndex        =   12
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label7 
      Caption         =   "No.Order"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   11
      Top             =   600
      Width           =   1275
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   10
      Top             =   1050
      Width           =   960
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3240
      TabIndex        =   9
      Top             =   990
      Width           =   3120
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   8
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddQCPenerimaanbarang2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte
Private Sub chkNumbering_Click()
    If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub


Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\Transferpembelian.rpt"
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'        .SelectionFormula = " {t_terimabarangh.ID}='" & lblNoTrans & "'"
'        .Destination = crptToWindow
'        .ParameterFields(0) = "login;" + user + ";True"
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
'        .action = 1
'    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    simpan
'    If posting_terimabarang(lblNoTrans) Then
'        MsgBox "Proses Posting telah berhasil"
'        reset_form
'    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchTerimaBarang
    frmSearch.nmform = "frmAddPenerimaanBarang"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "PO"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
 
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    
    add_dataheader
'    dbgrid.MoveFirst
'    While counter - 1 < dbgrid.Rows
'        conn.Execute "update t_terimabarangd set qc_fisik='" & dbgrid.Columns(4).text & "' where nomer_terimabarang='" & lblNoTrans & "' and kode_bahan='" & dbgrid.Columns(0).text & "'"
'        counter = counter + 1
'        dbgrid.MoveNext
'    Wend
    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtNoOrder.text = ""
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    txtNoSJ.text = ""
    txtNoTruk.text = ""
    txtNamaQCFisik = ""
    txtNamaQCLab = ""
    txtQCFisik = ""
    txtQCLab = ""
    txtQCKeterangan = ""
    status_posting = False
    DBGrid.RemoveAll
    
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_terimabarangh"
    fields(0) = "qc_fisik_nama"
    fields(1) = "qc_fisik"
    fields(2) = "qc_lab_nama"
    fields(3) = "qc_lab"
    fields(4) = "qc_keterangan"
    
    nilai(0) = txtNamaQCFisik
    nilai(1) = txtQCFisik
    nilai(2) = txtNamaQCLab
    nilai(3) = txtQCLab
    nilai(4) = txtQCKeterangan
    
    update_data table_name, fields, nilai, "nomer_terimabarang='" & lblNoTrans & "'", conn
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_terimabarangh where nomer_terimabarang='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        
        DTPicker1 = rs("tanggal_terimabarang")
        
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        txtNoOrder.text = rs("nomer_PO")
        txtNoSJ = rs!nomer_suratjalan
        txtNoTruk = rs!no_truk
        
        totalQty = 0
        total = 0
'        cmdPrint.Visible = True
        
        If rs.State Then rs.Close
        row = 1
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        rs.Open "select d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat from t_terimabarangd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_terimabarang='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
                flxGrid.TextMatrix(row, 5) = rs(3)
                flxGrid.TextMatrix(row, 6) = rs(4)
                'flxGrid.TextMatrix(row, 7) = Format(rs(4), "#,##0")
                flxGrid.TextMatrix(row, 8) = Format(rs(3) * rs(4), "#,##0")
                row = row + 1
                totalQty = totalQty + (rs(3) * rs(4))
        
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_noPO()
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_poh where nomer_po='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        txtKodeSupplier.text = rs("kode_supplier")
        cek_supplier
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        txtKdBrg.Clear
        rs.Open "select d.[kode_bahan] from t_POd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_PO='" & txtNoOrder & "' order by no_urut", conn
        While Not rs.EOF
            txtKdBrg.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub loaddetil()
    With frmKomposisiBarang
        .transaksi = "terimabarang"
        .lblNoTrans = lblNoTrans
        .lblNoSerial = flxGrid.TextMatrix(flxGrid.row, 3)
        .lblKodeBarang = flxGrid.TextMatrix(flxGrid.row, 1)
        .lblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
        .urut = flxGrid.row - 1
        .loadkomposisi
        .Show vbModal
    End With
    
End Sub

Private Sub Command1_Click()

End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
    End If

End Sub
Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub
Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        
        KeyAscii = 0
        Call MySendKeys("{tab}")
        
    End If
End Sub

Private Sub Form_Load()
    load_combo
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    seltab = 0
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1400
    flxGrid.ColWidth(2) = 3000
    flxGrid.ColWidth(3) = 1500
    flxGrid.ColWidth(4) = 0
    flxGrid.ColWidth(5) = 900
    flxGrid.ColWidth(6) = 900
    flxGrid.ColWidth(7) = 0
    flxGrid.ColWidth(8) = 0
    
    
    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Serial"
    flxGrid.TextMatrix(0, 4) = ""
    flxGrid.TextMatrix(0, 5) = "Qty"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "Harga"
    flxGrid.TextMatrix(0, 8) = "Total"
    
End Sub
Private Sub load_combo()
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan

        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""

        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier],kategori_harga from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
        lblKategori = rs(1)
        
    Else
        lblNamaSupplier = ""
        lblKategori = ""
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()

End Sub

Private Sub listKuli_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listKuli.RemoveItem (listKuli.ListIndex)
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub TabStrip1_Click()
    If TabStrip1.SelectedItem.Index = 1 Then
        DBGrid.Visible = True
        frDetail.Visible = False
    Else
        DBGrid.Visible = False
        frDetail.Visible = True
    End If
    
End Sub
