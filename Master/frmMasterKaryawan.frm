VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMasterKaryawan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Karyawan"
   ClientHeight    =   6525
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   13020
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6525
   ScaleWidth      =   13020
   Begin VB.TextBox txtFile 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7875
      TabIndex        =   33
      Top             =   1350
      Width           =   2280
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "Browse"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10215
      Picture         =   "frmMasterKaryawan.frx":0000
      TabIndex        =   32
      Top             =   1350
      Width           =   915
   End
   Begin VB.CommandButton cmdLoadRpt 
      Caption         =   "Load History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4500
      Picture         =   "frmMasterKaryawan.frx":0102
      TabIndex        =   31
      Top             =   135
      Width           =   1320
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   1890
      TabIndex        =   6
      Text            =   "0"
      Top             =   4905
      Width           =   1770
   End
   Begin VB.OptionButton optSex2 
      Caption         =   "Perempuan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3375
      TabIndex        =   28
      Top             =   945
      Width           =   1860
   End
   Begin VB.OptionButton optSex1 
      Caption         =   "Laki-laki"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1935
      TabIndex        =   27
      Top             =   945
      Value           =   -1  'True
      Width           =   1230
   End
   Begin VB.ComboBox cmbStatus 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterKaryawan.frx":0204
      Left            =   1890
      List            =   "frmMasterKaryawan.frx":0206
      TabIndex        =   5
      Top             =   4500
      Width           =   3885
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1545
      Index           =   4
      Left            =   1890
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   2880
      Width           =   5730
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4020
      Picture         =   "frmMasterKaryawan.frx":0208
      TabIndex        =   9
      Top             =   5970
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterKaryawan.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   135
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   1890
      TabIndex        =   3
      Top             =   2490
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   2
      Left            =   1890
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1350
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      MaxLength       =   50
      TabIndex        =   1
      Top             =   540
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      MaxLength       =   25
      TabIndex        =   0
      Top             =   135
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5280
      Picture         =   "frmMasterKaryawan.frx":040C
      TabIndex        =   10
      Top             =   5970
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2760
      Picture         =   "frmMasterKaryawan.frx":050E
      TabIndex        =   8
      Top             =   5970
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1440
      Picture         =   "frmMasterKaryawan.frx":0610
      TabIndex        =   7
      Top             =   5970
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Image Image1 
      Height          =   2535
      Left            =   7875
      Stretch         =   -1  'True
      Top             =   1800
      Width           =   4470
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   30
      Top             =   4935
      Width           =   105
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Limit Kredit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   29
      Top             =   4935
      Width           =   1320
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   26
      Top             =   4530
      Width           =   1320
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   25
      Top             =   4530
      Width           =   105
   End
   Begin VB.Label Label27 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   24
      Top             =   2925
      Width           =   105
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   23
      Top             =   2925
      Width           =   1320
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   21
      Top             =   585
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   20
      Top             =   180
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   60
      TabIndex        =   19
      Top             =   5595
      Width           =   2130
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   18
      Top             =   2520
      Width           =   1320
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   17
      Top             =   2520
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   1395
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   15
      Top             =   1395
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   14
      Top             =   585
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   13
      Top             =   180
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Karyawan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   585
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "NIK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   135
      TabIndex        =   11
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterKaryawan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBrowse_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.jpg|*.jpg"
    CommonDialog1.filename = "Image Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        Set Image1.Picture = LoadPicture(CommonDialog1.filename)
        txtFile = CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open

    conn.BeginTrans
    i = 1
    conn.Execute "delete from ms_karyawan where NIK='" & txtField(0).text & "'"
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoadRpt_Click()
    frmSearch.query = searchRptKaryawan
    frmSearch.nmform = "frmMasterKaryawan"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_karyawan"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data_rpt"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = searchKaryawan
    frmSearch.nmform = "frmMasterKaryawan"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_karyawan"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub




Private Sub cmdSimpan_Click()
'Dim conn As New ADODB.Connection
Dim i, J As Byte
On Error GoTo err
    i = 0
    For J = 0 To 1
     If txtField(J).text = "" Then
         MsgBox "Semua field yang bertanda * harus diisi"
         txtField(J).SetFocus
         Exit Sub
     End If
    Next

    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    rs.Open " select * from ms_karyawan where NIK='" & txtField(0) & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_karyawan where NIK='" & txtField(0).text & "'"
        conn.Execute "delete from rpt_ms_karyawan where NIK='" & txtField(0).text & "'"
    End If
    rs.Close
    add_dataheader
    If txtFile <> "" Then
        rs.Open "select * from ms_karyawan where [nik]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        Dim S As New ADODB.Stream
        With S
            .Type = adTypeBinary
            .Open
            .LoadFromFile txtFile
            rs("foto").value = .Read
            rs.Update
        End With
        rs.Close
        rs.Open "select * from rpt_ms_karyawan where [nik]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        rs("foto").value = S.Read
        Set S = Nothing
    End If
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    load_combo
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(4).text = ""
    txtField(5).text = "0"
    cmbStatus.text = ""
    txtFile = ""
    Image1.Picture = Nothing
End Sub
Public Sub cari_data_rpt()
    cari_data "rpt_"
End Sub
Public Sub cari_data(Optional prefix As String)
On Error Resume Next
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select *  from " & prefix & "ms_karyawan  " & _
            "where NIK='" & txtField(0).text & "' ", conn
    
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        txtField(4).text = rs(4)
        txtField(5).text = rs!limitkredit
        cmbStatus.text = rs!Tipe
        If rs!sex = "L" Then optSex1.value = True Else optSex2.value = True
        Dim mStream As New ADODB.Stream
    
        Dim fs As New Scripting.FileSystemObject
        fs.DeleteFile App.Path & "\temp.jpg"
            
          With mStream
            .Type = adTypeBinary
            .Open
            .Write rs("foto").value
            .SaveToFile App.Path & "\temp.jpg"
            Image1.Picture = LoadPicture(App.Path & "\temp.jpg")
            txtFile = App.Path & "\temp.jpg"
          End With
        
          Set mStream = Nothing
        rs.Close
        
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    If rs.State Then rs.Close
    
    conn.Close
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    reset_form
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub
Private Sub load_combo()
    alterkaryawan
    conn.ConnectionString = strcon
    conn.Open
    
    cmbStatus.Clear
    rs.Open "select distinct tipe from ms_karyawan order by tipe", conn
    While Not rs.EOF
        cmbStatus.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(8)
    ReDim nilai(8)
    
    table_name = "ms_karyawan"
    fields(0) = "NIK"
    fields(1) = "nama_karyawan"
    fields(2) = "alamat"
    fields(3) = "telp"
    fields(4) = "sex"
    fields(5) = "keterangan"
    fields(6) = "tipe"
    fields(7) = "limitkredit"
    
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = txtField(2).text
    nilai(3) = txtField(3).text
    nilai(4) = IIf(optSex1.value = True, "L", "P")
    nilai(5) = txtField(4).text
    nilai(6) = cmbStatus.text
    nilai(7) = txtField(5).text
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    table_name = "rpt_ms_karyawan"
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
