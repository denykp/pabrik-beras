VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmTransJurnal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Jurnal Umum"
   ClientHeight    =   7770
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8940
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   8940
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   24
      Top             =   120
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5895
      TabIndex        =   23
      Top             =   675
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   120
      TabIndex        =   15
      Top             =   1365
      Width           =   6990
      Begin VB.TextBox txtDebet 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1260
         TabIndex        =   3
         Top             =   660
         Width           =   1665
      End
      Begin VB.CommandButton cmdSearchAcc 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2925
         TabIndex        =   16
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2730
         TabIndex        =   7
         Top             =   1485
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1605
         TabIndex        =   6
         Top             =   1485
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   5
         Top             =   1485
         Width           =   1350
      End
      Begin VB.TextBox txtKredit 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1260
         TabIndex        =   4
         Top             =   1065
         Width           =   1665
      End
      Begin VB.TextBox txtAcc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1260
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   255
         Width           =   1530
      End
      Begin VB.Label LblNamaAcc 
         BackColor       =   &H8000000C&
         Caption         =   "Nama Account"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   435
         Left            =   3375
         TabIndex        =   17
         Top             =   315
         Width           =   3525
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         Caption         =   "Debet"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   32
         Top             =   720
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4185
         TabIndex        =   21
         Top             =   1575
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   20
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Account"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   19
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   18
         Top             =   1125
         Width           =   600
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   540
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   6975
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   14
      Top             =   150
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      TabIndex        =   1
      Top             =   945
      Width           =   3885
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5325
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   6975
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2565
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   6975
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1605
      TabIndex        =   0
      Top             =   555
      Width           =   2550
      _ExtentX        =   4498
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   100007939
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2850
      Left            =   135
      TabIndex        =   22
      Top             =   3420
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   5
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8460
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   33
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5625
      TabIndex        =   31
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6975
      TabIndex        =   30
      Top             =   225
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7155
      TabIndex        =   29
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label16 
      Caption         =   "Debet"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2295
      TabIndex        =   28
      Top             =   6390
      Width           =   870
   End
   Begin VB.Label lblDebet 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3255
      TabIndex        =   27
      Top             =   6390
      Width           =   2010
   End
   Begin VB.Label lblKredit 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6705
      TabIndex        =   26
      Top             =   6390
      Width           =   2010
   End
   Begin VB.Label Label17 
      Caption         =   "Kredit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   25
      Top             =   6390
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   615
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   990
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   11
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransJurnal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryCOA As String = "select *   from ms_coa"
Const queryTransfer As String = "select no_transaksi,Tanggal,Keterangan from t_jurnalh where tipe='GL'"
Dim mode As Byte
Dim totalDebet As Double
Dim totalKredit As Double

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 no_transaksi from t_jurnalh where substring(no_transaksi,3,2)='" & Format(DTPicker1, "yy") & "' and substring(no_transaksi,5,2)='" & Format(DTPicker1, "MM") & "' and tipe='GL' order by no_transaksi desc", conn
    If Not rs.EOF Then
        No = "GL" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "GL" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub






Private Sub cmdClear_Click()
    txtAcc.text = ""
    debet = True
    LblNamaAcc = ""
    txtDebet.text = "0"
    txtKredit.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    Row = flxGrid.Row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalDebet = totalDebet - (flxGrid.TextMatrix(Row, 3))
    totalKredit = totalKredit - (flxGrid.TextMatrix(Row, 4))
    lblDebet = Format(totalDebet, "#,##0")
    lblKredit = Format(totalKredit, "#,##0")
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtAcc.text <> "" And LblNamaAcc <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtAcc.text) Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(Row, 1) = txtAcc.text
        flxGrid.TextMatrix(Row, 2) = LblNamaAcc
        
        If flxGrid.TextMatrix(Row, 3) <> "" Then totalDebet = totalDebet - (flxGrid.TextMatrix(Row, 3))
        If flxGrid.TextMatrix(Row, 4) <> "" Then totalKredit = totalKredit - (flxGrid.TextMatrix(Row, 4))
        flxGrid.TextMatrix(Row, 3) = txtDebet
        flxGrid.TextMatrix(Row, 4) = txtKredit
        totalDebet = totalDebet + (flxGrid.TextMatrix(Row, 3))
        totalKredit = totalKredit + (flxGrid.TextMatrix(Row, 4))
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = 4
        
        lblDebet = Format(totalDebet, "#,##0")
        lblKredit = Format(totalKredit, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtAcc.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = queryCOA
    frmSearch.nmform = "frmTransJurnal"
    frmSearch.nmctrl = "txtAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_Acc Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = queryTransfer
    frmSearch.nmform = "frmAddjurnal"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "jurnalH"
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim Row As Integer

On Error GoTo err
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan Item Jurnal terlebih dahulu"
        txtAcc.SetFocus
        Exit Sub
    End If
    If totalDebet <> totalKredit Then
        If MsgBox("Debet dan Kredit tidak seimbang, yakin hendak menyimpan?", vbYesNo) = vbNo Then
            txtAcc.SetFocus
            Exit Sub
        End If
    End If
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        nomor_baru
        NoJurnal = Nomor_Jurnal(DTPicker1)
    Else
        
            rs.Open "select no_jurnal from t_jurnalh where no_transaksi='" & lblNoTrans & "'", conn
            If Not rs.EOF Then
                NoJurnal = rs("no_jurnal")
            End If
            rs.Close
            
            conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
            
            
'            conn.Execute "delete from jurnal where no_trans='" & lblNoTrans & "'"
        
    End If
    
    
    
    add_dataheader
    
    For Row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(Row, 1) <> "" Then
            add_datadetail (Row)
            
        End If
    Next
    
'    add_jurnal
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    txtKeterangan.text = ""
    loadcombo
    totalDebet = 0
    totalKredit = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(6)
    ReDim nilai(6)
    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "tipe"
    fields(5) = "userid"

    nilai(0) = lblNoTrans
    nilai(1) = NoJurnal
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = txtKeterangan.text
    nilai(4) = "GL"
    nilai(5) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "kredit"
    fields(4) = "no_urut"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = Replace(flxGrid.TextMatrix(Row, 3), ",", ".")
    nilai(3) = Replace(flxGrid.TextMatrix(Row, 4), ",", ".")
    nilai(4) = Row
    
    tambah_data table_name, fields, nilai
    
End Sub

'Private Sub add_jurnal(row As Integer)
'    Dim fields() As String
'    Dim nilai() As String
'    Dim table_name As String
'
'    ReDim fields(5)
'    ReDim nilai(5)
'
'    table_name = "jurnal"
'    fields(0) = "no_trans"
'    fields(1) = "keterangan"
'    fields(2) = "kode_acc"
'    fields(3) = "debet"
'    fields(4) = "kredit"
'
'
'    nilai(0) = lblNoTrans
'    nilai(1) = "Jurnal Umum"
'    nilai(2) = flxGrid.TextMatrix(row, 1)
'    nilai(3) = flxGrid.TextMatrix(row, 3)
'    nilai(4) = flxGrid.TextMatrix(row, 4)
'
'
'    tambah_data table_name, fields, nilai
'
'End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select * from t_jurnalh where no_transaksi='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
        
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        totalDebet = 0
        totalKredit = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1
        
        rs.Open "select d.kode_acc,m.Nama,d.debet,d.kredit from t_jurnald d inner join ms_coa m on d.kode_acc=m.kode_acc where d.no_transaksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = rs(2)
                flxGrid.TextMatrix(Row, 4) = rs(3)
                
                Row = Row + 1
                totalDebet = totalDebet + rs(2)
                totalKredit = totalKredit + rs(3)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblDebet = Format(totalDebet, "#,##0")
    lblKredit = Format(totalKredit, "#,##0")

        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then

    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtAcc.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            cek_Acc
            
            txtDebet.text = flxGrid.TextMatrix(flxGrid.Row, 3)
            txtKredit.text = flxGrid.TextMatrix(flxGrid.Row, 4)
            If txtDebet > 0 Then txtDebet.SetFocus Else txtKredit.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchAcc_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtAcc.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    LblNamaAcc = ""
    loadcombo
    reset_form
    total = 0
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    'End If
    'rs.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 1500
    flxGrid.ColWidth(4) = 1500
    
    
    flxGrid.TextMatrix(0, 1) = "Kode Acc"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Debet"
    flxGrid.TextMatrix(0, 4) = "Kredit"
    


End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_Acc() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where fg_aktif = 1 and kode_acc='" & txtAcc & "' ", conn
    If Not rs.EOF Then
        LblNamaAcc = rs(1)
        cek_Acc = True
        'If rs(4) = "D" Then debet = True Else debet = False
    Else
        LblNamaAcc = ""
        lblKode = ""
        
        cek_Acc = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtAcc_GotFocus()
    txtAcc.SelStart = 0
    txtAcc.SelLength = Len(txtAcc.text)
'    foc = 1
End Sub

Private Sub txtAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
'        If txtAcc.text = "" Then
'            flxGrid_Click
'        Else
        If txtAcc.text = "" Then KeyCode = 0
        
        If txtAcc.text <> "" Then
            If Not cek_Acc Then
                MsgBox "Kode yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    End If
End Sub

Private Sub txtAcc_LostFocus()
'On Error Resume Next
'
'    If txtAcc.text <> "" And Not cek_Acc Then
'        MsgBox "Kode yang anda masukkan salah"
'        txtAcc.SetFocus
'    Else
''        If debet = False Then txtKredit.SetFocus
'    End If
'    foc = 0
End Sub

Private Sub txtDebet_GotFocus()
    txtDebet.SelStart = 0
    txtDebet.SelLength = Len(txtDebet.text)
End Sub

Private Sub txtDebet_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtDebet_LostFocus()
    If Not IsNumeric(txtDebet.text) Then txtDebet.text = "1"
End Sub

Private Sub txtKeterangan_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtAcc.SetFocus
End Sub

Private Sub txtKredit_GotFocus()
    txtKredit.SelStart = 0
    txtKredit.SelLength = Len(txtKredit.text)
End Sub

Private Sub txtKredit_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtKredit_LostFocus()
    If Not IsNumeric(txtKredit.text) Then txtKredit.text = "1"
End Sub
