VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransStockMutasi 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mutasi Stock"
   ClientHeight    =   9135
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12705
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9135
   ScaleWidth      =   12705
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5280
      Picture         =   "frmTransStockMutasi.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   60
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4185
      Picture         =   "frmTransStockMutasi.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   59
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3030
      Index           =   1
      Left            =   5760
      TabIndex        =   22
      Top             =   3375
      Visible         =   0   'False
      Width           =   3210
      _ExtentX        =   5662
      _ExtentY        =   5345
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   6
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3030
      Index           =   0
      Left            =   5760
      TabIndex        =   21
      Top             =   2970
      Visible         =   0   'False
      Width           =   3210
      _ExtentX        =   5662
      _ExtentY        =   5345
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   6
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   6285
      Index           =   1
      Left            =   315
      TabIndex        =   43
      Top             =   2025
      Visible         =   0   'False
      Width           =   11340
      Begin VB.ListBox listPemindah 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1500
         Left            =   4425
         TabIndex        =   58
         Top             =   5010
         Visible         =   0   'False
         Width           =   3705
      End
      Begin VB.ListBox listPenyerah 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1020
         Left            =   4410
         TabIndex        =   52
         Top             =   2385
         Width           =   3660
      End
      Begin VB.TextBox txtMenerima 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2025
         TabIndex        =   51
         Top             =   3465
         Width           =   1755
      End
      Begin VB.ListBox listPenerima 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1500
         Left            =   4410
         TabIndex        =   50
         Top             =   3465
         Width           =   3705
      End
      Begin VB.CommandButton cmdSearchMenerima 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3870
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   49
         Top             =   3420
         Width           =   420
      End
      Begin VB.TextBox txtMenyerahkan 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2025
         TabIndex        =   48
         Top             =   2430
         Width           =   1755
      End
      Begin VB.CommandButton cmdSearchMenyerahkan 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3870
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   47
         Top             =   2430
         Width           =   420
      End
      Begin VB.ListBox listKuli 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1680
         Left            =   1935
         Style           =   1  'Checkbox
         TabIndex        =   46
         Top             =   405
         Width           =   3705
      End
      Begin VB.TextBox txtMemindahkan 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2055
         TabIndex        =   45
         Top             =   5100
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdSearchPemindah 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3870
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   44
         Top             =   5100
         Visible         =   0   'False
         Width           =   420
      End
      Begin VB.Label lblNamaPenerima 
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2025
         TabIndex        =   57
         Top             =   3870
         Width           =   2265
      End
      Begin VB.Label Label3 
         Caption         =   "Kuli"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   56
         Top             =   360
         Width           =   1905
      End
      Begin VB.Label Label5 
         Caption         =   "Yang Menyerahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   55
         Top             =   2430
         Width           =   1770
      End
      Begin VB.Label Label9 
         Caption         =   "Yang Menerima"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   54
         Top             =   3510
         Width           =   1545
      End
      Begin VB.Label lblNamaPenyerah 
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2070
         TabIndex        =   53
         Top             =   2835
         Width           =   2265
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5685
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   8460
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4455
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   8460
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      MaxLength       =   80
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   1005
      Width           =   5910
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1605
      TabIndex        =   17
      Top             =   150
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1170
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   8460
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   16
      Top             =   165
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6915
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   8460
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3195
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   8460
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   1620
      TabIndex        =   20
      Top             =   585
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   99352579
      CurrentDate     =   38927
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8925
      Top             =   210
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9555
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   6090
      Index           =   0
      Left            =   270
      TabIndex        =   24
      Top             =   1935
      Width           =   11400
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1920
         Left            =   90
         TabIndex        =   25
         Top             =   45
         Width           =   11160
         Begin VB.TextBox txtNoAsal 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   8640
            TabIndex        =   9
            Top             =   630
            Width           =   2295
         End
         Begin VB.TextBox txtNoTujuan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Left            =   8640
            TabIndex        =   11
            Top             =   1080
            Width           =   2295
         End
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   4
            Top             =   1485
            Width           =   1140
         End
         Begin VB.CommandButton cmdopentreeview 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   8235
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   31
            Top             =   1125
            Width           =   330
         End
         Begin VB.ComboBox cmbGudang 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   5400
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   1080
            Width           =   2805
         End
         Begin VB.CommandButton cmdopentreeview 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   8235
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   30
            Top             =   675
            Width           =   330
         End
         Begin VB.ComboBox cmbGudang 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   0
            Left            =   5400
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   630
            Width           =   2805
         End
         Begin VB.ComboBox cmbSerial 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1485
            TabIndex        =   2
            Text            =   "cmbSerial"
            Top             =   630
            Width           =   2325
         End
         Begin VB.CommandButton cmdSearchBarang 
            Caption         =   "F3"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3885
            TabIndex        =   29
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox txtQty 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1485
            TabIndex        =   3
            Top             =   1050
            Width           =   1140
         End
         Begin VB.CommandButton cmdDelete 
            Caption         =   "&Hapus"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2670
            TabIndex        =   28
            Top             =   2040
            Width           =   1050
         End
         Begin VB.CommandButton cmdClear 
            Caption         =   "&Baru"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1560
            TabIndex        =   27
            Top             =   2040
            Width           =   1050
         End
         Begin VB.CommandButton cmdOk 
            Caption         =   "&Tambahkan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   180
            TabIndex        =   26
            Top             =   2040
            Width           =   1350
         End
         Begin VB.TextBox txtKode 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1485
            TabIndex        =   1
            Top             =   225
            Width           =   2340
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2700
            TabIndex        =   40
            Top             =   1545
            Width           =   600
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   39
            Top             =   1515
            Width           =   1020
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Gudang Asal    :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4005
            TabIndex        =   38
            Top             =   675
            Width           =   1380
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Gudang Tujuan:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4005
            TabIndex        =   37
            Top             =   1095
            Width           =   1380
         End
         Begin VB.Label Label4 
            BackColor       =   &H8000000C&
            Caption         =   "No. Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   36
            Top             =   675
            Width           =   1170
         End
         Begin VB.Label Label6 
            BackColor       =   &H8000000C&
            Caption         =   "Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   35
            Top             =   1110
            Width           =   825
         End
         Begin VB.Label lblDefQtyJual 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   4710
            TabIndex        =   34
            Top             =   975
            Width           =   870
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   33
            Top             =   270
            Width           =   1050
         End
         Begin VB.Label LblNamaBarang 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Nama Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4350
            TabIndex        =   32
            Top             =   285
            Width           =   1155
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2850
         Left            =   135
         TabIndex        =   41
         Top             =   2025
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   5027
         _Version        =   393216
         Cols            =   8
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxgridRekap 
         Height          =   1125
         Left            =   135
         TabIndex        =   42
         Top             =   4995
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   1984
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   6765
      Left            =   180
      TabIndex        =   23
      Top             =   1575
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   11933
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Barang"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Pekerja"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label8 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   19
      Top             =   1035
      Width           =   1275
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   18
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   15
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   14
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransStockMutasi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mode As Byte
Dim total As Long
Dim totalHpp As Double

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String
Dim seltab As Byte


Private Sub cmbSerial_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
On Error GoTo err
    conn.Open strcon
    txtBerat = getStockKertas2(txtKode, Trim(Right(cmbGudang(0).text, 10)), cmbSerial.text, conn)
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdClear_Click()
    
    
    lblSatuan = ""
    txtQty.text = "0"
    txtBerat.text = "0"
    lblStock = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    delete_rekap
    If flxGrid.Rows <= 2 Then Exit Sub
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_mutasibahan] from t_stockmutasibahanH where [nomer_mutasibahan]>'" & lblNoTrans & "' order by [nomer_mutasibahan]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
On Error Resume Next
Dim i As Integer
Dim row As Integer
    row = 0
    
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = cmbGudang(0).text
        flxGrid.TextMatrix(row, 2) = cmbGudang(1).text
        flxGrid.TextMatrix(row, 3) = txtKode.text
        flxGrid.TextMatrix(row, 4) = LblNamaBarang
        flxGrid.TextMatrix(row, 5) = cmbSerial.text
        flxGrid.TextMatrix(row, 6) = txtQty.text
        flxGrid.TextMatrix(row, 7) = txtBerat
        add_rekap txtKode, LblNamaBarang, txtQty, txtBerat
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 4
        
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtKode.SetFocus
        
    
    mode = 1

End Sub
Private Sub add_rekap(kodebarang As String, namabarang As String, qty As Double, berat As Double)
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(kodebarang) Then row = i
    Next
    If row = 0 Then
        row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.row, 0) = ""
    .row = row


    .TextMatrix(row, 1) = kodebarang
    .TextMatrix(row, 2) = namabarang
    If .TextMatrix(row, 3) = "" Then .TextMatrix(row, 3) = qty Else .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) + qty
    If .TextMatrix(row, 4) = "" Then .TextMatrix(row, 4) = berat Else .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) + berat
    lblItem = .Rows - 2
    End With
End Sub



Private Sub cmdopentreeview_Click(Index As Integer)

On Error Resume Next
    If tvwMain(Index).Visible = False Then
    tvwMain(Index).Visible = True
    tvwMain(Index).SetFocus
    Else
    tvwMain(Index).Visible = False
    End If

End Sub

Private Sub cmdPosting_Click()
    If simpan Then
        postingMutasiBahan lblNoTrans, True
        reset_form
    End If
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_mutasibahan] from t_stockmutasibahanH where [nomer_mutasibahan]<'" & lblNoTrans & "' order by [nomer_mutasibahan] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
    cetaknota lblNoTrans
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearchBarang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmTransStockOpname"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_barang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub

Public Sub cari_barang()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select kode_bahan,nama_bahan from ms_bahan where kode_bahan='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(1)
        txtQty.text = 1
        
    Else
        LblNamaBarang = ""
        txtQty.text = 0
        
'        txtKode.SetFocus
    End If
    rs.Close
    cmbSerial.Clear
    rs.Open "select nomer_serial from stock where kode_bahan='" & txtKode.text & "' and kode_gudang='" & Trim(Right(cmbGudang(0).text, 10)) & "' ", conn
    While Not rs.EOF
        cmbSerial.AddItem rs(0)
        cmbSerial.Refresh
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
End Sub


Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select t.nomer_mutasibahan,t.tanggal,t.keterangan from t_stockmutasibahanH t where t.status_posting=0"
    frmSearch.nmform = "frmTransStockmutasi"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "mutasibahan"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    id = lblNoTrans

    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan item detail terlebih dahulu"
        txtKeterangan.SetFocus
        Exit Function
    End If
    
    conn.Open strcon
    
    conn.BeginTrans
    
    i = 1
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_stockMutasibahanH", "nomer_mutasibahan", DTPicker1, "MS")
    Else
        conn.Execute "delete from t_stockmutasibahanH where nomer_mutasibahan='" & lblNoTrans & "'"
        conn.Execute "delete from t_stockmutasibahanD where nomer_mutasibahan='" & lblNoTrans & "'"
    End If
    add_dataheader
    totalHpp = 0
    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    conn.CommitTrans
    DropConnection
    simpan = True
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub reset_form()
    lblNoTrans = "-"
    lblTotal = "0"
    txtKeterangan.text = ""
    total = 0
    DTPicker1 = Now
    txtMenerima = ""
    txtMemindahkan = ""
    txtMenyerahkan = ""
    listPenerima.Clear
    listPemindah.Clear
    listPenyerah.Clear
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxgridRekap.Rows = 1
    flxgridRekap.Rows = 2
    cmdPrint.Visible = False
'    cmdPosting.Enabled = False
    cmdSearchBarang.Enabled = True
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
    
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(9)
    ReDim nilai(9)
    table_name = "t_stockmutasibahanH"
    fields(0) = "nomer_mutasibahan"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "userid"
    fields(4) = "kuli"
    fields(5) = "penyerah"
    fields(6) = "penerima"
    fields(7) = "no_asal"
    fields(8) = "no_tujuan"
    Dim kuli, penyerah, penerima As String
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    
    For A = 0 To listPenyerah.ListCount - 1
        penyerah = penyerah & "[" & Left(listPenyerah.List(A), InStr(1, listPenyerah.List(A), "-") - 1) & "],"
    Next
    If penyerah <> "" Then penyerah = Left(penyerah, Len(penyerah) - 1)
    penerima = ""
    For A = 0 To listPenerima.ListCount - 1
        penerima = penerima & "[" & Left(listPenerima.List(A), InStr(1, listPenerima.List(A), "-") - 1) & "],"
    Next
    If penerima <> "" Then penerima = Left(penerima, Len(penerima) - 1)
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = User
    nilai(4) = kuli
    nilai(5) = penyerah
    nilai(6) = penerima
    nilai(7) = txtNoAsal
    nilai(8) = txtNoTujuan
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim selisih As Double
    
    ReDim fields(9)
    ReDim nilai(9)
    
    table_name = "t_stockmutasibahanD"
    fields(0) = "nomer_mutasibahan"
    fields(1) = "kode_gudangasal"
    fields(2) = "kode_gudangtujuan"
    fields(3) = "kode_bahan"
    fields(4) = "nomer_serial"
    fields(5) = "qty"
    fields(6) = "berat"
    fields(7) = "no_urut"
    fields(8) = "hpp"
       
    nilai(0) = lblNoTrans
    nilai(1) = Trim(Right(flxGrid.TextMatrix(row, 1), 10))
    nilai(2) = Trim(Right(flxGrid.TextMatrix(row, 2), 10))
    nilai(3) = flxGrid.TextMatrix(row, 3)
    nilai(4) = flxGrid.TextMatrix(row, 5)
    nilai(5) = flxGrid.TextMatrix(row, 6)
    nilai(6) = flxGrid.TextMatrix(row, 7)
    
    nilai(7) = row
    
    nilai(8) = Replace(Format(GetHPPKertas2(nilai(3), nilai(4), nilai(1), conn), "###0.##"), ",", ".")
    tambah_data table_name, fields, nilai
End Sub



Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_stockmutasibahanH t " & _
            "where t.nomer_mutasibahan ='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtNoAsal = rs!no_asal
        txtNoTujuan = rs!no_tujuan
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        Dim kuli() As String
        Dim penyerah() As String
        Dim penerima() As String
        kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
        Next
        penyerah = Split(rs!penyerah, ",")
        listPenyerah.Clear
        For A = 0 To UBound(penyerah)
            txtMenyerahkan = Replace(Replace(penyerah(A), "[", ""), "]", "")
            txtMenyerahkan_KeyDown 13, 0
        Next
        penerima = Split(rs!penerima, ",")
        listPenerima.Clear
        For A = 0 To UBound(penerima)
            txtMenerima = Replace(Replace(penerima(A), "[", ""), "]", "")
            txtMenerima_KeyDown 13, 0
        Next
        
        
        total = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.kode_gudangasal,d.kode_gudangtujuan,d.kode_bahan,d.nomer_serial,d.qty,d.berat " & _
                "from t_stockmutasibahanD d " & _
                "where d.nomer_mutasibahan='" & lblNoTrans & "' order by d.no_urut", conn, adOpenDynamic, adLockOptimistic
        While Not rs.EOF
                SetComboTextRight rs(0), cmbGudang(0)
                SetComboTextRight rs(1), cmbGudang(1)
                txtKode = rs(2)
                cari_barang
                SetComboText rs(3), cmbSerial
                txtQty = rs(4)
                txtBerat = rs(5)
                cmdOk_Click
                
                rs.MoveNext
        Wend
        rs.Close
        cmdPosting.Enabled = True
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKode.SetFocus
End Sub

Private Sub cmdSearchMenerima_Click()
    With frmSearch
        .connstr = strcon
        .query = searchKaryawan
        .nmform = "frmTransStockMutasi"
        .nmctrl = "txtMenerima"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_penerima"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchMenyerahkan_Click()
    With frmSearch
        .connstr = strcon
        .query = searchKaryawan
        .nmform = "frmTransStockMutasi"
        .nmctrl = "txtMenyerahkan"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_penyerah"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPemindah_Click()
    With frmSearch
        .connstr = strcon
        .query = searchKaryawan
        .nmform = "frmTransStockMutasi"
        .nmctrl = "txtMemindahkan"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_pemindah"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub


Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            SetComboTextRight flxGrid.TextMatrix(flxGrid.row, 1), cmbGudang(0)
            SetComboTextRight flxGrid.TextMatrix(flxGrid.row, 2), cmbGudang(1)
            txtKode.text = flxGrid.TextMatrix(flxGrid.row, 3)
            'LblNamaBarang =
            cari_barang
            SetComboText flxGrid.TextMatrix(flxGrid.row, 5), cmbSerial
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 7)
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKode.text = flxGrid.TextMatrix(flxGrid.row, 1)
            cari_barang
            SetComboText flxGrid.TextMatrix(flxGrid.row, 2), cmbSerial
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
            txtKode.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
'    MySendKeys "{tab}"
End Sub


Private Sub delete_rekap()
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(flxGrid.TextMatrix(flxGrid.row, 3)) Then row = i
    Next
    If row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(row, 3)) Then .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) - flxGrid.TextMatrix(flxGrid.row, 6)
    If IsNumeric(.TextMatrix(row, 4)) Then .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) - flxGrid.TextMatrix(flxGrid.row, 7)
    If .TextMatrix(row, 4) = 0 Or .TextMatrix(row, 3) = 0 Then
        For row = row To .Rows - 1
            If row = .Rows - 1 Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(row + 1, 1) = "" Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
            ElseIf .TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To .cols - 1
                .TextMatrix(row, col) = .TextMatrix(row + 1, col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    

    End With
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then

        KeyAscii = 0
        MySendKeys "{tab}"

    End If
End Sub

Private Sub Form_Load()
    loadgrup "0", tvwMain(0)
    loadgrup "0", tvwMain(1)
    
    LblNamaBarang = ""
    lblStock = ""
    DTPicker1 = Now
    reset_form
    
    Load_ListKuli
    total = 0
    seltab = 0
    conn.ConnectionString = strcon
    conn.Open
    
'    listKuli.Clear
'    rs.Open "select * from ms_karyawan order by nik", conn
'    While Not rs.EOF
'        listKuli.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
    
    cmbGudang(0).Clear
    cmbGudang(1).Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang(0).AddItem rs(0) & Space(50) & rs(1)
        cmbGudang(1).AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    
    rs.Close
    conn.Close
    
    lblGudang = gudang
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1200
    flxGrid.ColWidth(2) = 1200
    flxGrid.ColWidth(3) = 1200
    flxGrid.ColWidth(4) = 4000
    flxGrid.ColWidth(5) = 1200
    flxGrid.ColWidth(6) = 800
    flxGrid.ColWidth(7) = 800
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(2) = 1
    flxGrid.TextMatrix(0, 1) = "Gdg Asal"
    flxGrid.TextMatrix(0, 2) = "Gdg Tujuan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 3) = "Kode Bahan"
    flxGrid.TextMatrix(0, 4) = "Nama"
    flxGrid.TextMatrix(0, 5) = "Serial"
    flxGrid.TextMatrix(0, 6) = "Qty"
    flxGrid.TextMatrix(0, 7) = "Berat"
    If cmbGudang(0).ListCount > 0 Then cmbGudang(0).ListIndex = 0
    If cmbGudang(1).ListCount > 0 Then cmbGudang(1).ListIndex = 0
    With flxgridRekap
        .ColWidth(0) = 300
        .ColWidth(1) = 1400
        .ColWidth(2) = 3000
        .ColWidth(3) = 900
        .ColWidth(4) = 900
            
        .TextMatrix(0, 1) = "Kode Bahan"
        .ColAlignment(2) = 1 'vbAlignLeft
        .ColAlignment(1) = 1
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Jml Kemasan"
        .TextMatrix(0, 4) = "Berat"
    End With
End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub


Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub listPemindah_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listPemindah.RemoveItem (listPemindah.ListIndex)
End Sub

Private Sub listPenerima_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listPenerima.RemoveItem (listPenerima.ListIndex)
End Sub

Private Sub listPenyerah_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listPenyerah.RemoveItem (listPenyerah.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub Text1_Change()

End Sub

Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub tvwMain_NodeClick(Index As Integer, ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang(Index)
    tvwMain(Index).Visible = False
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtQty.text)
End Sub

Private Sub txtBerat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then cmdOk_Click
End Sub

Private Sub txtKeterangan_GotFocus()
    txtKeterangan.SelStart = 0
    txtKeterangan.SelLength = Len(txtKeterangan.text)
    foc = 1
End Sub

Private Sub txtKode_LostFocus()
 If txtKode <> "" Then cari_barang
End Sub

Private Sub txtMemindahkan_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPemindah_Click
    If KeyCode = 13 Then
        If cek_pemindah Then
            listPemindah.AddItem txtMemindahkan & "-" & lblNamaPemindah
            txtMemindahkan = ""
            lblNamaPemindah = ""
            MySendKeys "+{tab}"
        End If
    End If
End Sub

Private Sub txtMenerima_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchMenerima_Click
    If KeyCode = 13 Then
        If cek_penerima Then
        listPenerima.AddItem txtMenerima & "-" & lblNamaPenerima
        txtMenerima = ""
        lblNamaPenerima = ""
        MySendKeys "+{tab}"
        End If
    End If
End Sub

Private Sub txtMenyerahkan_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchMenyerahkan_Click
    If KeyCode = 13 Then
        If cek_penyerah Then
        listPenyerah.AddItem txtMenyerahkan & "-" & lblNamaPenyerah
        txtMenyerahkan = ""
        lblNamaPenyerah = ""
        MySendKeys "+{tab}"
        End If
    End If
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

'Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then cmdOk.SetFocus
'End Sub

'Private Sub txtQty_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        txtQty.text = calc(txtQty.text)
'    End If
'End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Public Sub cetaknota(no_nota As String)
On Error GoTo err
Dim query() As String
Dim rs() As New ADODB.Recordset

    conn.Open strcon
    ReDim query(2) As String
    query(0) = "select * from t_stockmutasibahanD"
    query(1) = "select * from t_stockmutasibahanH"
    query(2) = "select * from ms_bahan"
    ReDim rs(UBound(query) - 1)
    For i = 0 To UBound(query) - 1
        rs(i).Open query(i), conn, adOpenForwardOnly
    Next
    With CRPrint
        .reset
        .ReportFileName = reportDir & "\PO Stockmutasi.rpt"
        For i = 0 To UBound(query) - 1
            .SetTablePrivateData i, 3, rs(i)
        Next
        .Formulas(0) = "NamaPerusahaan='" & GNamaPerusahaan & "'"
        .Formulas(1) = "AlamatPerusahaan='" & GAlamatPerusahaan & "'"
        .Formulas(2) = "TeleponPerusahaan='" & GTeleponPerusahaan & "'"

        .SelectionFormula = "{t_stockmutasibahanH.nomer_mutasibahan}='" & no_nota & "'"
'        .PrinterSelect
        .ProgressDialog = False
        .WindowState = crptMaximized
'        .Destination = crptToPrinter
        .action = 1
    End With
    conn.Close
    Exit Sub
err:
MsgBox err.Description
End Sub

Public Function cek_penerima() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    cek_penerima = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtMenerima & "'", conn
    If Not rs.EOF Then
        lblNamaPenerima = rs(1)
        cek_penerima = True
    Else
        lblNamaPenerima = ""
    End If
    rs.Close
    conn.Close
err:
End Function
Public Function cek_penyerah() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    cek_penyerah = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtMenyerahkan & "'", conn
    If Not rs.EOF Then
        lblNamaPenyerah = rs(1)
        cek_penyerah = True
    Else
        lblNamaPenyerah = ""
    End If
    rs.Close
    conn.Close
err:
End Function
Public Function cek_pemindah() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    cek_pemindah = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtMemindahkan & "'", conn
    If Not rs.EOF Then
        lblNamaPemindah = rs(1)
        cek_pemindah = True
    Else
        lblNamaPemindah = ""
    End If
    rs.Close
    conn.Close
err:
End Function
