VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmLaporanStok 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Laporan Stok Bahan"
   ClientHeight    =   1470
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4620
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1470
   ScaleWidth      =   4620
   Begin VB.CommandButton cmdScreen 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Screen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   660
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   765
      Width           =   1455
   End
   Begin VB.CommandButton cmdPrinter 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Printer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2235
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   765
      Width           =   1455
   End
   Begin VB.OptionButton OptTipe 
      BackColor       =   &H00000080&
      Caption         =   "Laporan Stok Kritis"
      ForeColor       =   &H8000000E&
      Height          =   330
      Index           =   1
      Left            =   2070
      TabIndex        =   1
      Top             =   2835
      Width           =   1680
   End
   Begin VB.OptionButton OptTipe 
      BackColor       =   &H00000080&
      Caption         =   "Laporan Stok"
      ForeColor       =   &H8000000E&
      Height          =   330
      Index           =   0
      Left            =   405
      TabIndex        =   0
      Top             =   2745
      Width           =   1680
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   4995
      Top             =   225
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
End
Attribute VB_Name = "frmLaporanStok"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public Tipe As Byte


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub CmdPrinter_Click()
On Error GoTo err

    With CRPrint
        .reset
        .ReportFileName = reportDir & filename
                For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
        Next
        .ParameterFields(0) = "login;" & User & ";True"
        .ProgressDialog = False
        .Destination = crptToPrinter
        .action = 1
    End With
    Exit Sub

err:
   If err.Number <> 20520 Then MsgBox err.Description
End Sub

Private Sub cmdScreen_Click()
On Error GoTo err

    With CRPrint
        .reset
        .ReportFileName = reportDir & filename
                For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
        Next
        .ParameterFields(0) = "login;" & User & ";True"
        .ProgressDialog = False
        .WindowState = crptMaximized
        .Destination = crptToWindow
        .action = 1
    End With
    Exit Sub

err:
   If err.Number <> 20520 Then MsgBox err.Description


End Sub
Private Sub cetakLaporan()
On Error GoTo err
    
    With CRPrint
        .reset
         .ReportFileName = reportDir & "\Laporan Stock.rpt"
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";UID=" & User & ";PWD=" & pwd & ";"
        Next
    
        .SelectionFormula = Formula
        .Destination = crptToWindow
        .ParameterFields(0) = "detail;" + User + ";True"
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        .action = 1
        
        
    End With
    Exit Sub
err:
    MsgBox err.Description
    Resume Next

    
    
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Unload Me

End Sub

Private Sub Form_Load()
'    DTPicker1 = Now
'    DTPicker2 = Now
'    If filename = "\Laporan Penjualan" Then
       ' frDetail.Visible = True
'    End If
End Sub


Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
'    cmdPrint_Click
End Sub

Private Sub Option1_Click()

End Sub

