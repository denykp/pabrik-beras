VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{0A45DB48-BD0D-11D2-8D14-00104B9E072A}#2.0#0"; "sstabs2.ocx"
Begin VB.Form frmAddSuratJalan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pengeluaran Barang"
   ClientHeight    =   9675
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12210
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9675
   ScaleWidth      =   12210
   Begin ActiveTabs.SSActiveTabs SSActiveTabs1 
      Height          =   7245
      Left            =   90
      TabIndex        =   26
      Top             =   1770
      Width           =   11955
      _ExtentX        =   21087
      _ExtentY        =   12779
      _Version        =   131083
      TabCount        =   2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty FontSelectedTab {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TagVariant      =   ""
      Tabs            =   "frmAddSuratJalan.frx":0000
      Begin ActiveTabs.SSActiveTabPanel SSActiveTabPanel2 
         Height          =   6825
         Left            =   -99969
         TabIndex        =   28
         Top             =   390
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   12039
         _Version        =   131083
         TabGuid         =   "frmAddSuratJalan.frx":007B
         Begin VB.Frame frDetail 
            BorderStyle     =   0  'None
            Height          =   5835
            Index           =   1
            Left            =   0
            TabIndex        =   29
            Top             =   30
            Width           =   11430
            Begin VB.Frame Frame1 
               Caption         =   "Rincian Biaya"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   3090
               Left            =   4695
               TabIndex        =   37
               Top             =   2640
               Width           =   6705
               Begin VB.TextBox txtKeteranganEkspedisi 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   1755
                  TabIndex        =   46
                  Top             =   2025
                  Width           =   4770
               End
               Begin VB.PictureBox Picture3 
                  Appearance      =   0  'Flat
                  ForeColor       =   &H80000008&
                  Height          =   375
                  Left            =   1800
                  ScaleHeight     =   345
                  ScaleWidth      =   2670
                  TabIndex        =   42
                  Top             =   2580
                  Width           =   2700
                  Begin VB.OptionButton OptCaraBayar 
                     Caption         =   "Tunai"
                     BeginProperty Font 
                        Name            =   "Tahoma"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   0
                     Left            =   90
                     TabIndex        =   44
                     Top             =   45
                     Value           =   -1  'True
                     Width           =   915
                  End
                  Begin VB.OptionButton OptCaraBayar 
                     Caption         =   "Kredit"
                     BeginProperty Font 
                        Name            =   "Tahoma"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   1
                     Left            =   1575
                     TabIndex        =   43
                     Top             =   45
                     Width           =   915
                  End
                  Begin VB.Label Label30 
                     BackStyle       =   0  'Transparent
                     Caption         =   "Jumlah bayar"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   12
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   330
                     Left            =   135
                     TabIndex        =   45
                     Top             =   1035
                     Visible         =   0   'False
                     Width           =   1725
                  End
               End
               Begin VB.TextBox txtBiayaAngkut 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   4425
                  Locked          =   -1  'True
                  TabIndex        =   41
                  Top             =   570
                  Width           =   2070
               End
               Begin VB.TextBox txtBiayaLain 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   4425
                  TabIndex        =   40
                  Top             =   1050
                  Width           =   2070
               End
               Begin VB.TextBox txtBiayaAngkutBerat 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   1755
                  TabIndex        =   39
                  Top             =   540
                  Width           =   975
               End
               Begin VB.TextBox txtBiayaAngkutKG 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   3045
                  TabIndex        =   38
                  Top             =   540
                  Width           =   975
               End
               Begin VB.Label Label32 
                  Caption         =   "Keterangan"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   270
                  TabIndex        =   57
                  Top             =   2055
                  Width           =   1380
               End
               Begin VB.Label lblTotalBiaya 
                  Alignment       =   1  'Right Justify
                  Caption         =   "0"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   13.5
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   3840
                  TabIndex        =   56
                  Top             =   1605
                  Width           =   2700
               End
               Begin VB.Label Label31 
                  Caption         =   "Cara Bayar"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   285
                  TabIndex        =   55
                  Top             =   2640
                  Width           =   1380
               End
               Begin VB.Label Label29 
                  Caption         =   "Total Biaya :"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   13.5
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   2175
                  TabIndex        =   54
                  Top             =   1605
                  Width           =   1575
               End
               Begin VB.Label Label18 
                  Caption         =   "Biaya Angkut"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   270
                  TabIndex        =   53
                  Top             =   585
                  Width           =   1170
               End
               Begin VB.Label Label21 
                  Caption         =   "Biaya Lain-Lain"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   270
                  TabIndex        =   52
                  Top             =   1065
                  Width           =   1380
               End
               Begin VB.Label Label23 
                  Caption         =   "Berat"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   1980
                  TabIndex        =   51
                  Top             =   240
                  Width           =   450
               End
               Begin VB.Label Label25 
                  Caption         =   "X"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   2835
                  TabIndex        =   50
                  Top             =   585
                  Width           =   165
               End
               Begin VB.Label Label26 
                  Caption         =   "Biaya / Kg"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   3135
                  TabIndex        =   49
                  Top             =   240
                  Width           =   885
               End
               Begin VB.Label Label27 
                  Caption         =   "="
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   4155
                  TabIndex        =   48
                  Top             =   585
                  Width           =   165
               End
               Begin VB.Label Label28 
                  Caption         =   "Total Biaya Angkut"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   4590
                  TabIndex        =   47
                  Top             =   240
                  Width           =   1680
               End
               Begin VB.Line Line1 
                  X1              =   4170
                  X2              =   6540
                  Y1              =   1515
                  Y2              =   1515
               End
            End
            Begin VB.ListBox listKuli 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   2760
               Left            =   1575
               Style           =   1  'Checkbox
               TabIndex        =   36
               Top             =   2850
               Width           =   2880
            End
            Begin VB.ListBox listTruk 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   780
               Left            =   3960
               TabIndex        =   35
               Top             =   270
               Width           =   3660
            End
            Begin VB.TextBox txtNoTruk 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1575
               TabIndex        =   34
               Top             =   270
               Width           =   1755
            End
            Begin VB.CommandButton cmdSearchPengawas 
               Appearance      =   0  'Flat
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   3420
               MaskColor       =   &H00C0FFFF&
               TabIndex        =   33
               Top             =   1755
               Width           =   420
            End
            Begin VB.ListBox listPengawas 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   780
               Left            =   3960
               TabIndex        =   32
               Top             =   1755
               Width           =   3660
            End
            Begin VB.TextBox txtPengawas 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1575
               TabIndex        =   31
               Top             =   1755
               Width           =   1755
            End
            Begin VB.TextBox txtSupir 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1575
               TabIndex        =   30
               Top             =   1305
               Width           =   5220
            End
            Begin VB.Label Label5 
               Caption         =   "No. Truk"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   225
               TabIndex        =   62
               Top             =   315
               Width           =   960
            End
            Begin VB.Label lblNamaPengawas 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   1575
               TabIndex        =   61
               Top             =   2160
               Width           =   2265
            End
            Begin VB.Label Label9 
               Caption         =   "Supir"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   225
               TabIndex        =   60
               Top             =   1395
               Width           =   960
            End
            Begin VB.Label Label8 
               Caption         =   "Kuli"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   225
               TabIndex        =   59
               Top             =   2880
               Width           =   450
            End
            Begin VB.Label Label6 
               Caption         =   "Pengawas"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   225
               TabIndex        =   58
               Top             =   1800
               Width           =   960
            End
         End
      End
      Begin ActiveTabs.SSActiveTabPanel SSActiveTabPanel1 
         Height          =   6825
         Left            =   30
         TabIndex        =   27
         Top             =   390
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   12039
         _Version        =   131083
         TabGuid         =   "frmAddSuratJalan.frx":00A3
         Begin VB.Frame frDetail 
            BorderStyle     =   0  'None
            Height          =   6510
            Index           =   0
            Left            =   0
            TabIndex        =   63
            Top             =   120
            Width           =   11565
            Begin VB.Frame Frame2 
               BackColor       =   &H00C0C0C0&
               Caption         =   "Detail"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   2265
               Left            =   90
               TabIndex        =   64
               Top             =   45
               Width           =   9960
               Begin VB.ComboBox txtKdBrg 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   1260
                  TabIndex        =   81
                  Text            =   "Combo1"
                  Top             =   630
                  Width           =   2580
               End
               Begin VB.CommandButton cmdSearchBrg 
                  BackColor       =   &H8000000C&
                  Caption         =   "F3"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Left            =   3915
                  Picture         =   "frmAddSuratJalan.frx":00CB
                  TabIndex        =   80
                  Top             =   675
                  Width           =   420
               End
               Begin VB.TextBox txtBerat 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Left            =   1260
                  TabIndex        =   79
                  Top             =   1770
                  Width           =   1140
               End
               Begin VB.PictureBox Picture1 
                  AutoSize        =   -1  'True
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   0  'None
                  Height          =   465
                  Left            =   5670
                  ScaleHeight     =   465
                  ScaleWidth      =   3795
                  TabIndex        =   75
                  Top             =   1710
                  Width           =   3795
                  Begin VB.CommandButton cmdDelete 
                     BackColor       =   &H8000000C&
                     Caption         =   "&Hapus"
                     Enabled         =   0   'False
                     BeginProperty Font 
                        Name            =   "Tahoma"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   375
                     Left            =   2595
                     TabIndex        =   78
                     Top             =   90
                     Width           =   1050
                  End
                  Begin VB.CommandButton cmdClear 
                     BackColor       =   &H8000000C&
                     Caption         =   "&Baru"
                     Enabled         =   0   'False
                     BeginProperty Font 
                        Name            =   "Tahoma"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   375
                     Left            =   1440
                     TabIndex        =   77
                     Top             =   90
                     Width           =   1050
                  End
                  Begin VB.CommandButton cmdOk 
                     BackColor       =   &H8000000C&
                     Caption         =   "&Tambahkan"
                     BeginProperty Font 
                        Name            =   "Tahoma"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   375
                     Left            =   0
                     TabIndex        =   76
                     Top             =   90
                     Width           =   1350
                  End
               End
               Begin VB.TextBox txtQty 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Left            =   1260
                  TabIndex        =   74
                  Top             =   1395
                  Width           =   1140
               End
               Begin VB.TextBox txtNomerSerial 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Left            =   1260
                  TabIndex        =   73
                  Top             =   1035
                  Width           =   1950
               End
               Begin VB.CommandButton cmdSearchSerial 
                  BackColor       =   &H8000000C&
                  Caption         =   "F3"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Left            =   3285
                  Picture         =   "frmAddSuratJalan.frx":01CD
                  TabIndex        =   72
                  Top             =   1035
                  Width           =   420
               End
               Begin VB.TextBox txtNoOrder 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   1260
                  TabIndex        =   71
                  Top             =   225
                  Width           =   1875
               End
               Begin VB.CommandButton cmdSearchSO 
                  Appearance      =   0  'Flat
                  Caption         =   "F3"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   3195
                  MaskColor       =   &H00C0FFFF&
                  TabIndex        =   70
                  Top             =   225
                  Width           =   420
               End
               Begin VB.ComboBox cmbGudang 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   5715
                  Style           =   2  'Dropdown List
                  TabIndex        =   69
                  Top             =   225
                  Width           =   2805
               End
               Begin VB.CommandButton cmdopentreeview 
                  Appearance      =   0  'Flat
                  Caption         =   "..."
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   8550
                  MaskColor       =   &H00C0FFFF&
                  TabIndex        =   68
                  Top             =   270
                  Width           =   330
               End
               Begin VB.TextBox txtID 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   9360
                  TabIndex        =   67
                  Top             =   375
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.ComboBox cmbSatuan 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   2520
                  Style           =   2  'Dropdown List
                  TabIndex        =   66
                  Top             =   1395
                  Width           =   1365
               End
               Begin VB.TextBox txtSAP 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Left            =   3150
                  TabIndex        =   65
                  Top             =   1800
                  Width           =   825
               End
               Begin VB.Label Label13 
                  AutoSize        =   -1  'True
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Berat Total"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   135
                  TabIndex        =   93
                  Top             =   1800
                  Width           =   945
               End
               Begin VB.Label Label14 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Kode Barang"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   135
                  TabIndex        =   92
                  Top             =   675
                  Width           =   1080
               End
               Begin VB.Label Label3 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Kemasan"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   135
                  TabIndex        =   91
                  Top             =   1440
                  Width           =   780
               End
               Begin VB.Label LblNamaBarang1 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "caption"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   4545
                  TabIndex        =   90
                  Top             =   720
                  Width           =   615
               End
               Begin VB.Label lblKode 
                  BackColor       =   &H8000000C&
                  Caption         =   "Label12"
                  ForeColor       =   &H8000000C&
                  Height          =   330
                  Left            =   3825
                  TabIndex        =   89
                  Top             =   1710
                  Visible         =   0   'False
                  Width           =   600
               End
               Begin VB.Label lblSatuan 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Height          =   195
                  Left            =   2475
                  TabIndex        =   88
                  Top             =   1395
                  Width           =   45
               End
               Begin VB.Label Label1 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "Serial"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   135
                  TabIndex        =   87
                  Top             =   1080
                  Width           =   495
               End
               Begin VB.Label Label7 
                  BackStyle       =   0  'Transparent
                  Caption         =   "No.Order"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   135
                  TabIndex        =   86
                  Top             =   285
                  Width           =   1050
               End
               Begin VB.Label Label2 
                  BackStyle       =   0  'Transparent
                  Caption         =   "Gudang"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   4545
                  TabIndex        =   85
                  Top             =   285
                  Width           =   1005
               End
               Begin VB.Label Label19 
                  AutoSize        =   -1  'True
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "SAP"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   2520
                  TabIndex        =   84
                  Top             =   1830
                  Width           =   345
               End
               Begin VB.Label lblStockBerat 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "0"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   3780
                  TabIndex        =   83
                  Top             =   1080
                  Width           =   495
               End
               Begin VB.Label lblStockKemasan 
                  BackColor       =   &H8000000C&
                  BackStyle       =   0  'Transparent
                  Caption         =   "0"
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Left            =   4455
                  TabIndex        =   82
                  Top             =   1080
                  Width           =   495
               End
            End
            Begin MSFlexGridLib.MSFlexGrid flxGrid 
               Height          =   2430
               Left            =   90
               TabIndex        =   94
               Top             =   2385
               Width           =   11400
               _ExtentX        =   20108
               _ExtentY        =   4286
               _Version        =   393216
               Cols            =   10
               SelectionMode   =   1
               AllowUserResizing=   1
               BorderStyle     =   0
               Appearance      =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSFlexGridLib.MSFlexGrid flxgridRekap 
               Height          =   1125
               Left            =   90
               TabIndex        =   95
               Top             =   4950
               Width           =   11310
               _ExtentX        =   19950
               _ExtentY        =   1984
               _Version        =   393216
               Cols            =   6
               SelectionMode   =   1
               AllowUserResizing=   1
               BorderStyle     =   0
               Appearance      =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSComctlLib.TreeView tvwMain 
               Height          =   3030
               Left            =   5790
               TabIndex        =   96
               Top             =   630
               Visible         =   0   'False
               Width           =   3210
               _ExtentX        =   5662
               _ExtentY        =   5345
               _Version        =   393217
               HideSelection   =   0   'False
               LineStyle       =   1
               Style           =   6
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label Label17 
               Caption         =   "Jml Kemasan"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2520
               TabIndex        =   102
               Top             =   6120
               Width           =   1770
            End
            Begin VB.Label lblQty 
               Alignment       =   1  'Right Justify
               Caption         =   "0,00"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   4320
               TabIndex        =   101
               Top             =   6120
               Width           =   1140
            End
            Begin VB.Label lblItem 
               Alignment       =   1  'Right Justify
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   990
               TabIndex        =   100
               Top             =   6120
               Width           =   1140
            End
            Begin VB.Label Label16 
               Caption         =   "Item"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   45
               TabIndex        =   99
               Top             =   6120
               Width           =   735
            End
            Begin VB.Label lblBerat 
               Alignment       =   1  'Right Justify
               Caption         =   "lblBerat"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   7155
               TabIndex        =   98
               Top             =   6120
               Width           =   1140
            End
            Begin VB.Label Label24 
               Caption         =   "Jumlah"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   13.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   6210
               TabIndex        =   97
               Top             =   6120
               Width           =   960
            End
         End
      End
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5760
      Picture         =   "frmAddSuratJalan.frx":02CF
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6855
      Picture         =   "frmAddSuratJalan.frx":0801
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdSearchEkspedisi 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   21
      Top             =   1305
      Width           =   420
   End
   Begin VB.TextBox txtKodeEkspedisi 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6555
      TabIndex        =   20
      Top             =   1335
      Width           =   2025
   End
   Begin VB.CommandButton cmdSearchPosting 
      Caption         =   "Search Post"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4365
      Picture         =   "frmAddSuratJalan.frx":0D33
      TabIndex        =   19
      Top             =   45
      Width           =   1320
   End
   Begin VB.Frame frAngkut 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   330
      Left            =   12195
      TabIndex        =   14
      Top             =   1350
      Width           =   390
   End
   Begin VB.CheckBox chkAngkut 
      Caption         =   "Ongkos Angkut"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5370
      TabIndex        =   13
      Top             =   615
      Value           =   1  'Checked
      Width           =   1590
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   1320
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   495
      Width           =   3960
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      Picture         =   "frmAddSuratJalan.frx":0E35
      TabIndex        =   8
      Top             =   45
      Width           =   420
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9990
      Top             =   8955
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3105
      Picture         =   "frmAddSuratJalan.frx":0F37
      TabIndex        =   5
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   315
      Picture         =   "frmAddSuratJalan.frx":1039
      TabIndex        =   2
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4500
      Picture         =   "frmAddSuratJalan.frx":113B
      TabIndex        =   4
      Top             =   9045
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1710
      Picture         =   "frmAddSuratJalan.frx":123D
      TabIndex        =   3
      Top             =   9045
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   13200
      Top             =   1260
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   12930
      TabIndex        =   7
      Top             =   1335
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   9765
      TabIndex        =   0
      Top             =   15
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   137297923
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   10395
      TabIndex        =   15
      Top             =   465
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   137297923
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker3 
      Height          =   330
      Left            =   10395
      TabIndex        =   17
      Top             =   840
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   137297923
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Simpan + &Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5940
      Picture         =   "frmAddSuratJalan.frx":133F
      TabIndex        =   6
      Top             =   9045
      Width           =   1230
   End
   Begin VB.Label Label10 
      Caption         =   "Ekspedisi :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5400
      TabIndex        =   23
      Top             =   1380
      Width           =   960
   End
   Begin VB.Label lblNamaEkspedisi 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9090
      TabIndex        =   22
      Top             =   1395
      Width           =   3450
   End
   Begin VB.Label Label15 
      Caption         =   "Selesai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9510
      TabIndex        =   18
      Top             =   855
      Width           =   765
   End
   Begin VB.Label Label11 
      Caption         =   "Mulai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9510
      TabIndex        =   16
      Top             =   510
      Width           =   765
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   12
      Top             =   435
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8865
      TabIndex        =   11
      Top             =   60
      Width           =   825
   End
   Begin VB.Label Label22 
      Caption         =   "No.Pengeluaran"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   10
      Top             =   135
      Width           =   1410
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1710
      TabIndex        =   9
      Top             =   45
      Width           =   2085
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddSuratJalan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte
Public suratjalan As Boolean

Private Sub chkAngkut_Click()
    If chkAngkut.value = False Then frAngkut.Visible = False Else frAngkut.Visible = True
End Sub


Private Sub cmbGudang_Click()
    cekserial
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"

    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.Row, 0) = ""
    Row = flxGrid.Row
    totalQty = totalQty - flxGrid.TextMatrix(Row, 5)
    totalberat = totalberat - flxGrid.TextMatrix(Row, 6)
    lblQty = Format(totalQty, "#,##0.##")
    lblBerat = Format(totalberat, "#,##0.##")
    delete_rekap
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If flxGrid.Row > 1 Then flxGrid.Row = flxGrid.Row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.Col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_suratjalan] from t_suratjalanh where [nomer_suratjalan]>'" & lblNoTrans & "' order by [nomer_suratjalan]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then
    If CDbl(txtBerat) > CDbl(lblStockBerat) Then
        MsgBox "Jumlah yang anda masukkan melebihi persediaan yang ada"
        txtBerat.SetFocus
        txtBerat.SelStart = 0
        txtBerat.SelLength = Len(txtBerat)
        Exit Sub
    End If
    If CDbl(txtQty) > CDbl(lblStockKemasan) Then
        MsgBox "Jumlah kemasan yang anda masukkan melebihi persediaan yang ada"
        txtQty.SetFocus
        txtQty.SelStart = 0
        txtQty.SelLength = Len(txtQty)
        Exit Sub
    End If
'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 2) = txtKdBrg.text Then row = i
'        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.Row, 0) = ""
        flxGrid.Row = Row
        currentRow = flxGrid.Row
        flxGrid.TextMatrix(Row, 1) = txtNoOrder
        flxGrid.TextMatrix(Row, 2) = txtKdBrg.text
        flxGrid.TextMatrix(Row, 3) = LblNamaBarang1
        flxGrid.TextMatrix(Row, 4) = txtNomerSerial
        flxGrid.TextMatrix(Row, 5) = txtQty
        flxGrid.TextMatrix(Row, 6) = txtBerat
        flxGrid.TextMatrix(Row, 7) = cmbSatuan.text
        flxGrid.TextMatrix(Row, 8) = txtSAP.text
        flxGrid.TextMatrix(Row, 9) = cmbGudang.text
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(Row, 5)
        totalberat = totalberat + flxGrid.TextMatrix(Row, 6)
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 9 Then
            flxGrid.TopRow = Row - 8
        Else
            flxGrid.TopRow = 1
        End If
        add_rekap
        lblSatuan = ""
        
        txtNomerSerial = ""
        txtQty.text = "1"
        txtBerat.text = "1"
        
        txtNomerSerial.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
End Sub
Private Sub add_rekap()
Dim i As Integer
Dim Row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = txtKdBrg.text And .TextMatrix(i, 5) = cmbSatuan.text Then Row = i
    Next
    If Row = 0 Then
        Row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.Row, 0) = ""
    .Row = Row


    .TextMatrix(Row, 1) = txtKdBrg.text
    .TextMatrix(Row, 2) = LblNamaBarang1
    If .TextMatrix(Row, 3) = "" Then .TextMatrix(Row, 3) = txtQty Else .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) + txtQty
    If .TextMatrix(Row, 4) = "" Then .TextMatrix(Row, 4) = txtBerat Else .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) + txtBerat
    lblItem = .Rows - 2
    End With
End Sub
Private Sub delete_rekap()
Dim i As Integer
Dim Row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = flxGrid.TextMatrix(flxGrid.Row, 2) Then Row = i
    Next
    If Row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(Row, 3)) Then .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) - flxGrid.TextMatrix(flxGrid.Row, 5)
    If IsNumeric(.TextMatrix(Row, 4)) Then .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) - flxGrid.TextMatrix(flxGrid.Row, 6)
    If .TextMatrix(Row, 4) = 0 Or .TextMatrix(Row, 3) = 0 Then
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    End With
End Sub

Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtNoOrder.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            cek_noso
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.Row, 2)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.Row, 3)

            End If
            txtNomerSerial = flxGrid.TextMatrix(flxGrid.Row, 4)
            cekserial
            txtBerat.text = flxGrid.TextMatrix(flxGrid.Row, 6)
            txtQty.text = flxGrid.TextMatrix(flxGrid.Row, 5)
            txtSAP.text = flxGrid.TextMatrix(flxGrid.Row, 8)
            SetComboText flxGrid.TextMatrix(flxGrid.Row, 7), cmbSatuan
            SetComboText flxGrid.TextMatrix(flxGrid.Row, 9), cmbGudang
            txtBerat.SetFocus
        End If
'
End Sub

Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdopentreeview_Click()
On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    If simpan Then
    If posting_suratjalan(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_suratjalan] from t_suratjalanh where [nomer_suratjalan]<'" & lblNoTrans & "' order by [nomer_suratjalan] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
On Error GoTo err
Dim conn As New ADODB.Connection
    If lblNoTrans = "-" Then
        MsgBox "Pilih nomer pengeluaran terlebih dahulu"
        Exit Sub
    End If
    
    With CRPrint
        .reset
        .ReportFileName = reportDir & "\suratjalan.rpt"
        
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
            LogonInfo = .LogonInfo(i)
        Next
        .ParameterFields(0) = "namaperusahaan;" & var_namaPerusahaan & ";True"
        .ParameterFields(1) = "alamat;" & var_alamtPerusahaan & ";True"
        .SelectionFormula = "{t_suratjalanh.nomer_suratjalan}='" & lblNoTrans & "'"
         .PrinterSelect
        
        .Destination = crptToPrinter
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
        If .printername <> "" Then .action = 1
        reset_form
    End With
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearchAngkut_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from t_Angkut where convert(varchar(20),tanggal,12)='" & Format(DTPicker1, "yyMMdd") & "'"
    frmSearch.nmform = "frmAddSuratJalan"
    frmSearch.nmctrl = "txtNoAngkut"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Angkut"
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_angkut"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub cek_angkut()
Dim conn As New ADODB.Connection

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select t.*,e.nama_ekspedisi from t_angkut t " & _
            "left join ms_ekspedisi e on e.kode_ekspedisi=t.kode_ekspedisi " & _
            "where t.nomer_angkut='" & txtNoAngkut.text & "'", conn
    If Not rs.EOF Then
         
'        DTPicker1 = rs("tanggal")
        
        txtKodeEkspedisi = rs!kode_ekspedisi
        lblNamaEkspedisi = rs!nama_ekspedisi
        
        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
        Dim pengawas() As String
        Dim kuli() As String
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
            txtNoTruk_KeyDown 13, 0
        Next
        pengawas = Split(rs!pengawas, ",")
        listPengawas.Clear
        For A = 0 To UBound(pengawas)
            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
            txtPengawas_KeyDown 13, 0
        Next
        kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
        Next
    End If

    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmAddSuratJalan"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchEkspedisi_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKodeEkspedisi"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_ekspedisi"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_ekspedisi"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    If suratjalan Then
    frmSearch.query = Searchsuratjalan & " where status_posting='1'"
    Else
    frmSearch.query = Searchsuratjalan
    End If
    frmSearch.nmform = "frmAddSuratJalan"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "so"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKuli_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmAddSuratJalan"
        .nmctrl = "txtkuli"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .Col = 0
        .Index = -1
        .proc = "cek_kuli"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        .query = searchKaryawan
        .nmform = "frmAddSuratJalan"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .Col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPosting_Click()
    frmSearch.connstr = strcon
    frmSearch.query = Searchsuratjalan & " where status_posting='0'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Suratjalan"
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchso_Click()
    With frmSearch
    .connstr = strcon
    .query = "select nomer_so,kode_customer,nama_customer,keterangan,nomer_requestSO,tanggal_so,tanggal_kirim,status from vw_outstanding_so"
    .nmform = "frmAddSuratJalan"
    .nmctrl = "txtNoOrder"
    .nmctrl2 = ""
    .keyIni = "so"
    .Col = 0
    .Index = -1
    .proc = "cek_noso"
    .loadgrid .query
    '.cmbSort.ListIndex = 1
    .requery
    Set .frm = Me
    .Show vbModal
    End With
End Sub

Private Sub cmdSearchSerial_Click()
    frmSearch.query = "select nomer_serial,stock from stock where kode_bahan='" & txtKdBrg & "' and kode_gudang='" & Trim(Right(cmbGudang.text, 10)) & "'"
    frmSearch.nmform = "frmAddSuratJalan"
    frmSearch.nmctrl = "txtnomerserial"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "serial"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cekserial"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Function cekserial() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cekserial = False
    If cmbGudang.text = "" Or txtKdBrg.text = "" Then
        cekserial = True
        Exit Function
    End If
conn.Open strcon
rs.Open "select * from stock where kode_bahan='" & txtKdBrg.text & "' and kode_gudang='" & Trim(Right(cmbGudang.text, 10)) & "' and nomer_serial='" & txtNomerSerial & "'", conn
    If Not rs.EOF Then
    lblStockBerat = rs!stock
    lblStockKemasan = rs!qty
    cekserial = True
    Else
    If txtNomerSerial.text <> "" Then MsgBox "Barang tersebut tidak ditemukan"
    lblStockBerat = 0
    lblStockKemasan = 0
    End If
rs.Close
conn.Close
Exit Function
err:
MsgBox err.Description
If rs.State Then rs.Close
If conn.State Then conn.Close
Set conn = Nothing
End Function
Private Sub cmdSimpan_Click()
    If validasi Then
    If simpan Then
        MsgBox "Data sudah tersimpan"
'        reset_form
        cmdPosting.Visible = True
        Call MySendKeys("{tab}")
    End If
    End If
End Sub
Private Function validasi() As Boolean
On Error GoTo err
    validasi = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dikirim terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If cmbGudang.text = "" Then
        MsgBox "Pilih gudang terlebih dahulu"
        cmbGudang.SetFocus
        Exit Function
    End If
    validasi = True
err:
End Function

Private Function simpan() As Boolean

Dim i As Integer
Dim id As String
Dim Row As Integer
Dim rs As New ADODB.Recordset
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    If txtKodeEkspedisi.text = "" Then
        MsgBox "Ekspedisi harus diisi"
        Exit Function
    End If
   id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_suratjalanh", "nomer_suratjalan", DTPicker1, "KB")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
'    deletekartustock "Surat Jalan", id, conn
'    rs.Open "select d.nomer_suratjalan,kode_bahan,nomer_serial,qty,berat,kode_gudang from t_suratjalan_timbang d inner join t_suratjalanh h on d.nomer_suratjalan=h.nomer_suratjalan where d.nomer_suratjalan='" & id & "'", conn, adOpenStatic, adLockPessimistic
'    While Not rs.EOF
'        updatestock id, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!berat, 0, conn
'        rs.MoveNext
'    Wend
'    rs.Close
    conn.Execute "delete from t_suratjalanh where nomer_suratjalan='" & id & "'"
    conn.Execute "delete from t_suratjaland where nomer_suratjalan='" & id & "'"
    conn.Execute "delete from t_suratjalan_timbang where nomer_suratjalan='" & id & "'"
    conn.Execute "delete from t_suratjalan_angkut where nomer_suratjalan='" & id & "'"
    
    add_dataheader

    For Row = 1 To flxGrid.Rows - 2
        add_datadetail (Row)
    Next
    If chkAngkut.value = 1 Then add_ekspedisi
    conn.Execute "insert into t_suratjaland (nomer_suratjalan,kode_bahan,qty,berat,satuan,isi,no_urut) " & _
    " select nomer_suratjalan,kode_bahan,sum(qty),sum(berat),satuan,isi,min(no_urut) from t_suratjalan_timbang where nomer_suratjalan='" & lblNoTrans & "' group by nomer_suratjalan,kode_bahan,satuan,isi"
    
    conn.CommitTrans
    i = 0
    simpan = True
    conn.Close
    DropConnection
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    If Len(lblNoTrans) > 1 Then
        lblNoSuratJalan = "SJ" & Mid(lblNoTrans, 3, Len(lblNoTrans))
    Else
        lblNoSuratJalan = lblNoTrans
    End If
    txtNoOrder.text = ""
    txtKeterangan.text = ""
    chkAngkut.value = True
    
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    listPengawas.Clear
   
    txtSupir = ""
    status_posting = False
    totalQty = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblBerat = 0
    lblItem = 0
    
    txtBiayaAngkutBerat.text = 0
    txtBiayaAngkutKG.text = 0
    txtBiayaAngkut.text = 0
    txtBiayaLain.text = 0
    lblTotalBiaya.Caption = 0
    
    txtNamaQCFisik = ""
    txtNamaQCLab = ""
    txtQCFisik = ""
    txtQCLab = ""
    txtQCKeterangan = ""
    txtNoAngkut.text = ""
    cmdClear_Click
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    cmdPrint.Enabled = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxgridRekap.Rows = 1
    flxgridRekap.Rows = 2
    
    TabStrip1.Tabs(1).Selected = True
    
    txtNoOrder.SetFocus
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
End Sub

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(19)
    ReDim nilai(19)
    table_name = "t_suratjalanh"
    fields(0) = "nomer_suratjalan"
    fields(1) = "tanggal_suratjalan"
    
    fields(2) = "nomer_so"
    fields(3) = "keterangan"
    fields(4) = "userid"
    fields(5) = "status_posting"
    
    fields(6) = "no_truk"
    fields(7) = "pengawas"
    fields(8) = "kuli"
    fields(9) = "supir"
    fields(10) = "jam_mulai"
    fields(11) = "jam_selesai"
    fields(12) = "fg_angkut"
    fields(13) = "nomer_angkut"
    fields(14) = "biaya_angkut_berat"
    fields(15) = "biaya_angkut_kg"
    fields(16) = "biaya_angkut"
    fields(17) = "biaya_lain"
    fields(18) = "cara_bayar"
    
    Dim pengawas, supir, kuli, notruk As String
    pengawas = ""
    For A = 0 To listPengawas.ListCount - 1
        pengawas = pengawas & "[" & Left(listPengawas.List(A), InStr(1, listPengawas.List(A), "-") - 1) & "],"
    Next
    If pengawas <> "" Then pengawas = Left(pengawas, Len(pengawas) - 1)
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtNoOrder.text
    nilai(3) = txtKeterangan.text
    nilai(4) = User
    nilai(5) = 0
    nilai(6) = notruk
    nilai(7) = pengawas
    nilai(8) = kuli
    nilai(9) = supir
    nilai(10) = Format(DTPicker2, "HH:mm")
    nilai(11) = Format(DTPicker3, "HH:mm")
    nilai(12) = chkAngkut.value
    nilai(13) = "" 'txtNoAngkut.text
    If Trim(txtBiayaAngkutBerat.text) = "" Then txtBiayaAngkutBerat = 0
    If Trim(txtBiayaAngkutKG.text) = "" Then txtBiayaAngkutKG = 0
    If Trim(txtBiayaAngkut.text) = "" Then txtBiayaAngkut = 0
    If Trim(txtBiayaLain.text) = "" Then txtBiayaLain = 0
    nilai(14) = CDbl(txtBiayaAngkutBerat.text)
    nilai(15) = CDbl(txtBiayaAngkutKG.text)
    nilai(16) = CDbl(txtBiayaAngkut.text)
    nilai(17) = CDbl(txtBiayaLain.text)
    nilai(18) = IIf(OptCaraBayar(0).value = True, "T", "K")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(12)
    ReDim nilai(12)

    table_name = "t_suratjalan_timbang"
    fields(0) = "nomer_suratjalan"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "nomer_serial"
    fields(5) = "NO_URUT"
    fields(6) = "hpp"
    fields(7) = "nomer_order"
    fields(8) = "kode_gudang"
    fields(9) = "satuan"
    fields(10) = "isi"
    fields(11) = "sap"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 2)
    nilai(2) = Replace(Format(flxGrid.TextMatrix(Row, 5), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(Row, 6), "###0.##"), ",", ".")
    nilai(4) = flxGrid.TextMatrix(Row, 4)
    nilai(5) = Row
    
    nilai(7) = flxGrid.TextMatrix(Row, 1)
    nilai(8) = Trim(Right(flxGrid.TextMatrix(Row, 9), 10))
    nilai(9) = Trim(Left(flxGrid.TextMatrix(Row, 7), 50))
    nilai(10) = Trim(Right(flxGrid.TextMatrix(Row, 7), 50))
    nilai(6) = GetHPPKertas2(flxGrid.TextMatrix(Row, 2), flxGrid.TextMatrix(Row, 4), nilai(8), conn)
    nilai(11) = flxGrid.TextMatrix(Row, 8)
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_ekspedisi()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(8)
    ReDim nilai(8)

    table_name = "t_suratjalan_angkut"
    fields(0) = "nomer_suratjalan"
    fields(1) = "tanggal"
    fields(2) = "kode_ekspedisi"
    fields(3) = "fg_bayarangkut"
    fields(4) = "no_truk"
    fields(5) = "supir"
    fields(6) = "no_urut"
    fields(7) = "status"
    Dim supir, notruk As String
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd")
    
    nilai(2) = txtKodeEkspedisi
    nilai(3) = 0
    nilai(4) = notruk
    nilai(5) = supir
    nilai(6) = 1
    nilai(7) = 1
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
cmdSimpan.Enabled = True
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select t.*,a.kode_ekspedisi,isnull(m.nama_supplier,'') as nama_supplier from t_suratjalanh t " & _
            "left join t_suratjalan_angkut a on a.nomer_suratjalan=t.nomer_suratjalan " & _
            "left join ms_supplier m on m.kode_supplier=a.kode_ekspedisi " & _
            "where t.nomer_suratjalan='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        lblNoSuratJalan = "SJ" & Mid(lblNoTrans, 3, Len(lblNoTrans))
        DTPicker1 = rs("tanggal_suratjalan")
        txtKeterangan.text = rs("keterangan")

        txtNoOrder.text = rs("nomer_so")
        txtBiayaAngkutBerat.text = rs("biaya_angkut_berat")
        txtBiayaAngkutKG.text = rs("biaya_angkut_kg")
        txtBiayaAngkut.text = rs("biaya_angkut")
        txtBiayaLain.text = rs("biaya_lain")
        lblTotalBiaya.Caption = CDbl(txtBiayaAngkut.text) + CDbl(txtBiayaLain.text)
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        If rs("cara_bayar") = "T" Then
            OptCaraBayar(0).value = True
        Else
            OptCaraBayar(1).value = True
        End If
        If IsNull(rs("kode_ekspedisi")) = False Then
            txtKodeEkspedisi.text = rs("kode_ekspedisi")
            lblNamaEkspedisi = rs("nama_supplier")
        End If
        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
        Dim pengawas() As String
        Dim kuli() As String
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
            txtNoTruk_KeyDown 13, 0
        Next
        
        pengawas = Split(rs!pengawas, ",")
        listPengawas.Clear
        For A = 0 To UBound(pengawas)
            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
            txtPengawas_KeyDown 13, 0
        Next
        
        kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
'            txtKuli = Replace(Replace(kuli(A), "[", ""), "]", "")
'            txtKuli_KeyDown 13, 0
        Next
        If rs!status_posting = 0 Then
            cmdPosting.Visible = True
        Else
            cmdSimpan.Enabled = False
        End If
       
        totalQty = 0
        totalberat = 0
        total = 0
        If suratjalan = True Then cmdPrint.Enabled = True
        If rs.State Then rs.Close
        If chkAngkut.value = True Then
            rs.Open "select * from t_suratjalan_angkut where nomer_suratjalan='" & lblNoTrans & "' and status=1 ", conn
            If Not rs.EOF Then
                txtKodeEkspedisi = rs!kode_ekspedisi
                cek_ekspedisi
            End If
            If rs.State Then rs.Close
        End If
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1

        rs.Open "select d.nomer_order,d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat,satuan,isi,sap,kode_gudang from t_suratjalan_timbang d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_suratjalan='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = rs(2)
                flxGrid.TextMatrix(Row, 4) = rs(3)
                flxGrid.TextMatrix(Row, 5) = rs(4)
                flxGrid.TextMatrix(Row, 6) = rs(5)
                flxGrid.TextMatrix(Row, 7) = rs!satuan & Space(50) & rs!isi
                flxGrid.TextMatrix(Row, 8) = rs!sap
                SetComboTextRight rs!kode_gudang, cmbGudang
                flxGrid.TextMatrix(Row, 9) = cmbGudang.text
                Row = Row + 1
                totalQty = totalQty + rs(4)
                totalberat = totalberat + rs(5)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        With flxgridRekap
        .Rows = 1
        .Rows = 2
        Row = 1
        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat from t_suratjaland d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_suratjalan='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                .TextMatrix(Row, 1) = rs(0)
                .TextMatrix(Row, 2) = rs(1)
                .TextMatrix(Row, 3) = rs(2)
                .TextMatrix(Row, 4) = rs(3)
                Row = Row + 1
                
                .Rows = .Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        End With
        lblQty = totalQty
        lblBerat = totalberat
        
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Public Sub cek_noso()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_soh where nomer_so='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        
        If rs.State Then rs.Close
        txtKdBrg.Clear
        rs.Open "select distinct(d.[kode_bahan]) from t_sod d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_so='" & txtNoOrder & "' order by d.kode_bahan", conn
        While Not rs.EOF
            txtKdBrg.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub


Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click

    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    load_combo
    
    DTPicker1 = Now
    DTPicker2 = Now
    DTPicker3 = Now
    Load_ListKuli
    If suratjalan = True Then
'        frSuratJalan.Visible = True
        frDetail(0).Enabled = False
        frDetail(1).Enabled = True
        chkAngkut.Visible = True
        frAngkut.Visible = True
'        frHeader.Enabled = False
    Else
        
        cmdPrint.Visible = False
    End If
    loadgrup "0", tvwMain
    addserialsj
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    seltab = 0
'    conn.ConnectionString = strcon
'    conn.Open
'    listKuli.Clear
'    rs.Open "select * from ms_karyawan order by nik", conn
'    While Not rs.EOF
'        listKuli.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Close
    With flxGrid
    .ColWidth(0) = 300
    .ColWidth(1) = 1400
    .ColWidth(2) = 1400
    .ColWidth(3) = 3000
    .ColWidth(4) = 1200
    
    .ColWidth(5) = 900
    .ColWidth(6) = 900
    .ColWidth(7) = 1000
    .ColWidth(8) = 350
    .ColWidth(9) = 1500
    .ColAlignment(9) = 1
    
    .TextMatrix(0, 1) = "No. Order"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .ColAlignment(3) = 1
    .ColAlignment(4) = 1
    .TextMatrix(0, 2) = "Kode Bahan"
    .TextMatrix(0, 3) = "Nama"
    .TextMatrix(0, 4) = "Serial"
    
    .TextMatrix(0, 5) = "Qty"
    .TextMatrix(0, 6) = "Berat"
    .TextMatrix(0, 7) = "Satuan"
    .TextMatrix(0, 8) = "SAP"
    .TextMatrix(0, 9) = "Gudang"
    End With
    With flxgridRekap
    .ColWidth(0) = 300
    .ColWidth(1) = 1400
    .ColWidth(2) = 3000
    .ColWidth(3) = 900
    .ColWidth(4) = 900
    .ColWidth(5) = 1000
    .TextMatrix(0, 1) = "Kode Bahan"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .TextMatrix(0, 2) = "Nama"
    .TextMatrix(0, 3) = "Qty"
    .TextMatrix(0, 4) = "Berat"
    .TextMatrix(0, 5) = "Satuan"
    End With
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub


Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        cmbSatuan.Clear
        cmbSatuan.AddItem rs!satuan1 & Space(50) & rs!isi1
        cmbSatuan.AddItem rs!satuan2 & Space(50) & rs!isi2
        cmbSatuan.AddItem rs!satuan3 & Space(50) & rs!isi3
        cmbSatuan.ListIndex = 1
        cek_kodebarang1 = True
    Else
        LblNamaBarang1 = ""
        cmbSatuan.Clear
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_pengawas()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close
err:
End Sub

Public Sub cek_kuli()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtKuli & "'", conn
    If Not rs.EOF Then
        lblNamaKuli = rs(1)
    Else
        lblNamaKuli = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub

Private Sub hitung_ulang()
'On Error GoTo err
'    conn.Open strcon
'    With flxGrid
'    For i = 1 To .Rows - 2
'        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
'        If Not rs.EOF Then
'            total = total - .TextMatrix(i, 8)
'            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
'            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
'            total = total + .TextMatrix(i, 8)
'        End If
'        rs.Close
'    Next
'    End With
'    conn.Close
'    lblTotal = Format(total, "#,##0")
'    Exit Sub
'err:
'    MsgBox err.Description
'    If conn.State Then conn.Close
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyDelete And listPengawas.ListIndex >= 0 Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And listTruk.ListIndex >= 0 Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    tvwMain.Visible = False
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtBerat = calc(txtBerat)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
'    Angka KeyAscii
    
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
    
    
End Sub


Private Sub txtBiayaAngkutBerat_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtBiayaAngkutKG.SetFocus
End Sub

Private Sub txtBiayaAngkutBerat_Validate(Cancel As Boolean)
    HitungBiayaAngkut
End Sub

Private Sub HitungBiayaAngkut()
    If Trim(txtBiayaAngkutBerat.text) = "" Then txtBiayaAngkutBerat.text = 0
    If Trim(txtBiayaAngkutKG.text) = "" Then txtBiayaAngkutKG.text = 0
    If Trim(txtBiayaAngkut.text) = "" Then txtBiayaAngkut.text = 0
    If Trim(txtBiayaLain.text) = "" Then txtBiayaLain.text = 0
    txtBiayaAngkut.text = CDbl(txtBiayaAngkutBerat.text) * CDbl(txtBiayaAngkutKG.text)
    lblTotalBiaya.Caption = CDbl(txtBiayaAngkut.text) + CDbl(txtBiayaLain.text)
End Sub

Private Sub txtBiayaAngkutKG_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaAngkutBerat_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaAngkut_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaLain_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaAngkutKG_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtBiayaLain.SetFocus
End Sub

Private Sub txtBiayaAngkutKG_Validate(Cancel As Boolean)
    HitungBiayaAngkut
End Sub

Private Sub txtBiayaLain_Validate(Cancel As Boolean)
    HitungBiayaAngkut
End Sub

Private Sub txtBiayaLain_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        HitungBiayaAngkut
        OptCaraBayar(0).SetFocus
    End If
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        Else
            cekserial
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeEkspedisi_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyF3 Then cmdSearchEkspedisi_Click
End Sub
Public Function cek_ekspedisi() As Boolean
'On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier] from ms_supplier where kode_supplier='" & txtKodeEkspedisi & "'", conn
    If Not rs.EOF Then
        lblNamaEkspedisi = rs(0)
        cek_ekspedisi = True
    Else
        lblNamaEkspedisi = ""
        cek_ekspedisi = False
    End If
    rs.Close
    conn.Close
    
err:
End Function

Private Sub txtKodeEkspedisi_LostFocus()
'If txtKodeEkspedisi.text <> "" And Not cek_ekspedisi Then
'        MsgBox "Kode ekspedisi tidak ditemukan"
'        txtKodeEkspedisi.SetFocus
'    End If
End Sub



Private Sub txtNoAngkut_Change()
    If Trim(txtNoAngkut.text) = "" Then reset_angkut
End Sub

Private Sub txtNomerSerial_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSerial_Click
End Sub

Private Sub txtNomerSerial_LostFocus()
    If txtNomerSerial <> "" And Not cekserial Then
        txtNomerSerial.SetFocus
        txtNomerSerial.SelStart = 0
        txtNomerSerial.SelLength = Len(txtNomerSerial)
    End If
End Sub

Private Sub txtNoOrder_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchso_Click
End Sub

Private Sub txtNoOrder_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
End Sub

Private Sub txtNoOrder_LostFocus()
    cek_noso
End Sub

Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPengawas_Click
    If KeyCode = 13 Then
        cek_pengawas
        listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
        txtPengawas = ""
        lblNamaPengawas = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Public Sub reset_angkut()
Dim J As Integer
On Error Resume Next
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    txtKodeEkspedisi = ""
    listPengawas.Clear
    
    listTruk.Clear
    txtSupir = ""
    For J = 0 To listKuli.ListCount - 1
        listKuli.Selected(J) = False
    Next
End Sub
