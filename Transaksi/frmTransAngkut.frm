VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTransAngkut 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Data Angkut"
   ClientHeight    =   7080
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   7875
   FillColor       =   &H00E0E0E0&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7080
   ScaleWidth      =   7875
   Begin VB.TextBox txtSisaUangSaku 
      Height          =   330
      Left            =   5850
      TabIndex        =   17
      Top             =   5775
      Width           =   1710
   End
   Begin VB.CommandButton cmdPosting2 
      Caption         =   "&Posting Kembali"
      Height          =   555
      Left            =   6420
      Picture         =   "frmTransAngkut.frx":0000
      TabIndex        =   64
      Top             =   6330
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.TextBox txtBiayaTimbangan 
      Height          =   360
      Left            =   2025
      TabIndex        =   7
      Top             =   3735
      Width           =   1770
   End
   Begin VB.TextBox txtBiayaKuli 
      Height          =   360
      Left            =   2025
      TabIndex        =   12
      Top             =   5760
      Width           =   1770
   End
   Begin VB.TextBox txtBiayaIjin 
      Height          =   360
      Left            =   2025
      TabIndex        =   11
      Top             =   5355
      Width           =   1770
   End
   Begin VB.TextBox txtBiayaTol 
      Height          =   360
      Left            =   2025
      TabIndex        =   10
      Top             =   4950
      Width           =   1770
   End
   Begin VB.TextBox txtBiayaJalan 
      Height          =   360
      Left            =   2025
      TabIndex        =   9
      Top             =   4545
      Width           =   1770
   End
   Begin VB.TextBox txtBiayaForklift 
      Height          =   360
      Left            =   2025
      TabIndex        =   8
      Top             =   4140
      Width           =   1770
   End
   Begin VB.TextBox txtBiayaMakan 
      Height          =   360
      Left            =   2025
      TabIndex        =   6
      Top             =   3330
      Width           =   1770
   End
   Begin VB.CommandButton cmdCariNopol 
      Caption         =   "F3"
      Height          =   330
      Left            =   3960
      Picture         =   "frmTransAngkut.frx":0102
      TabIndex        =   49
      Top             =   450
      UseMaskColor    =   -1  'True
      Width           =   555
   End
   Begin VB.Frame Frame2 
      Caption         =   "Truk Kembali"
      Height          =   1695
      Left            =   4140
      TabIndex        =   34
      Top             =   1170
      Width           =   3585
      Begin VB.TextBox txtJarakKembali 
         Height          =   330
         Left            =   1740
         TabIndex        =   14
         Top             =   780
         Width           =   1230
      End
      Begin MSComCtl2.DTPicker dtpTglKembali 
         Height          =   345
         Left            =   1740
         TabIndex        =   13
         Top             =   300
         Width           =   1545
         _ExtentX        =   2725
         _ExtentY        =   609
         _Version        =   393216
         Format          =   48824321
         CurrentDate     =   40606
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   240
         Index           =   5
         Left            =   1590
         TabIndex        =   45
         Top             =   810
         Width           =   105
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   225
         Index           =   4
         Left            =   1560
         TabIndex        =   44
         Top             =   360
         Width           =   105
      End
      Begin VB.Label Label4 
         Caption         =   "km"
         Height          =   345
         Left            =   3030
         TabIndex        =   37
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Jarak Kembali"
         Height          =   240
         Index           =   5
         Left            =   240
         TabIndex        =   36
         Top             =   810
         Width           =   1320
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Tgl.Kembali"
         Height          =   240
         Index           =   3
         Left            =   210
         TabIndex        =   35
         Top             =   330
         Width           =   1320
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Truk Keluar"
      Height          =   2025
      Left            =   30
      TabIndex        =   30
      Top             =   840
      Width           =   3945
      Begin VB.TextBox txtUangSaku 
         Height          =   330
         Left            =   1740
         TabIndex        =   4
         Top             =   1530
         Width           =   1710
      End
      Begin VB.TextBox txtTujuan 
         Height          =   330
         Left            =   1740
         TabIndex        =   3
         Top             =   1155
         Width           =   2040
      End
      Begin VB.TextBox txtJarak 
         Height          =   330
         Left            =   1740
         TabIndex        =   2
         Top             =   780
         Width           =   1110
      End
      Begin MSComCtl2.DTPicker dtpTglKeluar 
         Height          =   375
         Left            =   1740
         TabIndex        =   1
         Top             =   360
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   661
         _Version        =   393216
         Format          =   48824321
         CurrentDate     =   40606
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Uang Saku"
         Height          =   240
         Index           =   17
         Left            =   150
         TabIndex        =   66
         Top             =   1560
         Width           =   1320
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   240
         Index           =   16
         Left            =   1560
         TabIndex        =   65
         Top             =   1560
         Width           =   105
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   240
         Index           =   3
         Left            =   1560
         TabIndex        =   43
         Top             =   1185
         Width           =   105
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   240
         Index           =   2
         Left            =   1560
         TabIndex        =   42
         Top             =   810
         Width           =   105
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   240
         Index           =   1
         Left            =   1560
         TabIndex        =   41
         Top             =   390
         Width           =   105
      End
      Begin VB.Label Label6 
         Caption         =   "km"
         Height          =   285
         Left            =   2940
         TabIndex        =   38
         Top             =   810
         Width           =   435
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Tujuan"
         Height          =   240
         Index           =   6
         Left            =   150
         TabIndex        =   33
         Top             =   1185
         Width           =   900
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Jarak Awal"
         Height          =   240
         Index           =   4
         Left            =   150
         TabIndex        =   32
         Top             =   810
         Width           =   1320
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Tgl.Keluar"
         Height          =   240
         Index           =   2
         Left            =   150
         TabIndex        =   31
         Top             =   390
         Width           =   1320
      End
   End
   Begin VB.TextBox txtJmlBensin 
      Height          =   330
      Left            =   5685
      MaxLength       =   10
      TabIndex        =   18
      Top             =   3390
      Width           =   1140
   End
   Begin VB.TextBox txtHrgPerLiter 
      Height          =   330
      Left            =   5685
      TabIndex        =   15
      Top             =   2970
      Width           =   2040
   End
   Begin VB.TextBox txtBiaya 
      Height          =   360
      Left            =   2025
      TabIndex        =   5
      Top             =   2910
      Width           =   1770
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      Height          =   330
      Left            =   3960
      TabIndex        =   25
      Top             =   75
      Width           =   555
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      Height          =   555
      Left            =   3750
      Picture         =   "frmTransAngkut.frx":0204
      TabIndex        =   20
      Top             =   6330
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   555
      Left            =   1080
      Picture         =   "frmTransAngkut.frx":0306
      TabIndex        =   16
      Top             =   6330
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting Berangkat"
      Height          =   555
      Left            =   2415
      Picture         =   "frmTransAngkut.frx":0408
      TabIndex        =   19
      Top             =   6330
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   555
      Left            =   5085
      Picture         =   "frmTransAngkut.frx":050A
      TabIndex        =   21
      Top             =   6330
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.TextBox txtNopol 
      Height          =   330
      Left            =   1845
      TabIndex        =   0
      Top             =   450
      Width           =   2040
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   17
      Left            =   5640
      TabIndex        =   68
      Top             =   5805
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Sisa Uang Saku"
      Height          =   240
      Index           =   18
      Left            =   4170
      TabIndex        =   67
      Top             =   5805
      Width           =   1410
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Kuli"
      Height          =   240
      Index           =   16
      Left            =   225
      TabIndex        =   63
      Top             =   5790
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   15
      Left            =   1815
      TabIndex        =   62
      Top             =   5790
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Ijin"
      Height          =   240
      Index           =   15
      Left            =   225
      TabIndex        =   61
      Top             =   5385
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   14
      Left            =   1815
      TabIndex        =   60
      Top             =   5385
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Tol"
      Height          =   240
      Index           =   14
      Left            =   225
      TabIndex        =   59
      Top             =   4980
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   13
      Left            =   1815
      TabIndex        =   58
      Top             =   4980
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Jalan"
      Height          =   240
      Index           =   13
      Left            =   225
      TabIndex        =   57
      Top             =   4575
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   12
      Left            =   1815
      TabIndex        =   56
      Top             =   4575
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Forklift"
      Height          =   240
      Index           =   12
      Left            =   225
      TabIndex        =   55
      Top             =   4170
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   11
      Left            =   1815
      TabIndex        =   54
      Top             =   4170
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Timbangan"
      Height          =   240
      Index           =   11
      Left            =   225
      TabIndex        =   53
      Top             =   3765
      Width           =   1500
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   10
      Left            =   1815
      TabIndex        =   52
      Top             =   3765
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Makan"
      Height          =   240
      Index           =   10
      Left            =   225
      TabIndex        =   51
      Top             =   3360
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   9
      Left            =   1815
      TabIndex        =   50
      Top             =   3360
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   8
      Left            =   5475
      TabIndex        =   48
      Top             =   3390
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   7
      Left            =   5475
      TabIndex        =   47
      Top             =   2970
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   6
      Left            =   1830
      TabIndex        =   46
      Top             =   2940
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1620
      TabIndex        =   40
      Top             =   465
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Index           =   0
      Left            =   1620
      TabIndex        =   39
      Top             =   105
      Width           =   105
   End
   Begin VB.Label Label2 
      Caption         =   "liter"
      Height          =   315
      Left            =   6975
      TabIndex        =   29
      Top             =   3420
      Width           =   555
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Jmlh.Solar"
      Height          =   240
      Index           =   9
      Left            =   4065
      TabIndex        =   28
      Top             =   3420
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga per liter"
      Height          =   240
      Index           =   8
      Left            =   4065
      TabIndex        =   27
      Top             =   2970
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Biaya Solar"
      Height          =   240
      Index           =   7
      Left            =   240
      TabIndex        =   26
      Top             =   2940
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      AutoSize        =   -1  'True
      Caption         =   "-"
      Height          =   240
      Left            =   1860
      TabIndex        =   24
      Top             =   135
      Width           =   2145
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "ID"
      Height          =   240
      Index           =   1
      Left            =   120
      TabIndex        =   23
      Top             =   105
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Polisi"
      Height          =   240
      Index           =   0
      Left            =   105
      TabIndex        =   22
      Top             =   465
      Width           =   1320
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmTransAngkut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCariNopol_Click()
    frmSearch.query = "select * from ms_truk"
    frmSearch.nmform = "frmMasterTruk"
    frmSearch.nmctrl = "txtNopol"
    frmSearch.keyIni = "ms_truk"
    frmSearch.connstr = strcon
    frmSearch.proc = "Cek_NoPol"
    frmSearch.Index = -1
    frmSearch.col = 0
    txtTujuan.SetFocus
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Public Sub cek_NoPol()
Dim Baru As Boolean
conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 * from t_trukAngkut where nopol='" & txtNopol.text & "' and status_posting='1' and status_kembali='0' order by tanggal_keluar desc  ", conn

    If Not rs.EOF Then
        dtpTglKeluar = rs!tanggal_keluar
        txtJarak = rs!Km_keluar
        txtUangSaku = rs!uang_saku
        txtTujuan = rs!tujuan
        cmdPosting.Enabled = False
        cmdPosting2.Enabled = True
    Else
        Baru = True
    End If
    If rs.State Then rs.Close
    
    If Baru Then
        rs.Open "select top 1 km_kembali,tanggal_kembali from t_trukAngkut where nopol='" & txtNopol.text & "' and status_kembali='1' order by tanggal_kembali desc  ", conn
        If Not rs.EOF Then
            txtJarak = rs!Km_kembali
            dtpTglKeluar.value = rs!tanggal_kembali
            dtpTglKembali.value = DateAdd("d", 1, rs!tanggal_kembali)
        End If
        If rs.State Then rs.Close
    End If
    conn.Close
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPosting_Click()
If simpan Then
    postingTrukBerangkat lblNoTrans, True
'    reset_form
End If
End Sub

Private Sub cmdPosting2_Click()
If simpan Then
    postingTrukKembali lblNoTrans, True
    reset_form
End If
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select * from t_trukAngkut"
    frmSearch.nmform = "frmTransAngkut"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.keyIni = "t_trukAngkut"
    frmSearch.connstr = strcon
    frmSearch.proc = "cek_notrans"
    frmSearch.Index = -1
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub dtpTglKembali_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtJarakKembali.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyF3 Then cmdCariNopol_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    reset_form
    dtpTglKeluar = Now
    dtpTglKembali = Now
End Sub

Private Sub mnuExit_Click()
Unload Me
End Sub

Private Sub mnuOpen_Click()
  cmdSearch_Click
End Sub
Private Sub reset_form()
    txtNopol = ""
    txtJarak = 0
    txtJarakKembali = 0
    txtJmlBensin = 0
    txtBiaya = 0
    txtBiayaMakan = 0
    txtBiayaForklift = 0
    txtBiayaTimbangan = 0
    txtBiayaIjin = 0
    txtBiayaKuli = 0
    txtBiayaTol = 0
    txtBiayaJalan = 0
    txtHrgPerLiter = 0
    txtTujuan = ""
    lblNoTrans = "-"
    txtUangSaku.text = 0
    txtSisaUangSaku.text = 0
End Sub
Public Sub cek_notrans()
conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_trukAngkut where nomer_truk_keluar='" & lblNoTrans & "' ", conn

    If Not rs.EOF Then
        txtNopol = rs!nopol
        dtpTglKeluar = rs!tanggal_keluar
        dtpTglKembali = rs!tanggal_kembali
        txtJarak = rs!Km_keluar
        txtJarakKembali = rs!Km_kembali
        txtTujuan = rs!tujuan
        txtBiaya = rs!biaya_solar
        txtHrgPerLiter = rs!harga_liter
        txtJmlBensin = rs!liter
        
        txtBiayaMakan = rs!biaya_makan
        txtBiayaTimbangan = rs!biaya_timbangan
        txtBiayaForklift = rs!biaya_forklift
        txtBiayaJalan = rs!biaya_jalan
        txtBiayaTol = rs!biaya_tol
        txtBiayaIjin = rs!biaya_ijin
        txtBiayaKuli = rs!biaya_kuli
        txtUangSaku.text = rs!uang_saku
        txtSisaUangSaku.text = rs!sisa_uang_saku
        If rs!status_posting = "1" Then cmdPosting.Enabled = False Else cmdPosting.Enabled = True
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Private Function simpan() As Boolean
Dim i As Integer
On Error GoTo err
simpan = False
    
i = 0
    If txtNopol.text = "" Then
        MsgBox "No.Polisi harus diisi"
        txtNopol.SetFocus
        Exit Function
    End If
    hitungSolar
   
    conn.Open strcon
    conn.BeginTrans
    If lblNoTrans <> "-" Then
        conn.Execute "delete from t_trukAngkut where nomer_truk_keluar='" & lblNoTrans & "'"
    Else
        lblNoTrans = newid("t_trukAngkut", "nomer_truk_keluar", Now, "TRK")
    End If
    add_dataheader

    
    conn.CommitTrans
    conn.Close
    simpan = True
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    MsgBox err.Description
End Function
Private Sub add_dataheader()
 Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(21)
    ReDim nilai(21)
    table_name = "t_trukAngkut"
    fields(0) = "nomer_truk_keluar"
    fields(1) = "tanggal_keluar"
    fields(2) = "tanggal_kembali"
    fields(3) = "nopol"
    fields(4) = "km_keluar"
    fields(5) = "km_kembali"
    fields(6) = "tujuan"
    fields(7) = "total_km"
    fields(8) = "biaya_solar"
    fields(9) = "harga_liter"
    fields(10) = "liter"
    fields(11) = "userid"
    fields(12) = "biaya_makan"
    fields(13) = "biaya_timbangan"
    fields(14) = "biaya_forklift"
    fields(15) = "biaya_jalan"
    fields(16) = "biaya_tol"
    fields(17) = "biaya_ijin"
    fields(18) = "biaya_kuli"
    fields(19) = "uang_saku"
    fields(20) = "sisa_uang_saku"

    nilai(0) = lblNoTrans
    nilai(1) = Format(dtpTglKeluar, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = Format(dtpTglKembali, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = txtNopol.text
    nilai(4) = Replace(txtJarak.text, ",", ".")
    nilai(5) = Replace(txtJarakKembali.text, ",", ".")
    nilai(6) = txtTujuan.text
    nilai(7) = Replace(CDbl(txtJarakKembali) - CDbl(txtJarak), ",", ".")
    nilai(8) = Replace(Format(txtBiaya, "###0"), ",", ".")
    nilai(9) = Replace(Format(txtHrgPerLiter, "###0"), ",", ".")
    nilai(10) = Replace(Format(txtJmlBensin, "###0.#0"), ",", ".")
    nilai(11) = User
    nilai(12) = Format(txtBiayaMakan, "###0")
    nilai(13) = Format(txtBiayaTimbangan, "###0")
    nilai(14) = Format(txtBiayaForklift, "###0")
    nilai(15) = Format(txtBiayaJalan, "###0")
    nilai(16) = Format(txtBiayaTol, "###0")
    nilai(17) = Format(txtBiayaIjin, "###0")
    nilai(18) = Format(txtBiayaKuli, "###0")
    nilai(19) = Format(txtUangSaku, "###0")
    nilai(20) = Format(txtSisaUangSaku, "###0")
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub hitungSolar()
    If IsNumeric(txtBiaya) And IsNumeric(txtHrgPerLiter) And txtBiaya <> 0 And txtHrgPerLiter <> 0 Then
        txtJmlBensin = Format(txtBiaya / txtHrgPerLiter, "###0.#0")
    Else
        txtJmlBensin = 0
    End If
End Sub

Private Sub txtBiaya_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiaya_LostFocus()
    hitungSolar
    HitungSisaUangSaku
End Sub


Private Sub txtBiayaForklift_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaForklift_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtBiayaIjin_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaIjin_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtBiayaJalan_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaJalan_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtBiayaKuli_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaKuli_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtBiayaMakan_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaMakan_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtBiayaTimbangan_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaTimbangan_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtBiayaTol_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtBiayaTol_LostFocus()
    HitungSisaUangSaku
End Sub

Private Sub txtHrgPerLiter_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtHrgPerLiter_LostFocus()
    hitungSolar
End Sub

Private Sub HitungSisaUangSaku()
    txtSisaUangSaku.text = Val(txtUangSaku.text) - (Val(txtBiaya.text) + Val(txtBiayaMakan.text) + Val(txtBiayaTimbangan.text) + Val(txtBiayaForklift.text) + Val(txtBiayaJalan.text) + Val(txtBiayaTol.text) + Val(txtBiayaIjin.text) + Val(txtBiayaKuli.text))
End Sub

Private Sub txtJarakKembali_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtJmlBensin_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtNopol_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdCariNopol_Click
End Sub

Private Sub txtSisaUangSaku_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub

Private Sub txtUangSaku_GotFocus()
    SendKeys "{HOME}+{END}"
End Sub
