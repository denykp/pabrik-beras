VERSION 5.00
Begin VB.Form frmNewUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New User"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   7095
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   7095
   Begin VB.ComboBox cmbGroup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1170
      TabIndex        =   1
      Text            =   "Combo1"
      Top             =   540
      Width           =   2130
   End
   Begin VB.CommandButton cmdDel 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2880
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1035
      Width           =   1125
   End
   Begin VB.CommandButton cmdSearchID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4140
      Picture         =   "frmNewUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4455
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1035
      Width           =   1125
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1260
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1035
      Width           =   1125
   End
   Begin VB.TextBox txtUserID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1155
      TabIndex        =   0
      Top             =   180
      Width           =   2895
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Group :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   7
      Top             =   585
      Width           =   930
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   5
      Top             =   210
      Width           =   975
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmNewUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Dim asc1 As New Cls_security
Dim seltab As Byte

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdDel_Click()
Dim i As Byte
On Error GoTo err
    If txtUserID = "" Then Exit Sub
    i = 0
    conn.ConnectionString = strcon
    conn.Open
'    conn.BeginTrans
    i = 1
    conn.Execute "delete from userid where userid='" & txtUserID.text & "'"
    conn.Execute "delete from user_group where nama_group='" & cmbGroup.text & "' and userid='" & txtUserID.text & "'"
    asc1.deluser txtUserID.text, conn
    asc1.dellogin txtUserID.text, conn
'    conn.CommitTrans
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
'    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
Dim result, result1, result2 As String
Dim counter As Byte
    If txtUserID.text <> "" Then


        conn.ConnectionString = strcon
        conn.Open
        rs.Open "select * from userid where userid='" & txtUserID.text & "'", conn
        If Not rs.EOF Then
            rs.Close
            GoTo B
        Else
        asc1.deluser txtUserID.text, conn
        asc1.dellogin txtUserID.text, conn
        End If
        rs.Close
        conn.BeginTrans
        conn.Execute "CREATE LOGIN " & txtUserID & " WITH PASSWORD = '" & txtUserID & "', DEFAULT_DATABASE= " & dbname1 & ", CHECK_POLICY = OFF "

    '    con.Execute "exec sp_addlogin '" & username & "','" & password & "','" & database & "'"
        Set rs = conn.Execute("select * from userid where userid='" & txtUserID & "'")
        If Not rs.EOF Then
            conn.Execute "delete from userid where userid='" & txtUserID & "'"
        End If
        rs.Close
        conn.Execute "insert into [userid] values ('" & txtUserID & "')"

        addlogin = "1"
        conn.CommitTrans
        result1 = asc1.addlogin(txtUserID, txtUserID, dbname1, conn)
        result1 = asc1.adduser(txtUserID.text, conn)

B:
        On Error Resume Next
        conn.Execute "EXEC sp_droprolemember 'db_securityadmin', '" & txtUserID.text & "'"
        conn.Execute "EXEC sp_droprolemember 'db_backupoperator', '" & txtUserID.text & "'"
        conn.Execute "delete from user_group where userid='" & txtUserID.text & "'"
        conn.Execute "insert into user_group values('" & txtUserID.text & "','" & cmbGroup.text & "')"
'        conn.Execute "delete from user_gudang where userid='" & txtUserID.Text & "'"
'        conn.Execute "insert into user_gudang select '" & txtUserID.Text & "',gudang from ms_gudang"
        rs.Open "select * from var_usergroup where [group]='" & cmbGroup.text & "'", conn
        If Not rs.EOF Then
            If rs("adduser") Then
                conn.Execute "exec sp_addrolemember 'db_securityadmin', '" & txtUserID.text & "'"
                conn.Execute "EXEC master.dbo.sp_addsrvrolemember @loginame = N'" & txtUserID.text & "', @rolename = N'sysadmin'"
            End If
            
            'If rs("backuprestore") Then conn.Execute "exec sp_addrolemember 'db_backupoperator', '" & txtUserID.text & "'"
        End If
        rs.Close
        conn.Execute "exec sp_addrolemember 'db_owner', '" & txtUserID.text & "'"
        conn.Close
        If result = "1" Or result = "" Then
            MsgBox "New User Added"
            reset
        Else
            MsgBox result
        End If

    Else
        MsgBox "semua field harus diisi"
    End If
End Sub

Private Sub reset()
    txtUserID.text = ""

End Sub


Private Sub cmdSearchID_Click()
    frmSearch.query = "Select * from userid"
    frmSearch.nmform = "frmNewUser"
    frmSearch.nmctrl = "txtUserID"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "userid"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_data"
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.top = 800
    frmSearch.Left = 70
    frmSearch.Show vbModal

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then Unload Me

End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()

    conn.ConnectionString = strcon
    conn.Open
    cmbGroup.Clear
    rs.Open "select * from var_usergroup", conn
    While Not rs.EOF
        cmbGroup.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
'    rs.Open "select * from ms_gudang", conn
'    While Not rs.EOF
'        DBGrid.AddItem "#" & rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close

    conn.Close
    seltab = 0
End Sub

Private Sub mnuCancel_Click()
Unload Me
End Sub

Private Sub mnuok_Click()
cmdOk_Click
End Sub

Public Sub cari_data()
    conn.ConnectionString = strcon
    conn.Open
    If txtUserID.text <> "" Then
    rs.Open "select * from userid where userid='" & txtUserID.text & "'", conn
    If Not rs.EOF Then
        rs.Close
        rs.Open "select nama_group from user_group where userid='" & txtUserID.text & "'", conn
        If Not rs.EOF Then
            SetComboText rs(0), cmbGroup
        Else
            cmbGroup.ListIndex = -1
        End If
        rs.Close
'
    End If
    If rs.State Then rs.Close
    End If
    conn.Close
End Sub

Private Sub txtUserID_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then cari_data
End Sub

Private Sub txtUserID_LostFocus()
    cari_data
End Sub
Private Sub reset_form()
txtUserID = ""
cmbGroup.text = ""
End Sub
