VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT3N.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmTransStockOpname 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stock Opname"
   ClientHeight    =   7770
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10875
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   10875
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4230
      Picture         =   "frmTransStockOpname.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5325
      Picture         =   "frmTransStockOpname.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3030
      Left            =   1590
      TabIndex        =   37
      Top             =   2280
      Visible         =   0   'False
      Width           =   3210
      _ExtentX        =   5662
      _ExtentY        =   5345
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   6
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Left            =   90
      TabIndex        =   25
      Top             =   1665
      Width           =   9825
      Begin VB.ComboBox cmbGudang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   225
         Width           =   2505
      End
      Begin VB.CommandButton cmdopentreeview 
         Appearance      =   0  'Flat
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4050
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   39
         Top             =   270
         Width           =   330
      End
      Begin VB.TextBox txtBerat 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3705
         TabIndex        =   7
         Top             =   1470
         Width           =   1140
      End
      Begin VB.TextBox txtHPP 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6345
         TabIndex        =   8
         Top             =   1440
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.CommandButton cmdHistoryHpp 
         Caption         =   "History"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7560
         TabIndex        =   27
         Top             =   1440
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.ComboBox cmbSerial 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1500
         TabIndex        =   5
         Text            =   "cmbSerial"
         Top             =   1065
         Width           =   2475
      End
      Begin VB.CommandButton cmdSearchBarang 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4050
         TabIndex        =   26
         Top             =   675
         Width           =   375
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1485
         TabIndex        =   6
         Top             =   1470
         Width           =   1140
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2760
         TabIndex        =   11
         Top             =   1890
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1650
         TabIndex        =   10
         Top             =   1890
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   1890
         Width           =   1350
      End
      Begin VB.TextBox txtKode 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1515
         TabIndex        =   4
         Top             =   660
         Width           =   2460
      End
      Begin Crystal.CrystalReport CRPrint 
         Left            =   6165
         Top             =   690
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   348160
         PrintFileLinesPerPage=   60
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Gudang :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   40
         Top             =   270
         Width           =   1320
      End
      Begin VB.Label Label4 
         BackColor       =   &H8000000C&
         Caption         =   "Berat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3090
         TabIndex        =   38
         Top             =   1515
         Width           =   600
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         Caption         =   "Harga Beli"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5040
         TabIndex        =   36
         Top             =   1485
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         Caption         =   "No. Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   35
         Top             =   1110
         Width           =   1170
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H8000000C&
         Caption         =   "Satuan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   6210
         TabIndex        =   33
         Top             =   1800
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label Label6 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   32
         Top             =   1500
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4185
         TabIndex        =   31
         Top             =   1935
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   30
         Top             =   1905
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   29
         Top             =   720
         Width           =   1050
      End
      Begin VB.Label LblNamaBarang 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   4530
         TabIndex        =   28
         Top             =   720
         Width           =   1155
      End
      Begin VB.Label lblStock 
         BackColor       =   &H8000000C&
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   4995
         TabIndex        =   34
         Top             =   1800
         Width           =   2160
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3990
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   7125
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   7125
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      MaxLength       =   80
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1005
      Width           =   5910
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1605
      TabIndex        =   0
      Top             =   150
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   -120
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   6975
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   19
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5220
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   7125
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1500
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   7125
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   1605
      TabIndex        =   1
      Top             =   585
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   98238467
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2850
      Left            =   135
      TabIndex        =   20
      Top             =   4020
      Width           =   10590
      _ExtentX        =   18680
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   9
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8220
      Top             =   1440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label8 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   24
      Top             =   1035
      Width           =   1275
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   23
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8025
      TabIndex        =   22
      Top             =   6690
      Visible         =   0   'False
      Width           =   2400
   End
   Begin VB.Label label36 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7155
      TabIndex        =   21
      Top             =   7005
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   18
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   17
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransStockOpname"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mode As Byte
Dim total As Long
Dim totalHpp As Double

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String

Private Sub cmdClear_Click()
    txtKode.text = ""
    lblNamaBarang = ""
    cmbSerial.Clear
    lblSatuan = ""
    txtQty.text = "0"
    txtBerat.text = "0"
    txtHpp = "0"
    lblStock = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    Row = flxGrid.Row
    If flxGrid.Rows <= 2 Then Exit Sub
'    total = total - (flxGrid.TextMatrix(row, 6))
'    lblTotal = Format(total, "#,##0")
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdHistoryHpp_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select tipe,id,kode_bahan,nomer_serial,hpp from tmp_kartustock where kode_bahan='" & txtKode & "' and masuk>0"
    frmSearch.nmform = "frmTransStockOpname"
    frmSearch.nmctrl = "txtHPP"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "history_hpp"
    frmSearch.Col = 4
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtQty.SetFocus
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_opname] from t_stockopnameh where [nomer_opname]>'" & lblNoTrans & "' order by [nomer_opname]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtKode.text <> "" And IsNumeric(txtHpp) Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKode.text) And flxGrid.TextMatrix(i, 3) = cmbSerial.text Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(Row, 1) = txtKode.text
        flxGrid.TextMatrix(Row, 2) = lblNamaBarang
        
        If Trim(cmbSerial.text) = "" Then
            cmbSerial.text = txtKode.text
        End If
        
        flxGrid.TextMatrix(Row, 3) = cmbSerial.text
        
        
        If flxGrid.TextMatrix(Row, 1) = txtKode.text Then
             flxGrid.TextMatrix(Row, 4) = txtQty.text
        Else
            If mode = 1 Then
                flxGrid.TextMatrix(Row, 4) = CDbl(flxGrid.TextMatrix(Row, 4)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(Row, 4) = CDbl(txtQty)
            End If
        End If
        
       
        flxGrid.TextMatrix(Row, 5) = lblSatuan
        flxGrid.TextMatrix(Row, 6) = txtBerat.text
        conn.Open strcon
        flxGrid.TextMatrix(Row, 7) = GetHPPKertas2(txtKode.text, cmbSerial.text, Trim(Right(cmbGudangKemasan, 10)), conn)
        conn.Close
        flxGrid.TextMatrix(Row, 8) = cmbGudang.text
        
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = 4
        
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtKode.SetFocus
        
    End If
    mode = 1

End Sub



Private Sub cmdopentreeview_Click()
On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub cmdPosting_Click()
'conn.Open strcon
'conn.BeginTrans
'i = 1
'    cmdSimpan_Click
    If simpan Then
        postingOpname lblNoTrans, True
        reset_form
    End If
'    If frmTransPostingManual.Visible Then Unload Me
'conn.CommitTrans
'i = 0
'DropConnection
'MsgBox "Data sudah terposting"
'MySendKeys "{tab}"
'Exit Sub
'err:
'    If i = 1 Then
'        conn.RollbackTrans
'    End If
'    DropConnection
'    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_opname] from t_stockopnameh where [nomer_opname]<'" & lblNoTrans & "' order by [nomer_opname] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
    cetaknota lblNoTrans
End Sub



Private Sub cmdReset_Click()
reset_form
End Sub

Private Sub cmdSearchBarang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_bahan ,nama_bahan,kategori,subkategori,grup,subgrup from ms_bahan "
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_barang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
'    txtQty.SetFocus
End Sub

Public Sub cari_barang()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select kode_bahan,nama_bahan from ms_bahan where kode_bahan='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        lblNamaBarang = rs(1)
        txtQty.text = 1

'        lblStock = GetStockKertasVia(txtKode.text, "", cmbGudang.text, conn)
    Else
        lblNamaBarang = ""
        txtQty.text = 0

        lblStock = "0"
        txtKode.SetFocus
    End If
    rs.Close
    
    cmbSerial.Clear
    If cmbGudang.text <> "" Then
    rs.Open "select nomer_serial from stock where kode_bahan='" & txtKode.text & "' and kode_gudang='" & Trim(Right(cmbGudang.text, 10)) & "' ", conn
        While Not rs.EOF
            cmbSerial.AddItem rs(0)
            cmbSerial.Refresh
            rs.MoveNext
        Wend
    rs.Close
    End If
    conn.Close
End Sub


Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
'    frmSearch.query = "Select t.nomer_opname,t.tanggal,t.keterangan from t_stockopnameh t  where t.status_posting=0"
    frmSearch.query = "Select t.nomer_opname,t.tanggal,t.keterangan from t_stockopnameh t  "
    frmSearch.nmform = "frmTransStockOpname"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "opnameKertas"
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    

    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan item detail terlebih dahulu"
        txtKeterangan.SetFocus
        Exit Function
    End If
    id = lblNoTrans
    conn.Open strcon
    
    conn.BeginTrans
    
    i = 1
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_stockopnameh", "nomer_opname", DTPicker1, "SOP")
    Else
        conn.Execute "delete from t_stockopnameh where nomer_opname='" & lblNoTrans & "'"
        conn.Execute "delete from t_stockopnamed where nomer_opname='" & lblNoTrans & "'"
    End If
    
    add_dataheader
    
    
    totalHpp = 0
    For Row = 1 To flxGrid.Rows - 2
            add_datadetail (Row), conn
'            conn.Execute "If not Exists(select * from stock where kode_bahan='" & flxGrid.TextMatrix(row, 1) & "' and nomer_serial='" & flxGrid.TextMatrix(row, 3) & "' and kode_gudang='" & cmbGudang.text & "') insert into stock (kode_bahan,nomer_serial,stock,kode_gudang) values('" & flxGrid.TextMatrix(row, 1) & "','" & flxGrid.TextMatrix(row, 3) & "',0,'" & cmbGudang.text & "') "
    'conn.Execute "If not Exists(select * from stock where kode_bahan='" & flxGrid.TextMatrix(row, 1) & "') and nomer_serial='" & flxGrid.TextMatrix(row, 3) & "' update stock set stock=stock+ '" & CLng(flxGrid.TextMatrix(i, 3)) & "' where kode_bahan='" & flxGrid.TextMatrix(i, 5) & "'   else insert into stock values('" & flxGrid.TextMatrix(i, 5) & "','" & flxGrid.TextMatrix(i, 3) & "') "
    Next Row
    
    conn.CommitTrans
    DropConnection
    simpan = True
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    MsgBox err.Description
End Function

Public Sub reset_form()
    lblNoTrans = "-"
    lblTotal = "0"
    txtKeterangan.text = ""
    total = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    txtQty = 0
    cmdPrint.Visible = False
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
'    cmdPosting.Enabled = False
End Sub

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(4)
    ReDim nilai(4)
    table_name = "t_stockopnameh"
    fields(0) = "nomer_opname"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    
    fields(3) = "userid"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKeterangan.text
    
    nilai(3) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer, conn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim selisih As Double
    
    ReDim fields(13)
    ReDim nilai(13)
    
    table_name = "t_stockopnamed"
    fields(0) = "nomer_opname"
    fields(1) = "kode_bahan"
    fields(2) = "nomer_serial"
    fields(3) = "qty"
    fields(4) = "stock_qty"
    fields(5) = "stock_berat"
    fields(6) = "selisih_qty"
    fields(7) = "selisih_berat"
    fields(8) = "no_urut"
    fields(9) = "hpp"
    fields(10) = "hpp_baru"
    fields(11) = "berat"
    fields(12) = "kode_gudang"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = flxGrid.TextMatrix(Row, 3)
    nilai(3) = Replace(flxGrid.TextMatrix(Row, 4), ",", ".")
    nilai(10) = Replace(Format(flxGrid.TextMatrix(Row, 7), "###0.##"), ",", ".")
    nilai(11) = Replace(Format(flxGrid.TextMatrix(Row, 6), "###0.##"), ",", ".")
    nilai(12) = Trim(Right(flxGrid.TextMatrix(Row, 8), 10))
    
    
    
    nilai(4) = Replace(getstockKartu(nilai(1), nilai(12), nilai(2), conn), ",", ".")
    nilai(5) = Replace(getQty(nilai(1), nilai(12), nilai(2), conn), ",", ".")
    selisih = CDbl(nilai(3)) - CDbl(nilai(4))
    nilai(6) = Replace(selisih, ",", ".")
    selisih = CDbl(nilai(11)) - CDbl(nilai(5))
    nilai(7) = Replace(selisih, ",", ".")
    nilai(8) = Row
    nilai(9) = Replace(Format(GetHPPKertasVia(nilai(1), nilai(2), nilai(12), conn), "###0.##"), ",", ".")
    
     conn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim StatusPosting As String
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_stockopnameh t " & _
            "where t.nomer_opname='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        
        total = 0
        
        StatusPosting = rs("status_posting")
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        
        If StatusPosting = "1" Then
            cmdSimpan.Enabled = False
            cmdPosting.Enabled = False
        Else
            cmdSimpan.Enabled = True
            cmdPosting.Enabled = True
        End If
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1
        
        rs.Open "select d.kode_bahan,b.nama_bahan,d.qty,d.nomer_serial,d.hpp_baru,d.berat,kode_gudang " & _
                "from t_stockopnamed d inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                "where d.nomer_opname='" & lblNoTrans & "' order by d.no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = rs(3) ' no.serial
                flxGrid.TextMatrix(Row, 4) = rs(2) 'qty inputan
'                flxGrid.TextMatrix(row, 6) = GetStockKertas(flxGrid.TextMatrix(row, 3), conn)
                flxGrid.TextMatrix(Row, 7) = rs!hpp_baru
                flxGrid.TextMatrix(Row, 6) = rs!berat
                SetComboTextRight rs("kode_gudang"), cmbGudang
                flxGrid.TextMatrix(Row, 8) = cmbGudang.text
                Row = Row + 1
                
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(total, "#,##0")

    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKode.SetFocus
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKode.text = flxGrid.TextMatrix(flxGrid.Row, 1)
        lblNamaBarang = flxGrid.TextMatrix(flxGrid.Row, 2)
        SetComboText flxGrid.TextMatrix(flxGrid.Row, 3), cmbSerial
        txtQty.text = flxGrid.TextMatrix(flxGrid.Row, 4)
        txtHpp = flxGrid.TextMatrix(flxGrid.Row, 7)
        lblSatuan = flxGrid.TextMatrix(flxGrid.Row, 5)
        txtBerat.text = flxGrid.TextMatrix(flxGrid.Row, 6)
        SetComboText flxGrid.TextMatrix(flxGrid.Row, 8), cmbGudang
'        txtKode.SetFocus
    
    Exit Sub
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            flxGrid_Click
            txtKode.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
'    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    load_combo
    loadgrup "0", tvwMain
    
    lblNamaBarang = ""
    lblSatuan = ""
    lblStock = ""

    reset_form
    total = 0
   
'    lblGudang = gudang
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1200
    flxGrid.ColWidth(2) = 2500
    flxGrid.ColWidth(3) = 1300 ' serial
    flxGrid.ColWidth(4) = 800
    flxGrid.ColWidth(5) = 0
    flxGrid.ColWidth(6) = 800
    flxGrid.ColWidth(7) = 0
    flxGrid.ColWidth(8) = 1500
    flxGrid.ColAlignment(8) = 1
    flxGrid.TextMatrix(0, 1) = "Kode Barang"
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 3) = "Serial"
    flxGrid.TextMatrix(0, 4) = "Qty"
    flxGrid.TextMatrix(0, 5) = "Satuan"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "HPP"
    flxGrid.TextMatrix(0, 8) = "Gudang"
    
    
End Sub

Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    tvwMain.Visible = False
End Sub

Private Sub txtKeterangan_GotFocus()
    txtKeterangan.SelStart = 0
    txtKeterangan.SelLength = Len(txtKeterangan.text)
    foc = 1
End Sub



Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
'If txtKode <> "" Then cari_barang
End Sub

Private Sub txtKode_LostFocus()
    If txtKode <> "" Then cari_barang
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

'Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then cmdOk.SetFocus
'End Sub

'Private Sub txtQty_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        txtQty.text = calc(txtQty.text)
'    End If
'End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Public Sub cetaknota(no_nota As String)
On Error GoTo err
Dim query() As String
Dim rs() As New ADODB.Recordset


    
    conn.Open strcon
    
    ReDim query(2) As String
    query(0) = "select * from t_stockopnamed"
    query(1) = "select * from t_stockopnameh"
    query(2) = "select * from ms_bahan"
    
    

    ReDim rs(UBound(query) - 1)
    For i = 0 To UBound(query) - 1
        rs(i).Open query(i), conn, adOpenForwardOnly
    Next
    With CRPrint
        .reset
        .ReportFileName = reportDir & "\PO StockOpname.rpt"
        For i = 0 To UBound(query) - 1
            .SetTablePrivateData i, 3, rs(i)
        Next
        .Formulas(0) = "NamaPerusahaan='" & GNamaPerusahaan & "'"
        .Formulas(1) = "AlamatPerusahaan='" & GAlamatPerusahaan & "'"
        .Formulas(2) = "TeleponPerusahaan='" & GTeleponPerusahaan & "'"

        .SelectionFormula = "{t_stockopnameh.ID}='" & no_nota & "'"
'        .PrinterSelect
        .ProgressDialog = False
        .WindowState = crptMaximized
'        .Destination = crptToPrinter
        .action = 1
    End With
    conn.Close
    Exit Sub
err:
MsgBox err.Description
End Sub
Private Function cariStock(kode As String, Serial As String)

End Function
