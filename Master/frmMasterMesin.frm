VERSION 5.00
Begin VB.Form frmMasterMesin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mesin"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6345
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   6345
   Begin VB.CheckBox chkKomposisi 
      Caption         =   "Merubah Komposisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   135
      TabIndex        =   3
      Top             =   1845
      Width           =   2625
   End
   Begin VB.ComboBox cmbkategori 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterMesin.frx":0000
      Left            =   1890
      List            =   "frmMasterMesin.frx":0002
      TabIndex        =   2
      Top             =   1350
      Width           =   3885
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3315
      Picture         =   "frmMasterMesin.frx":0004
      TabIndex        =   6
      Top             =   3060
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtNama 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1890
      MaxLength       =   35
      TabIndex        =   1
      Top             =   975
      Width           =   3840
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4650
      Picture         =   "frmMasterMesin.frx":0106
      TabIndex        =   7
      Top             =   3060
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1995
      Picture         =   "frmMasterMesin.frx":0208
      TabIndex        =   5
      Top             =   3060
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3360
      Picture         =   "frmMasterMesin.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   585
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtmesin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1890
      MaxLength       =   10
      TabIndex        =   0
      Top             =   585
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   660
      Picture         =   "frmMasterMesin.frx":040C
      TabIndex        =   4
      Top             =   3060
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label18 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   17
      Top             =   1380
      Width           =   105
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   1380
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Mesin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   1005
      Width           =   1260
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   14
      Top             =   1020
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   13
      Top             =   1020
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   2610
      Width           =   2130
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   11
      Top             =   630
      Width           =   150
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1695
      TabIndex        =   9
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Mesin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   630
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterMesin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0

    If txtMesin.text = "" Then
        MsgBox "Silahkan masukkan Kode mesin terlebih dahulu!", vbCritical
        txtMesin.SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
   
    
    
    conn.Execute "delete from ms_mesin where kode_mesin='" & txtMesin.text & "'"
    MsgBox "Data mesin sudah terhapus"

    conn.CommitTrans
    i = 0
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = "select * from ms_mesin"
    frmSearch.nmform = "frmMastermesin"
    frmSearch.nmctrl = "txtmesin"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_mesin"
    frmSearch.proc = "cari_data"
    frmSearch.Col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtMesin.text = "" Then
        MsgBox "Kode mesin harus diisi"
        txtMesin.SetFocus
        Exit Sub
    End If
    
    If txtNama.text = "" Then
        MsgBox "Nama mesin harus diisi"
        txtNama.SetFocus
        Exit Sub
    End If
        
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    
    
    conn.Execute "delete from ms_mesin where kode_mesin='" & txtMesin & "'"
    
    add_dataheader
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtMesin.text = ""
    txtNama.text = ""
    
    txtMesin.SetFocus
    chkKomposisi.value = 0
    Frame1.Visible = False
End Sub
Public Sub cari_data()
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_mesin where kode_mesin='" & txtMesin.text & "'", conn
    If Not rs.EOF Then
        txtNama.text = rs(1)
        cmbKategori = rs!kategori
        chkKomposisi = rs!status
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        txtNama.text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    load_combo
    If Not right_deletemaster Then cmdHapus.Visible = False
'    reset_form
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbKategori.Clear
    rs.Open "select distinct kategori from ms_mesin order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
End Sub
Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub




Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "ms_mesin"
    fields(0) = "kode_mesin"
    fields(1) = "nama_mesin"
    fields(2) = "kategori"
    fields(3) = "status"
    
    nilai(0) = txtMesin.text
    nilai(1) = txtNama.text
    nilai(2) = cmbKategori.text
    nilai(3) = chkKomposisi.value
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub txtmesin_LostFocus()
    If txtMesin <> "" Then cari_data
End Sub
