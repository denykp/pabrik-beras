VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPenjualan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Penjualan"
   ClientHeight    =   8955
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   17505
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8955
   ScaleWidth      =   17505
   Begin VB.TextBox txtPPN 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13950
      TabIndex        =   38
      Text            =   "0"
      Top             =   7920
      Width           =   600
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6855
      Picture         =   "frmAddPenjualan.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5640
      Picture         =   "frmAddPenjualan.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   36
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Post All"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   14985
      Picture         =   "frmAddPenjualan.frx":0A64
      TabIndex        =   35
      Top             =   135
      Width           =   960
   End
   Begin VB.TextBox txtUangMuka 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   10005
      TabIndex        =   33
      Text            =   "0"
      Top             =   8415
      Width           =   2175
   End
   Begin VB.CommandButton cmdSearchPosting 
      Caption         =   "Search Post"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4185
      Picture         =   "frmAddPenjualan.frx":0B66
      TabIndex        =   32
      Top             =   105
      Width           =   1320
   End
   Begin VB.CommandButton cmdPosting2 
      Caption         =   "Posting Final"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   7065
      Picture         =   "frmAddPenjualan.frx":0C68
      TabIndex        =   31
      Top             =   8055
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchCustomer 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3150
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   30
      Top             =   990
      Width           =   420
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5715
      Picture         =   "frmAddPenjualan.frx":0D6A
      TabIndex        =   29
      Top             =   8055
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchPO 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3465
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   26
      Top             =   570
      Width           =   420
   End
   Begin VB.Frame frJT 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   420
      Left            =   5535
      TabIndex        =   23
      Top             =   1380
      Width           =   3480
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   1530
         TabIndex        =   24
         Top             =   45
         Width           =   1875
         _ExtentX        =   3307
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   326041603
         CurrentDate     =   38927
      End
      Begin VB.Label Label9 
         Caption         =   "Jatuh Tempo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   45
         TabIndex        =   25
         Top             =   75
         Width           =   1320
      End
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5490
      ScaleHeight     =   345
      ScaleWidth      =   3090
      TabIndex        =   19
      Top             =   975
      Width           =   3120
      Begin VB.OptionButton OptLunas 
         Caption         =   "Tunai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   21
         Top             =   45
         Width           =   915
      End
      Begin VB.OptionButton OptLunas 
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1575
         TabIndex        =   20
         Top             =   45
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah bayar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   22
         Top             =   1035
         Visible         =   0   'False
         Width           =   1725
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2925
      Picture         =   "frmAddPenjualan.frx":0E6C
      TabIndex        =   17
      Top             =   8055
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   135
      Picture         =   "frmAddPenjualan.frx":0F6E
      TabIndex        =   3
      Top             =   8055
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4320
      Picture         =   "frmAddPenjualan.frx":1070
      TabIndex        =   5
      Top             =   8055
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   2
      Top             =   1905
      Width           =   8250
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3708
      Picture         =   "frmAddPenjualan.frx":1172
      TabIndex        =   8
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10680
      TabIndex        =   7
      Top             =   1200
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12240
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.TextBox txtKodeCustomer 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   1005
      Width           =   1530
   End
   Begin VB.TextBox txtNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   585
      Width           =   1875
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1530
      Picture         =   "frmAddPenjualan.frx":1274
      TabIndex        =   4
      Top             =   8055
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   6975
      TabIndex        =   9
      Top             =   630
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   325976067
      CurrentDate     =   38927
   End
   Begin SSDataWidgets_B.SSDBGrid dbgrid 
      Height          =   5460
      Left            =   90
      TabIndex        =   18
      Top             =   2385
      Width           =   17385
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   12
      MultiLine       =   0   'False
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   318
      Columns.Count   =   12
      Columns(0).Width=   2646
      Columns(0).Caption=   "Kode Bahan"
      Columns(0).Name =   "Kode Bahan"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   5398
      Columns(1).Caption=   "Nama Bahan"
      Columns(1).Name =   "Nama Bahan"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1349
      Columns(2).Caption=   "Qty"
      Columns(2).Name =   "Qty"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1773
      Columns(3).Caption=   "Berat"
      Columns(3).Name =   "Berat"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1984
      Columns(4).Caption=   "Satuan"
      Columns(4).Name =   "Satuan"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   2223
      Columns(5).Caption=   "Qty Invoice"
      Columns(5).Name =   "Qty Invoice"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3122
      Columns(6).Caption=   "Berat Invoice"
      Columns(6).Name =   "Berat Invoice"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2408
      Columns(7).Caption=   "Harga"
      Columns(7).Name =   "Harga"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   2355
      Columns(8).Caption=   "Harga rev"
      Columns(8).Name =   "Harga rev"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3387
      Columns(9).Caption=   "Harga Kemasan"
      Columns(9).Name =   "Harga Kemasan"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   4048
      Columns(10).Caption=   "Harga Kemasan rev"
      Columns(10).Name=   "Harga Kemasan rev"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   2752
      Columns(11).Caption=   "Total"
      Columns(11).Name=   "Total"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      _ExtentX        =   30665
      _ExtentY        =   9631
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   10560
      Top             =   720
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   12285
      X2              =   12285
      Y1              =   7875
      Y2              =   8955
   End
   Begin VB.Label lblGrandTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   13995
      TabIndex        =   42
      Top             =   8460
      Width           =   2085
   End
   Begin VB.Label Label5 
      Caption         =   "Grand Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12375
      TabIndex        =   41
      Top             =   8460
      Width           =   1545
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   14610
      TabIndex        =   40
      Top             =   7965
      Width           =   285
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "PPN(%)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   12360
      TabIndex        =   39
      Top             =   7965
      Width           =   990
   End
   Begin VB.Label Label32 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Uang Muka"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8460
      TabIndex        =   34
      Top             =   8460
      Width           =   1425
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9090
      TabIndex        =   28
      Top             =   7965
      Width           =   735
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10035
      TabIndex        =   27
      Top             =   7965
      Width           =   2175
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   16
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   15
      Top             =   1905
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5490
      TabIndex        =   14
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label7 
      Caption         =   "No.Order"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   13
      Top             =   600
      Width           =   1410
   End
   Begin VB.Label Label19 
      Caption         =   "Customer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   12
      Top             =   1050
      Width           =   960
   End
   Begin VB.Label lblNamaCustomer 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1575
      TabIndex        =   11
      Top             =   1440
      Width           =   3120
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   10
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPenjualan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Double
Dim harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte

Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_jual] from t_jualh where [nomer_jual]>'" & lblNoTrans & "' order by [nomer_jual]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
Dim conn As New ADODB.Connection
    If simpan Then
        If posting_jual(lblNoTrans) Then MsgBox "Posting Berhasil"
'        conn.Open strcon
'        conn.Execute "update t_jualh set status_posting='1' where nomer_jual='" & lblNoTrans & "'"
'        conn.Close
'        cmdPosting.Enabled = False
    End If
'    cmdPosting.Visible = False
'    cmdPosting2.Visible = False
'    cmdPrint.Enabled = True
    cek_notrans
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdPosting2_Click()
    If simpan2 Then If posting_jual(lblNoTrans) Then MsgBox "Posting Berhasil"
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_jual] from t_jualh where [nomer_jual]<'" & lblNoTrans & "' order by [nomer_jual] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
On Error GoTo err
    With CRPrint
        .reset
        .ReportFileName = reportDir & "\faktur.rpt"
        
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
            LogonInfo = .LogonInfo(i)
        Next
        .ParameterFields(0) = "namaperusahaan;" & var_namaPerusahaan & ";True"
        .ParameterFields(1) = "alamat;" & var_alamtPerusahaan & ";True"
        .SelectionFormula = "{t_juald.nomer_jual}='" & lblNoTrans & "'"
         .PrinterSelect
        
        .Destination = crptToWindow  'crptToPrinter
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        
        If .printername <> "" Then .action = 1
        reset_form
    End With
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchJual
    frmSearch.nmform = "frmAddpenjualan"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "jual"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchPO_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = "select * from vw_salesorder_belumtagih"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtNoOrder"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "SJ"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_noSJ"

    frmSearch.loadgrid frmSearch.query
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchCustomer_Click()
'    If User <> "sugik" Then
'        SearchCustomer = "select kode_customer,nama_customer,nama_toko,alamat from ms_customer"
'    End If
    frmSearch.connstr = strcon
    frmSearch.query = SearchCustomer
    frmSearch.nmform = "frmAddPenjualan"
    frmSearch.nmctrl = "txtKodeCustomer"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_customer"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchPosting_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchJual & " where status_posting<'1'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Penjualan"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub

Private Sub cmdSimpan_Click()
    If valid Then
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
    End If
End Sub
Private Function valid() As Boolean
valid = True
    If txtNoOrder.text = "" Then
        MsgBox "Order harus diisi"
        valid = False
    End If
    If txtKodeCustomer.text = "" Then
        MsgBox "Customer harus diisi"
        valid = False
    End If
End Function
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_jualh", "nomer_jual", DTPicker1, "PJ")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_jualh where nomer_jual='" & id & "'"
    conn.Execute "delete from t_juald where nomer_jual='" & id & "'"
    
    add_dataheader
    DBGrid.MoveFirst
    Row = 0
    While Row < DBGrid.Rows
        add_datadetail Row
        Row = Row + 1
        DBGrid.MoveNext
    Wend
    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    
    MsgBox err.Description
End Function
Private Function simpan2() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan2 = False
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    
    DBGrid.MoveFirst
    Row = 0
    While Row < DBGrid.Rows
        conn.Execute "update t_juald set harga_final='" & Format(DBGrid.Columns(8).text, "###0") & "',harga_kemasan_final='" & Format(DBGrid.Columns(10).text, "###0") & "' where nomer_jual='" & lblNoTrans & "' and kode_bahan='" & DBGrid.Columns(0).text & "'"
        Row = Row + 1
        DBGrid.MoveNext
    Wend
    conn.CommitTrans
    i = 0
    simpan2 = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtNoOrder.text = ""
    txtKeterangan.text = ""
    txtKodeCustomer.text = ""
    txtNoSJ.text = ""
    
    status_posting = False
    DBGrid.RemoveAll
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = Now
    cmdPrint.Enabled = False
    cmdSimpan.Enabled = True
    cmdPosting.Visible = True
    cmdPosting2.Visible = False
    DBGrid.Columns(8).Visible = False
    DBGrid.Columns(10).Visible = False
    DBGrid.Columns(7).Locked = False
    DBGrid.Columns(9).Locked = False
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
    txtPPN.text = 0
    txtUangMuka = 0
    lblGrandTotal = "0,00"
End Sub

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(11)
    ReDim nilai(11)
    
    table_name = "t_jualh"
    fields(0) = "nomer_jual"
    fields(1) = "tanggal_jual"
    fields(2) = "tanggal_jatuhtempo"
    fields(3) = "kode_customer"
    fields(4) = "nomer_order"
    fields(5) = "total"
    fields(6) = "keterangan"
    fields(7) = "userid"
    fields(8) = "cara_bayar"
    fields(9) = "ppn"
    fields(10) = "uang_muka"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = Format(DTPicker2, "yyyy/MM/dd")
    nilai(3) = txtKodeCustomer.text
    nilai(4) = txtNoOrder.text
    nilai(5) = Replace(Format(total, "###0.##"), ",", ".")
    nilai(6) = txtKeterangan.text
    nilai(7) = User
    
    If OptLunas(0).value = True Then
        nilai(8) = "T"
    Else
        nilai(8) = "K"
    End If
    nilai(9) = CDbl(txtPPN)
    nilai(10) = CDbl(txtUangMuka)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(13)
    ReDim nilai(13)

    table_name = "t_juald"
    fields(0) = "nomer_jual"
    fields(1) = "kode_Bahan"
    fields(2) = "qty"
    fields(3) = "qty_final"
    fields(4) = "NO_URUT"
    fields(5) = "berat"
    fields(6) = "berat_final"
    fields(7) = "satuan"
    fields(8) = "isi"
    fields(9) = "harga"
    fields(10) = "harga_kemasan"
    fields(11) = "harga_final"
    fields(12) = "harga_kemasan_final"

    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(0).text
    nilai(2) = DBGrid.Columns(2).text
    nilai(3) = DBGrid.Columns(5).text
    nilai(4) = Row
    nilai(5) = Replace(Format(DBGrid.Columns(3).text, "###0.###"), ",", ".")
    nilai(6) = Replace(Format(DBGrid.Columns(6).text, "###0.###"), ",", ".")
    nilai(7) = Trim(Left(DBGrid.Columns(4).text, 20))
    nilai(8) = Trim(Right(DBGrid.Columns(4).text, 20))
    nilai(9) = CCur(DBGrid.Columns(7).text)
    nilai(10) = CCur(DBGrid.Columns(9).text)
    nilai(11) = CCur(DBGrid.Columns(7).text)
    nilai(12) = CCur(DBGrid.Columns(9).text)

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
DBGrid.Columns(8).Visible = False
DBGrid.Columns(7).Locked = False
DBGrid.Columns(10).Visible = False
DBGrid.Columns(9).Locked = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_jualh where nomer_jual='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal_jual")
        txtKeterangan.text = rs("keterangan")
        txtKodeCustomer.text = rs("kode_customer")
        txtNoOrder.text = rs("nomer_order")
        cek_customer
        totalQty = 0
        total = 0
        txtPPN = rs!ppn
        txtUangMuka = Format(rs!uang_muka, "#,##0")
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        If rs!cara_bayar = "T" Then OptLunas(0).value = True Else OptLunas(1).value = True
'        cmdPrint.Visible = True
        If rs!status_posting >= "1" Then
            cmdPrint.Enabled = True
'            dbgrid.Columns(8).Visible = True
'            dbgrid.Columns(7).Locked = True
        End If
        If rs!status_posting = "0" Then
            cmdPosting.Visible = True
            cmdPosting2.Visible = False
        ElseIf rs!status_posting >= "1" Then
            cmdPosting.Visible = False
            cmdPosting2.Visible = False
            cmdSimpan.Enabled = False
            cmdPrint.Enabled = True
'        ElseIf rs!status_posting = "2" Then
'            cmdPosting.Visible = False
'            cmdPosting2.Visible = False
'            cmdSimpan.Enabled = False
        End If
        If rs.State Then rs.Close
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        Row = 1

        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat,satuan,isi,qty_final,berat_final,harga,harga_final,harga_kemasan,harga_kemasan_final,berat_final*harga_final as total,qty_final*harga_kemasan_final as total_kemasan from t_juald d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_jual='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs!satuan & Space(20) & rs!isi & "#" & rs!qty_final & "#" & rs!berat_final & "#" & Format(rs!harga, "#,##0") & "#" & Format(rs!harga_final, "#,##0") & "#" & Format(rs!harga_kemasan, "#,##0") & "#" & Format(rs!harga_kemasan_final, "#,##0") & "#" & Format(IIf(rs!total_kemasan > 0, rs!total_kemasan, rs!total), "#,##0")
                total = total + IIf(rs!harga_kemasan > 0, (rs!qty_final * rs!harga_kemasan), (rs!berat_final * rs!harga))
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(total, "#,##0")
        
        total = total - (total * (CDbl(txtPPN) / 100))
        
        hitunggrandtotal
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Function cek_noSJ() As Boolean
    'cmdPosting.Visible = False
    cek_noSJ = True
    If txtNoOrder.text = "" Then Exit Function
    If lblNoTrans <> "" And cmdPosting.Visible = False Then Exit Function
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from [vw_salesorder_belumtagih] where nomer_so='" & txtNoOrder & "'", conn
    If rs.EOF Then
        rs.Close
        If lblNoTrans <> "" Then
            rs.Open "select * from t_jualh where nomer_order='" & txtNoOrder & "' and nomer_jual='" & lblNoTrans & "'", conn
            If rs.EOF Then
                cek_noSJ = False
                MsgBox "Order tidak ditemukan atau sudah ditagih atau belum selesai"
            Else
                GoTo load
            
            End If
            rs.Close
        Else
            cek_noSJ = False
            MsgBox "Order tidak ditemukan atau sudah ditagih atau belum selesai"
        End If
        conn.Close
        Exit Function
    End If
load:
    If rs.State Then rs.Close
    rs.Open "select * from t_soh where nomer_so='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        
        txtKodeCustomer.text = rs("kode_customer")
        txtUangMuka = rs!UangMuka
        cek_customer
        totalQty = 0
        total = 0
'        cmdPrint.Visible = True
        
        If rs.State Then rs.Close
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        Row = 1
        rs.Open "select d.[kode_bahan],m.[nama_bahan], qtykirim,d.beratkirim,satuan,isi,harga,harga_kemasan from t_sod d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_so='" & txtNoOrder & "' order by no_urut", conn
        While Not rs.EOF
            DBGrid.AddItem rs!kode_bahan & "#" & rs!nama_bahan & "#" & rs!qtykirim & "#" & rs!beratkirim & "#" & rs!satuan & Space(20) & rs!isi & "#" & rs!qtykirim & "#" & rs!beratkirim & "#" & rs!harga & "#" & rs!harga & "#" & rs!harga_kemasan & "#" & rs!harga_kemasan & "#" & IIf(rs!harga_kemasan > 0, rs!qtykirim * rs!harga_kemasan, rs!beratkirim * rs!harga) & ""
            totalQty = totalQty + rs!qtykirim
            total = total + IIf(rs!harga_kemasan > 0, rs!qtykirim * rs!harga_kemasan, rs!beratkirim * rs!harga)
            rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(total, "#,##0")
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub Command1_Click()
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection

    conn.Open strcon
    rs.Open "select * from t_jualh where status_posting=0 order by nomer_jual", conn
    While Not rs.EOF
        posting_jual rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close


End Sub

Private Sub dbgrid_AfterColUpdate(ByVal ColIndex As Integer)
    If cmdPosting.Visible = True Then
        DBGrid.Columns(11).text = Format(IIf(DBGrid.Columns(9).text > 0, DBGrid.Columns(2).text * DBGrid.Columns(9).text, DBGrid.Columns(3).text * DBGrid.Columns(7).text), "#,##0")
        DBGrid.Columns(6).text = DBGrid.Columns(3).text
        DBGrid.Columns(8).text = DBGrid.Columns(7).text
        DBGrid.Columns(10).text = DBGrid.Columns(9).text
        total = total + CCur(DBGrid.Columns(11).text)
        lblTotal = Format(total, "#,##0")
    Else
        DBGrid.Columns(11).text = Format(IIf(DBGrid.Columns(9).text > 0, DBGrid.Columns(5).text * DBGrid.Columns(9).text, DBGrid.Columns(6).text * DBGrid.Columns(8).text), "#,##0")
        total = total + DBGrid.Columns(11).text
        lblTotal = Format(total, "#,##0")
        
    End If
End Sub

Private Sub dbgrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    
    If Not IsNumeric(DBGrid.Columns(ColIndex).text) And (ColIndex = 5 Or ColIndex = 6 Or ColIndex = 8 Or ColIndex = 9) Then
        Cancel = True
    Else
        total = total - DBGrid.Columns(11).text
    End If
End Sub

Private Sub dbgrid_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
'    If DBGrid.row <> Trim(LastRow) Then
'    Dim conn As New ADODB.Connection
'    Dim rs As New ADODB.Recordset
'    conn.Open strcon
'    rs.Open "select * from ms_bahan where kode_bahan='" & DBGrid.Columns(0).text & "'", conn
'    If Not rs.EOF Then
'        DBGrid.Columns(4).RemoveAll
'        DBGrid.Columns(4).AddItem rs!satuan1 & Space(20) & rs!isi1
'        DBGrid.Columns(4).AddItem rs!satuan2 & Space(20) & rs!isi2
'        DBGrid.Columns(4).AddItem rs!satuan3 & Space(20) & rs!isi3
'    End If
'    rs.Close
'    conn.Close
'    End If
End Sub

Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchcustomer_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        
        KeyAscii = 0
        Call MySendKeys("{tab}")
        
    End If
End Sub

Private Sub Form_Load()
    alterjual
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    seltab = 0
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan

        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""

        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_customer()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_customer] from ms_customer where kode_customer='" & txtKodeCustomer & "'", conn
    If Not rs.EOF Then
        lblNamaCustomer = rs(0)
        
        
    Else
        lblNamaCustomer = ""
        
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()

End Sub

Private Sub listKuli_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listKuli.RemoveItem (listKuli.ListIndex)
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub TabStrip1_Click()
    If TabStrip1.SelectedItem.Index = 1 Then
        DBGrid.Visible = True
        frDetail.Visible = False
    Else
        DBGrid.Visible = False
        frDetail.Visible = True
    End If
    
End Sub

Private Sub Text1_Change()

End Sub

Private Sub txtKodecustomer_LostFocus()
    cek_customer
End Sub

Private Sub txtNoOrder_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPO_Click
End Sub

Private Sub txtNoOrder_LostFocus()
On Error Resume Next
    If Not cek_noSJ Then txtNoOrder.SetFocus
End Sub

Private Sub txtPPN_Change()
    hitunggrandtotal
End Sub

Private Sub hitunggrandtotal()
    If txtPPN = "" Then txtPPN = 0
    If txtUangMuka = "" Then txtUangMuka = 0

    If CDbl(txtPPN.text) > 10 Then
        If (MsgBox("Yakin PPN diatas 10%", vbYesNo)) = vbNo Then
            txtPPN.text = 0
        Else
            Exit Sub
        End If
    End If
    
    lblGrandTotal = Format((CDbl(Format(lblTotal, "###0")) - CDbl(Format(txtUangMuka, "###0"))) - ((CDbl(Format(lblTotal, "###0")) - CDbl(Format(txtUangMuka, "###0"))) * CDbl(txtPPN) / 100), "#,##0")
    total = CDbl(Format(lblGrandTotal, "###0"))
End Sub

Private Sub txtUangMuka_Change()
    hitunggrandtotal
End Sub
