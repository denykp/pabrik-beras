VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPembelian1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembelian"
   ClientHeight    =   8085
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   14040
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   14040
   Visible         =   0   'False
   Begin VB.TextBox txtUangMuka 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   12135
      TabIndex        =   86
      Text            =   "0"
      Top             =   5565
      Width           =   1755
   End
   Begin VB.CommandButton cmdSearchKontrak 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3168
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   81
      Top             =   540
      Width           =   420
   End
   Begin VB.TextBox txtNoKontrak 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1572
      TabIndex        =   0
      Top             =   552
      Width           =   1530
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   11760
      Picture         =   "frmAddPembelian1.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   73
      Top             =   6135
      Width           =   1230
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5490
      ScaleHeight     =   345
      ScaleWidth      =   3090
      TabIndex        =   68
      Top             =   900
      Width           =   3120
      Begin VB.OptionButton OptLunas 
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1575
         TabIndex        =   5
         Top             =   45
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.OptionButton OptLunas 
         Caption         =   "Tunai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   4
         Top             =   45
         Width           =   915
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah bayar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   69
         Top             =   1035
         Visible         =   0   'False
         Width           =   1725
      End
   End
   Begin VB.Frame frJT 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   420
      Left            =   5535
      TabIndex        =   57
      Top             =   1305
      Width           =   3480
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   1530
         TabIndex        =   6
         Top             =   45
         Width           =   1875
         _ExtentX        =   3307
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   111607811
         CurrentDate     =   38927
      End
      Begin VB.Label Label9 
         Caption         =   "Jatuh Tempo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   45
         TabIndex        =   58
         Top             =   75
         Width           =   1320
      End
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   10440
      Picture         =   "frmAddPembelian1.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   6120
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   11805
      Picture         =   "frmAddPembelian1.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   6885
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   3
      Top             =   1752
      Width           =   3885
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3450
      Picture         =   "frmAddPembelian1.frx":0306
      TabIndex        =   30
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8685
      TabIndex        =   29
      Top             =   165
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10170
      TabIndex        =   28
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3165
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   27
      Top             =   1308
      Width           =   420
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8010
      TabIndex        =   26
      Top             =   1770
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdLoad2 
      Caption         =   "&Load Excel 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8730
      TabIndex        =   25
      Top             =   1770
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save Excel "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9225
      TabIndex        =   24
      Top             =   1770
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   1356
      Width           =   1530
   End
   Begin VB.TextBox txtBukti 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   948
      Width           =   1875
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   10440
      Picture         =   "frmAddPembelian1.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   6915
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7020
      TabIndex        =   31
      Top             =   555
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   111083523
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5385
      Index           =   0
      Left            =   195
      TabIndex        =   39
      Top             =   2550
      Width           =   10080
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2145
         Left            =   90
         TabIndex        =   51
         Top             =   2670
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   3784
         _Version        =   393216
         Cols            =   9
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2265
         Left            =   72
         TabIndex        =   40
         Top             =   156
         Width           =   6990
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1260
            TabIndex        =   9
            Top             =   960
            Width           =   1140
         End
         Begin VB.PictureBox Picture2 
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   2790
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   42
            Top             =   225
            Width           =   465
            Begin VB.CommandButton cmdSearchBrg 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmAddPembelian1.frx":050A
               TabIndex        =   43
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   90
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   41
            Top             =   1710
            Width           =   3795
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   13
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   12
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   11
               Top             =   84
               Width           =   1350
            End
         End
         Begin VB.TextBox txtHarga 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1260
            TabIndex        =   10
            Top             =   1335
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   8
            Top             =   615
            Width           =   1140
         End
         Begin VB.TextBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   7
            Top             =   270
            Width           =   1500
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2505
            TabIndex        =   78
            Top             =   1065
            Width           =   600
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Berat Total"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   77
            Top             =   1020
            Width           =   1020
         End
         Begin VB.Label lblNamaBarang2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   288
            Left            =   2556
            TabIndex        =   56
            Top             =   708
            Visible         =   0   'False
            Width           =   3480
         End
         Begin VB.Label Label8 
            BackColor       =   &H8000000C&
            Caption         =   "Harga"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   50
            Top             =   1365
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblDefQtyJual 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   270
            TabIndex        =   49
            Top             =   1395
            Width           =   870
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   48
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            Caption         =   "Qty"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   47
            Top             =   660
            Width           =   600
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3285
            TabIndex        =   46
            Top             =   315
            Width           =   3480
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   45
            Top             =   1305
            Width           =   600
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2520
            TabIndex        =   44
            Top             =   675
            Width           =   1050
         End
      End
      Begin VB.Label Label17 
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   55
         Top             =   4920
         Width           =   735
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3465
         TabIndex        =   54
         Top             =   4920
         Width           =   1140
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   53
         Top             =   4920
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   52
         Top             =   4920
         Width           =   735
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   4920
      Index           =   1
      Left            =   168
      TabIndex        =   59
      Top             =   2565
      Visible         =   0   'False
      Width           =   10185
      Begin VB.Frame Frame3 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1995
         Left            =   75
         TabIndex        =   60
         Top             =   180
         Width           =   6990
         Begin VB.TextBox txtSNBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1260
            TabIndex        =   16
            Top             =   1020
            Width           =   1140
         End
         Begin VB.TextBox txtSerial 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1260
            TabIndex        =   15
            Top             =   630
            Width           =   1140
         End
         Begin VB.TextBox txtQtySerial 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5610
            TabIndex        =   17
            Top             =   1200
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.PictureBox Picture6 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   90
            ScaleHeight     =   465
            ScaleWidth      =   3615
            TabIndex        =   61
            Top             =   1485
            Width           =   3615
            Begin VB.CommandButton cmdOk2 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   18
               Top             =   45
               Width           =   1350
            End
            Begin VB.CommandButton cmdClear2 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   19
               Top             =   45
               Width           =   1050
            End
            Begin VB.CommandButton cmdDelete2 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   20
               Top             =   45
               Width           =   1050
            End
         End
         Begin VB.ComboBox cmbItem 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   270
            Width           =   4740
         End
         Begin VB.Label Label5 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2505
            TabIndex        =   76
            Top             =   1065
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Berat"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   74
            Top             =   1035
            Width           =   450
         End
         Begin VB.Label lblNama2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4725
            TabIndex        =   72
            Top             =   1590
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.Label lblNama1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5730
            TabIndex        =   71
            Top             =   1575
            Visible         =   0   'False
            Width           =   870
         End
         Begin VB.Label Label26 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   66
            Top             =   1305
            Width           =   600
         End
         Begin VB.Label Label24 
            BackColor       =   &H8000000C&
            Caption         =   "Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   65
            Top             =   675
            Width           =   600
         End
         Begin VB.Label Label23 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   64
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label Label11 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   270
            TabIndex        =   63
            Top             =   1380
            Width           =   870
         End
         Begin VB.Label Label6 
            BackColor       =   &H8000000C&
            Caption         =   "Qty"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4530
            TabIndex        =   62
            Top             =   1245
            Visible         =   0   'False
            Width           =   600
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid2 
         Height          =   2175
         Left            =   90
         TabIndex        =   67
         Top             =   2265
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   3836
         _Version        =   393216
         Cols            =   7
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblQty2 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3465
         TabIndex        =   80
         Top             =   4485
         Width           =   1140
      End
      Begin VB.Label Label20 
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   79
         Top             =   4485
         Width           =   735
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5805
      Left            =   135
      TabIndex        =   70
      Top             =   2205
      Width           =   10230
      _ExtentX        =   18045
      _ExtentY        =   10239
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Global"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Serial Number"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label25 
      Caption         =   "Uang Muka"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10500
      TabIndex        =   87
      Top             =   5565
      Width           =   1935
   End
   Begin VB.Label Label10 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10500
      TabIndex        =   85
      Top             =   5085
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11670
      TabIndex        =   84
      Top             =   5085
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label21 
      Caption         =   "No Penerimaan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   83
      Top             =   600
      Width           =   1410
   End
   Begin VB.Label lblKontrak 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   228
      Left            =   3696
      TabIndex        =   82
      Top             =   600
      Width           =   72
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   336
      Left            =   144
      TabIndex        =   75
      Top             =   1356
      Visible         =   0   'False
      Width           =   492
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   38
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   37
      Top             =   1815
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5535
      TabIndex        =   36
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label7 
      Caption         =   "No Faktur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   96
      TabIndex        =   35
      Top             =   1020
      Width           =   1272
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   96
      TabIndex        =   34
      Top             =   1404
      Width           =   960
   End
   Begin VB.Label lblNamaSupplier 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   228
      Left            =   3672
      TabIndex        =   33
      Top             =   1368
      Width           =   72
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   32
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPembelian1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Double
Dim totalQty2 As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String

Private Sub chkNumbering_Click()
    If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub

Private Sub cmbItem_Click()
'    txtSerial.text = Trim(Right(cmbItem, 50))
    cek_kodebarang2
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"
    txtUangMuka.text = "0"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdClear2_Click()
    txtQtySerial.text = "1"
    txtSNBerat.text = "0"
    txtSerial.text = ""
    mode2 = 1
    cmdClear2.Enabled = False
    cmdDelete2.Enabled = False
    txtSerial.SetFocus
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    
    row = flxGrid.row
    For J = 1 To flxGrid2.Rows - 1
        If flxGrid2.TextMatrix(J, 1) = flxGrid.TextMatrix(row, 1) And flxGrid2.TextMatrix(J, 5) = flxGrid.TextMatrix(row, 5) Then flxGrid2.row = J
    Next
    totalQty = totalQty - (flxGrid.TextMatrix(row, 6))
    total = total - (flxGrid.TextMatrix(row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblTotal = Format(total, "#,##0")

    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    cmdDelete2_Click
End Sub

Private Sub cmdDelete2_Click()
Dim row, col As Integer
    If flxGrid2.Rows <= 2 Then Exit Sub
    flxGrid2.TextMatrix(flxGrid2.row, 0) = ""
    row = flxGrid2.row
    
    totalQty2 = totalQty2 - (flxGrid2.TextMatrix(row, 6))
    lblQty2 = Format(totalQty2, "#,##0.##")

    
    For row = row To flxGrid2.Rows - 1
        If row = flxGrid2.Rows - 1 Then
            For col = 1 To flxGrid2.cols - 1
                flxGrid2.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid2.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid2.cols - 1
                flxGrid2.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid2.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid2.cols - 1
            flxGrid2.TextMatrix(row, col) = flxGrid2.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid2.row > 1 Then flxGrid2.row = flxGrid2.row - 1

    flxGrid2.Rows = flxGrid2.Rows - 1
    flxGrid2.col = 0
    flxGrid2.ColSel = flxGrid2.cols - 1
    cmdClear2_Click
    mode2 = 1
    cmdClear2.Enabled = False
    cmdDelete2.Enabled = False

End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang1
        flxGrid.TextMatrix(row, 3) = lblNamaBarang2
        flxGrid.TextMatrix(row, 5) = txtQty
        If flxGrid.TextMatrix(row, 6) = "" Then
            flxGrid.TextMatrix(row, 6) = txtBerat
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 6))
            total = total - (flxGrid.TextMatrix(row, 8))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 6) = CDbl(flxGrid.TextMatrix(row, 6)) + CDbl(txtBerat)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 6) = CDbl(txtBerat)
            End If
        End If
        
        flxGrid.TextMatrix(row, 7) = txtHarga.text
        flxGrid.TextMatrix(row, 8) = flxGrid.TextMatrix(row, 6) * flxGrid.TextMatrix(row, 7)
        flxGrid.TextMatrix(row, 8) = Format(flxGrid.TextMatrix(row, 8), "#,##0")
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(row, 6)
        total = total + (flxGrid.TextMatrix(row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblTotal = Format(total, "#,##0")
        lblItem = flxGrid.Rows - 2
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        loadcombo
'        SetComboTextRight txtKdBrg.text, cmbItem
'        txtSerial = txtKdBrg.text
'        txtQtySerial = txtIsi
'        txtBerat = txtPalet
        
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        txtQty.text = "1"
        txtBerat.text = "1"
        
        txtHarga.text = "0"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
'    cmdOk2_Click
    'lblItem = flxGrid.Rows - 2
End Sub


Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\Transferpembelian.rpt"
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'        .SelectionFormula = " {t_beliRollh.ID}='" & lblNoTrans & "'"
'        .Destination = crptToWindow
'        .ParameterFields(0) = "login;" + user + ";True"
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
'        .action = 1
'    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdOk2_Click()
Dim i As Integer
Dim row As Integer, Serial As String
Dim kodebarang As String
    row = 0
    kodebarang = Trim(Right(cmbItem.text, 50))
    If txtSerial <> "" And txtQtySerial > 0 Then
    With flxGrid2
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = kodebarang And .TextMatrix(i, 4) = txtSerial.text And .TextMatrix(i, 5) = txtQtySerial.text Then row = i
        Next
        If row = 0 Then
            row = .Rows - 1
            .Rows = .Rows + 1
        End If
        
        .row = row
        currentRow = .row
        If .TextMatrix(row, 6) <> "" Then totalQty2 = totalQty2 - .TextMatrix(row, 6)
        .TextMatrix(row, 1) = kodebarang
        .TextMatrix(row, 2) = lblNama1
        .TextMatrix(row, 3) = lblNama2
        .TextMatrix(row, 6) = txtSNBerat
        .TextMatrix(row, 4) = txtSerial.text
'        .TextMatrix(row, 5) = txtQtySerial
        .TextMatrix(row, 5) = 1
        
        
        totalQty2 = totalQty2 + .TextMatrix(row, 6)
        lblQty2 = Format(totalQty2, "#,##0.##")
        
        .row = row
        .col = 0
        .ColSel = .cols - 1
        
        If row > 8 Then
            .TopRow = row - 7
        Else
            .TopRow = 1
        End If
        
        
        txtQtySerial.text = "1"
'        txtBerat.text = "1"
'        txtQtySerial.SetFocus
        Serial = ""
        If Len(txtSerial.text) > 3 Then
            Serial = Left(txtSerial.text, 3)
        End If
        
        cmdClear2_Click
        txtSerial.text = Serial
'        txtSerial.SelStart = Len(txtSerial.text)
        cmdClear2.Enabled = False
        cmdDelete2.Enabled = False
        mode2 = 1
        lblItem = .Rows - 2
        
'        txtSerial.SelStart = Len(txtSerial.text) + 1
    End With
    End If
    
    
    
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    simpan
    If posting_beliRoll(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
    For J = 1 To flxGrid2.Rows - 2
        If Not cek_serialnumber(flxGrid2.TextMatrix(J, 4), flxGrid2.TextMatrix(J, 1)) Then Cek_Serial = False
    Next
End Function
        
Private Function Cek_qty() As Boolean
Dim qtycek As Double
Dim beratcek As Double
    Cek_qty = True
    For i = 1 To flxGrid.Rows - 2
        qtycek = 0
        beratcek = 0
        For J = 1 To flxGrid2.Rows - 2
            If flxGrid2.TextMatrix(J, 1) = flxGrid.TextMatrix(i, 1) Then
                qtycek = qtycek + 1
                beratcek = beratcek + (flxGrid2.TextMatrix(J, 6))
            End If
            
        Next
        If beratcek <> flxGrid.TextMatrix(i, 6) Or qtycek <> flxGrid.TextMatrix(i, 5) Then Cek_qty = False
    Next i
End Function

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = SearchRoll & IIf(txtNoKontrak <> "", " and kode_bahan in (select kode_bahan from t_orderbelirolld where nomer_orderbeliroll='" & txtNoKontrak & "')", "")
    frmSearch.nmform = "frmAddPembelianRoll"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchBeliRoll & " where status_posting='0'"
    frmSearch.nmform = "frmAddPembelianRoll"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "beliRollH"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKontrak_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchTerimaBarang
    frmSearch.nmform = "frmAddpembelian"
    frmSearch.nmctrl = "txtnokontrak"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "orderBeliRollH"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_kontrak"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchSupplier_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmAddPembelianRoll"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If Trim(txtUangMuka.text) = "" Then txtUangMuka.text = "0"
    If CDbl(txtUangMuka.text) > CDbl(lblTotal.Caption) Then
        txtUangMuka.text = CDbl(lblTotal.Caption)
    End If

    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer, UangMuka As Double
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If txtKodeSupplier.text = "" Then
        MsgBox "Silahkan masukkan Supplier terlebih dahulu"
        txtKodeSupplier.SetFocus
        Exit Function
    End If
    If cmbGudang.text = "" Then
        MsgBox "Silahkan pilih gudang terlebih dahulu"
        cmbGudang.SetFocus
        Exit Function
    End If
    
    
    id = lblNoTrans
    If lblNoTrans = "-" And chkNumbering = "1" Then
        lblNoTrans = newid("t_beliRollh", "nomer_beliRoll", DTPicker1, "PBR")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select UangMuka from t_beliRollh where nomer_beliroll='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        UangMuka = rs("uangmuka")
        conn.Execute "Update t_orderbelirollh set uangmuka_keluar=uangmuka_keluar - " & Replace(UangMuka, ",", ".") & " where nomer_OrderBeliRoll='" & txtNoKontrak.text & "'"
    End If
    rs.Close
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_beliRollh where nomer_beliRoll='" & id & "'"
    conn.Execute "delete from t_beliRolld where nomer_beliRoll='" & id & "'"
    conn.Execute "delete from t_beliRoll_serial where nomer_beliRoll='" & id & "'"
    add_dataheader
    
    conn.Execute "Update t_orderbelirollh set uangmuka_keluar=uangmuka_keluar + " & Replace(txtUangMuka, ",", ".") & " where nomer_OrderBeliRoll='" & txtNoKontrak.text & "'"

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    For row = 1 To flxGrid2.Rows - 2
        add_datadetail2 (row)
    Next

    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtBukti.text = ""
    txtKeterangan.text = ""
'    txtKodeSupplier.text = ""
    txtUangMuka.text = "0"
    loadcombo
    totalQty = 0
    totalQty2 = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = DTPicker1 + 30
    cmdClear_Click
    cmdClear2_Click
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxGrid2.Rows = 1
    flxGrid2.Rows = 2
    
    TabStrip1.Tabs(1).Selected = True
    
    txtBukti.SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(12)
    ReDim nilai(12)
    table_name = "t_beliRollh"
    fields(0) = "nomer_beliRoll"
    fields(1) = "tanggal_beliRoll"
    fields(2) = "tanggal_jatuhtempo"
    fields(3) = "kode_supplier"
    fields(4) = "nomer_nota"
    fields(5) = "total"
    fields(6) = "keterangan"
    fields(7) = "userid"
    fields(8) = "cara_bayar"
    fields(9) = "kode_gudang"
    fields(10) = "nomer_orderbeliroll"
    fields(11) = "uangmuka"
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = Format(DTPicker2, "yyyy/MM/dd")
    nilai(3) = txtKodeSupplier.text
    nilai(4) = txtBukti.text
    nilai(5) = Replace(Format(lblTotal, "###0.##"), ",", ".")
    nilai(6) = txtKeterangan.text
    nilai(7) = User
    nilai(9) = cmbGudang.text
    nilai(10) = txtNoKontrak
    nilai(11) = txtUangMuka.text
    
    If OptLunas(0).value = True Then
        nilai(8) = "T"
    Else
        nilai(8) = "K"
    End If
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_beliRolld"
    fields(0) = "nomer_beliRoll"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    
    fields(4) = "NO_URUT"
    fields(5) = "HARGA"
    

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    
    nilai(2) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(row, 6), "###0.##"), ",", ".")
    nilai(4) = row
    nilai(5) = Replace(Format(flxGrid.TextMatrix(row, 7), "###0.##"), ",", ".")
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail2(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_beliRoll_serial"
    fields(0) = "nomer_beliRoll"
    fields(1) = "kode_bahan"
    fields(2) = "nomer_serial"
    fields(3) = "NO_URUT"
    fields(4) = "berat"
    
    

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid2.TextMatrix(row, 1)
    nilai(2) = flxGrid2.TextMatrix(row, 4)
    nilai(3) = row
    nilai(4) = Replace(Format(flxGrid2.TextMatrix(row, 6), "###0.##"), ",", ".")
    
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub


Public Sub cek_notrans()
    cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_beliRollh where nomer_beliRoll='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        txtNoKontrak = rs!nomer_orderbeliroll
        SetComboText rs!kode_gudang, cmbGudang
        DTPicker1 = rs("tanggal_beliRoll")
        DTPicker2 = rs("tanggal_jatuhtempo")
        txtKeterangan.text = rs("keterangan")
        txtUangMuka.text = rs("uangmuka")
        txtKodeSupplier.text = rs("kode_supplier")
        txtBukti.text = rs("nomer_nota")
        totalQty = 0
        totalQty2 = 0
        total = 0
'        cmdPrint.Visible = True
        If rs("status_posting") = 1 Then
'            cmdPrint.Enabled = False
            cmdSimpan.Enabled = False
            mnuSave.Enabled = False
        Else
            cmdSimpan.Enabled = True
            cmdPosting.Visible = True
            mnuSave.Enabled = True
        End If
        
        If rs("cara_bayar") = "T" Then
            OptLunas(0).value = True
        Else
            OptLunas(1).value = True
        End If
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat,d.harga,panjang,lebar,m.berat from t_beliRolld d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_beliRoll='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(5) & "x" & rs(6) & "x" & rs(7)
                
                flxGrid.TextMatrix(row, 5) = rs(2)
                flxGrid.TextMatrix(row, 6) = rs(3)
                flxGrid.TextMatrix(row, 7) = Format(rs(4), "#,##0")
                flxGrid.TextMatrix(row, 8) = Format(rs(3) * rs(4), "#,##0")
                row = row + 1
                totalQty = totalQty + rs(3)
                total = total + (rs(3) * rs(4))
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        flxGrid2.Rows = 1
        flxGrid2.Rows = 2
        row = 1
        rs.Open "select d.[kode_bahan],m.[nama_bahan], d.berat,d.nomer_serial,panjang,lebar,m.berat from t_beliRoll_serial d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_beliRoll='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid2.TextMatrix(row, 1) = rs(0)
                flxGrid2.TextMatrix(row, 2) = rs(1)
                flxGrid2.TextMatrix(row, 3) = rs(4) & "x" & rs(5) & "x" & rs(6)
                flxGrid2.TextMatrix(row, 4) = rs(3)
                flxGrid2.TextMatrix(row, 5) = 1
                flxGrid2.TextMatrix(row, 6) = rs(2)

                row = row + 1
                totalQty2 = totalQty2 + rs(2)
                
                flxGrid2.Rows = flxGrid2.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty
        lblQty2 = totalQty2
        lblTotal = Format(total, "#,##0.00")
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKdBrg.SetFocus
End Sub

Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
                lblNamaBarang2 = flxGrid.TextMatrix(flxGrid.row, 3)
                End If
            
            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 7)
            txtBerat.SetFocus
        End If

End Sub
Private Sub loaddetil2()

        If flxGrid2.TextMatrix(flxGrid2.row, 1) <> "" Then
            mode2 = 2
            cmdClear2.Enabled = True
            cmdDelete2.Enabled = True
            SetComboTextRight flxGrid2.TextMatrix(flxGrid2.row, 1), cmbItem
            txtSerial.text = flxGrid2.TextMatrix(flxGrid2.row, 4)
            txtQtySerial.text = flxGrid2.TextMatrix(flxGrid2.row, 5)
            txtSNBerat.text = flxGrid2.TextMatrix(flxGrid2.row, 6)
            txtSerial.SetFocus
        End If

End Sub
Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub


Private Sub flxGrid2_DblClick()
    If flxGrid2.TextMatrix(flxGrid2.row, 1) <> "" Then
        loaddetil2
        mode = 2
        txtSerial.SetFocus
    End If
End Sub

Private Sub flxGrid2_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDelete Then
        cmdDelete2_Click
    ElseIf KeyCode = vbKeyReturn Then
        loaddetil2
    End If
End Sub

Private Sub flxGrid2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtSerial.SetFocus
End Sub



Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 1200
    flxGrid.ColWidth(4) = 0
    flxGrid.ColWidth(5) = 900
    flxGrid.ColWidth(6) = 900
    flxGrid.ColWidth(7) = 900
    flxGrid.ColWidth(8) = 1500
    
    
'    If right_hpp = True Then
        
        txtHarga.Visible = True
        Label8.Visible = True
        lblTotal.Visible = True
        Label10.Visible = True
'    End If

    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Panjang"
    flxGrid.TextMatrix(0, 4) = ""
    flxGrid.TextMatrix(0, 5) = "Qty"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "Harga"
    flxGrid.TextMatrix(0, 8) = "Total"
    
    With flxGrid2
    .ColWidth(0) = 300
    .ColWidth(1) = 1100
    .ColWidth(2) = 2800
    .ColWidth(3) = 0
    .ColWidth(4) = 1000
    .ColWidth(5) = 0
    .ColWidth(6) = 1000
    
    .TextMatrix(0, 1) = "Kode Bahan"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .ColAlignment(3) = 1
    .TextMatrix(0, 2) = "Nama"
    .TextMatrix(0, 3) = "Dimensi"
    .TextMatrix(0, 4) = "Serial Number"
    .TextMatrix(0, 5) = "Qty"
    .TextMatrix(0, 6) = "Berat"
    End With
End Sub
Private Sub loadcombo()
Dim kode As String
    kode = Trim(Right(cmbItem.text, 50))
    cmbItem.Clear
    For i = 1 To flxGrid.Rows - 2
        cmbItem.AddItem flxGrid.TextMatrix(i, 2) & "(" & flxGrid.TextMatrix(i, 3) & ")" & Space(100) & flxGrid.TextMatrix(i, 1)
    Next
    SetComboTextRight kode, cmbItem
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        lblNamaBarang2 = rs!panjang & "x" & rs!lebar & "x" & rs!berat
        lblSatuan = ""
        If lblKategori <> "" Then
        txtHarga.text = IIf(IsNull(rs("harga_beli" & lblKategori)), 0, rs("harga_beli" & lblKategori))
        End If
        If txtNoKontrak <> "" Then
        rs.Close
        rs.Open "select harga from t_orderbelirolld where nomer_orderbeliroll='" & txtNoKontrak & "' and kode_bahan='" & txtKdBrg & "'", conn
        If Not rs.EOF Then
            txtHarga = rs!harga
        End If
        rs.Close
        End If
        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_kodebarang2() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select [nama_bahan],panjang,lebar,berat from ms_bahan where [kode_bahan]='" & Trim(Right(cmbItem, 50)) & "'", conn
    If Not rs.EOF Then
        lblNama1 = rs(0)
        lblNama2 = rs(1) & "x" & rs(2) & "x" & rs(3)
        
        cek_kodebarang2 = True
    Else
        lblNama1 = ""
        lblNama2 = ""
        
        cek_kodebarang2 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier] from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)

        
    Else
        lblNamaSupplier = ""

    End If
    rs.Close
    conn.Close
'    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()
On Error GoTo err
    conn.Open strcon
    With flxGrid
    For i = 1 To .Rows - 2
        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
        If Not rs.EOF Then
            total = total - .TextMatrix(i, 8)
            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
            total = total + .TextMatrix(i, 8)
        End If
        rs.Close
    Next
    End With
    conn.Close
    lblTotal = Format(total, "#,##0")
    Exit Sub
err:
    MsgBox err.Description
    If conn.State Then conn.Close
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub OptLunas_Click(Index As Integer)
    If Index = 1 Then frJT.Visible = True Else frJT.Visible = False
End Sub

Private Sub TabStrip1_Click()
On Error Resume Next
    For i = 0 To frDetail.UBound
        frDetail(i).Visible = False
    Next
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    If (TabStrip1.SelectedItem.Index - 1) = 1 Then
        loadcombo
        txtQtySerial.SetFocus
    ElseIf (TabStrip1.SelectedItem.Index - 1) = 0 Then
        txtKdBrg.SetFocus
    End If
        
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
End Sub

Private Sub txtIsi_GotFocus()
    txtIsi.SelStart = 0
    txtIsi.SelLength = Len(txtIsi.text)
End Sub

Private Sub txtIsi_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtIsi_LostFocus()
    txtQty = CDbl(txtPalet) * CDbl(txtIsi)
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub



Private Sub txtNoKontrak_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchKontrak_Click
End Sub

Private Sub txtNoKontrak_LostFocus()
    If txtNoKontrak <> "" Then
        If Not cek_kontrak Then
            MsgBox "Nomor kontrak tidak ditemukan"
            txtNoKontrak.SetFocus
        End If
    Else
        txtHarga.Locked = False
        txtKodeSupplier.Enabled = True
        cmdSearchSupplier.Enabled = True
    End If
End Sub
Public Function cek_kontrak() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    cek_kontrak = False
    conn.Open strcon
    rs.Open "select * from t_terimabarangh where nomer_terimabarang='" & txtNoKontrak & "'", conn
    If Not rs.EOF Then
        cek_kontrak = True
        txtKodeSupplier = rs!kode_supplier
        'txtUangMuka = (rs!uangmuka_masuk) - (rs!uangmuka_keluar) - (rs!uangmuka_kembali)
        cek_supplier
        
'        txtHarga.Locked = True
        txtKodeSupplier.Enabled = False
        cmdSearchSupplier.Enabled = False
    Else
        txtHarga.Locked = False
        txtKodeSupplier.Enabled = True
        cmdSearchSupplier.Enabled = True

    End If
    rs.Close
    
    conn.Close
    Exit Function
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Function
Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Private Sub txtQtySerial_GotFocus()
    txtQtySerial.SelStart = 0
    txtQtySerial.SelLength = Len(txtQtySerial.text)
End Sub

Private Sub txtQtySerial_KeyPress(KeyAscii As Integer)
    Angka (KeyAscii)
End Sub

Private Sub txtSerial_GotFocus()
    txtSerial.SelStart = Len(txtSerial.text)
'    txtSerial.SelLength = Len(txtSerial.text)
End Sub

Private Sub txtSerial_LostFocus()
    If Not cek_serialnumber(txtSerial, cmbItem.text) And txtSerial <> "" Then
        MsgBox "Nomer Serial tersebut sudah terpakai"
        txtSerial.SetFocus
    End If
End Sub

Private Sub txtSNBerat_GotFocus()
    txtSNBerat.SelStart = 0
    txtSNBerat.SelLength = Len(txtSNBerat.text)

End Sub

Private Sub txtUangMuka_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtUangMuka_GotFocus()
    txtUangMuka.SelStart = 0
    txtUangMuka.SelLength = Len(txtUangMuka.text)
End Sub

Public Function cek_serialnumber(noseri As String, kode_bahan As String) As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cek_serialnumber = True
If noseri = "" Then
    cek_serialnumber = True
    Exit Function
End If
conn.Open strcon
rs.Open "select * from stock where nomer_serial='" & noseri & "' and kode_bahan='" & kode_bahan & "'", conn
If Not rs.EOF Then
    cek_serialnumber = False
End If
rs.Close
conn.Close
Exit Function
err:
MsgBox err.Description
If rs.State Then rs.Close
If conn.State Then conn.Close
End Function

