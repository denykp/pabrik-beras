VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmAddBayarPiutang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembayaran Hutang"
   ClientHeight    =   7515
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   14010
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   14010
   Begin VB.TextBox txtBonus 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   11430
      TabIndex        =   39
      Text            =   "0"
      Top             =   4725
      Visible         =   0   'False
      Width           =   2220
   End
   Begin VB.Frame frTab 
      BorderStyle     =   0  'None
      Height          =   3345
      Index           =   1
      Left            =   405
      TabIndex        =   33
      Top             =   2520
      Visible         =   0   'False
      Width           =   4920
      Begin SSDataWidgets_B.SSDBGrid DBGrid1 
         Height          =   2985
         Left            =   135
         TabIndex        =   35
         Top             =   135
         Width           =   4365
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   2
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   450
         ExtraHeight     =   159
         Columns.Count   =   2
         Columns(0).Width=   3175
         Columns(0).Caption=   "No Retur"
         Columns(0).Name =   "No Beli"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   1
         Columns(1).Width=   2566
         Columns(1).Caption=   "Jumlah Retur"
         Columns(1).Name =   "Jumlah Beli"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).NumberFormat=   "#,##0.##"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         _ExtentX        =   7699
         _ExtentY        =   5265
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox txtJumlahUang 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   11430
      TabIndex        =   25
      Text            =   "0"
      Top             =   5715
      Width           =   2220
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      Height          =   285
      Left            =   90
      TabIndex        =   19
      Top             =   135
      Value           =   1  'Checked
      Width           =   1725
   End
   Begin VB.TextBox txtNoJual 
      Height          =   375
      Left            =   1935
      TabIndex        =   18
      Top             =   90
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   4095
      Picture         =   "frmAddBayarPiutang1.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   135
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTBayar 
      Height          =   330
      Left            =   1665
      TabIndex        =   1
      Top             =   1080
      Width           =   1320
      _ExtentX        =   2328
      _ExtentY        =   582
      _Version        =   393216
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   290914307
      CurrentDate     =   38927
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      Height          =   330
      Left            =   3150
      Picture         =   "frmAddBayarPiutang1.frx":0102
      TabIndex        =   11
      Top             =   675
      Width           =   375
   End
   Begin VB.TextBox txtSupplier 
      Height          =   330
      Left            =   1665
      TabIndex        =   0
      Top             =   675
      Width           =   1410
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   330
      Left            =   1665
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1485
      Width           =   3570
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   645
      Left            =   6285
      Picture         =   "frmAddBayarPiutang1.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   6570
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   7905
      Picture         =   "frmAddBayarPiutang1.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6570
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   645
      Left            =   4800
      Picture         =   "frmAddBayarPiutang1.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   6570
      Width           =   1230
   End
   Begin VB.Frame frTab 
      BorderStyle     =   0  'None
      Height          =   3345
      Index           =   0
      Left            =   360
      TabIndex        =   32
      Top             =   2520
      Visible         =   0   'False
      Width           =   7665
      Begin SSDataWidgets_B.SSDBGrid DBGrid 
         Height          =   2985
         Left            =   180
         TabIndex        =   34
         Top             =   135
         Width           =   7110
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   5
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   450
         ExtraHeight     =   318
         Columns.Count   =   5
         Columns(0).Width=   2884
         Columns(0).Caption=   "No Penjualan"
         Columns(0).Name =   "No Beli"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   1
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "No Invoice"
         Columns(1).Name =   "No Invoice"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   2249
         Columns(2).Caption=   "Tanggal"
         Columns(2).Name =   "Tanggal"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   2566
         Columns(3).Caption=   "Sisa Piutang"
         Columns(3).Name =   "Jumlah Beli"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2328
         Columns(4).Caption=   "Jumlah Bayar"
         Columns(4).Name =   "Jumlah Bayar"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   5
         Columns(4).FieldLen=   256
         _ExtentX        =   12541
         _ExtentY        =   5265
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   4065
      Left            =   135
      TabIndex        =   31
      Top             =   2025
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   7170
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Penjualan"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Retur"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label14 
      Caption         =   "Bonus"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9315
      TabIndex        =   40
      Top             =   4770
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.Label Label11 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9315
      TabIndex        =   38
      Top             =   4275
      Width           =   1320
   End
   Begin VB.Label Label10 
      Caption         =   "Retur"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9315
      TabIndex        =   37
      Top             =   3780
      Width           =   1320
   End
   Begin VB.Label Label3 
      Caption         =   "Hutang"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9315
      TabIndex        =   36
      Top             =   3285
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Jml Nota"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   765
      TabIndex        =   30
      Top             =   6435
      Width           =   1320
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2250
      TabIndex        =   29
      Top             =   6435
      Width           =   1185
   End
   Begin VB.Label lblTotalBayar 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11385
      TabIndex        =   28
      Top             =   4275
      Width           =   2265
   End
   Begin VB.Label Label17 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10755
      TabIndex        =   27
      Top             =   4275
      Width           =   555
   End
   Begin VB.Label Label12 
      Caption         =   "Jumlah Bayar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9315
      TabIndex        =   26
      Top             =   5760
      Width           =   2040
   End
   Begin VB.Label Label19 
      Caption         =   "Sisa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9315
      TabIndex        =   24
      Top             =   5265
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label16 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11070
      TabIndex        =   23
      Top             =   5265
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label lblSisa 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   11610
      TabIndex        =   22
      Top             =   5265
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.Label Label13 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10755
      TabIndex        =   21
      Top             =   3285
      Width           =   555
   End
   Begin VB.Label lbltotalretur 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11385
      TabIndex        =   20
      Top             =   3780
      Width           =   2265
   End
   Begin VB.Label Label9 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   16
      Top             =   1125
      Width           =   105
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal"
      Height          =   240
      Left            =   90
      TabIndex        =   15
      Top             =   1125
      Width           =   1320
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   14
      Top             =   1530
      Width           =   105
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      Height          =   240
      Left            =   90
      TabIndex        =   13
      Top             =   1530
      Width           =   1320
   End
   Begin VB.Label lblNmSupplier 
      Caption         =   "-"
      Height          =   240
      Left            =   3780
      TabIndex        =   12
      Top             =   720
      Width           =   2940
   End
   Begin VB.Label Label7 
      Caption         =   "Supplier"
      Height          =   240
      Left            =   90
      TabIndex        =   10
      Top             =   720
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   9
      Top             =   720
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1980
      TabIndex        =   8
      Top             =   90
      Width           =   2040
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11385
      TabIndex        =   7
      Top             =   3285
      Width           =   2265
   End
   Begin VB.Label Label1 
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10755
      TabIndex        =   6
      Top             =   3780
      Width           =   555
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
      End
   End
End
Attribute VB_Name = "frmAddBayarPiutang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryjual As String = "select * from q_piutang "
Const queryRetur As String = "select * from q_returjual"
Const querybayar As String = "select * from t_bayarpiutangh"
Const querycustomer As String = "select * from ms_customer"
Dim total, totalretur, totalbayar As Currency
Dim seltab As Byte
Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from T_BAYARpiutangH where left(ID,2)='" & Format(DTBayar, "yy") & "' and substring(ID,3,2)='" & Format(DTBayar, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = Format(DTBayar, "yy") & Format(DTBayar, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = Format(DTBayar, "yy") & Format(DTBayar, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub chkNumbering_Click()
    If chkNumbering.Value = "1" Then
        txtNoJual.Visible = False
    Else
        txtNoJual.Visible = True
    End If
End Sub

Private Sub cmdHapus_Click()
On Error GoTo err
Dim mode
    mode = 0
    If MsgBox("Apakah anda yakin hendak menghapus?", vbYesNo) = vbYes Then
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    mode = 1
    conn.Execute "delete from t_bayarpiutangh where id='" & lblNoTrans & "'"
    conn.Execute "delete from t_bayarpiutang_jual where id='" & lblNoTrans & "'"
    
    conn.Execute "delete from t_bayarpiutang_retur where id='" & lblNoTrans & "'"
    conn.CommitTrans
    mode = 0
    conn.Close
    MsgBox "Data sudah dihapus"
    reset_form
    End If
    Exit Sub
err:
    If mode = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = querycustomer
    frmSearch.nmform = "frmAddBayarpiutang"
    frmSearch.nmctrl = "txtSupplier"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodecustomer"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.query = querybayar
    frmSearch.nmform = "frmAddBayarpiutang"
    frmSearch.nmctrl = "lblnotrans"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
 
End Sub
Public Sub cek_notrans()
Dim rs As New ADODB.Recordset
'    reset_form
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_bayarpiutangh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTBayar = rs("tanggal")
        
        txtSupplier.text = rs("kode_customer")

        txtKeterangan.text = rs("keterangan")
        txtJumlahUang.text = rs("jumlah bayar")

        rs.Close
        total = 0
        DBGrid.MoveFirst
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select td.id,th.id,th.tanggal,sum(qty*harga) as jumlah,bp.[jumlah bayar] from (t_penjualanh th inner join t_penjualand td on th.id=td.id) inner join t_bayarpiutang_jual bp on td.id=bp.no_jual " & _
            " where th.[kode customer]='" & txtSupplier.text & "' and bp.id='" & lblNoTrans & "' group by td.id,th.id,th.tanggal,bp.[jumlah bayar]", conn
        
        Dim rs2 As New ADODB.Recordset
        
        While Not rs.EOF
            
            rs2.Open "select sum([jumlah bayar]) from t_bayarpiutang_jual where no_jual='" & DBGrid.Columns(0).text & "' and id<'" & lblNoTrans & "'", conn
            
            DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & Format(rs(2), "dd/MM/yyyy") & "#" & Format(rs(3) - IIf(IsNull(rs2(0)), 0, rs2(0)), "#,##0.###") & "#" & Format(rs(4), "#,##0.###") '
            total = total + rs(4)
            If rs2.State Then rs2.Close
'            cek_kodebarang
'            DBGrid.Columns(4).text = Format(rs(4), "#,##0.###")
'            total = total + rs(4)
'            DBGrid.MoveNext
            rs.MoveNext
        Wend
        DBGrid.MoveFirst
        lblQty = DBGrid.Rows
        total = Format(total, "#,##0.###")
        lblTotal = Format(total, "#,##0.###")
        rs.Close
        totalretur = 0
        DBGrid1.RemoveAll
        DBGrid1.FieldSeparator = "#"
        rs.Open "select d.[no_retur], d.[jumlah] from t_bayarpiutang_retur d where d.id='" & lblNoTrans & "'", conn
        While Not rs.EOF
            DBGrid1.AddItem rs(0) & "#" & Format(rs(1), "#,##0.###")
            totalretur = totalretur + rs(1)
            rs.MoveNext
        Wend
        totalretur = Format(totalretur, "#,##0.###")
        lbltotalretur = Format(totalretur, "#,##0.###")
        rs.Close

        totalbayar = Format(total - totalretur, "#,##0.###")
        lblTotalBayar = Format(totalbayar, "#,##0.###")
        lblSisa = Format(total - totalretur - totalbayar - txtBonus.text, "#,##0.###")
    End If
    If rs.State Then rs.Close
    conn.Close
    cek_kodecustomer
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim counter As Integer
On Error GoTo err
    If Format(txtJumlahUang.text, "#,##0.###") <> Format(total - totalretur - txtBonus.text, "#,##0.###") Then
        MsgBox "Jumlah uang harus sama dengan jumlah sisa"
        txtJumlahUang.text = Format(total - totalretur - txtBonus.text, "#,##0.###")
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    
    If chkNumbering <> "1" Then
    rs.Open "select * from t_bayarpiutangh where id='" & txtNoJual.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Nomor ID " & txtNoJual.text & " sudah digunakan, silahkan menggunakan ID yang lain"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
    End If
    conn.BeginTrans
    i = 1
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
    Else
        conn.Execute "delete from t_bayarpiutangh where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarpiutang_jual where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarpiutang_retur where ID='" & lblNoTrans & "'"
        
    End If
    If chkNumbering = "0" Then lblNoTrans = txtNoJual.text
    add_dataheader
    counter = 0
    DBGrid.MoveFirst
    While counter < DBGrid.Rows
        If DBGrid.Columns(0).text <> "" Then add_datadetail
        DBGrid.MoveNext
        counter = counter + 1
    Wend
    counter = 0
    DBGrid1.MoveFirst
    While counter < DBGrid1.Rows
        If DBGrid1.Columns(0).text <> "" Then add_datadetail1
        DBGrid1.MoveNext
        counter = counter + 1
    Wend
    counter = 0
   
    conn.CommitTrans
    i = 0
    conn.Close
    MsgBox "Data sudah tersimpan"
    reset_form
    'DBGrid.SetFocus
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    lblNmcustomer = ""
    txtSupplier.text = ""
    txtKeterangan.text = ""
    txtJumlahUang.text = "0"
    DTBayar = Now
    
    totalretur = 0
    totalbayar = 0
    DBGrid.RemoveAll
    DBGrid1.RemoveAll

    total = 0
    lblTotal = 0
    lblTotalBayar = 0
    lbltotalretur = 0
    txtBonus.text = "0"
    lblSisa = 0
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "T_BAYARpiutangH"
    fields(0) = "id"
    fields(1) = "kode_customer"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "[JUMLAH BAYAR]"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = txtSupplier.text
    nilai(2) = Format(DTBayar, "MM/dd/yyyy")
    nilai(3) = txtKeterangan.text
    nilai(4) = Format(txtJumlahUang.text, "###0.###")
    
    tambah_data table_name, fields, nilai
End Sub
Private Sub add_datadetail()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(3)
    ReDim nilai(3)
    
    table_name = "t_bayarpiutang_jual"
    fields(0) = "ID"
    fields(1) = "NO_jual"
    fields(2) = "[JUMLAH BAYAR]"
    
            
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(0).text
    nilai(2) = Format(DBGrid.Columns(4).text, "###0.###")
    
       
    tambah_data table_name, fields, nilai
End Sub
Private Sub add_datadetail1()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(2)
    ReDim nilai(2)
    
    table_name = "T_BAYARpiutang_RETUR"
    fields(0) = "ID"
    fields(1) = "NO_RETUR"
    fields(2) = "JUMLAH"
            
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid1.Columns(0).text
    nilai(2) = DBGrid1.Columns(1).text
       
    tambah_data table_name, fields, nilai
End Sub


Private Sub DBGrid_AfterColUpdate(ByVal ColIndex As Integer)
    If ColIndex = 4 Then
        If Not IsNumeric(DBGrid.Columns(4).text) Then
            DBGrid.Columns(4).text = DBGrid.Columns(3).text
        Else
            If DBGrid.Columns(4).text = 0 Then DBGrid.Columns(4).text = DBGrid.Columns(3).text
        End If
        total = total + DBGrid.Columns(4).text
    ElseIf ColIndex = 0 Then
        If DBGrid.Columns(0).text <> "" Then cek_jual
    End If
    
    lblTotal = Format(total, "#,##0.###")
    lblSisa = Format(total - totalretur - txtBonus.text, "#,##0.###")
    'DBGrid.Update
End Sub

Private Sub DBGrid_AfterDelete(RtnDispErrMsg As Integer)
    lblQty = DBGrid.Rows
End Sub

Private Sub DBGrid_AfterInsert(RtnDispErrMsg As Integer)
    lblQty = DBGrid.Rows
End Sub

Private Sub DBGrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If ColIndex = 4 Then
    If IsNumeric(OldValue) Then total = total - OldValue
    End If
    
End Sub

Private Sub DBGrid_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
    total = total - DBGrid.Columns(4).text
    lblTotal = Format(total, "#,##0.###")
    lblSisa = Format(total - totalretur - txtBonus.text, "#,##0.###")
End Sub

Private Sub DBGrid_BtnClick()
Dim newquery, group As String

    newquery = "select id ,th.[kode customer],th.tanggal,jumlah-bayar as Total" & _
            " from Q_piutang th "

    group = "group by id,th.tanggal,v.jumlah,kode_customer,keterangan,jual_id"
    If txtSupplier.text <> "" Then
        frmSearch.query = newquery & " where [kode customer]='" & txtSupplier.text & "' and jumlah>bayar"
    Else
        frmSearch.query = newquery & " where jumlah>bayar"
    End If
    frmSearch.nmform = "frmAddBayarpiutang"
    frmSearch.group = group
    frmSearch.nmctrl = "DBGrid"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = 0
    
    frmSearch.proc = "cek_jual"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub cek_kodecustomer()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_customer where [kode customer]='" & txtSupplier.text & "'", conn
    If Not rs.EOF Then
        lblNmcustomer = rs(1)
        
    Else
        lblNmcustomer = ""
        text1 = ""
        Text2 = ""
        Text3 = ""
        Text4 = ""
    End If
    rs.Close
    conn.Close
End Sub
Public Sub cek_jual()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim jumlah, sudahbayar As Long
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select td.id,th.id,th.tanggal,sum(qty*harga) as jumlah from t_penjualanh th inner join t_penjualand td on th.id=td.id" & _
            " where th.id='" & DBGrid.Columns(0).text & "' and th.[kode customer]='" & txtSupplier.text & "' group by td.id,th.id,th.tanggal", conn
    
    If Not rs.EOF Then
        If IsNumeric(DBGrid.Columns(4).text) Then total = total - DBGrid.Columns(4).text
        DBGrid.Columns(1).text = rs(1)
        DBGrid.Columns(2).text = Format(rs("tanggal"), "dd/MM/yyyy")
        jumlah = rs("jumlah")
        rs.Close
        If lblNoTrans <> "-" Then
            rs.Open "select sum([jumlah bayar]) from t_bayarpiutang_jual where no_jual='" & DBGrid.Columns(0).text & "'", conn
        Else
            rs.Open "select sum([jumlah bayar]) from t_bayarpiutang_jual where no_jual='" & DBGrid.Columns(0).text & "' and id<'" & lblNoTrans & "'", conn
        End If
        If Not rs.EOF Then
            sudahbayar = IIf(IsNull(rs(0)), 0, rs(0))
        Else
            sudahbayar = 0
        End If
        DBGrid.Columns(3).text = Format(jumlah - sudahbayar, "#,##0.###")
        DBGrid.Columns(4).text = Format(jumlah - sudahbayar, "#,##0.###")
        'total = total + jumlah - sudahbayar
    Else
        If IsNumeric(DBGrid.Columns(4).text) Then total = total - DBGrid.Columns(4).text
        DBGrid.Columns(1).text = ""
        DBGrid.Columns(2).text = ""
        DBGrid.Columns(3).text = 0
        DBGrid.Columns(4).text = 0
    End If
    rs.Close
    conn.Close
    Set conn = Nothing
    Set rs = Nothing
    lblTotal = Format(total, "#,##0.###")
    lblSisa = Format(total - totalretur - totalbayar - txtBonus.text, "#,##0.###")
End Sub
Public Sub cek_retur()
    'If IsNumeric(DBGrid1.Columns(1).text) Then totalretur = totalretur - DBGrid1.Columns(1).text
    conn.ConnectionString = strcon
    conn.Open
    If lblNoTrans = "-" Then
    rs.Open "select h.id,sum(qty*harga_jual) as jumlah from (t_returjualh h inner join t_returjuald d on h.id=d.id) inner join t_penjualanh ph on ph.id=d.no_nota  where h.id='" & DBGrid1.Columns(0).text & "' and kode_customer='" & txtSupplier.text & "' and id not in (select no_retur from t_bayarpiutang_retur) group by id", conn
    Else
    rs.Open "select h.id,sum(qty*harga_jual) as jumlah from (t_returjualh h inner join t_returjuald d on h.id=d.id) inner join t_penjualanh ph on ph.id=h.no_nota  where h.id='" & DBGrid1.Columns(0).text & "' and [kode customer]='" & txtSupplier.text & "' and h.id not in (select no_retur from t_bayarpiutang_retur where id<>'" & lblNoTrans & "') group by h.id", conn
    End If
    If Not rs.EOF Then
        If IsNumeric(DBGrid1.Columns(1).text) Then totalretur = totalretur - DBGrid1.Columns(1).text
        
        DBGrid1.Columns(1).text = Format(rs("jumlah"), "#,##0")
        totalretur = totalretur + rs("jumlah")
    Else
        
        DBGrid1.Columns(1).text = 0
    End If
    rs.Close
    conn.Close
    totalretur = Format(totalretur, "#,##0.###")
    lbltotalretur = Format(totalretur, "#,##0.###")
    lblSisa = Format(total - totalretur - txtBonus.text, "#,##0.###")
    lblTotalBayar = lblSisa
End Sub

Private Sub DBGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
    If DBGrid.col = 0 And DBGrid.Columns(0).text <> "" Then
        cek_jual
    End If
    If DBGrid.col = 4 And DBGrid.Columns(4).text = "0" Then
        DBGrid.Columns(4).text = DBGrid.Columns(3).text
'        total = total + DBGrid.Columns(4).text
    End If
    MySendKeys "{tab}"
    End If
End Sub

Private Sub DBGrid1_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
    totalretur = totalretur - DBGrid1.Columns(1).text
    lbltotalretur = Format(totalretur, "#,##0.###")
    lblSisa = Format(total - totalretur - txtBonus.text, "#,##0.###")
End Sub

Private Sub DBGrid1_BtnClick()
    If txtSupplier.text <> "" Then
        frmSearch.query = queryRetur & " where  [kode customer]='" & txtSupplier.text & "' and id not in (select distinct no_retur from t_bayarpiutang_retur)"
    Else
        frmSearch.query = queryRetur & " where id not in (select distinct no_retur from t_bayarpiutang_retur)"
    End If
    frmSearch.nmform = "frmAddBayarpiutang"
    frmSearch.nmctrl = "DBGrid1"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = 0
    
    frmSearch.proc = "cek_retur"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub DBGrid1_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub DBGrid1_KeyUp(KeyCode As Integer, Shift As Integer)
    If DBGrid1.col = 0 And DBGrid1.Columns(0).text <> "" Then
        cek_retur
    End If
End Sub


Private Sub Form_Activate()
    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearch_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub Form_Load()
    reset_form
    seltab = 0
    frTab(0).Visible = True
End Sub


Private Sub mnuKeluar_Click()
    cmdKeluar_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub TabStrip1_Click()
    frTab(seltab).Visible = False
    frTab(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub txtBonus_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        lblTotalBayar = Format(totalbayar, "#,##0.###")
        lblSisa = Format(total - totalretur - totalbayar - txtBonus.text, "#,##0.###")
    End If
End Sub

Private Sub txtBonus_LostFocus()
    If Not IsNumeric(txtBonus.text) Then txtBonus.text = "0"
    lblTotalBayar = Format(totalbayar, "#,##0.###")
    lblSisa = Format(total - totalretur - totalbayar - txtBonus.text, "#,##0.###")
End Sub

Private Sub txtJumlahUang_LostFocus()
    If Not IsNumeric(txtJumlahUang.text) Then txtJumlahUang.text = "0"
    
End Sub

Private Sub txtSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cek_kodecustomer
    End If
End Sub
