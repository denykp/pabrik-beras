VERSION 5.00
Begin VB.Form frmMasterGudang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gudang"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6420
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   6420
   Begin VB.CommandButton cmdLoadRpt 
      Caption         =   "Load History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3720
      Picture         =   "frmMasterGudang.frx":0000
      TabIndex        =   30
      Top             =   360
      Width           =   1320
   End
   Begin VB.Frame frAccTinta 
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   45
      TabIndex        =   25
      Top             =   2475
      Visible         =   0   'False
      Width           =   4695
      Begin VB.TextBox txtAccTinta 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         MaxLength       =   25
         TabIndex        =   27
         Top             =   135
         Width           =   1950
      End
      Begin VB.CommandButton cmdSearchAccPersediaan 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3825
         TabIndex        =   26
         Top             =   165
         Width           =   465
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Acc Persediaan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   29
         Top             =   120
         Width           =   1305
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1575
         TabIndex        =   28
         Top             =   135
         Width           =   105
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3365
      Picture         =   "frmMasterGudang.frx":0102
      TabIndex        =   7
      Top             =   3465
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   60
      TabIndex        =   21
      Top             =   1530
      Visible         =   0   'False
      Width           =   5205
      Begin VB.ComboBox cmbJenisMesin 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmMasterGudang.frx":0204
         Left            =   1800
         List            =   "frmMasterGudang.frx":020E
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   90
         Width           =   1605
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Jenis Mesin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   60
         TabIndex        =   24
         Top             =   150
         Width           =   1320
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1545
         TabIndex        =   23
         Top             =   150
         Width           =   105
      End
   End
   Begin VB.ComboBox cmbTinta 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterGudang.frx":0224
      Left            =   1830
      List            =   "frmMasterGudang.frx":022E
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   2100
      Width           =   1605
   End
   Begin VB.OptionButton optMesin 
      Caption         =   "Mesin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2970
      TabIndex        =   3
      Top             =   1290
      Width           =   1485
   End
   Begin VB.OptionButton optNonMesin 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1830
      TabIndex        =   2
      Top             =   1230
      Width           =   1185
   End
   Begin VB.TextBox txtNama 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1830
      MaxLength       =   35
      TabIndex        =   1
      Top             =   750
      Width           =   3840
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4695
      Picture         =   "frmMasterGudang.frx":0244
      TabIndex        =   8
      Top             =   3465
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2035
      Picture         =   "frmMasterGudang.frx":0346
      TabIndex        =   6
      Top             =   3465
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3315
      Picture         =   "frmMasterGudang.frx":0448
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   360
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1830
      MaxLength       =   10
      TabIndex        =   0
      Top             =   360
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   705
      Picture         =   "frmMasterGudang.frx":054A
      TabIndex        =   5
      Top             =   3465
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   20
      Top             =   2130
      Width           =   105
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   19
      Top             =   2130
      Width           =   1320
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   18
      Top             =   1260
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Status "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   17
      Top             =   1260
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   780
      Width           =   1260
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   15
      Top             =   795
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1470
      TabIndex        =   14
      Top             =   795
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   13
      Top             =   3015
      Width           =   2130
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   12
      Top             =   405
      Width           =   150
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1650
      TabIndex        =   10
      Top             =   405
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   405
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterGudang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmbTinta_Click()
    If Trim(cmbTinta.text) = "Tinta" Then frAccTinta.Visible = True Else frAccTinta.Visible = False
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0

    If txtGudang.text = "" Then
        MsgBox "Silahkan masukkan Kode gudang terlebih dahulu!", vbCritical
        txtGudang.SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    If cmbTinta.ListIndex = 1 Then  ' tinta
        rs.Open "select * from stock_tinta where kode_gudang='" & txtGudang & "'", conn
    Else
        rs.Open "select * from stock where kode_gudang='" & txtGudang & "'", conn
    End If
    
    If rs.EOF Then
        conn.Execute "delete from ms_gudang where kode_gudang='" & txtGudang.text & "'"
        conn.Execute "delete from var_gudang where kode_gudang='" & txtGudang.text & "'"
        MsgBox "Data gudang sudah terhapus"
    Else
        MsgBox "Data gudang sudah terpakai!"
    End If
    rs.Close
    conn.CommitTrans
    i = 0
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoadRpt_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select * from rpt_ms_gudang"
    frmSearch.nmform = "frmMasterGudang"
    frmSearch.nmctrl = "txtgudang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_gudang"
    frmSearch.proc = "cari_data"
    frmSearch.Col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = "select * from ms_gudang"
    frmSearch.nmform = "frmMasterGudang"
    frmSearch.nmctrl = "txtgudang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_gudang"
    frmSearch.proc = "cari_data"
    frmSearch.Col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccPersediaan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='110.040'"
    frmSearch.nmform = "frmMasterGudang"
    frmSearch.nmctrl = "txtAccTinta"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtGudang.text = "" Then
        MsgBox "Kode Gudang harus diisi"
        txtGudang.SetFocus
        Exit Sub
    End If
    
    If txtNama.text = "" Then
        MsgBox "Nama Gudang harus diisi"
        txtNama.SetFocus
        Exit Sub
    End If
        
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    
    If txtGudang <> "" Then
        conn.Execute "delete from ms_gudang where kode_gudang='" & txtGudang & "'"
    End If
    add_dataheader
    conn.Execute "if Exists(select kode_gudang from var_gudang where kode_gudang='" & txtGudang & "') update var_gudang set keterangan='" & txtNama & "' where kode_gudang='" & txtGudang & "' else insert into var_gudang (kode_gudang,keterangan) values ('" & txtGudang & "','" & txtNama & "')"
    conn.Execute "delete from setting_acctinta where kode_gudang='" & txtGudang & "'"
    If cmbTinta.text = "Tinta" Then
    conn.Execute "if not exists (select * from setting_acctinta where kode_gudang='" & txtGudang & "') insert into setting_accTinta  (kode_tinta,kode_acc,kode_gudang)  select kode_tinta,'" & txtAccTinta & "','" & txtGudang & "' from ms_tinta"
    
    End If
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtGudang.text = ""
    txtNama.text = ""
    txtAccTinta = ""
    txtGudang.SetFocus
    optNonMesin.value = True
'    If optMesin.Value = True Then optMesin_Click
    cmbTinta.ListIndex = 0
    Frame1.Visible = False
'    txtGudang.SetFocus
    If cmbJenisMesin.ListCount > 0 Then cmbJenisMesin.ListIndex = 0
End Sub
Public Sub cari_data()
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_gudang where kode_gudang='" & txtGudang.text & "'", conn
    If Not rs.EOF Then
        txtNama.text = rs(1)
        txtAccTinta = rs!tinta_acc
        If rs!status = 0 Then
            optNonMesin.value = True
            Frame1.Visible = False
        Else
            optMesin.value = True
            Frame1.Visible = True
            SetComboText rs!jenis_mesin, cmbJenisMesin
        End If
        If rs!stock_tinta = 0 Then
            cmbTinta.ListIndex = 0
        Else
            cmbTinta.ListIndex = 1
        End If
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        txtNama.text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    
    conn.Open strcon
    cmbJenisMesin.Clear
    rs.Open "select * from var_jenis order by jenis_proses", conn
        While Not rs.EOF
            cmbJenisMesin.AddItem rs(0)
            cmbJenisMesin.Refresh
        rs.MoveNext
        Wend
    rs.Close
    conn.Close
    reset_form
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub




Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "ms_gudang"
    fields(0) = "kode_gudang"
    fields(1) = "nama_gudang"
    fields(2) = "stock_tinta"
    fields(3) = "status"
    fields(4) = "jenis_mesin"
    fields(5) = "tinta_acc"
    
    nilai(0) = txtGudang.text
    nilai(1) = txtNama.text
    If cmbTinta.ListIndex = 0 Then nilai(2) = 0 Else nilai(2) = 1
    If optMesin.value = True Then nilai(3) = 1 Else nilai(3) = 0
    nilai(4) = cmbJenisMesin.text
    nilai(5) = txtAccTinta
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    table_name = "rpt_ms_gudang"
    conn.Execute tambah_data2(table_name, fields, nilai)

End Sub

Private Sub optMesin_Click()
    Frame1.Visible = True
End Sub

Private Sub optNonMesin_Click()
    Frame1.Visible = False
End Sub

Private Sub txtgudang_LostFocus()
    If txtGudang <> "" Then cari_data
End Sub
