VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddOrderPembelianKertas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Order Pembelian Sheet"
   ClientHeight    =   9210
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10515
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9210
   ScaleWidth      =   10515
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3060
      Picture         =   "frmAddOrderPembelianKertas.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   8232
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   270
      Picture         =   "frmAddOrderPembelianKertas.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   8232
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4455
      Picture         =   "frmAddOrderPembelianKertas.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   8232
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   2
      Top             =   1695
      Width           =   3885
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3720
      Picture         =   "frmAddOrderPembelianKertas.frx":0306
      TabIndex        =   17
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8685
      TabIndex        =   16
      Top             =   165
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10170
      TabIndex        =   15
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3165
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   14
      Top             =   915
      Width           =   420
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   960
      Width           =   1530
   End
   Begin VB.TextBox txtBukti 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   525
      Width           =   1875
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1665
      Picture         =   "frmAddOrderPembelianKertas.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   8232
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7020
      TabIndex        =   18
      Top             =   555
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   270532611
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5640
      Index           =   0
      Left            =   135
      TabIndex        =   28
      Top             =   2316
      Width           =   10185
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2265
         Left            =   90
         TabIndex        =   29
         Top             =   90
         Width           =   6990
         Begin VB.TextBox txtIsi 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   5
            Top             =   990
            Width           =   1140
         End
         Begin VB.TextBox txtPalet 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   4
            Top             =   630
            Width           =   1140
         End
         Begin VB.PictureBox Picture2 
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   2790
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   31
            Top             =   225
            Width           =   465
            Begin VB.CommandButton cmdSearchBrg 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmAddOrderPembelianKertas.frx":050A
               TabIndex        =   32
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   90
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   30
            Top             =   1710
            Width           =   3795
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   10
               Top             =   45
               Width           =   1050
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   9
               Top             =   45
               Width           =   1050
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   8
               Top             =   45
               Width           =   1350
            End
         End
         Begin VB.TextBox txtHarga 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   6
            Top             =   1350
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5580
            TabIndex        =   7
            Top             =   1485
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.TextBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   3
            Top             =   270
            Width           =   1500
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Qty Isi"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   49
            Top             =   1035
            Width           =   600
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Qty Palet"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   48
            Top             =   675
            Width           =   765
         End
         Begin VB.Label lblNamaBarang2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3285
            TabIndex        =   46
            Top             =   720
            Width           =   3480
         End
         Begin VB.Label Label8 
            BackColor       =   &H8000000C&
            Caption         =   "Harga"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   40
            Top             =   1395
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblDefQtyJual 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   270
            TabIndex        =   39
            Top             =   1395
            Width           =   870
         End
         Begin VB.Label lblID 
            Caption         =   "Label12"
            ForeColor       =   &H8000000F&
            Height          =   240
            Left            =   270
            TabIndex        =   38
            Top             =   1395
            Visible         =   0   'False
            Width           =   870
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   37
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            Caption         =   "Qty"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4500
            TabIndex        =   36
            Top             =   1530
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3285
            TabIndex        =   35
            Top             =   315
            Width           =   3480
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   34
            Top             =   1305
            Width           =   600
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2520
            TabIndex        =   33
            Top             =   675
            Width           =   1050
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2565
         Left            =   90
         TabIndex        =   41
         Top             =   2670
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   4524
         _Version        =   393216
         Cols            =   9
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label17 
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   45
         Top             =   5265
         Width           =   735
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3465
         TabIndex        =   44
         Top             =   5265
         Width           =   1140
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   43
         Top             =   5265
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   42
         Top             =   5265
         Width           =   735
      End
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   336
      Left            =   144
      TabIndex        =   50
      Top             =   1296
      Visible         =   0   'False
      Width           =   492
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   27
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   26
      Top             =   1695
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5535
      TabIndex        =   25
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label7 
      Caption         =   "No. Kontrak"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   24
      Top             =   600
      Width           =   1275
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   23
      Top             =   1005
      Width           =   960
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1605
      TabIndex        =   22
      Top             =   1365
      Width           =   3120
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8100
      TabIndex        =   21
      Top             =   7980
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7155
      TabIndex        =   20
      Top             =   7980
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   19
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddOrderPembelianKertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Private Sub chkNumbering_Click()
    If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub


Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    lblNamaBarang2 = ""
    txtQty.text = "1"
    txtPalet.text = "1"
    txtIsi.text = "1"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub


Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    
    row = flxGrid.row
    
    totalQty = totalQty - (flxGrid.TextMatrix(row, 6))
    total = total - (flxGrid.TextMatrix(row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblTotal = Format(total, "#,##0")

    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoad_Click()

End Sub

Private Sub cmdLoad2_Click()

End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    txtQty = CDbl(txtPalet) * CDbl(txtIsi)
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text And flxGrid.TextMatrix(i, 5) = txtIsi.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang1
        flxGrid.TextMatrix(row, 3) = lblNamaBarang2
        If flxGrid.TextMatrix(row, 4) = "" Then
            flxGrid.TextMatrix(row, 4) = txtPalet
        Else
            If mode = 1 Then
                flxGrid.TextMatrix(row, 4) = CDbl(flxGrid.TextMatrix(row, 4)) + CDbl(txtPalet)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 4) = CDbl(txtPalet)
            End If
        End If
        flxGrid.TextMatrix(row, 5) = txtIsi
        If flxGrid.TextMatrix(row, 6) = "" Then
            flxGrid.TextMatrix(row, 6) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 6))
            total = total - (flxGrid.TextMatrix(row, 8))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 6) = CDbl(flxGrid.TextMatrix(row, 6)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 6) = CDbl(txtQty)
            End If
        End If
        
        flxGrid.TextMatrix(row, 7) = txtHarga.text
        flxGrid.TextMatrix(row, 8) = flxGrid.TextMatrix(row, 6) * flxGrid.TextMatrix(row, 7)
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(row, 6)
        total = total + (flxGrid.TextMatrix(row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblTotal = Format(total, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        
        
        
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        txtQty.text = "1"
        txtIsi.text = "1"
        txtPalet.text = "1"
        txtHarga.text = "0"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    
    'lblItem = flxGrid.Rows - 2
End Sub


Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\Transferpembelian.rpt"
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'        .SelectionFormula = " {t_orderbelikertash.ID}='" & lblNoTrans & "'"
'        .Destination = crptToWindow
'        .ParameterFields(0) = "login;" + user + ";True"
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
'        .action = 1
'    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Qty Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
'    If Not Cek_Serial Then
'        MsgBox "Ada Serial Number yang sudah terpakai"
'        Exit Sub
'    End If
    Simpan
    If posting_orderbelikertas(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean

    Cek_qty = True
End Function

Private Sub cmdreset_Click()
    reset_form
End Sub



Private Sub cmdSearchBrg_Click()
    frmSearch.query = SearchKertas
    frmSearch.nmform = "frmAddOrderpembelianKertas"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchOrderBeliKertas & " where status_finish='0'"
    frmSearch.nmform = "frmAddOrderpembelianKertas"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "beliKertasH"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchSupplier_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmAddOrderpembelianKertas"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If Simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function Simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    Simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If txtKodeSupplier.text = "" Then
        MsgBox "Silahkan masukkan Supplier terlebih dahulu"
        txtKodeSupplier.SetFocus
        Exit Function
    End If
    
    
    id = lblNoTrans
    If lblNoTrans = "-" And chkNumbering = "1" Then
        lblNoTrans = newid("t_orderbelikertash", "nomer_orderbelikertas", DTPicker1, "OBK")
    End If
    conn.ConnectionString = strcon
    conn.Open
    If chkNumbering <> "1" Then
    rs.Open "select * from t_orderbelikertash where id='" & txtID.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Nomor ID " & txtID.text & " sudah digunakan, silahkan menggunakan ID yang lain"
        rs.Close
        DropConnection
        Exit Function
    End If
    rs.Close
    End If
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_orderbelikertash where nomer_orderbelikertas='" & id & "'"
    conn.Execute "delete from t_orderbelikertasd where nomer_orderbelikertas='" & id & "'"
    
    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    conn.CommitTrans
    i = 0
    Simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtNoKontrak.text = ""
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    status_posting = False
    totalQty = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    
    cmdClear_Click
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    
    
    TabStrip1.Tabs(1).Selected = True
    
    txtNoKontrak.SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(8)
    ReDim nilai(8)
    table_name = "t_orderbelikertash"
    fields(0) = "nomer_orderbelikertas"
    fields(1) = "tanggal_orderbelikertas"

    fields(2) = "kode_supplier"
    fields(3) = "nomer_kontrak"
    fields(4) = "total"
    fields(5) = "keterangan"
    fields(6) = "userid"
    fields(7) = "status_posting"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    
    nilai(2) = txtKodeSupplier.text
    nilai(3) = txtNoKontrak.text
    nilai(4) = Replace(Format(lblTotal, "###0.##"), ",", ".")
    nilai(5) = txtKeterangan.text
    nilai(6) = User
    nilai(7) = CInt(status_posting) * -1
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(7)
    ReDim nilai(7)

    table_name = "t_orderbelikertasd"
    fields(0) = "nomer_orderbelikertas"
    fields(1) = "kode_bahan"
    fields(2) = "qty_palet"
    fields(3) = "qty_isi"
    fields(4) = "qty"
    fields(5) = "NO_URUT"
    fields(6) = "HARGA"
    

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = Format(flxGrid.TextMatrix(row, 4), "###0")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    nilai(4) = Replace(Format(flxGrid.TextMatrix(row, 6), "###0.##"), ",", ".")
    nilai(5) = row
    nilai(6) = Replace(Format(flxGrid.TextMatrix(row, 7), "###0.##"), ",", ".")
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_orderbelikertash where nomer_orderbelikertas='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        
        DTPicker1 = rs("tanggal_orderbelikertas")
        
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        txtNoKontrak.text = rs("nomer_kontrak")
        totalQty = 0
        total = 0
        status_posting = rs!status_posting
'        cmdPrint.Visible = True
        If rs("status_posting") = 1 Then
'            cmdPrint.Enabled = False
'            cmdSimpan.Enabled = False
'            mnuSave.Enabled = False
            cmdPosting.Visible = False
        Else
'            cmdSimpan.Enabled = True
            cmdPosting.Visible = True
'            mnuSave.Enabled = True
        End If
        
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.harga,panjang,lebar,berat,qty_palet,qty_isi from t_orderbelikertasd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_orderbelikertas='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(4) & "x" & rs(5) & "x" & rs(6)
                flxGrid.TextMatrix(row, 4) = rs(7)
                flxGrid.TextMatrix(row, 5) = rs(8)
                flxGrid.TextMatrix(row, 6) = rs(2)
                flxGrid.TextMatrix(row, 7) = Format(rs(3), "#,##0")
                flxGrid.TextMatrix(row, 8) = Format(rs(2) * rs(3), "#,##0")
                row = row + 1
                totalQty = totalQty + rs(2)
                total = total + (rs(2) * rs(3))
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = totalQty
        lblTotal = Format(total, "#,##0.00")
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
                lblNamaBarang2 = flxGrid.TextMatrix(flxGrid.row, 3)
                End If
            txtPalet.text = flxGrid.TextMatrix(flxGrid.row, 4)
            txtIsi.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 7)
            txtPalet.SetFocus
        End If

End Sub
Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtPalet.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtQtyPalet.SetFocus
End Sub





Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    
    
    reset_form
    total = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 1200
    flxGrid.ColWidth(4) = 900
    flxGrid.ColWidth(5) = 900
    flxGrid.ColWidth(6) = 0
    flxGrid.ColWidth(7) = 900
    flxGrid.ColWidth(8) = 1500
    
    
'    If right_hpp = True Then
        
        txtHarga.Visible = True
        Label8.Visible = True
        lblTotal.Visible = True
        Label10.Visible = True
'    End If

    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Dimensi"
    flxGrid.TextMatrix(0, 4) = "Qty Palet"
    flxGrid.TextMatrix(0, 5) = "Qty Isi"
    flxGrid.TextMatrix(0, 6) = "Qty"
    flxGrid.TextMatrix(0, 7) = "Harga"
    flxGrid.TextMatrix(0, 8) = "Total"
    
    
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        lblNamaBarang2 = rs!panjang & "x" & rs!lebar & "x" & rs!berat
        lblSatuan = ""
        If lblKategori <> "" Then
        txtHarga.text = IIf(IsNull(rs("harga_beli" & lblKategori)), 0, rs("harga_beli" & lblKategori))
        End If
        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier],kategori_harga from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
        lblKategori = rs(1)
        
    Else
        lblNamaSupplier = ""
        lblKategori = ""
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()
On Error GoTo err
    conn.Open strcon
    With flxGrid
    For i = 1 To .Rows - 2
        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
        If Not rs.EOF Then
            total = total - .TextMatrix(i, 8)
            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
            total = total + .TextMatrix(i, 8)
        End If
        rs.Close
    Next
    End With
    conn.Close
    lblTotal = Format(total, "#,##0")
    Exit Sub
err:
    MsgBox err.Description
    If conn.State Then conn.Close
End Sub
Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub




Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtIsi_GotFocus()
    txtIsi.SelStart = 0
    txtIsi.SelLength = Len(txtIsi.text)
End Sub

Private Sub txtIsi_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtIsi_LostFocus()
    txtQty = CDbl(txtPalet) * CDbl(txtIsi)
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub

Private Sub txtPalet_GotFocus()
    txtPalet.SelStart = 0
    txtPalet.SelLength = Len(txtPalet.text)
End Sub

Private Sub txtPalet_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtPalet_LostFocus()
    
    txtQty = CDbl(txtPalet) * CDbl(txtIsi)
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub
