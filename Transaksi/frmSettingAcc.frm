VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSettingAcc 
   BackColor       =   &H8000000A&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6405
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15000
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   15000
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   5160
      Left            =   90
      TabIndex        =   4
      Top             =   330
      Width           =   14385
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   7
      BevelColorFrame =   -2147483641
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   503
      ExtraHeight     =   159
      Columns.Count   =   7
      Columns(0).Width=   2381
      Columns(0).Caption=   "Gudang"
      Columns(0).Name =   "Nama"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2963
      Columns(1).Caption=   "Kode"
      Columns(1).Name =   "Tanggal"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   6244
      Columns(2).Caption=   "Nama1"
      Columns(2).Name =   "Nama1"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2990
      Columns(3).Caption=   "Kode Perkiraan"
      Columns(3).Name =   "Kode Perkiraan"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   1
      Columns(4).Width=   3200
      Columns(4).Caption=   "namaAcc1"
      Columns(4).Name =   "namaAcc1"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3016
      Columns(5).Caption=   "Perkiraan2"
      Columns(5).Name =   "Perkiraan2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   1
      Columns(6).Width=   3200
      Columns(6).Caption=   "namaAcc2"
      Columns(6).Name =   "namaAcc2"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   25374
      _ExtentY        =   9102
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdSearchPerkiraan2 
      Caption         =   "F4"
      Height          =   330
      Left            =   1665
      TabIndex        =   5
      Top             =   4770
      Width           =   435
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   585
      Left            =   12300
      TabIndex        =   3
      Top             =   5640
      Width           =   1935
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   585
      Left            =   3300
      TabIndex        =   2
      Top             =   8010
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   585
      Left            =   1350
      TabIndex        =   1
      Top             =   8010
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton cmdSearchPerkiraan 
      Caption         =   "F3"
      Height          =   330
      Left            =   1080
      TabIndex        =   0
      Top             =   4830
      Width           =   435
   End
End
Attribute VB_Name = "frmSettingAcc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
'Dim Cols As TrueOleDBGrid70.Columns

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.nmform = "frmSettingAcc"
    frmSearch.nmctrl = "txtKode"
    frmSearch.nmctrl2 = ""
Select Case filename
    Case "Tinta"
        frmSearch.query = "Select * from ms_tinta"
        frmSearch.proc = "cari_tinta"
    Case "Bahan"
        frmSearch.query = "Select * from ms_bahan"
        frmSearch.proc = "cari_bahan"
    Case "Customer"
        frmSearch.query = SearchCustomer
        frmSearch.proc = "cari_customer"
        
    Case "Supplier"
        frmSearch.query = "Select * from ms_supplier"
        frmSearch.proc = "cari_supplier"
        
End Select
    frmSearch.Col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchPerkiraan2_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1"
    frmSearch.nmform = "frmMasterSupplier"
    frmSearch.nmctrl = "SSDBGrid1"
    frmSearch.nmctrl2 = "SSDBGrid1"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.col2 = 1
    frmSearch.Index = 5
    frmSearch.Index2 = 6
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Integer
On Error GoTo err
    If flxGrid.TextMatrix(flxGrid.Row, 1) = "" Then Exit Sub
 i = 0
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
  i = 1
    For J = 1 To flxGrid.Rows - 2
        add_data J
    Next J
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub
Private Sub cmdSearchPerkiraan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1"
    frmSearch.nmform = "frmSettingAcc"
    frmSearch.nmctrl = "SSDBGrid1"
    frmSearch.nmctrl2 = "SSDBGrid1"
    
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.col2 = 1
    frmSearch.Index = 3
    frmSearch.Index2 = 4
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub Form_Load()
    
    
    Select Case filename
    Case "Tinta"
        lblKode = "Kode Tinta"
        SSDBGrid1.Columns(0).Caption = "Gudang"
        SSDBGrid1.Columns(1).Caption = "Tinta"
        SSDBGrid1.Columns(2).Caption = "Nama Tinta"
        SSDBGrid1.Columns(3).Caption = "Perkiraan"
        SSDBGrid1.Columns(4).Caption = "Nama Perkiraan"
        SSDBGrid1.Columns(5).Visible = False
        SSDBGrid1.Columns(6).Visible = False
        
    Case "Bahan"
        lblKode = "Kode Bahan"
        SSDBGrid1.Columns(0).Visible = False
        SSDBGrid1.Columns(1).Caption = "Kode Bahan"
        SSDBGrid1.Columns(2).Caption = "Nama Bahan"
        SSDBGrid1.Columns(3).Caption = "Perkiraan Persediaan"
        SSDBGrid1.Columns(4).Caption = "Ket.Persediaan "
        SSDBGrid1.Columns(5).Visible = True
        SSDBGrid1.Columns(5).Caption = "Perkiraan HPP"
        SSDBGrid1.Columns(6).Visible = True
        SSDBGrid1.Columns(6).Caption = "Ket.Hpp "
    Case "Customer"
        lblKode = "Kode Customer"
        SSDBGrid1.Columns(0).Visible = False
        SSDBGrid1.Columns(1).Caption = "Kode Customer"
        SSDBGrid1.Columns(2).Caption = "Nama Customer"
        SSDBGrid1.Columns(3).Caption = "Perkiraan Piutang"
        SSDBGrid1.Columns(4).Visible = True
        SSDBGrid1.Columns(4).Caption = "Perkiraan Hutang Retur Jual"

    Case "Supplier"
        lblKode = "Kode Supplier"
        SSDBGrid1.Columns(0).Visible = False
        SSDBGrid1.Columns(1).Caption = "Kode Supplier"
        SSDBGrid1.Columns(2).Caption = "Nama Supplier"
        SSDBGrid1.Columns(3).Caption = "Perkiraan Hutang"
        SSDBGrid1.Columns(4).Visible = True
        SSDBGrid1.Columns(4).Caption = "Perkiraan Piutang Retur Beli"
    End Select
    reset_form
'    load_data
'    cari_perkiraan
End Sub



Private Sub reset_form()
    cari_data
End Sub

Private Sub add_data(i As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)
    Select Case filename
        Case "Tinta"
            table_name = "setting_accTinta"
            fields(0) = "kode_tinta"
            fields(1) = "kode_acc"
            fields(2) = "kode_gudang"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(0).text
        Case "Bahan"
            table_name = "setting_accbahan"
            fields(0) = "kode_bahan"
            fields(1) = "acc_persediaan"
            fields(2) = "acc_hpp"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(5).text
        Case "Customer"
            table_name = "setting_accCustomer"
            fields(0) = "kode_customer"
            fields(1) = "acc_piutang"
            fields(2) = "acc_hutangRetur"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(5).text
        Case "Supplier"
            table_name = "setting_accSupplier"
            fields(0) = "kode_supplier"
            fields(1) = "acc_hutang"
            fields(2) = "acc_piutangRetur"
            nilai(0) = SSDBGrid1.Columns(1).text
            nilai(1) = SSDBGrid1.Columns(3).text
            nilai(2) = SSDBGrid1.Columns(5).text
    End Select
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub SSDBGrid1_AfterColUpdate(ByVal ColIndex As Integer)
Dim i As Byte
On Error GoTo err
i = 0
If SSDBGrid1.Columns(1).text = "" Then Exit Sub

    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
i = 1
    Select Case filename
    Case "Tinta"
        rs.Open "select * from setting_accTinta where kode_gudang='" & SSDBGrid1.Columns(0).text & "' and kode_tinta='" & SSDBGrid1.Columns(1).text & "'", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accTinta where kode_gudang='" & SSDBGrid1.Columns(0).text & "' and kode_tinta='" & SSDBGrid1.Columns(1).text & "' "
        End If
    Case "Bahan"
        rs.Open "select * from setting_accbahan where kode_bahan='" & SSDBGrid1.Columns(1).text & "' ", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accbahan where kode_bahan='" & SSDBGrid1.Columns(1).text & "' "
        End If
    Case "Customer"
        rs.Open "select * from setting_accCustomer where kode_customer='" & SSDBGrid1.Columns(1).text & "' ", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accCustomer where kode_customer='" & SSDBGrid1.Columns(1).text & "' "
        End If
    Case "Supplier"
        rs.Open "select * from setting_accSupplier where kode_supplier='" & SSDBGrid1.Columns(1).text & "' ", conn
        If Not rs.EOF Then
            conn.Execute "delete from setting_accSupplier where kode_supplier='" & SSDBGrid1.Columns(1).text & "' "
        End If
    End Select

    add_data SSDBGrid1.Col
    conn.CommitTrans
i = 0
    DropConnection
Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub


Private Sub SSDBGrid1_BtnClick()
If SSDBGrid1.Columns(1).text = "" Then Exit Sub
    If SSDBGrid1.Col = 3 Then cmdSearchPerkiraan_Click
    If SSDBGrid1.Col = 5 Then cmdSearchPerkiraan2_Click
End Sub


Private Sub txtKode_LostFocus()
'    If txtKode <> "" Then cari_data
End Sub
Private Sub cari_data()
conn.Open strcon
    SSDBGrid1.RemoveAll
    SSDBGrid1.FieldSeparator = "#"
    SSDBGrid1.Columns(0).Locked = True
    SSDBGrid1.Columns(1).Locked = True
    SSDBGrid1.Columns(2).Locked = True
    SSDBGrid1.Columns(4).Locked = True
    SSDBGrid1.Columns(6).Locked = True
    
    
    Select Case filename
        Case "Tinta"
        rs.Open " select m.kode_gudang,m.kode_tinta,m.nama_tinta,isnull(a.kode_acc,'') as kode_acc , isnull(x.nama,'') as namaAcc" & _
            " from (select a.kode_gudang,kode_tinta ,nama_tinta from ms_gudang a,ms_tinta where a.stock_tinta=1) m left join setting_acctinta a on a.kode_tinta=m.kode_tinta and a.kode_gudang=m.kode_gudang left join ms_coa x on a.kode_acc=x.kode_acc order by m.kode_gudang,m.kode_tinta,m.nama_tinta", conn 'a.status=1
                While Not rs.EOF
                 SSDBGrid1.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4)
            rs.MoveNext
            Wend
        Case "Bahan"
           rs.Open "select b.kode_bahan,b.nama_bahan ,isnull(c.acc_persediaan,'') as acc_persediaan,isnull(x.nama,'')as KetPersediaan ,isnull(c.acc_hpp,'') as acc_hpp, isnull(ms_coa1.nama,'') as KetHpp " & _
             " from (select kode_bahan ,nama_bahan from ms_bahan  ) b left join setting_accbahan c on b.kode_bahan=c.kode_bahan left join ms_coa x on c.acc_persediaan=x.kode_acc left join ms_coa as ms_coa1 on c.acc_hpp=ms_coa1.kode_acc order by b.kode_bahan ,b.nama_bahan ", conn
            While Not rs.EOF
                 SSDBGrid1.AddItem "#" & rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(5)
            rs.MoveNext
            Wend
        Case "Customer"
           rs.Open "select b.kode_customer ,nama_customer,isnull(c.acc_piutang,'') as acc_piutang,isnull(x.nama,'')as KetPiutang,isnull(c.acc_hutangRetur,'') as acc_hutangRetur, isnull(ms_coa1.nama,'') as KetRetur " & _
             " from (select kode_customer,nama_customer from ms_customer  ) b left join setting_accCustomer c on b.kode_customer=c.kode_customer left join ms_coa x on c.acc_piutang=x.kode_acc left join ms_coa as ms_coa1 on c.acc_hutangRetur=ms_coa1.kode_acc order by b.kode_customer ,b.nama_customer", conn
            While Not rs.EOF
                 SSDBGrid1.AddItem "#" & rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(5)
            rs.MoveNext
            Wend
        Case "Supplier"
           rs.Open "select b.kode_supplier,b.nama_supplier ,isnull(c.acc_hutang,'') as acc_hutang,isnull(x.nama,'')as KetHutang,isnull(c.acc_piutangRetur,'') as acc_piutangRetur , isnull(ms_coa1.nama,'') as KetPiutangRetur " & _
             " from (select kode_supplier,nama_supplier from ms_supplier ) b left join setting_accSupplier c on b.kode_supplier=c.kode_supplier left join ms_coa x on c.acc_hutang=x.kode_acc left join ms_coa as ms_coa1 on c.acc_piutangRetur=ms_coa1.kode_acc order by b.kode_supplier,b.nama_supplier ", conn

            While Not rs.EOF
                SSDBGrid1.AddItem "#" & rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(5)
            rs.MoveNext
            Wend
    End Select

rs.Close
conn.Close
End Sub

Private Sub SSDBGrid1_KeyDown(KeyCode As Integer, Shift As Integer)

'If KeyCode = vbKeyF3 Then
'    frmMasterAcc.tutup = True
'    frmMasterAcc.Show
' Select Case filename
'        Case "Tinta"
'         SSDBGrid1.Columns(3).text = frmMasterAcc.txtField(1).text
'        Case "Bahan"
'
'        Case "Customer"
'
'        Case "Supplier"
'
'    End Select
'
'
'End If
'frmMasterAcc.tutup = False
End Sub

Private Sub SSDBGrid1_LostFocus()
    SSDBGrid1.Refresh
    SSDBGrid1.Update
    
End Sub
