alter table t_sod
add harga_kemasan numeric(18,2)
go
update t_sod set harga_kemasan = 0 where harga_kemasan is null
go
alter table t_jualD
add harga_kemasan numeric(18,2)
go
update t_jualD set harga_kemasan = 0 where harga_kemasan is null
go
alter table t_jualD
add harga_kemasan_final numeric(18,2)
go
update t_jualD set harga_kemasan_final = 0 where harga_kemasan_final is null
go
alter table tmp_PenjualanVsHPP
add qty numeric(18,2)
go
alter table tmp_PenjualanVsHPP
add harga_kemasan numeric(18,2)
go
alter table t_returjualD
add harga_kemasan numeric(18,2)
go