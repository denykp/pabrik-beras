VERSION 5.00
Begin VB.Form frmGantiPassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ganti Password"
   ClientHeight    =   1905
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   5205
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1905
   ScaleWidth      =   5205
   Begin VB.TextBox txtPasscode2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2340
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   855
      Width           =   2625
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   630
      TabIndex        =   3
      Top             =   1350
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3330
      TabIndex        =   4
      Top             =   1350
      Width           =   1125
   End
   Begin VB.TextBox txtPasscode1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2340
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   495
      Width           =   2625
   End
   Begin VB.TextBox txtPasscode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2340
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   135
      Width           =   2625
   End
   Begin VB.Label Label3 
      Caption         =   "Konfirmasi Password Baru :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   315
      TabIndex        =   7
      Top             =   885
      Width           =   1995
   End
   Begin VB.Label Label2 
      Caption         =   "Password Baru :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   300
      TabIndex        =   6
      Top             =   525
      Width           =   1725
   End
   Begin VB.Label Label1 
      Caption         =   "Password Lama  :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   300
      TabIndex        =   5
      Top             =   165
      Width           =   1695
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmGantiPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim asc1 As New Cls_security
Private Sub cmdCancel_Click()
    Unload Me

End Sub

Private Sub cmdOk_Click()
On Error GoTo err
Dim result As String
    If txtPasscode1.text <> "" And txtPasscode2.text <> "" Then
        conn.ConnectionString = strcon
        conn.Open
        
        If txtPasscode1.text = txtPasscode2.text Then
            result = asc1.change_password(CStr(User), txtPasscode.text, txtPasscode1.text, conn)
            If result <> "1" Then
                MsgBox result
            Else
                MsgBox "password anda telah berhasil diubah"
                pwd = txtPasscode1.text
                Unload Me
            End If
        Else
            MsgBox "Passcode baru yang anda masukkan tidak sama"
            txtPasscode1.text = ""
            txtPasscode2.text = ""
            txtPasscode1.SetFocus
        End If
        conn.Close
    Else
        MsgBox "Silahkan isi Password yang baru"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub reset()
    txtPasscode.text = ""
    txtPasscode1.text = ""
    txtPasscode2.text = ""
    txtPasscode.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub mnuCancel_Click()
    cmdCancel_Click
End Sub

Private Sub mnuok_Click()
    cmdOk_Click
End Sub
