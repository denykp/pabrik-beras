VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransReturBeli 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Retur Pembelian"
   ClientHeight    =   9750
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10710
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9750
   ScaleWidth      =   10710
   Begin VB.Frame frAngkut 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   420
      Left            =   1710
      TabIndex        =   71
      Top             =   1710
      Visible         =   0   'False
      Width           =   6720
      Begin VB.TextBox txtKodeEkspedisi 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   4
         Top             =   0
         Width           =   2025
      End
      Begin VB.CommandButton cmdSearchEkspedisi 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3735
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   72
         Top             =   0
         Width           =   420
      End
      Begin VB.Label lblNamaEkspedisi 
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4275
         TabIndex        =   74
         Top             =   0
         Width           =   3120
      End
      Begin VB.Label Label20 
         Caption         =   "Ekspedisi"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   73
         Top             =   60
         Width           =   960
      End
   End
   Begin VB.CheckBox chkAngkut 
      Caption         =   "Ongkos Angkut"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   3
      Top             =   1710
      Width           =   1545
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3060
      Style           =   1  'Graphical
      TabIndex        =   50
      Top             =   9090
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   300
      Picture         =   "frmTransReturBeli.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   9090
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4455
      Picture         =   "frmTransReturBeli.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   9090
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   1290
      Width           =   7845
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3708
      Picture         =   "frmTransReturBeli.frx":0204
      TabIndex        =   23
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9495
      TabIndex        =   22
      Top             =   225
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10170
      TabIndex        =   21
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3690
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   20
      Top             =   540
      Width           =   420
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   555
      Width           =   2025
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1665
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   9090
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7020
      TabIndex        =   2
      Top             =   555
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   89849859
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   6375
      Index           =   0
      Left            =   180
      TabIndex        =   32
      Top             =   2520
      Width           =   10260
      Begin MSComctlLib.TreeView tvwMain 
         Height          =   3030
         Left            =   6435
         TabIndex        =   78
         Top             =   630
         Visible         =   0   'False
         Width           =   3210
         _ExtentX        =   5662
         _ExtentY        =   5345
         _Version        =   393217
         HideSelection   =   0   'False
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2025
         Left            =   90
         TabIndex        =   45
         Top             =   2745
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   3572
         _Version        =   393216
         Cols            =   10
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2580
         Left            =   90
         TabIndex        =   33
         Top             =   45
         Width           =   9915
         Begin VB.ComboBox cmbGudang 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   6345
            Style           =   2  'Dropdown List
            TabIndex        =   76
            Top             =   225
            Width           =   2805
         End
         Begin VB.CommandButton cmdopentreeview 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   9180
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   75
            Top             =   270
            Width           =   330
         End
         Begin VB.TextBox txtNoSerial 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   7
            Top             =   945
            Width           =   1500
         End
         Begin VB.TextBox txtNoPO 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1485
            TabIndex        =   5
            Top             =   180
            Width           =   2025
         End
         Begin VB.CommandButton cmdSearchBeli 
            Appearance      =   0  'Flat
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3555
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   57
            Top             =   180
            Width           =   420
         End
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   9
            Top             =   1680
            Width           =   1140
         End
         Begin VB.PictureBox Picture2 
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   3015
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   35
            Top             =   540
            Width           =   465
            Begin VB.CommandButton cmdSearchBrg 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmTransReturBeli.frx":0306
               TabIndex        =   36
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   2790
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   34
            Top             =   1980
            Width           =   3795
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   13
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   12
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   11
               Top             =   84
               Width           =   1350
            End
         End
         Begin VB.TextBox txtHarga 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   10
            Top             =   2040
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   8
            Top             =   1305
            Width           =   1140
         End
         Begin VB.TextBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   6
            Top             =   585
            Width           =   1500
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Gudang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4860
            TabIndex        =   77
            Top             =   285
            Width           =   1320
         End
         Begin VB.Label Label2 
            BackColor       =   &H8000000C&
            Caption         =   "Nomer Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   60
            Top             =   990
            Width           =   1275
         End
         Begin VB.Label lblNamaBarang2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2775
            TabIndex        =   59
            Top             =   1335
            Visible         =   0   'False
            Width           =   3480
         End
         Begin VB.Label Label5 
            BackStyle       =   0  'Transparent
            Caption         =   "No. Beli"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   58
            Top             =   270
            Width           =   1095
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2730
            TabIndex        =   53
            Top             =   1740
            Width           =   600
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Berat / Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   52
            Top             =   1710
            Width           =   1245
         End
         Begin VB.Label Label8 
            BackColor       =   &H8000000C&
            Caption         =   "Harga"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   44
            Top             =   2085
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblDefQtyJual 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   270
            TabIndex        =   43
            Top             =   2025
            Width           =   870
         End
         Begin VB.Label lblID 
            Caption         =   "Label12"
            ForeColor       =   &H8000000F&
            Height          =   240
            Left            =   270
            TabIndex        =   42
            Top             =   2025
            Visible         =   0   'False
            Width           =   870
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   41
            Top             =   630
            Width           =   1050
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Jml Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   40
            Top             =   1350
            Width           =   1125
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3510
            TabIndex        =   39
            Top             =   630
            Width           =   3480
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   4050
            TabIndex        =   38
            Top             =   1665
            Width           =   600
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2745
            TabIndex        =   37
            Top             =   1305
            Width           =   1050
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxgridRekap 
         Height          =   1125
         Left            =   90
         TabIndex        =   54
         Top             =   4860
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   1984
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblBerat 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7470
         TabIndex        =   56
         Top             =   5985
         Width           =   1140
      End
      Begin VB.Label Label6 
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6210
         TabIndex        =   55
         Top             =   5985
         Width           =   1410
      End
      Begin VB.Label Label17 
         Caption         =   "Jml Kemasan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2475
         TabIndex        =   49
         Top             =   5985
         Width           =   1770
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4455
         TabIndex        =   48
         Top             =   5985
         Width           =   1140
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   47
         Top             =   5985
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   46
         Top             =   5985
         Width           =   735
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   6285
      Index           =   1
      Left            =   225
      TabIndex        =   61
      Top             =   2565
      Visible         =   0   'False
      Width           =   10215
      Begin VB.ListBox listKuli 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2760
         Left            =   1575
         Style           =   1  'Checkbox
         TabIndex        =   79
         Top             =   2565
         Width           =   3705
      End
      Begin VB.ListBox listTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   3915
         TabIndex        =   70
         Top             =   180
         Width           =   3660
      End
      Begin VB.TextBox txtNoTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         TabIndex        =   68
         Top             =   180
         Width           =   2205
      End
      Begin VB.TextBox txtSupir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         TabIndex        =   15
         Top             =   990
         Width           =   5220
      End
      Begin VB.TextBox txtPengawas 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         TabIndex        =   16
         Top             =   1440
         Width           =   1755
      End
      Begin VB.ListBox listPengawas 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   3915
         TabIndex        =   17
         Top             =   1440
         Width           =   3660
      End
      Begin VB.CommandButton cmdSearchPengawas 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3375
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   62
         Top             =   1440
         Width           =   420
      End
      Begin VB.Label Label15 
         Caption         =   "No. Truk"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   69
         Top             =   225
         Width           =   960
      End
      Begin VB.Label Label11 
         Caption         =   "Pengawas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   66
         Top             =   1485
         Width           =   960
      End
      Begin VB.Label Label7 
         Caption         =   "Kuli"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   65
         Top             =   2565
         Width           =   960
      End
      Begin VB.Label Label9 
         Caption         =   "Supir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   64
         Top             =   1080
         Width           =   960
      End
      Begin VB.Label lblNamaPengawas 
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1530
         TabIndex        =   63
         Top             =   1845
         Width           =   2265
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   6765
      Left            =   135
      TabIndex        =   67
      Top             =   2160
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   11933
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Barang"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Pekerja"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   150
      TabIndex        =   51
      Top             =   930
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   31
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   30
      Top             =   1290
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5535
      TabIndex        =   29
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   28
      Top             =   600
      Width           =   960
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1605
      TabIndex        =   27
      Top             =   960
      Width           =   3120
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8100
      TabIndex        =   26
      Top             =   9120
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7155
      TabIndex        =   25
      Top             =   9120
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   24
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransReturBeli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_returbelisting As Boolean
Dim seltab As Byte


Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.Row, 0) = ""
    
    Row = flxGrid.Row
    totalQty = totalQty - (flxGrid.TextMatrix(Row, 5))
    totalberat = totalberat - (flxGrid.TextMatrix(Row, 6))
    total = total - (flxGrid.TextMatrix(Row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        lblTotal = Format(total, "#,##0")
        
    delete_rekap
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If flxGrid.Row > 1 Then flxGrid.Row = flxGrid.Row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.Col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub
Private Sub delete_rekap()
Dim i As Integer
Dim Row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(flxGrid.TextMatrix(flxGrid.Row, 2)) Then Row = i
    Next
    If Row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(Row, 3)) Then .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) - flxGrid.TextMatrix(flxGrid.Row, 5)
    If IsNumeric(.TextMatrix(Row, 4)) Then .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) - flxGrid.TextMatrix(flxGrid.Row, 6)
    If .TextMatrix(Row, 4) = 0 Or .TextMatrix(Row, 3) = 0 Then
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    

    End With
End Sub
Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
'        Next
        If mode = 1 Then
            If Row = 0 Then
                Row = flxGrid.Rows - 1
                flxGrid.Rows = flxGrid.Rows + 1
            End If
        Else
            Row = flxGrid.Row
            delete_rekap
        End If
        flxGrid.Row = Row
        currentRow = flxGrid.Row
        flxGrid.TextMatrix(Row, 1) = txtNoPO.text
        flxGrid.TextMatrix(Row, 2) = txtKdBrg.text
        flxGrid.TextMatrix(Row, 3) = LblNamaBarang1
        flxGrid.TextMatrix(Row, 4) = txtNoSerial
        flxGrid.TextMatrix(Row, 5) = txtQty
        If flxGrid.TextMatrix(Row, 6) = "" Then
            flxGrid.TextMatrix(Row, 6) = txtBerat
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(Row, 5))
            totalberat = totalberat - (flxGrid.TextMatrix(Row, 6))
            total = total - (flxGrid.TextMatrix(Row, 8))
            If mode = 1 Then
                flxGrid.TextMatrix(Row, 6) = CDbl(flxGrid.TextMatrix(Row, 6)) + CDbl(txtBerat)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(Row, 6) = CDbl(txtBerat)
            End If
        End If
        
        flxGrid.TextMatrix(Row, 7) = txtHarga.text
        flxGrid.TextMatrix(Row, 8) = flxGrid.TextMatrix(Row, 6) * flxGrid.TextMatrix(Row, 7)
        flxGrid.TextMatrix(Row, 8) = Format(flxGrid.TextMatrix(Row, 8), "#,##0")
        flxGrid.TextMatrix(Row, 9) = cmbGudang.text
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(Row, 5)
        totalberat = totalberat + flxGrid.TextMatrix(Row, 6)
        total = total + (flxGrid.TextMatrix(Row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        lblTotal = Format(total, "#,##0")
        lblItem = flxGrid.Rows - 2
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        add_rekap txtKdBrg, LblNamaBarang1, txtQty, txtBerat

        
        lblSatuan = ""
'        LblNamaBarang1 = ""
'        lblNamaBarang2 = ""
        txtQty.text = "1"
        txtBerat.text = "1"
        
'        txtHarga.text = "0"
        txtQty.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
End Sub
Private Sub add_rekap(kodebarang As String, namabarang As String, qty As Double, berat As Double)
Dim i As Integer
Dim Row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(kodebarang) Then Row = i
    Next
    If Row = 0 Then
        Row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.Row, 0) = ""
    .Row = Row


    .TextMatrix(Row, 1) = kodebarang
    .TextMatrix(Row, 2) = namabarang
    If .TextMatrix(Row, 3) = "" Then .TextMatrix(Row, 3) = qty Else .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) + qty
    If .TextMatrix(Row, 4) = "" Then .TextMatrix(Row, 4) = berat Else .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) + berat
    lblItem = .Rows - 2
    End With
End Sub

Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdopentreeview_Click()

On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If

End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    simpan
    If posting_returbeli(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub CmdSearchBeli_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchBeli
    frmSearch.nmform = "frmTransreturbeli"
    frmSearch.nmctrl = "txtNoPO"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Pembelian"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang & " where kode_bahan in (select kode_bahan from t_belid where nomer_beli='" & txtNoPO & "')"
    frmSearch.nmform = "frmTransreturbeli"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchEkspedisi_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchEkspedisi
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKodeEkspedisi"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_ekspedisi"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_ekspedisi"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchReturBeli
    frmSearch.nmform = "frmtransreturbeli"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "returbeli"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchSupplier_Click()
'    If User <> "sugik" Then
'        SearchSupplier = "select kode_supplier,nama_supplier,alamat from ms_supplier"
'    End If
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "txtKodesupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function validasi() As Boolean
On Error GoTo err
    validasi = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dikirim terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If cmbGudang.text = "" Then
        MsgBox "Pilih gudang terlebih dahulu"
        cmbGudang.SetFocus
        Exit Function
    End If
    If txtKodeSupplier.text = "" Then
        MsgBox "Silahkan masukkan supplier terlebih dahulu"
        txtKodeSupplier.SetFocus
        Exit Function
    End If
    validasi = True
err:
End Function

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
 
    
    
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_returbelih", "nomer_returbeli", DTPicker1, "RB")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_returbelih where nomer_returbeli='" & id & "'"
    conn.Execute "delete from t_returbelid where nomer_returbeli='" & id & "'"
    conn.Execute "delete from t_returbeli_angkut where nomer_returbeli='" & id & "' and no_urut=1"
    add_dataheader

    For Row = 1 To flxGrid.Rows - 2
        add_datadetail (Row)
    Next
    If chkAngkut.value = True Then add_ekspedisi
    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    txtNoRequest.text = ""
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    listPengawas.Clear
    txtKodeEkspedisi = ""
    chkAngkut.value = False
    
    listTruk.Clear
    txtSupir = ""
    status_returbelisting = False
    totalQty = 0
    totalberat = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = Now
    cmdClear_Click
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxgridRekap.Rows = 1
    flxgridRekap.Rows = 2
    
    TabStrip1.Tabs(1).Selected = True
    
    
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(12)
    ReDim nilai(12)
    table_name = "t_returbelih"
    fields(0) = "nomer_returbeli"
    fields(1) = "tanggal_returbeli"
    fields(2) = "kode_supplier"
    fields(3) = "total"
    fields(4) = "keterangan"
    fields(5) = "userid"
    fields(6) = "status_posting"
    fields(7) = "no_truk"
    fields(8) = "pengawas"
    fields(9) = "kuli"
    fields(10) = "supir"
    
    fields(11) = "fg_angkut"
    
    Dim pengawas, supir, kuli, notruk As String
    pengawas = ""
    For A = 0 To listPengawas.ListCount - 1
        pengawas = pengawas & "[" & Left(listPengawas.List(A), InStr(1, listPengawas.List(A), "-") - 1) & "],"
    Next
    If pengawas <> "" Then pengawas = Left(pengawas, Len(pengawas) - 1)
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKodeSupplier.text
    nilai(3) = Replace(Format(lblTotal, "###0.##"), ",", ".")
    nilai(4) = txtKeterangan.text
    nilai(5) = User
    nilai(6) = "0"
    nilai(7) = notruk
    nilai(8) = pengawas
    nilai(9) = kuli
    nilai(10) = supir
    nilai(11) = chkAngkut.value
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_returbelid"
    fields(0) = "nomer_returbeli"
    fields(1) = "nomer_beli"
    fields(2) = "kode_bahan"
    fields(3) = "nomer_serial"
    fields(4) = "qty"
    fields(5) = "berat"
    fields(6) = "NO_URUT"
    fields(7) = "HARGA"
    fields(8) = "kode_gudang"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = flxGrid.TextMatrix(Row, 2)
    nilai(3) = flxGrid.TextMatrix(Row, 4)
    nilai(4) = Replace(Format(flxGrid.TextMatrix(Row, 5), "###0.##"), ",", ".")
    nilai(5) = Replace(Format(flxGrid.TextMatrix(Row, 6), "###0.##"), ",", ".")
    nilai(6) = Row
    nilai(7) = Replace(Format(flxGrid.TextMatrix(Row, 7), "###0.##"), ",", ".")
    nilai(8) = Trim(Right(flxGrid.TextMatrix(Row, 9), 10))
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_ekspedisi()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(8)
    ReDim nilai(8)

    table_name = "t_returbeli_angkut"
    fields(0) = "nomer_returbeli"
    fields(1) = "tanggal"
    fields(2) = "kode_ekspedisi"
    fields(3) = "fg_bayarangkut"
    fields(4) = "no_truk"
    fields(5) = "supir"
    fields(6) = "no_urut"
    fields(7) = "status"
    Dim supir, notruk As String
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd")
    
    nilai(2) = txtKodeEkspedisi
    nilai(3) = 0
    nilai(4) = notruk
    nilai(5) = supir
    nilai(6) = 1
    nilai(7) = 1
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_returbelih where nomer_returbeli='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal_returbeli")

        chkAngkut.value = rs!fg_angkut
        
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        'SetComboTextRight rs!kode_gudang, cmbGudang
        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
        Dim pengawas() As String
        Dim kuli() As String
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
            txtNoTruk_KeyDown 13, 0
        Next
        pengawas = Split(rs!pengawas, ",")
        listPengawas.Clear
        For A = 0 To UBound(pengawas)
            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
            txtPengawas_KeyDown 13, 0
        Next
        kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
        Next
        totalQty = 0
        totalberat = 0
        total = 0
'        cmdPrint.Visible = True
'        status_returbelisting = rs!status_returbelisting
        If rs("status_posting") = 1 Then
            cmdSimpan.Visible = False
            cmdPosting.Visible = False
        Else
            cmdPosting.Visible = True
            cmdSimpan.Visible = True
        End If
        
        
        If rs.State Then rs.Close
        If chkAngkut.value = True Then
            rs.Open "select * from t_returbeli_angkut where nomer_returbeli='" & lblNoTrans & "' and status='1'", conn
            If Not rs.EOF Then
                txtKodeEkspedisi = rs!kode_ekspedisi
                cek_ekspedisi
            End If
            If rs.State Then rs.Close
        End If
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        flxgridRekap.Rows = 1
        flxgridRekap.Rows = 2
        Row = 1

        rs.Open "select d.nomer_beli,d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat,d.harga,d.kode_gudang from t_returbelid d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_returbeli='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = rs(2)
                flxGrid.TextMatrix(Row, 4) = rs(3)
                flxGrid.TextMatrix(Row, 5) = rs(4)
                flxGrid.TextMatrix(Row, 6) = rs(5)
                flxGrid.TextMatrix(Row, 7) = Format(rs(6), "#,##0")
                flxGrid.TextMatrix(Row, 8) = Format(rs(5) * rs(6), "#,##0")
                flxGrid.TextMatrix(Row, 9) = rs!kode_gudang
                add_rekap rs(1), rs(2), rs(5), rs(6)
                Row = Row + 1
                totalQty = totalQty + rs(4)
                totalberat = totalberat + rs(5)
                total = total + (rs(5) * rs(6))
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = Format(totalQty, "#,##0.00")
        lblBerat = Format(totalberat, "#,##0.00")
        lblTotal = Format(total, "#,##0.00")
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtNoPO.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.Row, 2)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.Row, 3)
                txtNoSerial.text = flxGrid.TextMatrix(flxGrid.Row, 4)
                End If
            
            txtBerat.text = flxGrid.TextMatrix(flxGrid.Row, 6)
            txtQty.text = flxGrid.TextMatrix(flxGrid.Row, 5)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.Row, 7)
            SetComboTextRight Trim(Right(flxGrid.TextMatrix(flxGrid.Row, 9), 10)), cmbGudang
            txtBerat.SetFocus
        End If

End Sub

Private Sub Command1_Click()

End Sub

Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub


Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub


Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchsupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    load_combo
    loadgrup "0", tvwMain
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    Load_ListKuli
'    conn.ConnectionString = strcon
'    conn.Open
'    listKuli.Clear
'    rs.Open "select * from ms_karyawan order by nik", conn
'    While Not rs.EOF
'        listKuli.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Close
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 1100
    flxGrid.ColWidth(3) = 2800
    flxGrid.ColWidth(4) = 1100
    flxGrid.ColWidth(5) = 900
    flxGrid.ColWidth(6) = 900
    flxGrid.ColWidth(7) = 900
    flxGrid.ColWidth(8) = 1500
    flxGrid.ColWidth(9) = 1200
    
    
'    If right_hpp = True Then
        
        txtHarga.Visible = True
        Label8.Visible = True
        lblTotal.Visible = True
        Label10.Visible = True
'    End If

    flxGrid.TextMatrix(0, 1) = "No. Beli"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Kode Bahan"
    flxGrid.TextMatrix(0, 3) = "Nama"
    flxGrid.TextMatrix(0, 4) = "Nomer Serial"
    flxGrid.TextMatrix(0, 5) = "Qty"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "Harga"
    flxGrid.TextMatrix(0, 8) = "Total"
    flxGrid.TextMatrix(0, 9) = "Gudang"
    With flxgridRekap
        .ColWidth(0) = 300
        .ColWidth(1) = 1400
        .ColWidth(2) = 3000
        .ColWidth(3) = 900
        .ColWidth(4) = 900
            
        .TextMatrix(0, 1) = "Kode Bahan"
        .ColAlignment(2) = 1 'vbAlignLeft
        .ColAlignment(1) = 1
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Jml Kemasan"
        .TextMatrix(0, 4) = "Berat"
    End With
End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub


Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan inner join t_belid on ms_bahan.kode_bahan=t_belid.kode_bahan where ms_bahan.[kode_bahan]='" & txtKdBrg & "' and t_belid.nomer_beli='" & txtNoPO & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        txtNoSerial = rs!nomer_serial
        lblSatuan = ""
        
        txtHarga.text = rs("harga")
        
        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        txtNoSerial = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier] from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
        
        
    Else
        lblNamaSupplier = ""
        
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()
'On Error GoTo err
'    conn.Open strcon
'    With flxGrid
'    For i = 1 To .Rows - 2
'        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
'        If Not rs.EOF Then
'            total = total - .TextMatrix(i, 8)
'            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
'            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
'            total = total + .TextMatrix(i, 8)
'        End If
'        rs.Close
'    Next
'    End With
'    conn.Close
'    lblTotal = Format(total, "#,##0")
'    Exit Sub
'err:
'    MsgBox err.Description
'    If conn.State Then conn.Close
End Sub



Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    tvwMain.Visible = False
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
End Sub


Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub


Private Sub txtNoRequest_KeyDown(KeyCode As Integer, Shift As Integer)
    
End Sub


Private Sub txtNoPO_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then CmdSearchBeli_Click
End Sub


Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub


Private Sub cmdSearchKuli_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmAddPenerimaanBarang"
        .nmctrl = "txtkuli"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .Col = 0
        .Index = -1
        .proc = "cek_kuli"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub
Public Sub cek_pengawas()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close

err:
End Sub
Public Sub cek_kuli()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtKuli & "'", conn
    If Not rs.EOF Then
        lblNamaKuli = rs(1)
    Else
        lblNamaKuli = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub
Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmAddPenerimaanBarang"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .Col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub
Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPengawas_Click
    If KeyCode = 13 Then
        cek_pengawas
        listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
        txtPengawas = ""
        lblNamaPengawas = ""
        MySendKeys "+{tab}"
    End If
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub
Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub
Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Public Function cek_ekspedisi() As Boolean
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_ekspedisi] from ms_ekspedisi where kode_ekspedisi='" & txtKodeEkspedisi & "'", conn
    If Not rs.EOF Then
        lblNamaEkspedisi = rs(0)
        cek_ekspedisi = True
    Else
        lblNamaEkspedisi = ""
        cek_ekspedisi = False
    End If
    rs.Close
    conn.Close
    
err:
End Function
Private Sub txtKodeEkspedisi_LostFocus()
    If txtKodeEkspedisi.text <> "" And Not cek_ekspedisi Then
        MsgBox "Kode ekspedisi tidak ditemukan"
        txtKodeEkspedisi.SetFocus
    End If
End Sub
Private Sub txtKodeEkspedisi_KeyDown(KeyCode As Integer, Shift As Integer)
    cmdSearchEkspedisi_Click
End Sub
