VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0A45DB48-BD0D-11D2-8D14-00104B9E072A}#2.0#0"; "sstabs2.ocx"
Begin VB.Form frmAddPencairanGiro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pencairan Giro"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11880
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   11880
   Begin ActiveTabs.SSActiveTabs SSActiveTabs1 
      Height          =   5700
      Left            =   150
      TabIndex        =   22
      Top             =   1620
      Width           =   11595
      _ExtentX        =   20452
      _ExtentY        =   10054
      _Version        =   131083
      TabCount        =   2
      Tabs            =   "frmAddPencairanGiro.frx":0000
      Begin ActiveTabs.SSActiveTabPanel SSActiveTabPanel2 
         Height          =   5310
         Left            =   30
         TabIndex        =   23
         Top             =   360
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   9366
         _Version        =   131083
         TabGuid         =   "frmAddPencairanGiro.frx":0082
         Begin VSFlex8LCtl.VSFlexGrid vsfgPencairanGiro 
            Height          =   5145
            Left            =   90
            TabIndex        =   3
            Top             =   90
            Width           =   11355
            _cx             =   20029
            _cy             =   9075
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   1
            Cols            =   8
            FixedRows       =   1
            FixedCols       =   1
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   $"frmAddPencairanGiro.frx":00AA
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin ActiveTabs.SSActiveTabPanel SSActiveTabPanel1 
         Height          =   5310
         Left            =   30
         TabIndex        =   24
         Top             =   360
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   9366
         _Version        =   131083
         TabGuid         =   "frmAddPencairanGiro.frx":01AF
         Begin VB.Frame Frame1 
            Height          =   1695
            Left            =   90
            TabIndex        =   25
            Top             =   0
            Width           =   7455
            Begin VB.CommandButton cmdTambahkan 
               Caption         =   "Tambahkan"
               Height          =   330
               Left            =   6075
               Picture         =   "frmAddPencairanGiro.frx":01D7
               TabIndex        =   7
               Top             =   1290
               Width           =   1125
            End
            Begin VB.TextBox txtKeteranganAcc 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1785
               TabIndex        =   5
               Top             =   510
               Width           =   5430
            End
            Begin VB.CommandButton cmdSearchAcc 
               Caption         =   "F3"
               Height          =   330
               Left            =   3570
               Picture         =   "frmAddPencairanGiro.frx":02D9
               TabIndex        =   26
               Top             =   120
               Width           =   375
            End
            Begin VB.TextBox txtNominal 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1785
               TabIndex        =   6
               Top             =   900
               Width           =   1710
            End
            Begin VB.TextBox txtKodeAcc 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1785
               TabIndex        =   4
               Top             =   120
               Width           =   1710
            End
            Begin VB.Label lblNamaAcc 
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   4020
               TabIndex        =   34
               Top             =   180
               Width           =   3030
            End
            Begin VB.Label Label10 
               Caption         =   "Keterangan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   210
               TabIndex        =   32
               Top             =   555
               Width           =   1320
            End
            Begin VB.Label Label7 
               Caption         =   ":"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   1605
               TabIndex        =   31
               Top             =   555
               Width           =   105
            End
            Begin VB.Label Label12 
               Caption         =   "Nilai"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   210
               TabIndex        =   30
               Top             =   945
               Width           =   1320
            End
            Begin VB.Label Label11 
               Caption         =   ":"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   1605
               TabIndex        =   29
               Top             =   945
               Width           =   105
            End
            Begin VB.Label Label3 
               Caption         =   "Kode Acc"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   210
               TabIndex        =   28
               Top             =   165
               Width           =   1320
            End
            Begin VB.Label Label5 
               Caption         =   ":"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   1605
               TabIndex        =   27
               Top             =   165
               Width           =   105
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid vsfgAcc 
            Height          =   3500
            Left            =   90
            TabIndex        =   33
            Top             =   1740
            Width           =   9645
            _cx             =   17013
            _cy             =   6174
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   -2147483636
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   1
            Cols            =   5
            FixedRows       =   1
            FixedCols       =   1
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   $"frmAddPencairanGiro.frx":03DB
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   930
      Picture         =   "frmAddPencairanGiro.frx":047B
      TabIndex        =   10
      Top             =   7620
      Width           =   1350
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4050
      Picture         =   "frmAddPencairanGiro.frx":057D
      TabIndex        =   13
      Top             =   7620
      Width           =   1350
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2490
      Picture         =   "frmAddPencairanGiro.frx":067F
      TabIndex        =   12
      Top             =   7620
      Width           =   1350
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "Next"
      Height          =   330
      Left            =   5040
      Picture         =   "frmAddPencairanGiro.frx":0781
      TabIndex        =   19
      Top             =   30
      Width           =   645
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "Prev"
      Height          =   330
      Left            =   4290
      Picture         =   "frmAddPencairanGiro.frx":0883
      TabIndex        =   18
      Top             =   30
      Width           =   645
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1695
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1260
      Width           =   3570
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   330
      Left            =   3795
      Picture         =   "frmAddPencairanGiro.frx":0985
      TabIndex        =   8
      Top             =   30
      Width           =   375
   End
   Begin MSComCtl2.DTPicker dtpTanggal 
      Height          =   330
      Left            =   1695
      TabIndex        =   0
      Top             =   480
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   92536835
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker dtpTanggalCair 
      Height          =   330
      Left            =   1695
      TabIndex        =   1
      Top             =   870
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   92536835
      CurrentDate     =   38927
   End
   Begin VB.Label lblTotalNominalAcc 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8970
      TabIndex        =   38
      Top             =   7875
      Width           =   2775
   End
   Begin VB.Label Label14 
      Caption         =   "Total Nominal Acc."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6400
      TabIndex        =   37
      Top             =   7875
      Width           =   2500
   End
   Begin VB.Label lblTotalNominalGiro 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8970
      TabIndex        =   36
      Top             =   7425
      Width           =   2775
   End
   Begin VB.Label Label13 
      Caption         =   "Total Nominal Giro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6400
      TabIndex        =   35
      Top             =   7425
      Width           =   2500
   End
   Begin VB.Label Label2 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   21
      Top             =   915
      Width           =   105
   End
   Begin VB.Label Label1 
      Caption         =   "Tanggal Cair"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   20
      Top             =   915
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1635
      TabIndex        =   17
      Top             =   0
      Width           =   2040
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   16
      Top             =   1320
      Width           =   1320
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   15
      Top             =   1320
      Width           =   105
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   14
      Top             =   525
      Width           =   1320
   End
   Begin VB.Label Label9 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   11
      Top             =   525
      Width           =   105
   End
   Begin VB.Label Label31 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   60
      Width           =   1320
   End
End
Attribute VB_Name = "frmAddPencairanGiro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public tipe_giro As String

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 id from t_pencairangiroh where left(id,2)='" & IIf(tipe_giro = "masuk", "GM", "GK") & "' and substring(id,3,2)='" & Format(dtpTanggal.value, "yy") & "' and substring(id,5,2)='" & Format(dtpTanggal.value, "MM") & "' order by id desc", conn
    If Not rs.EOF Then
        No = IIf(tipe_giro = "masuk", "GM", "GK") & Format(dtpTanggal.value, "yy") & Format(dtpTanggal.value, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = IIf(tipe_giro = "masuk", "GM", "GK") & Format(dtpTanggal.value, "yy") & Format(dtpTanggal.value, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 id from t_pencairangiroh where id>'" & lblNoTrans & "' order by id", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 id from t_pencairangiroh where id<'" & lblNoTrans & "' order by id desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = "select * from ms_coa where fg_aktif = 1"
    frmSearch.nmform = "frmaddpencairan"
    frmSearch.nmctrl = "txtKodeAcc"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodeacc"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.query = "select * from t_pencairangiroh"
    frmSearch.nmform = "frmaddpencairan"
    frmSearch.nmctrl = "lblnotrans"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If CCur(lblTotalNominalGiro.Caption) <> CCur(lblTotalNominalAcc.Caption) Then
        If MsgBox("Total nominal giro dan total nominal acc tidak sama, apakah anda yakin ingin menyimpan?", vbYesNo) = vbNo Then Exit Sub
    End If
    On Error GoTo err
    Dim trans As Boolean
    Dim Row As Integer
    
    conn.Open strcon
    conn.BeginTrans
    trans = False
    
    If lblNoTrans.Caption = "-" Then
        nomor_baru
    Else
        rs.Open "select * from t_pencairangirod where id = '" & lblNoTrans.Caption & "'", conn
            While Not rs.EOF
                conn.Execute "update list_bg set status_cair = 0 where nomer_bg = '" & rs!no_giro & "'"
                rs.MoveNext
            Wend
        rs.Close
        conn.Execute "delete from t_pencairangiroh where id = '" & lblNoTrans.Caption & "'"
        conn.Execute "delete from t_pencairangirod where id = '" & lblNoTrans.Caption & "'"
        conn.Execute "delete from t_pencairangiro_acc where id = '" & lblNoTrans.Caption & "'"
    End If
    
    add_dataheader
    For Row = 1 To vsfgAcc.Rows - 1
        add_dataacc Row
    Next
    Dim row_exist As Boolean
    Dim total_nominalgiro As Currency
    total_nominalgiro = 0
    row_exist = False
    For Row = 1 To vsfgPencairanGiro.Rows - 1
        If vsfgPencairanGiro.TextMatrix(Row, 7) <> 0 Then
            row_exist = True
            add_datadetail Row
            conn.Execute "update list_bg set status_cair = 1 where nomer_bg = '" & vsfgPencairanGiro.TextMatrix(Row, 4) & "'"
            total_nominalgiro = total_nominalgiro + vsfgPencairanGiro.TextMatrix(Row, 6)
        End If
    Next
    conn.Execute "update t_pencairangiroh set total_nominalgiro = '" & total_nominalgiro & "' where id = '" & lblNoTrans.Caption & "'"
    If row_exist = False Then
        If lblNoTrans.Caption = "-" Then
            MsgBox "Silahkan pilih nomer giro yang akan dicairkan"
            conn.RollbackTrans
            conn.Close
            Exit Sub
        Else
            If MsgBox("Tidak ada nomer giro yang dipilih, ingin melanjutkan?", vbYesNo) = vbNo Then
                conn.RollbackTrans
                conn.Close
                Exit Sub
            End If
        End If
    End If
    conn.CommitTrans
    trans = True
    conn.Close
    MsgBox "Data sudah tersimpan"
    reset_form
    Exit Sub
err:
    If trans = False Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdTambahkan_Click()
    vsfgAcc.AddItem vbTab & txtKodeAcc.text & vbTab & lblNamaAcc.Caption & vbTab & txtKeteranganAcc.text & vbTab & txtNominal.text
    hitung_total
    txtKodeAcc.text = ""
    lblNamaAcc.Caption = "-"
    txtKeteranganAcc.text = ""
    txtNominal.text = ""
    txtKodeAcc.SetFocus
End Sub

Private Sub dtpTanggalCair_Change()
    load_giro
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    If tipe_giro = "masuk" Then
        vsfgPencairanGiro.ColHidden(2) = True
        vsfgPencairanGiro.Col = 3
        vsfgPencairanGiro.text = "Nama Customer"
    Else
        vsfgPencairanGiro.ColHidden(1) = True
        vsfgPencairanGiro.Col = 3
        vsfgPencairanGiro.text = "Nama Supplier"
    End If
    reset_form
End Sub

Private Sub add_dataheader()
    Dim table_name As String
    Dim fields() As String
    Dim nilai() As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_pencairangiroh"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "tanggal_cair"
    fields(3) = "keterangan"
    fields(4) = "total_nominalgiro"
    
    nilai(0) = lblNoTrans.Caption
    nilai(1) = dtpTanggal.value
    nilai(2) = dtpTanggalCair.value
    nilai(3) = txtKeterangan.text
    nilai(4) = 0
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim table_name As String
    Dim fields() As String
    Dim nilai() As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "t_pencairangirod"
    fields(0) = "id"
    fields(1) = "kode_customer"
    fields(2) = "kode_supplier"
    fields(3) = "no_giro"
    fields(4) = "tanggal_cair"
    fields(5) = "nominal"
    
    nilai(0) = lblNoTrans.Caption
    nilai(1) = vsfgPencairanGiro.TextMatrix(Row, 1)
    nilai(2) = vsfgPencairanGiro.TextMatrix(Row, 2)
    nilai(3) = vsfgPencairanGiro.TextMatrix(Row, 4)
    nilai(4) = vsfgPencairanGiro.TextMatrix(Row, 5)
    nilai(5) = vsfgPencairanGiro.TextMatrix(Row, 6)
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_dataacc(Row As Integer)
    Dim table_name As String
    Dim fields() As String
    Dim nilai() As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_pencairangiro_acc"
    fields(0) = "id"
    fields(1) = "kode_acc"
    fields(2) = "keterangan"
    fields(3) = "nominal"
    
    nilai(0) = lblNoTrans.Caption
    nilai(1) = vsfgAcc.TextMatrix(Row, 1)
    nilai(2) = vsfgAcc.TextMatrix(Row, 3)
    nilai(3) = vsfgAcc.TextMatrix(Row, 4)
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub reset_form()
    lblNoTrans.Caption = "-"
    dtpTanggal.value = Now
    dtpTanggalCair.value = Now
    txtKeterangan.text = ""
    vsfgAcc.Rows = 1
    vsfgPencairanGiro.Rows = 1
    load_giro
End Sub

Public Sub cek_notrans()
    If lblNoTrans.Caption = "-" Then Exit Sub
    conn.Open strcon
    rs.Open "select * from t_pencairangiroh where id = '" & lblNoTrans.Caption & "'", conn
    If Not rs.EOF Then
        dtpTanggal.value = rs!tanggal
        dtpTanggalCair.value = rs!tanggal_cair
        txtKeterangan.text = rs!keterangan
    End If
    rs.Close
    vsfgAcc.Rows = 1
    rs.Open "select a.*,b.nama from t_pencairangiro_acc a left join ms_coa b on a.kode_acc = b.kode_acc where id = '" & lblNoTrans.Caption & "'", conn
    While Not rs.EOF
        vsfgAcc.AddItem vbTab & rs!kode_acc & vbTab & rs!nama & vbTab & rs!keterangan & vbTab & rs!Nominal
        rs.MoveNext
    Wend
    rs.Close
    vsfgPencairanGiro.Rows = 1
    If tipe_giro = "masuk" Then
        rs.Open "select * from t_pencairangirod a left join ms_customer b on a.kode_customer = b.kode_customer where a.id = '" & lblNoTrans.Caption & "' order by a.kode_customer,tanggal_cair", conn
        While Not rs.EOF
            vsfgPencairanGiro.AddItem vbTab & rs!kode_customer & vbTab & "" & vbTab & rs!Nama_Customer & vbTab & rs!no_giro & vbTab & rs!tanggal_cair & vbTab & rs!Nominal & vbTab & 1
            rs.MoveNext
        Wend
    Else
        rs.Open "select * from t_pencairangirod a left join ms_supplier b on a.kode_supplier = b.kode_supplier where a.id = '" & lblNoTrans.Caption & "' order by a.kode_supplier,tanggal_cair", conn
        While Not rs.EOF
            vsfgPencairanGiro.AddItem vbTab & "" & vbTab & rs!kode_supplier & vbTab & rs!Nama_Supplier & vbTab & rs!no_giro & vbTab & rs!tanggal_cair & vbTab & rs!Nominal & vbTab & 1
            rs.MoveNext
        Wend
    End If
    rs.Close
    conn.Close
End Sub

Private Sub load_giro()
    If lblNoTrans.Caption <> "-" Then
        If MsgBox("Apakah anda yakin ingin menambahkan giro pada nota ini?", vbYesNo) = vbNo Then
            Exit Sub
        Else
            If vsfgPencairanGiro.Rows > 1 Then
                MsgBox "Nomor giro hanya bisa ditambahkan pada nomer yang kosong"
                Exit Sub
            End If
        End If
    End If
        
    vsfgPencairanGiro.Rows = 1
    conn.Open strcon
    If tipe_giro = "masuk" Then
        rs.Open "select l.*,m.nama_customer from list_bg l left join ms_customer m on l.kode_customer = m.kode_customer where convert(varchar,l.tanggal_cair,112) <= '" & Format(dtpTanggalCair.value, "yyyyMMdd") & "' and l.status_cair = 0 and l.kode_customer <> '' order by m.kode_customer,tanggal_cair", conn
        While Not rs.EOF
            vsfgPencairanGiro.AddItem vbTab & rs!kode_customer & vbTab & "" & vbTab & rs!Nama_Customer & vbTab & rs!nomer_bg & vbTab & rs!tanggal_cair & vbTab & rs!Nominal & vbTab & 0
        rs.MoveNext
        Wend
    Else
        rs.Open "select l.*,m.nama_supplier from list_bg l left join ms_supplier m on l.kode_supplier = m.kode_supplier where convert(varchar,l.tanggal_cair,112) <= '" & Format(dtpTanggalCair.value, "yyyyMMdd") & "' and l.status_cair = 0 and l.kode_supplier <> '' order by m.kode_customer,tanggal_cair", conn
        While Not rs.EOF
            vsfgPencairanGiro.AddItem vbTab & "" & vbTab & rs!kode_supplier & vbTab & rs!Nama_Supplier & vbTab & rs!nomer_bg & vbTab & rs!tanggal_cair & vbTab & rs!Nominal & vbTab & 0
        rs.MoveNext
    Wend
    End If
    rs.Close
    conn.Close
End Sub

Public Sub cek_kodeacc()
    conn.Open strcon
    rs.Open "select * from ms_coa where fg_aktif = 1 and kode_acc = '" & txtKodeAcc.text & "'", conn
    If Not rs.EOF Then
        lblNamaAcc.Caption = rs!nama
    End If
    rs.Close
    conn.Close
End Sub

Private Sub txtKodeAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchAcc_Click
End Sub

Private Sub txtNominal_LostFocus()
    If Not IsNumeric(txtNominal.text) Then
        MsgBox "Nilai akun harus berupa angka"
        'txtNominal.SetFocus
        Exit Sub
    End If
End Sub

Private Sub vsfgAcc_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If Col = 0 Or Col = 1 Or Col = 2 Or Col = 3 Then Cancel = True
End Sub

Private Sub vsfgAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        vsfgAcc.RemoveItem (vsfgAcc.Row)
    End If
End Sub

Private Sub vsfgPencairanGiro_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If Col = 0 Or Col = 1 Or Col = 2 Or Col = 3 Or Col = 4 Or Col = 5 Or Col = 6 Then
        Cancel = True
    End If
End Sub

Private Sub hitung_total()
    Dim Row As Integer
    Dim total As Currency
    Dim total_acc As Currency
    For Row = 1 To vsfgPencairanGiro.Rows - 1
        If vsfgPencairanGiro.TextMatrix(Row, 7) <> 0 Then total = total + vsfgPencairanGiro.TextMatrix(Row, 6)
    Next
    For Row = 1 To vsfgAcc.Rows - 1
        If IsNumeric(vsfgAcc.TextMatrix(Row, 4)) Then total_acc = total_acc + vsfgAcc.TextMatrix(Row, 4)
    Next
    lblTotalNominalGiro.Caption = Format(total, "#,##0")
    lblTotalNominalAcc.Caption = Format(total_acc, "#,##0")
End Sub

Private Sub vsfgPencairanGiro_CellChanged(ByVal Row As Long, ByVal Col As Long)
    If Col = 7 Then
        hitung_total
    End If
End Sub
