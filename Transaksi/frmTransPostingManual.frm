VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTransPostingManual 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Proses Posting Manual"
   ClientHeight    =   6195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8475
   FillColor       =   &H8000000A&
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   8475
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid FlxGrid 
      Height          =   5265
      Left            =   240
      TabIndex        =   4
      Top             =   840
      Width           =   7875
      _ExtentX        =   13891
      _ExtentY        =   9287
      _Version        =   393216
      Cols            =   4
      ScrollTrack     =   -1  'True
      FocusRect       =   2
      SelectionMode   =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2340
      TabIndex        =   3
      Top             =   6720
      Width           =   1695
   End
   Begin VB.CommandButton cmdCari 
      Caption         =   "&Cari"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5670
      TabIndex        =   2
      Top             =   330
      Width           =   1125
   End
   Begin VB.ComboBox cmbCari 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   300
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   330
      Width           =   2175
   End
   Begin VB.TextBox txtCari 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   1
      Top             =   330
      Width           =   2865
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   6300
      Top             =   6450
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
End
Attribute VB_Name = "frmTransPostingManual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCari_Click()
On errro GoTo err
    load_data
    Exit Sub
err:
    MsgBox err.Description
End Sub



Private Sub cmdPosting_Click()
Dim i As Integer
On Error GoTo err
If flxGrid.TextMatrix(flxGrid.row, 1) = "" Then Exit Sub
  row = flxGrid.row
If IsNumeric(Mid(flxGrid.TextMatrix(row, 1), 3, 1)) Then
     Select Case Left(flxGrid.TextMatrix(row, 1), 2)
        Case "PH"
            Posting_BayarHutang flxGrid.TextMatrix(row, 1), conn
        Case "PP"
        '    Posting_Piutang FlxGrid.TextMatrix(row, 1), conn
            
    End Select
Else
     Select Case Left(flxGrid.TextMatrix(row, 1), 3)
    'opname stock
        Case "SOP"
            postingOpname flxGrid.TextMatrix(row, 1), False
        
     'Koreksi stock
        Case "KRK"
            postingKoreksiKertas flxGrid.TextMatrix(row, 1), False
    'order beli kertas
        Case "OBK"
            posting_orderbelikertas flxGrid.TextMatrix(row, 1)
        Case "OBR"
            posting_orderbeliroll flxGrid.TextMatrix(row, 1)
        Case "PBK"
            posting_pembelian flxGrid.TextMatrix(row, 1)
        
        Case "PJ"
            posting_jual flxGrid.TextMatrix(row, 1)
        Case "RB"
            posting_returbeli flxGrid.TextMatrix(row, 1)
        
        Case "RJ"
            posting_returjual flxGrid.TextMatrix(row, 1)
        
    End Select
    MsgBox "Proses berhasil"
   
 End If

delete_row
Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub delete_row()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    row = flxGrid.row
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
End Sub
Private Sub flxgrid_DblClick()
'On Error GoTo err
'If FlxGrid.TextMatrix(FlxGrid.row, 1) = "" Then Exit Sub
'  row = FlxGrid.row
'
'     Select Case Left(FlxGrid.TextMatrix(row, 1), 3)
'    'opname stock
'        Case "SOK"
'            frmTransStockOpname.lblNoTrans = FlxGrid.TextMatrix(row, 1)
'            frmTransStockOpname.cek_notrans
'            frmTransStockOpname.Show vbModal
'        Case "SOP"
'        Case "SOT"
'            postingOpnameTinta FlxGrid.TextMatrix(row, 1), False
'     'Koreksi stock
'        Case "KRK"
'            postingKoreksiKertas FlxGrid.TextMatrix(row, 1), False
'        Case "KRP"
'            postingKoreksiPlat FlxGrid.TextMatrix(row, 1), False
'        Case "KRT"
'            postingKoreksiTinta FlxGrid.TextMatrix(row, 1), False
'     'Mutasi tinta
'        Case "MUT"
'            postingMutasiTinta FlxGrid.TextMatrix(row, 1), False
'    End Select
'
'load_data
'Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 13 Then cmdPosting_Click
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    Adodc1.ConnectionString = strcon
    Adodc1.RecordSource = "Select nomer_transaksi,tanggal,keterangan from vwRekapHeader where status_posting=0 order by nomer_transaksi" ', 'strcon, adOpenStatic, adLockBatchOptimistic
    Adodc1.Refresh
    For i = 0 To Adodc1.Recordset.fields.Count - 1
         cmbCari.AddItem Adodc1.Recordset(i).Name
    Next
  
    cmbCari.ListIndex = 0
    load_data
    flxGrid.ColWidth(0) = 1000
    flxGrid.ColWidth(1) = 2000
    flxGrid.ColWidth(2) = 2000
    flxGrid.ColWidth(3) = 2000
    flxGrid.TextMatrix(0, 1) = "No.Transaksi"
    flxGrid.TextMatrix(0, 2) = "Tanggal"
    flxGrid.TextMatrix(0, 3) = "Keterangan"

End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub
Function load_data()
Dim i As Integer
On Error GoTo err
    
    i = 1
    conn.ConnectionString = strcon
    conn.Open
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    rs.Open "Select nomer_transaksi,tanggal,keterangan from vwRekapHeader where " & cmbCari.text & " like '%" & txtCari.text & "%' and status_posting=0 order by tanggal,nomer_transaksi asc", conn

    While Not rs.EOF
         flxGrid.TextMatrix(i, 0) = i
         flxGrid.TextMatrix(i, 1) = rs(0)
         flxGrid.TextMatrix(i, 2) = rs(1)
         flxGrid.TextMatrix(i, 3) = rs(2)
         i = i + 1
         flxGrid.Rows = flxGrid.Rows + 1
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    
    Exit Function
err:
    MsgBox err.Description
End Function

