VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMasterContact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Contact"
   ClientHeight    =   7140
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   13320
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   13320
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1905
      MaxLength       =   25
      TabIndex        =   55
      Top             =   570
      Width           =   2040
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4065
      Picture         =   "frmMasterContact.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   54
      Top             =   570
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.CommandButton cmdLoadRpt 
      Caption         =   "Load History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4455
      Picture         =   "frmMasterContact.frx":0102
      TabIndex        =   53
      Top             =   570
      Width           =   1080
   End
   Begin VB.TextBox txtFile 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6210
      TabIndex        =   52
      Top             =   4410
      Width           =   2280
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "Browse"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8550
      Picture         =   "frmMasterContact.frx":0204
      TabIndex        =   51
      Top             =   4410
      Width           =   915
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   11
      Left            =   7515
      MaxLength       =   25
      TabIndex        =   14
      Text            =   "0"
      Top             =   3960
      Width           =   1905
   End
   Begin VB.ComboBox cmbWilayah2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterContact.frx":0306
      Left            =   7515
      List            =   "frmMasterContact.frx":0308
      TabIndex        =   13
      Top             =   3555
      Width           =   3885
   End
   Begin VB.ComboBox cmbWilayah 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterContact.frx":030A
      Left            =   7515
      List            =   "frmMasterContact.frx":030C
      TabIndex        =   11
      Top             =   2745
      Width           =   3885
   End
   Begin VB.ComboBox cmbWilayah1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterContact.frx":030E
      Left            =   7515
      List            =   "frmMasterContact.frx":0310
      TabIndex        =   12
      Top             =   3150
      Width           =   3885
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   1890
      TabIndex        =   4
      Top             =   2925
      Width           =   3570
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   9
      Left            =   1890
      TabIndex        =   8
      Top             =   4545
      Width           =   3075
   End
   Begin VB.ComboBox cmbStatus 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterContact.frx":0312
      Left            =   7515
      List            =   "frmMasterContact.frx":0314
      TabIndex        =   9
      Text            =   "cmbStatus"
      Top             =   540
      Width           =   3885
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3570
      Picture         =   "frmMasterContact.frx":0316
      TabIndex        =   17
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1740
      Index           =   10
      Left            =   7515
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   945
      Width           =   5595
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   8
      Left            =   1890
      TabIndex        =   1
      Top             =   1305
      Width           =   3075
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   7
      Left            =   1890
      TabIndex        =   7
      Top             =   4140
      Width           =   3570
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   1890
      TabIndex        =   6
      Top             =   3735
      Width           =   3570
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   1890
      TabIndex        =   5
      Top             =   3330
      Width           =   3570
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   1890
      TabIndex        =   3
      Top             =   2520
      Width           =   3570
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   2
      Left            =   1890
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1710
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      MaxLength       =   50
      TabIndex        =   0
      Top             =   945
      Width           =   3480
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4800
      Picture         =   "frmMasterContact.frx":0418
      TabIndex        =   18
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2340
      Picture         =   "frmMasterContact.frx":051A
      TabIndex        =   16
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1110
      Picture         =   "frmMasterContact.frx":061C
      TabIndex        =   15
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblKode_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   58
      Top             =   615
      Width           =   1320
   End
   Begin VB.Label lblKode_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1680
      TabIndex        =   57
      Top             =   615
      Width           =   105
   End
   Begin VB.Label lblKode_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   56
      Top             =   615
      Width           =   150
   End
   Begin VB.Image Image1 
      Height          =   2145
      Left            =   6195
      Stretch         =   -1  'True
      Top             =   4950
      Width           =   4095
   End
   Begin VB.Label lblLimit_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7290
      TabIndex        =   50
      Top             =   4005
      Width           =   105
   End
   Begin VB.Label lblLimit_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Limit Kredit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5760
      TabIndex        =   49
      Top             =   4005
      Width           =   1320
   End
   Begin VB.Label lblWilayah2_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7290
      TabIndex        =   48
      Top             =   3585
      Width           =   105
   End
   Begin VB.Label lblWilayah2_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5760
      TabIndex        =   47
      Top             =   3585
      Width           =   1320
   End
   Begin VB.Label lblWilayah_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7290
      TabIndex        =   46
      Top             =   2775
      Width           =   105
   End
   Begin VB.Label lblWilayah_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5760
      TabIndex        =   45
      Top             =   2775
      Width           =   1320
   End
   Begin VB.Label lblWilayah1_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5760
      TabIndex        =   44
      Top             =   3180
      Width           =   1320
   End
   Begin VB.Label lblWilayah1_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7290
      TabIndex        =   43
      Top             =   3180
      Width           =   105
   End
   Begin VB.Label lblHP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   42
      Top             =   2970
      Width           =   105
   End
   Begin VB.Label lblHP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "HP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   41
      Top             =   2970
      Width           =   1320
   End
   Begin VB.Label lblNPWP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Npwp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   40
      Top             =   4590
      Width           =   1320
   End
   Begin VB.Label lblNPWP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   39
      Top             =   4590
      Width           =   105
   End
   Begin VB.Label lblStatus_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5760
      TabIndex        =   38
      Top             =   585
      Width           =   1320
   End
   Begin VB.Label lblStatus_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7290
      TabIndex        =   37
      Top             =   585
      Width           =   105
   End
   Begin VB.Label lblCP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   36
      Top             =   1350
      Width           =   105
   End
   Begin VB.Label lblCP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Contact Person"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   35
      Top             =   1350
      Width           =   1320
   End
   Begin VB.Label lblRek_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   34
      Top             =   4185
      Width           =   105
   End
   Begin VB.Label lblRek_1 
      BackStyle       =   0  'Transparent
      Caption         =   "No Rekening"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   33
      Top             =   4185
      Width           =   1320
   End
   Begin VB.Label lblEmail_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   32
      Top             =   3780
      Width           =   105
   End
   Begin VB.Label lblEmail_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   31
      Top             =   3780
      Width           =   1320
   End
   Begin VB.Label lblFax_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   30
      Top             =   3375
      Width           =   105
   End
   Begin VB.Label lblFax_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   29
      Top             =   3375
      Width           =   1320
   End
   Begin VB.Label lblKeterangan_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7290
      TabIndex        =   27
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label lblNama_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1500
      TabIndex        =   26
      Top             =   975
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   25
      Top             =   5490
      Width           =   2130
   End
   Begin VB.Label lblTelp_1 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   24
      Top             =   2565
      Width           =   1320
   End
   Begin VB.Label lblTelp_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   23
      Top             =   2565
      Width           =   105
   End
   Begin VB.Label lblAlamat_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   22
      Top             =   1755
      Width           =   1320
   End
   Begin VB.Label lblAlamat_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   21
      Top             =   1755
      Width           =   105
   End
   Begin VB.Label lblNama_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   20
      Top             =   990
      Width           =   105
   End
   Begin VB.Label lblNama_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   19
      Top             =   990
      Width           =   1320
   End
   Begin VB.Label lblKeterangan_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5760
      TabIndex        =   28
      Top             =   1020
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbWilayah_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbWilayah1.Clear
    rs.Open "select distinct wilayah from ms_contact where area='" & cmbWilayah.text & "' order by wilayah", conn
    While Not rs.EOF
        cmbWilayah1.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub cmbWilayah1_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbWilayah2.Clear
    rs.Open "select distinct wilayah1 from ms_contact where area='" & cmbWilayah.text & "' and wilayah='" & cmbWilayah1.text & "' order by wilayah1", conn
    While Not rs.EOF
        cmbWilayah2.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close

End Sub

Private Sub cmdBrowse_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.jpg|*.jpg"
    CommonDialog1.filename = "Image Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        Set Image1.Picture = LoadPicture(CommonDialog1.filename)
        txtFile = CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
   
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "delete from ms_contact where kode_contact='" & txtField(0).text & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub
Private Sub load_combo1()
End Sub

Private Sub cmdLoadRpt_Click()
    frmSearch.query = "select kode_contact,[nama_contact],Contact_Person,Alamat,Telp,HP,Fax,Email,No_Rekening,keterangan,npwp,status,area,wilayah,wilayah1,limitkredit from rpt_ms_contact"
    frmSearch.nmform = "frmMastercontact"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_contact"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = SearchContact
    frmSearch.nmform = "frmMastercontact"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_contact"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
 
       For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
 
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    rs.Open "select * from ms_contact where kode_contact='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_contact where kode_contact='" & txtField(0).text & "'"
        

    End If
    rs.Close
    add_data
    If txtFile <> "" Then
        rs.Open "select * from ms_contact where [kode_contact]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        Dim S As New ADODB.Stream
        With S
            .Type = adTypeBinary
            .Open
            .LoadFromFile txtFile
            rs("foto").value = .Read
            rs.Update
        End With
        Set S = Nothing
    End If

    
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(16)
    ReDim nilai(16)
    table_name = "ms_contact"
    fields(0) = "kode_contact"
    fields(1) = "nama_contact"
    fields(2) = "kategori"
    fields(3) = "alamat"
    fields(4) = "telp"
    fields(5) = "hp"
    fields(6) = "keterangan"
    fields(7) = "fax"
    fields(8) = "email"
    fields(9) = "no_rekening"
    fields(10) = "contact_person"
    fields(11) = "area"
    fields(12) = "wilayah"
    fields(13) = "status"
    fields(14) = "npwp"
    fields(15) = "wilayah1"
    fields(2) = "limitkredit"
           
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text

    nilai(3) = txtField(2).text
    nilai(4) = txtField(3).text
    nilai(5) = txtField(4).text
    nilai(6) = txtField(10).text
    nilai(7) = txtField(5).text
    nilai(8) = txtField(6).text
    nilai(9) = txtField(7).text
    nilai(10) = txtField(8).text
    nilai(11) = cmbWilayah.text
    nilai(12) = cmbWilayah1.text
    nilai(13) = cmbStatus.text
    nilai(14) = txtField(9).text
    nilai(15) = cmbWilayah2.text
    nilai(2) = txtField(11).text
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    table_name = "rpt_ms_contact"
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(4).text = ""
    txtField(5).text = ""
    txtField(6).text = ""
    txtField(7).text = ""
    txtField(8).text = ""
    txtField(9).text = ""
    txtField(10).text = ""
    txtField(11).text = "0"
    cmbStatus.text = ""
    txtFile = ""
    Image1.Picture = Nothing
    txtField(0).SetFocus
    
End Sub
Public Sub cari_data()
On Error Resume Next
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_contact  " & _
            "where kode_contact='" & txtField(0).text & "' ", conn
    
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        txtField(4).text = rs(4)
        txtField(5).text = rs(7)
        txtField(6).text = rs(8)
        txtField(7).text = rs(9)
        txtField(8).text = rs(10)
        txtField(9).text = rs!npwp
        txtField(10).text = rs(5)
        txtField(11).text = rs!limitkredit

        cmbStatus.text = rs!status
        cmbWilayah.text = rs!area
        cmbWilayah1.text = rs!wilayah
        cmbWilayah2.text = rs!wilayah1
        If Not IsNull(rs!foto) Then
            Dim mStream As New ADODB.Stream
            Dim fs As New Scripting.FileSystemObject
            fs.DeleteFile App.Path & "\temp.jpg"
              With mStream
                .Type = adTypeBinary
                .Open
                .Write rs("foto").value
                .SaveToFile App.Path & "\temp.jpg"
                Image1.Picture = LoadPicture(App.Path & "\temp.jpg")
                txtFile = App.Path & "\temp.jpg"
              End With
              Set mStream = Nothing
          End If
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
    
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    altercontact
    load_combo
    reset_form
    security_setting
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub load_combo()
    
    conn.ConnectionString = strcon
    conn.Open
    cmbStatus.Clear
    rs.Open "select distinct status from ms_contact order by status", conn
    While Not rs.EOF
        cmbStatus.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbWilayah.Clear
    rs.Open "select distinct area from ms_contact order by area", conn
    While Not rs.EOF
        cmbWilayah.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close

    conn.Close

End Sub
Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index))
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub

Private Sub security_setting()
    Dim kolom As String
    Dim split_kolom() As String
    
    
    conn.Open strcon
    rs.Open "select * from user_groupquery where nama_group='" & usergroup & "' and nama_table='ms_contact'", conn
    If Not rs.EOF Then
        If rs!columnlist <> "" Then
            kolom = rs!columnlist
            txtField(0).Visible = False
            lblKode_1.Visible = False
            lblKode_2.Visible = False
            lblKode_3.Visible = False
            
            txtField(1).Visible = False
            lblNama_1.Visible = False
            lblNama_2.Visible = False
            lblNama_3.Visible = False
        
            txtField(2).Visible = False
            lblAlamat_1.Visible = False
            lblAlamat_2.Visible = False
        
            txtField(3).Visible = False
            lblTelp_1.Visible = False
            lblTelp_2.Visible = False
        
            txtField(4).Visible = False
            lblHP_1.Visible = False
            lblHP_2.Visible = False
        
            txtField(10).Visible = False
            lblKeterangan_1.Visible = False
            lblKeterangan_2.Visible = False
        
            txtField(5).Visible = False
            lblFax_1.Visible = False
            lblFax_2.Visible = False
        
            txtField(6).Visible = False
            lblEmail_1.Visible = False
            lblEmail_2.Visible = False
        
            txtField(7).Visible = False
            lblRek_1.Visible = False
            lblRek_2.Visible = False
        
            txtField(8).Visible = False
            lblCP_1.Visible = False
            lblCP_2.Visible = False
        
            cmbWilayah.Visible = False
            lblWilayah_1.Visible = False
            lblWilayah_2.Visible = False
        
            cmbWilayah1.Visible = False
            lblWilayah1_1.Visible = False
            lblWilayah1_2.Visible = False
        
            cmbStatus.Visible = False
            lblStatus_1.Visible = False
            lblStatus_2.Visible = False
        
            txtField(9).Visible = False
            lblNPWP_1.Visible = False
            lblNPWP_2.Visible = False
        
            cmbWilayah2.Visible = False
            lblWilayah2_1.Visible = False
            lblWilayah2_2.Visible = False
        
            txtField(11).Visible = False
            lblLimit_1.Visible = False
            lblLimit_2.Visible = False
            
            txtFile.Visible = False
            cmdBrowse.Visible = False
            Image1.Visible = False
        End If
    End If
    rs.Close
    split_kolom = Split(kolom, ",")
    For Each Column In split_kolom
        Select Case LCase(Column)
        Case "kode_contact"
            txtField(0).Visible = True
            lblKode_1.Visible = True
            lblKode_2.Visible = True
            lblKode_3.Visible = True
        Case "nama_contact"
            txtField(1).Visible = True
            lblNama_1.Visible = True
            lblNama_2.Visible = True
            lblNama_3.Visible = True
        Case "alamat"
            txtField(2).Visible = True
            lblAlamat_1.Visible = True
            lblAlamat_2.Visible = True
        Case "telp"
            txtField(3).Visible = True
            lblTelp_1.Visible = True
            lblTelp_2.Visible = True
        Case "hp"
            txtField(4).Visible = True
            lblHP_1.Visible = True
            lblHP_2.Visible = True
        Case "keterangan"
            txtField(10).Visible = True
            lblKeterangan_1.Visible = True
            lblKeterangan_2.Visible = True
        Case "fax"
            txtField(5).Visible = True
            lblFax_1.Visible = True
            lblFax_2.Visible = True
        Case "email"
            txtField(6).Visible = True
            lblEmail_1.Visible = True
            lblEmail_2.Visible = True
        Case "no_rekening"
            txtField(7).Visible = True
            lblRek_1.Visible = True
            lblRek_2.Visible = True
        Case "contact_person"
            txtField(8).Visible = True
            lblCP_1.Visible = True
            lblCP_2.Visible = True
        Case "area"
            cmbWilayah.Visible = True
            lblWilayah_1.Visible = True
            lblWilayah_2.Visible = True
        Case "wilayah"
            cmbWilayah1.Visible = True
            lblWilayah1_1.Visible = True
            lblWilayah1_2.Visible = True
        Case "status"
            cmbStatus.Visible = True
            lblStatus_1.Visible = True
            lblStatus_2.Visible = True
        Case "npwp"
            txtField(9).Visible = True
            lblNPWP_1.Visible = True
            lblNPWP_2.Visible = True
        Case "wilayah1"
            cmbWilayah2.Visible = True
            lblWilayah2_1.Visible = True
            lblWilayah2_2.Visible = True
        Case "limitkredit"
            txtField(11).Visible = True
            lblLimit_1.Visible = True
            lblLimit_2.Visible = True
        Case "foto"
            txtFile.Visible = True
            cmdBrowse.Visible = True
            Image1.Visible = True
        End Select
    Next
    conn.Close
End Sub
