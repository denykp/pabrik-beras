VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmprint 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1035
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3330
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1035
   ScaleWidth      =   3330
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkProperties 
      Caption         =   "Print Properties"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   2
      Top             =   675
      Width           =   2085
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   2940
      Top             =   615
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdPrinter 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Printer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1710
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   180
      Width           =   1455
   End
   Begin VB.CommandButton cmdScreen 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Screen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   135
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   180
      Width           =   1455
   End
End
Attribute VB_Name = "frmprint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public strfilename, strFormula, param1, param2, param3, param4 As String
Public sort1, sort2 As String
Private Sub cmdScreen_Click()
cetak False
End Sub
Private Sub cetak(keprinter As Boolean)
On Error GoTo err
Dim LogonInfo As String
    
    With CRPrint
        .reset
        .ReportFileName = reportDir & strfilename
        
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & dbname1 & ";UID=" & User & ";PWD=" & pwd & ";"
            LogonInfo = .LogonInfo(i)
        Next
        
        If strfilename <> "\Neraca.rpt" Then .SelectionFormula = strFormula
        If strfilename = "\Neraca.rpt" Then
            .SubreportToChange = "neraca_aktiva"
            For i = 0 To 3
            .LogonInfo(i) = LogonInfo
            Next
            .SubreportToChange = "neraca_pasiva"
            For i = 0 To 3
            .LogonInfo(i) = LogonInfo
            Next
        End If
         If strfilename = "\LabaRugi.rpt" Then
'            .Formulas(0) = "PendapatanLain=" & PendapatanLaba & ""
            .Formulas(0) = "strPeriode='" & strPeriodeLaba & "'"
        End If
        

        If strfilename = "\Laporan Produksi.rpt" Then
            .SubreportToChange = "SubKomposisi"
            For i = 0 To 1
            .LogonInfo(i) = LogonInfo
            Next
            .SubreportToChange = "SubHasil"
            For i = 0 To 1
            .LogonInfo(i) = LogonInfo
            Next
            .SubreportToChange = "SubBiaya"
            For i = 0 To 1
            .LogonInfo(i) = LogonInfo
            Next
            .SubreportToChange = "SubKomposisiHasil"
            For i = 0 To 2
            .LogonInfo(i) = LogonInfo
            Next
            .SubreportToChange = "SubKemasan"
            For i = 0 To 2
            .LogonInfo(i) = LogonInfo
            Next

        End If
        
        If strfilename = "\Laporan Penerimaan Piutang.rpt" Then
            .SubreportToChange = "BayarTransfer"
            .LogonInfo(0) = LogonInfo
            .SubreportToChange = "BayarBG"
            .LogonInfo(0) = LogonInfo
        End If
        
        If strfilename = "\Laporan Pembayaran Hutang.rpt" Then
            .SubreportToChange = "BayarTransfer"
            .LogonInfo(0) = LogonInfo
            .SubreportToChange = "BayarBG"
            .LogonInfo(0) = LogonInfo
        End If
        
        .ParameterFields(0) = "login;" & User & ";True"
        .ParameterFields(1) = "tglDari;" & param1 & ";True"
        .ParameterFields(2) = "tglSampai;" & param2 & ";True"
        If param3 <> "" Then .ParameterFields(3) = "Detail;" & param3 & ";True"
        '.ParameterFields(4) = "detail;" & param4 & ";True"
        If Left(strfilename, 29) = "\Laporan Penjualan HPP" Or Left(strfilename, 33) = "\Laporan Pembayaran Hutang" Then .ParameterFields(3) = "detail;" & param3 & ";True"
        
        
'        If sort1 <> "" Then
'        .SortFields(0) = sort1
'        End If
'        If sort2 <> "" Then
'        .SortFields(1) = sort2
'        End If
         If chkProperties.value = 1 Then .PrinterSelect
        If keprinter Then
        .Destination = crptToPrinter
        Else
            .Destination = crptToWindow
            .WindowTitle = "Cetak" & PrintMode
            .WindowState = crptMaximized
            .WindowShowPrintSetupBtn = True
            .WindowShowPrintBtn = True
            .WindowShowExportBtn = True
        End If
        Me.Hide
        .action = 1
        'If strfilename = "\Sales\Faktur Pajak.rpt" Or strfilename = "\Sales\Faktur Non Pajak.rpt" Then
        '    Conn.Execute "update tr_pi_h set fktr_fg='Y' where no_pi='" & Trim(frmPrintFaktur.txtPI_NO.Text) & "'"
        'End If
    End With
    Exit Sub
err:
    MsgBox err.Description
    Resume Next

End Sub

Private Sub CmdPrinter_Click()
cetak True
End Sub

Private Sub Form_Activate()
'    SetConnection1 "", ""
End Sub

Private Sub Form_Deactivate()
'    DropConnection
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

