VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Login"
   ClientHeight    =   2850
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4425
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2850
   ScaleWidth      =   4425
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbGudang 
      Height          =   315
      Left            =   1485
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1710
      Visible         =   0   'False
      Width           =   1410
   End
   Begin VB.ComboBox txtServerName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1485
      Style           =   1  'Simple Combo
      TabIndex        =   8
      Top             =   540
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2070
      TabIndex        =   6
      Top             =   2160
      Width           =   1500
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "&Login"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   300
      TabIndex        =   5
      Top             =   2160
      Width           =   1500
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1485
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   1305
      Width           =   1905
   End
   Begin VB.TextBox txtUserName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   1
      Top             =   900
      Width           =   1905
   End
   Begin VB.Label Label5 
      Height          =   195
      Left            =   45
      TabIndex        =   10
      Top             =   2565
      Width           =   150
   End
   Begin VB.Label Label3 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   9
      Top             =   1755
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Label Label4 
      Caption         =   "DB"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   7
      Top             =   585
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.Label Label2 
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   2
      Top             =   1350
      Width           =   1005
   End
   Begin VB.Label Label1 
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   0
      Top             =   945
      Width           =   1005
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public mSQLServer As New SQLDMO.SQLServer
'Public mApplication As New SQLDMO.Application
Private OpenForm_Fg As Boolean

Private Sub isicombo()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    Call SetConnection(txtServerName.text, txtUserName.text, txtPassword.text)
    

    cmbGudang.Clear
    conn.Open
    rs.Open "select * from var_gudang", conn
   
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbGudang.ListIndex = 0
    Call DropConnection

    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
End Sub

Private Sub cmbGudang_GotFocus()
    isicombo
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLogin_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject

    SetConnection App.Path & "\db.mdb", "", ""

    conn.Open
    rs.Open "select * from login where username='" & txtUserName.text & "'", conn
    
    If Not rs.EOF Then
        If rs(1) = RC4(LCase(txtPassword), katasandi) Then
        Else
            MsgBox "Password yang anda masukkan salah"
            txtPassword.text = ""
            txtPassword.SetFocus
            GoTo ex
        End If
    Else
        MsgBox "Username tidak ditemukan"
        txtPassword.text = ""
        txtUserName.text = ""
        txtUserName.SetFocus
        GoTo ex
    End If
    rs.Close
    user = txtUserName.text
    If cmbGudang.text = "" Then
        MsgBox "Silahkan memilih gudang terlebih dahulu"
        Exit Sub
    End If
    gudang = cmbGudang.text

    
    rs.Open "Select * from awal_pakai", conn
    If rs.EOF Then
        conn.Execute "Insert into awal_pakai(bulan,tahun) values ('" & Format(Date, "M") & "','" & Format(Date, "yyyy") & "') "
        conn.Execute "Insert into periode_aktif(bulan,tahun) values ('" & Format(Date, "M") & "','" & Format(Date, "yyyy") & "') "
    End If
    rs.Close
    
    rs.Open "Select * from awal_pakai", conn
    gBulanAwal = rs("bulan")
    gTahunAwal = rs("tahun")
    rs.Close
    

    
    Call DropConnection
    frmMain.Show
    OpenForm_Fg = True
    Unload Me
    Exit Sub
err:
    MsgBox err.Description
ex:
    If rs.State Then rs.Close
    DropConnection
    'txtUserName.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()

    
   Dim i As Integer
'   Dim ServerList As SQLDMO.NameList
'
'   Set ServerList = mApplication.ListAvailableSQLServers
'
'   For i = 1 To ServerList.Count
'       txtServerName.AddItem ServerList.Item(i)
'       txtServerNameT.AddItem ServerList.Item(i)
'   Next i

    OpenForm_Fg = False
    getConnString ""
    
    txtServerName.text = servname
    isicombo
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If OpenForm_Fg = False Then Call DropConnection
End Sub

Private Sub Label5_Click()
    txtServerName.Visible = True
    Label4.Visible = True
End Sub
