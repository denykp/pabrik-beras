VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMasterSAStock 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hitung Ulang Stock"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7725
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   7725
   Begin VB.CommandButton Command7 
      Caption         =   "HPP Jual"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   510
      Picture         =   "frmMasterSAStock.frx":0000
      TabIndex        =   31
      Top             =   3960
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton Command6 
      Caption         =   "HPP Nol"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   495
      Picture         =   "frmMasterSAStock.frx":0102
      TabIndex        =   30
      Top             =   3375
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Hapus Stock Nol"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   5625
      Picture         =   "frmMasterSAStock.frx":0204
      TabIndex        =   29
      Top             =   1515
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Opname"
      Height          =   345
      Left            =   6345
      TabIndex        =   28
      Top             =   570
      Width           =   1260
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Urutkan Kartu Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   5580
      TabIndex        =   27
      Top             =   2085
      Visible         =   0   'False
      Width           =   2025
   End
   Begin MSComctlLib.ProgressBar P1 
      Height          =   240
      Left            =   4935
      TabIndex        =   26
      Top             =   2685
      Visible         =   0   'False
      Width           =   2715
      _ExtentX        =   4789
      _ExtentY        =   423
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Hitung Ulang Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5685
      Picture         =   "frmMasterSAStock.frx":0306
      TabIndex        =   25
      Top             =   3030
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      ItemData        =   "frmMasterSAStock.frx":0408
      Left            =   1800
      List            =   "frmMasterSAStock.frx":040A
      Style           =   2  'Dropdown List
      TabIndex        =   23
      Top             =   1005
      Width           =   4305
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Master Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6225
      Picture         =   "frmMasterSAStock.frx":040C
      TabIndex        =   22
      Top             =   960
      Width           =   1410
   End
   Begin VB.CommandButton cmdEditHPP 
      Caption         =   "&Edit Qty && HPP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5805
      Picture         =   "frmMasterSAStock.frx":050E
      TabIndex        =   21
      Top             =   3720
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1740
   End
   Begin VB.ComboBox cmbGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   600
      Width           =   1845
   End
   Begin VB.TextBox txtSerial 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1815
      TabIndex        =   20
      Top             =   2190
      Width           =   2940
   End
   Begin VB.TextBox txtHpp 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      MaxLength       =   25
      TabIndex        =   4
      Top             =   2970
      Width           =   1530
   End
   Begin VB.TextBox txtQty 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      MaxLength       =   25
      TabIndex        =   3
      Top             =   2580
      Width           =   1320
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   5700
      TabIndex        =   1
      Top             =   165
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   144506881
      CurrentDate     =   40492
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4455
      Picture         =   "frmMasterSAStock.frx":0610
      TabIndex        =   7
      Top             =   3720
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3225
      Picture         =   "frmMasterSAStock.frx":0712
      TabIndex        =   6
      Top             =   3720
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3810
      TabIndex        =   13
      Top             =   150
      Width           =   405
   End
   Begin VB.TextBox txtNoTrans 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      Locked          =   -1  'True
      MaxLength       =   12
      TabIndex        =   0
      Top             =   195
      Width           =   1890
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4785
      Picture         =   "frmMasterSAStock.frx":0814
      TabIndex        =   10
      Top             =   1425
      UseMaskColor    =   -1  'True
      Width           =   405
   End
   Begin VB.TextBox txtKode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      TabIndex        =   2
      Top             =   1410
      Width           =   2940
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1995
      Picture         =   "frmMasterSAStock.frx":0916
      TabIndex        =   5
      Top             =   3720
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   24
      Top             =   1035
      Width           =   810
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   18
      Top             =   645
      Width           =   930
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "No.Serial"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   17
      Top             =   2220
      Width           =   1320
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Hpp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   16
      Top             =   3030
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   15
      Top             =   2610
      Width           =   1320
   End
   Begin VB.Label lblNama 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1815
      TabIndex        =   14
      Top             =   1860
      Width           =   645
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "No.Saldo Awal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   12
      Top             =   210
      Width           =   1440
   End
   Begin VB.Label lblParent 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3405
      TabIndex        =   11
      Top             =   270
      Width           =   60
   End
   Begin VB.Label lblKode 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   9
      Top             =   1440
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4635
      TabIndex        =   8
      Top             =   210
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterSAStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String


Private Sub cmdEditHPP_Click()
Dim i As Byte
Dim J As Integer
On Error GoTo err
    i = 0
    If txtKode = "" Then
        MsgBox "Silahkan mengisi data terlebih dahulu!"
        txtKode.SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
'    conn.Execute "update sa_stock set qty=" & txtQty & ", hpp=" & txtHpp & ",total= " & CDbl(txtQty) * CDbl(txtHpp) & " where nomer_transaksi='" & txtNoTrans & "'"
'    conn.Execute "update stock set stock=" & txtQty & ", hpp=" & txtHpp & " where kode_bahan='" & txtKode & "' and nomer_serial='" & txtSerial & "'"
'    conn.Execute "update hst_hpp set stockawal=0,hppawal=0,qty=" & txtQty & ", harga=" & txtHpp & ",hpp=" & txtHpp & ",stockakhir=" & txtQty & " where nomer_transaksi='" & txtNoTrans & "'"
'

    conn.Execute "update sa_stock set qty=" & Replace(txtQty, ",", ".") & ", hpp=" & Replace(txtHpp, ",", ".") & ",total= " & Replace(CDbl(txtQty) * CDbl(txtHpp), ",", ".") & " where nomer_transaksi='" & txtNoTrans & "'"
    'conn.Execute "update stock set nomer_serial='" & txtSerial.text & "',stock=" & Replace(txtQty, ",", ".") & ", hpp=" & Replace(txtHpp, ",", ".") & " where kode_bahan='" & txtKode & "' and kode_gudang='" & cmbGudang.text & "' and nomer_serial='" & txtSerial & "'"
    conn.Execute "update hst_hpp set stockawal=0,hppawal=0,qty=" & Replace(txtQty, ",", ".") & ", harga=" & Replace(txtHpp, ",", ".") & ",hpp=" & Replace(txtHpp, ",", ".") & ",stockakhir=" & Replace(txtQty, ",", ".") & " where kode_bahan='" & txtKode & "' and kode_gudang='" & cmbGudang.text & "' and nomer_serial='" & txtSerial & "'"
    conn.Execute "update tmp_kartustock set nomer_serial='" & txtSerial.text & "',stockawal=" & Replace(txtQty, ",", ".") & " where kode_bahan='" & txtKode & "' and kode_gudang='" & cmbGudang.text & "' and nomer_serial='" & txtSerial & "' and id='" & txtNoTrans & "'"
    conn.Execute "update stock set hpp=" & Replace(txtHpp, ",", ".") & ",stock=(select sum(qty) from sa_stock where kode_bahan='" & txtKode & "' and kode_gudang='" & cmbGudang.text & "' and nomer_serial='" & txtSerial & "') from stock where kode_bahan='" & txtKode & "' and kode_gudang='" & cmbGudang.text & "' and nomer_serial='" & txtSerial & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
'    If cmdEditHPP.Visible = False Then
'        frmSearch.query = "select * from ms_bahan where kode_bahan not in (select  kode_bahan from sa_stock where kode_gudang='" & cmbGudang.text & "')  "
'    Else
'        frmSearch.query = "select * from ms_bahan where kode_bahan in (select  kode_bahan from sa_stock where kode_gudang='" & cmbGudang.text & "')  "
'    End If

    frmSearch.query = "select * from ms_bahan where kategori='" & cmbField(1).text & "'"
    
'    frmSearch.query = "select * from ms_bahan where kode_bahan in (select kode_bahan from sa_stock where kode_gudang='" & cmbGudang.text & "')  "
    frmSearch.nmform = "frmMasterSAStock"
    frmSearch.nmctrl = "txtKode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchID_Click()
    frmSearch.query = "select * from sa_stock "
    frmSearch.nmform = "frmMasterSAStock"
    frmSearch.nmctrl = "txtNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "saStockKertas"
    frmSearch.connstr = strcon
    frmSearch.proc = "cek_notrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Sub cmdSimpan_Click()
Dim i As Byte
Dim J As Integer
On Error GoTo err
    i = 0
    If txtKode = "" Then
        MsgBox "Silahkan mengisi data terlebih dahulu!"
        txtKode.SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    If txtNoTrans = "-" Then
        nomor_baru
        add_data
        If Trim(txtHpp.text) = "" Then txtHpp.text = 0
         insertkartustock "Saldo Awal", txtNoTrans, DTPicker1, txtKode, txtQty, txtQty, cmbGudang.text, txtSerial, CDbl(txtHpp.text), conn

'        updateOpnameStockKertas txtKode.text, Replace(txtQty, ",", "."), txtSerial.text, cmbGudang.text, conn
        updateOpnameStockKertas2 txtKode.text, txtQty, txtSerial.text, cmbGudang.text, conn
        
        conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & txtKode & "' and nomer_serial='" & txtSerial.text & "') insert into stock  (kode_bahan,nomer_serial,stock,hpp,kode_gudang) values ('" & txtKode.text & "','" & txtSerial.text & "'," & Replace(txtQty, ",", ".") & "," & Replace(txtHpp, ",", ".") & ",'" & cmbGudang.text & "') else " & _
        "update stock set hpp=" & Replace(txtHpp, ",", ".") & " where kode_bahan='" & txtKode & "'"
        
        
       
        rs.Open "select * from hst_hpp where kode_bahan='" & txtKode.text & "'", conn
        If rs.EOF Then
            rs.Close
            add_dataHstHpp 1, conn
        End If
    If rs.State Then rs.Close
        
    End If
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(8)
    ReDim nilai(8)
    table_name = "sa_stock"
    
    fields(0) = "nomer_transaksi"
    fields(1) = "tanggal"
    fields(2) = "kode_gudang"
    fields(3) = "kode_bahan"
    fields(4) = "nomer_serial"
    fields(5) = "qty"
    fields(6) = "hpp"
    fields(7) = "total"
    
    
    nilai(0) = txtNoTrans.text
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = cmbGudang.text
    nilai(3) = txtKode
    nilai(4) = txtSerial
    nilai(5) = Replace(txtQty, ",", ".")
    nilai(6) = Format(txtHpp, "###0")
    nilai(7) = nilai(5) * nilai(6)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_dataStock()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "stock"
    
    
    fields(0) = "kode_bahan"
    fields(1) = "nomer_serial"
    fields(2) = "stock"
    fields(3) = "hpp"
    fields(4) = "kode_gudang"
    
    nilai(0) = txtKode
    nilai(1) = txtSerial
    nilai(2) = Replace(txtQty, ",", ".")
    nilai(3) = Format(txtHpp, "###0")
    nilai(4) = cmbGudang.text
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_dataHstHpp(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(12)
    ReDim nilai(12)

    table_name = "hst_hpp"
    fields(0) = "jenis"
    fields(1) = "nomer_transaksi"
    fields(2) = "kode_gudang"
    fields(3) = "nomer_serial"
    fields(4) = "tanggal"
    fields(5) = "kode_bahan"
    fields(6) = "stockawal"
    fields(7) = "hppawal"
    fields(8) = "qty"
    fields(9) = "harga"
    fields(10) = "hpp"
    fields(11) = "stockakhir"

    nilai(0) = "Saldo Awal"
    nilai(1) = txtNoTrans.text
    nilai(2) = cmbGudang.text
    nilai(3) = txtSerial.text
    nilai(4) = Format(Now, "yyyy/MM/dd HH:mm:ss")
    nilai(5) = txtKode.text
    nilai(6) = 0
    nilai(7) = 0
    nilai(8) = Replace(txtQty.text, ",", ".")
    nilai(9) = Replace(txtHpp.text, ",", ".")
    nilai(10) = Replace(txtHpp.text, ",", ".")
    nilai(11) = Replace(txtQty.text, ",", ".")
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtKode.text & "'", cn
    End If
End Sub

Private Sub reset_form()
On Error Resume Next
    txtNoTrans.text = "-"
    txtKode.text = ""
    txtKode.SetFocus
    lblNama.Caption = ""
    txtSerial = ""
'    DTPicker1 = Format(Now, "dd/MM/yyyy hh:mm:ss")
    txtQty = 1
    txtHpp = 0
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    cmdSimpan.Enabled = True
    txtKode.SetFocus
End Sub

Public Sub cari_data()
Dim rs  As New ADODB.Recordset
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where kode_bahan='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        lblNama.Caption = rs(1)
        txtSerial.text = txtKode.text
    Else
        lblNama.Caption = ""
        txtSerial.text = ""
    End If
    rs.Close
    conn.Close
End Sub

Public Sub cek_notrans()
Dim rs  As New ADODB.Recordset
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from sa_stock where nomer_transaksi='" & txtNoTrans.text & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs!tanggal
        txtKode = rs!kode_bahan
        cari_data
        txtQty = rs!qty
        txtHpp = rs!hpp
        txtSerial = rs!nomer_serial
        SetComboText (rs!kode_gudang), cmbGudang
'        cmdSimpan.Enabled = False
    End If
    rs.Close
    conn.Close
    
End Sub




Private Sub Command1_Click()
Dim rs  As New ADODB.Recordset, i As Long
Dim rs2 As New ADODB.Recordset
Dim conn As New ADODB.Connection, hpp As Double
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    rs.Open "select kode_gudang,kode_bahan,nomer_serial,sum(masuk-keluar) as total " & _
            "from tmp_kartustock " & _
            "where kode_bahan like '%" & txtKode.text & "%' " & _
            "group by kode_gudang,kode_bahan,nomer_serial", conn, adOpenDynamic, adLockOptimistic
            
    
    If Not rs.EOF Then
        P1.Visible = True
'        P1.Max = rs.RecordCount
        P1.value = 50
        
        Do While Not rs.EOF
'            P1.value = i
            
            conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & (rs!kode_bahan) & "' and nomer_serial='" & (rs!nomer_serial) & "' and kode_gudang='" & (rs!kode_gudang) & "' ) " & _
                         "insert into stock  (kode_bahan,kode_gudang,nomer_serial,stock) values ('" & (rs!kode_bahan) & "','" & (rs!kode_gudang) & "','" & (rs!nomer_serial) & "'," & Replace(rs!total, ",", ".") & ") else " & _
                         "update stock set stock =" & Replace((rs!total), ",", ".") & " " & _
                        "where kode_bahan='" & (rs!kode_bahan) & "' " & _
                        "and nomer_serial='" & (rs!nomer_serial) & "' " & _
                        "and kode_gudang='" & (rs!kode_gudang) & "' "
                        
            rs2.Open "select top 1 hpp from hst_hpp where kode_bahan='" & (rs!kode_bahan) & "' " & _
                        "and nomer_serial='" & (rs!nomer_serial) & "' " & _
                        "and kode_gudang='" & (rs!kode_gudang) & "' " & _
                        "order by tanggal desc", conn
            hpp = 0
            If Not rs2.EOF Then
                hpp = (rs2!hpp)
            End If
            rs2.Close
            If hpp > 0 Then
                conn.Execute "update stock set hpp=" & Replace(hpp, ",", ".") & " " & _
                             "where kode_bahan='" & (rs!kode_bahan) & "' " & _
                            "and nomer_serial='" & (rs!nomer_serial) & "' " & _
                            "and kode_gudang='" & (rs!kode_gudang) & "' "
            End If
            
            i = i + 1
            rs.MoveNext
        Loop
    End If
    P1.Visible = False
    rs.Close
    
    conn.CommitTrans
    conn.Close
End Sub

Private Sub Command2_Click()
    frmMasterBarang.Show vbModal
End Sub

Private Sub Command3_Click()
    conn.Open strcon
    conn.BeginTrans
    HitungUlang_KartuStock txtKode.text, cmbGudang.text, txtSerial.text, conn
    conn.CommitTrans
    conn.Close
    MsgBox "Selesai"
End Sub

Private Sub Command4_Click()
Dim rs  As New ADODB.Recordset, i As Long
Dim rs2 As New ADODB.Recordset
Dim qty As Double, total As Double, selisih As Double
Dim conn As New ADODB.Connection, hpp As Double
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    rs.Open "select t.nomer_opname,t.kode_gudang,d.kode_bahan,d.qty,d.nomer_serial " & _
            "from t_stockopnamed d " & _
            "inner join t_stockopnameh t on t.nomer_opname=d.nomer_opname " & _
            "where CONVERT(VARCHAR(10), t.tanggal, 111)='2011/04/08' AND KODE_BAHAN='350P-DP-109X62,5'", conn, adOpenDynamic, adLockOptimistic
          
'    rs.Open "select t.nomer_opname,t.kode_gudang,d.kode_bahan,d.qty,d.nomer_serial " & _
'            "from t_stockopnamed d " & _
'            "inner join t_stockopnameh t on t.nomer_opname=d.nomer_opname " & _
'            "where CONVERT(VARCHAR(10), t.tanggal, 111)='2011/04/08' ", conn, adOpenDynamic, adLockOptimistic
'
    
    If Not rs.EOF Then
        P1.Visible = True
'        P1.Max = rs.RecordCount
        P1.value = 50
        
        Do While Not rs.EOF
'            P1.value = i
            qty = (rs!qty)
            rs2.Open "select sum(masuk-keluar) as total " & _
                     "from tmp_kartustock " & _
                     "where kode_bahan='" & (rs!kode_bahan) & "' " & _
                     "and nomer_serial='" & (rs!nomer_serial) & "' " & _
                     "and kode_gudang='" & (rs!kode_gudang) & "' " & _
                     "and CONVERT(VARCHAR(10), tanggal, 111)<='2011/04/08' " & _
                     "and id<>'" & (rs!nomer_opname) & "' " & _
                     "group by kode_gudang,kode_bahan,nomer_serial", conn
            
            total = 0
            If Not rs2.EOF And IsNull(rs2!total) = False Then
                total = (rs2!total)
            End If
            rs2.Close
            
            selisih = qty - total
            
            If selisih <> 0 Then
                If selisih > 0 Then
                    conn.Execute "update tmp_kartustock " & _
                                 "set masuk=" & Replace(selisih, ",", ".") & ",keluar=0 " & _
                                 "where id='" & (rs!nomer_opname) & "' " & _
                                 "and kode_bahan='" & (rs!kode_bahan) & "' " & _
                                 "and nomer_serial='" & (rs!nomer_serial) & "' "
                Else
                    conn.Execute "update tmp_kartustock " & _
                                 "set masuk=0,keluar=" & Replace(-selisih, ",", ".") & " " & _
                                 "where id='" & (rs!nomer_opname) & "' " & _
                                 "and kode_bahan='" & (rs!kode_bahan) & "' " & _
                                 "and nomer_serial='" & (rs!nomer_serial) & "' "
                End If
            End If
                        
'            i = i + 1
            rs.MoveNext
        Loop
    End If
    P1.Visible = False
    MsgBox "Proses Selesai!"
    rs.Close
    conn.CommitTrans
    conn.Close
End Sub

Private Sub Command5_Click()
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    conn.Execute "delete from stock_plat where stock=0"
    conn.Execute "delete from stock_tinta where stock=0"
    conn.CommitTrans
    conn.Close
    MsgBox "Proses Selesai !"
End Sub

Private Sub Command6_Click()
Dim rs  As New ADODB.Recordset, i As Long
Dim rs2 As New ADODB.Recordset
Dim conn As New ADODB.Connection, hpp As Double
Dim KodeBahan As String, qty As Double, harga As Double
Dim tanggal As Date
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    rs.Open "SELECT * FROM tmp_jurnald ", conn, adOpenDynamic, adLockOptimistic
            
    
    If Not rs.EOF Then
        P1.Visible = True
'        P1.Max = rs.RecordCount
        P1.value = 50
        
        Do While Not rs.EOF
'            P1.value = i
            KodeBahan = (rs!kode1)
            rs2.Open "select t.tanggal_jualkertas as tanggal,d.kode_bahan,d.qty,d.harga " & _
                     "from t_jualkertasd d " & _
                     "inner join t_jualkertash t on t.nomer_jualkertas=d.nomer_jualkertas " & _
                     "where d.nomer_jualkertas='" & (rs!no_transaksi) & "' and d.kode_bahan='" & KodeBahan & "'", conn
            qty = 0
            If Not rs2.EOF Then
                qty = (rs2!qty)
                harga = (rs2!harga)
                tanggal = (rs2!tanggal)
            End If
            rs2.Close
            
'            rs2.Open "select top 1 hpp from hst_hpp where kode_bahan='" & KodeBahan & "' " & _
'                        "and hpp<>0 and CONVERT(VARCHAR(10), tanggal, 111)<'" & Format(Tanggal, "yyyy/MM/dd") & "' order by tanggal desc", conn

            If qty > 0 Then
                rs2.Open "select top 1 hpp from hst_hpp where kode_bahan='" & KodeBahan & "' " & _
                            "and hpp<>0", conn
    
                hpp = 0
                If Not rs2.EOF Then
                    hpp = (rs2!hpp)
                End If
                rs2.Close
            End If
            If hpp > 0 Then
                conn.Execute "update tmp_jurnald set debet=" & Replace(hpp * qty, ",", ".") & " " & _
                             "where kode1='" & KodeBahan & "' " & _
                            "and no_transaksi='" & (rs!no_transaksi) & "' "
            End If
            
            i = i + 1
            rs.MoveNext
        Loop
    End If
    P1.Visible = False
    rs.Close
'
    conn.CommitTrans
    conn.Close
End Sub

Private Sub Command7_Click()
Dim rs  As New ADODB.Recordset, i As Long
Dim rs2 As New ADODB.Recordset
Dim conn As New ADODB.Connection, hpp As Double
Dim KodeBahan As String, qty As Double, harga As Double
Dim tanggal As Date
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    rs.Open "SELECT * FROM tmp_jurnald ", conn, adOpenDynamic, adLockOptimistic
            
    
    If Not rs.EOF Then
        P1.Visible = True
'        P1.Max = rs.RecordCount
        P1.value = 50
        
        Do While Not rs.EOF
'            P1.value = i
            KodeBahan = (rs!kode1)
            rs2.Open "select t.nomer_jualkertas,d.kode_bahan,(d.qty*d.harga_customer) as total " & _
                     "from t_jualkertasd d " & _
                     "inner join t_jualkertash t on t.nomer_jualkertas=d.nomer_jualkertas " & _
                     "where d.nomer_jualkertas='" & (rs!no_transaksi) & "' and d.kode_bahan='" & KodeBahan & "'", conn
            harga = 0
            If Not rs2.EOF Then
                harga = (rs2!total)
            End If
            rs2.Close
            
            conn.Execute "update tmp_jurnald set kredit2=" & Replace(harga, ",", ".") & " " & _
                         "where kode1='" & KodeBahan & "' " & _
                        "and no_transaksi='" & (rs!no_transaksi) & "' "
            
            i = i + 1
            rs.MoveNext
        Loop
    End If
    P1.Visible = False
    rs.Close
'
    conn.CommitTrans
    conn.Close
    MsgBox "Proses selesai"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    load_combo
    reset_form
   lblKode.Caption = "Kode Bahan"

    
    conn.Open strcon
    cmbGudang.Clear
    rs.Open "select kode_gudang from ms_gudang order by kode_gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    
    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
End Sub



Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub


Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearch_Click
End Sub

Private Sub txtKode_KeyPress(KeyAscii As Integer)
    

    If KeyAscii = 13 And txtKode <> "" Then
        cari_data
    End If
 
    
End Sub

Private Sub txtKode_LostFocus()
    If txtKode <> "" Then cari_data
End Sub
Private Sub nomor_baru()
Dim No As String
'    rs.Open "select top 1 nomer_transaksi from sa_stock where substring(nomer_transaksi,3,4)='" & Format(Now, "yyMM") & "' order by nomer_transaksi desc", conn
'    If Not rs.EOF Then
'        No = "AK" & Format(DTPicker1, "yyMM") & Format((CLng(Right(rs(0), 6)) + 1), "000000")
'    Else
'        No = "AK" & Format(DTPicker1, "yyMM") & Format("1", "000000")
'    End If
    
    rs.Open "select top 1 nomer_transaksi from sa_stock where substring(nomer_transaksi,3,2)='" & Format(DTPicker1, "yy") & "' and substring(nomer_transaksi,5,2)='" & Format(DTPicker1, "MM") & "' order by nomer_transaksi desc", conn
    If Not rs.EOF Then
        No = "AK" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 6)) + 1), "000000")
    Else
        No = "AK" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "000000")
    End If
    
    rs.Close
    txtNoTrans = No
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
   Angka KeyAscii
End Sub

Private Sub txthpp_KeyPress(KeyAscii As Integer)
   Angka KeyAscii
End Sub

Private Sub txtSerial_GotFocus()
    txtSerial.SelStart = 0
    txtSerial.SelLength = Len(txtSerial)
End Sub
Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty)
End Sub
Private Sub txtHpp_GotFocus()
    txtHpp.SelStart = 0
    txtHpp.SelLength = Len(txtHpp)
End Sub

Private Sub txtSerial_KeyPress(KeyAscii As Integer)
    If KeyAscii = 35 Then '#
        KeyAscii = 0
        cmdEditHPP.Visible = True
    ElseIf KeyAscii = 42 Then '*
        KeyAscii = 0
        Command1.Visible = True
    ElseIf KeyAscii = 64 Then '@
        KeyAscii = 0
        Command5.Visible = True
    ElseIf KeyAscii = 36 Then '$
        KeyAscii = 0
        Command3.Visible = True
    End If
End Sub

Private Sub load_combo()
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(1).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close


    If cmbField(1).ListCount > 0 Then cmbField(1).ListIndex = 0

    conn.Close
End Sub

