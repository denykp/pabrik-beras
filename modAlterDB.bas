Attribute VB_Name = "modAlterDB"
Public Sub createterima()
On Error Resume Next
    
    Exit Sub

End Sub
Public Sub dropqcterima()

    conn.Open strcon
    conn.Execute "alter table t_terimabarangh drop qc_fisik_nama,qc_fisik,qc_lab_nama,qc_lab,qc_keterangan,qc_hasil"
    conn.Close
    Exit Sub
    If conn.State Then conn.Close
End Sub
Public Sub addqcterima()
On Error Resume Next
    conn.Open strcon
    conn.Execute "alter table t_terimabarangd add qc_fisik_nama varchar(25)  NULL CONSTRAINT [DF_t_terimabarangD_qc_fisik_nama]  DEFAULT ('')"
    conn.Execute "alter table t_terimabarangd add qc_fisik text  NULL CONSTRAINT [DF_t_terimabarangD_qc_fisik]  DEFAULT ('')"
    conn.Execute "alter table t_terimabarangd add [qc_lab_nama] [varchar](50) NULL CONSTRAINT [DF_t_terimabarangD_qc_lab_nama]  DEFAULT ('')"
    conn.Execute "alter table t_terimabarangd add [qc_lab] [text] NULL CONSTRAINT [DF_t_terimabarangD_qc_lab]  DEFAULT ('')"
    conn.Execute "alter table t_terimabarangd add [qc_keterangan] [text] NULL CONSTRAINT [DF_t_terimabarangD_qc_keterangan]  DEFAULT ('')"
    conn.Execute "alter table t_terimabarangd add [qc_hasil] [varchar](150) NULL CONSTRAINT [DF_t_terimabarangD_qc_hasil]  DEFAULT ('')"
    conn.Close
    Exit Sub
err:
    
End Sub
Public Sub addkulitransport()
On Error Resume Next
    conn.Open strcon
    conn.Execute "CREATE TABLE [dbo].[t_bayarkulid](" & _
    "[nomer_bayarkuli] [varchar](12) NOT NULL," & _
    "[nomer] [varchar](12) NOT NULL, " & _
    "[ongkos] [numeric](18, 0) NULL, " & _
    "[no_urut] [int] NULL, " & _
    "CONSTRAINT [PK_t_bayarkulid] PRIMARY KEY CLUSTERED " & _
    " ( " & _
    " [nomer_bayarkuli] ASC, " & _
    "[nomer] Asc " & _
    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
    ") ON [PRIMARY] "
    conn.Execute "CREATE TABLE [dbo].[t_bayarkulih]( " & _
    "[nomer_bayarkuli] [varchar](12) NOT NULL, " & _
    "[tanggal] [datetime] NULL, " & _
    "[periode_awal] [datetime] NULL,  " & _
    "[periode_akhir] [datetime] NULL, " & _
    "[totalberat] [numeric](18, 0) NULL, " & _
    "[total] [numeric](18, 0) NULL, " & _
    "[userid] [varchar](50) NULL, " & _
    "[status_posting] [char](1) NULL CONSTRAINT [DF_t_bayarkulih_status_posting]  DEFAULT ('0'), " & _
    "CONSTRAINT [PK_t_bayarkulih] PRIMARY KEY CLUSTERED " & _
    "( " & _
    "[nomer_bayarkuli] Asc " & _
    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
    ") ON [PRIMARY]"
    conn.Execute "CREATE TABLE [dbo].[t_ongkostransportd]( " & _
    "[nomer_ongkostransport] [varchar](12) NOT NULL, " & _
    "[nomer] [varchar](12) NOT NULL, " & _
    "[ongkos] [numeric](18, 0) NULL, " & _
    "[no_urut] [int] NULL, " & _
    "CONSTRAINT [PK_t_ongkostransportd] PRIMARY KEY CLUSTERED " & _
    "( " & _
    "[nomer_ongkostransport] ASC, " & _
    "[nomer] Asc " & _
    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
    ") ON [PRIMARY]"
    conn.Execute "CREATE TABLE [dbo].[t_ongkostransporth]( " & _
    "[nomer_ongkostransport] [varchar](12) NOT NULL, " & _
    "[tanggal] [datetime] NULL, " & _
    "[periode_awal] [datetime] NULL, " & _
    "[periode_akhir] [datetime] NULL, " & _
    "[total] [numeric](18, 0) NULL, " & _
    "[cara_bayar] [varchar](10) NULL, " & _
    "[kode_ekspedisi] [varchar](10) NULL, " & _
    "[userid] [varchar](50) NULL, " & _
    "[status_posting] [char](1) NULL CONSTRAINT [DF_t_ongkostransporth_status_posting]  DEFAULT ('0'), " & _
    "CONSTRAINT [PK_t_ongkostransporth] PRIMARY KEY CLUSTERED " & _
    "( " & _
    "[nomer_ongkostransport] Asc " & _
    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
    ") ON [PRIMARY]"
    conn.Execute "CREATE view [dbo].[vw_rekapkuli] as " & _
    "select h.nomer_konsitambah as nomer,fg_bayarkuli,tanggal_konsitambah as tanggal,'Muat Truk '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']','') as Nama ,kuli,sum(qty) as qty,sum(berat) as berat from t_konsitambahh h inner join t_konsitambahd d on h.nomer_konsitambah=d.nomer_konsitambah group by h.nomer_konsitambah,tanggal_konsitambah,kuli,no_truk,supir,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_konsiretur,fg_bayarkuli,tanggal_konsiretur,'Bongkar Truk '+ replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']','') as Nama,kuli,sum(qty) as qty,sum(berat) as berat from t_konsireturh h inner join t_konsireturd d on h.nomer_konsiretur=d.nomer_konsiretur group by h.nomer_konsiretur,tanggal_konsiretur,kuli,no_truk,supir,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_returbeli,fg_bayarkuli,tanggal_returbeli,'Muat Truk '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),kuli,sum(qty) as qty,sum(berat) as berat from t_returbelih h inner join t_returbelid d on h.nomer_returbeli=d.nomer_returbeli group by h.nomer_returbeli,tanggal_returbeli,kuli,no_truk,supir,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_returjual,fg_bayarkuli,tanggal_returjual,'Bongkar Truk '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),kuli,sum(qty) as qty,sum(berat) as berat from t_returjualh h inner join t_returjuald d on h.nomer_returjual=d.nomer_returjual group by h.nomer_returjual,tanggal_returjual,kuli,no_truk,supir,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_mutasibahan,fg_bayarkuli,tanggal,'Mutasi Stok ',kuli,sum(qty) as qty,sum(berat) as berat from t_stockmutasibahanh h inner join t_stockmutasibahand d on h.nomer_mutasibahan=d.nomer_mutasibahan group by h.nomer_mutasibahan,tanggal,kuli,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_suratjalan,fg_bayarkuli,tanggal_suratjalan,'Muat Truk '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),kuli,sum(qty) as qty,sum(berat) as berat from t_suratjalanh h inner join t_suratjaland d on h.nomer_suratjalan=d.nomer_suratjalan group by h.nomer_suratjalan,tanggal_suratjalan,kuli,no_truk,supir,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_terimabarang,fg_bayarkuli,tanggal_terimabarang,'Bongkar Truk '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),kuli,sum(qty) as qty,sum(berat) as berat from t_terimabarangh h inner join t_terimabarangd d on h.nomer_terimabarang=d.nomer_terimabarang group by h.nomer_terimabarang,tanggal_terimabarang,kuli,no_truk,supir,fg_bayarkuli " & _
    "Union All " & _
    "select h.nomer_produksi,fg_bayarkuli,tanggal_produksi,'Produksi ',pengawas,sum(qty) as qty,sum(berat) as berat from t_produksih h inner join t_produksi_bahan d on h.nomer_produksi=d.nomer_produksi group by h.nomer_produksi,tanggal_produksi,pengawas,fg_bayarkuli "
    conn.Execute "create view [dbo].[vw_rekaptransport] as " & _
    "select h.nomer_konsitambah as nomer,fg_bayarangkut as bayar,tanggal_konsitambah as tanggal,'Kirim Titip'+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']','') as Nama ,sum(qty) as qty,sum(berat) as berat from t_konsitambahh h inner join t_konsitambahd d on h.nomer_konsitambah=d.nomer_konsitambah group by h.nomer_konsitambah,tanggal_konsitambah,no_truk,supir,fg_bayarangkut " & _
    "Union All " & _
    "select h.nomer_konsiretur,fg_bayarangkut,tanggal_konsiretur,'Retur Titip '+ replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']','') as Nama,sum(qty) as qty,sum(berat) as berat from t_konsireturh h inner join t_konsireturd d on h.nomer_konsiretur=d.nomer_konsiretur group by h.nomer_konsiretur,tanggal_konsiretur,no_truk,supir,fg_bayarangkut " & _
    "Union All " & _
    "select h.nomer_returbeli,fg_bayarangkut,tanggal_returbeli,'Kirim Retur '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),sum(qty) as qty,sum(berat) as berat from t_returbelih h inner join t_returbelid d on h.nomer_returbeli=d.nomer_returbeli where fg_angkut=1 group by h.nomer_returbeli,tanggal_returbeli,no_truk,supir,fg_bayarangkut " & _
    "Union All " & _
    "select h.nomer_returjual,fg_bayarangkut,tanggal_returjual,'Ambil Retur '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),sum(qty) as qty,sum(berat) as berat from t_returjualh h inner join t_returjuald d on h.nomer_returjual=d.nomer_returjual where fg_angkut=1 group by h.nomer_returjual,tanggal_returjual,no_truk,supir,fg_bayarangkut " & _
    "Union All " & _
    "select h.nomer_suratjalan,fg_bayarangkut,tanggal_suratjalan,'Kirim Barang '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),sum(qty) as qty,sum(berat) as berat from t_suratjalanh h inner join t_suratjaland d on h.nomer_suratjalan=d.nomer_suratjalan where fg_angkut=1 group by h.nomer_suratjalan,tanggal_suratjalan,no_truk,supir,fg_bayarangkut " & _
    "Union All " & _
    "select h.nomer_terimabarang,fg_bayarangkut,tanggal_terimabarang,'Ambil Barang '+replace(replace(no_truk,'[',''),']','') +' '+ replace(replace(supir,'[',''),']',''),sum(qty) as qty,sum(berat) as berat from t_terimabarangh h inner join t_terimabarangd d on h.nomer_terimabarang=d.nomer_terimabarang where fg_angkut=1 group by h.nomer_terimabarang,tanggal_terimabarang,no_truk,supir,fg_bayarangkut"
    conn.Close
    Exit Sub
err:
    
End Sub
Public Sub addtransport()
On Error Resume Next
    conn.Open strcon
    conn.Execute "CREATE TABLE [dbo].[ms_ekspedisi]( " & _
    "[kode_ekspedisi] [varchar](10) NOT NULL, " & _
    "[nama_ekspedisi] [varchar](50) NULL, " & _
    "[alamat] [varchar](150) NULL, " & _
    "[telp] [varchar](50) NULL, " & _
    "[keterangan] [varchar](250) NULL, " & _
    "CONSTRAINT [PK_ms_ekspedisi] PRIMARY KEY CLUSTERED " & _
    "( " & _
    "[kode_ekspedisi] Asc " & _
    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
    ") ON [PRIMARY]"
    conn.Close
End Sub
Public Sub alterjual()
On Error Resume Next
    conn.Open strcon
    conn.Execute "alter table t_jualh add [nomer_invoice] [varchar](50) NULL CONSTRAINT [DF_t_jualH_nomer_invoice]  DEFAULT ('')"
    conn.Execute "alter table t_jualh add [tax] [int] NULL CONSTRAINT [DF_t_jualH_tax]  DEFAULT ((0))"
    conn.Execute "alter table t_jualh add [harga_final] [numeric](12, 2) NULL CONSTRAINT [DF__t_juald__hargafinal]  DEFAULT ((0))"
    conn.Close
    
End Sub
Public Sub altermscontact()
On Error Resume Next
    conn.Open strcon
    conn.Execute "alter table ms_contact alter column alamat varchar(150)"
    conn.Execute "alter table ms_contact alter column telp varchar(150)"
    conn.Execute "alter table ms_contact alter column hp varchar(150)"
    conn.Execute "alter table ms_contact alter column fax varchar(150)"
    conn.Execute "alter table ms_contact alter column email varchar(150)"
    conn.Execute "alter table ms_contact alter column contact_person varchar(70)"
End Sub
Public Sub alterbahan()
On Error Resume Next
    conn.Open strcon
    conn.Execute "alter table ms_bahan add keterangan [text] NULL CONSTRAINT [DF_ms_bahan_keterangan]  DEFAULT ('')"
    conn.Execute "alter table rpt_ms_bahan add keterangan [text] NULL CONSTRAINT [DF_rpt_ms_bahan_keterangan]  DEFAULT ('')"
    conn.Execute "update ms_bahan set foto=''"
    conn.Execute "update rpt_ms_bahan set foto=''"
    conn.Close
    
End Sub

Public Sub addserialsj()
On Error Resume Next
    conn.Open strcon
    conn.Execute "alter table t_suratjalan_timbang add nomer_serial varchar(20)  NULL CONSTRAINT [DF_t_suratjalanD_nomer_serial]  DEFAULT ('')"
    conn.Execute "alter table t_suratjalan_timbang add hpp numeric(18,4) NULL CONSTRAINT [DF_t_terimabarangD_hpp]  DEFAULT ('0')"
    
    conn.Close
    Exit Sub
err:
    
End Sub

Public Sub addlimitkreditkaryawan()
On Error GoTo err
    conn.Open strcon
    rs.Open "select top 1 limitkredit from ms_karyawan", conn
    rs.Close
    conn.Close
    Exit Sub
err:
    conn.Execute "alter table ms_karyawan add limitkredit numeric(15,0)"
    conn.Execute "update ms_karyawan set limitkredit=0"
    If conn.State Then conn.Close
End Sub

Public Sub changewebsite()
On Error GoTo err
conn.Open strcon
conn.Execute "alter table rpt_ms_customer alter column website varchar(150)"
conn.Execute "alter table ms_customer alter column website varchar(150)"
conn.Close
Exit Sub
err:
If conn.State Then conn.Close
End Sub
Public Sub altersupplier()
On Error GoTo err
    conn.Open
    conn.Execute "alter table ms_supplier add  foto image"
    conn.Execute "alter table rpt_ms_supplier add  foto image"
    conn.Execute "update ms_supplier set foto=''"
    conn.Execute "update rpt_ms_supplier set foto=''"
    conn.Close
Exit Sub
err:
If conn.State Then conn.Close
End Sub
Public Sub altercustomer()
On Error GoTo err
    conn.Open
    conn.Execute "alter table ms_customer add  atasnama varchar(255)"
    conn.Execute "alter table rpt_ms_customer add atasnama varchar(255)"
    conn.Execute "update ms_customer set atasnama=''"
    conn.Execute "update rpt_ms_supplier set atasnama=''"
    conn.Close
Exit Sub
err:
If conn.State Then conn.Close
End Sub
Public Sub alterkaryawan()
On Error GoTo err
    conn.Open
    conn.Execute "alter table ms_karyawan add foto image,limitkredit numeric(18,0)"
    conn.Execute "alter table rpt_ms_karyawan add foto image,limitkredit numeric(18,0)"
    conn.Execute "update ms_karyawan set foto='',limitkredit=0"
    conn.Execute "update rpt_ms_karyawan set foto='',limitkredit=0"
    conn.Close
Exit Sub
err:
If conn.State Then conn.Close
End Sub
Public Sub altercontact()
On Error GoTo err
    conn.Open
    conn.Execute "alter table ms_contact add  foto image"
    conn.Execute "alter table rpt_ms_contact add  foto image"
    conn.Execute "update ms_contact set foto=''"
    conn.Execute "update rpt_ms_contact set foto=''"
    conn.Close
Exit Sub
err:
If conn.State Then conn.Close
End Sub
