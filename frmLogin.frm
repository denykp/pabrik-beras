VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Login"
   ClientHeight    =   2850
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4425
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2850
   ScaleWidth      =   4425
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox txtDBName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1485
      Style           =   1  'Simple Combo
      TabIndex        =   0
      Top             =   135
      Width           =   1905
   End
   Begin VB.ComboBox txtServerName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1485
      Style           =   1  'Simple Combo
      TabIndex        =   1
      Top             =   540
      Width           =   1905
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2070
      TabIndex        =   5
      Top             =   2160
      Width           =   1500
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "&Login"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   300
      TabIndex        =   4
      Top             =   2160
      Width           =   1500
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1500
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   1305
      Width           =   1905
   End
   Begin VB.TextBox txtUserName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   2
      Top             =   900
      Width           =   1905
   End
   Begin VB.Label Label3 
      Caption         =   "DB Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   10
      Top             =   180
      Width           =   1140
   End
   Begin VB.Label Label5 
      Height          =   195
      Left            =   45
      TabIndex        =   9
      Top             =   2565
      Width           =   150
   End
   Begin VB.Label Label4 
      Caption         =   "Server"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   8
      Top             =   585
      Width           =   1140
   End
   Begin VB.Label Label2 
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   7
      Top             =   1350
      Width           =   1005
   End
   Begin VB.Label Label1 
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   6
      Top             =   945
      Width           =   1005
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public mSQLServer As New SQLDMO.SQLServer
'Public mApplication As New SQLDMO.Application
Private OpenForm_Fg As Boolean

Private Sub isicombo()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject

    Call SetConnection(txtServerName.text, txtUserName.text, txtPassword.text, txtDBName.text)
    
    Call DropConnection

    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
End Sub

Private Sub cmbGudang_GotFocus()
    isicombo
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLogin_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    Call SetConnection(txtServerName.text, txtUserName.text, txtPassword.text, txtDBName.text)
    User = txtUserName.text
    
    rs.Open "select * from (user_groupmenu g inner join user_group u on g.[nama_group]=u.[nama_group]) where userid='" & User & "'", conn
    While Not rs.EOF
        frmMain.Controls(rs("namamenu"))(rs("idx")).Visible = CBool(rs("value"))
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select nama_group from user_group where [userid]='" & User & "'"
    If Not rs.EOF Then usergroup = rs(0)
    rs.Close
    
    'load hak akses user
    rs.Open "select * from var_usergroup where [group] = '" & usergroup & "'", conn
    If Not rs.EOF Then
        right_export = rs!Export
        right_deletemaster = rs!delete_master
    End If
    rs.Close
    load_query usergroup
    load_IDperusahaan
    load_accvariables
    load_setting
    Call DropConnection
    frmMain.Show
    OpenForm_Fg = True
    Unload Me
    Exit Sub
err:
    MsgBox err.Description
ex:
    If rs.State Then rs.Close
    DropConnection
    'txtUserName.SetFocus
End Sub

Private Sub Form_Activate()
    txtUserName.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    
    
   Dim i As Integer
'   Dim ServerList As SQLDMO.NameList
'
'   Set ServerList = mApplication.ListAvailableSQLServers
'
'   For i = 1 To ServerList.Count
'       txtServerName.AddItem ServerList.Item(i)
'       txtServerNameT.AddItem ServerList.Item(i)
'   Next i
'    If modulename = "utama" Then
'        Label3.Visible = True
'        cmbGudang.Visible = True
'    End If
    OpenForm_Fg = False
    getConnString "", ""
    
    txtServerName.text = servname
    txtDBName.text = dbname1
'    isicombo
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If OpenForm_Fg = False Then Call DropConnection
End Sub

Private Sub Label5_Click()
    txtServerName.Visible = True
    Label4.Visible = True
End Sub

Private Sub txtPassword_GotFocus()
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.text)
End Sub

Private Sub txtUserName_GotFocus()
    txtUserName.SelStart = 0
    txtUserName.SelLength = Len(txtUserName.text)
End Sub
