VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmTransProduksi 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Produksi"
   ClientHeight    =   9825
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15390
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9825
   ScaleMode       =   0  'User
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5415
      Picture         =   "frmTransProduksi.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   162
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      Picture         =   "frmTransProduksi.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   161
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.Frame frDetail 
      Height          =   6630
      Index           =   0
      Left            =   135
      TabIndex        =   62
      Top             =   2880
      Width           =   8520
      Begin MSComctlLib.TreeView tvwMain 
         Height          =   3030
         Left            =   4950
         TabIndex        =   125
         Top             =   1125
         Visible         =   0   'False
         Width           =   3210
         _ExtentX        =   5662
         _ExtentY        =   5345
         _Version        =   393217
         HideSelection   =   0   'False
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2295
         Left            =   90
         TabIndex        =   106
         Top             =   2160
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   4048
         _Version        =   393216
         Cols            =   10
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxRekapBahan 
         Height          =   1890
         Left            =   90
         TabIndex        =   111
         Top             =   4545
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   3334
         _Version        =   393216
         Cols            =   6
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame2 
         Caption         =   "Bahan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2010
         Left            =   135
         TabIndex        =   93
         Top             =   120
         Width           =   8295
         Begin VB.TextBox txtNamaSupplier 
            Height          =   345
            Left            =   1485
            TabIndex        =   11
            Top             =   1635
            Width           =   2670
         End
         Begin VB.CommandButton cmdopentreeview 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7650
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   117
            Top             =   675
            Width           =   330
         End
         Begin VB.ComboBox cmbGudang 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   4815
            Style           =   2  'Dropdown List
            TabIndex        =   116
            Top             =   630
            Width           =   2805
         End
         Begin VB.TextBox txtNoSerial 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   8
            Top             =   585
            Width           =   1500
         End
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   10
            Top             =   1305
            Width           =   1140
         End
         Begin VB.PictureBox Picture2 
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   3015
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   96
            Top             =   225
            Width           =   465
            Begin VB.CommandButton cmdSearchBrg 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmTransProduksi.frx":0A64
               TabIndex        =   97
               Top             =   15
               Width           =   420
            End
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   4290
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   95
            Top             =   1470
            Width           =   3795
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   14
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   13
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   12
               Top             =   84
               Width           =   1350
            End
         End
         Begin VB.TextBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   7
            Top             =   225
            Width           =   1500
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   9
            Top             =   945
            Width           =   1140
         End
         Begin VB.CommandButton cmdSearchSerial 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3015
            Picture         =   "frmTransProduksi.frx":0B66
            TabIndex        =   94
            Top             =   600
            Width           =   420
         End
         Begin VB.Label Label42 
            AutoSize        =   -1  'True
            Caption         =   "Supplier"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   160
            Top             =   1650
            Width           =   705
         End
         Begin VB.Label lblStockBerat 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2655
            TabIndex        =   154
            Top             =   990
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label lblStockKemasan 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3330
            TabIndex        =   153
            Top             =   990
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Gudang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3510
            TabIndex        =   118
            Top             =   645
            Width           =   1320
         End
         Begin VB.Label Label2 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Nomer Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   105
            Top             =   615
            Width           =   1275
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2730
            TabIndex        =   104
            Top             =   1320
            Width           =   600
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Berat / Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   103
            Top             =   1305
            Width           =   1245
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   102
            Top             =   270
            Width           =   1050
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3510
            TabIndex        =   101
            Top             =   255
            Width           =   3210
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2745
            TabIndex        =   100
            Top             =   2025
            Visible         =   0   'False
            Width           =   1050
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Jml Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   99
            Top             =   2115
            Visible         =   0   'False
            Width           =   1125
         End
         Begin VB.Label Label15 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   98
            Top             =   975
            Width           =   1245
         End
      End
   End
   Begin VB.TextBox txtHasilProses 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1665
      MaxLength       =   80
      TabIndex        =   158
      Top             =   2070
      Width           =   6420
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   480
      Left            =   8235
      TabIndex        =   157
      Top             =   1845
      Width           =   600
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   330
      Left            =   9405
      TabIndex        =   156
      Top             =   9180
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   285
      Left            =   10935
      TabIndex        =   155
      Top             =   9225
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.ListBox listKuli 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   9090
      Style           =   1  'Checkbox
      TabIndex        =   151
      Top             =   1260
      Width           =   3705
   End
   Begin VB.ComboBox cmbProses 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1665
      TabIndex        =   146
      Top             =   1650
      Width           =   2895
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13545
      Picture         =   "frmTransProduksi.frx":0C68
      TabIndex        =   90
      Top             =   7155
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13545
      Picture         =   "frmTransProduksi.frx":0D6A
      TabIndex        =   89
      Top             =   8550
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13545
      Picture         =   "frmTransProduksi.frx":0E6C
      TabIndex        =   88
      Top             =   6480
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   13545
      Picture         =   "frmTransProduksi.frx":0F6E
      TabIndex        =   87
      Top             =   7875
      Width           =   1230
   End
   Begin VB.ListBox listMesin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   4230
      TabIndex        =   65
      Top             =   435
      Width           =   3075
   End
   Begin VB.ListBox listPengawas 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   11655
      TabIndex        =   33
      Top             =   405
      Width           =   3075
   End
   Begin VB.CommandButton cmdSearchPengawas 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10935
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   28
      Top             =   450
      Width           =   420
   End
   Begin VB.TextBox txtPengawas 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   9090
      TabIndex        =   1
      Top             =   450
      Width           =   1755
   End
   Begin VB.TextBox txtKodeMesin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1665
      TabIndex        =   0
      Top             =   495
      Width           =   2025
   End
   Begin VB.CommandButton cmdSearchCustomer 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3780
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   24
      Top             =   480
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3795
      Picture         =   "frmTransProduksi.frx":1070
      TabIndex        =   17
      Top             =   90
      Width           =   420
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1665
      TabIndex        =   18
      Top             =   1260
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   122486787
      CurrentDate     =   38927
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1665
      Left            =   9090
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   112
      Top             =   2925
      Width           =   5670
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   6120
      TabIndex        =   147
      Top             =   1275
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   122552323
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker3 
      Height          =   330
      Left            =   6120
      TabIndex        =   148
      Top             =   1680
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   122552323
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      Height          =   6630
      Index           =   4
      Left            =   135
      TabIndex        =   31
      Top             =   2880
      Visible         =   0   'False
      Width           =   8250
      Begin VB.Frame Frame6 
         Height          =   1950
         Left            =   90
         TabIndex        =   32
         Top             =   180
         Width           =   7575
         Begin VB.TextBox txtKeteranganBiaya 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   720
            Left            =   1620
            TabIndex        =   3
            Top             =   630
            Width           =   5490
         End
         Begin VB.PictureBox Picture3 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   3420
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   51
            Top             =   1350
            Width           =   3795
            Begin VB.CommandButton cmdAddBiaya 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   5
               Top             =   84
               Width           =   1350
            End
            Begin VB.CommandButton cmdResetBiaya 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   6
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdHapusBiaya 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   15
               Top             =   84
               Width           =   1050
            End
         End
         Begin VB.TextBox txtJumlahBiaya 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1620
            TabIndex        =   4
            Top             =   1440
            Width           =   1620
         End
         Begin VB.CommandButton cmdSearchBiaya 
            Appearance      =   0  'Flat
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   -6705
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   34
            Top             =   -2970
            Width           =   420
         End
         Begin VB.TextBox txtKodeBiaya 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1620
            TabIndex        =   2
            Top             =   180
            Width           =   1620
         End
         Begin VB.Label Label21 
            Caption         =   "Keterangan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   67
            Top             =   690
            Width           =   960
         End
         Begin VB.Label lblNamaBiaya 
            AutoSize        =   -1  'True
            Caption         =   "Nama biaya"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3780
            TabIndex        =   54
            Top             =   270
            Width           =   1005
         End
         Begin VB.Label Label23 
            Caption         =   "Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   44
            Top             =   1455
            Width           =   960
         End
         Begin VB.Label Label20 
            Caption         =   "Kode biaya"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   43
            Top             =   240
            Width           =   1365
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxBiaya 
         Height          =   3285
         Left            =   90
         TabIndex        =   16
         Top             =   2250
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   5794
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frDetail 
      Height          =   6450
      Index           =   1
      Left            =   135
      TabIndex        =   63
      Top             =   2880
      Visible         =   0   'False
      Width           =   8205
      Begin MSFlexGridLib.MSFlexGrid flxKomposisi 
         Height          =   5175
         Left            =   225
         TabIndex        =   64
         Top             =   270
         Width           =   7260
         _ExtentX        =   12806
         _ExtentY        =   9128
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frDetail 
      Height          =   6540
      Index           =   2
      Left            =   135
      TabIndex        =   68
      Top             =   2880
      Visible         =   0   'False
      Width           =   8340
      Begin MSComctlLib.TreeView tvwHasil 
         Height          =   3030
         Left            =   1485
         TabIndex        =   121
         Top             =   1515
         Visible         =   0   'False
         Width           =   3210
         _ExtentX        =   5662
         _ExtentY        =   5345
         _Version        =   393217
         HideSelection   =   0   'False
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame4 
         Height          =   2145
         Left            =   135
         TabIndex        =   69
         Top             =   120
         Width           =   8115
         Begin VB.CommandButton cmdopentreeHasil 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4455
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   119
            Top             =   1080
            Width           =   330
         End
         Begin VB.ComboBox cmbGudangHasil 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1350
            Style           =   2  'Dropdown List
            TabIndex        =   38
            Top             =   1035
            Width           =   2805
         End
         Begin VB.TextBox txtQtyHasil 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1350
            TabIndex        =   36
            Top             =   585
            Width           =   990
         End
         Begin VB.TextBox txtHarga 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   6300
            TabIndex        =   39
            Top             =   585
            Visible         =   0   'False
            Width           =   1620
         End
         Begin VB.TextBox txtKodeHasil 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1350
            TabIndex        =   35
            Top             =   180
            Width           =   1620
         End
         Begin VB.CommandButton cmdSearchKomposisi 
            Appearance      =   0  'Flat
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3015
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   71
            Top             =   180
            Width           =   420
         End
         Begin VB.TextBox txtJumlahHasil 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3510
            TabIndex        =   37
            Top             =   585
            Width           =   990
         End
         Begin VB.PictureBox Picture4 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   4380
            ScaleHeight     =   465
            ScaleWidth      =   3705
            TabIndex        =   70
            Top             =   1500
            Width           =   3705
            Begin VB.CommandButton cmdHapusHasil 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2670
               TabIndex        =   42
               Top             =   84
               Width           =   975
            End
            Begin VB.CommandButton cmdResetHasil 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1620
               TabIndex        =   41
               Top             =   84
               Width           =   990
            End
            Begin VB.CommandButton cmdAddHasil 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   270
               TabIndex        =   40
               Top             =   84
               Width           =   1290
            End
         End
         Begin VB.Label Label27 
            Caption         =   "Gudang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   120
            Top             =   1095
            Width           =   870
         End
         Begin VB.Label Label9 
            Caption         =   "Berat"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2745
            TabIndex        =   86
            Top             =   630
            Width           =   690
         End
         Begin VB.Label Label8 
            Caption         =   "HPP"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4815
            TabIndex        =   85
            Top             =   630
            Visible         =   0   'False
            Width           =   960
         End
         Begin VB.Label Label28 
            Caption         =   "Kode hasil"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   74
            Top             =   240
            Width           =   1140
         End
         Begin VB.Label Label26 
            Caption         =   "Qty"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   73
            Top             =   645
            Width           =   960
         End
         Begin VB.Label lblNamaHasil 
            AutoSize        =   -1  'True
            Caption         =   "Nama bahan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3780
            TabIndex        =   72
            Top             =   240
            Width           =   1080
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxHasil 
         Height          =   3510
         Left            =   180
         TabIndex        =   83
         Top             =   2295
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   6191
         _Version        =   393216
         Cols            =   8
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frDetail 
      Height          =   6630
      Index           =   5
      Left            =   135
      TabIndex        =   122
      Top             =   2880
      Visible         =   0   'False
      Width           =   8520
      Begin MSComctlLib.TreeView tvwKemasan 
         Height          =   330
         Left            =   4905
         TabIndex        =   141
         Top             =   720
         Visible         =   0   'False
         Width           =   3210
         _ExtentX        =   5662
         _ExtentY        =   582
         _Version        =   393217
         HideSelection   =   0   'False
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame1 
         Caption         =   "Kemasan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1770
         Left            =   90
         TabIndex        =   126
         Top             =   180
         Width           =   8295
         Begin VB.CommandButton cmdUpdateKemasan 
            BackColor       =   &H8000000C&
            Caption         =   "&Update"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6255
            TabIndex        =   167
            Top             =   720
            Width           =   1350
         End
         Begin VB.ComboBox cmbHasilKemasan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1485
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   53
            Top             =   225
            Width           =   2805
         End
         Begin VB.TextBox txtBeratKemasan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   58
            Top             =   1815
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.CommandButton cmdopentreeviewKemasan 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7650
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   131
            Top             =   270
            Width           =   330
         End
         Begin VB.ComboBox cmbGudangKemasan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   4815
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Top             =   225
            Width           =   2805
         End
         Begin VB.TextBox txtNomerSerialKemasan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   56
            Top             =   990
            Width           =   1500
         End
         Begin VB.PictureBox Picture6 
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   3015
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   129
            Top             =   585
            Width           =   465
            Begin VB.CommandButton cmdSearchKemasan 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmTransProduksi.frx":1172
               TabIndex        =   130
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.PictureBox Picture7 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   3375
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   128
            Top             =   1260
            Width           =   3795
            Begin VB.CommandButton cmdHapusKemasan 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   61
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdResetKemasan 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   60
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdAddKemasan 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   59
               Top             =   84
               Width           =   1350
            End
         End
         Begin VB.TextBox txtKodeKemasan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   55
            Top             =   630
            Width           =   1500
         End
         Begin VB.TextBox txtQtyKemasan 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   57
            Top             =   1350
            Width           =   1140
         End
         Begin VB.CommandButton cmdSearchSerialKemasan 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3015
            Picture         =   "frmTransProduksi.frx":1274
            TabIndex        =   127
            Top             =   990
            Width           =   420
         End
         Begin VB.Label Label29 
            AutoSize        =   -1  'True
            Caption         =   "Kode Hasil"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   142
            Top             =   270
            Width           =   885
         End
         Begin VB.Label lblGudangKemasan 
            Caption         =   "Gudang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3510
            TabIndex        =   140
            Top             =   1035
            Width           =   1320
         End
         Begin VB.Label Label30 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Nomer Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   139
            Top             =   1035
            Width           =   1275
         End
         Begin VB.Label Label31 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2730
            TabIndex        =   138
            Top             =   1875
            Width           =   600
         End
         Begin VB.Label Label32 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Berat / Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   137
            Top             =   1845
            Width           =   1245
         End
         Begin VB.Label Label33 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   136
            Top             =   630
            Width           =   1050
         End
         Begin VB.Label lblNamaKemasan 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3510
            TabIndex        =   135
            Top             =   675
            Width           =   3210
         End
         Begin VB.Label Label35 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2745
            TabIndex        =   134
            Top             =   2025
            Visible         =   0   'False
            Width           =   1050
         End
         Begin VB.Label Label36 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Jml Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   133
            Top             =   2115
            Visible         =   0   'False
            Width           =   1125
         End
         Begin VB.Label Label37 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   132
            Top             =   1380
            Width           =   1245
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid6 
         Height          =   2295
         Left            =   90
         TabIndex        =   123
         Top             =   2160
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   4048
         _Version        =   393216
         Cols            =   9
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxRekapKemasan 
         Height          =   1890
         Left            =   90
         TabIndex        =   124
         Top             =   4545
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   3334
         _Version        =   393216
         Cols            =   6
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frDetail 
      Height          =   6360
      Index           =   3
      Left            =   135
      TabIndex        =   75
      Top             =   2880
      Visible         =   0   'False
      Width           =   8520
      Begin VB.Frame frKomposisiHasil 
         Height          =   2130
         Left            =   225
         TabIndex        =   76
         Top             =   180
         Width           =   6225
         Begin VB.ComboBox cmbHasil 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1620
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   45
            Top             =   180
            Width           =   2805
         End
         Begin VB.PictureBox Picture5 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   135
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   78
            Top             =   1530
            Width           =   3795
            Begin VB.CommandButton cmdAddKomposisiHasil 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   48
               Top             =   84
               Width           =   1350
            End
            Begin VB.CommandButton cmdResetKomposisiHasil 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   49
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdHapusKomposisiHasil 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   50
               Top             =   84
               Width           =   1050
            End
         End
         Begin VB.TextBox txtpct 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1620
            TabIndex        =   47
            Top             =   1080
            Width           =   945
         End
         Begin VB.CommandButton cmdSearchKomposisiHasil 
            Appearance      =   0  'Flat
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3285
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   77
            Top             =   630
            Width           =   420
         End
         Begin VB.TextBox txtKodeKomposisiHasil 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1620
            TabIndex        =   46
            Top             =   630
            Width           =   1620
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Kode Hasil"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   84
            Top             =   225
            Width           =   885
         End
         Begin VB.Label lblNamakomposisiHasil 
            AutoSize        =   -1  'True
            Caption         =   "Nama bahan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3780
            TabIndex        =   81
            Top             =   720
            Width           =   1080
         End
         Begin VB.Label Label5 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   80
            Top             =   1095
            Width           =   960
         End
         Begin VB.Label Label4 
            Caption         =   "Kode bahan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   135
            TabIndex        =   79
            Top             =   690
            Width           =   1140
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxKomposisiHasil 
         Height          =   3555
         Left            =   225
         TabIndex        =   82
         Top             =   2385
         Width           =   8160
         _ExtentX        =   14393
         _ExtentY        =   6271
         _Version        =   393216
         Cols            =   7
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frDetail 
      Height          =   6405
      Index           =   6
      Left            =   135
      TabIndex        =   143
      Top             =   2880
      Visible         =   0   'False
      Width           =   8205
      Begin MSFlexGridLib.MSFlexGrid flxRekapKomposisi 
         Height          =   5175
         Left            =   225
         TabIndex        =   144
         Top             =   270
         Width           =   7260
         _ExtentX        =   12806
         _ExtentY        =   9128
         _Version        =   393216
         Cols            =   6
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   7125
      Left            =   45
      TabIndex        =   66
      Top             =   2520
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   12568
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   7
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Produksi"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Komposisi"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Hasil"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Komposisi Hasil"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Biaya"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Kemasan"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Persentase"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblpctLebih 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11430
      TabIndex        =   166
      Top             =   6570
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label Label45 
      Alignment       =   1  'Right Justify
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12285
      TabIndex        =   165
      Top             =   6570
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Label Label44 
      Caption         =   "Persentase Lebih"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9045
      TabIndex        =   164
      Top             =   6570
      Visible         =   0   'False
      Width           =   2220
   End
   Begin VB.Label Label43 
      Caption         =   "Persentase Susut"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9045
      TabIndex        =   163
      Top             =   6120
      Width           =   2220
   End
   Begin VB.Label Label41 
      Caption         =   "Hasil Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   159
      Top             =   2115
      Width           =   1350
   End
   Begin VB.Label Label40 
      Caption         =   "Kuli"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7965
      TabIndex        =   152
      Top             =   1305
      Width           =   960
   End
   Begin VB.Label Label39 
      Caption         =   "Mulai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4635
      TabIndex        =   150
      Top             =   1320
      Width           =   1050
   End
   Begin VB.Label Label38 
      Caption         =   "Selesai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4635
      TabIndex        =   149
      Top             =   1725
      Width           =   1050
   End
   Begin VB.Label Label34 
      Caption         =   "Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   145
      Top             =   1650
      Width           =   960
   End
   Begin VB.Label Label25 
      Alignment       =   1  'Right Justify
      Caption         =   "%"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12285
      TabIndex        =   115
      Top             =   6120
      Width           =   285
   End
   Begin VB.Label lblPctSusut 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11430
      TabIndex        =   114
      Top             =   6120
      Width           =   780
   End
   Begin VB.Label Label16 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9090
      TabIndex        =   113
      Top             =   2520
      Width           =   1320
   End
   Begin VB.Label Label10 
      Caption         =   "Total Hasil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9045
      TabIndex        =   110
      Top             =   5220
      Width           =   1500
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10350
      TabIndex        =   109
      Top             =   5220
      Width           =   2175
   End
   Begin VB.Label Label24 
      AutoSize        =   -1  'True
      Caption         =   "Total Biaya"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   9045
      TabIndex        =   108
      Top             =   7290
      Width           =   1365
   End
   Begin VB.Label lblTotalBiaya 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10710
      TabIndex        =   107
      Top             =   7290
      Width           =   1815
   End
   Begin VB.Label lblBerat 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10935
      TabIndex        =   92
      Top             =   4770
      Width           =   1590
   End
   Begin VB.Label Label6 
      Caption         =   "Total Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9045
      TabIndex        =   91
      Top             =   4770
      Width           =   1860
   End
   Begin VB.Label lblNamaPengawas 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9090
      TabIndex        =   30
      Top             =   855
      Width           =   2265
   End
   Begin VB.Label Label11 
      Caption         =   "Pengawas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7965
      TabIndex        =   29
      Top             =   495
      Width           =   960
   End
   Begin VB.Label lblNamaMesin 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1695
      TabIndex        =   27
      Top             =   900
      Width           =   3120
   End
   Begin VB.Label Label19 
      Caption         =   "Mesin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   26
      Top             =   540
      Width           =   960
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   25
      Top             =   930
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblSusut 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10350
      TabIndex        =   23
      Top             =   5670
      Width           =   2175
   End
   Begin VB.Label Label17 
      Caption         =   "Susut"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9045
      TabIndex        =   22
      Top             =   5670
      Width           =   1320
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   21
      Top             =   135
      Width           =   1320
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   20
      Top             =   1305
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1665
      TabIndex        =   19
      Top             =   60
      Width           =   2085
   End
End
Attribute VB_Name = "frmTransProduksi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mode As Byte
Dim total As Double
Dim totalberat As Double
Dim totalBiaya As Double
Dim hasil As Double
Dim totalkomposisi As Double
Dim seltab As Byte
Dim hpp_kemasan As Currency
Public Tipe As Byte

Private Sub cmbGudang_Click()
    cekserial
End Sub

Private Sub cmdAddBiaya_Click()
    add_biaya
End Sub

Private Sub cmdAddHasil_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If cmbGudangHasil.text = "" Then
        MsgBox "Pilih Gudang dulu"
        Exit Sub
    End If
    If txtKodeHasil.text <> "" And lblNamaHasil <> "" Then
        With flxHasil
        Row = .Rows - 1
        .Rows = .Rows + 1
        
        .Row = Row
        currentRow = .Row
        
        .TextMatrix(Row, 1) = txtKodeHasil.text
        .TextMatrix(Row, 2) = lblNamaHasil
        .TextMatrix(Row, 3) = txtQtyHasil
        .TextMatrix(Row, 4) = txtJumlahHasil
        .TextMatrix(Row, 5) = txtHarga
        .TextMatrix(Row, 6) = cmbGudangHasil.text
'        .TextMatrix(row, 7) = txtNamaSupplier.text
        total = total + .TextMatrix(Row, 4)
        .Row = Row
        .Col = 0
        .ColSel = .cols - 1
        If Tipe = 1 Then
            add_komposisihasilauto
        Else
        With flxKomposisiHasil
            For i = 1 To .Rows - 2
            If .TextMatrix(i, 1) = txtKodeHasil Then
                GoTo n
            End If
            Next
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 2, 1) = txtKodeHasil.text
            .TextMatrix(.Rows - 2, 2) = lblNamaHasil
            .TextMatrix(.Rows - 2, 3) = txtKodeHasil.text
            .TextMatrix(.Rows - 2, 4) = lblNamaHasil
            .TextMatrix(.Rows - 2, 6) = 100
            .TextMatrix(.Rows - 2, 5) = Val(txtJumlahHasil) * total / 100
            add_rekapkomposisi .TextMatrix(Row, 3), .TextMatrix(Row, 4), .TextMatrix(Row, 6)
n:
        End With
        End If
        
        
        showtotal
        
        If Row > 8 Then
            .TopRow = Row - 7
        Else
            .TopRow = 1
        End If
        End With

        
        txtKodeHasil.text = ""
        lblNamaHasil = ""
        txtJumlahHasil = "0"
        txtHarga = 0
        txtQtyHasil = 0
'        txtNamaSupplier = ""
        txtKodeHasil.SetFocus
        cmdResetHasil.Enabled = False
        cmdHapusHasil.Enabled = False
    End If
    mode = 1
    
End Sub

Private Sub cmdAddKemasan_Click()

Dim i As Integer
Dim Row As Integer
    Row = 0
    If cmbGudangKemasan.text = "" Then
        MsgBox "Pilih Gudang dulu"
        Exit Sub
    End If
    With flxGrid6
    If txtKodeKemasan.text <> "" And lblNamaKemasan <> "" Then

'        For i = 1 To .Rows - 1
'            If .TextMatrix(i, 1) = txtKdBrg.text Then row = i
'        Next
        If mode = 1 Then
            If Row = 0 Then
                Row = .Rows - 1
                .Rows = .Rows + 1
            End If
        Else
            Row = .Row
            
        End If
        .Row = Row
        currentRow = .Row
        .TextMatrix(Row, 1) = cmbHasilKemasan.text
        .TextMatrix(Row, 2) = txtKodeKemasan.text
        .TextMatrix(Row, 3) = lblNamaKemasan
        .TextMatrix(Row, 4) = txtNomerSerialKemasan
        .TextMatrix(Row, 5) = txtQtyKemasan
        
        .TextMatrix(Row, 6) = txtBeratKemasan
            
        
        conn.Open strcon
        .TextMatrix(Row, 7) = GetHPPKertas2(txtKodeKemasan.text, txtNomerSerialKemasan, Trim(Right(cmbGudangKemasan.text, 10)), conn)
        .TextMatrix(Row, 8) = cmbGudangKemasan.text

        conn.Close
        .Row = Row
        .Col = 0
        .ColSel = .cols - 1
        
'        total = total + (.TextMatrix(row, 8))
        showtotal
        '.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            .TopRow = Row - 7
        Else
            .TopRow = 1
        End If
        
        add_rekapkemasan
        
        
        txtKodeKemasan.text = ""
        txtNomerSerialKemasan.text = ""
        txtBeratKemasan.text = "1"
        On Error Resume Next
        txtKodeKemasan.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    End With
    mode = 1

End Sub

Private Sub cmdAddKomposisiHasil_Click()

Dim i As Integer
Dim Row As Integer
    Row = 0
    
    If txtKodeKomposisiHasil.text <> "" And lblNamakomposisiHasil <> "" And cmbHasil.text <> "" Then
        With flxKomposisiHasil
        Row = .Rows - 1
        .Rows = .Rows + 1
        
        .Row = Row
        currentRow = .Row
        
        .TextMatrix(Row, 1) = cmbHasil.text
        .TextMatrix(Row, 3) = txtKodeKomposisiHasil.text
        .TextMatrix(Row, 4) = lblNamakomposisiHasil
        .TextMatrix(Row, 6) = txtpct
        
        .Row = Row
        .Col = 0
        .ColSel = .cols - 1

        'total = total + .TextMatrix(row, 3)

        'lblTotal = Format(total, "#,##0.##")
        
        
        If Row > 8 Then
            .TopRow = Row - 7
        Else
            .TopRow = 1
        End If
        End With

        
        txtKodeKomposisiHasil.text = ""
        lblNamaHasil = ""
        txtJumlahHasil = ""
        txtHarga = 0
        txtKodeKomposisiHasil.SetFocus
        cmdResetHasil.Enabled = False
        cmdHapusHasil.Enabled = False
    End If
    mode = 1
    
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"
    txtNoSerial = ""
    txtNamaSupplier.text = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.Row, 0) = ""
    
    Row = flxGrid.Row
    totalQty = totalQty - (flxGrid.TextMatrix(Row, 5))
    totalberat = totalberat - (flxGrid.TextMatrix(Row, 6))
'    total = total - (flxGrid.TextMatrix(row, 8))
'        lblQty = Format(totalQty, "#,##0.##")
    showtotal
    delete_rekap
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 2) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 2) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If flxGrid.Row > 1 Then flxGrid.Row = flxGrid.Row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.Col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
'    add_komposisihasilauto
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdHapusBiaya_Click()
Dim Row, Col As Integer
    With flxBiaya
        If .Rows <= 2 Then Exit Sub
        .TextMatrix(.Row, 0) = ""
        
        Row = .Row
        totalBiaya = totalBiaya - (.TextMatrix(Row, 4))
        lblTotalBiaya = Format(totalBiaya, "#,##0.##")
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Row > 1 Then .Row = .Row - 1
    
        .Rows = .Rows - 1
        .Col = 0
        .ColSel = .cols - 1
    End With
'    cmdClear_Click
    mode = 1
    cmdResetHasil.Enabled = False
    cmdHapusHasil.Enabled = False
End Sub
Private Sub showtotal()
        lblTotal = Format(total, "#,##0.#0")
        lblSusut = Format(-(totalberat - total), "#,##0.#0")
        lblQty = Format(totalQty, "#,##0.#0")
        lblBerat = Format(totalberat, "#,##0.#0")
        
        If totalberat - total <= 0 Then
            lblPctSusut = 0
            lblpctLebih = Format(-(totalberat - total) / totalberat * 100, "#,##0.#0")
        Else
            lblpctLebih = 0
            lblPctSusut = Format(-(totalberat - total) / totalberat * 100, "#,##0.#0")
        End If
        
        lblTotalBiaya = Format(totalBiaya, "#,##0.00")
End Sub
Private Sub cmdHapusHasil_Click()
Dim Row, Col As Integer
Dim kode As String
    With flxHasil
        
        If .Rows <= 2 Then Exit Sub
        .TextMatrix(.Row, 0) = ""
        kode = .TextMatrix(.Row, 1)
        
        Row = .Row
        total = total - (.TextMatrix(Row, 4))
        showtotal
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Row > 1 Then .Row = .Row - 1
        For i = 1 To .Rows - 2
            If LCase(.TextMatrix(i, 1)) = kode Then GoTo n
        Next
        i = 1
        While i < flxKomposisiHasil.Rows - 1
            If flxKomposisiHasil.TextMatrix(i, 1) = kode Then
                flxKomposisiHasil.Row = i
                cmdHapusKomposisiHasil_Click
'                i = i - 1
            End If
            i = i + 1
        Wend
    
n:
        .Rows = .Rows - 1
        .Col = 0
        .ColSel = .cols - 1
    End With
'    cmdClear_Click
    mode = 1
    cmdResetHasil.Enabled = False
    cmdHapusHasil.Enabled = False
    
End Sub


Private Sub cmdHapusKemasan_Click()
Dim Row, Col As Integer
Dim kode As String
    With flxGrid6
        If flxGrid6.TextMatrix(flxGrid6.Row, 2) = "" Then
            flxGrid6.RemoveItem (flxGrid6.Row)
            Exit Sub
        End If
        subs_rekapkemasan .TextMatrix(.Row, 2), .TextMatrix(.Row, 6)
        If .Rows <= 2 Then Exit Sub
        .TextMatrix(.Row, 0) = ""
        kode = .TextMatrix(.Row, 1)
        
        Row = .Row
        
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        
        If .Row > 1 Then .Row = .Row - 1
        
        .Rows = .Rows - 1
        .Col = 0
        .ColSel = .cols - 1
    End With
'    cmdClear_Click
    mode = 1
    
End Sub

Private Sub cmdHapusKomposisiHasil_Click()
Dim Row, Col As Integer
Dim kode As String
    With flxKomposisiHasil
        subs_rekapkomposisi .TextMatrix(.Row, 3), .TextMatrix(.Row, 6)
        If .Rows <= 2 Then Exit Sub
        .TextMatrix(.Row, 0) = ""
        kode = .TextMatrix(.Row, 1)
        
        Row = .Row
        
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        
        If .Row > 1 Then .Row = .Row - 1
        
        .Rows = .Rows - 1
        .Col = 0
        .ColSel = .cols - 1
    End With
'    cmdClear_Click
    mode = 1
    cmdResetKomposisiHasil.Enabled = False
    cmdHapusKomposisiHasil.Enabled = False
    If Tipe = 1 Then
        add_komposisihasilauto
    End If
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_produksi] from t_produksih where [nomer_produksi]>'" & lblNoTrans & "' and tipe='" & Tipe & "' order by [nomer_produksi]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
On Error GoTo err
Dim i As Integer
Dim Row As Integer
    Row = 0
    If cmbGudang.text = "" Then
        MsgBox "Pilih Gudang dulu"
        Exit Sub
    End If
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then
'        If CDbl(txtBerat) > CDbl(lblStockBerat) Then
'            MsgBox "Jumlah yang anda masukkan melebihi persediaan yang ada"
'            txtBerat.SetFocus
'            txtBerat.SelStart = 0
'            txtBerat.SelLength = Len(txtBerat)
'            Exit Sub
'        End If
'

'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
'        Next
        If mode = 1 Then
            If Row = 0 Then
                Row = flxGrid.Rows - 1
                flxGrid.Rows = flxGrid.Rows + 1
            End If
        Else
            Row = flxGrid.Row
            
        End If
        flxGrid.Row = Row
        currentRow = flxGrid.Row
        'flxGrid.TextMatrix(row, 1) = txtNoPO.text
        flxGrid.TextMatrix(Row, 2) = txtKdBrg.text
        flxGrid.TextMatrix(Row, 3) = LblNamaBarang1
        flxGrid.TextMatrix(Row, 4) = txtNoSerial
        flxGrid.TextMatrix(Row, 5) = txtQty
        If flxGrid.TextMatrix(Row, 6) = "" Then
            flxGrid.TextMatrix(Row, 6) = txtBerat
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(Row, 5))
            totalberat = totalberat - (flxGrid.TextMatrix(Row, 6))
'            total = total - (flxGrid.TextMatrix(row, 8))
            If mode = 1 Then
                flxGrid.TextMatrix(Row, 6) = CDbl(flxGrid.TextMatrix(Row, 6)) + CDbl(txtBerat)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(Row, 6) = CDbl(txtBerat)
            End If
        End If
        conn.Open strcon
        flxGrid.TextMatrix(Row, 7) = GetHPPKertas2(txtKdBrg.text, txtNoSerial, Trim(Right(cmbGudang.text, 10)), conn)
        flxGrid.TextMatrix(Row, 8) = cmbGudang.text
        flxGrid.TextMatrix(Row, 9) = txtNamaSupplier.text
        conn.Close
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(Row, 5)
        totalberat = totalberat + flxGrid.TextMatrix(Row, 6)
'        total = total + (flxGrid.TextMatrix(row, 8))
        showtotal
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        add_komposisi
        If Tipe = 1 Then
            add_komposisihasilauto
        End If
        add_rekap
        
        lblSatuan = ""
        txtKdBrg.text = ""
        txtNoSerial.text = ""
        txtNamaSupplier.text = ""
        txtBerat.text = "1"
        On Error Resume Next
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub add_rekap()
Dim i As Integer
Dim Row As Integer

    With flxRekapBahan
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = txtKdBrg.text Then Row = i
    Next
    If Row = 0 Then
        Row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.Row, 0) = ""
    .Row = Row


    .TextMatrix(Row, 1) = txtKdBrg.text
    .TextMatrix(Row, 2) = LblNamaBarang1
    If .TextMatrix(Row, 3) = "" Then .TextMatrix(Row, 3) = txtQty Else .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) + txtQty
    If .TextMatrix(Row, 4) = "" Then .TextMatrix(Row, 4) = txtBerat Else .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) + txtBerat
    
    End With
    hitung_persen
End Sub
Private Sub add_rekapkomposisi(kode_bahan As String, nama_bahan As String, qty As Double)
Dim i As Integer
Dim Row As Integer

    With flxRekapKomposisi
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = kode_bahan Then Row = i
    Next
    If Row = 0 Then
        Row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.Row, 0) = ""
    .Row = Row

    .TextMatrix(Row, 1) = kode_bahan
    .TextMatrix(Row, 2) = nama_bahan
    If .TextMatrix(Row, 3) = "" Then .TextMatrix(Row, 3) = qty Else .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) + qty
    
    End With
    pct_rekapkomposisi
End Sub
Private Sub pct_rekapkomposisi()
    With flxRekapKomposisi
    For i = 1 To .Rows - 2
        If totalberat > 0 Then
            .TextMatrix(i, 4) = (.TextMatrix(i, 3) / totalberat) * 100
        End If
        If total > 0 Then
            .TextMatrix(i, 5) = (.TextMatrix(i, 3) / total) * 100
        End If
    Next
End With
End Sub
Private Sub delete_rekap()
Dim i As Integer
Dim Row As Integer

    With flxRekapBahan
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = flxGrid.TextMatrix(flxGrid.Row, 2) Then Row = i
    Next
    If Row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(Row, 3)) Then .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) - flxGrid.TextMatrix(flxGrid.Row, 5)
    If IsNumeric(.TextMatrix(Row, 4)) Then .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) - flxGrid.TextMatrix(flxGrid.Row, 6)
    If .TextMatrix(Row, 4) = 0 Or .TextMatrix(Row, 3) = 0 Then
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    

    End With
End Sub
Private Sub add_komposisi()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim Row As Integer
    conn.Open strcon
    rs.Open "select stock_komposisi.*,nama_bahan from stock_komposisi left join ms_bahan on kode_bahanbaku=ms_bahan.kode_bahan where stock_komposisi.kode_bahan='" & txtKdBrg.text & "' and nomer_serial='" & txtNoSerial & "'", conn
    While Not rs.EOF
        Row = 0
        For i = 1 To flxKomposisi.Rows - 1
            If flxKomposisi.TextMatrix(i, 1) = rs!kode_bahanbaku Then Row = i
        Next
        If Row = 0 Then
            Row = flxKomposisi.Rows - 1
            flxKomposisi.Rows = flxKomposisi.Rows + 1
            
'            dbgrid.AddItem rs!kode_bahanbaku & "#" & rs!nama_bahan & "#0#0#0"
        End If

        If flxKomposisi.TextMatrix(Row, 1) = "" Then
            flxKomposisi.TextMatrix(Row, 1) = rs!kode_bahanbaku
            flxKomposisi.TextMatrix(Row, 2) = rs!nama_bahan
            flxKomposisi.TextMatrix(Row, 3) = txtBerat * rs!pct / 100
'            dbgrid.Columns(2).text = flxKomposisi.TextMatrix(row, 3)
        Else
            flxKomposisi.TextMatrix(Row, 3) = flxKomposisi.TextMatrix(Row, 3) + (txtBerat * rs!pct / 100)
'            dbgrid.Columns(2).text = dbgrid.Columns(2).text + (txtBerat * rs!pct / 100)
        End If
        
        
'        dbgrid.Update
        rs.MoveNext
    Wend
    rs.Close
    Dim total1 As Double
    total1 = 0
    totalkomposisi = 0
    For i = 1 To flxKomposisi.Rows - 2
        totalkomposisi = totalkomposisi + flxKomposisi.TextMatrix(i, 3)
'        dbgrid.row = i - 1
'        totalkomposisi = totalkomposisi + dbgrid.Columns(2).text
    Next
    For i = 1 To flxKomposisi.Rows - 2
        flxKomposisi.TextMatrix(i, 4) = Format(flxKomposisi.TextMatrix(i, 3) / totalkomposisi * 100, "#0.00")
    Next
    hitung_persen
    
    showtotal
End Sub
Private Sub add_komposisihasilauto()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim Row As Integer
    If flxHasil.Rows <= 2 Then Exit Sub
    flxRekapKomposisi.Rows = 1
    flxRekapKomposisi.Rows = 2
    With flxKomposisiHasil
    conn.Open strcon
    .Rows = 1
    .Rows = 2
    
    For X = 1 To flxHasil.Rows - 2
    For y = 1 To flxKomposisi.Rows - 2

        Row = 0
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = flxHasil.TextMatrix(X, 1) And .TextMatrix(i, 2) = flxKomposisi.TextMatrix(y, 1) Then Row = i
        Next
        If Row = 0 Then
            Row = .Rows - 1
            .Rows = .Rows + 1
            

        End If

        If .TextMatrix(Row, 1) = "" Then
            .TextMatrix(Row, 1) = flxHasil.TextMatrix(X, 1)
            .TextMatrix(Row, 3) = flxKomposisi.TextMatrix(y, 1)
            .TextMatrix(Row, 4) = flxKomposisi.TextMatrix(y, 2)
            .TextMatrix(Row, 6) = flxKomposisi.TextMatrix(y, 4) 'flxHasil.TextMatrix(X, 4) * flxKomposisi.TextMatrix(y, 4) / 100
'            .TextMatrix(row, 5) = flxKomposisi.TextMatrix(y, 3)
            .TextMatrix(Row, 5) = flxKomposisi.TextMatrix(y, 4) * total / 100
        End If
        add_rekapkomposisi .TextMatrix(Row, 3), .TextMatrix(Row, 4), .TextMatrix(Row, 6)
    Next y
    Next X
    End With
End Sub

Private Sub subs_komposisi(Row As Integer)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.Open strcon
    rs.Open "select * from stock_komposisi where kode_bahan='" & flxGrid.TextMatrix(Row, 2) & "' and nomer_serial='" & flxGrid.TextMatrix(Row, 4) & "'", conn
    While Not rs.EOF
        Row = 0
        With flxKomposisi
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = rs!kode_bahanbaku Then Row = i
        Next
        If Row > 0 Then
            .TextMatrix(Row, 3) = .TextMatrix(Row, 3) - (flxGrid.TextMatrix(Row, 6) * rs!pct / 100)
        End If
        If .TextMatrix(Row, 3) = 0 Then
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        
        End If
        End With
        rs.MoveNext
    Wend
    rs.Close
    
    showtotal
    
End Sub
Private Sub add_rekapkemasan()
Dim i As Integer
Dim Row As Integer

    With flxRekapKemasan
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = txtKodeKemasan.text Then Row = i
    Next
    If Row = 0 Then
        Row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.Row, 0) = ""
    .Row = Row
    .TextMatrix(Row, 1) = txtKodeKemasan.text
    .TextMatrix(Row, 2) = lblNamaKemasan
    If .TextMatrix(Row, 3) = "" Then .TextMatrix(Row, 3) = txtQtyKemasan Else .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) + txtQtyKemasan
    If .TextMatrix(Row, 4) = "" Then .TextMatrix(Row, 4) = txtBeratKemasan Else .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) + txtBeratKemasan
    End With
End Sub
Private Sub subs_rekapkomposisi(kode_bahan As String, qty As Double)
Dim Row As Integer
    Row = 0
    With flxRekapKomposisi
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = kode_bahan Then Row = i
    Next
    If Row > 0 Then
        .TextMatrix(Row, 3) = .TextMatrix(Row, 3) - qty
    End If
    If Row > 0 Then
        If .TextMatrix(Row, 3) = 0 Then
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
    End If
    If .Rows > 1 Then .Rows = .Rows - 1
    End If
    End With
    pct_rekapkomposisi
End Sub
Private Sub subs_rekapkemasan(kode_bahan As String, qty As Double)
Dim Row As Integer
    Row = 0
    With flxRekapKemasan
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = kode_bahan Then Row = i
    Next
    If Row > 0 Then
        .TextMatrix(Row, 3) = .TextMatrix(Row, 3) - qty
    End If
    If .TextMatrix(Row, 3) = 0 Then
    For Row = Row To .Rows - 1
        If Row = .Rows - 1 Then
            For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf .TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = ""
            Next
        ElseIf .TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To .cols - 1
            .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    If .Rows > 1 Then .Rows = .Rows - 1
    End If
    End With
    
End Sub

Private Sub delete_rekapkemasan()
Dim i As Integer
Dim Row As Integer

    With flxRekapKemasan
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = flxGrid6.TextMatrix(flxGrid.Row, 2) Then Row = i
    Next
    If Row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(Row, 3)) Then .TextMatrix(Row, 3) = CDbl(.TextMatrix(Row, 3)) - flxGrid.TextMatrix(flxGrid.Row, 5)
    If IsNumeric(.TextMatrix(Row, 4)) Then .TextMatrix(Row, 4) = CDbl(.TextMatrix(Row, 4)) - flxGrid.TextMatrix(flxGrid.Row, 6)
    If .TextMatrix(Row, 4) = 0 Or .TextMatrix(Row, 3) = 0 Then
        For Row = Row To .Rows - 1
            If Row = .Rows - 1 Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(Row + 1, 1) = "" Then
                For Col = 1 To .cols - 1
                    .TextMatrix(Row, Col) = ""
                Next
            ElseIf .TextMatrix(Row + 1, 1) <> "" Then
                For Col = 1 To .cols - 1
                .TextMatrix(Row, Col) = .TextMatrix(Row + 1, Col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    

    End With
End Sub

Private Sub add_biaya()
Dim i As Integer
Dim Row As Integer
    Row = 0
    
    If txtKodeBiaya.text <> "" And lblNamaBiaya <> "" Then
        With flxBiaya
        Row = .Rows - 1
        .Rows = .Rows + 1
        
        .Row = Row
        currentRow = .Row
        
        .TextMatrix(Row, 1) = txtKodeBiaya.text
        .TextMatrix(Row, 2) = lblNamaBiaya
        .TextMatrix(Row, 3) = CStr(txtKeteranganBiaya)
        .TextMatrix(Row, 4) = txtJumlahBiaya
        .Row = Row
        .Col = 0
        .ColSel = .cols - 1

        totalBiaya = totalBiaya + .TextMatrix(Row, 4)

        lblTotalBiaya = Format(totalBiaya, "#,##0.##")
        
        
        If Row > 8 Then
            .TopRow = Row - 7
        Else
            .TopRow = 1
        End If
        End With

        
        txtKodeBiaya.text = ""
        lblNamaBiaya = ""
        txtJumlahBiaya = ""
        txtKeteranganBiaya = ""
        txtKodeBiaya.SetFocus
        cmdResetBiaya.Enabled = False
        cmdHapusBiaya.Enabled = False
    End If
    mode = 1
End Sub

Private Sub cmdopentreeHasil_Click()

On Error Resume Next
    If tvwHasil.Visible = False Then
    tvwHasil.Visible = True
    tvwHasil.SetFocus
    Else
    tvwHasil.Visible = False
    End If
End Sub

Private Sub cmdopentreeview_Click()

On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub cmdopentreeviewKemasan_Click()

On Error Resume Next
    If tvwKemasan.Visible = False Then
    tvwKemasan.Visible = True
    tvwKemasan.SetFocus
    Else
    tvwKemasan.Visible = False
    End If
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
'    If Not Cek_qty Then
'        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
'        Exit Sub
'    End If

    If simpan Then
    If posting_produksi(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_produksi] from t_produksih where [nomer_produksi]<'" & lblNoTrans & "' and tipe='" & Tipe & "' order by [nomer_produksi] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdResetBiaya_Click()
    txtKodeBiaya.text = ""
    
    lblNamaBiaya = ""
    txtKeteranganBiaya.text = ""
    txtJumlahBiaya.text = "0"
    mode = 1
    cmdResetBiaya.Enabled = False
    cmdHapusBiaya.Enabled = False
End Sub

Private Sub cmdResetHasil_Click()
    txtKodeHasil.text = ""
'    txtNamaSupplier = ""
    lblNamaHasil = ""
    txtQtyHasil.text = "0"
    txtJumlahHasil.text = "0"
    txtHarga = 0
    mode = 1
    cmdResetHasil.Enabled = False
    cmdHapusHasil.Enabled = False
End Sub

Private Sub cmdResetKemasan_Click()
    txtKodeKemasan = ""
    lblNamaKemasan = ""
    txtQtyKemasan = "1"
    txtBeratKemasan = "1"
End Sub

Private Sub cmdResetKomposisiHasil_Click()
    txtKodeKomposisiHasil.text = ""
    
    lblNamakomposisiHasil = ""
    
    txtpct.text = "0"
    mode = 1
    cmdResetKomposisiHasil.Enabled = False
    cmdHapusKomposisiHasil.Enabled = False
End Sub

Private Sub cmdSearchBiaya_Click()
    With frmSearch
        .connstr = strcon
        
        .query = "select * from ms_biayaproduksi"
        .nmform = "frmTransProduksi"
        .nmctrl = "txtkodeBiaya"
        .nmctrl2 = ""
        .keyIni = "ms_biayaproduksi"
        .Col = 0
        .Index = -1
        
        .proc = "cek_kodebiaya"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang1"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        cek_kodebarang1 = True
    Else
        LblNamaBarang1 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
    
    rs.Open "Select top 1 s.nama_supplier from t_belid d " & _
            "inner join t_belih t on t.nomer_beli=d.nomer_beli " & _
            "inner join ms_supplier s on s.kode_supplier=t.kode_supplier " & _
            "order by t.tanggal_beli desc", conn
    If Not rs.EOF Then
        txtNamaSupplier.text = rs!Nama_Supplier
    End If
    If rs.State Then rs.Close
    
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_kodebarang2() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKodeKomposisiHasil & "'", conn
    If Not rs.EOF Then
        lblNamakomposisiHasil = rs!nama_bahan
        cek_kodebarang2 = True
    Else
        lblNamakomposisiHasil = ""
        
        cek_kodebarang2 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_kodebarang3() As Boolean
If txtKodeKemasan.text = "" Then Exit Function
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKodeKemasan & "'", conn
    If Not rs.EOF Then
        lblNamaKemasan = rs!nama_bahan
        cek_kodebarang3 = True
    Else
        lblNamaKemasan = ""
        
        cek_kodebarang3 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_kodehasil() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKodeHasil & "'", conn
    If Not rs.EOF Then
        lblNamaHasil = rs!nama_bahan
        cek_kodehasil = True
    Else
        lblNamaHasil = ""
        cek_kodehasil = False
        GoTo ex
    End If
    If rs.State Then rs.Close
'    rs.Open "select top 1 hpp from hst_hppbahan where kode_bahan='" & txtKodeHasil & "' order by tanggal desc"
'    If Not rs.EOF Then
'        txtHarga = rs(0)
'    Else
'        txtHarga = 0
'    End If
'    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_kodebiaya() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_biayaproduksi where [kode_biaya]='" & txtKodeBiaya & "'", conn
    If Not rs.EOF Then
        lblNamaBiaya = rs!nama_biaya
        cek_kodebiaya = True
    Else
        lblNamaBiaya = ""

        cek_kodebiaya = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function get_namabarang(kode As String) As String
On Error GoTo ex
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        get_namabarang = rs!nama_bahan
    Else
        get_namabarang = ""
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function Cek_Serialkemasan() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from stock where nomer_serial='" & txtNomerSerialKemasan.text & "' and kode_bahan='" & txtKodeKemasan.text & "' and kode_gudang='" & Trim(Right(cmbGudangKemasan.text, 10)) & "'", conn
    If Not rs.EOF Then
        Cek_Serialkemasan = True
    Else
        Cek_Serialkemasan = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_mesin() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_mesin where kode_mesin='" & txtKodeMesin.text & "'", conn
    If Not rs.EOF Then
        lblNamaMesin = rs!nama_mesin
        cek_mesin = True
    Else
        lblNamaMesin = ""
        cek_mesin = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Private Sub cmdSearchCustomer_Click()
    frmSearch.query = "select * from ms_mesin"
    frmSearch.nmform = "frmTransProduksi"
    frmSearch.nmctrl = "txtkodemesin"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_mesin"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_mesin"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    'frmSearch.query = "select * from t_produksih where tipe='" & Tipe & "'" 'query dev
    frmSearch.query = "select * from vw_search_produksi where tipe='" & Tipe & "'"
    frmSearch.nmform = "frmtransproduksi"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "produksi"
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.top = "5000"
    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKemasan_Click()
    frmSearch.query = searchBarang & " where kategori='kemasan'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKodeKemasan"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang3"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKomposisi_Click()
    With frmSearch
        .connstr = strcon
        .query = searchBarang
        .nmform = "frmTransProduksi"
        .nmctrl = "txtKodeHasil"
        .nmctrl2 = ""
        .keyIni = "ms_bahan"
        .Col = 0
        .Index = -1
        .proc = "cek_kodehasil"
        .loadgrid .query
'        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchKomposisiHasil_Click()
    With frmSearch
        .connstr = strcon
        .query = searchBarang
        .nmform = "frmTransProduksi"
        .nmctrl = "txtKodeKomposisiHasil"
        .nmctrl2 = ""
        .keyIni = "ms_bahan"
        .Col = 0
        .Index = -1
        .proc = "cek_kodebarang2"
        .loadgrid .query
'        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmTransProduksi"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .Col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_produksih where nomer_produksi='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal_produksi")
        DTPicker2 = rs("jam_mulai")
        DTPicker3 = rs("jam_selesai")
        txtKeterangan.text = rs("keterangan")
        txtHasilProses.text = rs("keterangan_hasil")
        cmbProses.text = rs!proses
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        
        Dim pengawas() As String
        Dim mesin() As String
        Dim kuli() As String
        
'        pengawas = Split(rs!pengawas, ",")
'        listPengawas.Clear
'        For A = 0 To UBound(pengawas)
'            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
'            txtPengawas_KeyDown 13, 0
'        Next
'        mesin = Split(rs!mesin, ",")
'        listMesin.Clear
'        For A = 0 To UBound(mesin)
'            txtKodeMesin = Replace(Replace(mesin(A), "[", ""), "]", "")
'            txtKodeMesin_KeyDown 13, 0
'        Next
'        kuli = Split(rs!kuli, ",")
'        For J = 0 To listKuli.ListCount - 1
'            listKuli.Selected(J) = False
'        Next
'        For A = 0 To UBound(kuli)
'            For J = 0 To listKuli.ListCount - 1
'                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
'            Next
'        Next
        total = 0
        totalberat = 0
        totalBiaya = 0
'        cmdPrint.Visible = True
'        status_produksisting = rs!status_produksisting
        If rs("status_posting") = 1 Then
            cmdPosting.Visible = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Visible = True
            cmdSimpan.Enabled = True
        End If
        
        If rs.State Then rs.Close
        
        listMesin.Clear
        rs.Open "select t.*,m.nama_mesin from t_produksi_mesin t inner join ms_mesin m on t.kode_mesin = m.kode_mesin where nomer_produksi = '" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
            listMesin.AddItem rs!kode_mesin & "-" & rs!nama_mesin
            rs.MoveNext
        Wend
        rs.Close
        listPengawas.Clear
        rs.Open "select t.*,m.nama_karyawan from t_produksi_pengawas t inner join ms_karyawan m on t.nik = m.nik where nomer_produksi = '" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
            listPengawas.AddItem rs!nik & "-" & rs!nama_karyawan
            rs.MoveNext
        Wend
        rs.Close
        
        Load_ListKuli
        
        rs.Open "select t.*,m.nama_karyawan from t_produksi_kuli t inner join ms_karyawan m on t.kuli = m.nik where nomer_produksi = '" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
            For J = 0 To listKuli.ListCount - 1
                If listKuli.List(J) = rs!kuli & "-" & rs!nama_karyawan Then
                    listKuli.Selected(J) = True
                End If
            Next
            rs.MoveNext
        Wend
        rs.Close
        
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        flxKomposisi.Rows = 1
        flxKomposisi.Rows = 2
        flxBiaya.Rows = 1
        flxBiaya.Rows = 2
        flxKomposisiHasil.Rows = 1
        flxKomposisiHasil.Rows = 2
        flxHasil.Rows = 1
        flxHasil.Rows = 2
        flxGrid6.Rows = 1
        flxGrid6.Rows = 2
        flxRekapBahan.Rows = 1
        flxRekapBahan.Rows = 2
        flxRekapKemasan.Rows = 1
        flxRekapKemasan.Rows = 2
        Row = 1
        total = 0
        totalberat = 0
        totalQty = 0
        totalBiaya = 0
        Row = 1
        
        If (User = "sa") Then
            flxGrid.ColWidth(7) = 1300
            flxHasil.ColWidth(5) = 1300
        ElseIf (User = "sugik") Then
            flxGrid.ColWidth(7) = 1300
            flxHasil.ColWidth(5) = 1300
        Else
            flxGrid.ColWidth(7) = 0
            flxHasil.ColWidth(5) = 0
        End If
        
        With flxGrid
        rs.Open "select d.kode_gudang,d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat,d.hpp,d.supplier from t_produksi_bahan d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_produksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                SetComboTextRight rs!kode_gudang, cmbGudang
                .TextMatrix(Row, 2) = rs!kode_bahan
                .TextMatrix(Row, 3) = rs!nama_bahan
                .TextMatrix(Row, 4) = rs!nomer_serial
                .TextMatrix(Row, 5) = Format(rs!qty, "#,##0")
                .TextMatrix(Row, 6) = Format(rs!berat, "#,##0.##")
                .TextMatrix(Row, 7) = Format(rs!hpp, "#,##0.##")
                .TextMatrix(Row, 8) = cmbGudang.text
                .TextMatrix(Row, 9) = rs!supplier
                
                txtKdBrg = rs!kode_bahan
                LblNamaBarang1 = rs!nama_bahan
                txtNamaSupplier.text = rs!supplier
                txtNoSerial = rs!nomer_serial
                txtQty = rs!qty
                txtBerat = rs!berat
                
                add_rekap
'                .TextMatrix(row, 8) = cmbGudang.text
                'add_rekap rs(1), rs(2), rs(5), rs(6)
                Row = Row + 1
                .Rows = .Rows + 1
                totalberat = totalberat + rs!berat
                totalQty = totalQty + rs!qty
                
                rs.MoveNext
        Wend
        rs.Close
'        cmbGudang.ListIndex = 0
        End With
        Row = 1
        With flxKomposisi
        .Rows = 2
        rs.Open "select d.[kode_bahan],m.[nama_bahan], d.berat,d.pct from t_produksi_komposisibahan d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_produksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                .TextMatrix(Row, 1) = rs(0)
                .TextMatrix(Row, 2) = rs(1)
                .TextMatrix(Row, 3) = rs(2)
                .TextMatrix(Row, 4) = rs(3)
                Row = Row + 1
                .Rows = .Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        End With
        Row = 1
        With flxHasil
        rs.Open "select d.kode_gudang,d.[kode_bahan],m.[nama_bahan],d.qty,d.berat,d.hpp,d.supplier from t_produksi_hasil d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_produksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                .TextMatrix(Row, 1) = rs!kode_bahan
                .TextMatrix(Row, 2) = rs!nama_bahan
                .TextMatrix(Row, 3) = rs!qty
                .TextMatrix(Row, 4) = rs!berat
                .TextMatrix(Row, 5) = Format(rs!hpp, "#,##0.##")
                SetComboTextRight rs!kode_gudang, cmbGudangHasil
                .TextMatrix(Row, 6) = cmbGudangHasil.text
                .TextMatrix(Row, 7) = rs!supplier
                'add_rekap rs(1), rs(2), rs(5), rs(6)
                Row = Row + 1
                total = total + rs!berat
                .Rows = .Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        cmbGudangHasil.ListIndex = 0
        End With
        Row = 1
        With flxKomposisiHasil
        rs.Open "select d.[kode_bahan],'',d.kode_komposisi,m.[nama_bahan],d.pct from t_produksi_komposisihasil d inner join ms_bahan m on d.[kode_komposisi]=m.[kode_bahan] where d.nomer_produksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                .TextMatrix(Row, 1) = rs(0)
                .TextMatrix(Row, 2) = rs(1)
                .TextMatrix(Row, 3) = rs(2)
                .TextMatrix(Row, 4) = rs(3)
                .TextMatrix(Row, 5) = rs(4) * total / 100
                .TextMatrix(Row, 6) = rs(4)
                Row = Row + 1
                
                .Rows = .Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        End With
        Row = 1
        With flxBiaya
        rs.Open "select d.[kode_biaya],m.[nama_biaya],d.keterangan,biaya from t_produksi_biaya d inner join ms_biayaproduksi m on d.[kode_biaya]=m.[kode_biaya] where d.nomer_produksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                .TextMatrix(Row, 1) = rs(0)
                .TextMatrix(Row, 2) = rs(1)
                .TextMatrix(Row, 3) = rs(2)
                .TextMatrix(Row, 4) = rs(3)
                totalBiaya = totalBiaya + rs(3)
                Row = Row + 1
                
                .Rows = .Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        End With
        Row = 1
        TabStrip1.SelectedItem = TabStrip1.Tabs(6)
        TabStrip1_Click
        With flxGrid6
        
        rs.Open "select kode_hasil,d.[kode_bahan],d.nomer_serial,m.[nama_bahan],d.qty,d.berat,d.kode_gudang,d.hpp from t_produksi_kemasan d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_produksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                SetComboText rs!kode_hasil, cmbHasilKemasan
                txtKodeKemasan = rs!kode_bahan
                lblNamaKemasan = rs!nama_bahan
                txtNomerSerialKemasan = rs!nomer_serial
                txtQtyKemasan = rs!qty
                txtBeratKemasan = rs!berat
                SetComboTextRight rs!kode_gudang, cmbGudangKemasan
                hpp_kemasan = rs!hpp
                cmdAddKemasan_Click
                Row = Row + 1
                rs.MoveNext
        Wend
        rs.Close
        cmbGudangKemasan.ListIndex = 0
        End With
        TabStrip1.SelectedItem = TabStrip1.Tabs(1)
        TabStrip1_Click
        showtotal
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Function cek_pengawas() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
cek_pengawas = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
        cek_pengawas = True
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close

err:
End Function
Private Sub cmdSearchSerial_Click()
    frmSearch.query = "select nomer_serial,stock from stock where kode_bahan='" & txtKdBrg.text & "' and kode_gudang='" & Trim(Right(cmbGudang.text, 10)) & "'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtnoserial"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "serial"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cekserial"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Function cekserial() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cekserial = False
    If cmbGudang.text = "" Or txtKdBrg.text = "" Then
        cekserial = True
        Exit Function
    End If
conn.Open strcon
rs.Open "select * from stock where kode_bahan='" & txtKdBrg.text & "' and kode_gudang='" & Trim(Right(cmbGudang.text, 10)) & "' and nomer_serial='" & txtNoSerial & "'", conn
    If Not rs.EOF Then
    lblStockBerat = rs!stock
    lblStockKemasan = rs!qty
    cekserial = True
    Else
    cekserial = True
'MsgBox "Barang tersebut tidak ditemukan"
    End If
rs.Close
conn.Close
Exit Function
err:
MsgBox err.Description
If rs.State Then rs.Close
If conn.State Then conn.Close
Set conn = Nothing
End Function
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    cmbGudangHasil.Clear
    cmbGudangKemasan.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        cmbGudangHasil.AddItem rs(0) & Space(50) & rs(1)
        cmbGudangKemasan.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    'rs.Open "select distinct proses from t_produksih",conn
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub loaddetil()
    If flxGrid.TextMatrix(flxGrid.Row, 2) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.Row, 2)
        
        If Not cek_kodebarang1 Then
            LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.Row, 3)
        End If
        txtQty.text = flxGrid.TextMatrix(flxGrid.Row, 5)
        txtNoSerial = flxGrid.TextMatrix(flxGrid.Row, 4)
        txtBerat.text = flxGrid.TextMatrix(flxGrid.Row, 6)
        txtNamaSupplier.text = flxGrid.TextMatrix(flxGrid.Row, 9)
        txtBerat.SetFocus
    End If
End Sub

Private Sub cmdSearchSerialKemasan_Click()
 frmSearch.query = "select nomer_serial,stock from stock where kode_bahan='" & txtKodeKemasan.text & "' and kode_gudang='" & Trim(Right(cmbGudangKemasan.text, 10)) & "'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtNomerSerialKemasan"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "serial"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_serialkemasan"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If validasi Then
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
    End If
End Sub

Private Function validasi() As Boolean
On Error GoTo err
    validasi = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dikirim terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If listMesin.ListCount = 0 Then
        MsgBox "Masukkan mesin terlebih dahulu"
        txtKodeMesin.SetFocus
        Exit Function
    End If
'    If flxHasil.Rows <= 2 Then
'        MsgBox "Silahkan masukkan hasil produksi terlebih dahulu"
'        TabStrip1.SelectedItem = 3
'        TabStrip1_Click
'        Exit Function
'    End If

    validasi = True
err:
End Function

Private Sub Refresh_HPP()
    conn.ConnectionString = strcon
    conn.Open
    For i = 1 To flxHasil.Rows - 2
        rs.Open "select top 1 hpp from hst_hppbahan where kode_bahan='" & flxHasil.TextMatrix(i, 1) & "' order by tanggal desc", conn
        If Not rs.EOF Then
            flxHasil.TextMatrix(i, 5) = rs(0)
        Else
            flxHasil.TextMatrix(i, 5) = 0
        End If
        If rs.State Then rs.Close
    Next
    conn.Close
End Sub

Private Function hitunghpp() As Boolean
On Error GoTo err
Dim totalhppbahan As Double
Dim totalhpphasil As Double
hitunghpp = False
totalhppbahan = 0
totalhpphasil = 0

Refresh_HPP

For i = 1 To flxGrid.Rows - 2
totalhppbahan = totalhppbahan + (flxGrid.TextMatrix(i, 7) * flxGrid.TextMatrix(i, 6))
Next
For i = 1 To flxBiaya.Rows - 2
totalhppbahan = totalhppbahan + (flxBiaya.TextMatrix(i, 4))
Next

Dim hppnol As Byte
Dim totalberathppnol As Double
hppnol = 0
For i = 1 To flxHasil.Rows - 2
    If flxHasil.TextMatrix(i, 5) = 0 Then
        hppnol = hppnol + 1
        totalberathppnol = totalberathppnol + flxHasil.TextMatrix(i, 4)
    End If
    totalhpphasil = totalhpphasil + (flxHasil.TextMatrix(i, 5) * flxHasil.TextMatrix(i, 4))
Next

    For i = 1 To flxHasil.Rows - 2
        If flxHasil.TextMatrix(i, 4) > 0 And IsNumeric(flxHasil.TextMatrix(i, 4)) Then
            If flxHasil.TextMatrix(i, 5) = 0 Then flxHasil.TextMatrix(i, 5) = Format((totalhppbahan - totalhpphasil) / totalberathppnol, "#,##0.##") '* flxHasil.TextMatrix(i, 4)
        End If
    Next
    'Dim hpp_kemasan As Currency
    For i = 1 To flxHasil.Rows - 2
        For J = 1 To flxGrid6.Rows - 2
            If flxGrid6.TextMatrix(J, 1) = flxHasil.TextMatrix(i, 1) Then flxHasil.TextMatrix(i, 5) = Format(((flxHasil.TextMatrix(i, 4) * flxHasil.TextMatrix(i, 5)) + (flxGrid6.TextMatrix(J, 7) * flxGrid6.TextMatrix(J, 6))) / flxHasil.TextMatrix(i, 4), "#,##0.##")
        Next J
    Next i
hitunghpp = True
Exit Function
err:
MsgBox err.Description
End Function

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim rs As New ADODB.Recordset
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    hitunghpp
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_produksih", "nomer_produksi", DTPicker1, "PR")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_produksih where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_komposisibahan where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_bahan where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_biaya where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_hasil where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_komposisihasil where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_kemasan where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_mesin where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_pengawas where nomer_produksi='" & id & "'"
    conn.Execute "delete from t_produksi_kuli where nomer_produksi='" & id & "'"
    
    add_dataheader

    For Row = 1 To flxGrid.Rows - 2
        add_databahan (Row)
    Next
    For Row = 1 To flxKomposisi.Rows - 2
        add_datakomposisi (Row)
    Next
    For Row = 1 To flxBiaya.Rows - 2
        add_databiaya (Row)
    Next
    For Row = 1 To flxHasil.Rows - 2
        add_datahasil (Row)
    Next
    For Row = 1 To flxKomposisiHasil.Rows - 2
        add_datakomposisihasil (Row)
    Next
    For Row = 1 To flxGrid6.Rows - 2
        If flxGrid6.TextMatrix(Row, 2) <> "" Then add_datakemasan (Row)
    Next
    Dim nourut As Integer
    nourut = 1
    For Row = 0 To listMesin.ListCount - 1
        conn.Execute "insert into t_produksi_mesin values('" & lblNoTrans & "','" & Left(listMesin.List(Row), InStr(1, listMesin.List(Row), "-") - 1) & "'," & nourut & " )"
        nourut = nourut + 1
    Next
    nourut = 1
    For Row = 0 To listPengawas.ListCount - 1
        conn.Execute "insert into t_produksi_pengawas values('" & lblNoTrans & "','" & Left(listPengawas.List(Row), InStr(1, listPengawas.List(Row), "-") - 1) & "'," & nourut & " )"
        nourut = nourut + 1
    Next
    nourut = 1
    For Row = 0 To listKuli.ListCount - 1
        If listKuli.Selected(Row) = True Then
            conn.Execute "insert into t_produksi_kuli values('" & lblNoTrans & "','" & Left(listKuli.List(Row), InStr(1, listKuli.List(Row), "-") - 1) & "'," & nourut & " )"
            nourut = nourut + 1
        End If
    Next
    
    conn.CommitTrans
    i = 0
    simpan = True
    conn.Close
    DropConnection
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(16)
    ReDim nilai(16)
    table_name = "t_produksih"
    fields(0) = "nomer_produksi"
    fields(1) = "tanggal_produksi"
    
'    fields(2) = "nomer_so"
    fields(2) = "keterangan"
    fields(3) = "userid"
    fields(4) = "status_posting"
    
    fields(5) = "mesin"
    fields(6) = "pengawas"
    fields(7) = "kuli"
    
    fields(8) = "tipe"
    fields(9) = "totalhasil"
    fields(10) = "totalbiaya"
    fields(11) = "totalbahan"
    fields(12) = "jam_mulai"
    fields(13) = "jam_selesai"
    fields(14) = "proses"
    fields(15) = "keterangan_hasil"
    
    Dim pengawas, mesin, kuli As String
    pengawas = ""
    For A = 0 To listPengawas.ListCount - 1
        pengawas = pengawas & "[" & Left(listPengawas.List(A), InStr(1, listPengawas.List(A), "-") - 1) & "],"
    Next
    If pengawas <> "" Then pengawas = Left(pengawas, Len(pengawas) - 1)
    mesin = ""
    For A = 0 To listMesin.ListCount - 1
        mesin = mesin & "[" & Left(listMesin.List(A), InStr(1, listMesin.List(A), "-") - 1) & "],"
    Next
    If mesin <> "" Then mesin = Left(mesin, Len(mesin) - 1)
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    
'    nilai(2) = txtNoOrder.text
    nilai(2) = txtKeterangan.text
    nilai(3) = User
    nilai(4) = 0
    nilai(5) = mesin
    nilai(6) = pengawas
    nilai(7) = kuli
    
    nilai(8) = Tipe
    nilai(9) = totalberat
    nilai(10) = totalBiaya
    nilai(11) = total
    nilai(12) = Format(DTPicker2, "HH:mm:ss")
    nilai(13) = Format(DTPicker3, "HH:mm:ss")
    nilai(14) = cmbProses.text
    nilai(15) = txtHasilProses.text
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_databahan(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_produksi_bahan"
    fields(0) = "nomer_produksi"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "nomer_serial"
    fields(5) = "NO_URUT"
    fields(6) = "hpp"
    fields(7) = "kode_gudang"
    fields(8) = "supplier"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 2)
    nilai(2) = Replace(Format(flxGrid.TextMatrix(Row, 5), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(Row, 6), "###0.##"), ",", ".")
    nilai(4) = flxGrid.TextMatrix(Row, 4)
    nilai(5) = Row
    nilai(6) = GetHPPKertas2(flxGrid.TextMatrix(Row, 2), flxGrid.TextMatrix(Row, 4), Trim(Right(cmbGudang.text, 10)), conn)
    nilai(7) = Trim(Right(flxGrid.TextMatrix(Row, 8), 10))
    nilai(8) = flxGrid.TextMatrix(Row, 9)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datakomposisi(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_produksi_komposisibahan"
    fields(0) = "nomer_produksi"
    fields(1) = "kode_bahan"
    fields(2) = "berat"
    fields(3) = "pct"
    fields(4) = "NO_URUT"
   
    nilai(0) = lblNoTrans
    nilai(1) = flxKomposisi.TextMatrix(Row, 1)
    nilai(2) = Replace(Format(flxKomposisi.TextMatrix(Row, 3), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxKomposisi.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(4) = Row
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_databiaya(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_produksi_biaya"
    fields(0) = "nomer_produksi"
    fields(1) = "kode_biaya"
    fields(2) = "keterangan"
    fields(3) = "biaya"
    fields(4) = "NO_URUT"
   
    nilai(0) = lblNoTrans
    nilai(1) = flxBiaya.TextMatrix(Row, 1)
    nilai(2) = flxBiaya.TextMatrix(Row, 3)
    nilai(3) = Replace(Format(flxBiaya.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(4) = Row
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datahasil(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(8)
    ReDim nilai(8)

    table_name = "t_produksi_hasil"
    fields(0) = "nomer_produksi"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "NO_URUT"
    fields(5) = "hpp"
    fields(6) = "kode_gudang"
    fields(7) = "supplier"
   
    nilai(0) = lblNoTrans
    nilai(1) = flxHasil.TextMatrix(Row, 1)
    nilai(2) = Replace(Format(flxHasil.TextMatrix(Row, 3), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxHasil.TextMatrix(Row, 4), "###0.##"), ",", ".")
    nilai(4) = Row
    nilai(5) = Replace(Format(flxHasil.TextMatrix(Row, 5), "###0.##"), ",", ".")
    nilai(6) = Trim(Right(flxHasil.TextMatrix(Row, 6), 10))
    nilai(7) = flxHasil.TextMatrix(Row, 7)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datakemasan(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_produksi_kemasan"
    fields(0) = "nomer_produksi"
    fields(1) = "kode_hasil"
    fields(2) = "kode_bahan"
    fields(3) = "nomer_serial"
    fields(4) = "qty"
    fields(5) = "berat"
    fields(6) = "NO_URUT"
    fields(7) = "hpp"
    fields(8) = "kode_gudang"
   
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid6.TextMatrix(Row, 1)
    nilai(2) = flxGrid6.TextMatrix(Row, 2)
    nilai(3) = flxGrid6.TextMatrix(Row, 4)
    nilai(4) = Replace(Format(flxGrid6.TextMatrix(Row, 5), "###0.##"), ",", ".")
    nilai(5) = Replace(Format(flxGrid6.TextMatrix(Row, 6), "###0.##"), ",", ".")
    nilai(6) = Row
    nilai(7) = Format(GetHPPKertas2(flxGrid6.TextMatrix(Row, 2), flxGrid6.TextMatrix(Row, 4), Trim(Right(flxGrid6.TextMatrix(Row, 8), 10)), conn), "###0.##")
    nilai(8) = Trim(Right(flxGrid6.TextMatrix(Row, 8), 10))
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datakomposisihasil(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_produksi_komposisihasil"
    fields(0) = "nomer_produksi"
    fields(1) = "kode_bahan"
    fields(2) = "kode_komposisi"
    fields(3) = "pct"
    fields(4) = "NO_URUT"
    
   
    nilai(0) = lblNoTrans
    nilai(1) = flxKomposisiHasil.TextMatrix(Row, 1)
    nilai(2) = Replace(Format(flxKomposisiHasil.TextMatrix(Row, 3), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxKomposisiHasil.TextMatrix(Row, 6), "###0.##"), ",", ".")
    nilai(4) = Row
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub hitung_persen()
On Error GoTo err
'    For i = 0 To dbgrid.Rows - 1
'        dbgrid.row = i
'        dbgrid.Columns(3).text = Format(dbgrid.Columns(2).text / total * 100, "#0.00")
'        dbgrid.Update
'    Next
    If totalkomposisi > 0 Then
    For i = 1 To flxKomposisi.Rows - 2
        flxKomposisi.TextMatrix(i, 4) = Format(flxKomposisi.TextMatrix(i, 3) / totalkomposisi * 100, "#0.00")
    Next
    End If
    If totalberat > 0 Then
    For i = 1 To flxRekapBahan.Rows - 2
        flxRekapBahan.TextMatrix(i, 5) = Format(flxRekapBahan.TextMatrix(i, 4) / totalberat * 100, "#0.00")
    Next
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdUpdateKemasan_Click()
    Dim trans As Boolean
    On Error GoTo err
    conn.Open strcon
    conn.BeginTrans
    trans = True
    conn.Execute "delete from t_produksi_kemasan where nomer_produksi='" & lblNoTrans.Caption & "'"
    For Row = 1 To flxGrid6.Rows - 2
        If flxGrid6.TextMatrix(Row, 2) <> "" Then add_datakemasan (Row)
    Next
    conn.CommitTrans
    trans = True
    MsgBox "Data kemasan sudah diupdate."
    conn.Close
    Exit Sub
err:
    If trans = True Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub Command2_Click()
Dim conn As New ADODB.Connection
Dim rs1 As New ADODB.Recordset
        conn.Open strcon
        For i = 1 To flxHasil.Rows - 2
        rs1.Open "select top 1 hpp from hst_hppbahan where kode_bahan='" & flxHasil.TextMatrix(i, 1) & "' order by tanggal desc", conn
            If Not rs1.EOF Then
                flxHasil.TextMatrix(i, 5) = rs1(0)
            Else
                flxHasil.TextMatrix(i, 5) = 0
            End If
            If rs1.State Then rs1.Close
        Next
        hitunghpp
        updatehpp
        conn.Close
End Sub

Private Sub Command1_Click()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select nomer_produksi from t_produksih where left(nomer_produksi,4)='PR12'", conn
    While Not rs.EOF
        lblNoTrans = rs(0)
        cek_notrans
        For i = 1 To flxHasil.Rows - 1
        rs1.Open "select top 1 hpp from hst_hppbahan where kode_bahan='" & flxHasil.TextMatrix(i, 1) & "' order by tanggal desc", conn
            If Not rs1.EOF Then
                flxHasil.TextMatrix(i, 5) = rs1(0)
            Else
                flxHasil.TextMatrix(i, 5) = 0
            End If
            If rs1.State Then rs1.Close
        Next
        hitunghpp
        updatehpp
        rs.MoveNext
    Wend
    rs.Close
End Sub
Private Sub updatehpp()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    conn.Open strcon
    For i = 1 To flxHasil.Rows - 1
        conn.Execute "update t_produksi_hasil set hpp='" & Replace(Format(flxHasil.TextMatrix(i, 5), "###0.##"), ",", ".") & "' where kode_bahan='" & flxHasil.TextMatrix(i, 1) & "' and nomer_produksi='" & lblNoTrans & "' and kode_gudang='" & Trim(Right(flxHasil.TextMatrix(i, 6), 10)) & "'"
    Next
    conn.Close
    
End Sub

Private Sub Command3_Click()
hitunghpp
End Sub

Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub


Private Sub flxBiaya_DblClick()
    If flxBiaya.TextMatrix(flxBiaya.Row, 1) <> "" Then
        loaddetilbiaya
        txtJumlahBiaya.SetFocus
    End If
End Sub
Private Sub loaddetilbiaya()
    txtKodeBiaya = flxBiaya.TextMatrix(flxBiaya.Row, 1)
    txtKeteranganBiaya = flxBiaya.TextMatrix(flxBiaya.Row, 3)
    txtJumlahBiaya = flxBiaya.TextMatrix(flxBiaya.Row, 4)
End Sub

Private Sub flxBiaya_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then cmdHapusBiaya_Click
End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.Row, 2) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub


Private Sub flxGrid6_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then 'And flxHasil.Row < flxHasil.Rows - 1 Then
        If flxGrid6.Row > 0 And flxGrid6.Row < flxGrid6.Rows - 1 Then flxGrid6.RemoveItem (flxGrid6.Row)
        'cmdHapusKemasan_Click
    End If
End Sub

Private Sub flxHasil_DblClick()
On Error Resume Next
    If flxHasil.TextMatrix(flxHasil.Row, 1) <> "" Then
        loaddetilhasil
        txtQtyHasil.SetFocus
    End If
End Sub

Private Sub loaddetilhasil()
    txtKodeHasil = flxHasil.TextMatrix(flxHasil.Row, 1)
    cek_kodehasil
    txtQtyHasil = flxHasil.TextMatrix(flxHasil.Row, 3)
    txtJumlahHasil = flxHasil.TextMatrix(flxHasil.Row, 4)
    txtHarga = flxHasil.TextMatrix(flxHasil.Row, 5)
'    txtNamaSupplier = flxHasil.TextMatrix(flxHasil.row, 7)
End Sub

Private Sub flxHasil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And flxHasil.Row < flxHasil.Rows - 1 Then
        cmdHapusHasil_Click
    End If
End Sub

Private Sub flxKomposisi_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then cmdHapusHasil_Click
End Sub

Private Sub flxKomposisiHasil_DblClick()

    If flxKomposisiHasil.TextMatrix(flxKomposisiHasil.Row, 1) <> "" Then
        loaddetilkomposisihasil
        txtpct.SetFocus
    End If
End Sub
Private Sub loaddetilkomposisihasil()
    SetComboText flxKomposisiHasil.TextMatrix(flxKomposisiHasil.Row, 1), cmbHasil
    txtKodeKomposisiHasil = flxKomposisiHasil.TextMatrix(flxKomposisiHasil.Row, 3)
    cek_kodebarang2
    txtpct = flxKomposisiHasil.TextMatrix(flxKomposisiHasil.Row, 5)
End Sub


Private Sub flxKomposisiHasil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And flxKomposisiHasil.Row < flxKomposisiHasil.Rows - 1 Then
        cmdHapusKomposisiHasil_Click
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then MySendKeys "{tab}"
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
'    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
Dim NamaGrup As String, LihatHPP As Boolean
    
    LihatHPP = True

    loadgrup "0", tvwMain
    loadgrup "0", tvwHasil
    loadgrup "0", tvwKemasan
    DTPicker1 = Now
    DTPicker2 = Now
    DTPicker3 = Now
    seltab = 0
    If Tipe = 1 Then
        frKomposisiHasil.Visible = False
    Else
        frKomposisiHasil.Visible = True
    End If
    
    Load_ListKuli
    
    conn.ConnectionString = strcon
    conn.Open
    
    
    
'    listKuli.Clear
'    rs.Open "select * from ms_karyawan order by nik", conn
'    While Not rs.EOF
'        listKuli.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
    
    If LCase(User) <> "sa" Then
        rs.Open "select nama_group from user_group where userid='" & User & "'", conn
        If Not rs.EOF Then
            NamaGrup = rs("nama_group")
        End If
        rs.Close
        
        rs.Open "select hpp_produksi from var_usergroup where [group]='" & NamaGrup & "'", conn
        If Not rs.EOF Then
            If rs!hpp_produksi = "0" Then LihatHPP = False
        End If
        rs.Close
    End If
    
    
    conn.Close
    load_combo
    reset_form
    
    'Tab Produksi
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 0
    flxGrid.ColWidth(2) = 1300
    flxGrid.ColWidth(3) = 2800
    flxGrid.ColWidth(4) = 1600
    flxGrid.ColWidth(5) = 900
    flxGrid.ColWidth(6) = 900
    
    If LihatHPP Then
        flxGrid.ColWidth(7) = 900
    Else
        flxGrid.ColWidth(7) = 0
    End If
    
    flxGrid.ColWidth(8) = 1200
    flxGrid.ColWidth(9) = 2000

    flxGrid.TextMatrix(0, 1) = "No. jual"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.ColAlignment(8) = 1
    flxGrid.TextMatrix(0, 2) = "Kode Bahan"
    flxGrid.TextMatrix(0, 3) = "Nama"
    flxGrid.TextMatrix(0, 4) = "Nomer Serial"
    flxGrid.TextMatrix(0, 5) = "Qty"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "Harga"
    flxGrid.TextMatrix(0, 8) = "Gudang"
    flxGrid.TextMatrix(0, 9) = "Supplier"
    
    With flxKomposisi
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 2800
        .ColWidth(3) = 1300
        .ColWidth(4) = 900
        .TextMatrix(0, 1) = "Kode"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Jumlah"
        .TextMatrix(0, 4) = "%"
    End With
    With flxRekapBahan
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 2800
        .ColWidth(3) = 1200
        .ColWidth(4) = 1200
        .ColWidth(5) = 900
        .TextMatrix(0, 1) = "Kode"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Kemasan"
        .TextMatrix(0, 4) = "Berat"
        .TextMatrix(0, 5) = "%"
    End With
    With flxRekapKemasan
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 2800
        .ColWidth(3) = 1200
        .ColWidth(4) = 0
        .ColWidth(5) = 0
        .TextMatrix(0, 1) = "Kode"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Jumlah"
        .TextMatrix(0, 4) = "Berat"
        
    End With
    With flxGrid6 'Tab Kemasan
        .ColWidth(0) = 300
        .ColWidth(1) = 1200
        .ColWidth(2) = 1200
        .ColWidth(3) = 2800
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 0
        If LihatHPP Then
            .ColWidth(7) = 2000
        Else
            .ColWidth(7) = 0
        End If
        .ColWidth(8) = 0

        .TextMatrix(0, 1) = "Hasil"
        .TextMatrix(0, 2) = "Kode Kemasan"
        .TextMatrix(0, 3) = "Nama"
        .TextMatrix(0, 4) = "Serial"
        .TextMatrix(0, 5) = "Jumlah"
        .TextMatrix(0, 6) = "Berat"
        .TextMatrix(0, 7) = "HPP"
        .TextMatrix(0, 8) = "Gudang"

    End With
    
    With flxHasil 'Tab Hasil
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 2800
        .ColWidth(3) = 1300
        .ColWidth(4) = 900
        If LihatHPP Then
            .ColWidth(5) = 900
        Else
            .ColWidth(5) = 0
        End If
        .ColWidth(6) = 900
        .ColWidth(7) = 2000
        .TextMatrix(0, 1) = "Kode"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Kemasan"
        .TextMatrix(0, 4) = "Berat"
        .TextMatrix(0, 5) = "HPP"
        .TextMatrix(0, 6) = "Gudang"
        .TextMatrix(0, 7) = "Supplier"
    End With
    
    With flxKomposisiHasil
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 0
        .ColWidth(3) = 1300
        .ColWidth(4) = 2800
        .ColWidth(5) = 900
        .ColWidth(6) = 900
        
        .TextMatrix(0, 1) = "KodeHasil"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "KodeKomposisi"
        .TextMatrix(0, 4) = "NamaKomposisi"
        .TextMatrix(0, 5) = "Jml"
        .TextMatrix(0, 6) = "%"
        
    End With
    With flxRekapKomposisi
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 2000
        .ColWidth(3) = 1000
        .ColWidth(4) = 900
        .ColWidth(5) = 900
        
        .TextMatrix(0, 1) = "Kode"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Berat"
        
        .TextMatrix(0, 4) = "% Sblm"
        .TextMatrix(0, 5) = "% Ssdh"
        
    End With
    With flxBiaya
        .ColWidth(0) = 300
        .ColWidth(1) = 1300
        .ColWidth(2) = 2800
        .ColWidth(3) = 1300
        .ColWidth(4) = 900
        .TextMatrix(0, 1) = "Kode"
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Ket"
        .TextMatrix(0, 4) = "Jumlah"
    End With

End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub


Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    
    txtKeterangan.text = ""
    txtHasilProses.text = ""
    mode = 1
    
    
    totalberat = 0
    total = 0
    totalBiaya = 0
    lblTotal = 0
    lblQty = 0
    lblItem = 0
    lblBerat = 0
    lblSusut = 0
    lblPctSusut = 0
    
    DTPicker1 = Now
    
    cmdClear_Click
    cmdResetBiaya_Click
    cmdResetHasil_Click
    cmdResetKomposisiHasil_Click
    cmdResetKemasan_Click
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    With flxGrid
    .Rows = 1
    .Rows = 2
    End With
    With flxBiaya
    .Rows = 1
    .Rows = 2
    End With
    With flxGrid6
    .Rows = 1
    .Rows = 2
    End With
    With flxRekapBahan
    .Rows = 1
    .Rows = 2
    End With
    With flxRekapKemasan
    .Rows = 1
    .Rows = 2
    End With
    With flxHasil
    .Rows = 1
    .Rows = 2
    End With
    With flxKomposisi
    .Rows = 1
    .Rows = 2
    End With
    With flxKomposisiHasil
    .Rows = 1
    .Rows = 2
    End With
    conn.Open strcon
    cmbProses.Clear
    rs.Open "select distinct proses from t_produksih order by proses", conn
    While Not rs.EOF
        cmbProses.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
    
    conn.Close
End Sub

Private Sub lblItem_Click()

End Sub

Private Sub listMesin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then If ListIndex >= 0 Then listMesin.RemoveItem listMesin.ListIndex
    
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then If ListIndex >= 0 Then listPengawas.RemoveItem listPengawas.ListIndex
End Sub

Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
    If TabStrip1.SelectedItem.Index = 4 Then
        cmbHasil.Clear
        For i = 1 To flxHasil.Rows - 2
            For J = 0 To cmbHasil.ListCount - 1
                If flxHasil.TextMatrix(i, 1) = cmbHasil.List(J) Then GoTo n
            Next
            cmbHasil.AddItem flxHasil.TextMatrix(i, 1)
n:
        Next
    End If
    If TabStrip1.SelectedItem.Index = 6 Then
        cmbHasilKemasan.Clear
        For i = 1 To flxHasil.Rows - 2
            For J = 0 To cmbHasil.ListCount - 1
                If flxHasil.TextMatrix(i, 1) = cmbHasilKemasan.List(J) Then GoTo m
            Next
            cmbHasilKemasan.AddItem flxHasil.TextMatrix(i, 1)
m:
        Next
    End If
End Sub

Private Sub tvwHasil_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudangHasil
    tvwHasil.Visible = False
End Sub

Private Sub txtBeratGotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat)
End Sub


Private Sub tvwKemasan_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudangKemasan
    tvwKemasan.Visible = False

End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    tvwMain.Visible = False
End Sub

Private Sub txtJumlahHasil_GotFocus()
    txtJumlahHasil.SelStart = 0
    txtJumlahHasil.SelLength = Len(txtJumlahHasil)
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_LostFocus()
    If Not cek_kodebarang1 And txtKdBrg.text <> "" Then
        txtKdBrg.SetFocus
        MsgBox "Kode tidak ditemukan"
    End If
End Sub

Private Sub txtKodeBiaya_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBiaya_Click
End Sub

Private Sub txtKodeBiaya_LostFocus()
    cek_kodebiaya
End Sub

Private Sub txtKodeHasil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchKomposisi_Click
End Sub

Private Sub txtKodeHasil_LostFocus()
    cek_kodehasil
End Sub

Private Sub txtKodeKemasan_LostFocus()
    cek_kodebarang3
End Sub

Private Sub txtKodeKomposisiHasil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchKomposisiHasil_Click
End Sub

Private Sub txtKodeKomposisiHasil_LostFocus()
    cek_kodebarang2
End Sub

Private Sub txtKodeMesin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If cek_mesin Then
            listMesin.AddItem txtKodeMesin & "-" & lblNamaMesin
            txtKodeMesin = ""
            lblNamaMesin = ""
            MySendKeys "+{tab}"
        End If
    End If
End Sub

Private Sub txtKodeMesin_LostFocus()
    If Not cek_mesin And txtKodeMesin.text <> "" Then
        txtKodeMesin.SetFocus
        MsgBox "Kode mesin tidak ditemukan"
    End If
End Sub

Private Sub txtNoSerial_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSerial_Click
End Sub

Private Sub txtNoSerial_LostFocus()
   If txtNoSerial <> "" And Not cekserial Then
        txtNoSerial.SetFocus
        txtNoSerial.SelStart = 0
        txtNoSerial.SelLength = Len(txtNoSerial)
    End If
End Sub

Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        If cek_pengawas Then
            listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
            txtPengawas = ""
            lblNamaPengawas = ""
            MySendKeys "+{tab}"
        End If
    End If
End Sub

Private Sub txtPengawas_LostFocus()
    If Not cek_pengawas And txtPengawas.text <> "" Then
        txtPengawas.SetFocus
        MsgBox "Kode karyawan tidak ditemukan"
    End If
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty)
End Sub

Private Sub txtQtyHasil_GotFocus()
    txtQtyHasil.SelStart = 0
    txtQtyHasil.SelLength = Len(txtQtyHasil)
End Sub

Private Sub txtQtyKemasan_LostFocus()
    If Not IsNumeric(txtQtyKemasan) Then txtQtyKemasan.SetFocus
    txtBeratKemasan = txtQtyKemasan
End Sub
