VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmLaporanHarian 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Out Laporan"
   ClientHeight    =   1755
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1755
   ScaleWidth      =   6015
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4590
      TabIndex        =   5
      Top             =   990
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1530
      TabIndex        =   2
      Top             =   360
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   109379587
      CurrentDate     =   39094
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3150
      Picture         =   "frmLaporanHarian.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   945
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1530
      Picture         =   "frmLaporanHarian.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   945
      Width           =   1230
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   3285
      TabIndex        =   6
      Top             =   360
      Visible         =   0   'False
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   109379587
      CurrentDate     =   39094
   End
   Begin VB.Label Label2 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1305
      TabIndex        =   4
      Top             =   405
      Width           =   240
   End
   Begin VB.Label Label1 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   3
      Top             =   405
      Width           =   1185
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmLaporanHarian"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public Tipe As Byte
Dim sname As String
Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
Dim filtergudang, filtertanggal, filtercarabayar, filter3, filteruserid As String
Dim ext As String
Dim param3 As String
    Select Case filename
        Case "\Laporan Penjualan Harian":

            filtertanggal = "{tmp_lapharian.tanggal}"
            filteruserid = "{tmp_lapharian.userid}"

            'filter3 = "{t_penjualanh.bayar}"
            generate_dailyreport DTPicker1
        Case "\Laporan RL":

'            filtertanggal = "{tmp_lapharian.tanggal}"
'            filteruserid = "{tmp_lapharian.userid}"
            'filter3 = "{t_penjualanh.bayar}"
            generate_RL DTPicker1, DTPicker2
    
    End Select
    Formula = ""
        If filtertanggal <> "" Then
    'tanggal
        Formula = filtertanggal & ">=#" & Format(DTPicker1, "yyyy/MM/dd") & "# and " & filtertanggal & "<#" & Format(DTPicker1 + 1, "yyyy/MM/dd") & "#"
        End If
'    'gudang
'        If Formula <> "" Then Formula = Formula & " and "
'        Formula = Formula & filtergudang & "='" & gudang & "'"
'    'userid
    If filteruserid <> "" Then
    If LCase(group) <> "owner" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filteruserid & "='" & User & "'"
    End If
    End If
        cetakLaporan (filename & ".rpt")
    
End Sub
Private Sub cetakLaporan(filename As String)
On Error GoTo err
    
    With CRPrint
        .reset
        .ReportFileName = reportDir & filename
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            'If ServerType = "mysql" Or ServerType = "mssql" Then
            '.LogonInfo(i) = "DSN=" & servname
            'ElseIf ServerType = "access" Then
            .LogonInfo(i) = "DSN=" & sname & ";UID=Admin" '& ";PWD=" & pwd & ";"
            '.DataFiles(i) = servname
            'End If
        .ParameterFields(0) = "login;" + User + ";True"
        .ParameterFields(1) = "tglDari;" + Format(DTPicker1, "yyyy/MM/dd") + ";True"
        .ParameterFields(2) = "tglSampai;" + IIf(right_hpp, "1", "0") + ";True"
        .ParameterFields(3) = "Detail;1;True"

        
        Next
        .SelectionFormula = Formula
        .Destination = crptToWindow
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        .action = 1
    End With
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub Command1_Click()
    generate_dailystock DTPicker1
    generate_monthlystock DTPicker1
    generate_yearlystock DTPicker1
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    DTPicker1 = Now
    sname = servnameasli
    
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub
