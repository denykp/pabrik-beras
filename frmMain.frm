VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Begin VB.MDIForm frmMain 
   BackColor       =   &H8000000C&
   Caption         =   "Pabrik"
   ClientHeight    =   7065
   ClientLeft      =   225
   ClientTop       =   255
   ClientWidth     =   11205
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   0
      Top             =   0
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   6645
      Width           =   11205
      _ExtentX        =   19764
      _ExtentY        =   741
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Username"
            TextSave        =   "Username"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Gudang"
            TextSave        =   "Gudang"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7832
            MinWidth        =   7832
            Text            =   "Server"
            TextSave        =   "Server"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3528
            MinWidth        =   3528
            Text            =   "Printer"
            TextSave        =   "Printer"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Master"
      Index           =   0
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Grup Perkiraan"
         Index           =   0
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Sub Grup Perkiraan"
         Index           =   10
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Perkiraan"
         Index           =   20
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "-"
         Index           =   25
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Barang"
         Index           =   30
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Harga Jual Barang"
         Index           =   40
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "HPP"
         Index           =   50
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Karyawan"
         Index           =   70
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Customer"
         Index           =   80
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Supplier"
         Index           =   90
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Toko Konsinyasi"
         Index           =   92
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Lain-Lain"
         Index           =   95
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Bank"
         Index           =   100
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Ekspedisi"
         Index           =   101
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "-"
         Index           =   103
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Grup Gudang"
         Index           =   105
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Mesin"
         Index           =   110
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Biaya Produksi"
         Index           =   115
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Perkiraan Setting"
         Index           =   120
         Begin VB.Menu mnuSettingAcc 
            Caption         =   "Perkiraan Bahan"
            Index           =   10
         End
         Begin VB.Menu mnuSettingAcc 
            Caption         =   "Perkiraaan Supplier"
            Index           =   20
         End
         Begin VB.Menu mnuSettingAcc 
            Caption         =   "Perkiraan Customer"
            Index           =   30
         End
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Pembelian"
      Index           =   10
      Begin VB.Menu mnuPembelian 
         Caption         =   "Request Order Pembelian"
         Index           =   0
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Order Pembelian"
         Index           =   5
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Pengangkutan"
         Index           =   7
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Penerimaan Barang"
         Index           =   10
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "QC Penerimaan"
         Index           =   20
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Komposisi Barang datang"
         Index           =   25
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Pembelian"
         Index           =   30
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Retur Pembelian"
         Index           =   40
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "-"
         Index           =   45
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Laporan Request Order Pembelian"
         Index           =   50
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Laporan Order Pembelian"
         Index           =   55
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Laporan Penerimaan Barang"
         Index           =   60
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Laporan Pembelian"
         Index           =   65
      End
      Begin VB.Menu mnuPembelian 
         Caption         =   "Laporan Retur Pembelian "
         Index           =   70
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Produksi"
      Index           =   20
      Begin VB.Menu mnuProduksi 
         Caption         =   "Produksi dgn Komposisi Tetap"
         Index           =   0
      End
      Begin VB.Menu mnuProduksi 
         Caption         =   "Produksi Berubah Komposisi"
         Index           =   10
      End
      Begin VB.Menu mnuProduksi 
         Caption         =   "Kalkulasi"
         Index           =   20
      End
      Begin VB.Menu mnuProduksi 
         Caption         =   "-"
         Index           =   25
      End
      Begin VB.Menu mnuProduksi 
         Caption         =   "Laporan Produksi"
         Index           =   30
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Stock"
      Index           =   30
      Begin VB.Menu mnuStock 
         Caption         =   "Stock Opname"
         Index           =   0
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Koreksi Stock"
         Index           =   30
         Visible         =   0   'False
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Mutasi Stock"
         Index           =   55
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Info Stock"
         Index           =   60
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Info Stock Gabungan"
         Index           =   100
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Laporan Stock Akhir"
         Index           =   122
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Laporan Stock Per Tanggal"
         Index           =   125
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Kartu Stock"
         Index           =   130
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Laporan Mutasi Stock"
         Index           =   160
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Penjualan"
      Index           =   40
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Request Order Penjualan"
         Index           =   0
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Order Penjualan"
         Index           =   5
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Pengangkutan"
         Index           =   7
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Pengeluaran Barang"
         Index           =   10
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Surat Jalan"
         Index           =   15
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Retur Kirim"
         Index           =   18
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Penjualan"
         Index           =   20
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Retur Penjualan"
         Index           =   25
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Ubah Status SO"
         Index           =   30
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "-"
         Index           =   40
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Tambah Barang Konsinyasi"
         Index           =   50
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Retur Konsinyasi"
         Index           =   60
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Jual Konsinyasi"
         Index           =   70
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Info Stock Toko"
         Index           =   80
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "-"
         Index           =   85
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Request Penjualan"
         Index           =   90
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Order Penjualan"
         Index           =   95
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Pengeluaran Barang"
         Index           =   100
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Penjualan"
         Index           =   105
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Retur Penjualan"
         Index           =   110
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Ekspedisi"
         Index           =   120
      End
      Begin VB.Menu mnuPenjualan 
         Caption         =   "Laporan Ekspedisi biaya lain"
         Index           =   130
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Akuntansi dan Keuangan"
      Index           =   50
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Pembayaran Hutang"
         Index           =   5
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Pelunasan Piutang"
         Index           =   10
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Nota Debet / Kredit Piutang"
         Index           =   12
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Nota Debet / Kredit Hutang"
         Index           =   14
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Kas Masuk"
         Index           =   20
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Kas Keluar"
         Index           =   30
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Bank Masuk"
         Index           =   40
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Bank Keluar"
         Index           =   50
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Pencairan Giro Masuk"
         Index           =   53
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Pencairan Giro Keluar"
         Index           =   55
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Hutang"
         Index           =   57
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Piutang"
         Index           =   58
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Jurnal Umum"
         Index           =   60
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Pembelian Buku Cek/Giro"
         Index           =   65
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Ongkos Pengiriman"
         Index           =   66
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Manol"
         Index           =   67
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "-"
         Index           =   68
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Jurnal Harian"
         Index           =   70
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Hutang"
         Index           =   80
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Pembayaran Hutang"
         Index           =   90
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Piutang"
         Index           =   100
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Penerimaan Piutang"
         Index           =   110
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Outstanding Cek/BG"
         Index           =   112
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan History Harga Pokok"
         Index           =   120
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Buku Besar"
         Index           =   130
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Neraca"
         Index           =   140
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Laporan Laba Rugi"
         Index           =   150
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Kartu Hutang"
         Index           =   160
      End
      Begin VB.Menu mnuAkuntansi 
         Caption         =   "Kartu Piutang"
         Index           =   170
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Variabel dan Maintenance"
      Index           =   60
      Begin VB.Menu mnuUtility 
         Caption         =   "Setting"
         Index           =   0
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Setting Perkiraan"
         Index           =   5
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Satuan"
         Index           =   10
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Kategori Customer"
         Index           =   15
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Kategori Supplier"
         Index           =   17
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Kategori Barang"
         Index           =   20
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Sub Kategori Barang"
         Index           =   30
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "-"
         Index           =   36
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Data Perusahaan"
         Index           =   40
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Tutup Buku Bulanan"
         Index           =   50
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Batal Tutup Buku Bulanan"
         Index           =   60
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Ganti Pasword"
         Index           =   70
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "User"
         Index           =   80
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Group"
         Index           =   90
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Backup Database"
         Index           =   100
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Posting Otomatis"
         Index           =   120
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Posting Manual"
         Index           =   130
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Proses HPP"
         Index           =   140
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "UnPosting"
         Index           =   150
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Automatic Posting"
         Index           =   160
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Cut Off"
         Index           =   170
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Reminder"
      Index           =   70
      Begin VB.Menu mnuReminder 
         Caption         =   "Set Reminder"
         Index           =   0
      End
      Begin VB.Menu mnuReminder 
         Caption         =   "Alerts"
         Index           =   10
      End
      Begin VB.Menu mnuReminder 
         Caption         =   "generatemesindanpengawas"
         Index           =   20
      End
      Begin VB.Menu mnuReminder 
         Caption         =   "Laporan Penilaian Karyawan"
         Index           =   30
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Exit"
      Index           =   90
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim timer As Integer
Private Sub MDIForm_Load()
On Error Resume Next
    getConfig
    'Me.BackColor = BGColor1
    'Me.Picture = LoadPicture(BG, 1, , 0, 0)
    StatusBar1.Panels(1).text = "User = " & User
    StatusBar1.Panels(2).text = gudang
    StatusBar1.Panels(3).text = servname
    StatusBar1.Panels(4).text = printername
'    Call SetConnection(txtServerName.text)
    UpdateStatusReminder
End Sub

Private Sub mnuAkuntansi_Click(Index As Integer)
    Select Case Index
    Case 5: frmAddBayarHutang.Show
            SetFormPosition frmAddBayarHutang
    Case 10: frmAddBayarPiutang.Show
            SetFormPosition frmAddBayarPiutang
    Case 12: frmTransNotaDebet.Show
            SetFormPosition frmTransNotaDebet
    Case 14: frmTransNotaKredit.Show
            SetFormPosition frmTransNotaKredit
    Case 20:            'Kas Masuk
'        Load frmTransKasBank
        frmTransKasBank.Show
        SetFormPosition frmTransKasBank
        frmTransKasBank.Tipe = "KM"
        frmTransKasBank.Caption = "Kas Masuk"
        frmTransKasBank.lblKasBank = "Acc. Kas"
    Case 30:            'Kas Keluar
        frmTransKasBank.Show
        SetFormPosition frmTransKasBank
        frmTransKasBank.Tipe = "KK"
        frmTransKasBank.Caption = "Kas Keluar"
        frmTransKasBank.lblKasBank = "Acc. Kas"
    Case 40:            'Bank Masuk
        frmTransKasBank.Show
        SetFormPosition frmTransKasBank
        frmTransKasBank.Tipe = "BM"
        frmTransKasBank.Caption = "Bank Masuk"
        frmTransKasBank.lblKasBank = "Acc. Bank"
    Case 50:            'Bank Keluar
        frmTransKasBank.Show
        SetFormPosition frmTransKasBank
        frmTransKasBank.Tipe = "BK"
        frmTransKasBank.Caption = "Bank Keluar"
        frmTransKasBank.lblKasBank = "Acc. Bank"
    Case 53:
        frmAddPencairanGiro.tipe_giro = "masuk"
        frmAddPencairanGiro.Caption = "Pencairan Giro Masuk"
        frmAddPencairanGiro.Show
    Case 55:
        frmAddPencairanGiro.tipe_giro = "keluar"
        frmAddPencairanGiro.Caption = "Pencairan Giro Keluar"
        frmAddPencairanGiro.Show
    Case 57:
        frmTransAP.Show
        SetFormPosition frmTransAP
    Case 58:
        frmTransAR.Show
        SetFormPosition frmTransAR
    Case 60:
        frmTransJurnal.Show
        SetFormPosition frmTransJurnal
    Case 65:
        frmTransBeliCekGiro.Show
        SetFormPosition frmTransBeliCekGiro
    Case 66:
        frmTransTransport.Show vbModal
    Case 67:
        frmTransManol.Show vbModal
    Case 70:
        frmLaporanA.filename = "\Laporan Jurnal"
        frmLaporanA.Caption = "Laporan Jurnal Harian"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
'        frmSearch.connstr = strcon
'        frmSearch.query = "select no_transaksi,nama,debet,kredit from t_jurnald d inner join ms_coa m on d.kode_acc=m.kode_acc order by no_jurnal,no_urut"
'        frmSearch.loadgrid frmSearch.query
'        frmSearch.Show vbModal
    Case 80:
        frmLaporanA.filename = "\Laporan Hutang"
        frmLaporanA.Caption = "Laporan Hutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 90:
        frmLaporanA.filename = "\Laporan Pembayaran Hutang"
        frmLaporanA.Caption = "Laporan Pembayaran Hutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 100:
        frmLaporanA.filename = "\Laporan Piutang"
        frmLaporanA.Caption = "Laporan Piutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 110:
        frmLaporanA.filename = "\Laporan Penerimaan Piutang"
        frmLaporanA.Caption = "Laporan Penerimaan Piutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 112:
        frmLaporanA.filename = "\Laporan Outstanding BG"
        frmLaporanA.Caption = "Laporan Outstanding BG"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 120:
        frmLaporanA.filename = "\Laporan History Harga Pokok"
        frmLaporanA.Caption = "History Harga Pokok"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 130:
        frmLaporanA.filename = "\Laporan Buku Besar"
        frmLaporanA.Caption = "Buku Besar"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 140:
        frmLaporanA.filename = "\Neraca"
        frmLaporanA.Caption = "Neraca"
        frmLaporanA.Show
        SetFormPosition frmLaporanA

    Case 150:
        frmLaporanA.filename = "\LabaRugi"
        frmLaporanA.Caption = "Laba Rugi"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
        frmLaporanA.FrLabaRugi.Visible = True
    Case 160:
        frmLaporanA.filename = "\Laporan Kartu Hutang"
        frmLaporanA.Caption = "Kartu Hutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 170:
        frmLaporanA.filename = "\Laporan Kartu Piutang"
        frmLaporanA.Caption = "Kartu Piutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    End Select
End Sub


Private Sub mnuMain_Click(Index As Integer)
    If Index = 90 Then
    If MsgBox("Keluar dari aplikasi ?", vbYesNo + vbQuestion) = vbYes Then
        Unload Me
    End If

    End If
End Sub

Private Sub mnuMaster_Click(Index As Integer)
    Select Case Index
    Case 0:
        frmMasterParentAcc.Show
        SetFormPosition frmMasterParentAcc
    Case 10:
        frmMasterSubGrupAcc.Show
        SetFormPosition frmMasterSubGrupAcc
    Case 20:
        frmMasterAcc.Show
        SetFormPosition frmMasterAcc
    Case 30:
        frmMasterBarang.Show
        SetFormPosition frmMasterBarang
    Case 40:
        frmAktivasiMsBarang.Show
        SetFormPosition frmAktivasiMsBarang
    Case 50:
        frmHPP.Show
        SetFormPosition frmHPP
    Case 60:
        frmMasterProses.Show
        SetFormPosition frmMasterProses
    Case 70:
        frmMasterKaryawan.Show
        SetFormPosition frmMasterKaryawan
    Case 80:
        frmMasterCustomer.Show
        SetFormPosition frmMasterCustomer
    Case 90:
        frmMasterSupplier.Show
        SetFormPosition frmMasterSupplier
    Case 92:
        frmMasterToko.Show
        SetFormPosition frmMasterToko
    Case 95:
        frmMasterContact.Show
        SetFormPosition frmMasterContact
    Case 100:
        frmMasterBank.Show
        SetFormPosition frmMasterBank
    Case 101:
        frmMasterEkspedisi.Show
        SetFormPosition frmMasterEkspedisi
    Case 105:
        frmMasterGroupGudang.Show
        SetFormPosition frmMasterGroupGudang

    Case 110:
        frmMasterMesin.Show
        SetFormPosition frmMasterMesin
    Case 115:
        frmMasterBiayaProduksi.Show
        SetFormPosition frmMasterBiayaProduksi
    End Select
    
End Sub

Private Sub mnuPembelian_Click(Index As Integer)
Select Case Index
    Case 0: frmAddRequestOP.Show
            SetFormPosition frmAddRequestOP
    Case 5: frmAddPO.Show
            SetFormPosition frmAddPO
    Case 7: frmTransAngkut.Show
            SetFormPosition frmTransAngkut
    Case 10: frmAddPenerimaanbarang.Show
            SetFormPosition frmAddPenerimaanbarang
    Case 20: frmAddQCPenerimaanbarang1.Show
            SetFormPosition frmAddQCPenerimaanbarang1
    Case 25: frmAddQCPenerimaanbarang2.Show
            SetFormPosition frmAddQCPenerimaanbarang2
    Case 30: frmAddPembelian.Show
            SetFormPosition frmAddPembelian
    Case 40: frmTransReturBeli.Show
            SetFormPosition frmTransReturBeli
    Case 50: frmLaporanA.filename = "\Laporan Request Pembelian"
            frmLaporanA.Caption = "Laporan Request Pembelian"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 55: frmLaporanA.filename = "\Laporan Order Pembelian"
            frmLaporanA.Caption = "Laporan Order Pembelian"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 60: frmLaporanA.filename = "\Laporan Penerimaan Barang"
            frmLaporanA.Caption = "Laporan Penerimaan Barang"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 65: frmLaporanA.filename = "\Laporan Pembelian"
            frmLaporanA.Caption = "Laporan Pembelian"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 70: frmLaporanA.filename = "\Laporan Retur Pembelian"
            frmLaporanA.Caption = "Laporan Retur Pembelian"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
'    End Select
    End Select
End Sub

Private Sub mnuPenjualan_Click(Index As Integer)
Select Case Index
    Case 0:
        frmAddRequestSO.Show
        SetFormPosition frmAddRequestSO
    Case 5:
        frmAddSO.Show
        SetFormPosition frmAddSO
    Case 7: frmTransAngkut.Show
            SetFormPosition frmTransAngkut
    Case 10:
        frmAddSuratJalan.suratjalan = False
        frmAddSuratJalan.Caption = "Pengeluaran Barang"
        frmAddSuratJalan.Show
        SetFormPosition frmAddSuratJalan
    Case 15:
        frmAddSuratJalan.suratjalan = True
        frmAddSuratJalan.Caption = "Surat Jalan"
        frmAddSuratJalan.Show
        SetFormPosition frmAddSuratJalan
    Case 18:
        With frmAddReturSuratJalan
        .suratjalan = True
        .Caption = "Surat Jalan"
        .Show
        End With
        SetFormPosition frmAddReturSuratJalan
    Case 20:
        frmAddPenjualan.Show
        SetFormPosition frmAddPenjualan
    Case 25:
        frmTransReturJual.Show
        SetFormPosition frmTransReturJual
    Case 30:
        frmUbahStatus.Show
        SetFormPosition frmUbahStatus
    
    Case 50:
        frmTransKonsinyasiTambah.Show
        SetFormPosition frmTransKonsinyasiTambah
    Case 60:
        frmTransKonsinyasiRetur.Show
        SetFormPosition frmTransKonsinyasiRetur
    Case 70:
        frmTransKonsinyasiJual.Show
        SetFormPosition frmTransKonsinyasiJual
    Case 80:
        frmSearch.connstr = strcon
        Set frmSearch.frm = Nothing
        frmSearch.query = "select * from stock_toko"
        frmSearch.nmctrl = ""
        frmSearch.nmctrl2 = ""
        frmSearch.keyIni = "vwStock"
        
        frmSearch.loadgrid frmSearch.query
        frmSearch.Show vbModal
        SetFormPosition frmSearch
    Case 90: frmLaporanA.filename = "\Laporan Request Penjualan"
            frmLaporanA.Caption = "Laporan Request Penjualan"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 95: frmLaporanA.filename = "\Laporan Order Penjualan"
            frmLaporanA.Caption = "Laporan Order Penjualan"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 100: frmLaporanA.filename = "\Laporan Pengeluaran Barang"
            frmLaporanA.Caption = "Laporan Pengeluaran Barang"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 105: frmLaporanA.filename = "\Laporan Penjualan"
            frmLaporanA.Caption = "Laporan Penjualan"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 110: frmLaporanA.filename = "\Laporan Retur Penjualan"
            frmLaporanA.Caption = "Laporan Retur Penjualan"
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 120: frmLaporanA.filename = "\laporan angkutan"
            frmLaporanA.Caption = mnuPenjualan(Index).Caption
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    Case 130: frmLaporanA.filename = "\laporan angkutan lain"
            frmLaporanA.Caption = mnuPenjualan(Index).Caption
            frmLaporanA.Show
            SetFormPosition frmLaporanA
End Select
End Sub

Private Sub mnuProduksi_Click(Index As Integer)
    Select Case Index
    Case 0:
        frmTransProduksi.Tipe = 1
        frmTransProduksi.Show
        
        SetFormPosition frmTransProduksi
    Case 10:
        frmTransProduksi.Tipe = 2
        frmTransProduksi.Show
        SetFormPosition frmTransProduksi
    Case 20: frmKalkulator.Show vbModal
    
    Case 30: frmLaporanA.filename = "\Laporan Produksi"
            frmLaporanA.Caption = "Laporan Produksi"
            frmLaporanA.FrBahanHasil.Visible = True
            frmLaporanA.Show
            SetFormPosition frmLaporanA
    End Select
End Sub

Private Sub mnuSaldo_Click(Index As Integer)
Select Case Index
    Case 0:
        frmMasterSA.filename = "Hutang"
        frmMasterSA.Caption = "Setting Saldo Awal Hutang"
        frmMasterSA.Show
        SetFormPosition frmMasterSA
    Case 10:
        frmMasterSA.filename = "Piutang"
        frmMasterSA.Caption = "Setting Saldo Awal Piutang"
        frmMasterSA.Show
        SetFormPosition frmMasterSA
    Case 20:
        frmMasterSA.filename = "Kertas"
        frmMasterSAStock.Caption = "Setting Saldo Awal Stock Kertas"
        frmMasterSAStock.Show
        SetFormPosition frmMasterSAStock
    
End Select
End Sub

Private Sub mnuReminder_Click(Index As Integer)
    Select Case Index
    Case 0: frmSettingPesan.Show
        SetFormPosition frmSettingPesan
    Case 10: frmAlert.Show
        SetFormPosition frmAlert
    Case 20: frmgeneratemesinpengawas.Show
        SetFormPosition frmgeneratemesinpengawas
    Case 30:
        frmLaporanA.filename = "\Laporan Penilaian Karyawan"
        frmLaporanA.Caption = "Laporan Penilaian Karyawan"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    End Select
End Sub

Private Sub mnuSettingAcc_Click(Index As Integer)
    Select Case Index
    Case 10:
        frmSettingAcc.filename = "Bahan"
        frmSettingAcc.Caption = "Setting Perkiraan Bahan"
        frmSettingAcc.Show
        SetFormPosition frmSettingAcc
    Case 20:
        frmSettingAcc.filename = "Supplier"
        frmSettingAcc.Caption = "Setting Perkiraan Supplier"
        frmSettingAcc.Show
        SetFormPosition frmSettingAcc
    Case 30:
        frmSettingAcc.filename = "Customer"
        frmSettingAcc.Caption = "Setting Perkiraan Customer"
        frmSettingAcc.Show
        SetFormPosition frmSettingAcc
    
    End Select
End Sub

Private Sub mnuStock_Click(Index As Integer)
    Select Case Index
    Case 0: frmTransStockOpname.Show
            SetFormPosition frmTransStockOpname
'    Case 30:
'        frmTransKoreksiStockKertas.Show
'        SetFormPosition frmTransKoreksiStockKertas
    Case 55:
        frmTransStockMutasi.Show
        SetFormPosition frmTransStockMutasi
    Case 60: 'info stock kertas
        frmSearch.connstr = strcon
        Set frmSearch.frm = Nothing
        frmSearch.query = "select * from vw_Stock"
        frmSearch.nmctrl = ""
        frmSearch.nmctrl2 = ""
        frmSearch.keyIni = "vwStock"
        
        frmSearch.loadgrid frmSearch.query
        frmSearch.Show vbModal
        SetFormPosition frmSearch
    Case 100: 'info stock kertas
        frmSearch.connstr = strcon
        Set frmSearch.frm = Nothing
        frmSearch.query = "select * from vw_Stockgabungan"
        frmSearch.nmctrl = ""
        frmSearch.nmctrl2 = ""
        frmSearch.keyIni = "vwStock"
        
        frmSearch.loadgrid frmSearch.query
        frmSearch.Show vbModal
        SetFormPosition frmSearch
    Case 122:
        frmLaporanA.filename = "\Laporan Stock Kertas"
        frmLaporanA.Caption = "Stock"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 125:
        frmLaporanA.filename = "\Laporan Stock Per Tanggal"
        frmLaporanA.Caption = "Stock Per Tanggal"
        frmLaporanA.Show
        frmLaporanA.DTSampai.Visible = False
        frmLaporanA.Label6.Visible = False
        SetFormPosition frmLaporanA
    Case 130:
        frmLaporanA.filename = "\Laporan Kartu Stock"
        frmLaporanA.Caption = "Kartu Stock Kertas"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 160:
        frmLaporanA.filename = "\Laporan Mutasi Stock Kertas"
        frmLaporanA.Caption = "Laporan Mutasi Stock Kertas"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    End Select
End Sub





Private Sub mnuUtility_Click(Index As Integer)
    Select Case Index
    Case 0: frmSetting.Show
        SetFormPosition frmSetting
    Case 5: frmSettingCOA.Show
        SetFormPosition frmSettingCOA
    Case 10: frmMasterSatuan.Show
        SetFormPosition frmMasterSatuan
    Case 15: frmMasterKategoriCustomer.Show
        SetFormPosition frmMasterKategoriCustomer
    Case 17: frmMasterKategoriSupplier.Show
        SetFormPosition frmMasterKategoriSupplier
    Case 20: frmMasterKategori.Show
        SetFormPosition frmMasterKategori
    Case 30: frmMasterSubKategori.Show
        SetFormPosition frmMasterSubKategori

'    Case 35: frmMasterWarna.Show
'        SetFormPosition frmMasterWarna
   
    
    Case 40: frmDataPerusahaan.Show
        SetFormPosition frmDataPerusahaan
    Case 50: frmTutupBukuBulanan.Show
        SetFormPosition frmTutupBukuBulanan
    Case 60:
        conn.ConnectionString = strcon
        conn.Open

        rs.Open "Select * from mutasi_coa", conn
        If rs.EOF Then
            rs.Close
            conn.Close
            MsgBox "Belum ada proses tutup buku !", vbExclamation
            Exit Sub
        End If

        rs.Close
        conn.Close

        frmBatalTutupBukuBulanan.Show
        SetFormPosition frmBatalTutupBukuBulanan

    Case 70:
        frmGantiPassword.Show
        SetFormPosition frmGantiPassword
    Case 80:
        frmNewUser.Show
        SetFormPosition frmNewUser
    Case 90:
        frmMasterUserGroup.Show
        SetFormPosition frmMasterUserGroup
    Case 100:
        frmBackupDB.Show
        SetFormPosition frmBackupDB
    Case 110:

    Case 120:
        frmTransPostingOtomatis.Show
    Case 130:
        frmTransPostingManual.Show vbModal
    Case 140:
        frmProsesHPP.Show
    Case 150:
        frmTransUnposting.Show
    Case 160:
        frmAutomaticPosting.Show vbModal
    Case 170:
        frmCutOff.Show
    End Select
End Sub

Sub TampilData()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    
    rs.Open "select nomer,dari,pesan,tanggal_alert from alert where  status_popup='0' and (userid='" & User & "' or userid='') " & _
              "and convert(varchar(16),tanggal_alert,120)<= '" & Format(Now, "YYYY-MM-DD HH:MM") & "' ", conn
    
    While Not rs.EOF
        Beep
        
        frmReminder.lblNomer.Caption = rs("nomer")
        frmReminder.lblUser.Caption = rs("dari")
        frmReminder.LblPesan.Caption = rs("pesan")
        frmReminder.DTPicker1.value = Format(rs("tanggal_alert"), "dd-MM-yyyy hh:mm")
        conn.Execute "update alert set status_popup='1' where nomer='" & rs("nomer") & "' "
        frmReminder.Show vbModal
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Public Sub UpdateStatusReminder()
    conn.Open strcon
    conn.BeginTrans
    conn.Execute "update alert set status='0',status_popup='0',tanggal_alert=dateadd(month,1,tanggal_alert) " & _
                  "where  status='0' and bulanan='y' " & _
                  "and convert(varchar(16),dateadd(month,1,tanggal_alert),120)<= '" & Format(Now, "YYYY-MM-DD HH:MM") & "' "
    conn.CommitTrans
    conn.Close
End Sub

Private Sub Timer1_Timer()
    TampilData
    If Format(Now, "HH:mm:ss") < "14:00:00" Then Exit Sub
    Dim sudah_menilai As Boolean
    timer = timer + 1
    If timer < 10 Then Exit Sub
    conn.Open strcon
    rs.Open "select * from penilaian_karyawan where userid = '" & User & "' and convert(varchar,tanggal,112) = '" & Format(Now, "yyyyMMdd") & "'", conn
    If rs.EOF Then
        sudah_menilai = False
    Else
        sudah_menilai = True
    End If
    rs.Close
    conn.Close
    If sudah_menilai = False And frmPenilaianKaryawan.Visible = False Then
        frmPenilaianKaryawan.Show vbModal
    End If
    timer = 0
End Sub
