Attribute VB_Name = "modProcedure"
Public Type POINTAPI
        X As Long
        y As Long
End Type
Public Declare Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal y As Long) As Long
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
'Public Sub main()
'    Dim filename As String
'
'    Open filename For Input As #1
'    Input #1, strconn
'    Close #1
'    filename = Left(filename, Len(filename) - 6)
'
'End Sub
Public strPeriodeLaba As String
Public katakunci, kunci As String
Public gBulanAwal As String
Public gTahunAwal As String
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
        (ByVal hwnd As Long, _
        ByVal wMsg As Long, _
        ByVal wParam As Long, _
        lParam As Any) As Long

'declare constants
    Public Const CB_SHOWDROPDOWN = &H14F


Public Sub DropList(WhichCombo As Control, DropItDown As Boolean)
    On Error Resume Next

    Dim vTmpLng As Long
    vTmpLng = SendMessage(WhichCombo.hwnd, CB_SHOWDROPDOWN, _
                  ByVal Abs(CLng(DropItDown)), ByVal CLng(0))
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' U/ mambatasi input hanya bisa numeric tidak bisa huruf
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub NumberOnly(ByRef key As Integer)
    Select Case key
        Case Asc("0") To Asc("9"):
        Case 8:
        Case 13:
        Case 46:
        Case Else:
            key = 0
    End Select
    
End Sub
Function FormatEngineering(Number As Variant, Optional DecimalPlaces As Long = 1) As String
FormatEngineering = Format(Split(Format(Number, "0.0#############E+0"), _
    "E")(0) * 10 ^ (Split(Format(Number, _
    "0.0#############E+0"), "E")(1) - 3 * Int( _
    Split(Format(Number, "0.0#############E+0"), _
    "E")(1) / 3)), "0." & String(DecimalPlaces, _
    "0")) & "E" & Format(3 * Int(Split(Format( _
    Number, "0.0#############E+0"), "E")(1) / _
    3), "+0;-0")
End Function
Public Sub SetComboText(ByVal teks As String, ByRef combo As ComboBox)
    Dim Index As Integer

    For Index = 0 To combo.ListCount - 1
        If combo.List(Index) = teks Then
            combo.ListIndex = Index
            Exit For
        End If
    Next Index
End Sub
Public Sub SetComboTextLeft(ByVal teks As String, ByRef combo As ComboBox)
    Dim Index As Integer

    For Index = 0 To combo.ListCount - 1
        If Left(combo.List(Index), Len(teks)) = teks Then
            combo.ListIndex = Index
            Exit For
        End If
    Next Index
End Sub
Public Sub SetComboTextRight(ByVal teks As String, ByRef combo As ComboBox)
    Dim Index As Integer

    For Index = 0 To combo.ListCount - 1
        If Right(combo.List(Index), Len(teks)) = teks Then
            combo.ListIndex = Index
            Exit For
        End If
    Next Index
End Sub
Public Sub ClearText(frm As Form, NumberOfField As Integer)
    Dim i As Integer
    Dim ControlNm As String
    
    For i = 0 To NumberOfField
        ControlNm = "Field" & CStr(i)
        With frm.Controls(ControlNm)
            Select Case .Tag
                Case "Text": .text = ""
                Case "Combo": Call SetComboText("", frm.Controls(ControlNm))
                Case "Label": .Caption = ""
                Case "Option": .value = False
                Case "Number": .Caption = "0"
                Case "Date": .value = Date
            End Select
        End With
    Next
    
End Sub

Public Sub SetFormPosition(frm As Form)
    frm.Left = 0
    frm.top = 0
End Sub

' Kalau Sub Form dibuka(Sts = false), Main Form & MDI Form di-disable
Public Sub EnableForm(frm As Form, Sts As Boolean)
    If Sts = True Then
        frm.Enabled = True
'        frmMDI.mnuMain(1).Enabled = True
    Else
        frm.Enabled = False
'        frmMDI.mnuMain(1).Enabled = False
    End If
End Sub


Public Sub Get_Data(Form As Form, Start_Array As Single, End_Array As Single, Table_Nm As String, Condition As String)
    Dim strSQL As String
    Dim i As Single
    Dim fields As String
    Dim rs1 As New ADODB.Recordset
    
    For i = Start_Array To End_Array
        fields = fields & Form.Label(i).Tag & ","
    Next i
    fields = Left(fields, Len(fields) - 1)
    
    strSQL = "SELECT " & fields & " FROM " & Table_Nm
    strSQL = strSQL & " WHERE " & Condition
    rs1.Open strSQL, conn
    If Not rs1.EOF Then
        For i = Start_Array To End_Array
            Form.Label(i).Caption = rs1.fields(i - 1)
        Next i
    Else
        For i = Start_Array To End_Array
            Form.Label(i).Caption = ""
        Next i
    End If
    rs1.Close
End Sub


Public Sub activateMDI()
On Error GoTo err
Dim rs1 As New ADODB.Recordset
    If LCase(login) = "sa" Then
        rs1.Open "select ms_menu.nm_menu,ms_menu.idx,isnull(access,0),ms_menu.tipe from ms_menu left join access on ms_menu.nm_menu=access.nm_menu and ms_menu.idx=access.idx and userid='" & login & "' order by ms_menu.nm_menu", conn
        While Not rs1.EOF
            frmMDI.Controls(rs1(0))(rs1(1)).Visible = True
            frmMDI.Controls(rs1(0))(rs1(1)).Enabled = True
            rs1.MoveNext
        Wend
        rs1.Close
    Else
        rs1.Open "select ms_menu.nm_menu,ms_menu.idx,isnull(access,0),ms_menu.tipe from ms_menu left join access on ms_menu.nm_menu=access.nm_menu and ms_menu.idx=access.idx and userid='" & login & "' order by ms_menu.nm_menu", conn
        While Not rs1.EOF
            If rs1(2) = "1" Then
                'If rs1(3) = "0" Then
                frmMDI.Controls(rs1(0))(rs1(1)).Visible = True
                'Else
                'frmMDI.Controls(rs1(0))(rs1(1)).Enabled = True
                'End If
            Else
                'If rs1(3) = "0" Then
                frmMDI.Controls(rs1(0))(rs1(1)).Visible = False
                'Else
                'frmMDI.Controls(rs1(0))(rs1(1)).Enabled = False
                'End If
            End If
        rs1.MoveNext
        Wend
        rs1.Close
    End If
    Exit Sub
err:
    Resume Next
End Sub
Public Sub deactivateMDI()
On Error GoTo err
Dim rs1 As New ADODB.Recordset
rs1.Open "select ms_menu.nm_menu,ms_menu.idx,isnull(access,0),ms_menu.tipe from ms_menu left join access on ms_menu.nm_menu=access.nm_menu and userid='" & login & "' order by ms_menu.nm_menu", conn
While Not rs1.EOF
    'If rs1(3) = "0" Then
        frmMDI.Controls(rs1(0))(CInt(rs1(1))).Visible = False
    'Else
    '    frmMDI.Controls(rs1(0))(CInt(rs1(1))).Enabled = False
    'End If
rs1.MoveNext
Wend
rs1.Close
Exit Sub
err:
    Resume Next
End Sub
Public Function tambah_data2(table_name As String, field() As String, nilai() As String) As String
Dim query As String
    query = "insert into " & table_name & " ("
    For i = 0 To UBound(field) - 1
        query = query & "[" & field(i) & "],"
    Next
    query = Left(query, Len(query) - 1) & ") values ("
    For i = 0 To UBound(nilai) - 1
        query = query & "'" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & ")"
    
    tambah_data2 = query
    'MsgBox query
End Function
Public Sub tambah_data(table_name As String, field() As String, nilai() As String)
Dim query As String
    query = "insert into " & table_name & " ("
    For i = 0 To UBound(field) - 1
        query = query & field(i) & ","
    Next
    query = Left(query, Len(query) - 1) & ") values ("
    For i = 0 To UBound(nilai) - 1
        query = query & "'" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & ")"
    conn.Execute query
    'MsgBox query
End Sub
Public Sub tambah_data3(table_name As String, field() As String, nilai() As String, conn2 As ADODB.Connection)
Dim query As String
    query = "insert into " & table_name & " ("
    For i = 0 To UBound(field) - 1
        query = query & field(i) & ","
    Next
    query = Left(query, Len(query) - 1) & ") values ("
    For i = 0 To UBound(nilai) - 1
        query = query & "'" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & ")"
    conn2.Execute query
    'MsgBox query
End Sub

Public Sub update_data(table_name As String, field() As String, nilai() As String, where As String, cn As ADODB.Connection)
Dim query As String
    query = "update " & table_name & " set "
    For i = 0 To UBound(field) - 1
        query = query & field(i) & "='" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & " where " & where
    cn.Execute query
    'MsgBox query
End Sub

Public Sub exportdataT(id As String, modul As String, namafile As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim A As New Scripting.FileSystemObject
Dim query, from, output As String
Dim field(), Tipe() As String
Dim panjang(), counter As Byte
    'If a.FileExists(namafile) Then a.DeleteFile namafile
    
    conn.ConnectionString = strcon
    conn.Open
    A.CreateTextFile namafile, True
    query = ""
    ReDim field(1)
    ReDim Tipe(1)
    ReDim panjang(1)
    counter = 1
    rs.Open "select * from mapping_export where modul='" & modul & "' order by urutan", conn
    While Not rs.EOF
        ReDim Preserve field(counter)
        ReDim Preserve Tipe(counter)
        ReDim Preserve panjang(counter)
        field(counter - 1) = rs(2)
        Tipe(counter - 1) = rs(3)
        panjang(counter - 1) = rs(4)
        query = query & rs(2) & ","
        If from = "" Then from = rs(1)
        rs.MoveNext
        counter = counter + 1
    Wend
    rs.Close
    query = "select " & Left(query, Len(query) - 1) & " from " & from & " where id='" & id & "'"

   Open namafile For Output As #1
    rs.Open query, conn
    While Not rs.EOF
        output = modul & ";"
        For i = 0 To rs.fields.Count - 1
            Select Case LCase(Tipe(i))
            Case "varchar":
                output = output & rs(i) & Space(CInt(panjang(i)) - Len(rs(i))) & ";"
            Case "datetime":
                output = output & Format(rs(i), "MM/dd/yy") & Space(CInt(panjang(i)) - Len(Format(rs(i), "MM/dd/yy"))) & ";"
            Case "numeric":
                output = output & Format(rs(i), "#,##0.00") & Space(CInt(panjang(i)) - Len(Format(rs(i), "#,##0.00"))) & ";"
            End Select
        Next
        Print #1, output
        rs.MoveNext
    Wend
    rs.Close
    Close #1
End Sub

Public Sub ubah_hpp(harga As Currency, id As String, Tipe As String, kode_barang As String)
On Error GoTo err
Dim mode As Byte
Dim conn As New ADODB.Connection
    mode = 0
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    mode = 1
    If Not IsNumeric(harga) Then harga = "0"
    conn.Execute "update hpp_fifo set harga_beli='" & harga & "' where tipe='" & Tipe & "' and no_trans='" & id & "' and kode_barang='" & kode_barang & "'"
    conn.Execute "update t_penjualand_hpp set hpp='" & harga & "' where tipe='" & Tipe & "' and no_trans='" & id & "' and kode_barang='" & kode_barang & "'"
    conn.Execute "update t_returbelid_hpp set hpp='" & harga & "' where tipe='" & Tipe & "' and no_trans='" & id & "' and kode_barang='" & kode_barang & "'"
    conn.Execute "update t_stockopnamed_hpp set hpp='" & harga & "' where tipe='" & Tipe & "' and no_trans='" & id & "' and kode_barang='" & kode_barang & "'"
    conn.Execute "update t_transferoutd_hpp set hpp='" & harga & "' where tipe='" & Tipe & "' and no_trans='" & id & "' and kode_barang='" & kode_barang & "'"
    conn.CommitTrans
    'transfer
    rs.Open "select * from t_transferoutd_hpp d inner join t_transferinh h on d.id=h.no_ref where tipe='" & Tipe & "' and no_trans='" & id & "' and kode_barang='" & kode_barang & "' and warna='" & warna & "' and ukuran='" & ukuran & "'", conn
    While Not rs.EOF
        ubah_hpp harga, rs("h.id"), "T", kode_barang
        rs.MoveNext
    Wend
    rs.Close
    mode = 0
    conn.Close
    
    Exit Sub
err:
    MsgBox err.Description
    If mode = 1 Then conn.RollbackTrans
    DropConnection
End Sub

Public Sub generate_dailystock(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

conn.Open strcon
conn.Execute "delete from tmp_stockharian where tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"

    conn.Execute "insert into tmp_stockharian (tahun,bulan,tanggal,[kode],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "','" & Format(tanggal, "MM") & "','" & Format(tanggal, "dd") & "',ms_bahan.[kode],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')),0, " & _
    "(select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' and [kode]=ms_bahan.[kode])),0," & _
    "(select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' and [kode]=ms_bahan.[kode])) as stockawal from ms_bahan"
'rs.Open "select * from tmp_stockharian", conn
'While Not rs.EOF
'    conn.Execute "update tmp_stockharian set beli=(select sum(qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]='" & rs(0) & "' and h.tanggal='" & Format(tanggal, "yyyy/MM/dd") & "') where  tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"
'    rs.MoveNext
'Wend
'rs.Close
'conn.Close
End Sub
Public Sub generate_dailyreport(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.Open strcon
    conn.Execute "delete from tmp_lapharian "
    rs.Open "select distinct userid from t_bayarpiutangh where format(tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "'", conn
    While Not rs.EOF
    conn.Execute "insert into tmp_lapharian (tanggal,[modal awal],tunai,kartu,retur,userid,keuntungan) select #" & Format(tanggal, "yyyy/MM/dd") & "#,0," & _
    "sum([jumlah_bayar]) as t,0 as k,(select iif(isnull(sum(jumlah)),0,sum(jumlah)) from t_bayarpiutang_retur inner join t_bayarpiutangh on t_bayarpiutang_retur.id=t_bayarpiutangh.id where format(tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and userid='" & userid & "') ,userid,(select sum(iif(tipe=1,(qty*(harga-disc-disctotal-hpp)),(qty*(harga-hpp)*-1))) from q_omset qd where format(qd.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and qd.userid='" & userid & "') from t_bayarpiutangh p where format(p.tanggal,'yyyy/MM/dd')='" & Format(tanggal, "yyyy/MM/dd") & "' and p.userid='" & userid & "'  group by userid"
    rs.MoveNext
    Wend
End Sub
Public Sub generate_RL(tanggal1 As Date, tanggal2 As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.Open strcon
    conn.Execute "delete from tmp_RL"
    rs.Open "select sum(qty*(harga-disc-disc_total)) from q_juald d inner join t_penjualanh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "' and h.bayar='1'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,description,debit,kredit) values (1,'Penjualan','" & IIf(IsNull(rs(0)), 0, rs(0)) & "',0)"
    End If
    rs.Close
    rs.Open "select sum(qty*(harga_jual)) from t_returjuald d inner join t_returjualh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "' and status='1'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,description,debit,kredit) values (2,'Retur Jual',0,'" & IIf(IsNull(rs(0)), 0, rs(0)) & "')"
    End If
    rs.Close
    rs.Open "select sum(qty*hpp) from q_juald d inner join t_penjualanh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "' and h.bayar='1'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,description,debit,kredit) values (3,'HPP',0,'" & IIf(IsNull(rs(0)), 0, rs(0)) & "')"
    End If
    rs.Close
    rs.Open "select sum(qty*hpp) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,description,debit,kredit) values (4,'Pengambilan Barang',0,'" & IIf(IsNull(rs(0)), 0, rs(0)) & "')"
    End If
    rs.Close
    rs.Open "select sum(selisih*hpp) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "' and status='1'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,description,debit,kredit) values (5,'Stock Opname','" & IIf(IsNull(rs(0)), 0, IIf(rs(0) < 0, 0, rs(0))) & "','" & IIf(IsNull(rs(0)), 0, IIf(rs(0) > 0, 0, rs(0))) & "')"
    End If
    rs.Close
End Sub

Public Sub generate_monthlystock(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.Open strcon
    conn.Execute "delete from tmp_stockbulanan where tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "'"
    conn.Execute "insert into tmp_stockbulanan (tahun,bulan,[kode],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "','" & Format(tanggal, "MM") & "',ms_bahan.[kode],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(h.tanggal,'MM/yyyy')<'" & Format(tanggal, "MM/yyyy") & "' and [kode]=ms_bahan.[kode])),0," & _
    "(select sum(qty) from q_mutasistock q where format(h.tanggal,'MM/yyyy')<'" & Format(tanggal, "MM/yyyy") & "' and [kode]=ms_bahan.[kode])) as stockawal from ms_bahan"
'rs.Open "select * from ms_bahan", conn
'While Not rs.EOF
    'conn.Execute "update tmp_stockharian set beli=(select sum(qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]='" & rs(0) & "' and h.tanggal='" & Format(tanggal, "MM/yyyy") & "') where  tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"
'    rs.MoveNext
'Wend
'rs.Close
conn.Close
End Sub
Public Sub generate_yearlystock(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.Open strcon
    conn.Execute "delete from tmp_stocktahunan where tahun='" & Format(tanggal, "yyyy") & "'"
'rs.Open "select * from ms_bahan", conn
'While Not rs.EOF
    conn.Execute "insert into tmp_stocktahunan (tahun,[kode],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "',ms_bahan.[kode],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]=ms_bahan.[kode] and format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode]=ms_bahan.[kode] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy')<'" & Format(tanggal, "yyyy") & "' and [kode]=ms_bahan.[kode])),0," & _
    "(select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy')<'" & Format(tanggal, "yyyy") & "' and [kode]=ms_bahan.[kode])) as stockawal from ms_bahan"
    'conn.Execute "update tmp_stockharian set beli=(select sum(qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode]='" & rs(0) & "' and h.tanggal='" & Format(tanggal, "yyyy") & "') where  tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"
'    rs.MoveNext
'Wend
'rs.Close
conn.Close
End Sub
Public Sub genomset(tanggaldari As Date, tanggalsampai As Date)
Dim conn As New ADODB.Connection

conn.Open strcon
conn.Execute "delete from tmp_omset"
conn.Execute "insert into tmp_omset select * from q_omset where format(tanggal,'yyyy/MM/dd')>='" & Format(tanggaldari, "yyyy/MM/dd") & "' and format(tanggal,'yyyy/MM/dd')<='" & Format(tanggalsampai, "yyyy/MM/dd") & "'"
conn.Close
End Sub
Public Sub genomsetmonth(tahun As String, bulan As String)
Dim conn As New ADODB.Connection

conn.Open strcon
conn.Execute "delete from tmp_omset"
If bulan <> "" Then
conn.Execute "insert into tmp_omset select * from q_omset where format(tanggal,'MM/yyyy')>='" & bulan & "/" & tahun & "'"
Else
conn.Execute "insert into tmp_omset select * from q_omset where format(tanggal,'yyyy')>='" & tahun & "'"
End If
conn.Close
End Sub

Public Sub generate_kartustock(tanggal As Date, tanggal2 As Date, Optional kategori As String, Optional search As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim StockAwal, stockakhir As Long
Dim Baru As Boolean

conn.Open strcon
conn.Execute "delete from tmp_kartustock"



If kategori <> "" And search <> "" Then
conn.Execute "insert into tmp_kartustock (tipe,gudang,id,tanggal,[kode],masuk,keluar) select * from q_kartustock where format(tanggal,'yyyy/MM/dd')>'" & Format(tanggal - 1, "yyyy/MM/dd") & "' and format(tanggal,'yyyy/MM/dd')<'" & Format(tanggal2 + 1, "yyyy/MM/dd") & "' and " & kategori & "='" & search & "'"
Else
conn.Execute "insert into tmp_kartustock (tipe,gudang,id,tanggal,[kode],masuk,keluar) select * from q_kartustock where format(tanggal,'yyyy/MM/dd')>'" & Format(tanggal - 1, "yyyy/MM/dd") & "' and format(tanggal,'yyyy/MM/dd')<'" & Format(tanggal2 + 1, "yyyy/MM/dd") & "'"
End If
rs.Open "select distinct [kode],gudang from tmp_kartustock", conn

While Not rs.EOF
    StockAwal = 0
    Baru = True
    rs2.Open "select * from tmp_kartustock where [kode]='" & rs(0) & "' and gudang='" & rs(1) & "' order by tanggal desc", conn
    While Not rs2.EOF
        If Baru Then
            stockakhir = getstockakhirkartu(rs2("kode"), rs2("id"), rs2("tanggal"), rs2("gudang"))
        End If
        conn.Execute "update tmp_kartustock set stockawal=" & stockakhir & "+keluar-masuk where  id='" & rs2("id") & "' and [kode]='" & rs2("kode") & "' and gudang='" & rs2("gudang") & "'"
        stockakhir = stockakhir - rs2("masuk") + rs2("keluar")
        Baru = False
        rs2.MoveNext
    Wend
    rs2.Close
    rs.MoveNext
Wend
rs.Close
conn.Close
End Sub

'Public Sub generate_BukuBesar(tanggal As Date, tanggal2 As Date, kode_acc As String)
'Dim conn As New ADODB.Connection
'Dim rs As New ADODB.Recordset
'Dim rs2 As New ADODB.Recordset
'Dim stockawal, stockakhir As Long
'Dim Jenis As String, kategori As String
'Dim i As Long, Saldo As Double
'Dim Acc As String
'Dim Baru As Boolean
'
'conn.Open strcon
'
'conn.BeginTrans
'
'conn.Execute "delete from tmp_bukubesar"
'
'rs.Open "Select kode_acc from ms_coa where kode_acc like '%" & kode_acc & "%'", conn, adOpenDynamic, adLockOptimistic
'While Not rs.EOF
'    rs2.Open "select kategori,Jenis from ms_parentAcc where kode_parent='" & Left(rs("kode_acc"), 3) & "'", conn
'    Jenis = rs2("kategori")
'    kategori = rs2("jenis")
'    rs2.Close
'
'    Select Case Jenis
'    Case "HT", "HPP", "BO", "BNO", "BL"
'        If kategori = "N" Then
'            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
'                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
'                         "sum(d.debet-d.kredit),0,'" & Jenis & "' from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi " & _
'                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 111)<'" & Format(tanggal, "yyyy/MM/dd") & "' " & _
'                         "group by d.kode_acc"
'        Else
'            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
'                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
'                         "sum(d.debet-d.kredit),0,'" & Jenis & "' from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi " & _
'                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 111)<'" & Format(tanggal, "yyyy/MM/dd") & "' AND MONTH(H.TANGGAL)='" & Month(tanggal) & "' " & _
'                         "group by d.kode_acc"
'
'        End If
'
'    Case Else
'        If kategori = "N" Then
'            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
'                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
'                         "0,sum(d.kredit-d.debet),'" & Jenis & "' from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi " & _
'                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 111)<'" & Format(tanggal, "yyyy/MM/dd") & "' " & _
'                         "group by d.kode_acc"
'
'        Else
'            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
'                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
'                         "0,sum(d.kredit-d.debet),'" & Jenis & "' from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi " & _
'                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 111)<'" & Format(tanggal, "yyyy/MM/dd") & "' AND MONTH(H.TANGGAL)='" & Month(tanggal) & "'" & _
'                         "group by d.kode_acc"
'
'        End If
'
'    End Select
'
'    conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis,kode1,kode2,nilai) " & _
'                     "select d.no_transaksi,h.tanggal,d.kode_acc,d.keterangan, " & _
'                     "d.debet,d.kredit,'" & Jenis & "',d.kode1,d.kode2,d.nilai from t_jurnald d inner join t_jurnalh h on h.no_transaksi=d.no_transaksi " & _
'                     "where d.kode_acc='" & rs("kode_acc") & "' " & _
'                     "and CONVERT(VARCHAR(10), h.tanggal, 111)>='" & Format(tanggal, "yyyy/MM/dd") & "'  " & _
'                     "and CONVERT(VARCHAR(10), h.tanggal, 111)<='" & Format(tanggal2, "yyyy/MM/dd") & "'  "
'    rs.MoveNext
'Wend
'rs.Close
'
'If kode_acc <> "" Then
'    rs2.Open "select kategori from ms_parentAcc where kode_parent='" & Left(kode_acc, 3) & "'", conn
'    Jenis = rs2("kategori")
'    rs2.Close
'
'    Select Case Jenis
'    Case "HT", "HPP", "BO", "BNO", "BL"
'        Saldo = 0
'        rs.Open "Select * from tmp_bukubesar order by kode_acc,CONVERT(VARCHAR(10), tanggal, 111),kredit,no_transaksi", conn, adOpenDynamic, adLockOptimistic
'        While Not rs.EOF
'            Saldo = Saldo + (rs!debet) - (rs!kredit)
'            conn.Execute "update tmp_bukubesar set Saldo=" & Replace(Saldo, ",", ".") & " where no_transaksi='" & (rs!no_transaksi) & "' and kode_acc='" & (rs!kode_acc) & "'"
'        rs.MoveNext
'        Wend
'        rs.Close
'    Case Else
'        rs.Open "Select * from tmp_bukubesar order by kode_acc,CONVERT(VARCHAR(10), tanggal, 111),kredit,no_transaksi", conn, adOpenDynamic, adLockOptimistic
'        While Not rs.EOF
'            Saldo = Saldo + (rs!kredit) - (rs!debet)
'            conn.Execute "update tmp_bukubesar set Saldo=" & Replace(Saldo, ",", ".") & " where no_transaksi='" & (rs!no_transaksi) & "' and kode_acc='" & (rs!kode_acc) & "'"
'        rs.MoveNext
'        Wend
'        rs.Close
'    End Select
'End If
'
'conn.CommitTrans
'
'conn.Close
'End Sub

Public Sub generate_BukuBesar(tanggal As Date, tanggal2 As Date, kode_acc As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim StockAwal, stockakhir As Long
Dim Jenis As String, kategori As String
Dim i As Long, Saldo As Double
Dim Acc As String
Dim Baru As Boolean

conn.Open strcon

conn.BeginTrans

conn.Execute "delete from tmp_bukubesar"

rs.Open "Select kode_acc from ms_coa where kode_acc like '%" & kode_acc & "%'", conn, adOpenDynamic, adLockOptimistic
While Not rs.EOF
    rs2.Open "select kategori,Jenis from ms_parentAcc where kode_parent='" & Left(rs("kode_acc"), 3) & "'", conn
    Jenis = rs2("kategori")
    kategori = rs2("jenis")
    rs2.Close
    
    Select Case Jenis
    Case "HT", "HPP", "BO", "BNO", "BL"
        If kategori = "N" Then
            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
                         "sum(d.debet-d.kredit),0,'" & Jenis & "' from t_jurnald_tmp d inner join t_jurnalh_tmp h on h.no_transaksi=d.no_transaksi " & _
                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 112)<'" & Format(tanggal, "yyyyMMdd") & "' " & _
                         "group by d.kode_acc"
        Else
            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
                         "sum(d.debet-d.kredit),0,'" & Jenis & "' from t_jurnald_tmp d inner join t_jurnalh_tmp h on h.no_transaksi=d.no_transaksi " & _
                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 112)<'" & Format(tanggal, "yyyyMMdd") & "' AND MONTH(H.TANGGAL)='" & Month(tanggal) & "' " & _
                         "group by d.kode_acc"

        End If
                     
    Case Else
        If kategori = "N" Then
            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
                         "0,sum(d.kredit-d.debet),'" & Jenis & "' from t_jurnald_tmp d inner join t_jurnalh_tmp h on h.no_transaksi=d.no_transaksi " & _
                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 112)<'" & Format(tanggal, "yyyyMMdd") & "' " & _
                         "group by d.kode_acc"
                        
        Else
            conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis) " & _
                         "select '-','" & Format(tanggal - 1, "yyyy/MM/dd") & "','" & rs("kode_acc") & "','Saldo Awal', " & _
                         "0,sum(d.kredit-d.debet),'" & Jenis & "' from t_jurnald_tmp d inner join t_jurnalh_tmp h on h.no_transaksi=d.no_transaksi " & _
                         "where d.kode_acc='" & rs("kode_acc") & "' and CONVERT(VARCHAR(10), h.tanggal, 112)<'" & Format(tanggal, "yyyyMMd") & "' AND MONTH(H.TANGGAL)='" & Month(tanggal) & "'" & _
                         "group by d.kode_acc"

        End If
                 
    End Select
    
    conn.Execute "insert into tmp_bukubesar (no_transaksi,tanggal,kode_acc,keterangan,debet,kredit,jenis,kode1,kode2,nilai) " & _
                     "select d.no_transaksi,h.tanggal,d.kode_acc,d.keterangan, " & _
                     "d.debet,d.kredit,'" & Jenis & "',d.kode1,d.kode2,d.nilai from t_jurnald_tmp d inner join t_jurnalh_tmp h on h.no_transaksi=d.no_transaksi " & _
                     "where d.kode_acc='" & rs("kode_acc") & "' " & _
                     "and CONVERT(VARCHAR(10), h.tanggal, 112)>='" & Format(tanggal, "yyyyMMdd") & "'  " & _
                     "and CONVERT(VARCHAR(10), h.tanggal, 112)<='" & Format(tanggal2, "yyyyMMdd") & "'  "
    rs.MoveNext
Wend
rs.Close

If kode_acc <> "" Then
    rs2.Open "select kategori from ms_parentAcc where kode_parent='" & Left(kode_acc, 3) & "'", conn
    Jenis = rs2("kategori")
    rs2.Close
    
    Select Case Jenis
    Case "HT", "HPP", "BO", "BNO", "BL"
        Saldo = 0
        rs.Open "Select * from tmp_bukubesar order by kode_acc,CONVERT(VARCHAR(10), tanggal, 111),kredit,no_transaksi,nourut", conn, adOpenDynamic, adLockOptimistic
        While Not rs.EOF
            Saldo = Saldo + (rs!debet) - (rs!kredit)
            conn.Execute "update tmp_bukubesar set Saldo=" & Replace(Saldo, ",", ".") & " where no_transaksi='" & (rs!no_transaksi) & "' and kode_acc='" & (rs!kode_acc) & "'  and nourut='" & (rs!nourut) & "'"
        rs.MoveNext
        Wend
        rs.Close
    Case Else
        rs.Open "Select * from tmp_bukubesar order by kode_acc,CONVERT(VARCHAR(10), tanggal, 111),kredit,no_transaksi,nourut", conn, adOpenDynamic, adLockOptimistic
        While Not rs.EOF
            Saldo = Saldo + (rs!kredit) - (rs!debet)
            conn.Execute "update tmp_bukubesar set Saldo=" & Replace(Saldo, ",", ".") & " where no_transaksi='" & (rs!no_transaksi) & "' and kode_acc='" & (rs!kode_acc) & "'  and nourut='" & (rs!nourut) & "'"
        rs.MoveNext
        Wend
        rs.Close
    End Select
End If

conn.CommitTrans

conn.Close
End Sub

Public Function getstockakhirkartu(kodebarang As String, id As String, tanggal As Date, gudang As String) As Long
Dim con As New ADODB.Connection
Dim rs As New ADODB.Recordset
con.Open strcon
rs.Open "select stock-(select iif(isnull(sum(qty)),0,sum(qty)) from t_pembelianh bh inner join t_pembeliand bd on bh.id=bd.id where bd.[kode]=jd.[kode] and format(bh.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and bh.gudang='" & gudang & "' and bh.status='1' )+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returbelih rbh inner join t_returbelid rbd on rbh.id=rbd.id where rbd.[kode]=jd.[kode] and format(rbh.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and rbh.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returjualh rjh inner join t_returjuald rjd on rjh.id=rjd.id where rjd.[kode]=jd.[kode] and format(rjh.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and rjh.gudang='" & gudang & "' and rjh.status='1' )+ " & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_penjualanh tjh inner join t_penjualand tjd on tjh.id=tjd.id where tjd.[kode]=jd.[kode] and format(tjh.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and tjh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_adjustmenth eh inner join t_adjustmentd ed on eh.id=ed.id where ed.[kode]=jd.[kode] and format(eh.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and eh.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode]=jd.[kode] and format(th.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and th.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode]=jd.[kode] and format(th.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and th.tujuan='" & gudang & "')-" & _
"(select iif(isnull(sum(selisih)),0,sum(selisih)) from t_stockopnameh soh inner join t_stockopnamed sod on soh.id=sod.id where sod.[kode]=jd.[kode] and format(soh.tanggal,'yyyy/MM/dd HH:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and soh.gudang='" & gudang & "' and soh.status='1')" & _
" from ms_bahan jd inner join stock s on jd.[kode]=s.[kode] where jd.[kode]='" & kodebarang & "' and s.gudang='" & gudang & "'", con
If Not rs.EOF Then
getstockakhirkartu = IIf(IsNull(rs(0)), 0, rs(0))
Else
getstockakhirkartu = 0
End If
End Function

Public Function getstockawalkartu(kodebarang As String, id As String, tanggal As Date, gudang As String) As Long
Dim con As New ADODB.Connection
Dim rs As New ADODB.Recordset
con.Open strcon
rs.Open "select stockawal+(select iif(isnull(sum(qty)),0,sum(qty)) from t_pembelianh bh inner join t_pembeliand bd on bh.id=bd.id where bd.[kode]=jd.[kode] and format(bh.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and bh.gudang='" & gudang & "' and bh.status='1' )-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returbelih rbh inner join t_returbelid rbd on rbh.id=rbd.id where rbd.[kode]=jd.[kode] and format(rbh.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and rbh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returjualh rjh inner join t_returjuald rjd on rjh.id=rjd.id where rjd.[kode]=jd.[kode] and format(rjh.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and rjh.gudang='" & gudang & "' and rjh.status='1' )- " & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_penjualanh tjh inner join t_penjualand tjd on tjh.id=tjd.id where tjd.[kode]=jd.[kode] and format(tjh.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and tjh.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_adjustmenth eh inner join t_adjustmentd ed on eh.id=ed.id where ed.[kode]=jd.[kode] and format(eh.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and eh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode]=jd.[kode] and format(th.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and th.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode]=jd.[kode] and format(th.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and th.tujuan='" & gudang & "')+" & _
"(select iif(isnull(sum(selisih)),0,sum(selisih)) from t_stockopnameh soh inner join t_stockopnamed sod on soh.id=sod.id where sod.[kode]=jd.[kode] and format(soh.tanggal,'yyyy/MM/dd HH:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "' and soh.gudang='" & gudang & "' and soh.status='1')" & _
" from ms_bahan jd where [kode]='" & kodebarang & "'", con
getstockawalkartu = IIf(IsNull(rs(0)), 0, rs(0))
End Function
Public Sub exportstock()
On Error GoTo err
Dim exapp As New Excel.Application
Dim exwb As Excel.Workbook
Dim exws As Excel.Worksheet
Dim range As String
Dim Col As Byte
Dim i As Integer
'Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Add
Set exws = exwb.Sheets("Sheet1")

Col = Asc("A")
range = Chr(Col) & "1"

'While exapp.Workbooks.Application.range(range) <> ""
'    ReDim Preserve colname(col - 65)
'    colname(col - 65) = exws.range(range)
'    col = col + 1
'    range = Chr(col) & "1"
'Wend

exws.range(Chr(0 + 65) & "1") = "KodeBarang"
exws.range(Chr(1 + 65) & "1") = "Nama"
exws.range(Chr(2 + 65) & "1") = "Stok"
i = 2
conn.Open strcon
    rs.Open "select m.[kode],m.[nama barang],stock from ms_bahan m inner join stock s on m.[kode]=s.[kode] where flag='1'", conn
    While Not rs.EOF
        exws.range(Chr(0 + 65) & CStr(i)) = rs(0)
        exws.range(Chr(1 + 65) & CStr(i)) = rs(1)
        exws.range(Chr(2 + 65) & CStr(i)) = rs(2)
        
        i = i + 1
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
err:
exwb.SaveAs App.Path & "\stock.xls"
exwb.Close
exapp.Application.Quit
MsgBox "export selesai"
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
If conn.State Then conn.Close
End Sub
Public Sub updateitem(kodebarang As String, barcode As String, namabarang As String, PriceList As Currency, harga1 As Currency, harga2 As Currency, h_eceran As Currency, kategori As String, merk As String, satuan As String)
        Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(11)
    ReDim nilai(11)

    table_name = "ms_bahan"
    fields(0) = "[kode]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "SATUAN"
    fields(4) = "merk"
    fields(5) = "harga1"
    fields(6) = "harga2"
    fields(7) = "h_eceran"
    fields(8) = "flag"
    fields(9) = "lokasi"
    fields(10) = "PriceList"
    
    nilai(0) = IIf(IsNull(barcode), "", barcode)
    nilai(1) = Replace(IIf(IsNull(namabarang), "", namabarang), "'", "''")
    nilai(2) = kategori
    nilai(3) = satuan
    nilai(4) = merk
    nilai(5) = IIf(IsNull(harga1), 0, harga1)
    nilai(6) = IIf(IsNull(harga2), 0, harga2)
    nilai(7) = IIf(IsNull(h_eceran), 0, h_eceran)
    nilai(8) = 1
    nilai(9) = kodebarang
    nilai(10) = IIf(IsNull(PriceList), 0, PriceList)
    
    update_data table_name, fields, nilai, "[kode]='" & nilai(0) & "'", conn
End Sub
Public Sub newitem(kodebarang As String, barcode As String, namabarang As String, PriceList As Currency, harga1 As Currency, harga2 As Currency, h_eceran As Currency, kategori As String, merk As String, satuan As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(11)
    ReDim nilai(11)

    table_name = "ms_bahan"
    fields(0) = "[kode]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "SATUAN"
    fields(4) = "merk"
    fields(5) = "harga1"
    fields(6) = "harga2"
    fields(7) = "h_eceran"
    fields(8) = "flag"
    fields(9) = "lokasi"
    fields(10) = "PriceList"
    
    nilai(0) = IIf(IsNull(barcode), "", barcode)
    nilai(1) = Replace(IIf(IsNull(namabarang), "", namabarang), "'", "''")
    nilai(2) = kategori
    nilai(3) = satuan
    nilai(4) = merk
    nilai(5) = IIf(IsNull(harga1), 0, harga1)
    nilai(6) = IIf(IsNull(harga2), 0, harga2)
    nilai(7) = IIf(IsNull(h_eceran), 0, h_eceran)
    nilai(8) = 1
    nilai(9) = kodebarang
    nilai(10) = IIf(IsNull(PriceList), 0, PriceList)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Public Sub ReplaceQuery(query() As String, filtername As String, filter As String)
    For i = 0 To UBound(query)
        If InStr(1, query(i), filtername) > 0 Then
            If filter <> "" Then
                If InStr(1, query(i), "^^") > 0 Then
                    query(i) = Replace(query(i), "^^", "where")
                End If
                query(i) = Replace(query(i), filtername, " and " & filter)
            Else
                query(i) = Replace(query(i), filtername, filter)
            End If
        End If
    Next
End Sub

Public Sub Angka(KeyAscii As Integer)
Dim strAngka As String
    
        strAngka = "0123456789-"
        If InStr(1, Format("0", "0.00"), ",") > 0 Then strAngka = strAngka & ","
        If InStr(1, Format("0", "0.00"), ".") > 0 Then strAngka = strAngka & "."
   If KeyAscii > 26 Then
        If InStr(strAngka, Chr(KeyAscii)) = 0 Then
           KeyAscii = 0
        End If
    End If
End Sub
Public Sub Angkacalc(KeyAscii As Integer)
Dim strAngka As String
    
        strAngka = "0123456789-+/*"
        If InStr(1, Format("0", "0.00"), ",") > 0 Then strAngka = strAngka & ","
        If InStr(1, Format("0", "0.00"), ".") > 0 Then strAngka = strAngka & "."
   If KeyAscii > 26 Then
        If InStr(strAngka, Chr(KeyAscii)) = 0 Then
           KeyAscii = 0
        End If
    End If
End Sub
Public Sub load_setting()
    reportDir = sGetINI(App.Path & "\setting.ini", "setting", "Report Folder", App.Path & "\Report")
End Sub
Public Sub loadgrup(parent As String, tvwMain As TreeView)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim nodex As Node
conn.Open strcon
rs.Open "select * from ms_grupgudang where parent='" & parent & "' order by urut", conn
While Not rs.EOF
    Set nodex = tvwMain.Nodes.Add(IIf(rs("parent") = "0", Null, "a" & rs("parent").value), IIf(rs("parent") = "", Null, tvwChild), "a" & rs("urut"), rs("grup"))
    nodex.Expanded = True
    loadgrup rs!urut, tvwMain
    rs.MoveNext
Wend
rs.Close
conn.Close
End Sub
