--drop table t_pencairangiroh
create table t_pencairangiroh(
id varchar(15),
tanggal datetime,
tanggal_cair datetime,
keterangan text,
total_nominalgiro numeric(18,0)
)
--drop table t_pencairangirod
create table t_pencairangirod(
id varchar(15),
kode_customer varchar(15),
kode_supplier varchar(15),
no_giro varchar(25),
tanggal_cair datetime,
nominal numeric(18,0)
)
--drop table t_pencairangiro_acc
create table t_pencairangiro_acc(
id varchar(15),
kode_acc varchar(25),
keterangan text,
nominal numeric(18,0)
)