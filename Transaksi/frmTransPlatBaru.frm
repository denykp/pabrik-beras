VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTransPlatBaru 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembuatan Plat "
   ClientHeight    =   4380
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   6870
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4380
   ScaleWidth      =   6870
   Begin VB.CommandButton cmdListPlat 
      Caption         =   "List Plat"
      Height          =   375
      Left            =   3285
      TabIndex        =   25
      Top             =   1485
      Width           =   1140
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   510
      Left            =   3420
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3630
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      Height          =   510
      Left            =   960
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3630
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      Height          =   510
      Left            =   4650
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3630
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      Enabled         =   0   'False
      Height          =   510
      Left            =   2190
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3630
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   855
      Left            =   1530
      MaxLength       =   50
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   4
      Top             =   2520
      Width           =   3465
   End
   Begin VB.TextBox txtQty 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   1530
      MaxLength       =   4
      TabIndex        =   3
      Top             =   2010
      Width           =   825
   End
   Begin VB.TextBox txtSerial 
      Height          =   375
      Left            =   1530
      MaxLength       =   35
      TabIndex        =   2
      Top             =   1500
      Width           =   1545
   End
   Begin VB.TextBox txtKode 
      Height          =   375
      Left            =   1530
      MaxLength       =   35
      TabIndex        =   1
      Top             =   750
      Width           =   1725
   End
   Begin VB.CommandButton cmdSearchPlat 
      Caption         =   "F3"
      Height          =   375
      Left            =   3390
      TabIndex        =   10
      Top             =   720
      Width           =   555
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   5220
      TabIndex        =   13
      Top             =   210
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   661
      _Version        =   393216
      Format          =   48889857
      CurrentDate     =   40498
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   375
      Left            =   3390
      TabIndex        =   9
      Top             =   240
      Width           =   555
   End
   Begin VB.Label lblNama 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Plat"
      Height          =   240
      Left            =   1560
      TabIndex        =   24
      Top             =   1170
      Width           =   870
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1380
      TabIndex        =   23
      Top             =   2520
      Width           =   165
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   5070
      TabIndex        =   22
      Top             =   240
      Width           =   165
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1350
      TabIndex        =   21
      Top             =   1980
      Width           =   165
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1350
      TabIndex        =   20
      Top             =   1530
      Width           =   165
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1350
      TabIndex        =   19
      Top             =   780
      Width           =   165
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1350
      TabIndex        =   18
      Top             =   300
      Width           =   165
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      Height          =   240
      Left            =   300
      TabIndex        =   17
      Top             =   2550
      Width           =   975
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Qty"
      Height          =   240
      Left            =   300
      TabIndex        =   16
      Top             =   2100
      Width           =   285
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "No.Register"
      Height          =   240
      Left            =   300
      TabIndex        =   15
      Top             =   1560
      Width           =   990
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Plat"
      Height          =   240
      Left            =   300
      TabIndex        =   14
      Top             =   780
      Width           =   795
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      Height          =   240
      Left            =   4290
      TabIndex        =   12
      Top             =   270
      Width           =   690
   End
   Begin VB.Label lblNoTrans 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      Height          =   240
      Left            =   1560
      TabIndex        =   11
      Top             =   300
      Width           =   1275
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "No.ID"
      Height          =   240
      Left            =   300
      TabIndex        =   0
      Top             =   270
      Width           =   465
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu menu2 
         Caption         =   "Reset Form"
      End
      Begin VB.Menu menu 
         Caption         =   "Simpan"
      End
      Begin VB.Menu menu1 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmTransPlatBaru"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdListPlat_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from serial_plat where kode_plat='" & txtKode.text & "'"
    frmSearch.nmform = "frmTransPlatBaru"
    frmSearch.nmctrl = ""
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "plat_serial"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtSerial.SetFocus

End Sub

Private Sub cmdPosting_Click()
 If simpan And Cek_Serial Then
    posting_PlatBaru lblNoTrans, True
    reset_form
End If
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSimpan_Click()
If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub
Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select t.nomer_transaksi,t.tanggal,t.keterangan from t_plat_baru t where t.status_posting=0"
    frmSearch.nmform = "frmTransPlatBaru"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "platBaruH"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchPlat_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_plat,nama_plat,kategori from ms_plat"
    frmSearch.nmform = "frmTransPlatBaru"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_plat"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_barang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtSerial.SetFocus
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    If txtKode = "" Then
        MsgBox "Silahkan masukkan Kode Plat terlebih dahulu"
        txtKode.SetFocus
        Exit Function
    End If
    If txtSerial = "" Then
        MsgBox "Silahkan masukkan No.Register terlebih dahulu"
        txtSerial.SetFocus
        Exit Function
    End If
    
    conn.Open strcon
    conn.BeginTrans
i = 1
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_plat_baru", "nomer_transaksi", DTPicker1, "PLB")
    Else
        conn.Execute "delete from t_plat_baru where nomer_transaksi='" & lblNoTrans & "'"
    End If
    
    add_dataheader
   
    
    conn.CommitTrans
i = 0
    DropConnection
    simpan = True
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_plat_baru"
    fields(0) = "nomer_transaksi"
    fields(1) = "kode_plat"
    fields(2) = "tanggal"
    fields(3) = "nomer_serial"
    fields(4) = "qty"
    fields(5) = "keterangan"
    fields(6) = "hpp"
    
    nilai(0) = lblNoTrans
    nilai(1) = txtKode
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = txtSerial
    nilai(4) = Replace(txtQty, ",", ".")
    nilai(5) = txtKeterangan.text
    nilai(6) = Replace(GetHPPplat(txtKode, conn), ",", ".")
    tambah_data table_name, fields, nilai
End Sub

Public Function cari_barang() As Boolean
If txtKode = "" And cari_barang = False Then Exit Function
cari_barang = False
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select kode_plat,nama_plat from ms_plat where kode_plat='" & txtKode.text & "'", conn
    If Not rs.EOF Then
    cari_barang = True
        lblNama = rs(1)
        txtQty.text = 1
    Else
        lblNama = ""
        txtQty.text = 0
        
    End If
    rs.Close
    conn.Close
End Function

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select a.*,b.nama_plat from t_plat_baru a inner join ms_plat b on a.kode_plat=b.kode_plat where a.nomer_transaksi='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = Format(rs!tanggal, "dd/MM/yyyy")
        txtKode = rs!kode_plat
        lblNama = rs!nama_plat
        txtQty = rs!qty
        txtSerial = rs!nomer_serial
        If Not IsNull(rs!keterangan) Then txtKeterangan.text = rs!keterangan Else txtKeterangan = ""
        cmdPosting.Enabled = True
    
    End If
    rs.Close
    conn.Close

End Sub
Private Sub reset_form()
    lblNoTrans = "-"
'    txtKode = ""
    txtSerial = ""
    lblNama = ""
    txtQty = 1
    txtKeterangan = ""
'    DTPicker1 = Format(Now, "dd/MM/yyyy hh:mm:ss")
'    cmdPosting.Enabled = False
End Sub

Private Sub Form_Load()
    reset_form
    DTPicker1 = Format(Now, "dd/MM/yyyy hh:mm:ss")
End Sub

Private Sub menu2_Click()
    reset_form
End Sub

Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchPlat_Click

End Sub

Private Sub txtKode_LostFocus()
    If Not cari_barang And txtKode <> "" Then txtKode.SetFocus
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtSerial_LostFocus()
    If txtSerial <> "" Then Cek_Serial

End Sub
Function Cek_Serial() As Boolean

On Error GoTo err
Cek_Serial = False
conn.Open strcon
    rs.Open "select nomer_serial from serial_plat where kode_plat='" & txtKode & "' and nomer_serial='" & txtSerial & "' " & _
            " union all select nomer_serial from t_plat_baru where kode_plat='" & txtKode & "' and nomer_serial='" & txtSerial & "' and status_posting='1'", conn
    If Not rs.EOF Then
        MsgBox "No.Register sudah ada!"
        txtSerial.SetFocus
    Else
        Cek_Serial = True
        txtQty.SetFocus
    End If
    rs.Close
conn.Close
Exit Function
err:
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Function

