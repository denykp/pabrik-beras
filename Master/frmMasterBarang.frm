VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMasterBarang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MasterBarang"
   ClientHeight    =   7875
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12795
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   12795
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "Browse"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10350
      Picture         =   "frmMasterBarang.frx":0000
      TabIndex        =   74
      Top             =   1305
      Width           =   915
   End
   Begin VB.TextBox txtFile 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8010
      TabIndex        =   73
      Top             =   1305
      Width           =   2280
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1545
      Index           =   10
      Left            =   5670
      MultiLine       =   -1  'True
      TabIndex        =   19
      Top             =   4545
      Width           =   5730
   End
   Begin VB.CommandButton cmdLoadRpt 
      Caption         =   "Load History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4230
      Picture         =   "frmMasterBarang.frx":0102
      TabIndex        =   70
      Top             =   1755
      Width           =   1320
   End
   Begin VB.CommandButton cmdSatuanBaru 
      Caption         =   "Satuan Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5175
      TabIndex        =   69
      Top             =   2970
      Width           =   1365
   End
   Begin VB.CommandButton cmdSubKategori 
      Caption         =   "Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6210
      TabIndex        =   68
      Top             =   585
      Width           =   735
   End
   Begin VB.CommandButton cmdNewKategori 
      Caption         =   "Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6210
      TabIndex        =   67
      Top             =   180
      Width           =   735
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   10260
      Top             =   6480
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   1845
      TabIndex        =   3
      Text            =   "Combo1"
      Top             =   1395
      Width           =   4305
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   1845
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   990
      Width           =   4305
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   9
      Left            =   1845
      TabIndex        =   16
      Text            =   "0"
      Top             =   4770
      Width           =   1065
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   8
      Left            =   1845
      TabIndex        =   15
      Text            =   "0"
      Top             =   4410
      Width           =   1065
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   7
      Left            =   1845
      TabIndex        =   14
      Text            =   "0"
      Top             =   4050
      Width           =   1065
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   6
      Left            =   1845
      TabIndex        =   13
      Text            =   "0"
      Top             =   3690
      Width           =   1065
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      ItemData        =   "frmMasterBarang.frx":0204
      Left            =   1845
      List            =   "frmMasterBarang.frx":0206
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   3285
      Width           =   1995
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   5
      Left            =   4320
      TabIndex        =   12
      Top             =   3300
      Width           =   795
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      ItemData        =   "frmMasterBarang.frx":0208
      Left            =   1845
      List            =   "frmMasterBarang.frx":020A
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   2925
      Width           =   1995
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   4
      Left            =   4320
      TabIndex        =   10
      Top             =   2940
      Width           =   795
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   1860
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   585
      Width           =   4305
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   6225
      TabIndex        =   8
      Top             =   2565
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton cmdSearchAccHPP 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3870
      TabIndex        =   51
      Top             =   5550
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.TextBox txtAccHPP 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1860
      MaxLength       =   25
      TabIndex        =   18
      Top             =   5550
      Width           =   1950
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Non Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5805
      TabIndex        =   50
      Top             =   2160
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5805
      TabIndex        =   49
      Top             =   1800
      Value           =   -1  'True
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   3
      Left            =   4335
      TabIndex        =   7
      Text            =   "1"
      Top             =   2565
      Width           =   795
   End
   Begin VB.TextBox txtAccPersediaan 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1860
      MaxLength       =   25
      TabIndex        =   17
      Top             =   5145
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearchAccPersediaan 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3870
      TabIndex        =   46
      Top             =   5175
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   1860
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   180
      Width           =   4305
   End
   Begin VB.Frame frStock 
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      Height          =   330
      Left            =   3195
      TabIndex        =   40
      Top             =   3690
      Visible         =   0   'False
      Width           =   3570
      Begin VB.TextBox lblStock 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1785
         TabIndex        =   27
         Top             =   0
         Visible         =   0   'False
         Width           =   1545
      End
      Begin VB.Label Label32 
         BackStyle       =   0  'Transparent
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   270
         TabIndex        =   41
         Top             =   45
         Visible         =   0   'False
         Width           =   1140
      End
   End
   Begin VB.Frame frHPP 
      BorderStyle     =   0  'None
      Caption         =   "Frame6"
      Height          =   375
      Left            =   3195
      TabIndex        =   38
      Top             =   4050
      Visible         =   0   'False
      Width           =   4065
      Begin VB.TextBox lblHPP 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1785
         TabIndex        =   26
         Top             =   60
         Width           =   1455
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   1515
         TabIndex        =   45
         Top             =   75
         Width           =   285
      End
      Begin VB.Label Label23 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga Pokok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   270
         TabIndex        =   39
         Top             =   75
         Width           =   1140
      End
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7425
      TabIndex        =   37
      Top             =   6420
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdBaru 
      Caption         =   "&Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4515
      Picture         =   "frmMasterBarang.frx":020C
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   7050
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5025
      Picture         =   "frmMasterBarang.frx":073E
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   6420
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6240
      Picture         =   "frmMasterBarang.frx":0C70
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   6420
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdHistory 
      Caption         =   "&History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   7560
      Picture         =   "frmMasterBarang.frx":11A2
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   7065
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   9000
      Picture         =   "frmMasterBarang.frx":12A4
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   7065
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CheckBox chkAktif 
      Caption         =   "Aktif"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   45
      TabIndex        =   30
      Top             =   6345
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      ItemData        =   "frmMasterBarang.frx":13A6
      Left            =   1845
      List            =   "frmMasterBarang.frx":13A8
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   2520
      Width           =   1995
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1470
      Picture         =   "frmMasterBarang.frx":13AA
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   7050
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3825
      Picture         =   "frmMasterBarang.frx":14AC
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   1770
      Width           =   375
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3015
      Picture         =   "frmMasterBarang.frx":15AE
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   7050
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6030
      Picture         =   "frmMasterBarang.frx":16B0
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   7050
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   1860
      MaxLength       =   70
      TabIndex        =   5
      Top             =   2160
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      IMEMode         =   3  'DISABLE
      Index           =   0
      Left            =   1860
      MaxLength       =   35
      TabIndex        =   4
      Top             =   1770
      Width           =   1950
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8100
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Image Image1 
      Height          =   2535
      Left            =   8010
      Stretch         =   -1  'True
      Top             =   1755
      Width           =   4470
   End
   Begin VB.Label Label24 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4410
      TabIndex        =   72
      Top             =   4590
      Width           =   1050
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5445
      TabIndex        =   71
      Top             =   4590
      Width           =   105
   End
   Begin VB.Label Label17 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   66
      Top             =   1425
      Width           =   1485
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1590
      TabIndex        =   65
      Top             =   1455
      Width           =   150
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   64
      Top             =   1020
      Width           =   1485
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1590
      TabIndex        =   63
      Top             =   1050
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "Disc4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   62
      Top             =   4800
      Width           =   1320
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Disc3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   61
      Top             =   4440
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Disc2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   60
      Top             =   4080
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Disc1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   59
      Top             =   3720
      Width           =   1320
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Isi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3945
      TabIndex        =   58
      Top             =   3345
      Visible         =   0   'False
      Width           =   330
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Isi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3945
      TabIndex        =   57
      Top             =   2985
      Visible         =   0   'False
      Width           =   330
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1605
      TabIndex        =   56
      Top             =   645
      Width           =   150
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   55
      Top             =   615
      Width           =   1485
   End
   Begin VB.Label Label18 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5235
      TabIndex        =   54
      Top             =   2625
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.Label lbl1 
      BackStyle       =   0  'Transparent
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Index           =   6
      Left            =   5895
      TabIndex        =   53
      Top             =   2655
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Label Label27 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc HPP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   52
      Top             =   5595
      Width           =   690
   End
   Begin VB.Label Label25 
      BackStyle       =   0  'Transparent
      Caption         =   "Isi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3960
      TabIndex        =   48
      Top             =   2610
      Width           =   330
   End
   Begin VB.Label Label20 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Persediaan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   47
      Top             =   5220
      Width           =   1305
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   43
      Top             =   210
      Width           =   810
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   42
      Top             =   240
      Width           =   150
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1560
      TabIndex        =   36
      Top             =   2205
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1560
      TabIndex        =   35
      Top             =   1770
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   0
      TabIndex        =   34
      Top             =   6675
      Width           =   2130
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Satuan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   33
      Top             =   2595
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   32
      Top             =   2205
      Width           =   1320
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   44
      Top             =   1815
      Width           =   1140
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim foc As Byte
Dim colname() As String
Public tgldari As Date, tglsampai As Date
Private Sub cmbField_Click(Index As Integer)
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open strcon
If Index = 3 Then
    cmbField(4).Clear
    rs.Open "select * from var_subkategori where kategori='" & cmbField(3).text & "' order by subkategori", conn
    While Not rs.EOF
        cmbField(4).AddItem rs(1)
        rs.MoveNext
    Wend
    rs.Close
ElseIf Index = 4 Then
    rs.Open "select * from var_subkategori where kategori='" & cmbField(3).text & "' and subkategori='" & cmbField(4).text & "'", conn
    If Not rs.EOF Then
        txtAccPersediaan = rs!Acc_Persediaan
        txtAccHPP = rs!acc_hpp
        
    End If
    rs.Close
    cmbField(5).Clear
    rs.Open "select distinct grup from ms_bahan where kategori='" & cmbField(3).text & "' and subkategori='" & cmbField(4).text & "' order by grup", conn
    While Not rs.EOF
        cmbField(5).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
ElseIf Index = 5 Then
    cmbField(6).Clear
    rs.Open "select distinct subgrup from ms_bahan where kategori='" & cmbField(3).text & "' and subkategori='" & cmbField(4).text & "' and grup='" & cmbField(5).text & "' order by subgrup", conn
    While Not rs.EOF
        cmbField(6).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close

End If
conn.Close
End Sub

Private Sub cmdBaru_Click()
    reset_form
End Sub

Private Sub cmdBrowse_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.jpg|*.jpg"
    CommonDialog1.filename = "Image Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        Set Image1.Picture = LoadPicture(CommonDialog1.filename)
        txtFile = CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
On Error GoTo err
   If txtField(0).text = "" Then
    MsgBox "Silahkan masukkan Kode bahan terlebih dahulu!", vbCritical
    txtField(0).SetFocus
    Exit Sub
   End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    rs.Open "select  distinct [kode_bahan] from stock where [kode_bahan]='" & txtField(0).text & "' and stock >0", conn
    If rs.EOF Then
'        While Not rs.EOF
    '        conn.Execute "update ms_bahan set flag='0' where [kode_bahan]='" & txtField(0).text & "'"
    '    Else
            conn.Execute "delete from ms_bahan where [kode_bahan]='" & txtField(0).text & "'"
'            conn.Execute "delete from setting_accbahan where [kode_bahan]='" & txtField(0).text & "'"
'            conn.Execute "delete from stock where [kode_bahan]='" & txtField(0).text & "'"
    '        conn.Execute "delete from hpp where [kode_bahan]='" & txtField(0).text & "'"
'            rs.MoveNext
'        Wend
       MsgBox "Data sudah dihapus"
       reset_form
    Else
         MsgBox "Data tidak dapat dihapus!"
    End If
    If rs.State Then rs.Close

    conn.CommitTrans
    i = 0
    
    
    DropConnection
    Set conn = Nothing
'    cmdPrev_Click
'    cmdNext_Click
    'reset_form
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    conn.RollbackTrans
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdHistory_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [kode_bahan] , nama_bahan , " & _
    "kategori,subkategori,harga_sat   from hst_ms_bahan" ' order by [kode_bahan]"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = ""
    frmSearch.nmctrl2 = ""
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    frmSearch.chkDate.Visible = True
    frmSearch.DTPicker1.Visible = True
    frmSearch.Show vbModal

End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoadRpt_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select * from rpt_ms_bahan"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.proc = "cari_data_rpt"
    frmSearch.Col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    frmSearch.Show vbModal
End Sub

Private Sub cmdNewKategori_Click()
    frmMasterKategori.Show vbModal
    load_combo1
End Sub

Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_bahan] from ms_bahan where [kode_bahan]>'" & txtField(0).text & "' order by [kode_bahan]", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection

    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_bahan] from ms_bahan where [kode_bahan]<'" & txtField(0).text & "' order by [kode_bahan] desc", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
On Error GoTo err

'Dim AppExcel As Excel.Application
'Set AppExcel = CreateObject("Excel.Application")
'ChDir reportDir
'Workbooks.Open filename:=reportDir & "\history_harga.xls"
'
'AppExcel.Visible = True

Dim LogonInfo As String
    Set frmPeriode.frm = Me
    frmPeriode.Show vbModal

    With CRPrint
        .reset
        .ReportFileName = reportDir & "\history_harga.rpt"

        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
        Next
        If txtField(0) <> "" Then
            .SelectionFormula = "{hst_hargajualh.tanggal}>=#" & Format(tgldari, "yyyy/MM/dd") & "# and {hst_hargajualh.tanggal}<#" & Format(tglsampai + 1, "yyyy/MM/dd") & "#" & " and {ms_bahan.kode_bahan} = '" & txtField(0).text & "'"  '& " and {hst_hargajuald.kode_bahan} = '" & txtField(0).text & "'"
        Else
            .SelectionFormula = "{hst_hargajualh.tanggal}>=#" & Format(tgldari, "yyyy/MM/dd") & "# and {hst_hargajualh.tanggal}<#" & Format(tglsampai + 1, "yyyy/MM/dd") & "#"
        End If
        '.SelectionFormula = "{ms_bahan.kode_bahan} = '" & txtField(0).text & "'"
        .Destination = crptToWindow
        .ParameterFields(0) = "tglDari;" + Format(frmPeriode.DTDari, "dd/MM/yyyy") + ";True"
        .ParameterFields(1) = "tglsampai;" + Format(frmPeriode.DTSampai, "dd/MM/yyyy") + ";True"
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        .action = 1
    End With

    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdSatuanBaru_Click()
    frmMasterSatuan.Show vbModal
    load_combo2
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.proc = "cari_data"
    frmSearch.Col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchAccHPP_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and nama like '%HPP%' or nama like '%Harga Pokok%' "
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtAccSelisih"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccPersediaan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='110.040'"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtAccPersediaan"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccSelisih_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and nama like '%HPP%' or nama like '%Harga Pokok%' "
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtAccSelisih"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub



Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0


    If Trim(lblStock.text) = "" Then lblStock.text = 0
    If Trim(lblHPP.text) = "" Then lblHPP.text = 0

    For J = 0 To 1
    If txtField(J).text = "" Then
        MsgBox "semua field yang bertanda * harus diisi"
        txtField(J).SetFocus
        Exit Sub
    End If
    Next J
    
'    If (Trim(txtAccPersediaan.text) = "") Or txtAccHPP.text = "" Or txtAccSelisih = "" Then
'        MsgBox "Acc Persediaan harus di-isi ! Bisa di-isi dari setiing kategori. ", vbExclamation
'        Exit Sub
'    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans

    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic

    If Not rs.EOF Then
        
        add_dataheader 2, conn
        
        
    Else
        add_dataheader 1, conn
        
    End If
    If rs.State Then rs.Close
    Dim S As New ADODB.Stream
    If txtFile <> "" Then
        rs.Open "select * from ms_bahan where [kode_bahan]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        
        With S
            .Type = adTypeBinary
            .Open
            .LoadFromFile txtFile
            rs("foto").value = .Read
            rs.Update
        End With
        Set S = Nothing
    End If
    If rs.State Then rs.Close
    rs.Open "select * from rpt_ms_bahan where [kode_bahan]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        add_dataheaderRpt 2, conn
        
    Else
        add_dataheaderRpt 1, conn
        
    End If
        If txtFile <> "" Then
        If rs.State Then rs.Close
        rs.Open "select * from rpt_ms_bahan where [kode_bahan]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        
        With S
            .Type = adTypeBinary
            .Open
            .LoadFromFile txtFile
            rs("foto").value = .Read
            rs.Update
        End With
        Set S = Nothing
    End If

    conn.Execute "delete from setting_accbahan where kode_bahan='" & txtField(0).text & "'"
    add_dataacc 1, conn
    If rs.State Then rs.Close
    
'    If lblHPP.Locked = False And lblStock.Locked = False Then
'        conn.Execute "update stock set stock=" & lblStock & ",hpp=" & Format(lblHPP, "###0") & " where [kode_bahan]='" & txtField(0).text & "' and kode_gudang='" & gudang & "'"
'        conn.Execute "update hst_hpp set qty=" & lblStock & ",harga=" & lblHPP & ",hpp=" & lblHPP & ",stockakhir=" & lblStock & " where [kode_bahan]='" & txtField(0).text & "' and kode_gudang='" & gudang & "' and jenis='Awal'"
'    End If
    conn.CommitTrans
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form

    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = "0"
    txtField(3).text = "1"
    txtField(4).text = "1"
    txtField(5).text = "1"
    txtField(6).text = "0"
    txtField(7).text = "0"
    txtField(8).text = "0"
    txtField(9).text = "0"
    txtField(10).text = ""
    txtAccPersediaan = ""
    txtAccSelisih = ""
    txtAccHPP = ""
    
    txtGroup.text = ""
    lblStock.Locked = False
    lblHPP.Locked = False
    lblStock = "0"
    lblHPP = "0"
    txtField(0).SetFocus
    cmdPrev.Enabled = False
    cmdNext.Enabled = False
    txtFile = ""
    Image1.Picture = Nothing
    Call cmbField_Click(1)
End Sub
Private Sub add_dataheader(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(18)
    ReDim nilai(18)

    table_name = "ms_bahan"
    fields(0) = "[kode_bahan]"
    fields(1) = "nama_bahan"
    fields(2) = "[KATEGORI]"
    fields(3) = "subkategori"
    fields(4) = "grup"
    fields(5) = "subgrup"
    fields(6) = "harga_sat"
    fields(7) = "satuan1"
    fields(8) = "isi1"
    fields(9) = "satuan2"
    fields(10) = "isi2"
    fields(11) = "satuan3"
    fields(12) = "isi3"
    fields(13) = "disc1"
    fields(14) = "disc2"
    fields(15) = "disc3"
    fields(16) = "disc4"
    fields(17) = "keterangan"
    
    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(3).text
    nilai(3) = cmbField(4).text
    nilai(4) = cmbField(5).text
    nilai(5) = cmbField(6).text
    nilai(6) = Replace(txtField(2).text, ",", ".")
    
    nilai(7) = cmbField(0).text
    nilai(8) = txtField(3).text
    nilai(9) = cmbField(1).text
    nilai(10) = txtField(4).text
    nilai(11) = cmbField(2).text
    nilai(12) = txtField(5).text
    nilai(13) = txtField(6).text
    nilai(14) = txtField(7).text
    nilai(15) = txtField(8).text
    nilai(16) = txtField(9).text
    nilai(17) = txtField(10).text
    
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_dataheaderRpt(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    table_name = "rpt_ms_bahan"
 
    fields(0) = "[kode_bahan]"
    fields(1) = "nama_bahan"
    fields(2) = "SATUAN1"
    fields(3) = "[KATEGORI]"
    fields(4) = "subkategori"
    fields(5) = "isi1"
    fields(6) = "grup"
    fields(7) = "subgrup"
    fields(8) = "keterangan"
    
    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(0).text
    nilai(3) = cmbField(3).text
    nilai(4) = cmbField(4).text
    nilai(5) = txtField(3).text
    nilai(6) = cmbField(5).text
    nilai(7) = cmbField(6).text
    nilai(8) = txtField(10).text

    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_dataacc(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)

    table_name = "setting_accbahan"
    fields(0) = "[kode_bahan]"
    fields(1) = "acc_persediaan"
    fields(2) = "acc_hpp"
    
    
    nilai(0) = txtField(0).text
    nilai(1) = txtAccPersediaan
    nilai(2) = txtAccHPP
    
    
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub

Public Sub cari_data_rpt()
    cari_data "rpt_"
End Sub
Public Sub cari_data(Optional prefix As String)
On Error Resume Next
    If txtField(0).text = "" Then Exit Sub
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from " & prefix & "ms_bahan  " & _
            "where kode_bahan='" & txtField(0).text & "' ", conn

    If Not rs.EOF Then
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        txtField(1).text = rs("nama_bahan")
        txtField(2).text = rs("harga_sat")
        
        txtField(3).text = rs("isi1")
        txtField(4).text = rs("isi2")
        txtField(5).text = rs("isi3")
        txtField(6).text = rs("disc1")
        txtField(7).text = rs("disc2")
        txtField(8).text = rs("disc3")
        txtField(9).text = rs("disc4")
        txtField(10).text = rs("keterangan")
        
        SetComboTextRight rs!satuan1, cmbField(0)
        SetComboTextRight rs!satuan2, cmbField(1)
        SetComboTextRight rs!satuan3, cmbField(2)
        
        SetComboTextRight rs!kategori, cmbField(3)
        SetComboTextRight rs!subkategori, cmbField(4)
        SetComboTextRight rs!grup, cmbField(5)
        SetComboTextRight rs!subgrup, cmbField(6)
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        Dim mStream As New ADODB.Stream
    
    Dim fs As New Scripting.FileSystemObject
    
    fs.DeleteFile App.Path & "\temp.jpg"
    
  With mStream
    .Type = adTypeBinary
    .Open
    .Write rs("foto").value
    .SaveToFile App.Path & "\temp.jpg"
    Image1.Picture = LoadPicture(App.Path & "\temp.jpg")
    txtFile = App.Path & "\temp.jpg"
  End With

  Set mStream = Nothing
        
        If rs.State Then rs.Close
        rs.Open "select * from setting_accbahan  " & _
            "where kode_bahan='" & txtField(0).text & "' ", conn
        If Not rs.EOF Then
            txtAccPersediaan.text = rs!Acc_Persediaan
            txtAccHPP.text = rs!acc_hpp
            
        End If
        If rs.State Then rs.Close
        
        

        lblHPP.Locked = True
'        lblStock = getStockKertas(txtField(0).text, gudang, "", strcon)
        
'        lblHPP = getHppKertas(txtField(0).text, conn)
        lblStock.Locked = True
    Else
        cmdPrev.Enabled = False
        cmdPrev.Enabled = False
        
      
        cmdHapus.Enabled = False
        mnuHapus.Enabled = True
        lblHPP = 0
        lblStock = 0
        lblHPP.Locked = False
        lblStock.Locked = False
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub load_combo1()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbField(3).Clear
    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(3).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
End Sub
Private Sub load_combo2()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    
    cmbField(0).Clear
    cmbField(1).Clear
    cmbField(2).Clear
    rs.Open "select * from var_satuan order by satuan", conn
    While Not rs.EOF
        cmbField(0).AddItem rs(0)
        cmbField(1).AddItem rs(0)
        cmbField(2).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub




Private Sub cmdSubKategori_Click()
    SetComboText cmbField(3).text, frmMasterSubKategori.cmbField(0)
    frmMasterSubKategori.Show vbModal
    cmbField_Click (3)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
 
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
'    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        If foc = 1 And txtField(0).text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If

End Sub

Private Sub Form_Load()
    alterbahan
    load_combo1
    load_combo2
    reset_form
    If cmbField(0).ListCount > 0 Then cmbField(0).ListIndex = 0
'    If cmbField(1).ListCount > 0 Then cmbField(1).ListIndex = 0
'    If cmbField(2).ListCount > 0 Then cmbField(2).ListIndex = 0
'    If cmbField(3).ListCount > 0 Then cmbField(3).ListIndex = 0

    cmbField(5).text = ""
    cmbField(6).text = ""
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub lblHPP_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub lblStock_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtAccHPP_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchAccHPP_Click
End Sub

Private Sub txtAccPersediaan_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchAccPersediaan_Click
End Sub
Private Sub txtAccSelisih_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchAccSelisih_Click
End Sub

Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index).text)
    foc = 1
End Sub

Private Sub lblHPP_GotFocus()
    lblHPP.SelStart = 0
    lblHPP.SelLength = Len(lblHPP.text)
    foc = 1
End Sub

Private Sub lblStock_GotFocus()
    lblStock.SelStart = 0
    lblStock.SelLength = Len(lblStock.text)
    foc = 1
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index = 0 Then
            cari_data
        End If
    ElseIf Index = 2 Then
        Angka KeyAscii
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    foc = 0
    If Index = 0 And txtField(0).text <> "" Then cari_data

'        If Not cek_barcode(txtField(0).text, txtField(5).text) Then
'            MsgBox "Barcode tersebut sudah pernah digunakan sebelumnya"
'            txtField(5).SetFocus
'        End If
    
'    If Index = 8 Then
'        If txtField(Index).text <> "" Then
'        If Not cek_barcode(txtField(0).text, txtField(Index).text) Then
'            txtField(Index).SetFocus
'            MsgBox "Barcode yang anda masukkan sudah dipakai sebelumnya"
'            txtField(Index).SelStart = 0
'            txtField(Index).SelLength = Len(txtField(Index).text)
'        End If
'        End If
'    End If

End Sub


Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
        MsgBox "Loading selesai"
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Function loadexcelfile1(filename As String) As Boolean
On Error GoTo err
Dim exapp As Object
Dim range As String
Dim rs2 As New ADODB.Recordset
Dim Col As Byte
Dim i As Integer
loadexcelfile1 = False
Set exapp = CreateObject("excel.application")
exapp.Workbooks.Open filename
exapp.Sheets(1).Select
conn.Open strcon
Col = Asc("A")
range = Chr(Col) & "1"

While exapp.Workbooks.Application.range(range) <> ""
    ReDim Preserve colname(Col - 65)
    colname(Col - 65) = exapp.Workbooks.Application.range(range)
    Col = Col + 1
    range = Chr(Col) & "1"
Wend

i = 2
While exapp.range("A" & CStr(i)) <> ""
        rs2.Open "select * from var_kategori where kategori='" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_bahan where [kode_bahan]='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            newitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))
        Else
            updateitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))

        End If
        rs2.Close
'        rs2.Open "select * from stock where [kode_bahan]='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
'        If rs2.EOF Then
'        conn.Execute "insert into stock  (kode_gudang,[kode_bahan],stock)  values ('" & gudang & "'," & _
'            "'" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
'            "'" & exapp.range(Chr(getcolindex("stok", colname) + 65) & CStr(i)) & "')"
'        End If
'        rs2.Close
'        rs2.Open "select * from hpp where [kode_bahan]='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
'        If rs2.EOF Then
'        conn.Execute "insert into hpp (kode_gudang,[kode_bahan],hpp)  values ('" & gudang & "'," & _
'            "'" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
'            "" & IIf(IsNumeric(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), CDbl(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), 0) & ")"
'        End If
'        rs2.Close

    i = i + 1
Wend
exapp.Workbooks.Close
loadexcelfile1 = True
exapp.Application.Quit
Set exapp = Nothing

MsgBox "Success"
conn.Close
Exit Function
err:
MsgBox err.Description
Resume Next
''If conn.State Then conn.Close
''If exapp.Workbooks.Count > 0 Then exapp.Workbooks.Close
''exapp.Application.Quit
''Set exapp = Nothing
End Function
Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset

Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=Excel 8.0"
con.Open stcon
conn.Open strcon
rs.Open "select * from [sheet1$]", con
While Not rs.EOF
    If rs(0) <> "E" Then
        rs2.Open "select * from var_kategori where kategori='" & rs("kategori") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & rs("kategori") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & rs("merk") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & rs("merk") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & rs("satuan") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & rs("satuan") & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_bahan where [kode_bahan]='" & rs("kodebarang") & "'", conn
        If rs2.EOF Then
            newitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")

            conn.Execute "insert into stock  (kode_gudang,[kode_bahan],stock)  select gudang,'" & rs("kodebarang") & "','" & rs("stok") & "' from var_gudang"
            conn.Execute "insert into hpp (kode_gudang,[kode_bahan],hpp)  select gudang,'" & rs("kodebarang") & "','" & rs("hargapokok") & "' from var_gudang"
        Else
            updateitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")
        End If
        rs2.Close
    End If
    rs.MoveNext
Wend
rs.Close
con.Close
DropConnection
Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub




Private Sub txtHrgJual1_GotFocus()
   txtHrgJual1.SelStart = 0
   txtHrgJual1.SelLength = Len(txtHrgJual1)
End Sub

Private Sub txtHrgJual1_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub
Private Sub txtHrgJual2_GotFocus()
   txtHrgJual2.SelStart = 0
   txtHrgJual2.SelLength = Len(txtHrgJual2)
End Sub

Private Sub txtHrgJual2_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub
Private Sub txtHrgJual3_GotFocus()
   txtHrgJual3.SelStart = 0
   txtHrgJual3.SelLength = Len(txtHrgJual3)
End Sub

Private Sub txtHrgJual3_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub
Private Sub txtHrgJual4_GotFocus()
   txtHrgJual4.SelStart = 0
   txtHrgJual4.SelLength = Len(txtHrgJual4)
End Sub

Private Sub txtHrgJual4_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHrg2_GotFocus()
   txtHrg2.SelStart = 0
   txtHrg2.SelLength = Len(txtHrg2)
End Sub

Private Sub txtHrg2_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub
Private Sub txtHrg3_GotFocus()
   txtHrg3.SelStart = 0
   txtHrg3.SelLength = Len(txtHrg3)
End Sub

Private Sub txtHrg3_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub
