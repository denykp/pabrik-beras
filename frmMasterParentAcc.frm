VERSION 5.00
Begin VB.Form frmMasterParentAcc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Grup Perkiraan"
   ClientHeight    =   2970
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7110
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2970
   ScaleWidth      =   7110
   Begin VB.ComboBox cmbDefault 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterParentAcc.frx":0000
      Left            =   1890
      List            =   "frmMasterParentAcc.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1440
      Width           =   2280
   End
   Begin VB.ComboBox cmbJenis 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterParentAcc.frx":0016
      Left            =   1890
      List            =   "frmMasterParentAcc.frx":0020
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1020
      Width           =   1590
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterParentAcc.frx":0037
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      MaxLength       =   50
      TabIndex        =   1
      Top             =   630
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      MaxLength       =   3
      TabIndex        =   0
      Top             =   225
      Width           =   915
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4538
      Picture         =   "frmMasterParentAcc.frx":0139
      TabIndex        =   6
      Top             =   2295
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2918
      Picture         =   "frmMasterParentAcc.frx":023B
      TabIndex        =   5
      Top             =   2295
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1343
      Picture         =   "frmMasterParentAcc.frx":033D
      TabIndex        =   4
      Top             =   2295
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   18
      Top             =   1455
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   17
      Top             =   1455
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Jenis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   1065
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   15
      Top             =   1065
      Width           =   105
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   13
      Top             =   675
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   12
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   11
      Top             =   1935
      Width           =   2130
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   10
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   9
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterParentAcc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbJenis_Click()

' HT=Harta, KW=Kewajiban, MD=Modal, PD=Pendapatan, HPP=Harga Pokok Penjualan
' BO=Biaya Operasional, BNO=Biaya Non Operasional, PL=Pendapatan Lain-Lain, BL=Biaya Lain-Lain

    If cmbJenis.text = "Neraca" Then
        cmbDefault.Clear
        cmbDefault.AddItem "Harta"
        cmbDefault.AddItem "Kewajiban"
        cmbDefault.AddItem "Modal"
        cmbDefault.ListIndex = 0
    Else
        cmbDefault.Clear
        cmbDefault.AddItem "Pendapatan"
        cmbDefault.AddItem "Harga Pokok Penjualan"
        cmbDefault.AddItem "Biaya Operasional"
        cmbDefault.AddItem "Biaya Non Operasional"
        cmbDefault.AddItem "Pendapatan Lain-Lain"
        cmbDefault.AddItem "Biaya Lain-Lain"
        cmbDefault.ListIndex = 0
    End If
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where kode_parent='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
   
    conn.BeginTrans
    
    i = 1
    
    conn.Execute "delete from ms_parentAcc where [kode_parent]='" & txtField(0).text & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
    conn.RollbackTrans
    
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [kode_parent], [Nama],Jenis,kategori from ms_parentAcc " ' order by kode_Member"
    frmSearch.nmform = "frmMasterParentAcc"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_parentAcc"
        frmSearch.connstr = strcon
    
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    
    If Len(Trim(txtField(0).text)) <> 3 Then
        MsgBox "Panjang Kode Parent harus 3 karakter !", vbExclamation
        Exit Sub
    End If
    
    
    For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
    
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    rs.Open "select * from ms_parentAcc where [kode_parent]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_parentAcc where [kode_parent]='" & txtField(0).text & "'"
        
    End If
    rs.Close
    add_data
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(4)
    ReDim nilai(4)
    
    ' HT=Harta, KW=Kewajiban, MD=Modal, PD=Pendapatan, HPP=Harga Pokok Penjualan
    ' BO=Biaya Operasional, BNO=Biaya Non Operasional, PL=Pendapatan Lain-Lain, BL=Biaya Lain-lain
    
    table_name = "ms_parentAcc"
    fields(0) = "[kode_parent]"
    fields(1) = "[nama]"
    fields(2) = "jenis"
    fields(3) = "kategori"
        
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = Left(cmbJenis.text, 1)
    Select Case cmbDefault.text
        Case "Harta"
            nilai(3) = "HT"
        Case "Kewajiban"
            nilai(3) = "KW"
        Case "Modal"
            nilai(3) = "MD"
        Case "Pendapatan"
            nilai(3) = "PD"
        Case "Harga Pokok Penjualan"
            nilai(3) = "HPP"
        Case "Biaya Operasional"
            nilai(3) = "BO"
        Case "Biaya Non Operasional"
            nilai(3) = "BNO"
        Case "Pendapatan Lain-Lain"
            nilai(3) = "PL"
        Case "Biaya Lain-Lain"
            nilai(3) = "BL"
    End Select
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
'    If cmbJenis.ListIndex < 0 Then cmbJenis.ListIndex = 0
'    If cmbDefault.ListIndex < 0 Then cmbDefault.ListIndex = 0
    cmbJenis.ListIndex = 0
    cmbDefault.ListIndex = 0
    txtField(0).SetFocus
    
End Sub

Public Sub cari_data()
Dim sText As String
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_parentAcc where [kode_parent]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        SetComboText IIf(rs(2) = "N", "Neraca", "Laba Rugi"), cmbJenis
        Select Case rs(3)
            Case "HT"
                sText = "Harta"
            Case "KW"
                sText = "Kewajiban"
            Case "MD"
                sText = "Modal"
            Case "PD"
                sText = "Pendapatan"
            Case "HPP"
                sText = "Harga Pokok Penjualan"
            Case "BO"
                sText = "Biaya Operasional"
            Case "BNO"
                sText = "Biaya Non Operasional"
            Case "PL"
                sText = "Pendapatan Lain-Lain"
            Case "BL"
                sText = "Biaya Lain-Lain"
        End Select
        SetComboText sText, cmbDefault
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        
        txtField(1).text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    reset_form
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
    If Index = 0 Then Angka KeyAscii
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub

