VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmHPP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Barang"
   ClientHeight    =   3150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8190
   ClipControls    =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   8190
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5235
      Picture         =   "frmHPP.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4140
      Picture         =   "frmHPP.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.TextBox txtHarga 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1530
      TabIndex        =   2
      Top             =   1485
      Width           =   2025
   End
   Begin VB.CommandButton cmdSearchBrg 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      Picture         =   "frmHPP.frx":0A64
      TabIndex        =   10
      Top             =   1080
      Width           =   420
   End
   Begin VB.TextBox txtKdBrg 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1530
      TabIndex        =   1
      Top             =   1080
      Width           =   2025
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3712
      TabIndex        =   8
      Top             =   2205
      Width           =   1110
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   375
      Left            =   3645
      Picture         =   "frmHPP.frx":0B66
      TabIndex        =   6
      Top             =   135
      Width           =   420
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2317
      TabIndex        =   3
      Top             =   2205
      Width           =   1110
   End
   Begin VB.CommandButton cmdout 
      Caption         =   "[ESC]  &Exit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5062
      TabIndex        =   4
      Top             =   2205
      Width           =   1065
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1530
      TabIndex        =   0
      Top             =   630
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   100007939
      CurrentDate     =   38927
   End
   Begin VB.Label lblNamaBarang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4095
      TabIndex        =   12
      Top             =   1125
      Width           =   4020
   End
   Begin VB.Label Label1 
      Caption         =   "Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   11
      Top             =   1530
      Width           =   1050
   End
   Begin VB.Label Label5 
      Caption         =   "Kode Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   9
      Top             =   1125
      Width           =   1050
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1530
      TabIndex        =   7
      Top             =   135
      Width           =   2085
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   5
      Top             =   675
      Width           =   1320
   End
End
Attribute VB_Name = "frmHPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDelete_Click()
    conn.Open strcon
    conn.Execute "delete from hst_hppbahan where id='" & lblNoTrans & "'"
    conn.Close
    MsgBox "Data sudah dihapus"
    reset_form
End Sub

Private Sub cmdHapus_Click()
    DBGrid.Columns.Remove (CLng(lblCaption))
    lblCaption = ""
    txtColHeader = ""
    cmdHapus.Enabled = False
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [id] from hst_hppbahan where [id]>'" & lblNoTrans & "' order by [id]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [id] from hst_hppbahan where [id]<'" & lblNoTrans & "' order by [id] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmHPP"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang1"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    cek_kodebarang1 = False
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        lblNamaBarang = rs!nama_bahan
        cek_kodebarang1 = True
    Else
        DBGrid.Columns(1).text = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Private Sub cmdout_Click()
    Unload Me
End Sub

Private Sub cmdSearchID_Click()
On Error GoTo salah
    With frmSearch
        .connstr = strcon
        .query = "select * from search_hsthppbahan"
        .nmform = "frmHPP"
        .nmctrl = "lblNoTrans"
        .nmctrl2 = ""
        .keyIni = "hpp"
        .col = 0
        frmSearch.Index = -1
        .proc = "cek_notrans"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        
        .requery
        Set .frm = Me
        .Show vbModal
    End With
Exit Sub:
salah:
End Sub

Public Sub cek_notrans()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    Dim id As String
    conn.Open strcon
    rs.Open "select * from hst_hppbahan where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        id = rs!id
        DTPicker1 = rs!tanggal
        txtKdBrg = rs!kode_bahan
        txtHarga = rs!hpp
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
    End If
    rs.Close
    
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
On Error GoTo err
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim id As String
    Dim counter As Integer
    Dim status As Byte
    status = 0
    id = lblNoTrans
    
    
    conn.Open strcon
    conn.BeginTrans
    status = 1
    If lblNoTrans = "" Then
    lblNoTrans = newid("hst_hppbahan", "id", DTPicker1, "hpp")
    Else
    conn.Execute "delete from hst_hppbahan where id='" & lblNoTrans & "'"
    
    End If
    conn.Execute "insert into hst_hppbahan values ('" & lblNoTrans & "','" & Format(DTPicker1, "yyyy/MM/dd HH:mm:ss") & "','" & User & "','" & txtKdBrg.text & "','" & txtHarga.text & "')"
    counter = 0
    conn.CommitTrans
    status = 0
    conn.Close
    reset_form
    Exit Sub
err:
    If status = 1 Then conn.RollbackTrans
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    
    txtKdBrg = ""
    lblNamaBarang = ""
    lblNoTrans = ""
    txtHarga = "0"
    txtKdBrg.SetFocus
    
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        MySendKeys "{tab}"
        KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    reset_form
    DTPicker1 = Now
    
    
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga)
End Sub

Private Sub txtKdBrg_LostFocus()
    cek_kodebarang1
End Sub
