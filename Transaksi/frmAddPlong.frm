VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPlong 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Proses Plong"
   ClientHeight    =   9060
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   11865
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9060
   ScaleWidth      =   11865
   Begin VB.CommandButton cmdKoreksi 
      Caption         =   "&Koreksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   9555
      Style           =   1  'Graphical
      TabIndex        =   52
      Top             =   8205
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchKodeBahan 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   51
      Top             =   990
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchKodeHasil 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   47
      Top             =   2715
      Width           =   420
   End
   Begin VB.TextBox txtKodeHasil 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1650
      TabIndex        =   10
      Top             =   2700
      Width           =   1815
   End
   Begin VB.TextBox txtQtyHasil 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5040
      TabIndex        =   11
      Top             =   2700
      Width           =   990
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Master Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4170
      Picture         =   "frmAddPlong.frx":0000
      TabIndex        =   45
      Top             =   570
      Width           =   1410
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Master Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4170
      Picture         =   "frmAddPlong.frx":0102
      TabIndex        =   44
      Top             =   975
      Width           =   1410
   End
   Begin VB.TextBox txtKodeGudangOutput 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1650
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   1845
      Width           =   1815
   End
   Begin VB.CommandButton cmdSearchGudangOutput 
      BackColor       =   &H8000000C&
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      Picture         =   "frmAddPlong.frx":0204
      TabIndex        =   42
      Top             =   1845
      Width           =   420
   End
   Begin VB.CommandButton cmdHasil 
      Caption         =   "Input Hasil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   255
      TabIndex        =   41
      Top             =   8205
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5130
      Picture         =   "frmAddPlong.frx":0306
      TabIndex        =   40
      Top             =   8205
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchGudang 
      BackColor       =   &H8000000C&
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      Picture         =   "frmAddPlong.frx":0408
      TabIndex        =   39
      Top             =   1410
      Width           =   420
   End
   Begin VB.TextBox txtQty 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   4545
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1425
      Width           =   990
   End
   Begin VB.CommandButton cmdSearchSerial 
      BackColor       =   &H8000000C&
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      Picture         =   "frmAddPlong.frx":050A
      TabIndex        =   34
      Top             =   2280
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.TextBox txtNoSerial 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1650
      TabIndex        =   8
      Top             =   2280
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox txtKodeGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1650
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1410
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Caption         =   "Output Hasil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Left            =   195
      TabIndex        =   32
      Top             =   5685
      Width           =   11085
      Begin MSFlexGridLib.MSFlexGrid flxGrid2 
         Height          =   1935
         Left            =   165
         TabIndex        =   33
         Top             =   285
         Width           =   10740
         _ExtentX        =   18944
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   6
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "&Finish (F6)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   8115
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   8205
      Width           =   1230
   End
   Begin VB.TextBox txtQtyAsal 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   8475
      TabIndex        =   30
      Top             =   2910
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting (F4)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   8205
      Width           =   1230
   End
   Begin VB.Frame Frame2 
      Caption         =   "Output Standart"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Left            =   195
      TabIndex        =   26
      Top             =   3255
      Width           =   11085
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   1935
         Left            =   165
         TabIndex        =   28
         Top             =   285
         Width           =   10740
         _ExtentX        =   18944
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   8
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSearchProses 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3540
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   24
      Top             =   570
      Width           =   420
   End
   Begin VB.TextBox txtKodeProses 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1650
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   570
      Width           =   1815
   End
   Begin VB.CommandButton cmdSearchMesin 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   9540
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   22
      Top             =   1410
      Width           =   420
   End
   Begin VB.TextBox txtKodeMesin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   7935
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   1410
      Width           =   1530
   End
   Begin VB.TextBox txtKodeBahan 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1650
      TabIndex        =   5
      Top             =   990
      Width           =   1815
   End
   Begin VB.TextBox txtKodeOperator 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   7935
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   570
      Width           =   1530
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10890
      Top             =   -240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSearchOperator 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   9525
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   19
      Top             =   570
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3540
      Picture         =   "frmAddPlong.frx":060C
      TabIndex        =   17
      Top             =   135
      Width           =   420
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   990
      Left            =   7935
      MaxLength       =   50
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   1830
      Width           =   3525
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3630
      Picture         =   "frmAddPlong.frx":070E
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   8205
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2085
      Picture         =   "frmAddPlong.frx":0810
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   8190
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   7935
      TabIndex        =   1
      Top             =   135
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   48824323
      CurrentDate     =   38927
   End
   Begin VB.Label Label6 
      Caption         =   "Kode Hasil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   50
      Top             =   2745
      Width           =   1410
   End
   Begin VB.Label lblSatuanHasil 
      Caption         =   "--"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6120
      TabIndex        =   49
      Top             =   2760
      Width           =   900
   End
   Begin VB.Label Label8 
      Caption         =   "Std Hasil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4125
      TabIndex        =   48
      Top             =   2745
      Width           =   840
   End
   Begin VB.Label LblNamaBahan 
      Caption         =   "--"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4740
      TabIndex        =   46
      Top             =   105
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "Gudang Output"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   43
      Top             =   1860
      Width           =   1395
   End
   Begin VB.Label Label3 
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4125
      TabIndex        =   38
      Top             =   1470
      Width           =   345
   End
   Begin VB.Label Label10 
      Caption         =   "No. Serial"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   37
      Top             =   2325
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.Label Label5 
      Caption         =   "Dari Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   36
      Top             =   1425
      Width           =   1395
   End
   Begin VB.Label lblSatuan 
      Caption         =   "--"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5625
      TabIndex        =   35
      Top             =   1470
      Width           =   1035
   End
   Begin VB.Label Label2 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   27
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label9 
      Caption         =   "Kode Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   25
      Top             =   615
      Width           =   1410
   End
   Begin VB.Label Label7 
      Caption         =   "Mesin"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6840
      TabIndex        =   23
      Top             =   1455
      Width           =   1410
   End
   Begin VB.Label lblNamaOperator 
      Caption         =   "--"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7965
      TabIndex        =   21
      Top             =   1050
      Width           =   3495
   End
   Begin VB.Label Label14 
      Caption         =   "Kode Bahan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   20
      Top             =   1035
      Width           =   1305
   End
   Begin VB.Label Label19 
      Caption         =   "Operator"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6840
      TabIndex        =   18
      Top             =   615
      Width           =   1320
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6840
      TabIndex        =   16
      Top             =   225
      Width           =   1020
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6840
      TabIndex        =   15
      Top             =   1845
      Width           =   1095
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1710
      TabIndex        =   14
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPlong"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim foc As Byte
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Public JenisTransaksi As String

Private Sub nomor_baru()
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select top 1 nomer_Plong from t_Plongh where substring(nomer_Plong,3,2)='" & Format(DTPicker1, "yy") & "' and substring(nomer_Plong,5,2)='" & Format(DTPicker1, "MM") & "' order by nomer_Plong desc", conn
    If Not rs.EOF Then
        No = "PL" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "PL" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    conn.Close
End Sub


Private Sub cmdFinish_Click()
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double, harga As Double, HPPBaru As Double

On Error GoTo err
    
    conn.Open strcon
'    conn2.Open strcon
    rs.Open "select sum(qty_manual) as totaljumlah from t_Plongd where nomer_Plong='" & lblNoTrans & "'", conn
    If Not rs.EOF And IsNull(rs("totaljumlah")) = False Then
        '
    Else
        MsgBox "Total output nol, proses tidak dapat dilanjutkan !"
        Exit Sub
    End If
    rs.Close

    conn.BeginTrans
    
'    For row = 1 To flxGrid.Rows - 2
'        conn.Execute "Update t_Plongd set qty_manual=" & Replace(flxGrid.TextMatrix(row, 6), ",", ".") & ", " & _
'                     "selisih=" & Replace(flxGrid.TextMatrix(row, 7), ",", ".") & " " & _
'                     "where kode_bahan='" & flxGrid.TextMatrix(row, 1) & "' " & _
'                     "and nomer_Plong='" & lblNoTrans & "'"
'    Next
    
    Call Finish_Plong(lblNoTrans, conn)

    conn.CommitTrans
    
    DropConnection

    MsgBox "Proses selesai"
    reset_form

    Call MySendKeys("{tab}")
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub

Private Sub cmdHasil_Click()
    frmAddPenerimaanHasilPlong.txtNoPlong = lblNoTrans
    frmAddPenerimaanHasilPlong.Cek_Plong
    frmAddPenerimaanHasilPlong.Show vbModal
    
    lblNoTrans = "-"
    txtNoSerial = ""
    Cek_Serial
    flxGrid2.Rows = 1
    flxGrid2.Rows = 2
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub cmdKoreksi_Click()
    Koreksi (lblNoTrans)
End Sub

Private Sub cmdPosting_Click()
    If simpan Then
        Call Posting_Plong(lblNoTrans, conn)
        cmdPosting.Enabled = False
        cmdHasil.Visible = True
    End If
End Sub



Private Sub cmdreset_Click()
    reset_form
End Sub



Private Sub cmdSearchGudangOutput_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_gudang,nama_gudang from ms_gudang where status='0'"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodeGudangOutput"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_gudang"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "Cek_Gudang_Output"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Gudang_Output()
    '
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
'    If JenisTransaksi = "0" Then
'        frmSearch.query = SearchPlong & " where status_posting='0'"
'    Else
'        frmSearch.query = SearchPlong & " where status_posting='1' and status_finish='0'"
'    End If
    frmSearch.query = SearchPlong
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "plongH"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKodeHasil_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_bahan,nama_bahan from ms_bahan where kategori like '%BARANG JADI%'"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodeHasil"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "Cek_Hasil"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Hasil()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select b.kode_bahan,b.nama_bahan,b.satuan from ms_bahan b " & _
            "where b.kode_bahan='" & txtKodehasil.text & "'", conn
    If Not rs.EOF Then
        
        lblSatuanHasil = rs("satuan")
        
    End If
    If rs.State Then rs.Close
    conn.Close
Exit Sub
err:
    DropConnection
    MsgBox err.Description
End Sub

Private Sub InputHasil()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select b.kode_bahan,b.nama_bahan,b.satuan from ms_bahan b " & _
            "where b.kode_bahan='" & txtKodehasil.text & "'", conn
    If Not rs.EOF Then
        
        lblSatuanHasil = rs("satuan")
        
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        If Trim(txtQtyHasil.text) = "" Then txtQtyHasil.text = 0
        flxGrid.TextMatrix(row, 1) = rs(0)
        flxGrid.TextMatrix(row, 2) = rs(1)
        flxGrid.TextMatrix(row, 3) = CDbl(txtQtyHasil.text)
        flxGrid.TextMatrix(row, 4) = rs(2)
        flxGrid.TextMatrix(row, 5) = CDbl(txtQtyHasil.text)
'        flxGrid.TextMatrix(row, 8) = 1
        row = row + 1
        flxGrid.Rows = flxGrid.Rows + 1
            
    End If
    If rs.State Then rs.Close
    conn.Close
Exit Sub
err:
    DropConnection
    MsgBox err.Description
End Sub
Private Sub cmdSearchProses_Click()
    frmSearch.query = SearchMasterProses & " where jenis_proses='Plong'"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodeProses"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_proses"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "Cek_Proses"

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Proses()
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select p.*, b.nama_bahan,b.satuan from ms_prosesh p " & _
            "inner join ms_bahan b on b.kode_bahan=p.kode_bahan " & _
            "where p.kode_proses='" & txtKodeProses.text & "'", conn
    If Not rs.EOF Then
        
        lblSatuan = rs("satuan")
        txtNoSerial.text = ""
        txtKodeGudang.text = ""
        txtKodehasil.text = ""
        txtQtyHasil.text = "0"
        lblSatuanHasil.Caption = ""
        
        txtKodeBahan.text = rs("kode_bahan")
        LblNamaBahan.Caption = rs("nama_bahan")
        txtQty.text = rs("Qty")
        txtQtyAsal.text = rs("Qty")
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.kode_bahan,b.nama_bahan,d.qty_hasil,b.satuan from ms_prosesd d " & _
                "inner join ms_bahan b on d.kode_bahan=b.kode_bahan " & _
                "where d.kode_proses='" & txtKodeProses.text & "' order by d.no_urut", conn
        While Not rs.EOF
            flxGrid.TextMatrix(row, 1) = rs(0)
            flxGrid.TextMatrix(row, 2) = rs(1)
            flxGrid.TextMatrix(row, 3) = rs(2)
            flxGrid.TextMatrix(row, 4) = rs(3)
            flxGrid.TextMatrix(row, 5) = rs(2)
            row = row + 1
            flxGrid.Rows = flxGrid.Rows + 1
            rs.MoveNext
        Wend
        rs.Close
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub cmdSearchSerial_Click()
    
    If Trim(txtKodeGudang.text) = "" Then
        MsgBox "Pilih gudang dulu !", vbExclamation
        txtKodeGudang.SetFocus
        Exit Sub
    End If
    
    frmSearch.query = searchProsesStock & " where kode_bahan='" & txtKodeBahan.text & "' and kode_gudang='" & txtKodeGudang.text & "'"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtNoSerial"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "stock"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "Cek_Serial"

    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Serial()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Byte, FaktorKali As Double
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select stock from stock where kode_bahan='" & txtKodeBahan & "' and nomer_serial='" & txtNoSerial & "' and kode_gudang='" & txtKodeGudang.text & "'", conn
    If Not rs.EOF Then
        txtQty = rs("stock")
        If CDbl(txtQtyAsal.text) > 0 Then
            FaktorKali = CDbl(txtQty.text) / CDbl(txtQtyAsal.text)
            For i = 1 To flxGrid.Rows - 2
                 flxGrid.TextMatrix(i, 3) = flxGrid.TextMatrix(i, 5) * FaktorKali
            Next i
        End If
    Else
        txtQty.text = "0"
    End If
    rs.Close
    conn.Close
Exit Sub
err:

MsgBox err.Description
End Sub


Private Sub cmdSearchOperator_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_operator,nama_operator from ms_operator"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodeOperator"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_operator"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "Cek_Operator"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Operator()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select nama_operator from ms_operator where kode_operator='" & txtKodeOperator & "'", conn
    If Not rs.EOF Then
        lblNamaOperator = rs(0)
    Else
        lblNamaOperator = ""
    End If
    rs.Close
    conn.Close

err:
End Sub

Private Sub cmdSearchGudang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_gudang from stock where kode_bahan='" & txtKodeBahan.text & "' and stock>0 group by kode_gudang"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodeGudang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "stock"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "Cek_Gudang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Gudang()
    'Cek_Gudang
End Sub

Private Sub cmdSearchMesin_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_gudang as kode_mesin,nama_gudang as nama_mesin from ms_gudang where status='1' and jenis_mesin='Plong'"
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodemesin"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_gudang"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "Cek_Mesin"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Mesin()
    'Cek_Mesin
End Sub


Private Sub cmdSimpan_Click()
On Error Resume Next
    If simpan Then
        MsgBox "Data sudah tersimpan"
'        reset_form
'        Call MySendKeys("{tab}")
        lblNoTrans = "-"
        txtNoSerial = ""
        Cek_Serial
        flxGrid2.Rows = 1
        flxGrid2.Rows = 2
        flxGrid.TextMatrix(1, 3) = "0"
        txtNoSerial.SetFocus
    End If
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim row As Integer
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double, harga As Double, HPPBaru As Double

On Error GoTo err
    i = 0
    simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan detail bahan terlebih dahulu"
        txtKodeBahan.SetFocus
        Exit Function
    End If
    
    If txtNoSerial.text = "" Then
        MsgBox "Silahkan masukkan Nomor Serial Bahan terlebih dahulu"
        txtNoSerial.SetFocus
        Exit Function
    End If
    
    If txtKodeOperator.text = "" Then
        MsgBox "Silahkan masukkan Operator terlebih dahulu"
        txtKodeOperator.SetFocus
        Exit Function
    End If
    
    If txtNoSerial.text = "" Then
        MsgBox "Silahkan masukkan Nomer Serial terlebih dahulu"
        txtNoSerial.SetFocus
        Exit Function
    End If
    
    If txtKodeGudangOutput.text = "" Then
        MsgBox "Silahkan masukkan Kode Gudang Output terlebih dahulu"
        txtKodeGudangOutput.SetFocus
        Exit Function
    End If

    
    If txtKodeMesin.text = "" Then
        MsgBox "Silahkan masukkan Kode Mesin terlebih dahulu"
        txtKodeMesin.SetFocus
        Exit Function
    End If

    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        conn.Execute "delete from t_Plongd where nomer_Plong='" & lblNoTrans & "'"
        conn.Execute "delete from t_Plongh where nomer_Plong='" & lblNoTrans & "'"
    End If

    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next

    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Function



Public Sub reset_form()
    lblNoTrans = "-"
    txtKodeProses.text = ""
    txtKodeBahan.text = ""
    txtKodehasil.text = ""
    txtQtyHasil = ""
    LblNamaBahan = ""
    txtNoSerial.text = ""
    txtQty.text = "0"
        
    
    txtKeterangan.text = ""
    txtQtyAsal.text = "0"
    DTPicker1 = Now
    
    cmdSimpan.Enabled = True
'    cmdPosting.Enabled = False
    cmdHasil.Visible = False
    cmdFinish.Enabled = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxGrid2.Rows = 1
    flxGrid2.Rows = 2
End Sub

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(17)
    ReDim nilai(17)
    table_name = "t_Plongh"
    fields(0) = "nomer_Plong"
    fields(1) = "tanggal"
    fields(2) = "kode_proses"
    fields(3) = "kode_bahan"
    fields(4) = "nomer_serial"
    fields(5) = "qty"
    fields(6) = "kode_operator"
    fields(7) = "kode_gudang"
    fields(8) = "kode_mesin"
    fields(9) = "keterangan"
    fields(10) = "qty_asal"
    fields(11) = "userid"
    fields(12) = "no_history"
    fields(13) = "serial_history"
    fields(14) = "kode_history"
    fields(15) = "hpp"
    fields(16) = "kode_gudang_output"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKodeProses.text
    nilai(3) = txtKodeBahan.text
    nilai(4) = txtNoSerial.text
    nilai(5) = Replace(txtQty.text, ",", ".")
    nilai(6) = txtKodeOperator.text
    nilai(7) = txtKodeGudang.text
    nilai(8) = txtKodeMesin.text
    nilai(9) = txtKeterangan.text
    nilai(10) = Replace(txtQtyAsal.text, ",", ".")
    nilai(11) = User
    nilai(12) = getNoHistory(txtKodeBahan.text, txtKodeGudang.text, txtNoSerial.text, conn)
    nilai(13) = getSerialHistory(txtKodeBahan.text, txtKodeGudang.text, txtNoSerial.text, conn)
    nilai(14) = getKodeHistory(txtKodeBahan.text, txtKodeGudang.text, txtNoSerial.text, conn)
    
    nilai(15) = Replace(Format(GetHPPKertas2(txtKodeBahan.text, txtNoSerial.text, txtKodeGudang.text, conn), "###0.####"), ",", ".")
    nilai(16) = txtKodeGudangOutput.text
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_Plongd"
    fields(0) = "nomer_Plong"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "no_urut"
    fields(4) = "qty_asal"

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = Replace(flxGrid.TextMatrix(row, 3), ",", ".")
    nilai(3) = row
    nilai(4) = Replace(flxGrid.TextMatrix(row, 5), ",", ".")
    

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub


Public Sub cek_notrans()
Dim StatusPosting As String, StatusFinish As String
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select t.*,b.nama_bahan,r.nama_operator from t_Plongh t " & _
            "left join ms_bahan b on t.kode_bahan=b.kode_bahan " & _
            "left join ms_operator r on t.kode_operator=r.kode_operator " & _
            "where t.nomer_Plong='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKodeProses.text = rs("kode_proses")
        txtKodeBahan.text = rs("kode_bahan")
        LblNamaBahan = rs("nama_bahan")
        txtNoSerial.text = rs("nomer_serial")
        txtQty.text = rs("qty")
        txtKodeOperator.text = rs("kode_operator")
        lblNamaOperator = rs("nama_operator")
        txtKodeGudang.text = rs("kode_gudang")
        txtKodeGudangOutput.text = rs("kode_gudang_output")
        txtKodeMesin.text = rs("kode_mesin")
        txtKeterangan.text = rs("keterangan")
        txtQtyAsal.text = rs("qty_asal")
        StatusPosting = rs("status_posting")
        StatusFinish = rs("status_finish")
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.kode_bahan,b.nama_bahan,d.qty,b.satuan,d.qty_asal,isnull(a.output,0) as output from t_Plongd d " & _
                "inner join ms_bahan b on d.kode_bahan=b.kode_bahan " & _
                "Left Join (Select x.kode_bahan, sum(x.qty) as output from t_hasilPlongd x " & _
                "inner join t_hasilPlongh y on y.nomer_hasilPlong=x.nomer_hasilPlong " & _
                "where y.nomer_Plong='" & lblNoTrans & "' and y.status_posting='1' group by x.kode_bahan) a on a.kode_bahan=d.kode_bahan " & _
                "where d.nomer_Plong='" & lblNoTrans & "' order by d.no_urut", conn
        While Not rs.EOF
            flxGrid.TextMatrix(row, 1) = rs(0)
            flxGrid.TextMatrix(row, 2) = rs(1)
            flxGrid.TextMatrix(row, 3) = rs(2)
            flxGrid.TextMatrix(row, 4) = rs(3)
            flxGrid.TextMatrix(row, 5) = rs(4)
            flxGrid.TextMatrix(row, 6) = rs(5)
'            flxGrid.TextMatrix(row, 7) = rs(2) - rs(5)
            flxGrid.TextMatrix(row, 7) = rs(5) - rs(2)
            row = row + 1
            flxGrid.Rows = flxGrid.Rows + 1
            rs.MoveNext
        Wend
        rs.Close
        
        flxGrid2.Rows = 1
        flxGrid2.Rows = 2
        row = 1

        rs.Open "select d.kode_bahan,b.nama_bahan,d.nomer_serial,sum(d.qty),b.satuan from t_hasilPlongd d " & _
                "inner join ms_bahan b on d.kode_bahan=b.kode_bahan " & _
                "inner join t_hasilPlongh t on t.nomer_hasilPlong=d.nomer_hasilPlong " & _
                "where t.nomer_Plong='" & lblNoTrans & "' and t.status_posting='1' and d.qty>0 group by d.kode_bahan,b.nama_bahan,d.nomer_serial,b.satuan ", conn
        While Not rs.EOF
            flxGrid2.TextMatrix(row, 1) = rs(0)
            flxGrid2.TextMatrix(row, 2) = rs(1)
            flxGrid2.TextMatrix(row, 3) = rs(2)
            flxGrid2.TextMatrix(row, 4) = rs(3)
            flxGrid2.TextMatrix(row, 5) = rs(4)
            row = row + 1
            flxGrid2.Rows = flxGrid2.Rows + 1
            rs.MoveNext
        Wend
        rs.Close
        
        If StatusPosting = "1" Then
            cmdPosting.Enabled = False
            cmdKoreksi.Enabled = True
            cmdSimpan.Enabled = False
            cmdFinish.Enabled = True
        Else
            cmdPosting.Enabled = True
            cmdKoreksi.Enabled = False
            cmdSimpan.Enabled = True
            cmdFinish.Enabled = False
        End If
        If StatusFinish = "1" Then
            cmdFinish.Enabled = False
        End If
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub




Private Sub Command1_Click()
    frmMasterProses.Show vbModal
End Sub

Private Sub Command2_Click()
    frmMasterBarang.Show vbModal
End Sub

Private Sub cmdSearchKodeBahan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_bahan,nama_bahan from ms_bahan "
    frmSearch.nmform = "frmAddPlong"
    frmSearch.nmctrl = "txtKodeBahan"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "Cek_Bahan"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Public Sub Cek_Bahan()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select satuan from ms_bahan where kode_bahan='" & txtKodeBahan & "'", conn
    If Not rs.EOF Then
        lblSatuan = rs(0)
        txtNoSerial = txtKodeBahan.text
    Else
        lblSatuan = ""
        txtNoSerial.text = ""
    End If
    rs.Close
    conn.Close

err:
End Sub

Private Sub Command3_Click()

End Sub

Private Sub DTPicker1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then MySendKeys "{tab}"
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub

Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF4 Then cmdPosting_Click
    If KeyCode = vbKeyF6 Then cmdFinish_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKodeBahan.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    reset_form
    total = 0
    txtKodeGudang.text = ""
    txtKodeGudangOutput.text = ""
    DTPicker1 = Now
    txtKodeMesin.text = ""
    txtKodeOperator.text = ""
    lblNamaOperator = ""
        
    
    conn.ConnectionString = strcon
    conn.Open
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then

    'End If
    'rs.Close
    conn.Close
    'newitem rs("kodebarang"), rs("namabarang"), rs("disc1"), rs("disc2"), rs("disc3"), rs("disc4"), rs("disc5"),  rs("hargajual")
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1500
    flxGrid.ColWidth(2) = 4000
    flxGrid.ColWidth(3) = 1200
    flxGrid.ColWidth(4) = 900
    flxGrid.ColWidth(5) = 0
    flxGrid.ColWidth(6) = 1200
    flxGrid.ColWidth(7) = 1200
    
    flxGrid.TextMatrix(0, 1) = "kode"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "nama"
    flxGrid.TextMatrix(0, 3) = "Qty Standart"
    flxGrid.TextMatrix(0, 4) = "Satuan"
    flxGrid.TextMatrix(0, 5) = "Qty Asal"
    flxGrid.TextMatrix(0, 6) = "Qty Manual"
    flxGrid.TextMatrix(0, 7) = "Selisih"
    
    
    flxGrid2.ColWidth(0) = 300
    flxGrid2.ColWidth(1) = 1500
    flxGrid2.ColWidth(2) = 4000
    flxGrid2.ColWidth(3) = 1500
    flxGrid2.ColWidth(4) = 1200
    flxGrid2.ColWidth(5) = 900
    
    flxGrid2.TextMatrix(0, 1) = "kode"
    flxGrid2.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid2.TextMatrix(0, 2) = "nama"
    flxGrid2.TextMatrix(0, 3) = "no serial"
    flxGrid2.TextMatrix(0, 4) = "Qty"
    flxGrid2.TextMatrix(0, 5) = "Satuan"
    
    If JenisTransaksi = "0" Then
        cmdFinish.Visible = False
        Frame1.Visible = False
        flxGrid.ColWidth(6) = 0
        flxGrid.ColWidth(7) = 0
        cmdSimpan.Left = cmdSimpan.Left + 795
        cmdKeluar.Left = cmdKeluar.Left + 795
        cmdPosting.Left = cmdPosting.Left + 795
        cmdReset.Left = cmdReset.Left + 795
        cmdHasil.Left = cmdHasil.Left + 795
        cmdSimpan.top = cmdSimpan.top - 2400
        cmdKoreksi.top = cmdKoreksi.top - 2400
        cmdKeluar.top = cmdKeluar.top - 2400
        cmdPosting.top = cmdPosting.top - 2400
        cmdReset.top = cmdReset.top - 2400
        cmdHasil.top = cmdHasil.top - 2400
        Me.Height = 7200
    Else
        cmdSimpan.Visible = False
        cmdPosting.Visible = False
        cmdSearchProses.Visible = False
        cmdSearchSerial.Visible = False
        cmdSearchOperator.Visible = False
        cmdSearchMesin.Visible = False
        txtQty.Enabled = False
        txtKeterangan.Enabled = False
        cmdFinish.Left = cmdPosting.Left
        Me.Caption = "Finishing Plong"
    End If


End Sub


Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub txtKodeBahan_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchKodeBahan_Click
End Sub
Private Sub txtKodehasil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchKodeHasil_Click
End Sub

Private Sub txtKodeGudang_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchGudang_Click
    If KeyAscii = 13 Then Cek_Serial
End Sub

Private Sub txtKodeGudang_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Cek_Gudang
End Sub

Private Sub txtKodeGudang_LostFocus()
    Cek_Gudang
    Cek_Serial
End Sub

Private Sub txtKodeGudangOutput_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchGudangOutput_Click
    
End Sub

Private Sub txtKodeGudangOutput_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Cek_Gudang_Output
End Sub

Private Sub txtKodeGudangOutput_LostFocus()
    Cek_Gudang_Output
    
End Sub

Private Sub txtKodeMesin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchMesin_Click
End Sub

Private Sub txtKodeMesin_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Cek_Mesin
End Sub

Private Sub txtKodeMesin_LostFocus()
    Cek_Mesin
End Sub

Private Sub txtKodeOperator_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchOperator_Click
End Sub

Private Sub txtKodeOperator_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Cek_Operator
End Sub

Private Sub txtKodeOperator_LostFocus()
    Cek_Operator
End Sub

Private Sub txtKodeProses_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchProses_Click
End Sub

Private Sub txtKodeProses_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Cek_Operator
End Sub

Private Sub txtKodeProses_LostFocus()
    Cek_Operator
End Sub

Private Sub txtNoSerial_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSerial_Click
End Sub

Private Sub txtNoSerial_LostFocus()
    Cek_Serial
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtQty = calc(txtQty)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angkacalc KeyAscii
End Sub

Private Sub txtQty_LostFocus()
Dim FaktorKali As Double
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
    
    If CDbl(txtQtyAsal.text) > 0 And Trim(txtKodeProses.text) <> "" Then
        FaktorKali = CDbl(txtQty.text) / CDbl(txtQtyAsal.text)
            
        For i = 1 To flxGrid.Rows - 2
             flxGrid.TextMatrix(i, 3) = flxGrid.TextMatrix(i, 5) * FaktorKali
        Next i
    End If
End Sub

Private Sub txtQtyHasil_KeyPress(KeyAscii As Integer)
    Angkacalc KeyAscii
End Sub

Private Sub txtQtyHasil_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        txtQtyHasil = calc(txtQtyHasil)
        InputHasil
        txtKodeOperator.SetFocus
    End If
End Sub

Sub Koreksi(NoTransaksi As String)
Dim i As Integer
Dim id As String
Dim row As Integer
Dim jumlah As Double, hpp As Double
'Dim rs As New ADODB.Recordset
Dim KodeBahan As String, KodeGudang As String, NoSerial As String, KodeMesin As String
Dim tanggal As Date, stockawal As Double
Dim NoHistory As String

'On Error GoTo err

    conn.Open strcon
    rs.Open "select tanggal,kode_mesin,kode_bahan,kode_gudang,nomer_serial,qty from t_Plongh where nomer_Plong='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    KodeBahan = rs("kode_bahan")
    KodeMesin = rs("kode_mesin")
    KodeGudang = rs("kode_gudang")
    NoSerial = rs("nomer_serial")
    jumlah = rs("qty")
    rs.Close
    NoHistory = getNoHistory(KodeBahan, KodeGudang, NoSerial, conn)
    If Trim(NoHistory) = "" Or NoHistory = "0" Or NoHistory = "-" Then
        NoHistory = NoTransaksi
    End If
    hpp = GetHPPKertas2(KodeBahan, NoSerial, KodeGudang, conn)
        
    conn.BeginTrans
    
    'Tambah stock di gudang asal
    
    conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & KodeBahan & "'and nomer_serial='" & NoSerial & "'  and kode_gudang='" & KodeGudang & "') insert into stock (kode_bahan,stock,nomer_serial,kode_gudang) values ('" & KodeBahan & "'," & Replace(jumlah, ",", ".") & ",'" & NoSerial & "','" & KodeGudang & "') else " & _
                "update stock set stock=stock + " & Replace(jumlah, ",", ".") & " where kode_bahan='" & KodeBahan & "' and nomer_serial='" & NoSerial & "' and kode_gudang='" & KodeGudang & "'"
    
    'Kurangi stock di gudang tujuan (mesin)
    rs.Open "select * from stock where kode_bahan='" & KodeBahan & "' " & _
            "and kode_gudang='" & KodeMesin & "' and nomer_serial='" & NoSerial & "' ", conn
    If rs.EOF Then
        
        conn.Execute "insert into stock  (kode_gudang,kode_bahan,nomer_serial,stock) " & _
                     "values ('" & KodeMesin & "','" & KodeBahan & "' ,'" & NoSerial & "'," & Replace(-jumlah, ",", ".") & ")"
        rs.Close
    Else
        conn.Execute "update stock set no_history='" & NoHistory & "', stock=stock - " & Replace(jumlah, ",", ".") & " where kode_bahan='" & KodeBahan & "' " & _
                     "and kode_gudang='" & KodeMesin & "' and nomer_serial='" & NoSerial & "' "
    End If
    If rs.State Then rs.Close
    
    conn.Execute "delete from t_Plongd where nomer_Plong='" & lblNoTrans & "'"
    conn.Execute "delete from t_Plongh where nomer_Plong='" & lblNoTrans & "'"

    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    
    rs.Open "select tanggal,kode_mesin,kode_bahan,kode_gudang,nomer_serial,qty from t_Plongh where nomer_Plong='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    KodeBahan = rs("kode_bahan")
    KodeMesin = rs("kode_mesin")
    KodeGudang = rs("kode_gudang")
    NoSerial = rs("nomer_serial")
    jumlah = rs("qty")
    rs.Close
    
    conn.Execute "update tmp_kartustockkertas set kode_bahan='" & KodeBahan & "',kode_gudang='" & KodeGudang & "',nomer_serial='" & NoSerial & "',keluar=" & jumlah & " " & _
                 "where tipe='Proses Plong' and id='" & NoTransaksi & "' and keluar>0"
                 
    conn.Execute "update tmp_kartustockkertas set kode_bahan='" & KodeBahan & "',kode_gudang='" & KodeMesin & "',nomer_serial='" & NoSerial & "',masuk=" & jumlah & " " & _
                 "where tipe='Proses Plong' and id='" & NoTransaksi & "' and masuk>0"
                 

    HitungUlang_KartuStock KodeBahan, KodeGudang, NoSerial, conn
    HitungUlang_KartuStock KodeBahan, KodeMesin, NoSerial, conn

    
    conn.CommitTrans
    
    DropConnection
    MsgBox "Proses koreksi sukses"
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub
