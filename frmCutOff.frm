VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCutOff 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cut Off Transaksi"
   ClientHeight    =   1860
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5145
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   5145
   Begin MSComDlg.CommonDialog cmnDialog 
      Left            =   4200
      Top             =   300
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCutOff 
      Caption         =   "Cut Off"
      Height          =   435
      Left            =   2010
      TabIndex        =   1
      Top             =   570
      Width           =   1125
   End
   Begin MSComCtl2.DTPicker dtpCutOff 
      Height          =   315
      Left            =   1860
      TabIndex        =   0
      Top             =   150
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CustomFormat    =   "dd MMM yyyy"
      Format          =   138412035
      CurrentDate     =   42909
   End
   Begin MSComctlLib.ProgressBar PBTotalProses 
      Height          =   345
      Left            =   120
      TabIndex        =   2
      Top             =   1380
      Width           =   4905
      _ExtentX        =   8652
      _ExtentY        =   609
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "%"
      Height          =   225
      Left            =   4860
      TabIndex        =   5
      Top             =   1140
      Width           =   165
   End
   Begin VB.Label lblNamaProses 
      AutoSize        =   -1  'True
      Caption         =   "-"
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   1140
      Width           =   765
   End
   Begin VB.Label lblPersentase 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      Height          =   225
      Left            =   4170
      TabIndex        =   3
      Top             =   1140
      Width           =   675
   End
End
Attribute VB_Name = "frmCutOff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCutOff_Click()
    frmMain.Timer1.Enabled = False
    lblPersentase = 0
    PBTotalProses.Max = 100
    If BackupDatabase = False Then
        lblNamaProses = "-"
        Exit Sub
    End If
    lblPersentase = 20
    ExecuteCutOff
    frmMain.Timer1.Enabled = True
End Sub

Private Function BackupDatabase() As Boolean
    lblNamaProses.Caption = "Backup Database"
    On Error GoTo err
    BackupDatabase = False
    cmnDialog.DialogTitle = "Select a directory to save" 'titlebar
    cmnDialog.InitDir = "D:\3PM\database\"
    cmnDialog.filename = "Backup_" & dbname1 & "_" & Format(Now, "yyyyMMdd") 'Something in filenamebox
    cmnDialog.DefaultExt = "bak"
    cmnDialog.filter = "*.bak"
    cmnDialog.CancelError = True 'allow escape key/cancel
    cmnDialog.ShowSave   'show the dialog screen
    
    If err <> 32755 Then     ' User didn't chose Cancel.
        conn.Open strcon
        conn.Execute "BACKUP DATABASE [" & dbname1 & "] TO  DISK = N'" & cmnDialog.filename & "' WITH NOFORMAT, NOINIT,  NAME = N'" & dbname1 & "-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10"
        conn.Close
        BackupDatabase = True
    End If
    
    Exit Function
err:
    MsgBox err.Description
    If conn.State Then conn.Close
    BackupDatabase = False
End Function

Private Sub ExecuteCutOff()
'On Error GoTo err
    Dim trans As Boolean
    Dim query As String
    
    conn.Open strcon
    conn.BeginTrans
    trans = True
    
    'Generate stock awal
    lblNamaProses = "generate stock awal"
    'Update stock ALL
    conn.Execute "update stock set stock = (select SUM(masuk-keluar) from tmp_kartustock where kode_bahan = stock.kode_bahan " & _
                 "and kode_gudang = stock.kode_gudang and nomer_serial = stock.nomer_serial), qty = (select SUM(qtymasuk-qtykeluar) " & _
                 "from tmp_kartustock where kode_bahan = stock.kode_bahan and kode_gudang = stock.kode_gudang and nomer_serial = stock.nomer_serial)"
    lblPersentase = 25
    'Generate stock awal
    conn.Execute "exec sp_stockawal '', '', '', '" & Format(dtpCutOff.value, "yyyyMMdd") & "'"
    lblPersentase = 30
    'Insert Stock Awal
    conn.Execute "set IDENTITY_INSERT tmp_kartustock ON insert into tmp_kartustock " & _
                 "(kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,qtyawal,qtymasuk,qtykeluar,tanggal_posting,nomer_urut,hpp) " & _
                 "select kode_bahan,nomer_serial,'StockAwal' tipe,kode_gudang,'StockAwal' id,'" & dtpCutOff.value & "' tanggal, " & _
                 "(case when stockawal>=0 then stockawal else 0 end) masuk,(case when stockawal<0 then stockawal*-1 else 0 end) keluar, " & _
                 "0 stockawal,0 qtyawal,(case when qtyawal>=0 then qtyawal else 0 end) qtymasuk,(case when qtyawal<0 then qtyawal*-1 else 0 end) qtykeluar, " & _
                 "'" & dtpCutOff.value & "' tanggal_posting, ROW_NUMBER() over(order by kode_bahan) nomer_urut, (select top 1 hpp from tmp_kartustock " & _
                 "where kode_bahan = tmp_stockawal.kode_bahan and nomer_serial = tmp_stockawal.nomer_serial and convert(varchar,tanggal,112) <= '" & _
                 Format(dtpCutOff.value, "yyyyMMdd") & "' order by tanggal desc) as hpp " & _
                 "from tmp_stockawal where stockawal <> 0 or qtyawal <> 0 set IDENTITY_INSERT tmp_kartustock OFF"
    'Insert Saldo Awal Hutang
    lblNamaProses = "generate saldo awal hutang"
    conn.Execute "set identity_insert tmp_kartuhutang on insert into tmp_kartuhutang " & _
                 "(kode_supplier,tipe,id,tanggal,debet,kredit,saldoawal,no_urut) " & _
                 "select kode_supplier,'SaldoAwal' tipe,'SaldoAwal' id,'" & dtpCutOff.value & "' tanggal,(case when sum(debet-kredit) >= 0 then sum(debet-kredit) else 0 end) debet " & _
                 ",(case when sum(debet-kredit)<0 then sum(debet-kredit)*-1 else 0 end) kredit,0 saldoawal, ROW_NUMBER() over(order by kode_supplier) nomer_urut " & _
                 "from tmp_kartuhutang where CONVERT(varchar,tanggal,112) <= '" & Format(dtpCutOff.value, "yyyyMMdd") & "' " & _
                 "group by kode_supplier Having Sum(debet - kredit) <> 0 " & _
                 "set identity_insert tmp_kartuhutang off"

    'Insert Saldo Awal Piutang
    lblNamaProses = "generate saldo awal piutang"
    delete_single "tmp_kartupiutang", , "tipe = 'Bayar Piutang (Retur Jual)'"
    conn.Execute "set identity_insert tmp_kartupiutang on insert into tmp_kartupiutang " & _
                 "(kode_customer,tipe,id,tanggal,debet,kredit,saldoawal,no_urut) " & _
                 "select kode_customer,'SaldoAwal' tipe,'SaldoAwal' id,'" & dtpCutOff.value & "' tanggal,(case when sum(debet-kredit) >= 0 then sum(debet-kredit) else 0 end) debet " & _
                 ",(case when sum(debet-kredit)<0 then sum(debet-kredit)*-1 else 0 end) kredit,0 saldoawal, ROW_NUMBER() over(order by kode_customer) nomer_urut " & _
                 "from tmp_kartupiutang where CONVERT(varchar,tanggal,112) <= '" & Format(dtpCutOff.value, "yyyyMMdd") & "' " & _
                 "group by kode_customer Having Sum(debet - kredit) <> 0 " & _
                 "set identity_insert tmp_kartupiutang off"
    lblPersentase = 40
    
    query = delete_single("absen_karyawan")
    query = query & delete_single("alert")
    query = query & delete_single("history_posting")
    query = query & delete_single("history_posting_tmp")
    query = query & delete_hd("hst_hargajualh", "id", "tanggal", "hst_hargajuald", "id")
    query = query & delete_single("hst_hpp")
    query = query & delete_single("hst_hppbahan")
    query = query & delete_single("hst_ms_bahan")
    query = query & delete_single("hst_stock")
    query = query & delete_list("list_bg", "tanggal", "status_cair", "status:1")
    query = query & delete_list("list_hutang", "tanggal_transaksi", "total_bayar", "hutang")
    query = query & delete_list("list_piutang", "tanggal_transaksi", "total_bayar", "piutang")
    query = query & delete_single("penilaian_karyawan")
    query = query & delete_single("t_angkut")
    query = query & delete_hd("t_apH", "nomer_ap", "tanggal", "t_apD", "nomer_ap")
    query = query & delete_hd("t_arH", "nomer_ar", "tanggal", "t_arD", "nomer_ar")
    query = query & delete_hd("t_bayarhutangh", "nomer_bayarhutang", "tanggal_bayarhutang", "t_bayarhutang_beli,t_bayarhutang_cek,t_bayarhutang_retur,t_bayarhutang_transfer", "nomer_bayarhutang,nomer_bayarhutang,nomer_bayarhutang,nomer_bayarhutang")
    query = query & delete_hd("t_bayarkulih", "nomer_bayarkuli", "tanggal", "t_bayarkulid", "nomer_bayarkuli")
    query = query & delete_hd("t_bayarpiutangh", "nomer_bayarpiutang", "tanggal_bayarpiutang", "t_bayarpiutang_cek,t_bayarpiutang_jual,t_bayarpiutang_retur,t_bayarpiutang_transfer", "nomer_bayarpiutang,nomer_bayarpiutang,nomer_bayarpiutang,nomer_bayarpiutang")
    query = query & delete_hd("t_belibukuh", "nomer_belibuku", "tanggal", "t_belibukud", "nomer_belibuku")
    query = query & delete_hd("t_belibukuh", "nomer_belibuku", "tanggal", "t_belibukud", "nomer_belibuku")
    query = query & delete_hd("t_beliH", "nomer_beli", "tanggal_beli", "t_beliD", "nomer_beli")
    query = query & delete_hd("t_jualH", "nomer_jual", "tanggal_jual", "t_jualD", "nomer_jual")
    query = query & delete_hd("t_kasbankh", "no_transaksi", "tanggal", "t_kasbankd,t_kasbankd2", "no_transaksi,no_transaksi")
    query = query & delete_hd("t_konsijualh", "nomer_konsijual", "tanggal_konsijual", "t_konsijuald", "nomer_konsijual")
    query = query & delete_hd("t_konsireturh", "nomer_konsiretur", "tanggal_konsiretur", "t_konsireturd", "nomer_konsiretur")
    query = query & delete_single("t_konsiretur_angkut")
    query = query & delete_hd("t_konsitambahh", "nomer_konsitambah", "tanggal_konsitambah", "t_konsitambahd", "nomer_konsitambah")
    query = query & delete_single("t_konsitambah_angkut")
    query = query & delete_hd("t_NotaDebetH", "nomer_notadebet", "tanggal", "t_NotaDebetD", "nomer_notadebet")
    query = query & delete_hd("t_NotaKreditH", "nomer_notakredit", "tanggal", "t_NotaKreditD", "nomer_notakredit")
    query = query & delete_hd("t_ongkostransporth", "nomer_ongkostransport", "tanggal", "t_ongkostransportd", "nomer_ongkostransport")
    query = query & delete_hd("t_pencairangiroh", "id", "tanggal", "t_pencairangirod,t_pencairangiro_acc", "id,id")
    query = query & delete_hd("t_POH", "nomer_PO", "tanggal_PO", "t_POD", "nomer_PO")
    query = query & delete_hd("t_produksih", "nomer_produksi", "tanggal_produksi", "t_produksi_bahan,t_produksi_biaya,t_produksi_hasil,t_produksi_kemasan,t_produksi_komposisibahan,t_produksi_komposisihasil,t_produksi_kuli,t_produksi_mesin,t_produksi_pengawas", "nomer_produksi,nomer_produksi,nomer_produksi,nomer_produksi,nomer_produksi,nomer_produksi,nomer_produksi,nomer_produksi,nomer_produksi")
    query = query & delete_hd("t_requestPOH", "nomer_requestPO", "tanggal_requestPO", "t_requestPOD", "nomer_requestPO")
    query = query & delete_hd("t_requestSOH", "nomer_requestSO", "tanggal_requestSO", "t_requestSOD", "nomer_requestSO")
    query = query & delete_hd("t_ReturBeliH", "nomer_ReturBeli", "tanggal_ReturBeli", "t_ReturBeliD", "nomer_ReturBeli")
    query = query & delete_single("t_returbeli_angkut")
    query = query & delete_hd("t_returjualH", "nomer_returjual", "tanggal_returjual", "t_returjualD", "nomer_returjual")
    query = query & delete_single("t_returjual_angkut")
    query = query & delete_hd("t_retursuratjalanH", "nomer_retursuratjalan", "tanggal_retursuratjalan", "t_retursuratjaland", "nomer_retursuratjalan")
    query = query & delete_hd("t_SOH", "nomer_SO", "tanggal_SO", "t_SOD", "nomer_SO")
    query = query & delete_hd("t_stockkoreksih", "nomer_koreksi", "tanggal", "t_stockkoreksid", "nomer_koreksi")
    query = query & delete_hd("t_stockmutasiBahanH", "nomer_mutasibahan", "tanggal", "t_stockmutasiBahanD", "nomer_mutasibahan")
    query = query & delete_hd("t_stockopnameh", "nomer_opname", "tanggal", "t_stockopnamed,t_stockopnamed_tmp", "nomer_opname,nomer_opname")
    query = query & delete_hd("t_suratjalanH", "nomer_suratjalan", "tanggal_suratjalan", "t_suratjalanD,t_suratjalan_timbang,t_suratjalan_angkut", "nomer_suratjalan,nomer_suratjalan,nomer_suratjalan")
    query = query & delete_hd("t_stockmutasiBahanH", "nomer_mutasibahan", "tanggal", "t_stockmutasiBahanD", "nomer_mutasibahan")
    query = query & delete_hd("t_terimabarangH", "nomer_terimabarang", "tanggal_terimabarang", "t_terimabarangD,t_terimabarang_timbang,t_terimabarang_komposisi,t_terimabarang_angkut", "nomer_terimabarang,nomer_terimabarang,nomer_terimabarang,nomer_terimabarang")
    query = query & delete_single("tmp_kartuHutang", , "id <> 'SaldoAwal'")
    query = query & delete_single("tmp_kartuPiutang", , "id <> 'SaldoAwal'")
    query = query & delete_single("tmp_kartustock", , "id <> 'StockAwal'")
    
    conn.CommitTrans
    conn.Close
    lblPersentase = 100
    MsgBox "Selesai"
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    
End Sub

Private Function delete_single(ByVal nama_tabel As String, Optional ByRef kolom_tanggal As String = "", Optional ByRef add_condition As String = "") As String
    'menghapus transaksi dengan tanggal, tidak memiliki tabel detail
    If kolom_tanggal = "" Then kolom_tanggal = "tanggal"
    If add_condition <> "" Then add_condition = " and " & add_condition
    delete_single = "delete from " & nama_tabel & " where convert(varchar," & kolom_tanggal & ",112) <= '" & Format(dtpCutOff.value, "yyyyMMdd") & "'" & add_condition & ";"
    lblNamaProses = nama_tabel
    If lblPersentase < 99 Then lblPersentase = lblPersentase + 1
    conn.Execute delete_single
    DoEvents
End Function

Private Function delete_hd(header_table As String, header_id As String, coltanggal As String, detail_table As String, detail_id As String)
    'menghapus transaksi dengan tanggal memiliki tabel header dan detail
    Dim split_detailtable() As String
    Dim split_detailid() As String
    
    split_detailtable = Split(detail_table, ",")
    split_detailid = Split(detail_id, ",")
    
    For i = 0 To UBound(split_detailtable)
        query = query & "delete a from " & split_detailtable(i) & " a inner join " & header_table & " b on a." & split_detailid(i) & " = b." & header_id & " where convert(varchar, b." & coltanggal & ", 112) <= '" & Format(dtpCutOff.value, "yyyyMMdd") & "';"
    Next
    
    query = query & "delete from " & header_table & " where convert(varchar, " & coltanggal & ", 112) <= '" & Format(dtpCutOff.value, "yyyyMMdd") & "';"
    If header_table = "t_bukakasir" Then
        query = query & " and status = 'tutup'"
    End If
    delete_hd = query
    lblNamaProses = header_table
    If lblPersentase < 99 Then lblPersentase = lblPersentase + 1
    conn.Execute delete_hd
    DoEvents
End Function

Private Function delete_list(table As String, coltanggal As String, colhutangpiutang As String, colparameter As String)
    Dim split_detailtable() As String
    Dim split_detailid() As String
    
    query = "delete from " & table & " where convert(varchar, " & coltanggal & ", 112) <= '" & Format(dtpCutOff.value, "yyyyMMdd") & "' and " & colhutangpiutang & IIf(InStr(1, colparameter, "status:"), " = ", " >= ") & Replace(colparameter, "status:", "") & ";"
     
    delete_list = query
    lblNamaProses = table
    If lblPersentase < 99 Then lblPersentase = lblPersentase + 1
    conn.Execute delete_list
    DoEvents
End Function

Private Sub lblPersentase_Change()
    If Not IsNumeric(lblPersentase) Then lblPersentase = 0
    PBTotalProses.value = CInt(lblPersentase)
End Sub
