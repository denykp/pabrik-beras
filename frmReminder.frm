VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmReminder 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Reminder"
   ClientHeight    =   5010
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10260
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5010
   ScaleWidth      =   10260
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command2 
      Caption         =   "Ok"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   3255
      TabIndex        =   9
      Top             =   4305
      Width           =   1680
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   4995
      TabIndex        =   8
      Top             =   4320
      Width           =   1680
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1575
      TabIndex        =   5
      Top             =   1095
      Width           =   2475
      _ExtentX        =   4366
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   108789763
      CurrentDate     =   38927
   End
   Begin VB.Label Label5 
      Caption         =   "Nomer :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   225
      TabIndex        =   7
      Top             =   240
      Width           =   1290
   End
   Begin VB.Label lblNomer 
      Caption         =   "Nomer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1530
      TabIndex        =   6
      Top             =   195
      Width           =   1920
   End
   Begin VB.Label Label3 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   225
      TabIndex        =   4
      Top             =   1140
      Width           =   1170
   End
   Begin VB.Label lblUser 
      Caption         =   "User"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1530
      TabIndex        =   3
      Top             =   660
      Width           =   1920
   End
   Begin VB.Label Label2 
      Caption         =   "Pesan dari :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   225
      TabIndex        =   2
      Top             =   690
      Width           =   1290
   End
   Begin VB.Label Label1 
      Caption         =   "Isi Pesan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   225
      TabIndex        =   1
      Top             =   1605
      Width           =   2310
   End
   Begin VB.Label LblPesan 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "ISI"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Left            =   195
      TabIndex        =   0
      Top             =   1920
      Width           =   9855
   End
End
Attribute VB_Name = "frmReminder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim conn1 As New ADODB.Connection
    conn1.Open strcon
    conn1.BeginTrans
    conn1.Execute "update alert set status='1',status_popup='1' where nomer='" & lblNomer.Caption & "' "
    conn1.Execute "update alert set tanggal_alert=dateadd(month,1,tanggal_alert),status='0',status_popup='0' where nomer='" & lblNomer.Caption & "' and bulanan='y'"
    
    conn1.CommitTrans
    conn1.Close
    Unload frmReminder
   
End Sub

Private Sub Command2_Click()
    
    Unload Me
   
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

