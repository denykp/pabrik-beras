VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAutomaticPosting 
   Caption         =   "Automatc Posting"
   ClientHeight    =   6480
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4665
   LinkTopic       =   "Form1"
   ScaleHeight     =   6480
   ScaleWidth      =   4665
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check1 
      Caption         =   "Select All"
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting Selected"
      Height          =   495
      Left            =   3120
      TabIndex        =   1
      Top             =   3000
      Width           =   1215
   End
   Begin VB.ListBox lstTransaksi 
      Height          =   5010
      Left            =   360
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   720
      Width           =   2535
   End
   Begin MSComCtl2.DTPicker DTPicker0 
      Height          =   375
      Left            =   1125
      TabIndex        =   2
      Top             =   270
      Width           =   2340
      _ExtentX        =   4128
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   91947011
      CurrentDate     =   40960
   End
   Begin VB.Label Label7 
      Caption         =   "Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   360
      TabIndex        =   3
      Top             =   315
      Width           =   690
   End
End
Attribute VB_Name = "frmAutomaticPosting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()
Dim J As Integer
If Check1.value = 1 Then
    For J = 0 To lstTransaksi.ListCount - 1
        lstTransaksi.Selected(J) = True
    Next
Else
    For J = 0 To lstTransaksi.ListCount - 1
        lstTransaksi.Selected(J) = False
    Next
End If
End Sub

Private Sub cmdPosting_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

conn.Open strcon

Dim J As Integer
    For J = 0 To lstTransaksi.ListCount - 1
        If lstTransaksi.Selected(J) = True Then
            If lstTransaksi.List(J) = "Penjualan" Then
                rs.Open "select * from t_jualh where month(tanggal_jual) = '" & Month(DTPicker0) & "' and year(tanggal_jual) = '" & Year(DTPicker0) & "' and day(tanggal_jual) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_jual desc", conn
                While Not rs.EOF
                    posting_jual rs!nomer_jual
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Account Payable" Then
                rs.Open "select * from t_aph where month(tanggal) = '" & Month(DTPicker0) & "' and year(tanggal) = '" & Year(DTPicker0) & "' and day(tanggal) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal desc", conn
                While Not rs.EOF
                    Posting_AP rs!nomer_ap
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Account Receivable" Then
                rs.Open "select * from t_aph where month(tanggal) = '" & Month(DTPicker0) & "' and year(tanggal) = '" & Year(DTPicker0) & "' and day(tanggal) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal desc", conn
                While Not rs.EOF
                    Posting_AR rs!nomer_ar
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Konsinyasi Jual" Then
                rs.Open "select * from t_konsijualh where month(tanggal_konsijual) = '" & Month(DTPicker0) & "' and year(tanggal_konsijual) = '" & Year(DTPicker0) & "' and day(tanggal_konsijual) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_konsijual desc", conn
                While Not rs.EOF
                    posting_konsijual rs!nomer_konsijual
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Konsinyasi Retur" Then
                rs.Open "select * from t_konsijualh where month(tanggal_konsiretur) = '" & Month(DTPicker0) & "' and year(tanggal_konsiretur) = '" & Year(DTPicker0) & "' and day(tanggal_konsiretur) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_konsiretur desc", conn
                While Not rs.EOF
                    posting_konsiretur rs!nomer_konsiretur
                    rs.MoveNext
                Wend
                rs.Close
            ElseIf lstTransaksi.List(J) = "Konsinyasi Tambah" Then
                rs.Open "select * from t_konsitambahh where month(tanggal_konsitambah) = '" & Month(DTPicker0) & "' and year(tanggal_konsitambah) = '" & Year(DTPicker0) & "' and day(tanggal_konsitambah) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_konsitambah desc", conn
                While Not rs.EOF
                    posting_konsitambah rs!nomer_konsitambah
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Pembelian" Then
                rs.Open "select * from t_belih where month(tanggal_beli) = '" & Month(DTPicker0) & "' and year(tanggal_beli) = '" & Year(DTPicker0) & "' and day(tanggal_beli) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_beli desc", conn
                While Not rs.EOF
                    posting_pembelian rs!nomer_beli
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Produksi" Then
                rs.Open "select * from t_produksih where month(tanggal_produksi) = '" & Month(DTPicker0) & "' and year(tanggal_produksi) = '" & Year(DTPicker0) & "' and day(tanggal_produksi) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_produksi desc", conn
                While Not rs.EOF
                    posting_produksi rs!nomer_produksi
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Retur Beli" Then
                rs.Open "select * from t_returbelih where month(tanggal_returbeli) = '" & Month(DTPicker0) & "' and year(tanggal_returbeli) = '" & Year(DTPicker0) & "' and day(tanggal_erturbeli) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_returbeli desc", conn
                While Not rs.EOF
                    posting_returbeli rs!nomer_returbeli
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Retur Jual" Then
                rs.Open "select * from t_returjualh where month(tanggal_returjual) = '" & Month(DTPicker0) & "' and year(tanggal_returjual) = '" & Year(DTPicker0) & "' and day(tanggal_returjual) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_returjual desc", conn
                While Not rs.EOF
                    posting_returjual rs!nomer_returjual
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Retur Surat Jalan" Then
                rs.Open "select * from t_retursuratjalanh where month(tanggal_retursuratjalan) = '" & Month(DTPicker0) & "' and year(tanggal_retursuratjalan) = '" & Year(DTPicker0) & "' and day(tanggal_retursuratjalan) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_retursuratjalan desc", conn
                While Not rs.EOF
                    posting_retursuratjalan rs!nomer_retursuratjalan
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Surat Jalan" Then
                rs.Open "select * from t_suratjalanh where month(tanggal_suratjalan) = '" & Month(DTPicker0) & "' and year(tanggal_suratjalan) = '" & Year(DTPicker0) & "' and day(tanggal_suratjalan) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_suratjalan desc", conn
                While Not rs.EOF
                    posting_suratjalan rs!nomer_suratjalan
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Terima Barang" Then
                rs.Open "select * from t_terimabarangh where month(tanggal_terimabarang) = '" & Month(DTPicker0) & "' and year(tanggal_terimabarang) = '" & Year(DTPicker0) & "' and day(tanggal_terimabarang) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal_terimabarang desc", conn
                While Not rs.EOF
                    posting_terimabarang rs!nomer_terimabarang
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            ElseIf lstTransaksi.List(J) = "Transportasi" Then
                rs.Open "select * from t_ongkostransporth where month(tanggal_) = '" & Month(DTPicker0) & "' and year(tanggal) = '" & Year(DTPicker0) & "' and day(tanggal) = '" & Day(DTPicker0) & "' and status_posting = '0' order by tanggal desc", conn
                While Not rs.EOF
                    posting_transport rs!nomer_terimabarang
                    rs.MoveNext
                Wend
                rs.Close
                lstTransaksi.RemoveItem J
            End If
        End If
    Next
    MsgBox "Posting berhasil"
    
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker0_Change()
    loadlist_transaksi Month(DTPicker0), Year(DTPicker0), Day(DTPicker0)
End Sub

Private Sub Form_Load()
DTPicker0.value = Now
loadlist_transaksi Month(DTPicker0), Year(DTPicker0), Day(DTPicker0)
End Sub

Private Sub loadlist_transaksi(bulan As String, tahun As String, hari As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.Open strcon
    lstTransaksi.Clear
    
    rs.Open "select * from t_belih where month(tanggal_beli) = '" & bulan & "' and year(tanggal_beli) = '" & tahun & "' and day(tanggal_beli) = '" & hari & "' and status_posting = '0' order by tanggal_beli desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Pembelian"
    End If
    rs.Close
    
    rs.Open "select * from t_returbelih where month(tanggal_returbeli) = '" & bulan & "' and year(tanggal_returbeli) = '" & tahun & "' and day(tanggal_returbeli) = '" & hari & "' and status_posting = '0' order by tanggal_returbeli desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Retur Beli"
    End If
    rs.Close
    
    rs.Open "select * from t_terimabarangh where month(tanggal_terimabarang) = '" & bulan & "' and year(tanggal_terimabarang) = '" & tahun & "' and day(tanggal_terimabarang) = '" & hari & "' and status_posting = '0' order by tanggal_terimabarang desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Terima Barang"
    End If
    rs.Close
    
    rs.Open "select * from t_suratjalanh where month(tanggal_suratjalan) = '" & bulan & "' and year(tanggal_suratjalan) = '" & tahun & "' and day(tanggal_suratjalan) = '" & hari & "' and status_posting = '0' order by tanggal_suratjalan desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Surat Jalan"
    End If
    rs.Close
    
    rs.Open "select * from t_retursuratjalanh where month(tanggal_retursuratjalan) = '" & bulan & "' and year(tanggal_retursuratjalan) = '" & tahun & "' and day(tanggal_retursuratjalan) = '" & hari & "' and status_posting = '0' order by tanggal_retursuratjalan desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Retur Surat Jalan"
    End If
    rs.Close
    
    rs.Open "select * from t_ongkostransporth where month(tanggal) = '" & bulan & "' and year(tanggal) = '" & tahun & "' and day(tanggal) = '" & hari & "' and status_posting = '0' order by tanggal desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Transportasi"
    End If
    rs.Close
    
    rs.Open "select * from t_konsijualh where month(tanggal_konsijual) = '" & bulan & "' and year(tanggal_konsijual) = '" & tahun & "' and day(tanggal_konsijual) = '" & hari & "' and status_posting = '0' order by tanggal_konsijual desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Konsinyasi Jual"
    End If
    rs.Close
    
    rs.Open "select * from t_konsireturh where month(tanggal_konsiretur) = '" & bulan & "' and year(tanggal_konsiretur) = '" & tahun & "' and day(tanggal_konsiretur) = '" & hari & "' and status_posting = '0' order by tanggal_konsiretur desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Konsinyasi Retur"
    End If
    rs.Close
    
    rs.Open "select * from t_konsitambahh where month(tanggal_konsitambah) = '" & bulan & "' and year(tanggal_konsitambah) = '" & tahun & "' and day(tanggal_konsitambah) = '" & hari & "' and status_posting = '0' order by tanggal_konsitambah desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Konsinyasi Tambah"
    End If
    rs.Close
    
    rs.Open "select * from t_aph where month(tanggal) = '" & bulan & "' and year(tanggal) = '" & tahun & "' and day(tanggal) = '" & hari & "' and status_posting = '0' order by tanggal desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Account Payable"
    End If
    rs.Close
    
    rs.Open "select * from t_arh where month(tanggal) = '" & bulan & "' and year(tanggal) = '" & tahun & "' and day(tanggal) = '" & hari & "' and status_posting = '0' order by tanggal desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Account Receivable"
    End If
    rs.Close
    
    rs.Open "select * from t_produksih where month(tanggal_produksi) = '" & bulan & "' and year(tanggal_produksi) = '" & tahun & "' and day(tanggal_produksi) = '" & hari & "' and status_posting = '0' order by tanggal_produksi desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Produksi"
    End If
    rs.Close
    
    rs.Open "select * from t_jualh where month(tanggal_jual) = '" & bulan & "' and year(tanggal_jual) = '" & tahun & "' and day(tanggal_jual) = '" & hari & "' and status_posting = '0' order by tanggal_jual desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Penjualan"
    End If
    rs.Close
    
    rs.Open "select * from t_returjualh where month(tanggal_returjual) = '" & bulan & "' and year(tanggal_returjual) = '" & tahun & "' and day(tanggal_returjual) = '" & hari & "' and status_posting = '0' order by tanggal_returjual desc", conn
    If Not rs.EOF Then
          lstTransaksi.AddItem "Retur Jual"
    End If
    rs.Close
    conn.Close
End Sub
