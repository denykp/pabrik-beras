Attribute VB_Name = "ModJualBeli"
Public Const searchBarang As String = "select kode_bahan,nama_bahan,kategori,subkategori,grup,harga_sat,keterangan from ms_bahan"
Public Const SearchBeli As String = "select nomer_beli,tanggal_beli,kode_supplier,tanggal_jatuhtempo,nomer_invoice,nomer_terimabarang,total,userid,status_posting from t_belih"
Public Const SearchJual As String = "select nomer_jual,tanggal_jual,kode_customer,tanggal_jatuhtempo,nomer_order,total,userid,status_posting from t_JUALh"
Public Const SearchOrderBeli As String = "select nomer_Orderbelikertas,tanggal_Orderbelikertas,kode_supplier,nomer_kontrak,total,userid from t_orderbelikertash"
Public Const SearchrequestPO As String = "select nomer_requestPO,tanggal_requestpo,keterangan,tanggal_expired,userid from t_requestpoh"
Public Const SearchrequestSO As String = "select nomer_requestSO,tanggal_requestso,kode_customer,tanggal_expired,keterangan,userid from t_requestsoh"
Public Const SearchPO As String = "select nomer_PO,tanggal_po,kode_supplier,keterangan,userid from t_poh"
Public Const SearchSO As String = "select * from vw_soh"
Public Const SearchTerimaBarang As String = "select nomer_terimabarang,tanggal_terimabarang,nomer_po,kode_supplier,nomer_suratjalan,no_truk,keterangan,biaya_angkut_berat,biaya_angkut_kg,biaya_angkut,biaya_lain,userid,status_posting from t_terimabarangh"
Public Const Searchsuratjalan As String = "select nomer_suratjalan,tanggal_suratjalan,nomer_so,kode_customer,no_truk,keterangan,biaya_angkut_berat,biaya_angkut_kg,biaya_angkut,biaya_lain,userid,status_posting from t_suratjalanh"
Public Const Searchretursuratjalan As String = "select nomer_retursuratjalan,tanggal_retursuratjalan,nomer_suratjalan,no_truk,keterangan,userid,status_posting from t_retursuratjalanh"
Public Const Searchkonsitambah As String = "select nomer_konsitambah,tanggal_konsitambah,kode_customer,no_truk,keterangan,userid from t_konsitambahh"
Public Const Searchkonsiretur As String = "select nomer_konsiretur,tanggal_konsiretur,kode_customer,no_truk,keterangan,userid from t_konsireturh"
Public Const Searchkonsijual As String = "select nomer_konsijual,tanggal_konsijual,kode_toko,keterangan,userid from t_konsijualh"
Public Const Searchproduksi As String = "select * from t_produksih"
Public Const SearchBayarKuli As String = "Select nomer_bayarkuli,tanggal,periode_awal,periode_akhir,totalberat,total,status_posting from t_bayarkuliH"
Public Const SearchOngkosTransport As String = "Select nomer_ongkostransport,tanggal,periode_awal,periode_akhir,total,cara_bayar,kode_ekspedisi,status_posting from t_ongkostransportH"
Public Const SearchReturBeli As String = "select * from t_returbelih"
Public Const SearchReturJual As String = "select * from t_returjualh"

Public Const SearchRptSupplier As String = "select kode_supplier,[nama_supplier] from rpt_ms_supplier"
Public Const SearchRptCustomer As String = "select kode_customer,[nama_customer] from rpt_ms_customer"
Public Const SearchRptContact As String = "select kode_contact,[nama_contact],Contact_Person from rpt_ms_contact"
Public Const searchRptKaryawan As String = "select NIK,Nama_Karyawan from rpt_ms_karyawan"
Public SearchCustomer As String
Public SearchSupplier As String
Public SearchContact As String

Public searchKaryawan As String

Public Const SearchEkspedisi As String = "select kode_ekspedisi,[nama_ekspedisi],Alamat,Telp,Keterangan from ms_ekspedisi"

Public Const searchAP As String = "select nomer_ap,Tanggal,kode_supplier,tanggal_jatuhtempo,Keterangan,tipe,status_posting from t_aph "
Public Const searchAR As String = "select nomer_ar,Tanggal,kode_customer,tanggal_jatuhtempo,Keterangan,tipe,status_posting from t_arh "
Public var_acckas As String
Public var_acckasangkut As String
Public var_accpendapatan As String
Public var_accpendapatanlain As String
Public var_accbiayalain As String
Public var_accbiayaangkut As String
Public var_accreturjual As String
Public var_accpajakjual As String
Public var_accpajakbeli As String
Public var_accpiutang As String
Public var_accHutang As String
Public var_accpiutangretur As String
Public var_accHutangretur As String
Public var_accpersediaan As String
Public var_acchpp As String
Public usergroup As String

Public Sub load_query(namagroup As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

SearchSupplier = "select kode_supplier,nama_supplier,alamat,telp,hp,keterangan,fax,email,no_rekening,contact_person," & _
                    " [status],npwp,kategori,area,wilayah,wilayah1, " & _
                    " isnull((select top 1 datediff(day,tanggal_beli,'" & Now & "') from t_belih " & _
                    " where kode_supplier = ms_supplier.kode_supplier order by tanggal_beli desc),'') as [belum_pesan(hari)] from ms_supplier "
                    
SearchCustomer = "select kode_customer,nama_customer,nama_toko,alamat,telp,hp,fax,email," & _
                      " website,keterangan,npwp,[status],kategori,wilayah,wilayah1,wilayah2,ppn,kategori_disc,pajak, " & _
                      " isnull((select top 1 datediff(day,tanggal_jual,'" & Now & "') from t_jualh " & _
                      " where kode_customer = ms_customer.kode_customer order by tanggal_jual desc),'') as [belum_pesan(hari)] " & _
                      " from ms_customer "
                      
SearchContact = "select kode_contact,[nama_contact],Contact_Person,Alamat,Telp,HP,Fax,Email,No_Rekening,keterangan,npwp,status,area,wilayah,wilayah1,limitkredit from ms_contact"
searchKaryawan = "select NIK,Nama_Karyawan,Alamat,Keterangan,Telp,Sex,Tipe,LimitKredit from ms_karyawan"
    conn.Open strcon
    rs.Open "select * from user_groupquery where nama_group='" & namagroup & "' and nama_table='ms_customer'", conn
    If Not rs.EOF Then SearchCustomer = "select " & rs!columnlist & " from " & rs!nama_table
    rs.Close
    rs.Open "select * from user_groupquery where nama_group='" & namagroup & "' and nama_table='ms_supplier'", conn
    If Not rs.EOF Then SearchSupplier = "select " & rs!columnlist & " from " & rs!nama_table
    rs.Close
    rs.Open "select * from user_groupquery where nama_group='" & namagroup & "' and nama_table='ms_contact'", conn
    
    If Not rs.EOF Then SearchContact = "select " & rs!columnlist & " from " & rs!nama_table
    rs.Close
    
    conn.Close
    
End Sub
Public Sub load_accvariables()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    var_acckas = ""
    var_accpendapatan = ""
    var_accpendapatanlain = ""
    var_accbiayalain = ""
    var_accreturjual = ""
    var_accreturbeli = ""
    var_accpiutang = ""
    var_accHutang = ""
    var_accpiutangretur = ""
    var_accHutangretur = ""
    var_accpersediaan = ""
    var_acchpp = ""
    conn.Open strcon
    rs.Open "select kode_acc from setting_coa where kode='kas'", conn
    If Not rs.EOF Then var_acckas = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='pendapatan'", conn
    If Not rs.EOF Then var_accpendapatan = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='pendapatanlain'", conn
    If Not rs.EOF Then var_accpendapatanlain = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='biayalain'", conn
    If Not rs.EOF Then var_accbiayalain = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='retur jual'", conn
    If Not rs.EOF Then var_accreturjual = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='pajak pembelian'", conn
    If Not rs.EOF Then var_accpajakbeli = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='pajak penjualan'", conn
    If Not rs.EOF Then var_accpajakjual = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='hutang usaha'", conn
    If Not rs.EOF Then var_accHutang = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='piutang usaha'", conn
    If Not rs.EOF Then var_accpiutang = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='piutang retur pembelian'", conn
    If Not rs.EOF Then var_accpiutangretur = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='hutang retur penjualan'", conn
    If Not rs.EOF Then var_accHutangretur = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='persediaan barang'", conn
    If Not rs.EOF Then var_accpersediaan = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='hpp'", conn
    If Not rs.EOF Then var_acchpp = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='biaya angkut'", conn
    If Not rs.EOF Then var_accbiayaangkut = rs(0)
    rs.Close
    rs.Open "select kode_acc from setting_coa where kode='kas angkut'", conn
    If Not rs.EOF Then var_acckasangkut = rs(0)
    rs.Close
End Sub

Public Function newid(tabel As String, nama_id As String, tanggal As Date, prefix As String) As String
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim query As String
Dim start As Byte
    conn.Open strcon
    start = Len(prefix) + 1
    query = "select top 1 [nama_id] from [tabel] where substring([nama_id],[start],2)='" & Format(tanggal, "yy") & "' and substring([nama_id],[start]+2,2)='" & Format(tanggal, "MM") & "' and left([nama_id],[start]-1)='" & prefix & "' order by [nama_id] desc"
    query = Replace(query, "[tabel]", tabel)
    query = Replace(query, "[nama_id]", nama_id)
    query = Replace(query, "[start]", start)
    rs.Open query, conn
    If Not rs.EOF Then
        newid = prefix & Format(tanggal, "yy") & Format(tanggal, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        newid = prefix & Format(tanggal, "yy") & Format(tanggal, "MM") & Format("1", "00000")
    End If
    rs.Close
    conn.Close
End Function

Private Sub add_jurnal(ByRef NoTransaksi As String, no_jurnal As String, tanggal As Date, keterangan As String, Nominal As Currency, Tipe As String, ByRef cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
'    fields(4) = "totaldebet"
'    fields(5) = "totalkredit"
    fields(4) = "tipe"

    nilai(0) = NoTransaksi
    nilai(1) = no_jurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
'    nilai(4) = CCur(nominal)
'    nilai(5) = CCur(nominal)
    nilai(4) = Tipe

    cn.Execute tambah_data2(table_name, fields, nilai)

End Sub

Private Sub add_jurnaldetail(ByRef NoTransaksi As String, NoJurnal As String, kode_acc As String, debet As Currency, kredit As Currency, Row As Integer, ByRef cn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "kredit"
    fields(4) = "no_urut"
    fields(5) = "no_jurnal"
    fields(6) = "keterangan"
    fields(7) = "kode1"
    fields(8) = "kode2"

    nilai(0) = NoTransaksi
    nilai(1) = kode_acc
    nilai(2) = Replace(Format(debet, "###0.##"), ",", ".")
    nilai(3) = Replace(Format(kredit, "###0.##"), ",", ".")
    nilai(4) = Row
    nilai(5) = NoJurnal
    nilai(6) = keterangan
    nilai(7) = kode1
    nilai(8) = kode2

    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub


Public Function posting_produksi(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim rs As New ADODB.Recordset
Dim biayakg, hppkg As Double
Dim i As Byte
    i = 0
    posting_produksi = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    rs.Open "select totalbiaya/totalhasil,tanggal_produksi from t_produksih where nomer_produksi='" & NoTransaksi & "'", conn
    If rs.EOF Then Exit Function
    biayakg = rs(0)
    tanggal = rs(1)
    rs.Close
    rs.Open "select sum(hpp*berat)/sum(berat) from t_produksi_bahan where nomer_produksi='" & NoTransaksi & "'", conn
    If rs.EOF Then Exit Function
    hppkg = rs(0)
    rs.Close
    rs.Open "select t.kode_gudang,h.tanggal_produksi,t.kode_bahan,t.nomer_serial,t.hpp,sum(berat) as berat,sum(qty) as qty from t_produksi_bahan t inner join t_produksih h on t.nomer_produksi=h.nomer_produksi where t.nomer_produksi='" & NoTransaksi & "' group by t.kode_gudang,h.tanggal_produksi,t.kode_bahan,t.nomer_serial,t.hpp,no_urut order by no_urut", conn
    While Not rs.EOF
        insertkartustock "Bahan Produksi", NoTransaksi, rs!tanggal_produksi, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_gudang, rs!nomer_serial, rs!hpp, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty * -1, rs!berat * -1, rs!hpp, conntrans, False
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select t.kode_gudang,h.tanggal_produksi,t.kode_bahan,t.nomer_serial,t.hpp,sum(berat) as berat,sum(qty) as qty from t_produksi_kemasan t inner join t_produksih h on t.nomer_produksi=h.nomer_produksi where t.nomer_produksi='" & NoTransaksi & "' group by t.kode_gudang,h.tanggal_produksi,t.kode_bahan,t.nomer_serial,t.hpp,no_urut order by no_urut", conn
    While Not rs.EOF
        insertkartustock "Bahan Produksi", NoTransaksi, rs!tanggal_produksi, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_gudang, rs!nomer_serial, rs!hpp, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty * -1, rs!berat * -1, rs!hpp, conntrans, False
        rs.MoveNext
    Wend
    rs.Close
    Dim nomer_serial As String
    rs.Open "select t.kode_gudang,h.tanggal_produksi,t.kode_bahan,t.supplier,(select isnull(sum(berat*hpp),0)  from t_produksi_kemasan where kode_hasil=t.kode_bahan and nomer_produksi='" & NoTransaksi & "') as hppkemasan,sum(berat) as berat,sum(qty) as qty,no_urut from t_produksi_hasil t inner join t_produksih h on t.nomer_produksi=h.nomer_produksi where t.nomer_produksi='" & NoTransaksi & "' group by t.kode_gudang,h.tanggal_produksi,t.kode_bahan,t.supplier,t.hpp,no_urut order by no_urut", conn
    While Not rs.EOF
        nomer_serial = Format(tanggal, "dd/MM/yy") & "-" & Right(NoTransaksi, 5)
        insertkartustock "Hasil Produksi", NoTransaksi, rs!tanggal_produksi, rs!kode_bahan, rs!qty, rs!berat, rs!kode_gudang, nomer_serial, hppkg + biayakg + rs!hppkemasan, conntrans
        updatestockSupplier NoTransaksi, rs!kode_bahan, rs!kode_gudang, nomer_serial, rs!qty, rs!berat, hppkg + biayakg + rs!hppkemasan, rs!supplier, conntrans, False
        conn.Execute "update t_produksi_hasil set nomer_serial='" & nomer_serial & "' where nomer_produksi+kode_bahan='" & NoTransaksi & rs!kode_bahan & "'"
        conntrans.Execute "insert into stock_komposisi select kode_bahan,'" & nomer_serial & "',kode_komposisi,pct from t_produksi_komposisihasil  where nomer_produksi='" & NoTransaksi & "' and kode_bahan='" & rs!kode_bahan & "' and no_urut='" & rs!no_urut & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    conntrans.Execute "update t_produksih set status_posting='1' where nomer_produksi='" & NoTransaksi & "'"
    conntrans.CommitTrans
    posting_produksi = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function

Public Function posting_konsiretur(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Byte
    i = 0
    posting_konsiretur = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    rs.Open "select t.kode_gudang,h.kode_customer,h.tanggal_konsiretur,t.kode_bahan,t.nomer_serial,harga,t.hpp,berat as berat,qty from t_konsireturd t inner join t_konsireturh h on t.nomer_konsiretur=h.nomer_konsiretur where t.nomer_konsiretur='" & NoTransaksi & "' order by no_urut", conn
    While Not rs.EOF
        insertkartustock "Retur Barang Konsinyasi", NoTransaksi, rs!tanggal_konsiretur, rs!kode_bahan, rs!qty, rs!berat, rs!kode_gudang, rs!nomer_serial, 0, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty, rs!berat, 0, conntrans, False
        updatestockcustomer NoTransaksi, rs!kode_bahan, rs!kode_customer, rs!nomer_serial, rs!qty * -1, rs!berat * -1, 0, rs!hpp, conntrans, False
        insertkartustockcustomer "Retur", NoTransaksi, rs!tanggal_konsiretur, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_customer, rs!nomer_serial, 0, conntrans
        rs.MoveNext
    Wend
    rs.Close
    conntrans.Execute "update t_konsireturh set status_posting='1' where nomer_konsiretur='" & NoTransaksi & "'"
    conntrans.CommitTrans
    posting_konsiretur = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function
Public Function posting_konsijual(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Byte
Dim pajak As Double
    i = 0
    posting_konsijual = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    
    rs.Open "select * from t_konsijualh h where h.nomer_konsijual='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
        pajak = rs!tax
        insertpiutang rs!kode_customer, NoTransaksi, "", (total * (100 + pajak) / 100), rs!tanggal_jatuhtempo, conntrans
        If rs!cara_bayar = "T" Then
            insertbayarpiutangtunai NoTransaksi, rs!tanggal_jual, rs!kode_customer, (total * (100 + pajak) / 100), conntrans
'            add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckas), CCur((total * (100 + pajak) / 100)), 0, row, conntrans, "Penjualan", kode_customer
'        Else
'            rs.Close
'            rs.Open "select acc_piutang from setting_acccustomer where kode_customer='" & kode_customer & "'"
'            add_jurnaldetail NoTransaksi, NoJurnal, rs(0), (total * (100 + pajak) / 100), 0, row, conntrans, "Penjualan", kode_customer
        End If
    End If
    rs.Close
    rs.Open "select h.kode_customer,h.tanggal_konsijual,t.kode_bahan,t.nomer_serial,harga,t.hpp,qty,berat as berat from t_konsijuald t inner join t_konsijualh h on t.nomer_konsijual=h.nomer_konsijual where t.nomer_konsijual='" & NoTransaksi & "' order by no_urut", conn
    While Not rs.EOF
        updatestockcustomer NoTransaksi, rs!kode_bahan, rs!kode_customer, rs!nomer_serial, rs!qty * -1, rs!berat * -1, 0, rs!hpp, conntrans, False
        insertkartustockcustomer "Penjualan", NoTransaksi, rs!tanggal_konsijual, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_customer, rs!nomer_serial, 0, conntrans
        rs.MoveNext
    Wend
    rs.Close
    conntrans.Execute "update t_konsijualh set status_posting='1' where nomer_konsijual='" & NoTransaksi & "'"
    conntrans.CommitTrans
    posting_konsijual = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function

Public Function posting_konsitambah(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Byte
    i = 0
    posting_konsitambah = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    rs.Open "select t.kode_gudang,h.kode_customer,h.tanggal_konsitambah,t.kode_bahan,t.nomer_serial,harga,t.hpp,qty,berat as berat from t_konsitambahd t inner join t_konsitambahh h on t.nomer_konsitambah=h.nomer_konsitambah where t.nomer_konsitambah='" & NoTransaksi & "' order by no_urut", conn
    While Not rs.EOF
        insertkartustock "Tambah Barang Konsinyasi", NoTransaksi, rs!tanggal_konsitambah, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_gudang, rs!nomer_serial, 0, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty * -1, rs!berat * -1, 0, conntrans, False
        updatestockcustomer NoTransaksi, rs!kode_bahan, rs!kode_customer, rs!nomer_serial, rs!qty, rs!berat, rs!harga, rs!hpp, conntrans, False
        insertkartustockcustomer "Kirim", NoTransaksi, rs!tanggal_konsitambah, rs!kode_bahan, rs!qty, rs!berat, rs!kode_customer, rs!nomer_serial, 0, conntrans
        rs.MoveNext
    Wend
    rs.Close
    conntrans.Execute "update t_konsitambahh set status_posting='1' where nomer_konsitambah='" & NoTransaksi & "'"
    conntrans.CommitTrans
    posting_konsitambah = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function
Public Function posting_suratjalan(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim NoJurnal As String
Dim rs As New ADODB.Recordset
Dim i As Byte
    i = 0
    posting_suratjalan = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    i = 1
    rs.Open "select t.kode_gudang,h.tanggal_suratjalan,t.nomer_suratjalan,t.kode_bahan,t.nomer_serial,t.hpp,sum(berat) as berat,sum(qty) as qty from t_suratjalan_timbang t inner join t_suratjalanh h on t.nomer_suratjalan=h.nomer_suratjalan where t.nomer_suratjalan='" & NoTransaksi & "' group by t.nomer_suratjalan,kode_bahan,nomer_serial,hpp,kode_gudang,tanggal_suratjalan order by min(no_urut)", conn
    While Not rs.EOF
        insertkartustock "Surat Jalan", NoTransaksi, rs!tanggal_suratjalan, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_gudang, rs!nomer_serial, 0, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty * -1, rs!berat * -1, 0, conntrans, False
        rs.MoveNext
    Wend
    rs.Close
    Dim biayaangkut As Currency
    Dim ekspedisi As String
    Dim Row As Integer
    Row = 0
    rs.Open "select biaya_angkut,biaya_lain,kode_ekspedisi,cara_bayar,tanggal from t_suratjalanh h inner join t_suratjalan_angkut t on h.nomer_suratjalan=t.nomer_suratjalan where  h.nomer_suratjalan='" & NoTransaksi & "'"
    If Not rs.EOF Then
        ekspedisi = rs!kode_ekspedisi
        biayaangkut = rs!biaya_angkut + rs!biaya_lain
        NoJurnal = newid("t_jurnalh", "no_jurnal", rs!tanggal, "JU")
        add_jurnal NoTransaksi, NoJurnal, rs!tanggal, "Surat Jalan", biayaangkut, "SJ", conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accbiayaangkut), CCur(biayaangkut), 0, Row, conntrans, "Biaya Angkut", ekspedisi
        Row = Row + 1
        If rs!cara_bayar = "T" Then
            add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckasangkut), 0, CCur(biayaangkut), Row, conntrans, "Biaya Angkut", ekspedisi
        Else
            insertkartuHutang "Biaya Angkutan", NoTransaksi, rs!tanggal, ekspedisi, biayaangkut, conntrans
            insertHutang ekspedisi, NoTransaksi, "", biayaangkut, DateAdd("m", 1, rs!tanggal), conntrans
    
            rs.Close
            rs.Open "select acc_hutang from setting_accsupplier where kode_supplier='" & ekspedisi & "'", conntrans
            If Not rs.EOF Then add_jurnaldetail NoTransaksi, NoJurnal, rs(0), 0, biayaangkut, Row, conntrans, "Ongkos Angkutan", ekspedisi
        End If
        If rs.State Then rs.Close

    End If
    If rs.State Then rs.Close
    
    conntrans.Execute "update t_suratjalanh set status_posting='1' where nomer_suratjalan='" & NoTransaksi & "'"
        
    
    conntrans.CommitTrans
    hitungulang_so
    posting_suratjalan = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function

Public Function hitungulang_so() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    hitungulang_so = False
    conn.Open strcon
    conn.Execute "update t_sod set beratkirim=(select isnull(sum(berat),0) as beratkirim from t_suratjalan_timbang inner join t_suratjalanh h on t_suratjalan_timbang.nomer_suratjalan=h.nomer_suratjalan where status_posting='1' and kode_bahan=t_sod.kode_bahan and nomer_order=t_sod.nomer_so )- " & _
        "(select isnull(sum(berat),0) as beratretur from t_retursuratjaland d inner join t_retursuratjalanh h on h.nomer_retursuratjalan=d.nomer_retursuratjalan where status_posting='1'  and kode_bahan=t_sod.kode_bahan and nomer_order=t_sod.nomer_so ) where nomer_so in (select nomer_so from t_soh where status<2)"
    conn.Execute "update t_sod set qtykirim=(select isnull(sum(qty),0) from t_suratjalan_timbang inner join t_suratjalanh h on t_suratjalan_timbang.nomer_suratjalan=h.nomer_suratjalan where status_posting='1' and kode_bahan=t_sod.kode_bahan and nomer_order=t_sod.nomer_so )- " & _
        "(select isnull(sum(qty),0) as beratretur from t_retursuratjaland d inner join t_retursuratjalanh h on h.nomer_retursuratjalan=d.nomer_retursuratjalan where status_posting='1'  and kode_bahan=t_sod.kode_bahan and nomer_order=t_sod.nomer_so ) where nomer_so in (select nomer_so from t_soh where status<2)"
    
    rs.Open "select d.nomer_so from t_sod d inner join t_soh h on d.nomer_so=h.nomer_so where status < 3 " & _
            "group by d.nomer_so having sum(qty)<=sum(qtykirim) and sum(berat)<=sum(beratkirim)  ", conn
    While Not rs.EOF
        conn.Execute "update t_soh set status=3  where nomer_so='" & rs(0) & "'"
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    hitungulang_so = True
    Exit Function
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    Set conn = Nothing
    Set rs = Nothing
End Function

Public Function posting_terimabarang(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim counter As Integer
Dim conn As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim NoJurnal As String
Dim i As Byte
    i = 0
    posting_terimabarang = False
    conn.Open strcon
    conntrans.Open strcon
    conntrans.BeginTrans
    
    
    i = 1
    rs.Open "select g.kode_gudang,h.tanggal_terimabarang,t.nomer_terimabarang,t.kode_bahan,t.nomer_serial,sum(g.qty) as qty, sum(g.berat) as berat from t_terimabarangd t inner join t_terimabarangh h on t.nomer_terimabarang=h.nomer_terimabarang " & _
    "inner join t_terimabarang_timbang g on g.nomer_terimabarang=h.nomer_terimabarang and g.kode_bahan=t.kode_bahan where t.nomer_terimabarang='" & NoTransaksi & "' group by g.kode_gudang,h.tanggal_terimabarang,t.nomer_terimabarang,t.kode_bahan,t.nomer_serial,t.no_urut,g.no_urut order by t.no_urut,g.no_urut", conn
    While Not rs.EOF
        insertkartustock "Penerimaan Barang", NoTransaksi, rs!tanggal_terimabarang, rs!kode_bahan, rs!qty, rs!berat, rs!kode_gudang, rs!nomer_serial, 0, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty, rs!berat, 0, conntrans, False
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select distinct t.kode_bahan,t.nomer_serial,t.no_urut from t_terimabarangd t inner join t_terimabarangh h on t.nomer_terimabarang=h.nomer_terimabarang " & _
    " where t.nomer_terimabarang='" & NoTransaksi & "' ", conn
    While Not rs.EOF
        conntrans.Execute "insert into stock_komposisi values ('" & rs!kode_bahan & "','" & rs!nomer_serial & "','" & rs!kode_bahan & "',100)"
        conntrans.Execute "insert into t_terimabarang_komposisi values ('" & NoTransaksi & "','" & rs!kode_bahan & "','" & rs!no_urut & "','" & rs!kode_bahan & "',100,1)"
        rs.MoveNext
    Wend
    rs.Close
    
    Dim biayaangkut As Currency
    Dim Row As Integer
    Dim ekspedisi As String
    Row = 0
    rs.Open "select biaya_angkut,biaya_lain,kode_ekspedisi,cara_bayar,tanggal from t_terimabarangh h inner join t_terimabarang_angkut t on h.nomer_terimabarang=t.nomer_terimabarang where  h.nomer_terimabarang='" & NoTransaksi & "'"
    If Not rs.EOF Then
        biayaangkut = rs!biaya_angkut + rs!biaya_lain
        ekspedisi = rs!kode_ekspedisi
        NoJurnal = newid("t_jurnalh", "no_jurnal", rs!tanggal, "JU")
        add_jurnal NoTransaksi, NoJurnal, rs!tanggal, "Penerimaan Barang", biayaangkut, "TB", conntrans
        
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accbiayaangkut), CCur(biayaangkut), 0, Row, conntrans, "Biaya Angkut", ekspedisi
        Row = Row + 1
        If rs!cara_bayar = "T" Then
            add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckasangkut), 0, CCur(biayaangkut), Row, conntrans, "Biaya Angkut", ekspedisi
        Else
            insertkartuHutang "Pembelian", NoTransaksi, rs!tanggal, ekspedisi, biayaangkut * -1, conntrans
            insertHutang ekspedisi, NoTransaksi, "", biayaangkut, DateAdd("m", 1, rs!tanggal), conntrans
    
            rs.Close
            rs.Open "select acc_hutang from setting_accsupplier where kode_supplier='" & ekspedisi & "'", conntrans
            If Not rs.EOF Then add_jurnaldetail NoTransaksi, NoJurnal, rs(0), 0, biayaangkut, Row, conntrans, "ongkos angkutan", ekspedisi
        End If
        rs.Close

    End If
    If rs.State Then rs.Close
    
    conntrans.Execute "update t_terimabarangh set status_posting='1' where nomer_terimabarang='" & NoTransaksi & "'"
    conntrans.CommitTrans
    posting_terimabarang = True
    i = 0
    conn.Close
    conntrans.Close
    Exit Function
err:
    If i = 1 Then conntrans.RollbackTrans
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    If conntrans.State Then conntrans.Close
End Function
Public Function posting_pembelian(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim i As Byte
Dim NoJurnal As String
Dim NoSuratJalan As String
Dim kode_supplier As String
Dim Nama_Supplier As String
Dim total As Currency
Dim gudang As String
Dim pajak As Currency
Dim Row As Integer
i = 0
posting_pembelian = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select b.*,s.Nama_Supplier from t_belih b left join ms_supplier s on s.Kode_Supplier=b.Kode_Supplier where b.nomer_beli='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    tanggal = rs!tanggal_beli
    tanggal_jatuhtempo = rs!tanggal_jatuhtempo
    kode_supplier = rs!kode_supplier
    Nama_Supplier = Left(rs!Nama_Supplier, 35)
    NoSuratJalan = rs!nomer_invoice
    total = rs!total
    pajak = 0
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Pembelian", rs!total, "PB", conntrans

    If rs!cara_bayar = "T" Then
        insertbayarHutangtunai NoTransaksi, rs!tanggal_beli, kode_supplier, rs!total, conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckas), 0, CCur(rs!total), Row, conntrans, "Pembelian", Nama_Supplier
    Else
        insertkartuHutang "Pembelian", NoTransaksi, tanggal, kode_supplier, rs!total * -1, conntrans
        insertHutang kode_supplier, NoTransaksi, NoSuratJalan, rs!total, tanggal_jatuhtempo, conntrans

        rs.Close
        rs.Open "select acc_hutang from setting_accsupplier where kode_supplier='" & kode_supplier & "'"
        If Not rs.EOF Then add_jurnaldetail NoTransaksi, NoJurnal, rs(0), 0, total, Row, conntrans, "Pembelian", Nama_Supplier
    End If
    rs.Close
    rs.Open "select d.*,s.*,m.nama_bahan from t_belid d " & _
            "inner join setting_accbahan s on d.kode_bahan=s.kode_bahan " & _
            "left join ms_bahan m on m.kode_bahan=d.kode_bahan " & _
            "where d.nomer_beli='" & NoTransaksi & "'", connrs, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        'updatehpp NoTransaksi, rs!kode_bahan, gudang, rs!nomer_serial, rs!berat, rs!harga, conntrans
        conntrans.Execute "update stock set hpp='" & rs!harga & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "'"
        conntrans.Execute "update tmp_kartustock set hpp='" & rs!harga & "' where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "' and id='" & NoSuratJalan & "'"
        add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, (rs!berat * rs!harga), 0, Row, conntrans, "Pembelian", Left(rs!nama_bahan, 35)
        rs.MoveNext
    Wend
    Row = Row + 1

    conntrans.Execute "update t_belih set status_posting='1' where nomer_beli='" & NoTransaksi & "'"
    posting_pembelian = True
Else
    GoTo err
End If
rs.Close
connrs.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function

Public Function posting_orderbelikertas(ByRef NoTransaksi As String) As Boolean
'On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim NoJurnal As String
Dim kode_supplier As String

Dim Row As Integer
i = 0
posting_orderbelikertas = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select * from t_orderbelikertash where nomer_orderbelikertas='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    kode_supplier = rs!kode_supplier
'    inserthutang rs!kode_supplier, NoTransaksi, 0, conntrans
    conntrans.Execute "update t_orderbelikertash set status_posting='1' where nomer_orderbelikertas='" & NoTransaksi & "'"
    posting_orderbelikertas = True
Else
    GoTo err
End If
rs.Close
connrs.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function
Public Function posting_orderbeliroll(ByRef NoTransaksi As String) As Boolean
'On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim NoJurnal As String
Dim kode_supplier As String

Dim Row As Integer
i = 0
posting_orderbeliroll = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select * from t_orderbelirollh where nomer_orderbeliroll='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    kode_supplier = rs!kode_supplier
'    inserthutang rs!kode_supplier, NoTransaksi, 0, conntrans
    conntrans.Execute "update t_orderbelirollh set status_posting='1' where nomer_orderbeliroll='" & NoTransaksi & "'"
    posting_orderbeliroll = True
Else
    GoTo err
End If
rs.Close
connrs.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function


Public Function posting_jual(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte, hpp As Double
Dim kode_customer As String
Dim Nama_Customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim noFaktur As String
Dim pajak As Double

Dim Acc_Penjualan_Lain As String
Dim Acc_Pendapatan As String

i = 0
posting_jual = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1

rs.Open "select kode_acc from setting_coa where kode='jual lain'", connrs
If Not rs.EOF Then Acc_Penjualan_Lain = rs(0)
rs.Close

If Left(NoTransaksi, 2) = "PJ" Then
    Acc_Pendapatan = var_accpendapatan
Else
    Acc_Pendapatan = Acc_Penjualan_Lain
End If

rs.Open "select j.*,c.nama_customer from t_jualh j left join ms_customer c on c.kode_customer=j.kode_customer where j.nomer_jual='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    
    tanggal = rs!tanggal_jual
    kode_customer = rs!kode_customer
    Nama_Customer = Left(rs!Nama_Customer, 35)
    noFaktur = rs!nomer_invoice
    pajak = rs!tax
    total = rs!total
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    insertpiutang rs!kode_customer, NoTransaksi, noFaktur, (total * (100 + pajak) / 100), rs!tanggal_jatuhtempo, conntrans
    
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Penjualan", (total * (100 + pajak) / 100), "PJ", conntrans
    If rs!cara_bayar = "T" Then
        insertbayarpiutangtunai NoTransaksi, rs!tanggal_jual, rs!kode_customer, (total * (100 + pajak) / 100), conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckas), CCur((total * (100 + pajak) / 100)), 0, Row, conntrans, "Penjualan", Nama_Customer
    Else
        insertkartuPiutang "Penjualan", NoTransaksi, rs!tanggal_jual, rs!kode_customer, (total * (100 + pajak) / 100), conntrans
        rs.Close
        rs.Open "select acc_piutang from setting_acccustomer where kode_customer='" & kode_customer & "'"
        add_jurnaldetail NoTransaksi, NoJurnal, rs(0), (total * (100 + pajak) / 100), 0, Row, conntrans, "Penjualan", Nama_Customer
    End If
    Row = Row + 1
    add_jurnaldetail NoTransaksi, NoJurnal, CStr(Acc_Pendapatan), 0, total, Row, conntrans, "Penjualan", Nama_Customer
    
    Row = Row + 1
    add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accpajakjual), 0, (total * pajak / 100), Row, conntrans, "Penjualan", noFaktur
    rs.Close
    
    conntrans.Execute "update t_jualh set status_posting='2' where nomer_jual='" & NoTransaksi & "'"
    posting_jual = True
Else
    GoTo err
End If
If rs.State Then rs.Close
connrs.Close
conntrans.CommitTrans
i = 0

Exit Function
err:
'Debug.Print NoTransaksi & " " & err.Description
    MsgBox err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function

Public Function posting_returbeli(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim NoJurnal As String
Dim kode_supplier As String
Dim Nama_Supplier As String
Dim total As Currency
Dim gudang As String
Dim pajak As Currency
Dim Row As Integer
i = 0
posting_returbeli = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
rs.Open "select t.*,s.nama_supplier from t_returbelih t left join ms_supplier s on s.kode_supplier=t.kode_supplier where t.nomer_returbeli='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    
    tanggal = rs!tanggal_returbeli
    kode_supplier = rs!kode_supplier
    Nama_Supplier = Left(rs!Nama_Supplier, 35)
    total = rs!total
    pajak = 0
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Retur Beli", rs!total, "RB", conntrans
    insertpiutangretur kode_supplier, NoTransaksi, rs!total, conntrans
    rs.Close
    rs.Open "select acc_piutangretur from setting_accsupplier where kode_supplier='" & kode_supplier & "'", conntrans
    If rs.EOF Then
        MsgBox "Perkiraan retur untuk supplier " & kode_supplier & " belum diisi"
        GoTo err
    End If
    add_jurnaldetail NoTransaksi, NoJurnal, rs(0), total, 0, Row, conntrans, "Retur Beli", Nama_Supplier
    rs.Close
    rs.Open "select d.kode_gudang,d.kode_bahan,d.nomer_serial,qty,berat,d.harga from t_returbelid d where d.nomer_returbeli='" & NoTransaksi & "'", connrs, adOpenForwardOnly
    While Not rs.EOF
        insertkartustock "Retur Pembelian", NoTransaksi, tanggal, rs!kode_bahan, rs!qty * -1, rs!berat * -1, rs!kode_gudang, rs!nomer_serial, rs!harga, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty * -1, rs!berat * -1, rs!harga, conntrans
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select d.kode_bahan,m.nama_bahan,acc_persediaan,sum(berat*harga) as total from t_returbelid d " & _
            "inner join setting_accbahan s on d.kode_bahan=s.kode_bahan " & _
            "left join ms_bahan m on m.kode_bahan=d.kode_bahan " & _
            "where d.nomer_returbeli='" & NoTransaksi & "' group by d.kode_bahan,m.nama_bahan,acc_persediaan", connrs, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, 0, rs!total, Row, conntrans, "Retur Beli", Left(rs!nama_bahan, 35)
'        pajak = pajak + ((rs!qty * rs!harga) / 11)
        rs.MoveNext
    Wend
    Row = Row + 1

    conntrans.Execute "update t_returbelih set status_posting='1' where nomer_returbeli='" & NoTransaksi & "'"
    posting_returbeli = True
Else
    GoTo err
End If
rs.Close
connrs.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
MsgBox NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function
Public Function posting_returjual(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim kode_customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim pajak As Double
i = 0
posting_returjual = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1
rs.Open "select * from t_returjualh where nomer_returjual='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    
    tanggal = rs!tanggal_returjual
    kode_customer = rs!kode_customer

    total = rs!total
    
    insertHutangretur rs!kode_customer, NoTransaksi, rs!total, conntrans
    
    rs.Close
    rs.Open "select d.kode_bahan,d.nomer_serial,d.qty,d.berat,d.hpp,d.kode_gudang from t_returjuald d " & _
        " where d.nomer_returjual='" & NoTransaksi & "'", connrs, adOpenForwardOnly
    While Not rs.EOF
        insertkartustock "ReturJual", NoTransaksi, tanggal, rs!kode_bahan, rs!qty, rs!berat, rs!kode_gudang, rs!nomer_serial, rs!hpp, conntrans
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty, rs!berat, rs!hpp, conntrans
        rs.MoveNext
    Wend
    rs.Close
    conntrans.Execute "update t_returjualh set status_posting='1' where nomer_returjual='" & NoTransaksi & "'"
    posting_returjual = True
Else
    GoTo err
End If
If rs.State Then rs.Close
connrs.Close
conntrans.CommitTrans
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function

Public Function posting_retursuratjalan(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim kode_customer As String
Dim gudang As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim pajak As Double
i = 0
posting_retursuratjalan = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1
rs.Open "select * from t_retursuratjalanh where nomer_retursuratjalan='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    
    tanggal = rs!tanggal_retursuratjalan
    total = 0
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")

    add_jurnal NoTransaksi, NoJurnal, tanggal, "Pengembalian Kiriman", total, "RKB", conntrans
    rs.Close
    

    
    rs.Open "select d.kode_bahan,d.nomer_serial,d.qty,d.berat,d.hpp,kode_gudang from t_retursuratjaland d " & _
        " where d.nomer_retursuratjalan='" & NoTransaksi & "'", connrs, adOpenForwardOnly
    While Not rs.EOF
        gudang = rs!kode_gudang
        insertkartustock "Retur Barang Keluar", NoTransaksi, tanggal, rs!kode_bahan, rs!qty, rs!berat, gudang, rs!nomer_serial, rs!hpp, conntrans
        updatestock NoTransaksi, rs!kode_bahan, gudang, rs!nomer_serial, rs!qty, rs!berat, rs!hpp, conntrans
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select s.kode_bahan,b.nama_bahan,sum(s.berat*s.hpp),acc_hpp,acc_persediaan from t_retursuratjaland s " & _
        " inner join setting_accbahan m on s.kode_bahan=m.kode_bahan " & _
        " left join ms_bahan b on b.kode_bahan=s.kode_bahan " & _
        " where s.nomer_retursuratjalan='" & NoTransaksi & "' group by s.kode_bahan,acc_hpp,acc_persediaan", connrs, adOpenForwardOnly
    While Not rs.EOF
        Row = Row + 1
        add_jurnaldetail NoTransaksi, NoJurnal, rs!Acc_Persediaan, rs(1), 0, Row, conntrans, "Retur", Left(rs!nama_bahan, 35)
        Row = Row + 1
        add_jurnaldetail NoTransaksi, NoJurnal, rs!acc_hpp, 0, rs(1), Row, conntrans, , "Retur", Left(rs!nama_bahan, 35)
        rs.MoveNext
    Wend
    conntrans.Execute "update t_retursuratjalanh set status_posting='1' where nomer_retursuratjalan='" & NoTransaksi & "'"
    
    posting_retursuratjalan = True
Else
    GoTo err
End If
If rs.State Then rs.Close

conntrans.CommitTrans
hitungulang_so
    connrs.Close
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function
Public Sub Posting_AP(ByRef NoTransaksi As String)
Dim connrs As New ADODB.Connection
Dim conn As New ADODB.Connection
Dim i As Integer
Dim J As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim AccHutang As String
Dim Kodesupplier As String
Dim NilaiJurnalH As Currency
Dim keterangan As String
Dim NoJurnal As String
'On Error GoTo err
    i = 0
    connrs.Open strcon
    conn.Open strcon
    conn.BeginTrans
    i = 1
    Tipe = Left(NoTransaksi, 2)
    
    rs.Open "select tanggal,total,t.kode_supplier,tanggal_jatuhtempo,keterangan from t_aph t where nomer_ap='" & NoTransaksi & "'", connrs
    tanggal = rs("tanggal")
    tanggal_jatuhtempo = rs("tanggal_jatuhtempo")
    Kodesupplier = rs!kode_supplier
    NilaiJurnalH = rs("total")
    rs.Close
    insertHutang Kodesupplier, NoTransaksi, "", NilaiJurnalH, tanggal_jatuhtempo, conn
    insertkartuHutang "Hutang Lain-Lain", NoTransaksi, tanggal, Kodesupplier, NilaiJurnalH * -1, conn
    
    conn.Execute "update t_aph set status_posting='1' where nomer_ap='" & NoTransaksi & "'"
    conn.CommitTrans
    i = 0
    DropConnection
    MsgBox "Data sudah diposting"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description
End Sub



Public Sub Posting_AR(ByRef NoTransaksi As String)
Dim connrs As New ADODB.Connection
Dim conn As New ADODB.Connection
Dim i As Integer
Dim J As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim AccPiutang As String
Dim KodeCustomer As String
Dim NilaiJurnalH As Currency
Dim keterangan As String
Dim NoJurnal As String
Dim noFaktur As String
'On Error GoTo err
    i = 0
    connrs.Open strcon
    conn.Open strcon
    conn.BeginTrans
    i = 1
    Tipe = Left(NoTransaksi, 2)
    rs.Open "select tanggal,total,t.kode_customer,tanggal_jatuhtempo,keterangan from t_arh t  where nomer_ar='" & NoTransaksi & "'", connrs
    tanggal = rs("tanggal")
    tanggal_jatuhtempo = rs("tanggal_jatuhtempo")
    
    KodeCustomer = rs!kode_customer
    NilaiJurnalH = rs("total")
    noFaktur = rs!keterangan
    rs.Close
    insertpiutang KodeCustomer, NoTransaksi, noFaktur, NilaiJurnalH, tanggal_jatuhtempo, conn
    
    insertkartuPiutang "Piutang Lain-Lain", NoTransaksi, tanggal, KodeCustomer, NilaiJurnalH, conn
    conn.Execute "update t_arh set status_posting='1' where nomer_ar='" & NoTransaksi & "'"
    conn.CommitTrans
    i = 0
    DropConnection
    MsgBox "Data sudah diposting"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    Debug.Print NoTransaksi & " " & err.Description
End Sub
Public Function posting_transport(ByRef NoTransaksi As String) As Boolean
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim connrs As New ADODB.Connection
Dim conntrans As New ADODB.Connection
Dim tanggal As Date
Dim i As Byte
Dim kode_supplier As String
Dim CaraBayar As String
Dim total As Currency
Dim Row As Integer
Dim NoJurnal As String
Dim Nama_Supplier As String
i = 0
posting_transport = False
connrs.Open strcon
conntrans.Open strcon
conntrans.BeginTrans
i = 1
Row = 1
rs.Open "select h.*,m.nama_ekspedisi from t_ongkostransporth h left join ms_ekspedisi m on h.kode_ekspedisi=m.kode_ekspedisi where nomer_ongkostransport='" & NoTransaksi & "'", connrs
If Not rs.EOF Then
    tanggal = rs!tanggal
    'kode_supplier = rs!kode_ekspedisi
    total = rs!total
    CaraBayar = rs!cara_bayar
    'Nama_Supplier = rs!nama_ekspedisi
    rs.Close
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "JU")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Transport", total, "OT", conntrans
    
    If CaraBayar = "Kredit" Then
        insertHutang kode_supplier, NoTransaksi, "", total, tanggal, conntrans
        insertkartuHutang "Transport", NoTransaksi, tanggal, kode_supplier, total, conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accHutang), 0, CCur(total), Row, conntrans, "Transport", Nama_Supplier
    Else
        insertbayarHutangtunai NoTransaksi, tanggal, kode_supplier, total, conntrans
        add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_acckas), 0, CCur(total), Row, conntrans, "Transport", Nama_Supplier
    End If
    add_jurnaldetail NoTransaksi, NoJurnal, CStr(var_accbiayakirim), CCur(total), 0, Row, conntrans, "Transport", Nama_Supplier
    conntrans.Execute "update t_ongkostransporth set status_posting='1' where nomer_ongkostransport='" & NoTransaksi & "'"
    posting_transport = True
Else
    GoTo err
End If
If rs.State Then rs.Close
connrs.Close
conntrans.CommitTrans
If CaraBayar = "Kredit" Then
    Posting_BayarHutang NoTransaksi, conntrans
End If
i = 0
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If i = 1 Then conntrans.RollbackTrans
If connrs.State Then connrs.Close
If conntrans.State Then conntrans.Close
End Function

Private Sub insertHutang(kode_supplier As String, ByRef NoTransaksi As String, NoReferensi As String, total As Currency, tanggal_jatuhtempo As Date, ByRef cn As ADODB.Connection)
    cn.Execute "if not exists (select * from list_hutang where nomer_transaksi='" & NoTransaksi & "') " & _
            "insert into list_hutang (kode_supplier,nomer_transaksi,nomer_referensi,hutang,tanggal_jatuhtempo) values ('" & kode_supplier & "','" & NoTransaksi & "','" & NoReferensi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "','" & Format(tanggal_jatuhtempo, "yyyy/MM/dd") & "')"
End Sub

Private Sub updateHutang(kode_supplier As String, ByRef NoTransaksi As String, total As Currency, ByRef cn As ADODB.Connection)
    cn.Execute "update list_hutang set hutang=hutang+" & Replace(Format(total, "###0.##"), ",", ".") & " where kode_supplier='" & kode_supplier & "' and nomer_transaksi='" & NoTransaksi & "'"
End Sub
Private Sub insertpiutangretur(kode_supplier As String, ByRef NoTransaksi As String, total As Currency, ByRef cn As ADODB.Connection)
    cn.Execute "insert into list_piutang_retur (kode_supplier,nomer_transaksi,piutang) values ('" & kode_supplier & "','" & NoTransaksi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "')"
End Sub
Private Sub updatepiutangretur(kode_supplier As String, ByRef NoTransaksi As String, total As Currency, ByRef cn As ADODB.Connection)
    cn.Execute "update list_piutang_retur set piutang=piutang+" & Replace(Format(total, "###0.##"), ",", ".") & " where kode_supplier='" & kode_supplier & "' and nomer_transaksi='" & NoTransaksi & "'"
End Sub

Private Sub insertbayarHutangtunai(ByRef NoTransaksi As String, tanggal As Date, kode_supplier As String, total As Currency, ByRef cn As ADODB.Connection)
Dim id As String
    id = newid("t_bayarhutangh", "nomer_bayarhutang", tanggal, "PH")
    cn.Execute "insert into t_bayarhutangh (nomer_bayarhutang,tanggal_bayarhutang,kode_supplier,jumlah_bayar,userid,status_posting) values ('" & id & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & kode_supplier & "','" & Replace(Format(total, "###0.##"), ",", ".") & "','" & User & "','1')"
    cn.Execute "insert into t_bayarhutang_beli (nomer_bayarhutang,nomer_beli,jumlah_bayar,keterangan,no_urut) values ('" & id & "','" & NoTransaksi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "','','1')"
    updateHutang kode_supplier, NoTransaksi, total * -1, cn
    
End Sub
Private Sub insertpiutang(kode_customer As String, ByRef NoTransaksi As String, noFaktur As String, total As Currency, tanggal_jatuhtempo As Date, ByRef cn As ADODB.Connection)
    cn.Execute "if not exists (select * from list_piutang where nomer_transaksi='" & NoTransaksi & "') insert into list_piutang (kode_customer,nomer_transaksi,piutang,tanggal_jatuhtempo,nomer_referensi) values ('" & kode_customer & "','" & NoTransaksi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "','" & Format(tanggal_jatuhtempo, "yyyy/MM/dd") & "','" & noFaktur & "')"
End Sub

Private Sub updatepiutang(kode_supplier As String, ByRef NoTransaksi As String, total As Currency, ByRef cn As ADODB.Connection)
    cn.Execute "update list_piutang set piutang=piutang+" & Replace(Format(total, "###0.##"), ",", ".") & " where kode_customer='" & kode_supplier & "' and nomer_transaksi='" & NoTransaksi & "'"
End Sub
Private Sub insertHutangretur(kode_customer As String, ByRef NoTransaksi As String, total As Currency, ByRef cn As ADODB.Connection)
    cn.Execute "insert into list_hutang_retur (kode_customer,nomer_transaksi,hutang) values ('" & kode_customer & "','" & NoTransaksi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "')"
End Sub
Private Sub insertbayarpiutangtunai(ByRef NoTransaksi As String, tanggal As Date, kode_customer As String, total As Currency, ByRef cn As ADODB.Connection)
Dim id As String
    id = newid("t_bayarpiutangh", "nomer_bayarpiutang", tanggal, "PP")
    cn.Execute "insert into t_bayarpiutangh (nomer_bayarpiutang,tanggal_bayarpiutang,kode_customer,jumlah_bayar,userid,status_posting) values ('" & id & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & kode_customer & "','" & Replace(Format(total, "###0.##"), ",", ".") & "','" & User & "','1')"
    cn.Execute "insert into t_bayarpiutang_jual (nomer_bayarpiutang,nomer_jual,jumlah_bayar,keterangan,no_urut) values ('" & id & "','" & NoTransaksi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "','','1')"
    updatepiutang kode_customer, NoTransaksi, total * -1, cn
    Posting_BayarPiutang id, cn
End Sub

Public Sub updatestock(ByRef NoTransaksi As String, kode_bahan As String, kode_gudang As String, noseri As String, qty As Double, berat As Double, harga As Currency, ByRef cn As ADODB.Connection, Optional status As Boolean)
    If IsNull(status) Then status = True
    cn.Execute "if exists(select * from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' ) " & _
        "update stock set stock=stock+'" & Replace(Format(berat, "###0.##"), ",", ".") & "',qty=qty+'" & Replace(Format(qty, "###0.##"), ",", ".") & "' where nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' else " & _
        "insert into stock (kode_bahan,nomer_serial,stock,qty,hpp,kode_gudang,status) values ('" & kode_bahan & "','" & noseri & "','" & Replace(Format(berat, "###0.##"), ",", ".") & "','" & Replace(Format(qty, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & kode_gudang & "','" & CInt(status) & "')"
    'cn.Execute "if (select stock from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' )=0 delete from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' "
End Sub

Public Sub updatestockSupplier(ByRef NoTransaksi As String, kode_bahan As String, kode_gudang As String, noseri As String, qty As Double, berat As Double, harga As Currency, supplier As String, ByRef cn As ADODB.Connection, Optional status As Boolean)
    If IsNull(status) Then status = True
    cn.Execute "if exists(select * from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' ) " & _
        "update stock set stock=stock+'" & Replace(Format(berat, "###0.##"), ",", ".") & "',qty=qty+'" & Replace(Format(qty, "###0.##"), ",", ".") & "' where nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' else " & _
        "insert into stock (kode_bahan,nomer_serial,stock,qty,hpp,kode_gudang,status,supplier) values ('" & kode_bahan & "','" & noseri & "','" & Replace(Format(berat, "###0.##"), ",", ".") & "','" & Replace(Format(qty, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & kode_gudang & "','" & CInt(status) & "','" & supplier & "')"
    'cn.Execute "if (select stock from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' )=0 delete from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' "
End Sub


Public Sub updatestockcustomer(ByRef NoTransaksi As String, kode_bahan As String, kode_customer As String, noseri As String, qty As Double, berat As Double, harga As Currency, hpp As Currency, ByRef cn As ADODB.Connection, Optional status As Boolean)
    If IsNull(status) Then status = True
    cn.Execute "if exists(select * from stock_customer where nomer_serial='" & noseri & "'  and kode_customer='" & kode_customer & "' and kode_bahan='" & kode_bahan & "' ) " & _
        "update stock_customer set stock=stock+'" & Replace(Format(berat, "###0.##"), ",", ".") & "',qty=qty+'" & Replace(Format(qty, "###0.##"), ",", ".") & "' where nomer_serial='" & noseri & "' and kode_customer='" & kode_customer & "' and kode_bahan='" & kode_bahan & "' else " & _
        "insert into stock_customer (kode_bahan,nomer_serial,stock,qty,harga_jual,hpp,kode_customer) values ('" & kode_bahan & "','" & noseri & "','" & Replace(Format(berat, "###0.##"), ",", ".") & ",'" & Replace(Format(qty, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & Replace(Format(hpp, "###0.##"), ",", ".") & "','" & kode_customer & "')"
    'cn.Execute "if (select stock from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' )=0 delete from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "' and kode_bahan='" & kode_bahan & "' "
End Sub

Public Sub updatehpp(ByRef NoTransaksi As String, kode_bahan As String, kode_gudang As String, noseri As String, qty As Double, harga As Currency, ByRef cn As ADODB.Connection)
Dim rs As New ADODB.Recordset
rs.Open "select sum(stock),max(hpp) from stock where nomer_serial='" & noseri & "' and kode_bahan='" & kode_bahan & "'", cn
If Not rs.EOF Then
'cn.Execute "if exists(select * from stock where nomer_serial='" & noseri & "' and kode_bahan='" & kode_bahan & "' ) " & _
'        "update stock set hpp=((stock*hpp)+(" & Replace(Format(qty, "###0.##"), ",", ".") & "*" & Replace(Format(harga, "###0.##"), ",", ".") & "))/(stock+" & Replace(Format(qty, "###0.##"), ",", ".") & ") where nomer_serial='" & noseri & "'  and kode_bahan='" & kode_bahan & "' "
cn.Execute "update stock set hpp=(" & rs(0) * rs(1) & "+(" & Replace(Format(qty * harga, "###0.##"), ",", ".") & "))/(" & Replace(Format(rs(0) + qty, "###0.##"), ",", ".") & ") where nomer_serial='" & noseri & "'  and kode_bahan='" & kode_bahan & "' "
End If
rs.Close
End Sub

Public Function cekserialkertas2(noseri As String, ByRef cn As ADODB.Connection) As Boolean
'On Error GoTo err
Dim rs As New ADODB.Recordset
    cekserialkertas2 = False
    rs.Open "select * from serial_kertas where nomer_serial='" & noseri & "'", cn
    If Not rs.EOF Then
        cekserialkertas2 = True
    End If
    rs.Close
    Exit Function
err:
    Debug.Print NoTransaksi & " " & err.Description
    If rs.State Then rs.Close
End Function
Public Function cekserialkertas1(noseri As String, connstr As String) As Boolean
'On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    cekserialkertas1 = False
    conn.Open connstr
    rs.Open "select * from serial_kertas where nomer_serial='" & noseri & "'", conn
    If rs.EOF Then
        cekserialkertas1 = True
    End If
    rs.Close
    conn.Close
    Exit Function
err:
    Debug.Print NoTransaksi & " " & err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Function

Public Function GetHPPTinta(ByVal kode_tinta As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select hpp from hpp_tinta where [kode_tinta]='" & kode_tinta & "' ", cn
If Not rs.EOF And IsNull(rs!hpp) = False Then
    GetHPPTinta = rs(0)
Else
    GetHPPTinta = 0
End If
rs.Close

End Function
Public Function GetStockTintaTotal(ByVal kode_tinta As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select sum(Stock) as stock from stock_tinta where [kode_tinta]='" & kode_tinta & "' ", cn
If Not rs.EOF And IsNull(rs!stock) = False Then
    GetStockTintaTotal = rs(0)
Else
    GetStockTintaTotal = 0
End If
rs.Close

End Function
Public Function GetHPPplat(ByVal kode_plat As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select hpp from hpp_plat where [kode_plat]='" & kode_plat & "' ", cn
If Not rs.EOF Then
    GetHPPplat = rs(0)
Else
    GetHPPplat = 0
End If
rs.Close

End Function

Public Function GetStockplatTotal(ByVal kode_plat As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select sum(Stock) as stock from stock_plat where [kode_plat]='" & kode_plat & "' ", cn
If Not rs.EOF And IsNull(rs!stock) = False Then
    GetStockplatTotal = rs(0)
Else
    GetStockplatTotal = 0
End If
rs.Close
End Function
'Public Function getStockPlat(ByVal noseri As String, byref cn As ADODB.Connection) As Currency
'Dim rs As New ADODB.Recordset
'
'rs.Open "select sum(Stock) as stock from stock_plat where [nomer_serial]='" & noseri & "' ", cn
'If Not rs.EOF And IsNull(rs!stock) = False Then
'    getStockPlat = rs(0)
'Else
'    getStockPlat = 0
'End If
'rs.Close
'End Function
Public Function GetHPPKertas(ByVal noseri As String, ByVal kode_gudang As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select hpp from stock where [nomer_serial]='" & noseri & "'  and kode_gudang='" & kode_gudang & "'", cn
If Not rs.EOF And IsNull(rs!hpp) = False Then
    GetHPPKertas = rs(0)
Else
    GetHPPKertas = 0
End If
rs.Close

End Function
Public Function GetStockKertas(ByVal noseri As String, ByVal kode_gudang As String, ByRef cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select Stock as stock from stock where nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "'", cn
If Not rs.EOF And IsNull(rs!stock) = False Then
    GetStockKertas = rs(0)
Else
    GetStockKertas = 0
End If
rs.Close

End Function
Public Function cek_serialnumber(noseri As String) As Boolean
'On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cek_serialnumber = True
If noseri = "" Then
    cek_serialnumber = True
    Exit Function
End If
conn.Open strcon
rs.Open "select * from stock where nomer_serial='" & noseri & "'", conn
If Not rs.EOF Then
    cek_serialnumber = False
End If
rs.Close
conn.Close
Exit Function
err:
Debug.Print NoTransaksi & " " & err.Description
If rs.State Then rs.Close
If conn.State Then conn.Close
End Function
Public Sub loadcombobox(combo As ComboBox, query)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim text As String
text = combo.text
conn.Open strcon
combo.Clear
rs.Open query, conn
While Not rs.EOF
    combo.AddItem rs(0)
    rs.MoveNext
Wend
rs.Close
conn.Close
SetComboText text, combo
If combo.ListCount > 0 And combo.ListIndex = -1 Then combo.ListIndex = 0
End Sub
