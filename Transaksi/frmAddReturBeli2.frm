VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAddReturBeli2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Retur Pembelian"
   ClientHeight    =   7425
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9780
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   9780
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3195
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0000
      TabIndex        =   20
      Top             =   630
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtNoNota 
      Height          =   285
      Left            =   1710
      TabIndex        =   0
      Top             =   675
      Width           =   1410
   End
   Begin VB.ComboBox cmbTipeRetur 
      Height          =   315
      ItemData        =   "frmAddReturBeli.frx":0102
      Left            =   7785
      List            =   "frmAddReturBeli.frx":010C
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   1125
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0132
      TabIndex        =   14
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   285
      Left            =   1710
      TabIndex        =   1
      Top             =   1035
      Width           =   3885
   End
   Begin VB.CommandButton cmdApprove 
      Caption         =   "&Approve"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4320
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0234
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   6570
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5895
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0336
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6570
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2745
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturBeli.frx":0438
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   6570
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   5130
      TabIndex        =   6
      Top             =   180
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   59768835
      CurrentDate     =   38927
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   4200
      Left            =   180
      TabIndex        =   2
      Top             =   1665
      Width           =   9195
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   "#"
      Col.Count       =   5
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      RowSelectionStyle=   2
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowNavigation   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   238
      Columns.Count   =   5
      Columns(0).Width=   2275
      Columns(0).Caption=   "Kd Barang"
      Columns(0).Name =   "Kode Barang"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   7805
      Columns(1).Caption=   "Nama Barang"
      Columns(1).Name =   "Nama Barang"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1905
      Columns(2).Caption=   "Qty"
      Columns(2).Name =   "Qty"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,##0"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "Qty Jual"
      Columns(3).Name =   "Qty Jual"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "Harga_beli"
      Columns(4).Name =   "Harga_beli"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      _ExtentX        =   16219
      _ExtentY        =   7408
      _StockProps     =   79
      BackColor       =   -2147483636
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "No Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   19
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   18
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7560
      TabIndex        =   16
      Top             =   1215
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   15
      Top             =   1215
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4410
      TabIndex        =   13
      Top             =   6030
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5490
      TabIndex        =   12
      Top             =   6030
      Width           =   1320
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4320
      TabIndex        =   11
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4995
      TabIndex        =   10
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   9
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   8
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000009&
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   135
      TabIndex        =   7
      Top             =   135
      Width           =   2040
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddReturBeli2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Const querybrg As String = "select m.kode_barang,nama_barang, merk, kode_jenis, satuan_2, min_qty, m.harga from ms_barang m "
Const queryJual As String = "Select ID,Tanggal,JatuhTempo,Invoice,[Kode Supplier],Keterangan,Gudang  from t_pembelianh"
Const queryRetur As String = "Select * from t_returbeliH"
Public jual As Boolean
Dim qty As Long
Dim NoTrans2 As String
Dim mode As Byte
Dim detail As Boolean
Dim currentRow As Integer
Dim NoJurnal As String
Dim totalRp As Double


Private Sub nomor_baru()
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
No = ""
If db2 Then
    conn.Open strconasli
    rs.Open "select top 1 ID from T_returbeliH where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    conn.Close
End If
No = ""
If db1 Then
    conn.Open strcon
    rs.Open "select top 1 ID from T_returbeliH where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    NoTrans2 = No
    If Not db2 Then lblNoTrans = NoTrans2
    conn.Close
End If
End Sub
Private Sub cmdApprove_Click()
On Error GoTo err
Dim counter As Integer
    conn.ConnectionString = strcon
    conn.Open
    conn.Execute "update t_returbelih set flag='1' where id='" & lblNoTrans & "'"
    
    counter = 0
    DBGrid.MoveFirst
    While counter < DBGrid.Rows
        If DBGrid.Columns(0).text <> "" Then
            conn.Execute "exec sp_updatehrgpkk '" & lblNoTrans & "','" & DBGrid.Columns(0).text & "','" & DBGrid.Columns(2).text & "','R','" & gudang & "'"
        End If
        DBGrid.MoveNext
        counter = counter + 1
    Wend
    
    conn.Close
    MsgBox "Data retur dengan nomor " & lblNoTrans & " telah berhasil di approve"
    reset_form
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.query = queryJual
    
    frmSearch.nmform = "frmAddreturbeli"
    frmSearch.nmctrl = "txtNoNota"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_nota"
    frmSearch.loadgrid frmSearch.query
'    frmSearch.txtSearch.text = txtNoNota.text
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    
End Sub


Private Sub cmdSearchID_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.query = queryRetur
    frmSearch.nmform = "frmAddreturbeli"
    frmSearch.nmctrl = "lblNoTrans"
    
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    
    frmSearch.Show vbModal
End Sub
Public Sub cek_notrans()
Dim rs As New ADODB.Recordset
'    reset_form
    cmdSimpan.Enabled = True
    cmdApprove.Enabled = False
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    rs.Open "select * from t_returbelih where id='" & lblNoTrans & "' and gudang='" & gudang & "'", conn
    If Not rs.EOF Then
        NoTrans2 = rs("pk")
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtNoNota.text = rs("no_nota")
        rs.Close
        qty = 0
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select a.[kode],b.[nama barang],a.qty,c.qty,a.harga_beli from (t_returbelid a inner join ms_barang b on a.[kode]=b.[kode]) inner join t_pembeliand c on a.[kode]=c.[kode]  where a.id='" & lblNoTrans & "' and c.id='" & txtNoNota.text & "'", conn
        While Not rs.EOF
            DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4)
            qty = qty + rs(2)
            rs.MoveNext
        Wend
        lblQty = Format(qty, "#,##0")
    End If
    rs.Close
    conn.Close
    
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim id As String
Dim counter As Integer
Dim no_urut As Integer
On Error GoTo err
    
    If txtNoNota.text = "" Or DBGrid.Rows < 1 Then
        MsgBox "Silahkan masukkan Nota dan Barang yang akan diretur terlebih dahulu"
        txtNoNota.SetFocus
        Exit Sub
    End If
    If db2 Then
    conn.ConnectionString = strconasli
    conn.Open
    conn.BeginTrans
    End If
    If db1 Then
    conn_fake.ConnectionString = strcon
    conn_fake.Open
    conn_fake.BeginTrans
    End If
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        nomor_baru
        NoJurnal = Nomor_Jurnal(DTPicker1)

    Else
        If db2 Then
            conn.Execute "delete from t_returbelih where ID='" & lblNoTrans & "'"
            rs.Open "select * from t_returbelid where id='" & lblNoTrans & "'", conn
            While Not rs.EOF
                updatestock gudang, rs("kode"), rs("qty"), conn
                rs.MoveNext
            Wend
            rs.Close
            conn.Execute "delete from t_returbelid where ID='" & lblNoTrans & "'"
            conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"

        End If
        If db1 Then
            conn_fake.Execute "delete from t_returbelih where ID='" & NoTrans2 & "'"
            rs.Open "select * from t_returbelid where id='" & NoTrans2 & "'", conn_fake
            While Not rs.EOF
                updatestock gudang, rs("kode"), rs("qty"), conn_fake
            rs.MoveNext
            Wend
            rs.Close
            conn_fake.Execute "delete from t_returbelid where ID='" & NoTrans2 & "'"
            conn_fake.Execute "delete from t_jurnalh where no_transaksi='" & NoTrans2 & "'"
            conn_fake.Execute "delete from t_jurnald where no_transaksi='" & NoTrans2 & "'"

        End If
    End If
    add_dataheader
    counter = 0
    DBGrid.MoveFirst
    totalRp = 0
    While counter < DBGrid.Rows
        add_datadetail counter
        If db2 Then updatestock gudang, DBGrid.Columns(0).text, DBGrid.Columns(2).text * -1, conn
        If db1 Then
'            If db2 And id = "-" Then updatestock2 gudang, DBGrid.Columns(0).text, getStock(DBGrid.Columns(0).text, gudang, strconasli), conn_fake
            updatestock gudang, DBGrid.Columns(0).text, DBGrid.Columns(2).text * -1, conn_fake
        End If
        
        counter = counter + 1
        DBGrid.MoveNext
    Wend
    
    add_jurnal
    
    If db2 Then conn.CommitTrans
    If db1 Then conn_fake.CommitTrans
    DropConnection
    i = 0
    MsgBox "Data sudah tersimpan"
    reset_form

    Call MySendKeys("{tab}")
    Exit Sub
err:
    If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    lblQty = "0,00"
    txtKeterangan.text = ""
    DTPicker1 = Now
    qty = 0
    cmdApprove.Enabled = False
    DBGrid.RemoveAll
    
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "T_returbeliH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "gudang"
    fields(4) = "no_nota"
    fields(5) = "pk"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = gudang
    nilai(4) = txtNoNota.text
    nilai(5) = NoTrans2
    
    If db2 Then conn.Execute tambah_data2(table_name, fields, nilai)
    If db1 Then
        nilai(0) = NoTrans2
        conn_fake.Execute tambah_data2(table_name, fields, nilai)
    End If
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "T_returbeliD"
    fields(0) = "ID"
    fields(1) = "[kode]"
    fields(2) = "qty"
    fields(3) = "harga_beli"
    fields(4) = "URUT"
        
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(0).text
    nilai(2) = DBGrid.Columns(2).text
    nilai(3) = DBGrid.Columns(4).text
    nilai(4) = row
    
    totalRp = totalRp + (CDbl(nilai(3)) * CDbl(nilai(2)))
       
    If db2 Then conn.Execute tambah_data2(table_name, fields, nilai)
    If db1 Then
        nilai(0) = NoTrans2
        conn_fake.Execute tambah_data2(table_name, fields, nilai)
    End If
End Sub

Private Sub add_jurnal()
Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim Acc_Kas As String
    Dim Acc_Hutang As String
    Dim Acc_Persediaan As String

    ReDim fields(7)
    ReDim nilai(7)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "totaldebet"
    fields(5) = "totalkredit"
    fields(6) = "tipe"

    nilai(0) = lblNoTrans
    nilai(1) = NoJurnal
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = "Retur Pembelian"
    nilai(4) = totalRp
    nilai(5) = totalRp
    nilai(6) = "RB"

    tambah_data table_name, fields, nilai


    ReDim fields(4)
    ReDim nilai(4)

    Acc_Kas = getAcc("kas")
    Acc_Persediaan = getAcc("persediaan")

    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "urut"


    nilai(0) = lblNoTrans
    nilai(1) = Acc_Kas
    nilai(2) = totalRp
    nilai(3) = 1

    tambah_data table_name, fields, nilai

    ReDim fields(4)
    ReDim nilai(4)

    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "kredit"
    fields(3) = "urut"


    nilai(0) = lblNoTrans
    nilai(1) = Acc_Persediaan
    nilai(2) = totalRp
    nilai(3) = 2

    tambah_data table_name, fields, nilai
    
End Sub

Private Sub DBGrid_AfterColUpdate(ByVal ColIndex As Integer)
    qty = qty + DBGrid.Columns(2).text
    lblQty = Format(qty, "#,##0")
End Sub

Private Sub DBGrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If CLng(DBGrid.Columns(2).text) > CLng(DBGrid.Columns(3).text) Then
        MsgBox "Jumlah Retur tidak boleh lebih besar daripada jumlah yang dibeli (" & DBGrid.Columns(3).text & ")"
        Cancel = True
    Else
        qty = qty - OldValue
    End If
End Sub

'Public Sub cek_PO()
'    conn.ConnectionString = strcon
'    conn.Open
'    rs.Open "select nama_supplier from ms_supplier where kode_supplier='" & txtPO.text & "'", conn
'    If Not rs.EOF Then
'        lblNmSupplier = rs(0)
'    Else
'        lblNmSupplier = ""
'    End If
'    rs.Close
'    conn.Close
'End Sub



Private Sub Form_Activate()
'    Call MySendKeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then
        cmdSearch_Click
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then Call MySendKeys("{tab}")
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub
Public Sub cek_nota()

    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    qty = 0
    If db2 Then
    conn.Open strconasli
    Else
    conn.Open strcon
    End If
    Dim where As String
    If lblNoTrans <> "-" Then where = " and id<>'" & lblNoTrans & "'"
    rs.Open "select * from t_returbelih where no_nota='" & txtNoNota & "' " & where, conn
    If Not rs.EOF Then
        If (MsgBox("Nota tersebut sudah diretur, apakah anda ingin melihat retur tersebut?", vbYesNo) = vbYes) Then
            lblNoTrans = rs(0)
            rs.Close
        End If
        conn.Close
        cek_notrans
        Exit Sub
    End If
    rs.Close
    rs.Open "select d.[kode],m.[nama barang],d.qty,d.harga from t_pembeliand d inner join ms_barang m on d.[kode]=m.[kode] where id='" & txtNoNota.text & "' order by urut", conn
    While Not rs.EOF
        DBGrid.AddItem rs(0).Value & "#" & rs(1).Value & "#0#" & rs(2).Value & "#" & rs(3).Value
        
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    lblQty = Format(qty, "#,##0")
    
End Sub
Private Sub fokusnota()
On Error Resume Next
    txtNoNota.SetFocus
End Sub
Private Sub txtNoNota_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cek_nota
    End If
End Sub

Private Sub txtNoNota_LostFocus()
    cek_nota
End Sub
