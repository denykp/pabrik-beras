VERSION 5.00
Begin VB.Form frmMasterSubGrupAcc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Sub Grup Perkiraan"
   ClientHeight    =   2715
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7650
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   7650
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4605
      Picture         =   "frmMasterSubGrupAcc.frx":0000
      TabIndex        =   5
      Top             =   2025
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3390
      Picture         =   "frmMasterSubGrupAcc.frx":0102
      TabIndex        =   20
      Top             =   2025
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchParent 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3540
      TabIndex        =   19
      Top             =   195
      Width           =   375
   End
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterSubGrupAcc.frx":0204
      Left            =   5730
      List            =   "frmMasterSubGrupAcc.frx":0217
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   1620
      Visible         =   0   'False
      Width           =   1260
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   2475
      Locked          =   -1  'True
      MaxLength       =   3
      TabIndex        =   0
      Top             =   195
      Width           =   1005
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   2475
      MaxLength       =   7
      TabIndex        =   1
      Top             =   585
      Width           =   1710
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4260
      Picture         =   "frmMasterSubGrupAcc.frx":023E
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   585
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   2475
      MaxLength       =   50
      TabIndex        =   2
      Top             =   1005
      Width           =   4695
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2175
      Picture         =   "frmMasterSubGrupAcc.frx":0340
      TabIndex        =   4
      Top             =   2025
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   960
      Picture         =   "frmMasterSubGrupAcc.frx":0442
      TabIndex        =   3
      Top             =   2025
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2250
      TabIndex        =   18
      Top             =   195
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1845
      TabIndex        =   17
      Top             =   210
      Width           =   150
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   16
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label lblParent 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4005
      TabIndex        =   15
      Top             =   240
      Width           =   3555
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1860
      TabIndex        =   13
      Top             =   1035
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1845
      TabIndex        =   12
      Top             =   630
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   11
      Top             =   1470
      Width           =   1245
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2265
      TabIndex        =   10
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2250
      TabIndex        =   9
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Sub Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   8
      Top             =   1020
      Width           =   1575
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Sub Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   7
      Top             =   615
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterSubGrupAcc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public tutup As Boolean
Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Silahkan masukkan Kode Parent terlebih dahulu!", vbCritical
        txtField(0).SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where [kode_subgrup_acc]='" & txtField(1).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
   
    conn.BeginTrans
    
    i = 1
    
    conn.Execute "delete from ms_subgrup_acc where [kode_subgrup_acc]='" & txtField(1).text & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
    conn.RollbackTrans
    
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [kode_subgrup_acc], [Nama], kode_parent from ms_subgrup_acc where left(kode_subgrup_acc,3) like '%" & txtField(0).text & "%'"  ' order by kode_Member"
    frmSearch.nmform = "frmMasterSubGrupAcc"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_subgrup_acc"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Col = 0
    frmSearch.Index = 1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchParent_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_parent,nama from ms_parentAcc"
    frmSearch.nmform = "frmMasterSubGrupAcc"
    frmSearch.nmctrl = "txtfield"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_parentAcc"
    frmSearch.Col = 0
    frmSearch.Index = 0
    
    frmSearch.proc = "cari_parent"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtField(1).SetFocus
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte, strFormat As String
On Error GoTo err
    i = 0
    
    If Len(Trim(txtField(1).text)) <> 7 Then
        MsgBox "Panjang Kode Account harus 7 karakter !", vbExclamation
        Exit Sub
    End If

    strFormat = txtField(0).text & "."
    
    If Left(txtField(1).text, 4) <> strFormat Then
        MsgBox "Format untuk kode account harus seperti berikut : " & vbCrLf & _
               "'Kode Parent' diikuti tanda titik (.) lalu diikuti 'Kode Account'"
        Exit Sub
    End If
    
    For J = 0 To 2
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
    
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    rs.Open "select * from ms_subgrup_acc where [kode_parent]='" & txtField(0).text & "' and [kode_subgrup_acc]='" & txtField(1).text & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_subgrup_acc where [kode_parent]='" & txtField(0).text & "' and [kode_subgrup_acc]='" & txtField(1).text & "'"
        
    End If
    rs.Close
    add_data
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    If tutup = True Then
     Select Case filename
        Case "Tinta"
            frmSettingAcc.SSDBGrid1.Columns(3).text = txtField(1).text
        Case "Bahan"

        Case "Customer"

        Case "Supplier"
 
    End Select
'        reset_form
        Unload Me
        Exit Sub
    End If
    
    reset_form
    
    
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(3)
    ReDim nilai(3)
    table_name = "ms_subgrup_acc"
    
    fields(0) = "kode_subgrup_acc"
    fields(1) = "nama"
    fields(2) = "kode_parent"
    fields(3) = "kategori"
    
    
    nilai(0) = txtField(1).text
    nilai(1) = txtField(2).text
    nilai(2) = txtField(0).text
    nilai(3) = cmbKategori.text
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    lblParent = ""
    
    txtField(0).SetFocus
    
End Sub

Public Sub cari_data()

    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_subgrup_acc where kode_subgrup_acc='" & txtField(1).text & "'", conn
    If Not rs.EOF Then
        
        txtField(0).text = rs(2)
        txtField(2).text = rs(1)
'        MsgBox rs(3)
'        SetComboText rs(3), cmbKategori
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
'        txtField(0).text = ""
        txtField(2).text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub

Public Sub cari_parent()
Dim No As String
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select top 1 kode_subgrup_acc from ms_subgrup_acc where left(kode_subgrup_acc,3)='" & txtField(0).text & "' order by kode_subgrup_acc desc", conn
    If Not rs.EOF Then
        No = txtField(0).text & "." & Format((CLng(Right(rs(0), 3)) + 1), "000")
    Else
        No = txtField(0).text & "." & Format("1", "000")
    End If
    rs.Close
    
    rs.Open "select * from ms_parentAcc where [kode_parent]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        lblParent = rs(1)
        txtField(1).text = No
        txtField(1).SelStart = Len(No)
        txtField(2).text = ""
    Else
        lblParent = ""
        txtField(1).text = ""
        txtField(2).text = ""
    End If
    rs.Close
    conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchParent_Click
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    reset_form
    If Not right_deletemaster Then cmdHapus.Visible = False
'    Me.top = 100
'    Me.Left = 100
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    
    If KeyAscii = 13 And Index = 0 Then
        cari_parent
    End If
    If KeyAscii = 13 And Index = 1 Then
        cari_data
    End If
    If Index = 0 Or Index = 1 Then Angka KeyAscii
    
End Sub

Private Sub txtField_LostFocus(Index As Integer)
'    If Index = 0 Then cari_parent
    If Index = 1 Then cari_data
End Sub


