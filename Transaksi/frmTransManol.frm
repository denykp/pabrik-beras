VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmTransManol 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Manol"
   ClientHeight    =   7065
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   12525
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7065
   ScaleWidth      =   12525
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5550
      Picture         =   "frmTransManol.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4455
      Picture         =   "frmTransManol.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.Frame frPeriode 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   600
      Left            =   225
      TabIndex        =   13
      Top             =   1125
      Width           =   7755
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5175
         Picture         =   "frmTransManol.frx":0A64
         TabIndex        =   14
         Top             =   180
         Width           =   1635
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Left            =   1620
         TabIndex        =   15
         Top             =   180
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   99745795
         CurrentDate     =   38927
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   3375
         TabIndex        =   16
         Top             =   180
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   99745795
         CurrentDate     =   38927
      End
      Begin VB.Label Label9 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1440
         TabIndex        =   18
         Top             =   225
         Width           =   105
      End
      Begin VB.Label Label8 
         Caption         =   "Periode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   17
         Top             =   225
         Width           =   1320
      End
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2970
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   6480
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6690
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   6480
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4230
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   6480
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5460
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   6480
      Width           =   1230
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   4065
      Left            =   225
      TabIndex        =   6
      Top             =   1800
      Width           =   12195
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   7
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   291
      Columns.Count   =   7
      Columns(0).Width=   4974
      Columns(0).Caption=   "Keterangan"
      Columns(0).Name =   "Keterangan"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   2461
      Columns(1).Caption=   "No Bukti"
      Columns(1).Name =   "No Bukti"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   2223
      Columns(2).Caption=   "tanggal"
      Columns(2).Name =   "tanggal"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2858
      Columns(3).Caption=   "Kuli"
      Columns(3).Name =   "Kuli"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1508
      Columns(4).Caption=   "Berat"
      Columns(4).Name =   "Berat"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3175
      Columns(5).Caption=   "Ongkos"
      Columns(5).Name =   "Ongkos"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Total"
      Columns(6).Name =   "Total"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      _ExtentX        =   21511
      _ExtentY        =   7170
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   3975
      Picture         =   "frmTransManol.frx":0B66
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   165
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTPicker3 
      Height          =   345
      Left            =   1815
      TabIndex        =   7
      Top             =   630
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   99745795
      CurrentDate     =   38927
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   8
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   5940
      Width           =   2040
   End
   Begin VB.Label lblTotalBerat 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5355
      TabIndex        =   4
      Top             =   5940
      Width           =   2040
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8835
      TabIndex        =   3
      Top             =   5940
      Width           =   2040
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1815
      TabIndex        =   2
      Top             =   135
      Width           =   2040
   End
   Begin VB.Label Label31 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   1
      Top             =   195
      Width           =   1320
   End
End
Attribute VB_Name = "frmTransManol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Double
Dim totalQty, totalberat As Double
Dim totalhapus As Double

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_bayarkuli] from t_bayarkulih where [nomer_bayarkuli]>'" & lblNoTrans & "' order by [nomer_bayarkuli]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPosting_Click()
    Call Posting_Manol(lblNoTrans, conn)
    reset_form
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_bayarkuli] from t_bayarkulih where [nomer_bayarkuli]<'" & lblNoTrans & "' order by [nomer_bayarkuli] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
    conn.Open strcon
        totalQty = 0
        totalberat = 0
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select * from vw_rekapkuli where convert(varchar(12),tanggal,111) between '" & Format(DTPicker1, "yyyy/MM/dd") & "' and '" & Format(DTPicker2, "yyyy/MM/dd") & "' and fg_bayarkuli=0", conn
        While Not rs.EOF
            DBGrid.AddItem rs!nama & "#" & rs!nomer & "#" & Format(rs!tanggal, "dd/MM/yyyy") & "#" & rs!kuli & "#" & rs!berat & "#0#0"
            
            totalQty = totalQty + rs!qty
            totalberat = totalberat + rs!berat
            
            rs.MoveNext
        Wend
        rs.Close
    conn.Close
    lblTotal = 0
    lblTotalBerat = totalberat
End Sub
Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select t.* from t_bayarkuliH t " & _
            "where t.nomer_bayarkuli ='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker3 = rs("tanggal")
        DTPicker1 = rs("periode_awal")
        DTPicker2 = rs("periode_akhir")
        frPeriode.Visible = False
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        
        If rs!status_posting = 1 Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
        End If
        
        total = 0
        totalberat = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select nama,d.nomer,tanggal,kuli,berat,ongkos,berat*ongkos " & _
                "from t_bayarkuliD d inner join vw_rekapkuli v on d.nomer=v.nomer " & _
                "where d.nomer_bayarkuli='" & lblNoTrans & "' order by d.no_urut", conn, adOpenDynamic, adLockOptimistic
        While Not rs.EOF
            DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & Format(rs(2), "dd/MM/yyyy") & "#" & rs(3) & "#" & rs(4) & "#" & rs(5) & "#" & rs(6)
            totalberat = totalberat + rs(4)
            total = total + rs(6)
            rs.MoveNext
        Wend
        rs.Close
        lblTotalBerat = totalberat
        lblTotal = total
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim Row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    id = lblNoTrans
    conn.Open strcon
    
    conn.BeginTrans
    DBGrid.Update
    i = 1
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_bayarkuliH", "nomer_bayarkuli", DTPicker3, "MN")
    Else
        conn.Execute "delete from t_bayarkuliH where nomer_bayarkuli='" & lblNoTrans & "'"
        conn.Execute "delete from t_bayarkuliD where nomer_bayarkuli='" & lblNoTrans & "'"
    End If
    add_dataheader
    
    For Row = 1 To DBGrid.Rows
        add_datadetail (Row)
        Select Case Left(DBGrid.Columns(1).CellText(Row - 1), 2)
        Case "TB": conn.Execute "update t_terimabarangh set fg_bayarkuli=1 where nomer_terimabarang='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "KB": conn.Execute "update t_suratjalanh set fg_bayarkuli=1 where nomer_suratjalan='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "KR": conn.Execute "update t_konsireturh set fg_bayarkuli=1 where nomer_konsiretur='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "KT": conn.Execute "update t_konsitambahh set fg_bayarkuli=1 where nomer_konsitambah='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "MS": conn.Execute "update t_stockmutasibahanh set fg_bayarkuli=1 where nomer_mutasibahan='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "RB": conn.Execute "update t_returbelih set fg_bayarkuli=1 where nomer_returbeli='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "RJ": conn.Execute "update t_returjualh set fg_bayarkuli=1 where nomer_returjual='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        Case "PR": conn.Execute "update t_produksih set fg_bayarkuli=1 where nomer_produksi='" & DBGrid.Columns(1).CellText(Row - 1) & "'"
        End Select
        
    Next
    
    conn.CommitTrans
    DropConnection
    simpan = True
    reset_form
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_bayarkuliH"
    fields(0) = "nomer_bayarkuli"
    fields(1) = "tanggal"
    fields(2) = "periode_awal"
    fields(3) = "periode_akhir"
    fields(4) = "totalBerat"
    fields(5) = "Total"
    fields(6) = "Userid"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker3, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = Format(DTPicker2, "yyyy/MM/dd HH:mm:ss")
    nilai(4) = totalberat
    nilai(5) = CDbl(lblTotal.Caption)
    nilai(6) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim selisih As Double
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_bayarkuliD"
    fields(0) = "nomer_bayarkuli"
    fields(1) = "nomer"
    fields(2) = "ongkos"
    fields(3) = "no_urut"
       
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(1).CellText(Row - 1)
    nilai(2) = DBGrid.Columns(5).CellText(Row - 1)
    nilai(3) = Row
    tambah_data table_name, fields, nilai
    
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchBayarKuli
    frmSearch.nmform = "frmTransManol"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "manol"
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub dbgrid_AfterColUpdate(ByVal ColIndex As Integer)
    DBGrid.Columns(6).text = DBGrid.Columns(5).text * DBGrid.Columns(4).text
    total = total + DBGrid.Columns(6).text
    lblTotal = Format(total, "#,##0")
End Sub

Private Sub DBGrid_AfterDelete(RtnDispErrMsg As Integer)
    totalberat = totalberat - DBGrid.Columns(4).text
    lblTotalBerat = totalberat
    total = total - totalhapus
    lblTotal = Format(total, "#,##0")
    totalhapus = 0
End Sub

Private Sub dbgrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    total = total - (DBGrid.Columns(4).text * OldValue)
End Sub
Private Sub reset_form()
    total = 0
    totalberat = 0
    lblNoTrans = "-"
    DBGrid.RemoveAll
    lblTotalBerat = totalberat
    lblTotal = total
    frPeriode.Visible = True
    
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
End Sub

Private Sub DBGrid_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    totalhapus = DBGrid.Columns(6).text
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    addkulitransport
    DTPicker1 = Now
    DTPicker2 = Now
    total = 0
    totalberat = 0
    DTPicker3 = Now
End Sub
