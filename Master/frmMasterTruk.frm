VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmMasterTruk 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Truk"
   ClientHeight    =   8370
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   8355
   FillColor       =   &H00E0E0E0&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   8355
   Begin VB.CommandButton cmdSearchSeri 
      Caption         =   "F3"
      Height          =   390
      Left            =   6240
      Picture         =   "frmMasterTruk.frx":0000
      TabIndex        =   24
      Top             =   1185
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.TextBox txtTipe 
      Height          =   330
      Left            =   1860
      Locked          =   -1  'True
      TabIndex        =   20
      Top             =   1935
      Width           =   1950
   End
   Begin VB.CommandButton cmdTipe 
      Caption         =   "F3"
      Height          =   390
      Left            =   3870
      Picture         =   "frmMasterTruk.frx":0102
      TabIndex        =   19
      Top             =   1875
      Width           =   465
   End
   Begin VB.CommandButton cmdAcc 
      Caption         =   "F3"
      Height          =   390
      Left            =   3870
      Picture         =   "frmMasterTruk.frx":0204
      TabIndex        =   14
      Top             =   930
      Width           =   465
   End
   Begin VB.TextBox txtKodeAcc 
      Height          =   330
      Left            =   1860
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   990
      Width           =   1950
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      Height          =   435
      Left            =   3780
      Picture         =   "frmMasterTruk.frx":0306
      TabIndex        =   5
      Top             =   7500
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   435
      Left            =   1080
      Picture         =   "frmMasterTruk.frx":0408
      TabIndex        =   3
      Top             =   7500
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   435
      Left            =   2430
      Picture         =   "frmMasterTruk.frx":050A
      TabIndex        =   4
      Top             =   7500
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   435
      Left            =   5130
      Picture         =   "frmMasterTruk.frx":060C
      TabIndex        =   6
      Top             =   7500
      UseMaskColor    =   -1  'True
      Width           =   1245
   End
   Begin VB.TextBox txtKode 
      Height          =   330
      Left            =   1845
      MaxLength       =   10
      TabIndex        =   0
      Top             =   150
      Width           =   2040
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   330
      Left            =   1860
      TabIndex        =   1
      Top             =   540
      Width           =   3480
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3990
      Picture         =   "frmMasterTruk.frx":070E
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   150
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   4065
      Left            =   240
      TabIndex        =   23
      Top             =   2460
      Width           =   7905
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   3
      BevelColorFrame =   -2147483641
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   503
      ExtraHeight     =   106
      Columns.Count   =   3
      Columns(0).Width=   4657
      Columns(0).Caption=   "Posisi"
      Columns(0).Name =   "Nama"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3916
      Columns(1).Caption=   "Nomer Seri Ban"
      Columns(1).Name =   "Kode Perkiraan"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   1
      Columns(2).Width=   4154
      Columns(2).Caption=   "Merk Ban"
      Columns(2).Name =   "namaAcc1"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   13944
      _ExtentY        =   7170
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Truk"
      Height          =   240
      Left            =   90
      TabIndex        =   22
      Top             =   1980
      Width           =   825
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1620
      TabIndex        =   21
      Top             =   1995
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1620
      TabIndex        =   18
      Top             =   1050
      Width           =   105
   End
   Begin VB.Label lblNamaPerkiraan 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Perkiraan"
      Height          =   240
      Left            =   1920
      TabIndex        =   17
      Top             =   1440
      Width           =   1365
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   16
      Top             =   990
      Width           =   150
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Perkiraan"
      Height          =   240
      Left            =   90
      TabIndex        =   15
      Top             =   1035
      Width           =   1290
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Polisi"
      Height          =   240
      Index           =   0
      Left            =   105
      TabIndex        =   13
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      Height          =   240
      Left            =   90
      TabIndex        =   12
      Top             =   600
      Width           =   1320
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1620
      TabIndex        =   11
      Top             =   195
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1620
      TabIndex        =   10
      Top             =   600
      Width           =   105
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      Height          =   240
      Left            =   510
      TabIndex        =   9
      Top             =   7080
      Width           =   2130
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      Height          =   240
      Left            =   1440
      TabIndex        =   8
      Top             =   195
      Width           =   150
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmMasterTruk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim idx As Integer
Private Sub cmdAcc_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa"
    frmSearch.nmform = "frmMasterTruk"
    frmSearch.nmctrl = "txtKodeAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = "cari_perkiraan"
    frmSearch.col = 0
    
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Sub cmdHapus_Click()
Dim i As Integer
On Error GoTo err
If txtKode.text = "" Then Exit Sub
   
    conn.ConnectionString = strcon
    conn.Open

    conn.Execute "delete from ms_truk where nopol='" & txtKode.text & "'"

    MsgBox "Data sudah Terhapus"
    DropConnection
    reset_form
    txtKode.SetFocus
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection

MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select * from ms_truk"
    frmSearch.nmform = "frmMasterTruk"
    frmSearch.nmctrl = "txtkode"
    frmSearch.keyIni = "ms_truk"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_truk"
    frmSearch.Index = -1
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
On Error GoTo err
i = 0
    If txtKode.text = "" Then
        MsgBox "Field yang bertanda * harus diisi"
        txtKode.SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_truk where nopol='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_truk where nopol='" & txtKode.text & "'"
    End If
    rs.Close
    conn.BeginTrans
    conn.Execute "insert into ms_truk (nopol,keterangan,kode_acc) values ('" & txtKode.text & "', '" & txtKeterangan & "', '" & txtKodeAcc & "')"
    conn.CommitTrans
    
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    txtKode.SetFocus
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection

MsgBox err.Description
End Sub

Private Sub cmdTipe_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_tipe_trukh"
    frmSearch.nmform = "frmMasterTruk"
    frmSearch.nmctrl = "txtTipe"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_tipe_trukh"
    frmSearch.proc = "cari_tipe"
    frmSearch.col = 0
    
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Public Sub cari_tipe()
conn.Open strcon
    SSDBGrid1.RemoveAll
    SSDBGrid1.FieldSeparator = "#"
    SSDBGrid1.Columns(0).Locked = True
    SSDBGrid1.Columns(2).Locked = True
    
    rs.Open " select d.Posisi,'' as Serial,'' as Merk " & _
            " from ms_tipe_trukd d where d.tipe='" & txtTipe.text & "'", conn
            While Not rs.EOF
                 SSDBGrid1.AddItem rs(0) & "#" & rs(1) & "#" & rs(2)
            rs.MoveNext
            Wend
    rs.Close
    
conn.Close
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then Unload Me
If KeyCode = vbKeyF5 Then cmdSearch_Click
If KeyCode = vbKeyF3 Then cmdAcc_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub SSDBGrid1_BtnClick()
If SSDBGrid1.Columns(0).text = "" Then Exit Sub
    If SSDBGrid1.col = 1 Then cmdSearchSeri_Click
End Sub

Private Sub SSDBGrid1_AfterColUpdate(ByVal ColIndex As Integer)
Dim i As Byte
On Error GoTo err
i = 0
If SSDBGrid1.Columns(0).text = "" Then Exit Sub

    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
i = 1
    
    rs.Open "select * from ms_truk_posisi_ban where nopol='" & txtKode.text & "' and posisi='" & SSDBGrid1.Columns(0).text & "' ", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_truk_posisi_ban where nopol='" & txtKode.text & "' and posisi='" & SSDBGrid1.Columns(0).text & "' "
    End If
    

    add_data SSDBGrid1.col
    conn.CommitTrans
i = 0
    DropConnection
Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub add_data(i As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "ms_truk_posisi_ban"
    fields(0) = "nopol"
    fields(1) = "posisi"
    fields(2) = "nomer_seri"
    fields(3) = "merk"
    
    nilai(0) = txtKode.text
    nilai(1) = SSDBGrid1.Columns(0).text
    nilai(2) = SSDBGrid1.Columns(1).text
    nilai(3) = SSDBGrid1.Columns(2).text
        
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub


Private Sub cmdSearchSeri_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from stock_ban"
    frmSearch.nmform = "frmMasterTruk"
    frmSearch.nmctrl = "SSDBGrid1"
    frmSearch.nmctrl2 = "SSDBGrid1"
    
    frmSearch.proc = ""
    frmSearch.col = 0
    frmSearch.col2 = 1
    frmSearch.Index = 1
    frmSearch.Index2 = 2
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
    '
End Sub

Private Sub mnuExit_Click()
Unload Me
End Sub

Private Sub mnuOpen_Click()
  cmdSearch_Click
End Sub
Private Sub reset_form()

    txtKode = ""
    txtKeterangan = ""
    txtKodeAcc = ""
    lblNamaPerkiraan = ""
    
    SSDBGrid1.RemoveAll
    SSDBGrid1.FieldSeparator = "#"
    SSDBGrid1.Columns(0).Locked = True
    SSDBGrid1.Columns(2).Locked = True
    
End Sub
Public Sub cari_truk()
conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_truk where nopol='" & txtKode.text & "' ", conn

    If Not rs.EOF Then
        txtKeterangan.text = rs(1)
        txtKodeAcc.text = rs("kode_acc")
        cmdHapus.Enabled = True
    Else

        txtKodeAcc = ""
        txtKeterangan = ""
        txtIsi = 0
        cmdHapus.Enabled = False
    End If
    If rs.State Then rs.Close
    
'    SSDBGrid1.RemoveAll
'    SSDBGrid1.FieldSeparator = "#"
'    SSDBGrid1.Columns(0).Locked = True
'    SSDBGrid1.Columns(2).Locked = True
'
'    rs.Open " select d.Posisi,d.nomer_seri,d.Kode_ban " & _
'            " from ms_truk_posisi_ban d where d.nopol='" & txtKode.text & "'", conn
'            While Not rs.EOF
'                 SSDBGrid1.AddItem rs(0) & "#" & rs(1) & "#" & rs(2)
'            rs.MoveNext
'            Wend
'    rs.Close
    
    
    conn.Close
    cari_perkiraan
End Sub
Public Sub cari_perkiraan()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_coa where kode_acc='" & txtKodeAcc.text & "'", conn
    If Not rs.EOF Then
        lblNamaPerkiraan.Caption = rs(1)
    Else
        lblNamaPerkiraan.Caption = ""
    End If
    rs.Close
    conn.Close
End Sub

Private Sub SSDBGrid1_LostFocus()
    SSDBGrid1.Refresh
    SSDBGrid1.Update
End Sub

Private Sub txtKode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 And txtKode <> "" Then cari_truk
End Sub
