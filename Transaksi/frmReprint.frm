VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmReprint 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Reprint"
   ClientHeight    =   1695
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   3810
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1695
   ScaleWidth      =   3810
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtNoTrans 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   945
      TabIndex        =   0
      Top             =   90
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      BackColor       =   &H80000003&
      Caption         =   "Print (F2)"
      Height          =   330
      Left            =   1395
      Picture         =   "frmReprint.frx":0000
      TabIndex        =   1
      Top             =   1125
      Width           =   915
   End
   Begin VB.ComboBox cmbBayar 
      Height          =   315
      Left            =   2790
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   90
      Visible         =   0   'False
      Width           =   705
   End
   Begin VB.CommandButton cmdSearchID 
      BackColor       =   &H80000003&
      Caption         =   "F5"
      Height          =   330
      Left            =   2340
      Picture         =   "frmReprint.frx":0102
      TabIndex        =   2
      Top             =   90
      Width           =   375
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   180
      Top             =   450
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label lblBayar 
      Alignment       =   1  'Right Justify
      Height          =   240
      Left            =   900
      TabIndex        =   6
      Top             =   765
      Width           =   1275
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Height          =   240
      Left            =   945
      TabIndex        =   5
      Top             =   495
      Width           =   1230
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   135
      Width           =   825
   End
   Begin VB.Menu mnu 
      Caption         =   "menu"
      Begin VB.Menu mnuPrint 
         Caption         =   "Print"
         Shortcut        =   {F2}
      End
   End
End
Attribute VB_Name = "frmReprint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sname As String
Private Sub cmbBayar_Click()
    Dim rs As New ADODB.Recordset
    Dim con As New ADODB.Connection
    
    con.Open strcon
    rs.Open "select [jumlah_bayar] from t_bayarpiutang_jual where no_jual='" & txtNoTrans.text & "' and id='" & cmbBayar.text & "'", con
    If Not rs.EOF Then
        lblBayar = rs(0)
    Else
        lblBayar = ""
    End If
    rs.Close
    con.Close
End Sub

Private Sub cmdPrint_Click()
    On Error GoTo err
    If txtNoTrans.text <> "" And cmbBayar.text <> "" Then
    With CRPrint
        .reset
        .ReportFileName = reportDir & "\Nota.rpt"
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            If ServerType = "mysql" Or ServerType = "mssql" Then
                .LogonInfo(i) = "DSN=" & servname & ";UID=Admin"  '& ";PWD=" & pwd & ";"
                
'            ElseIf ServerType = "access" Then
                '.DataFiles(i) = servname
'            End If
        Next
        .SelectionFormula = "{t_penjualanh.id}='" & txtNoTrans.text & "' and {t_bayarpiutang_jual.id}='" & cmbBayar.text & "'"
'        .PrinterSelect
        .ProgressDialog = False
        .Destination = crptToWindow 'crptToPrinter
        .WindowState = crptMaximized
        .action = 1
    End With
    Unload Me
    Exit Sub
    Else
        MsgBox "Silahkan mengisi nomor nota dan pembayaran terlebih dahulu"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = "select ID,TANGGAL,[KODE CUSTOMER],TOTAL,[jumlah_bayar] FROM T_PENJUALANH where gudang='" & gudang & "'"
    frmSearch.chkDate.Visible = True
    frmSearch.DTPicker1.Visible = True
    frmSearch.DTPicker1 = Now
    frmSearch.chkDate.value = 1
    frmSearch.nmform = "frmReprint"
    frmSearch.nmctrl = "txtNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub cek_notrans()
On Error GoTo err
If txtNoTrans <> "" Then

    conn.Open strcon
rs.Open "select * from t_penjualanh where id='" & txtNoTrans & "'", conn
If Not rs.EOF Then
    lblTotal = rs("total")
    rs.Close
    cmbBayar.Clear
    rs.Open "select * from t_bayarpiutang_jual where no_jual='" & txtNoTrans & "'", conn
    While Not rs.EOF
        cmbBayar.AddItem rs(0)
        rs.MoveNext
    Wend
    If cmbBayar.ListCount > 0 Then cmbBayar.ListIndex = 0
    If cmbBayar.ListCount > 1 Then
        cmbBayar.Visible = True
    Else
        cmbBayar.Visible = False
    End If
    rs.Close
    
Else
    lblTotal = ""
    lblBayar = ""
    cmbBayar.Visible = False
    MsgBox "Nomor tersebut tidak ditemukan"
    txtNoTrans.SetFocus
    
End If
If rs.State Then rs.Close
conn.Close
End If
Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF2 Then cmdPrint_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
    KeyAscii = 0
    Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    sname = servnameasli
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmAddPenjualan.Enabled = True
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub txtNoTrans_LostFocus()
    cek_notrans
End Sub
