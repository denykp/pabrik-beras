VERSION 5.00
Begin VB.Form frmDataPerusahaan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Data Perusahaan"
   ClientHeight    =   3180
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   7515
   FillColor       =   &H8000000F&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   7515
   Begin VB.TextBox txtNPWP 
      Height          =   330
      Left            =   1920
      MaxLength       =   50
      TabIndex        =   3
      Top             =   1830
      Width           =   4185
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   510
      Left            =   2010
      Picture         =   "frmDataPerusahaan.frx":0000
      TabIndex        =   4
      Top             =   2520
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtTelp 
      Height          =   330
      Left            =   1920
      MaxLength       =   50
      TabIndex        =   2
      Top             =   1440
      Width           =   3045
   End
   Begin VB.TextBox txtAlamat 
      Height          =   720
      Left            =   1920
      MaxLength       =   50
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   630
      Width           =   4140
   End
   Begin VB.TextBox txtNama 
      Height          =   330
      Left            =   1920
      MaxLength       =   50
      TabIndex        =   0
      Top             =   240
      Width           =   4125
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      Height          =   510
      Left            =   3270
      Picture         =   "frmDataPerusahaan.frx":0102
      TabIndex        =   5
      Top             =   2520
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   510
      Left            =   4530
      Picture         =   "frmDataPerusahaan.frx":0204
      TabIndex        =   6
      Top             =   2520
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1560
      TabIndex        =   15
      Top             =   1875
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      Height          =   240
      Left            =   195
      TabIndex        =   14
      Top             =   675
      Width           =   720
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Telp"
      Height          =   240
      Left            =   195
      TabIndex        =   13
      Top             =   1470
      Width           =   720
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1635
      TabIndex        =   12
      Top             =   690
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1635
      TabIndex        =   11
      Top             =   1485
      Width           =   105
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "NPWP"
      Height          =   240
      Left            =   180
      TabIndex        =   10
      Top             =   1860
      Width           =   1245
   End
   Begin VB.Label lblParent 
      AutoSize        =   -1  'True
      Height          =   240
      Left            =   3375
      TabIndex        =   9
      Top             =   330
      Width           =   60
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Perusahaan"
      Height          =   240
      Left            =   195
      TabIndex        =   8
      Top             =   255
      Width           =   780
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1635
      TabIndex        =   7
      Top             =   255
      Width           =   105
   End
   Begin VB.Menu mune1 
      Caption         =   "Data"
      Begin VB.Menu menu2 
         Caption         =   "Open "
         Shortcut        =   {F5}
      End
      Begin VB.Menu menu3 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmDataPerusahaan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdKeluar_Click()
Unload Me
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    conn.Execute "delete from setting_perusahaan"
    conn.Execute "insert into setting_perusahaan (nama_perusahaan,telp,alamat,NPWP) " & _
                 "values ('" & txtNama.text & "','" & txtTelp.text & "','" & txtAlamat.text & "','" & txtNPWP.text & "')"
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah disimpan !", vbInformation
    load_IDperusahaan
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Public Sub cari_data()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from setting_perusahaan ", conn
    If Not rs.EOF Then
        txtNama.text = rs(0)
        txtTelp.text = rs(1)
        txtAlamat.text = rs(2)
        txtNPWP.text = rs(3)
    End If
    rs.Close
   
    conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    cari_data
End Sub
Private Sub menu2_Click()
'    cmdSearch_Click
End Sub

Private Sub menu3_Click()
Unload Me
End Sub

Sub reset_form()
txtNama = ""
txtAlamat = ""
txtTelp = ""
txtNPWP = ""
End Sub
