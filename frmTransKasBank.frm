VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0A45DB48-BD0D-11D2-8D14-00104B9E072A}#2.0#0"; "sstabs2.ocx"
Begin VB.Form frmTransKasBank 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Kas & Bank"
   ClientHeight    =   9255
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   11520
   DrawStyle       =   1  'Dash
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9255
   ScaleWidth      =   11520
   Begin ActiveTabs.SSActiveTabs SSActiveTabs1 
      Height          =   6360
      Left            =   135
      TabIndex        =   22
      Top             =   1530
      Width           =   11265
      _ExtentX        =   19870
      _ExtentY        =   11218
      _Version        =   131083
      TabCount        =   2
      TagVariant      =   ""
      Tabs            =   "frmTransKasBank.frx":0000
      Begin ActiveTabs.SSActiveTabPanel SSActiveTabPanel2 
         Height          =   5970
         Left            =   -99969
         TabIndex        =   24
         Top             =   360
         Width           =   11205
         _ExtentX        =   19764
         _ExtentY        =   10530
         _Version        =   131083
         TabGuid         =   "frmTransKasBank.frx":008F
         Begin VB.Frame Frame1 
            BackColor       =   &H8000000C&
            Caption         =   "Detail"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   1815
            Left            =   90
            TabIndex        =   43
            Top             =   135
            Width           =   11000
            Begin VB.TextBox txtNilaiKasbank 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1875
               TabIndex        =   50
               Top             =   660
               Width           =   2055
            End
            Begin VB.CommandButton cmdSearchKas 
               Caption         =   "F4"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   7755
               TabIndex        =   47
               Top             =   270
               Width           =   375
            End
            Begin VB.TextBox txtNamaKasBank 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   3525
               Locked          =   -1  'True
               TabIndex        =   46
               Top             =   270
               Width           =   4155
            End
            Begin VB.TextBox txtAccKasBank 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1875
               Locked          =   -1  'True
               TabIndex        =   45
               Top             =   270
               Width           =   1605
            End
            Begin VB.CommandButton cmdTambahkan 
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   180
               TabIndex        =   44
               Top             =   1335
               Width           =   1350
            End
            Begin VB.Label Label123 
               BackColor       =   &H8000000C&
               Caption         =   "Nilai"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   240
               Left            =   180
               TabIndex        =   51
               Top             =   705
               Width           =   600
            End
            Begin VB.Label lblKasBank 
               BackStyle       =   0  'Transparent
               Caption         =   "Acc. Kas/Bank"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   240
               Left            =   180
               TabIndex        =   48
               Top             =   315
               Width           =   1320
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid vsflex 
            Height          =   2850
            Left            =   90
            TabIndex        =   49
            Top             =   2025
            Width           =   10995
            _cx             =   19394
            _cy             =   5027
            Appearance      =   1
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   -2147483635
            ForeColorSel    =   -2147483634
            BackColorBkg    =   8421504
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   1
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   0
            SelectionMode   =   0
            GridLines       =   1
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   1
            Cols            =   4
            FixedRows       =   1
            FixedCols       =   1
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   0   'False
            FormatString    =   $"frmTransKasBank.frx":00B7
            ScrollTrack     =   0   'False
            ScrollBars      =   3
            ScrollTips      =   0   'False
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   0
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   0
            ExplorerBar     =   0
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   0
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
      Begin ActiveTabs.SSActiveTabPanel ssTab 
         Height          =   5970
         Left            =   30
         TabIndex        =   23
         Top             =   360
         Width           =   11205
         _ExtentX        =   19764
         _ExtentY        =   10530
         _Version        =   131083
         TabGuid         =   "frmTransKasBank.frx":01BB
         Begin VB.Frame Frame2 
            BackColor       =   &H8000000C&
            Caption         =   "Detail"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   2805
            Left            =   90
            TabIndex        =   25
            Top             =   135
            Width           =   11000
            Begin VB.CommandButton cmdSearchNoPengeluaran 
               Caption         =   "F6"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   3990
               TabIndex        =   34
               Top             =   690
               Width           =   375
            End
            Begin VB.TextBox txtNoPengeluaran 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1875
               TabIndex        =   33
               Top             =   660
               Width           =   2040
            End
            Begin VB.TextBox txtKeteranganDetail 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1875
               TabIndex        =   32
               Top             =   1500
               Width           =   6225
            End
            Begin VB.TextBox txtNilai 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   1875
               TabIndex        =   31
               Top             =   1080
               Width           =   2055
            End
            Begin VB.CommandButton cmdSearchAcc 
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   3450
               TabIndex        =   30
               Top             =   270
               Width           =   375
            End
            Begin VB.CommandButton cmdDelete 
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2700
               TabIndex        =   29
               Top             =   2205
               Width           =   1050
            End
            Begin VB.CommandButton cmdClear 
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1620
               TabIndex        =   28
               Top             =   2205
               Width           =   1050
            End
            Begin VB.CommandButton cmdOk 
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   225
               TabIndex        =   27
               Top             =   2205
               Width           =   1350
            End
            Begin VB.TextBox txtAcc 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1875
               TabIndex        =   26
               Top             =   270
               Width           =   1500
            End
            Begin VB.Label Label3 
               BackColor       =   &H8000000C&
               Caption         =   "No. Pengeluaran"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H8000000E&
               Height          =   300
               Left            =   180
               TabIndex        =   41
               Top             =   705
               Width           =   1620
            End
            Begin VB.Label Label1 
               BackColor       =   &H8000000C&
               Caption         =   "Keterangan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   240
               Index           =   0
               Left            =   180
               TabIndex        =   40
               Top             =   1545
               Width           =   1050
            End
            Begin VB.Label LblNamaAcc 
               BackColor       =   &H8000000C&
               Caption         =   "Nama Account"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H8000000E&
               Height          =   300
               Left            =   3900
               TabIndex        =   39
               Top             =   315
               Width           =   3525
            End
            Begin VB.Label Label1 
               BackColor       =   &H8000000C&
               Caption         =   "Nilai"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   240
               Index           =   1
               Left            =   180
               TabIndex        =   38
               Top             =   1125
               Width           =   600
            End
            Begin VB.Label lblDefQtyJual 
               BackColor       =   &H8000000C&
               Caption         =   "Label12"
               ForeColor       =   &H8000000C&
               Height          =   285
               Left            =   4185
               TabIndex        =   37
               Top             =   1575
               Width           =   870
            End
            Begin VB.Label lblID 
               BackColor       =   &H8000000C&
               Caption         =   "Label12"
               ForeColor       =   &H8000000C&
               Height          =   240
               Left            =   4050
               TabIndex        =   36
               Top             =   1125
               Width           =   870
            End
            Begin VB.Label Label14 
               BackColor       =   &H8000000C&
               Caption         =   "Account"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   240
               Left            =   180
               TabIndex        =   35
               Top             =   315
               Width           =   1050
            End
         End
         Begin MSFlexGridLib.MSFlexGrid flxGrid 
            Height          =   2850
            Left            =   90
            TabIndex        =   42
            Top             =   2970
            Width           =   10995
            _ExtentX        =   19394
            _ExtentY        =   5027
            _Version        =   393216
            Cols            =   8
            AllowBigSelection=   -1  'True
            SelectionMode   =   1
            AllowUserResizing=   1
            BorderStyle     =   0
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6525
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   8505
      Width           =   1230
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5325
      Picture         =   "frmTransKasBank.frx":01E3
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4230
      Picture         =   "frmTransKasBank.frx":0715
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.TextBox txtKetBG 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5565
      TabIndex        =   18
      Top             =   990
      Width           =   2040
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting (F5)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5145
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   8505
      Width           =   1230
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   10
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7035
      TabIndex        =   9
      Top             =   510
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2385
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   8505
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   8
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1605
      TabIndex        =   1
      Top             =   990
      Width           =   3885
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   7890
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   8505
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3780
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   8505
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1605
      TabIndex        =   0
      Top             =   585
      Width           =   2475
      _ExtentX        =   4366
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   93585411
      CurrentDate     =   38927
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8820
      Top             =   540
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label8 
      Caption         =   "Total Acc Kas/Bank"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5895
      TabIndex        =   53
      Top             =   8000
      Width           =   2490
   End
   Begin VB.Label lblTotalKasBank 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8460
      TabIndex        =   52
      Top             =   8000
      Width           =   2610
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   16
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6705
      TabIndex        =   15
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8055
      TabIndex        =   14
      Top             =   225
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8235
      TabIndex        =   13
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3030
      TabIndex        =   12
      Top             =   8000
      Width           =   2610
   End
   Begin VB.Label Label17 
      Caption         =   "Total Acc Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   495
      TabIndex        =   11
      Top             =   8000
      Width           =   2490
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   7
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   6
      Top             =   1020
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   5
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransKasBank"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryCOA As String = "select * from ms_coa where fg_aktif = 1"

Dim mode As Byte
Dim totalNilai As Currency

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String
Public Tipe As String

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 no_transaksi from t_kasbankh where substring(no_transaksi,3,2)='" & Format(DTPicker1, "yy") & "' and substring(no_transaksi,5,2)='" & Format(DTPicker1, "MM") & "' and tipe='" & Tipe & "' order by no_transaksi desc", conn
    If Not rs.EOF Then
        No = Tipe & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = Tipe & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub cmdClear_Click()
    txtAcc.text = ""
    debet = True
    lblNamaAcc = ""
    txtNilai.text = "0"
    txtKeteranganDetail.text = ""
    txtNoPengeluaran.text = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    txtAccKasBank.text = ""
    txtNamaKasBank.text = ""
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    Row = flxGrid.Row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalNilai = Format(totalNilai - CCur(flxGrid.TextMatrix(Row, 3)), "#,##0.##")
    lblTotal = Format(totalNilai, "#,##0.##")
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    hitung_total
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [no_transaksi] from t_kasbankh where [no_transaksi]>'" & lblNoTrans & "' order by [no_transaksi]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
If txtAcc.text = "" Then
    MsgBox "Acc Pengeluaran harus diisi"
    Exit Sub
End If
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtAcc.text <> "" And lblNamaAcc <> "" Then
        
'        For i = 1 To flxGrid.Rows - 1
'            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtAcc.text) Then row = i
'        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(Row, 1) = txtAcc.text
        flxGrid.TextMatrix(Row, 2) = lblNamaAcc
        
        If flxGrid.TextMatrix(Row, 3) <> "" Then totalNilai = Format(totalNilai - CCur(flxGrid.TextMatrix(Row, 3)), "#,##0.##")
        flxGrid.TextMatrix(Row, 3) = IIf(txtNilai <> 0, Format(txtNilai, "#,##0.##"), Format(txtNilai, "#,##0.##"))
        flxGrid.TextMatrix(Row, 4) = txtKeteranganDetail
        flxGrid.TextMatrix(Row, 5) = txtNoPengeluaran
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = 3
        
        hitung_total
        
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtAcc.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub hitung_total()
    Dim Row As Integer
    Dim totalNilai As Currency
    Dim totalNilaiKB As Currency
    For Row = 1 To flxGrid.Rows - 2
        totalNilai = totalNilai + CCur(flxGrid.TextMatrix(Row, 3))
    Next
    For Row = 1 To vsflex.Rows - 1
        totalNilaiKB = totalNilaiKB + CCur(vsflex.TextMatrix(Row, 3))
    Next
    lblTotal = Format(totalNilai, "#,##0.##")
    lblTotalKasBank = Format(totalNilaiKB, "#,##0.##")
End Sub

Private Sub cmdPosting_Click()
    cmdSimpan_Click
    Call Posting_KasBank(lblNoTrans, conn)
    reset_form
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [no_transaksi] from t_kasbankh where [no_transaksi]<'" & lblNoTrans & "' order by [no_transaksi] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = queryCOA
    frmSearch.nmform = "frmTransKasBank"
    frmSearch.nmctrl = "txtAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub search_cari()
On Error Resume Next
    If cek_Acc Then MySendKeys "{tab}"
End Sub

Public Function cek_Acc() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_coa where fg_aktif = 1 and kode_acc='" & txtAcc & "' ", conn
    If Not rs.EOF Then
        lblNamaAcc = rs(1)
        cek_Acc = True
        'If rs(4) = "D" Then debet = True Else debet = False
    Else
        lblNamaAcc = ""
        lblKode = ""
        
        cek_Acc = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select no_transaksi,Tanggal,Keterangan,Status_Posting from t_kasbankh where tipe='" & Tipe & "'"
    frmSearch.nmform = "frmAddjurnal"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "kasBankH"
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKas_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1"
    frmSearch.nmform = "frmSetting"
    frmSearch.nmctrl = "txtAccKasBank"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
'    frmSearch.txtSearch.text = txtKodeCustomer.text
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_kas"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtKeterangan.SetFocus
End Sub

Public Sub cari_kas()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_coa where fg_aktif = 1 and kode_acc='" & txtAccKasBank.text & "'", conn
    If Not rs.EOF Then
        txtNamaKasBank = rs(1)
    Else
        txtNamaKasBank = ""
    End If
    rs.Close
    conn.Close
End Sub

Private Sub cmdSearchNoPengeluaran_Click()
frmSearch.connstr = strcon
    frmSearch.query = "Select * from t_suratjalanH where status_kas='0' and status_posting<>'0' and (biaya_angkut+biaya_lain)>0 and cara_bayar='T'"
    frmSearch.nmform = "frmTransKasBank"
    frmSearch.nmctrl = "txtNoPengeluaran"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "t_SuratJalanH"
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_NoPengeluaran"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtKeterangan.SetFocus
End Sub

Public Sub cari_NoPengeluaran()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select (biaya_angkut+biaya_lain) as TotalBiaya from t_suratjalanH where nomer_suratjalan='" & txtNoPengeluaran.text & "'", conn
    If Not rs.EOF Then
        txtNilai = rs("TotalBiaya")
    Else
        txtNilai = 0
    End If
    rs.Close
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
If CCur(lblTotal.Caption) <> CCur(lblTotalKasBank.Caption) Then
    MsgBox "Total nilai akun transaksi dan akun kasbank harus sama"
    Exit Sub
End If
Dim i As Integer
Dim id As String
Dim Row As Integer

On Error GoTo err
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan detail terlebih dahulu"
        txtAcc.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Or Len(lblNoTrans) < 10 Then
        nomor_baru
'        NoJurnal = Nomor_Jurnal(DTPicker1)
    Else
        
            rs.Open "select no_jurnal from t_jurnalh where no_transaksi='" & lblNoTrans & "'", conn
            If Not rs.EOF Then
                NoJurnal = rs("no_jurnal")
            End If
            rs.Close
            
            conn.Execute "delete from t_kasbankh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_kasbankd where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_kasbankd2 where no_transaksi='" & lblNoTrans & "'"
            
            conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
            
            
'            conn.Execute "delete from jurnal where no_trans='" & lblNoTrans & "'"
        
    End If
    
    
    
    add_dataheader
    
    For Row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(Row, 1) <> "" Then
            add_datadetail (Row)
        End If
    Next
    
    For Row = 1 To vsflex.Rows - 1
        If vsflex.TextMatrix(Row, 1) <> "" Then
            add_datadetail2 Row
        End If
    Next
    
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah tersimpan"
'    reset_form
'    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub


Public Sub reset_form()
    lblNoTrans = "-"
    txtAccKasBank.text = ""
    txtNamaKasBank.text = ""
'    txtKeterangan.text = ""
    txtKetBG.text = ""
    totalNilai = 0
    txtNilaiKasbank.text = 0
'    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    vsflex.Rows = 1
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_kasbankh"
    fields(0) = "no_transaksi"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "total"
    fields(4) = "tipe"
    fields(5) = "userid"
    fields(6) = "ket_BG"
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = CCur(lblTotal.Caption)
    nilai(4) = Tipe
    nilai(5) = User
    nilai(6) = txtKetBG.text
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "t_kasbankd"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "nilai"
    fields(3) = "no_urut"
    fields(4) = "keterangan"
    fields(5) = "no_pengeluaran"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = CCur(flxGrid.TextMatrix(Row, 3))
    nilai(3) = Row
    nilai(4) = flxGrid.TextMatrix(Row, 4)
    nilai(5) = flxGrid.TextMatrix(Row, 5)
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail2(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_kasbankd2"
    fields(0) = "no_transaksi"
    fields(1) = "acc_kasbank"
    fields(2) = "nilai"
    fields(3) = "no_urut"
    
    nilai(0) = lblNoTrans
    nilai(1) = vsflex.TextMatrix(Row, 1)
    nilai(2) = CCur(vsflex.TextMatrix(Row, 3))
    nilai(3) = Row
    
    tambah_data table_name, fields, nilai
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim StatusPosting As String
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select * from t_kasbankh " & _
            "where no_transaksi='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
        
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtKetBG.text = rs("ket_BG")
        StatusPosting = rs("status_posting")
        totalNilai = 0
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1
        
        rs.Open "select d.kode_acc,m.Nama,d.nilai,d.keterangan,d.no_pengeluaran as nama_kasbank from t_kasbankd d inner join ms_coa m on d.kode_acc=m.kode_acc where d.no_transaksi='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 2) = rs(1)
                flxGrid.TextMatrix(Row, 3) = IIf(rs(2) <> 0, Format(rs(2), "#,##0.##"), Format(rs(2), "#,##0.##"))
                flxGrid.TextMatrix(Row, 4) = rs(3)
                flxGrid.TextMatrix(Row, 5) = rs(4)
                
                Row = Row + 1
                totalNilai = Format(totalNilai + rs(2), "#,##0.##")
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblTotal = Format(totalNilai, "#,##0.##")
        vsflex.Rows = 1
        rs.Open "select d.*,m.nama from t_kasbankd2 d left join ms_coa m on d.acc_kasbank=m.kode_acc where no_transaksi = '" & lblNoTrans.Caption & "'", conn
        While Not rs.EOF
            vsflex.AddItem vbTab & rs!acc_kasbank & vbTab & rs!nama & vbTab & rs!nilai
            rs.MoveNext
        Wend
        rs.Close
        If StatusPosting = "1" Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
        End If

        
    End If
    If rs.State Then rs.Close
    conn.Close
    hitung_total
End Sub

Private Sub Command1_Click()
End Sub

Private Sub cmdTambahkan_Click()
    If txtAccKasBank.text = "" Then
        MsgBox "Acc Kasbank harus diisi"
        Exit Sub
    End If
    vsflex.AddItem vbTab & txtAccKasBank.text & vbTab & txtNamaKasBank.text & vbTab & txtNilaiKasbank.text
    hitung_total
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then

    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtAcc.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            cek_Acc
            txtNilai.text = flxGrid.TextMatrix(flxGrid.Row, 3)
            txtKeteranganDetail.text = flxGrid.TextMatrix(flxGrid.Row, 4)
            txtNoPengeluaran.text = flxGrid.TextMatrix(flxGrid.Row, 5)
            txtAccKasBank.text = flxGrid.TextMatrix(flxGrid.Row, 6)
            txtNamaKasBank.text = flxGrid.TextMatrix(flxGrid.Row, 7)
            txtNilai.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchAcc_Click
    If KeyCode = vbKeyF4 Then cmdSearchKas_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF6 Then cmdSearchNoPengeluaran_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtAcc.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    lblNamaAcc = ""
    loadcombo
    reset_form
    total = 0
    
    DTPicker1 = Now
    
    
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    'End If
    'rs.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1500
    flxGrid.ColWidth(2) = 3100
    flxGrid.ColWidth(3) = 1100
    flxGrid.ColWidth(4) = 2600
    flxGrid.ColWidth(5) = 1500
    
    flxGrid.TextMatrix(0, 1) = "Kode Acc"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Nilai"
    flxGrid.TextMatrix(0, 4) = "Keterangan"
    flxGrid.TextMatrix(0, 5) = "No. Pengeluaran"
End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtAcc_GotFocus()
    txtAcc.SelStart = 0
    txtAcc.SelLength = Len(txtAcc.text)
'    foc = 1
End Sub

Private Sub txtAcc_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
'        If txtAcc.text = "" Then
'            flxGrid_Click
'        Else
        If txtAcc.text = "" Then KeyCode = 0
        
        If txtAcc.text <> "" Then
            If Not cek_Acc Then
                MsgBox "Kode yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    End If
End Sub


Private Sub txtKetBG_KeyDown(KeyCode As Integer, Shift As Integer)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    If KeyCode = 13 Then
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select NOMINAL from list_bg where nomer_bg='" & txtKetBG.text & "' and status_cair='0'", conn
    If Not rs.EOF Then
        txtNilai = rs(0)
    Else
        txtNilai = "0"
    End If
    rs.Close
    conn.Close
    End If
        
End Sub

Private Sub txtNilai_GotFocus()
    txtNilai.SelStart = 0
    txtNilai.SelLength = Len(txtNilai.text)
End Sub

Private Sub txtNilai_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtNilai_LostFocus()
    If Not IsNumeric(txtNilai.text) Then txtNilai.text = "1"
End Sub

Private Sub txtKeterangan_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtAcc.SetFocus
End Sub

Private Sub txtNoPengeluaran_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF6 Then cmdSearchNoPengeluaran_Click
End Sub

Private Sub vsflex_DblClick()
    txtAccKasBank.text = vsflex.TextMatrix(Row, 1)
    txtNamaKasBank.text = vsflex.TextMatrix(Row, 2)
    txtNilaiKasbank.text = vsflex.TextMatrix(Row, 3)
    hitung_total
End Sub

Private Sub vsflex_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If vsflex.Row > 0 Then
            vsflex.RemoveItem (vsflex.Row)
            hitung_total
        End If
    ElseIf KeyCode = vbKeyReturn Then
        txtAccKasBank.text = vsflex.TextMatrix(Row, 1)
        txtNamaKasBank.text = vsflex.TextMatrix(Row, 2)
        txtNilaiKasbank.text = vsflex.TextMatrix(Row, 3)
        hitung_total
    End If
End Sub
