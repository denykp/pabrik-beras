VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransKonsinyasiJual 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tambah Konsinyasi"
   ClientHeight    =   9420
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   14280
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9420
   ScaleWidth      =   14280
   Begin VB.TextBox txtPPN 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   11700
      TabIndex        =   57
      Top             =   7695
      Width           =   960
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   10530
      ScaleHeight     =   345
      ScaleWidth      =   3090
      TabIndex        =   53
      Top             =   900
      Width           =   3120
      Begin VB.OptionButton OptLunas 
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1575
         TabIndex        =   55
         Top             =   45
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.OptionButton OptLunas 
         Caption         =   "Tunai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   54
         Top             =   45
         Width           =   915
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah bayar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   56
         Top             =   1035
         Visible         =   0   'False
         Width           =   1725
      End
   End
   Begin VB.Frame frJT 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   420
      Left            =   10530
      TabIndex        =   50
      Top             =   1305
      Width           =   3480
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   1530
         TabIndex        =   51
         Top             =   45
         Width           =   1875
         _ExtentX        =   3307
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   93257731
         CurrentDate     =   38927
      End
      Begin VB.Label Label5 
         Caption         =   "Jatuh Tempo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   45
         TabIndex        =   52
         Top             =   75
         Width           =   1320
      End
   End
   Begin VB.TextBox txtKodeToko 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1665
      TabIndex        =   42
      Top             =   495
      Width           =   1875
   End
   Begin VB.CommandButton cmdSearchToko 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   41
      Top             =   495
      Width           =   420
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   1665
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   930
      Width           =   8250
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      Picture         =   "frmTransKonsinyasiJual.frx":0000
      TabIndex        =   36
      Top             =   45
      Width           =   420
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   225
      Top             =   9720
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Simpan + &Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5940
      Picture         =   "frmTransKonsinyasiJual.frx":0102
      TabIndex        =   33
      Top             =   8775
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3105
      Picture         =   "frmTransKonsinyasiJual.frx":0204
      TabIndex        =   14
      Top             =   8775
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   315
      Picture         =   "frmTransKonsinyasiJual.frx":0306
      TabIndex        =   11
      Top             =   8775
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4500
      Picture         =   "frmTransKonsinyasiJual.frx":0408
      TabIndex        =   13
      Top             =   8775
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1710
      Picture         =   "frmTransKonsinyasiJual.frx":050A
      TabIndex        =   12
      Top             =   8775
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   6825
      Index           =   0
      Left            =   180
      TabIndex        =   15
      Top             =   1800
      Width           =   10215
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2310
         Left            =   90
         TabIndex        =   16
         Top             =   45
         Width           =   9960
         Begin VB.TextBox txtHarga 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   6
            Top             =   1845
            Width           =   1140
         End
         Begin VB.CommandButton cmdSearchSerial 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3285
            Picture         =   "frmTransKonsinyasiJual.frx":060C
            TabIndex        =   35
            Top             =   720
            Width           =   420
         End
         Begin VB.TextBox txtNomerSerial 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   3
            Top             =   720
            Width           =   1950
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   4
            Top             =   1080
            Width           =   1140
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   3330
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   18
            Top             =   1665
            Width           =   3795
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   7
               Top             =   90
               Width           =   1350
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   8
               Top             =   90
               Width           =   1050
            End
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   9
               Top             =   90
               Width           =   1050
            End
         End
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   5
            Top             =   1455
            Width           =   1140
         End
         Begin VB.CommandButton cmdSearchBrg 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3915
            Picture         =   "frmTransKonsinyasiJual.frx":070E
            TabIndex        =   17
            Top             =   360
            Width           =   420
         End
         Begin VB.ComboBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1260
            Style           =   1  'Simple Combo
            TabIndex        =   2
            Text            =   "Combo1"
            Top             =   315
            Width           =   2580
         End
         Begin VB.Label Label10 
            BackColor       =   &H8000000C&
            Caption         =   "Harga"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   44
            Top             =   1875
            Width           =   1020
         End
         Begin VB.Label Label1 
            BackColor       =   &H8000000C&
            Caption         =   "Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   34
            Top             =   765
            Width           =   1005
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2475
            TabIndex        =   25
            Top             =   1080
            Width           =   1050
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   24
            Top             =   1395
            Width           =   600
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4545
            TabIndex        =   23
            Top             =   405
            Width           =   3480
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            Caption         =   "Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   22
            Top             =   1125
            Width           =   1005
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   21
            Top             =   360
            Width           =   1050
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   20
            Top             =   1485
            Width           =   1020
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2475
            TabIndex        =   19
            Top             =   1515
            Width           =   600
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2430
         Left            =   90
         TabIndex        =   10
         Top             =   2430
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   4286
         _Version        =   393216
         Cols            =   9
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxgridRekap 
         Height          =   1440
         Left            =   90
         TabIndex        =   30
         Top             =   4950
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   2540
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label24 
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6210
         TabIndex        =   32
         Top             =   6435
         Width           =   960
      End
      Begin VB.Label lblBerat 
         Alignment       =   1  'Right Justify
         Caption         =   "lblBerat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7155
         TabIndex        =   31
         Top             =   6435
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   29
         Top             =   6435
         Width           =   735
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   945
         TabIndex        =   28
         Top             =   6435
         Width           =   1140
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4320
         TabIndex        =   27
         Top             =   6435
         Width           =   1140
      End
      Begin VB.Label Label17 
         Caption         =   "Jml Kemasan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   26
         Top             =   6435
         Width           =   1770
      End
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7380
      TabIndex        =   0
      Top             =   60
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   172818435
      CurrentDate     =   38927
   End
   Begin VB.Label Label9 
      Caption         =   "PPN"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10575
      TabIndex        =   49
      Top             =   7695
      Width           =   960
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "lblBerat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11520
      TabIndex        =   48
      Top             =   8190
      Width           =   1140
   End
   Begin VB.Label Label6 
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10575
      TabIndex        =   47
      Top             =   8190
      Width           =   960
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11520
      TabIndex        =   46
      Top             =   7200
      Width           =   1140
   End
   Begin VB.Label Label2 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10575
      TabIndex        =   45
      Top             =   7200
      Width           =   960
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Toko"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   43
      Top             =   540
      Width           =   1050
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   40
      Top             =   930
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5895
      TabIndex        =   39
      Top             =   105
      Width           =   1320
   End
   Begin VB.Label Label22 
      Caption         =   "No.Pengeluaran"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   38
      Top             =   135
      Width           =   1410
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1710
      TabIndex        =   37
      Top             =   45
      Width           =   2085
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransKonsinyasiJual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte


Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    row = flxGrid.row
    totalQty = totalQty - flxGrid.TextMatrix(row, 5)
    totalberat = totalberat - flxGrid.TextMatrix(row, 6)
    lblQty = Format(totalQty, "#,##0.##")
    lblBerat = Format(totalberat, "#,##0.##")
    delete_rekap
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub cmdOk_Click()
On Error GoTo err
Dim i As Integer
Dim row As Integer
    row = 0
    
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
'        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row
'        flxGrid.TextMatrix(row, 1) = txtNoOrder
        flxGrid.TextMatrix(row, 2) = txtKdBrg.text
        flxGrid.TextMatrix(row, 3) = LblNamaBarang1
        flxGrid.TextMatrix(row, 4) = txtNomerSerial
        flxGrid.TextMatrix(row, 5) = txtQty
        flxGrid.TextMatrix(row, 6) = txtBerat
        flxGrid.TextMatrix(row, 7) = txtHarga
        flxGrid.TextMatrix(row, 8) = txtHarga * txtBerat
        
        
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(row, 5)
        totalberat = totalberat + flxGrid.TextMatrix(row, 6)
        total = total + flxGrid.TextMatrix(row, 8)
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        lblTotal = Format(total, "#,##0.##")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 9 Then
            flxGrid.TopRow = row - 8
        Else
            flxGrid.TopRow = 1
        End If
        add_rekap
        lblSatuan = ""
        
        txtNomerSerial = ""
        txtQty.text = "1"
        txtBerat.text = "1"
        On Error Resume Next
        txtNomerSerial.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub add_rekap()
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = txtKdBrg.text Then row = i
    Next
    If row = 0 Then
        row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.row, 0) = ""
    .row = row


    .TextMatrix(row, 1) = txtKdBrg.text
    .TextMatrix(row, 2) = LblNamaBarang1
    If .TextMatrix(row, 3) = "" Then .TextMatrix(row, 3) = txtQty Else .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) + txtQty
    If .TextMatrix(row, 4) = "" Then .TextMatrix(row, 4) = txtBerat Else .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) + txtBerat
    lblItem = .Rows - 2
    End With
End Sub
Private Sub delete_rekap()
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = flxGrid.TextMatrix(flxGrid.row, 2) Then row = i
    Next
    If row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(row, 3)) Then .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) - flxGrid.TextMatrix(flxGrid.row, 5)
    If IsNumeric(.TextMatrix(row, 4)) Then .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) - flxGrid.TextMatrix(flxGrid.row, 6)
    If .TextMatrix(row, 4) = 0 Or .TextMatrix(row, 3) = 0 Then
        For row = row To .Rows - 1
            If row = .Rows - 1 Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(row + 1, 1) = "" Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
            ElseIf .TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To .cols - 1
                .TextMatrix(row, col) = .TextMatrix(row + 1, col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    

    End With
End Sub

Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub




Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    simpan
    If posting_konsijual(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdPrint_Click()
On Error GoTo err
Dim conn As New ADODB.Connection
    If lblNoTrans = "-" Then
        MsgBox "Pilih nomer pengeluaran terlebih dahulu"
        Exit Sub
    End If
    
    With CRPrint
        .reset
        .ReportFileName = reportDir & "\konsijual.rpt"
        
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
            LogonInfo = .LogonInfo(i)
        Next
        .ParameterFields(0) = "namaperusahaan;" & var_namaPerusahaan & ";True"
        .ParameterFields(1) = "alamat;" & var_alamtPerusahaan & ";True"
        
         .PrinterSelect
        
        .Destination = crptToWindow
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        Me.Hide
        .action = 1
        
    End With
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang & " where kode_bahan in (select kode_bahan from stock_toko where kode_toko='" & txtKodeToko & "' and stock>0)"
    frmSearch.nmform = "frmtranskonsinyasitambah"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = Searchkonsijual
    
    frmSearch.nmform = "frmtranskonsinyasitambah"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "so"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKuli_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmtranskonsinyasitambah"
        .nmctrl = "txtkuli"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_kuli"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        .query = searchKaryawan
        .nmform = "frmtranskonsinyasitambah"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchso_Click()
    With frmSearch
    .connstr = strcon
    .query = "select nomer_so,nomer_requestSO,tanggal_so,tanggal_kirim,status from T_soh"
    .nmform = "frmtranskonsinyasitambah"
    .nmctrl = "txtNoOrder"
    .nmctrl2 = ""
    .keyIni = "so"
    .col = 0
    .Index = -1
    .proc = "cek_noso"
    .loadgrid .query
    .cmbSort.ListIndex = 1
    .requery
    Set .frm = Me
    .Show vbModal
    End With
End Sub


Private Sub cmdSearchSerial_Click()
    frmSearch.query = "select nomer_serial,stock from stock_toko where kode_bahan='" & txtKdBrg & "' and kode_toko='" & txtKodeToko & "'"
    frmSearch.nmform = "frmtranskonsinyasitambah"
    frmSearch.nmctrl = "txtnomerserial"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "serial"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    
    frmSearch.loadgrid frmSearch.query

    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchSupplier_Click()

End Sub

Private Sub cmdSearchToko_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = "select * from ms_customer"
    
    frmSearch.nmform = "frmtranskonsinyasitambah"
    frmSearch.nmctrl = "txtkodetoko"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "so"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = ""

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
End If
End Sub
Private Function simpan() As Boolean

Dim i As Integer
Dim id As String
Dim row As Integer
Dim rs As New ADODB.Recordset
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dikirim terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
   id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_konsijualh", "nomer_konsijual", DTPicker1, "KJ")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
'    deletekartustock "Surat Jalan", id, conn
'    rs.Open "select d.nomer_suratjalan,kode_bahan,nomer_serial,qty,berat,kode_gudang from t_suratjalan_timbang d inner join t_suratjalanh h on d.nomer_suratjalan=h.nomer_suratjalan where d.nomer_suratjalan='" & id & "'", conn, adOpenStatic, adLockPessimistic
'    While Not rs.EOF
'        updatestock id, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!berat, 0, conn
'        rs.MoveNext
'    Wend
'    rs.Close
    conn.Execute "delete from t_konsijualh where nomer_konsijual='" & id & "'"
    conn.Execute "delete from t_konsijuald where nomer_konsijual='" & id & "'"
    
    'conn.Execute "delete from t_suratjalan_komposisi where nomer_suratjalan='" & id & "'"
    
    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    conn.CommitTrans
    i = 0
    simpan = True
    conn.Close
    DropConnection
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
   
    txtKodeToko.text = ""
    txtKeterangan.text = ""

    status_posting = False
    totalQty = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblBerat = 0
    lblItem = 0
    DTPicker1 = Now
    
    cmdClear_Click
    
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    cmdPrint.Enabled = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxgridRekap.Rows = 1
    flxgridRekap.Rows = 2
    
    txtKdBrg.SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(10)
    ReDim nilai(10)
    table_name = "t_konsijualh"
    fields(0) = "nomer_konsijual"
    fields(1) = "tanggal_konsijual"
'    fields(2) = "nomer_so"
    fields(2) = "keterangan"
    fields(3) = "userid"
    fields(4) = "status_posting"
    fields(5) = "kode_toko"
    fields(6) = "total"
    fields(7) = "tax"
    fields(8) = "cara_bayar"
    fields(9) = "tanggal_jatuhtempo"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
'    nilai(2) = txtNoOrder.text
    nilai(2) = txtKeterangan.text
    nilai(3) = User
    nilai(4) = 0
    nilai(5) = txtKodeToko
    nilai(6) = total
    nilai(7) = txtPPN
    If OptLunas(0).value = True Then
        nilai(8) = "T"
    Else
        nilai(8) = "K"
    End If
    nilai(9) = Format(DTPicker2, "yyyy/MM/dd")

    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(8)
    ReDim nilai(8)

    table_name = "t_konsijuald"
    fields(0) = "nomer_konsijual"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "nomer_serial"
    fields(5) = "NO_URUT"
    fields(6) = "hpp"
    fields(7) = "harga"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 2)
    nilai(2) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(row, 6), "###0.##"), ",", ".")
    nilai(4) = flxGrid.TextMatrix(row, 4)
    nilai(5) = row
    nilai(6) = GetHPPKertas2(flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3), txtKodeToko, conn)
    nilai(7) = flxGrid.TextMatrix(row, 7)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_konsijualh where nomer_konsijual='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal_konsijual")
        txtKeterangan.text = rs("keterangan")
        txtKodeToko = rs!kode_toko
        
        If rs!status_posting = 0 Then
            cmdPosting.Visible = True
        Else
            cmdSimpan.Enabled = False
        End If
       
        totalQty = 0
        totalberat = 0
        total = 0
        With flxgridRekap
        .Rows = 1
        .Rows = 2
        
        End With
        
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select '',d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat,harga from t_konsijuald d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_konsijual='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                txtKdBrg.text = rs(1)
                cek_kodebarang1
                txtNomerSerial = rs(3)
                txtQty = rs(4)
                
                txtBerat = rs(5)
                txtHarga = rs(6)
                cmdOk_Click
                'flxGrid.TextMatrix(row, 7) = Format(rs(4), "#,##0")
'                flxGrid.TextMatrix(row, 8) = Format(rs(3) * rs(4), "#,##0")
'                row = row + 1
'                totalQty = totalQty + rs(4)
'                totalBerat = totalBerat + rs(5)
'                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = totalQty
        lblBerat = totalberat
        
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_noso()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_soh where nomer_so='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        txtKdBrg.Clear
        rs.Open "select distinct(d.[kode_bahan]) from t_sod d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_so='" & txtNoOrder & "' order by d.kode_bahan", conn
        While Not rs.EOF
            txtKdBrg.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
                
            End If
            txtNomerSerial = flxGrid.TextMatrix(flxGrid.row, 3)
            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 7)
            txtBerat.SetFocus
        End If

End Sub

Private Sub Command1_Click()

End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub


Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click

    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    
    
    
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    seltab = 0
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    With flxGrid
    .ColWidth(0) = 300
    .ColWidth(1) = 0
    .ColWidth(2) = 1400
    .ColWidth(3) = 3000
    .ColWidth(4) = 1200
    .ColWidth(5) = 900
    .ColWidth(6) = 900
    .ColWidth(7) = 1200
    .ColWidth(8) = 0
    
    
    .TextMatrix(0, 1) = "No. Order"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .ColAlignment(3) = 1
    .ColAlignment(4) = 1
    .TextMatrix(0, 2) = "Kode Bahan"
    .TextMatrix(0, 3) = "Nama"
    .TextMatrix(0, 4) = "Serial"
    .TextMatrix(0, 5) = "Qty"
    .TextMatrix(0, 6) = "Berat"
    .TextMatrix(0, 7) = "Harga"
    .TextMatrix(0, 8) = "Total"
    End With
    With flxgridRekap
    .ColWidth(0) = 300
    .ColWidth(1) = 1400
    .ColWidth(2) = 3000
    .ColWidth(3) = 900
    .ColWidth(4) = 900
        
    .TextMatrix(0, 1) = "Kode Bahan"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .TextMatrix(0, 2) = "Nama"
    .TextMatrix(0, 3) = "Qty"
    .TextMatrix(0, 4) = "Berat"
    End With
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        cek_kodebarang1 = True
    Else
        LblNamaBarang1 = ""

        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_pengawas()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close

err:
End Sub
Public Sub cek_kuli()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtKuli & "'", conn
    If Not rs.EOF Then
        lblNamaKuli = rs(1)
    Else
        lblNamaKuli = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub

Private Sub hitung_ulang()
'On Error GoTo err
'    conn.Open strcon
'    With flxGrid
'    For i = 1 To .Rows - 2
'        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
'        If Not rs.EOF Then
'            total = total - .TextMatrix(i, 8)
'            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
'            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
'            total = total + .TextMatrix(i, 8)
'        End If
'        rs.Close
'    Next
'    End With
'    conn.Close
'    lblTotal = Format(total, "#,##0")
'    Exit Sub
'err:
'    MsgBox err.Description
'    If conn.State Then conn.Close
End Sub

Private Sub listKuli_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyDelete Then listKuli.RemoveItem (listKuli.ListIndex)
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyDelete Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub



Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
End Sub


Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub




Private Sub txtKodeToko_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchToko_Click
End Sub

Private Sub txtKuli_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchKuli_Click
    If KeyCode = 13 Then
        cek_kuli
        listKuli.AddItem txtKuli & "-" & lblNamaKuli
        txtKuli = ""
        lblNamaKuli = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtNoOrder_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchso_Click
End Sub

Private Sub txtNoOrder_LostFocus()
    cek_noso
End Sub

Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPengawas_Click
    If KeyCode = 13 Then
        cek_pengawas
        listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
        txtPengawas = ""
        lblNamaPengawas = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub


