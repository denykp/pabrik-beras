VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAddPindahBG 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pindah Giro"
   ClientHeight    =   6420
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12405
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6420
   ScaleWidth      =   12405
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4440
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   12210
      Begin SSDataWidgets_B.SSDBGrid DBGrid 
         Height          =   4050
         Left            =   75
         TabIndex        =   0
         Top             =   285
         Width           =   12045
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         MultiLine       =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   14671839
         RowHeight       =   450
         ExtraHeight     =   212
         Columns.Count   =   8
         Columns(0).Width=   1376
         Columns(0).Caption=   "Pindah"
         Columns(0).Name =   "Pindah"
         Columns(0).Alignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   2699
         Columns(1).Caption=   "Nomer BG"
         Columns(1).Name =   "Nomer BG"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   2566
         Columns(2).Caption=   "Tanggal Cair"
         Columns(2).Name =   "Tanggal Cair"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   2514
         Columns(3).Caption=   "Nominal"
         Columns(3).Name =   "Nominal"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "#,##0"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3942
         Columns(4).Caption=   "Supplier"
         Columns(4).Name =   "Supplier"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2805
         Columns(5).Caption=   "Bank"
         Columns(5).Name =   "Bank"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "nomer transaksi"
         Columns(6).Name =   "nomer transaksi"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   4022
         Columns(7).Caption=   "kode_bank"
         Columns(7).Name =   "kode_bank"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Style=   3
         _ExtentX        =   21246
         _ExtentY        =   7144
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblID 
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   240
         Left            =   270
         TabIndex        =   5
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label lblSatuan 
         Height          =   330
         Left            =   2520
         TabIndex        =   4
         Top             =   675
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3585
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6255
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5550
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4875
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5550
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   7095
      TabIndex        =   7
      Top             =   90
      Visible         =   0   'False
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   156631043
      CurrentDate     =   38927
   End
   Begin VB.Label Label2 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   300
      TabIndex        =   10
      Top             =   165
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1695
      TabIndex        =   9
      Top             =   90
      Visible         =   0   'False
      Width           =   1830
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5385
      TabIndex        =   8
      Top             =   132
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPindahBG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public JenisPindah As String


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Public Sub TampilBG()
    conn.ConnectionString = strcon
    conn.Open
    
    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    If JenisPindah = "ke_bank" Then
        rs.Open "select case when CONVERT(VARCHAR(10), l.tanggal_cair, 111)<='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' then '0' else '0' end as status,l.nomer_bg,l.tanggal_cair,l.nominal,s.nama_customer,keterangan_bank,l.nomer_transaksi,l.kode_bank " & _
                "from list_bg l " & _
                "inner join ms_customer s on s.kode_customer=l.kode_customer " & _
                "where l.jenis='p' and l.status_cair='0' and lokasi='i'  " & _
                "union " & _
                "select case when CONVERT(VARCHAR(10), l.tanggal_cair, 111)<='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' then '0' else '0' end as status,l.nomer_bg,l.tanggal_cair,l.nominal,s.nama_supplier,keterangan_bank,l.nomer_transaksi,l.kode_bank " & _
                "from list_bg l " & _
                "inner join ms_supplier s on s.kode_supplier=l.kode_supplier " & _
                "where l.jenis='u' and l.status_cair='0' and lokasi='i' order by tanggal_cair", conn
    Else
        rs.Open "select case when CONVERT(VARCHAR(10), l.tanggal_cair, 111)<='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' then '0' else '0' end as status,l.nomer_bg,l.tanggal_cair,l.nominal,s.nama_customer,l.keterangan_bank,l.nomer_transaksi,l.kode_bank " & _
                "from list_bg l " & _
                "inner join ms_customer s on s.kode_customer=l.kode_customer " & _
                "inner join ms_bank b on b.kode_bank=l.kode_bank " & _
                "where l.jenis='p' and l.status_cair='0' and lokasi='o' " & _
                "union " & _
                "select case when CONVERT(VARCHAR(10), l.tanggal_cair, 111)<='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' then '0' else '0' end as status,l.nomer_bg,l.tanggal_cair,l.nominal,s.nama_supplier,l.keterangan_bank,l.nomer_transaksi,l.kode_bank " & _
                "from list_bg l " & _
                "inner join ms_supplier s on s.kode_supplier=l.kode_supplier " & _
                "inner join ms_bank b on b.kode_bank=l.kode_bank " & _
                "where l.jenis='u' and l.status_cair='0' and lokasi='i' order by tanggal_cair", conn

    End If
    While Not rs.EOF
        DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(5) & "#" & rs(6) & "#" & rs(7)
        rs.MoveNext
    Wend
    rs.Close

    conn.Close
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim counter As Integer
Dim Acc_Bank As String, Acc_PiutangGiroDiTangan As String, Acc_PiutangGiroDiBank As String
Dim tanggal As Date, Nominal As Double, NoJurnal As String
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    tanggal = DTPicker1.value
    NoJurnal = Nomor_Jurnal(tanggal)
    Acc_PiutangGiroDiTangan = getAcc("piutang giro di tangan")
    Acc_PiutangGiroDiBank = getAcc("piutang giro di bank")
    
    counter = 0
    DBGrid.MoveFirst
    Nominal = 0
    While counter < DBGrid.Rows

            If DBGrid.Columns(0).value = True Then
                If JenisPindah = "ke_bank" Then
                    conn.Execute "update list_bg set lokasi='o',kode_bank='" & DBGrid.Columns(7).text & "'" & _
                                 "where nomer_transaksi='" & DBGrid.Columns(6).text & "' " & _
                                 "and nomer_bg='" & DBGrid.Columns(1).text & "' "
                Else
                    conn.Execute "update list_bg set lokasi='i' " & _
                                 "where nomer_transaksi='" & DBGrid.Columns(6).text & "' " & _
                                 "and nomer_bg='" & DBGrid.Columns(1).text & "' "
                End If
                Nominal = Nominal + CDbl(DBGrid.Columns(3).text)
            End If
        
        DBGrid.MoveNext
        counter = counter + 1
    Wend
    NoJurnal = newid("t_jurnalh", "no_transaksi", DTPicker1, "GL")
    If Nominal > 0 Then
        If JenisPindah = "ke_bank" Then
            JurnalD NoJurnal, NoJurnal, Acc_PiutangGiroDiBank, "d", Nominal, "Pemindahan giro ke bank", DBGrid.Columns(1).text
            JurnalD NoJurnal, NoJurnal, Acc_PiutangGiroDiTangan, "k", Nominal, "Pemindahan giro ke bank", DBGrid.Columns(1).text
            JurnalH NoJurnal, NoJurnal, tanggal, "Pindah Giro ke Bank"
        Else
            JurnalD NoJurnal, NoJurnal, Acc_PiutangGiroDiTangan, "d", Nominal, "Pemindahan giro dari bank", DBGrid.Columns(1).text
            JurnalD NoJurnal, NoJurnal, Acc_PiutangGiroDiBank, "k", Nominal, "Pemindahan giro dari bank", DBGrid.Columns(1).text
            JurnalH NoJurnal, NoJurnal, tanggal, "Pindah Giro dari Bank"
        End If
    End If

    conn.CommitTrans
    conn.Close
    
    MsgBox "Data sudah disimpan ! ", vbOKOnly
    
    TampilBG


    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub Form_Load()
    conn.Open strcon
    
    rs.Open "select * from ms_bank", conn
    While Not rs.EOF
        DBGrid.Columns(7).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    
    DTPicker1 = Now
    TampilBG

End Sub
