VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Purchase Order"
   ClientHeight    =   9195
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10710
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9195
   ScaleWidth      =   10710
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4185
      Picture         =   "frmAddPO.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   59
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5280
      Picture         =   "frmAddPO.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   58
      Top             =   90
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdSearchRequest 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3690
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   52
      Top             =   540
      Width           =   420
   End
   Begin VB.TextBox txtNoRequest 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   540
      Width           =   2025
   End
   Begin VB.TextBox txtUangMuka 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   8145
      TabIndex        =   11
      Top             =   8640
      Width           =   2175
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5925
      Index           =   0
      Left            =   180
      TabIndex        =   27
      Top             =   2160
      Width           =   10260
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2025
         Left            =   90
         TabIndex        =   40
         Top             =   2250
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   3572
         _Version        =   393216
         Cols            =   9
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   90
         TabIndex        =   28
         Top             =   45
         Width           =   9915
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   6
            Top             =   960
            Width           =   1140
         End
         Begin VB.PictureBox Picture2 
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   3015
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   30
            Top             =   135
            Width           =   465
            Begin VB.CommandButton cmdSearchBrg 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmAddPO.frx":0A64
               TabIndex        =   31
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   90
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   29
            Top             =   1620
            Width           =   3795
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   10
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   9
               Top             =   84
               Width           =   1050
            End
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   8
               Top             =   84
               Width           =   1350
            End
         End
         Begin VB.TextBox txtHarga 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   7
            Top             =   1320
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   5
            Top             =   585
            Width           =   1140
         End
         Begin VB.TextBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            TabIndex        =   4
            Top             =   180
            Width           =   1500
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2730
            TabIndex        =   49
            Top             =   1020
            Width           =   600
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            Caption         =   "Berat / Jumlah"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   48
            Top             =   990
            Width           =   1245
         End
         Begin VB.Label lblNamaBarang2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2775
            TabIndex        =   45
            Top             =   615
            Visible         =   0   'False
            Width           =   3480
         End
         Begin VB.Label Label8 
            BackColor       =   &H8000000C&
            Caption         =   "Harga"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   39
            Top             =   1365
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblDefQtyJual 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   285
            Left            =   270
            TabIndex        =   38
            Top             =   1305
            Width           =   870
         End
         Begin VB.Label lblID 
            Caption         =   "Label12"
            ForeColor       =   &H8000000F&
            Height          =   240
            Left            =   270
            TabIndex        =   37
            Top             =   1305
            Visible         =   0   'False
            Width           =   870
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   36
            Top             =   225
            Width           =   1050
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            Caption         =   "Jml Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   35
            Top             =   630
            Width           =   1125
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3510
            TabIndex        =   34
            Top             =   225
            Width           =   3480
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   4050
            TabIndex        =   33
            Top             =   1215
            Width           =   600
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2745
            TabIndex        =   32
            Top             =   585
            Width           =   1050
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxgridRekap 
         Height          =   1125
         Left            =   90
         TabIndex        =   55
         Top             =   4365
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   1984
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblBerat 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7470
         TabIndex        =   57
         Top             =   5490
         Width           =   1140
      End
      Begin VB.Label Label6 
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6210
         TabIndex        =   56
         Top             =   5490
         Width           =   1410
      End
      Begin VB.Label Label17 
         Caption         =   "Jml Kemasan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2475
         TabIndex        =   44
         Top             =   5490
         Width           =   1770
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4455
         TabIndex        =   43
         Top             =   5490
         Width           =   1140
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   42
         Top             =   5490
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   41
         Top             =   5490
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3060
      Picture         =   "frmAddPO.frx":0B66
      TabIndex        =   46
      Top             =   8415
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   270
      Picture         =   "frmAddPO.frx":0C68
      TabIndex        =   12
      Top             =   8415
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4455
      Picture         =   "frmAddPO.frx":0D6A
      TabIndex        =   14
      Top             =   8415
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   2
      Top             =   1695
      Width           =   7845
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3708
      Picture         =   "frmAddPO.frx":0E6C
      TabIndex        =   18
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8685
      TabIndex        =   17
      Top             =   165
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10170
      TabIndex        =   16
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3690
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   15
      Top             =   945
      Width           =   420
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   960
      Width           =   2025
   End
   Begin VB.CommandButton cmdBatal 
      Caption         =   "Batalkan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1665
      Picture         =   "frmAddPO.frx":0F6E
      TabIndex        =   13
      Top             =   8415
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   7020
      TabIndex        =   3
      Top             =   780
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   228261891
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   7020
      TabIndex        =   53
      Top             =   1170
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   228261891
      CurrentDate     =   38927
   End
   Begin VB.Label Label2 
      Caption         =   "Tanggal Kirim"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5535
      TabIndex        =   54
      Top             =   1215
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   "No. Request"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   51
      Top             =   585
      Width           =   1365
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Uang Muka"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6300
      TabIndex        =   50
      Top             =   8685
      Width           =   1575
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   150
      TabIndex        =   47
      Top             =   1335
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   26
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   25
      Top             =   1695
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5535
      TabIndex        =   24
      Top             =   825
      Width           =   1320
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   23
      Top             =   1005
      Width           =   960
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1605
      TabIndex        =   22
      Top             =   1365
      Width           =   3120
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8100
      TabIndex        =   21
      Top             =   8220
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7155
      TabIndex        =   20
      Top             =   8220
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   19
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean

Private Sub cmdBatal_Click()
Dim conn As New ADODB.Connection
    If MsgBox("Anda yakin hendak membatalkan pesanan ini?", vbYesNo) = vbYes Then
        conn.Open strcon
        conn.Execute "update t_poh set status='2' where nomer_po='" & lblNoTrans & "'"
        conn.Close
        MsgBox "Pesanan sudah dibatalkan"
        reset_form
    End If

End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    
    row = flxGrid.row
    totalQty = totalQty - (flxGrid.TextMatrix(row, 5))
    totalberat = totalberat - (flxGrid.TextMatrix(row, 6))
    total = total - (flxGrid.TextMatrix(row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        lblTotal = Format(total, "#,##0")
        
    delete_rekap
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub
Private Sub delete_rekap()
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(flxGrid.TextMatrix(flxGrid.row, 1)) Then row = i
    Next
    If row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(row, 3)) Then .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) - flxGrid.TextMatrix(flxGrid.row, 5)
    If IsNumeric(.TextMatrix(row, 4)) Then .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) - flxGrid.TextMatrix(flxGrid.row, 6)
    If .TextMatrix(row, 4) = 0 Or .TextMatrix(row, 3) = 0 Then
        For row = row To .Rows - 1
            If row = .Rows - 1 Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(row + 1, 1) = "" Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
            ElseIf .TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To .cols - 1
                .TextMatrix(row, col) = .TextMatrix(row + 1, col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    

    End With
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_po] from t_poh where [nomer_po]>'" & lblNoTrans & "' order by [nomer_po]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then

'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
'        Next
        If mode = 1 Then
            If row = 0 Then
                row = flxGrid.Rows - 1
                flxGrid.Rows = flxGrid.Rows + 1
            End If
        Else
            row = flxGrid.row
            delete_rekap
        End If
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang1
        flxGrid.TextMatrix(row, 3) = ""
        flxGrid.TextMatrix(row, 5) = txtQty
        If flxGrid.TextMatrix(row, 6) = "" Then
            flxGrid.TextMatrix(row, 6) = txtBerat
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 5))
            totalberat = totalberat - (flxGrid.TextMatrix(row, 6))
            total = total - (flxGrid.TextMatrix(row, 8))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 6) = CDbl(flxGrid.TextMatrix(row, 6)) + CDbl(txtBerat)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 6) = CDbl(txtBerat)
            End If
        End If
        
        flxGrid.TextMatrix(row, 7) = txtHarga.text
        flxGrid.TextMatrix(row, 8) = flxGrid.TextMatrix(row, 6) * flxGrid.TextMatrix(row, 7)
        flxGrid.TextMatrix(row, 8) = Format(flxGrid.TextMatrix(row, 8), "#,##0")
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        totalQty = totalQty + flxGrid.TextMatrix(row, 5)
        totalberat = totalberat + flxGrid.TextMatrix(row, 6)
        total = total + (flxGrid.TextMatrix(row, 8))
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        lblTotal = Format(total, "#,##0")
        lblItem = flxGrid.Rows - 2
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        add_rekap txtKdBrg, LblNamaBarang1, txtQty, txtBerat

        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        txtQty.text = "1"
        txtBerat.text = "1"
        
        txtHarga.text = "0"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
End Sub
Private Sub add_rekap(kodebarang As String, namabarang As String, qty As Double, berat As Double)
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(kodebarang) Then row = i
    Next
    If row = 0 Then
        row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.row, 0) = ""
    .row = row


    .TextMatrix(row, 1) = kodebarang
    .TextMatrix(row, 2) = namabarang
    If .TextMatrix(row, 3) = "" Then .TextMatrix(row, 3) = qty Else .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) + qty
    If .TextMatrix(row, 4) = "" Then .TextMatrix(row, 4) = berat Else .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) + berat
    lblItem = .Rows - 2
    End With
End Sub

Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_po] from t_poh where [nomer_po]<'" & lblNoTrans & "' order by [nomer_po] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchPO
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "POH"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchRequest_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchrequestPO
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "txtNoRequest"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "RequestPO"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_request"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchSupplier_Click()
'    If User <> "sugik" Then
'        SearchSupplier = "select kode_supplier,nama_supplier,alamat from ms_supplier"
'    End If
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If txtKodeSupplier.text = "" Then
        MsgBox "Silahkan masukkan Supplier terlebih dahulu"
        txtKodeSupplier.SetFocus
        Exit Function
    End If
 
    
    
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_POh", "nomer_PO", DTPicker1, "PO")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_POh where nomer_PO='" & id & "'"
    conn.Execute "delete from t_POd where nomer_PO='" & id & "'"
    
    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtUangMuka.text = "0"
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    txtNoRequest.text = ""
    status_posting = False
    totalQty = 0
    totalberat = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = Now
    cmdClear_Click
    
    cmdSimpan.Enabled = True
    cmdBatal.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxgridRekap.Rows = 1
    flxgridRekap.Rows = 2
    
    TabStrip1.Tabs(1).Selected = True
    cmdNext.Enabled = False
    cmdNext.Enabled = False
    
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(10)
    ReDim nilai(10)
    table_name = "t_POh"
    fields(0) = "nomer_PO"
    fields(1) = "tanggal_PO"
    fields(2) = "nomer_requestPO"
    
    fields(3) = "kode_supplier"
    fields(4) = "tanggal_kirim"
    fields(5) = "total"
    fields(6) = "keterangan"
    fields(7) = "userid"
    fields(8) = "status"
    fields(9) = "uangmuka"
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtNoRequest
    nilai(3) = txtKodeSupplier.text
    nilai(4) = Format(DTPicker2, "yyyy/MM/dd HH:mm:ss")
    nilai(5) = Replace(Format(lblTotal, "###0.##"), ",", ".")
    nilai(6) = txtKeterangan.text
    nilai(7) = User
    nilai(8) = "0"
    nilai(9) = txtUangMuka.text
    
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_POd"
    fields(0) = "nomer_PO"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "NO_URUT"
    fields(5) = "HARGA"
    

    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(row, 6), "###0.##"), ",", ".")
    nilai(4) = row
    nilai(5) = Replace(Format(flxGrid.TextMatrix(row, 7), "###0.##"), ",", ".")
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cmdBatal.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_POh where nomer_PO='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        txtNoRequest = rs!nomer_requestPO
        DTPicker1 = rs("tanggal_PO")
        DTPicker2 = rs("tanggal_kirim")
        
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        txtUangMuka.text = rs!UangMuka
        totalQty = 0
        totalberat = 0
        total = 0
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
'        cmdPrint.Visible = True
'        status_posting = rs!status_posting
        If rs("status") = 0 Then
            cmdBatal.Visible = True
        Else
            cmdBatal.Visible = False
        End If
        If rs.State Then rs.Close
        rs.Open "select * from vw_outstanding_so where nomer_so='" & lblNoTrans & "'", conn
        If rs.EOF Then
           cmdBatal.Visible = False
        End If
        rs.Close
        
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        flxgridRekap.Rows = 1
        flxgridRekap.Rows = 2
        row = 1
        
        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat,d.harga from t_POd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_PO='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = ""
                flxGrid.TextMatrix(row, 5) = rs(2)
                flxGrid.TextMatrix(row, 6) = rs(3)
                flxGrid.TextMatrix(row, 7) = Format(rs(4), "#,##0")
                flxGrid.TextMatrix(row, 8) = Format(rs(3) * rs(4), "#,##0")
                add_rekap rs(0), rs(1), rs(2), rs(3)
                row = row + 1
                totalQty = totalQty + rs(2)
                totalberat = totalberat + rs(3)
                total = total + (rs(3) * rs(4))
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = Format(totalQty, "#,##0.00")
        lblBerat = Format(totalberat, "#,##0.00")
        lblTotal = Format(total, "#,##0.00")
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Public Sub cek_request()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cmdBatal.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    totalQty = 0
    totalberat = 0
    total = 0
    row = 1
    flxGrid.Rows = 2
        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat from t_requestPOd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_requestPO='" & txtNoRequest & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = ""
                
                flxGrid.TextMatrix(row, 5) = rs(2)
                flxGrid.TextMatrix(row, 6) = rs(3)
                flxGrid.TextMatrix(row, 7) = 0 'Format(rs(4), "#,##0")
                flxGrid.TextMatrix(row, 8) = 0 'Format(rs(3) * rs(4), "#,##0")
                add_rekap rs(0), rs(1), rs(2), rs(3)
                row = row + 1
                totalQty = totalQty + rs(2)
                totalberat = totalberat + rs(3)
'                total = total + (rs(3) * rs(4))
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        
        lblQty = Format(totalQty, "#,##0.00")
        lblBerat = Format(totalberat, "#,##0.00")
        'lblTotal = Format(total, "#,##0.00")
        lblItem = flxGrid.Rows - 2
   
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
                lblNamaBarang2 = flxGrid.TextMatrix(flxGrid.row, 3)
                End If
            
            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 7)
            txtBerat.SetFocus
        End If

End Sub

Private Sub Command1_Click()

End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
        
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub


Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 0
    flxGrid.ColWidth(4) = 0
    flxGrid.ColWidth(5) = 900
    flxGrid.ColWidth(6) = 900
    flxGrid.ColWidth(7) = 900
    flxGrid.ColWidth(8) = 1500
    
    
'    If right_hpp = True Then
        
        txtHarga.Visible = True
        Label8.Visible = True
        lblTotal.Visible = True
        Label10.Visible = True
'    End If

    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(3) = 1
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Panjang"
    flxGrid.TextMatrix(0, 4) = ""
    flxGrid.TextMatrix(0, 5) = "Qty"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "Harga"
    flxGrid.TextMatrix(0, 8) = "Total"
    With flxgridRekap
        .ColWidth(0) = 300
        .ColWidth(1) = 1400
        .ColWidth(2) = 3000
        .ColWidth(3) = 900
        .ColWidth(4) = 900
            
        .TextMatrix(0, 1) = "Kode Bahan"
        .ColAlignment(2) = 1 'vbAlignLeft
        .ColAlignment(1) = 1
        .TextMatrix(0, 2) = "Nama"
        .TextMatrix(0, 3) = "Jml Kemasan"
        .TextMatrix(0, 4) = "Berat"
    End With
End Sub

Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan

        lblSatuan = ""
        If lblKategori <> "" Then
        txtHarga.text = IIf(IsNull(rs("harga_beli" & lblKategori)), 0, rs("harga_beli" & lblKategori))
        End If
        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""
        lblNamaBarang2 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier] from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
        
        
    Else
        lblNamaSupplier = ""
        
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()
'On Error GoTo err
'    conn.Open strcon
'    With flxGrid
'    For i = 1 To .Rows - 2
'        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
'        If Not rs.EOF Then
'            total = total - .TextMatrix(i, 8)
'            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
'            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
'            total = total + .TextMatrix(i, 8)
'        End If
'        rs.Close
'    Next
'    End With
'    conn.Close
'    lblTotal = Format(total, "#,##0")
'    Exit Sub
'err:
'    MsgBox err.Description
'    If conn.State Then conn.Close
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
End Sub


Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub


Private Sub txtNoRequest_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchRequest_Click
End Sub

Private Sub txtNoRequest_LostFocus()
    cek_request
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub


