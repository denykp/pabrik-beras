VERSION 5.00
Begin VB.Form frmMasterSubKategori 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sub Kategori"
   ClientHeight    =   3000
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6255
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   6255
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   1710
      TabIndex        =   15
      Text            =   "cmbField"
      Top             =   180
      Width           =   3450
   End
   Begin VB.TextBox txtAccHPP 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1710
      MaxLength       =   25
      TabIndex        =   13
      Top             =   1440
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearchHPP 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3735
      TabIndex        =   12
      Top             =   1440
      Width           =   465
   End
   Begin VB.CommandButton cmdSearchAccPersediaan 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3735
      TabIndex        =   10
      Top             =   1035
      Width           =   465
   End
   Begin VB.TextBox txtAccPersediaan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1710
      MaxLength       =   25
      TabIndex        =   9
      Top             =   1035
      Width           =   1950
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3555
      Picture         =   "frmMasterSubKategori.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2280
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1935
      Picture         =   "frmMasterSubKategori.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2280
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4725
      Picture         =   "frmMasterSubKategori.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   585
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   585
      Width           =   2850
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   450
      Picture         =   "frmMasterSubKategori.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2280
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   17
      Top             =   240
      Width           =   150
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   210
      Width           =   810
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc HPP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1485
      Width           =   690
   End
   Begin VB.Label Label20 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Persediaan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   11
      Top             =   1080
      Width           =   1305
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   8
      Top             =   630
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   7
      Top             =   1920
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   5
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   4
      Top             =   615
      Width           =   1320
   End
   Begin VB.Label lblID 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1755
      TabIndex        =   18
      Top             =   585
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterSubKategori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbField_Click(Index As Integer)
    Dim rs As New ADODB.Recordset
    Dim conn As New ADODB.Connection
    conn.Open strcon
    rs.Open "select * from var_kategori where kategori='" & cmbField(0).text & "'", conn
    If Not rs.EOF Then
        txtAccPersediaan = rs!Acc_Persediaan
        txtAccHPP = rs!acc_hpp
    End If
    rs.Close
    conn.Close
End Sub

Private Sub cmbField_LostFocus(Index As Integer)
Dim Data As String
    Data = cmbField(0).text
    For i = 0 To cmbField(0).ListCount - 1
        If LCase(Data) = LCase(cmbField(0).List(i)) Then Exit Sub
    Next
    MsgBox "Kategori tersebut tidak ada"
    cmbField(0).SetFocus
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where subkategori='" & txtField(0).text & "' and kategori='" & cmbField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        DropConnection
        Exit Sub
    End If
    rs.Close
    
    
    conn.BeginTrans
    conn.Execute "delete from var_subkategori where kategori='" & cmbField(0).text & "' and subkategori='" & txtField(0).text & "'"
    conn.CommitTrans
    
    MsgBox "Data sudah dihapus"
    
    DropConnection
    
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = "select subkategori,kategori from var_subkategori" ' order by kategori"
    frmSearch.nmform = "frmMasterkategori"
    frmSearch.nmctrl = "txtfield"
    frmSearch.nmctrl2 = "cmbfield"
    frmSearch.keyIni = "kategori"

    frmSearch.Col = 0
    frmSearch.col2 = 1
    frmSearch.Index = 0
    frmSearch.Index2 = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
   
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSearchAccPersediaan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='110.040'"
    frmSearch.nmform = "frmMasterKategori"
    frmSearch.nmctrl = "txtAccPersediaan"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal

End Sub


Private Sub cmdSearchHPP_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and nama like '%HPP%' or nama like '%Harga Pokok%' "
    frmSearch.nmform = "frmMasterKategori"
    frmSearch.nmctrl = "txtAccHPP"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal

End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim conn As New ADODB.Connection
On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        txtField(0).SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    
    
    conn.BeginTrans
    conn.Execute "if exists(select * from var_subkategori where kategori='" & cmbField(0).text & "' and subkategori='" & txtField(0).text & "') delete from var_subkategori where  kategori='" & cmbField(0).text & "' and subkategori='" & txtField(0).text & "'"
    conn.Execute "insert into var_subkategori values ('" & cmbField(0).text & "','" & txtField(0).text & "','" & txtAccPersediaan & "','" & txtAccHPP & "')"
    If MsgBox("Hendak update Setting Acc semua barang Kategori " & cmbField(0).text & " subkategori " & txtField(0).text & "?", vbYesNo) = vbYes Then
    conn.Execute "update setting_accbahan set acc_persediaan='" & txtAccPersediaan & "',acc_hpp='" & txtAccHPP & "' from ms_bahan m inner join setting_accbahan s on m.kode_bahan=s.kode_bahan where m.kategori='" & txtField(0) & "'  and m.subkategori='" & txtField(0).text & "'"
    End If
    conn.CommitTrans
    
    MsgBox "Data sudah Tersimpan"
    DropConnection
    If frmMasterBarang.Visible = True Then
        Unload Me
        Exit Sub
    End If
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
    txtField(0).text = ""
    txtField(0).SetFocus
End Sub
Public Sub cari_data()
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from var_subkategori where kategori='" & cmbField(0).text & "' and subkategori='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        SetComboText rs!kategori, cmbField(0)
        txtAccPersediaan = rs!Acc_Persediaan
        txtAccHPP = rs!acc_hpp
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbField(0).Clear
    rs.Open "select kategori from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(0).AddItem rs(0)
        rs.MoveNext
    Wend
    conn.Close
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    load_combo
    txtAccPersediaan = var_accpersediaan
    txtAccHPP = var_acchpp
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub



