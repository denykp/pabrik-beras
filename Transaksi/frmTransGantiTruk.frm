VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTransGantiTruk 
   Caption         =   "Penggantian Truk"
   ClientHeight    =   7530
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7965
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7530
   ScaleWidth      =   7965
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   1710
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   5760
      Width           =   5220
   End
   Begin VB.Frame frAngkut 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   420
      Left            =   180
      TabIndex        =   18
      Top             =   3915
      Visible         =   0   'False
      Width           =   7665
      Begin VB.CommandButton cmdSearchEkspedisi 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3555
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   19
         Top             =   0
         Width           =   420
      End
      Begin VB.TextBox txtKodeEkspedisi 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   2
         Top             =   0
         Width           =   2025
      End
      Begin VB.Label Label19 
         Caption         =   "Ekspedisi"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   21
         Top             =   60
         Width           =   960
      End
      Begin VB.Label lblNamaEkspedisi 
         BackStyle       =   0  'Transparent
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4050
         TabIndex        =   20
         Top             =   0
         Width           =   3120
      End
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3150
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   6840
      Width           =   1230
   End
   Begin VB.TextBox txtSupirSekarang 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1620
      TabIndex        =   14
      Top             =   2295
      Width           =   5220
   End
   Begin VB.ListBox listTrukSekarang 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   1620
      TabIndex        =   13
      Top             =   1440
      Width           =   3660
   End
   Begin VB.TextBox txtNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1620
      TabIndex        =   0
      Top             =   585
      Width           =   1875
   End
   Begin VB.CommandButton cmdSearchPO 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3555
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   10
      Top             =   540
      Width           =   420
   End
   Begin VB.TextBox txtSupir 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1710
      TabIndex        =   4
      Top             =   5355
      Width           =   5220
   End
   Begin VB.ListBox listTruk 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   3465
      TabIndex        =   7
      Top             =   4410
      Width           =   3660
   End
   Begin VB.TextBox txtNoTruk 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1665
      TabIndex        =   3
      Top             =   4410
      Width           =   1755
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1665
      TabIndex        =   1
      Top             =   3420
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   170852355
      CurrentDate     =   38927
   End
   Begin VB.Label Label6 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   360
      TabIndex        =   24
      Top             =   5850
      Width           =   960
   End
   Begin VB.Label lblEkspedisi 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   23
      Top             =   1035
      Width           =   2625
   End
   Begin VB.Label Label4 
      Caption         =   "Ekspedisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   22
      Top             =   1035
      Width           =   960
   End
   Begin VB.Line Line1 
      X1              =   315
      X2              =   7740
      Y1              =   3195
      Y2              =   3195
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Pengganti :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   17
      Top             =   2790
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Supir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   16
      Top             =   2340
      Width           =   960
   End
   Begin VB.Label Label1 
      Caption         =   "No. Truk"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   15
      Top             =   1485
      Width           =   960
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   360
      TabIndex        =   12
      Top             =   3465
      Width           =   1140
   End
   Begin VB.Label Label7 
      Caption         =   "No.SuratJalan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   315
      TabIndex        =   11
      Top             =   630
      Width           =   1275
   End
   Begin VB.Label Label9 
      Caption         =   "Supir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   360
      TabIndex        =   9
      Top             =   5445
      Width           =   960
   End
   Begin VB.Label Label5 
      Caption         =   "No. Truk"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   360
      TabIndex        =   8
      Top             =   4455
      Width           =   960
   End
End
Attribute VB_Name = "frmTransGantiTruk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSearchPO_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = Searchsuratjalan
    
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtnoorder"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "suratjalan"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select top 1 a.*,m.nama_ekspedisi from t_suratjalan_angkut a left join ms_ekspedisi m on a.kode_ekspedisi=m.kode_ekspedisi where nomer_suratjalan='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        listTrukSekarang.Clear
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            listTrukSekarang.AddItem Replace(Replace(notruk(A), "[", ""), "]", "")
        Next
        txtSupirSekarang = rs!supir
        lblEkspedisi = rs!kode_ekspedisi & " - " & rs!nama_ekspedisi
    End If
    rs.Close
    
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
Dim no_urut As Integer
    If MsgBox("Anda yakin hendak menyimpan?", vbYesNo) = vbNo Then Exit Sub
    conn.Open
    rs.Open "select top 1 no_urut from t_suratjalan_angkut where nomer_suratjalan='" & txtNoOrder & "' order by no_urut desc", conn
    no_urut = rs(0) + 1
    conn.Execute "update t_suratjalan_angkut set status=0  where nomer_suratjalan='" & txtNoOrder & "'"
    add_angkut no_urut
    MsgBox "Simpan berhasil"
    conn.Close
End Sub
Private Sub add_angkut(no_urut As Integer)
Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(9)
    ReDim nilai(9)
    table_name = "t_suratjalan_angkut"
    fields(0) = "nomer_suratjalan"
    fields(1) = "tanggal"
    fields(2) = "no_truk"
    fields(3) = "supir"
    fields(4) = "kode_ekspedisi"
    fields(5) = "fg_bayarangkut"
    fields(6) = "no_urut"
    fields(7) = "status"
    fields(8) = "keterangan"
    Dim notruk As String
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    nilai(0) = txtNoOrder
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = notruk
    nilai(3) = txtSupir
    nilai(4) = txtKodeEkspedisi
    nilai(5) = 0
    nilai(6) = no_urut
    nilai(7) = 1
    nilai(8) = txtKeterangan
    tambah_data table_name, fields, nilai
End Sub
Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Private Sub txtNoOrder_LostFocus()
    cek_notrans
End Sub

Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub
