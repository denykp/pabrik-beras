VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMasterSupplier 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Supplier"
   ClientHeight    =   7440
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   13320
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   13320
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   7695
      Picture         =   "frmMasterSupplier.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   70
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6660
      Picture         =   "frmMasterSupplier.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   69
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.TextBox txtFile 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6300
      TabIndex        =   68
      Top             =   5010
      Width           =   2280
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "Browse"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8640
      Picture         =   "frmMasterSupplier.frx":0A64
      TabIndex        =   67
      Top             =   5010
      Width           =   915
   End
   Begin VB.CommandButton cmdLoadRpt 
      Caption         =   "Load History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4500
      Picture         =   "frmMasterSupplier.frx":0B66
      TabIndex        =   66
      Top             =   540
      Width           =   1320
   End
   Begin VB.CommandButton cmdNewKategori 
      Caption         =   "Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5850
      TabIndex        =   65
      Top             =   135
      Width           =   735
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   11
      Left            =   7605
      MaxLength       =   25
      TabIndex        =   62
      Text            =   "0"
      Top             =   4515
      Width           =   1905
   End
   Begin VB.ComboBox cmbWilayah2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterSupplier.frx":0C68
      Left            =   7605
      List            =   "frmMasterSupplier.frx":0C6A
      TabIndex        =   59
      Top             =   4110
      Width           =   3885
   End
   Begin VB.ComboBox cmbWilayah 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterSupplier.frx":0C6C
      Left            =   7605
      List            =   "frmMasterSupplier.frx":0C6E
      TabIndex        =   15
      Top             =   3300
      Width           =   3885
   End
   Begin VB.ComboBox cmbWilayah1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterSupplier.frx":0C70
      Left            =   7605
      List            =   "frmMasterSupplier.frx":0C72
      TabIndex        =   16
      Top             =   3705
      Width           =   3885
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   4
      Left            =   1890
      TabIndex        =   6
      Top             =   2925
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   9
      Left            =   1890
      TabIndex        =   10
      Top             =   4545
      Width           =   3075
   End
   Begin VB.ComboBox cmbStatus 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterSupplier.frx":0C74
      Left            =   7605
      List            =   "frmMasterSupplier.frx":0C76
      TabIndex        =   11
      Text            =   "cmbStatus"
      Top             =   585
      Width           =   3885
   End
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterSupplier.frx":0C78
      Left            =   1890
      List            =   "frmMasterSupplier.frx":0C7A
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   135
      Width           =   3885
   End
   Begin VB.CommandButton cmdSearchAccHutang 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9630
      TabIndex        =   45
      Top             =   2535
      Width           =   465
   End
   Begin VB.TextBox txtAccHutang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7605
      MaxLength       =   25
      TabIndex        =   13
      Text            =   "210.010.0100"
      Top             =   2505
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearchAccPiutang 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9630
      TabIndex        =   43
      Top             =   2895
      Width           =   465
   End
   Begin VB.TextBox txtAccPiutang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7605
      MaxLength       =   25
      TabIndex        =   14
      Top             =   2910
      Width           =   1950
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3570
      Picture         =   "frmMasterSupplier.frx":0C7C
      TabIndex        =   19
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1440
      Index           =   10
      Left            =   7605
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   12
      Top             =   990
      Width           =   5595
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   8
      Left            =   1890
      TabIndex        =   3
      Top             =   1305
      Width           =   3075
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   7
      Left            =   1890
      TabIndex        =   9
      Top             =   4140
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   6
      Left            =   1890
      TabIndex        =   8
      Top             =   3735
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   5
      Left            =   1890
      TabIndex        =   7
      Top             =   3330
      Width           =   3480
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterSupplier.frx":0D7E
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   540
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   3
      Left            =   1890
      TabIndex        =   5
      Top             =   2520
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   2
      Left            =   1890
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1800
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      MaxLength       =   50
      TabIndex        =   2
      Top             =   945
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      MaxLength       =   25
      TabIndex        =   1
      Top             =   540
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4800
      Picture         =   "frmMasterSupplier.frx":0E80
      TabIndex        =   20
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2340
      Picture         =   "frmMasterSupplier.frx":0F82
      TabIndex        =   18
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1125
      Picture         =   "frmMasterSupplier.frx":1084
      TabIndex        =   17
      Top             =   5865
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2025
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblAccPiutRetur_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   72
      Top             =   2940
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   71
      Top             =   2520
      Width           =   105
   End
   Begin VB.Image Image1 
      Height          =   1860
      Left            =   6270
      Stretch         =   -1  'True
      Top             =   5460
      Width           =   3465
   End
   Begin VB.Label lblLimit_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   64
      Top             =   4560
      Width           =   105
   End
   Begin VB.Label lblLimit_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Limit Kredit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   63
      Top             =   4560
      Width           =   1320
   End
   Begin VB.Label lblWilayah2_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   61
      Top             =   4140
      Width           =   105
   End
   Begin VB.Label lblWilayah2_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   60
      Top             =   4140
      Width           =   1320
   End
   Begin VB.Label lblWilayah_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   58
      Top             =   3330
      Width           =   105
   End
   Begin VB.Label lblWilayah_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   57
      Top             =   3330
      Width           =   1320
   End
   Begin VB.Label lblWilayah1_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   56
      Top             =   3735
      Width           =   1320
   End
   Begin VB.Label lblWilayah1_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   55
      Top             =   3735
      Width           =   105
   End
   Begin VB.Label lblHP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   54
      Top             =   2970
      Width           =   105
   End
   Begin VB.Label lblHP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "HP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   53
      Top             =   2970
      Width           =   1320
   End
   Begin VB.Label lblNPWP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "NPWP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   52
      Top             =   4590
      Width           =   1320
   End
   Begin VB.Label lblNPWP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   51
      Top             =   4590
      Width           =   105
   End
   Begin VB.Label lblStatus_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   50
      Top             =   615
      Width           =   1320
   End
   Begin VB.Label lblStatus_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   49
      Top             =   615
      Width           =   105
   End
   Begin VB.Label lblKategori_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   48
      Top             =   165
      Width           =   1320
   End
   Begin VB.Label lblKategori_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   47
      Top             =   165
      Width           =   105
   End
   Begin VB.Label lblAccHutang_1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Hutang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   46
      Top             =   2535
      Width           =   960
   End
   Begin VB.Label lblAccPiutRetur_1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Piutang Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   44
      Top             =   2940
      Width           =   1515
   End
   Begin VB.Label lblCP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   42
      Top             =   1350
      Width           =   105
   End
   Begin VB.Label lblCP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Contact Person"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   41
      Top             =   1350
      Width           =   1320
   End
   Begin VB.Label lblRek_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   40
      Top             =   4185
      Width           =   105
   End
   Begin VB.Label lblRek_1 
      BackStyle       =   0  'Transparent
      Caption         =   "No Rekening"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   39
      Top             =   4185
      Width           =   1320
   End
   Begin VB.Label lblEmail_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   38
      Top             =   3780
      Width           =   105
   End
   Begin VB.Label lblEmail_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   37
      Top             =   3780
      Width           =   1320
   End
   Begin VB.Label lblFax_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   36
      Top             =   3375
      Width           =   105
   End
   Begin VB.Label lblFax_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   35
      Top             =   3375
      Width           =   1320
   End
   Begin VB.Label lblKeterangan_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7380
      TabIndex        =   33
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label lblNama_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   31
      Top             =   975
      Width           =   150
   End
   Begin VB.Label lblKode_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1515
      TabIndex        =   30
      Top             =   585
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   29
      Top             =   5490
      Width           =   2130
   End
   Begin VB.Label lblTelp_1 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   28
      Top             =   2565
      Width           =   1320
   End
   Begin VB.Label lblTelp_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   27
      Top             =   2565
      Width           =   105
   End
   Begin VB.Label lblAlamat_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   26
      Top             =   1755
      Width           =   1320
   End
   Begin VB.Label lblAlamat_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   25
      Top             =   1755
      Width           =   105
   End
   Begin VB.Label lblNama_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   24
      Top             =   990
      Width           =   105
   End
   Begin VB.Label lblKode_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   23
      Top             =   585
      Width           =   105
   End
   Begin VB.Label lblNama_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   22
      Top             =   990
      Width           =   1320
   End
   Begin VB.Label lblKode_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   21
      Top             =   585
      Width           =   1320
   End
   Begin VB.Label lblKeterangan_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   34
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterSupplier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmbKategori_Click()
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open strcon

    rs.Open "select * from var_kategorisupplier where kategori='" & cmbKategori.text & "'", conn
    If Not rs.EOF Then
        txtAccHutang = rs!Acc_Hutang
        txtAccPiutang = rs!acc_piutangretur
    End If
    rs.Close

conn.Close
End Sub

Private Sub cmbWilayah_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbWilayah1.Clear
    rs.Open "select distinct wilayah from ms_supplier where area='" & cmbWilayah.text & "' order by wilayah", conn
    While Not rs.EOF
        cmbWilayah1.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub cmbWilayah1_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbWilayah2.Clear
    rs.Open "select distinct wilayah1 from ms_supplier where area='" & cmbWilayah.text & "' and wilayah='" & cmbWilayah1.text & "' order by wilayah1", conn
    While Not rs.EOF
        cmbWilayah2.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close

End Sub

Private Sub cmdBrowse_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.jpg|*.jpg"
    CommonDialog1.filename = "Image Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        Set Image1.Picture = LoadPicture(CommonDialog1.filename)
        txtFile = CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
   
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "delete from ms_supplier where kode_supplier='" & txtField(0).text & "'"
    conn.Execute "delete from setting_accsupplier where kode_supplier='" & txtField(0).text & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoadRpt_Click()
    frmSearch.query = SearchRptSupplier
    frmSearch.nmform = "frmMastersupplier"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data_rpt"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdNewKategori_Click()
    frmMasterKategoriSupplier.Show vbModal
    load_combo1
End Sub
Private Sub load_combo1()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbKategori.Clear
    rs.Open "select * from var_kategorisupplier order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_supplier] from ms_supplier where [kode_supplier]>'" & txtField(0) & "' order by [kode_supplier]", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_supplier] from ms_supplier where [kode_supplier]<'" & txtField(0) & "' order by [kode_supplier] desc", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearch_Click()
'    If User <> "sugik" Then
'         = "select kode_supplier,nama_supplier,alamat from ms_supplier"
'    End If
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmMastersupplier"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccHutang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='210.010'"
    frmSearch.nmform = "frmMastersupplier"
    frmSearch.nmctrl = "txtAccHutang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccPiutang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='110.030'"
    frmSearch.nmform = "frmMastersupplier"
    frmSearch.nmctrl = "txtAccpiutang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal

    
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
 
       For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
    
    If txtAccHutang.text = "" Then
        MsgBox "Acc Hutang harus diisi !", vbExclamation
        txtAccHutang.SetFocus
        Exit Sub
    End If
    
    If txtAccPiutang.text = "" Then
        MsgBox "Acc Piutang Retur harus diisi !", vbExclamation
        txtAccPiutang.SetFocus
        Exit Sub
    End If
 
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    conn.Execute "delete from rpt_ms_supplier where kode_supplier='" & txtField(0).text & "'"
    rs.Open "select * from ms_supplier where kode_supplier='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_supplier where kode_supplier='" & txtField(0).text & "'"
        
        add_dataacc 2, conn
    Else
        add_dataacc 1, conn
    End If
    rs.Close
    add_data
    add_dataRpt
    If txtFile <> "" Then
        rs.Open "select * from ms_supplier where [kode_supplier]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        Dim S As New ADODB.Stream
        With S
            .Type = adTypeBinary
            .Open
            .LoadFromFile txtFile
            rs("foto").value = .Read
            rs.Update
        End With
        rs.Close
        rs.Open "select * from rpt_ms_supplier where [kode_supplier]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
        rs("foto").value = S.Read
        Set S = Nothing
    End If
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(17)
    ReDim nilai(17)
    table_name = "ms_supplier"
    fields(0) = "kode_supplier"
    fields(1) = "nama_supplier"
    fields(2) = "kategori"
    fields(3) = "alamat"
    fields(4) = "telp"
    fields(5) = "hp"
    fields(6) = "keterangan"
    fields(7) = "fax"
    fields(8) = "email"
    fields(9) = "no_rekening"
    fields(10) = "contact_person"
    fields(11) = "area"
    fields(12) = "wilayah"
    fields(13) = "status"
    fields(14) = "npwp"
    fields(15) = "limitkredit"
    fields(16) = "wilayah1"
           
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = cmbKategori.text
    nilai(3) = txtField(2).text
    nilai(4) = txtField(3).text
    nilai(5) = txtField(4).text
    nilai(6) = txtField(10).text
    nilai(7) = txtField(5).text
    nilai(8) = txtField(6).text
    nilai(9) = txtField(7).text
    nilai(10) = txtField(8).text
    nilai(11) = cmbWilayah.text
    nilai(12) = cmbWilayah1.text
    nilai(13) = cmbStatus.text
    nilai(14) = txtField(9).text
    nilai(15) = txtField(11).text
    nilai(16) = cmbWilayah2.text
    
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_dataRpt()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(16)
    ReDim nilai(16)
    table_name = "rpt_ms_supplier"
    fields(0) = "kode_supplier"
    fields(1) = "nama_supplier"
    fields(2) = "kategori"
    fields(3) = "alamat"
    fields(4) = "telp"
    fields(5) = "hp"
    fields(6) = "keterangan"
    fields(7) = "fax"
    fields(8) = "email"
    fields(9) = "no_rekening"
    fields(10) = "contact_person"
    fields(11) = "area"
    fields(12) = "wilayah"
    fields(13) = "wilayah1"
    fields(14) = "status"
    fields(15) = "npwp"
           
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = cmbKategori.text
    nilai(3) = txtField(2).text
    nilai(4) = txtField(3).text
    nilai(5) = txtField(4).text
    nilai(6) = txtField(10).text
    nilai(7) = txtField(5).text
    nilai(8) = txtField(6).text
    nilai(9) = txtField(7).text
    nilai(10) = txtField(8).text
    nilai(11) = cmbWilayah.text
    nilai(12) = cmbWilayah1.text
    nilai(13) = cmbWilayah2.text
    nilai(14) = cmbStatus.text
    nilai(15) = txtField(9).text
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub add_dataacc(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(3)
    ReDim nilai(3)
    table_name = "setting_accSupplier"
    fields(0) = "kode_supplier"
    fields(1) = "acc_hutang"
    fields(2) = "acc_piutangRetur"
    
    nilai(0) = txtField(0).text
    nilai(1) = txtAccHutang
    nilai(2) = txtAccPiutang
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_supplier]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(4).text = ""
    txtField(5).text = ""
    txtField(6).text = ""
    txtField(7).text = ""
    txtField(8).text = ""
    txtField(9).text = ""
    txtField(10).text = ""
    txtField(11).text = "0"
    cmbStatus.text = ""
    txtFile = ""
    Image1.Picture = Nothing
    txtField(0).SetFocus
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
End Sub

Public Sub cari_data_rpt()
    cari_data "rpt_"
End Sub

Public Sub cari_data(Optional prefix As String)
On Error Resume Next
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from " & prefix & "ms_supplier  " & _
            "where kode_supplier='" & txtField(0).text & "' ", conn
    
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        txtField(4).text = rs(4)
        txtField(5).text = rs(7)
        txtField(6).text = rs(8)
        txtField(7).text = rs(9)
        txtField(8).text = rs(10)
        txtField(9).text = rs!npwp
        txtField(10).text = rs(5)
        txtField(11).text = rs!limitkredit
        SetComboText rs!kategori, cmbKategori
        cmbStatus.text = rs!status
        cmbWilayah.text = rs!area
        cmbWilayah1.text = rs!wilayah
        Dim mStream As New ADODB.Stream
        Dim fs As New Scripting.FileSystemObject
        fs.DeleteFile App.Path & "\temp.jpg"
          With mStream
            .Type = adTypeBinary
            .Open
            .Write rs("foto").value
            .SaveToFile App.Path & "\temp.jpg"
            Image1.Picture = LoadPicture(App.Path & "\temp.jpg")
            txtFile = App.Path & "\temp.jpg"
          End With
        
          Set mStream = Nothing
        rs.Close
        rs.Open "select * from setting_accsupplier  " & _
            "where kode_supplier='" & txtField(0).text & "' ", conn
        If Not rs.EOF Then
            txtAccHutang = rs!Acc_Hutang
            txtAccPiutang = rs!acc_piutangretur
        End If
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
    
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    load_combo
    reset_form
    altersupplier
    security_setting
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub

Private Sub load_combo()
    altersupplier
    conn.ConnectionString = strcon
    conn.Open
    cmbKategori.Clear
    rs.Open "select * from var_kategorisupplier order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbStatus.Clear
    rs.Open "select distinct status from ms_supplier order by status", conn
    While Not rs.EOF
        cmbStatus.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbWilayah.Clear
    rs.Open "select distinct area from ms_supplier order by area", conn
    While Not rs.EOF
        cmbWilayah.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    If cmbKategori.ListCount > 0 Then cmbKategori.ListIndex = 0
    conn.Close

End Sub
Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index))
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub

Private Sub security_setting()
    Dim kolom As String
    Dim split_kolom() As String
    
    conn.Open strcon
    rs.Open "select * from user_groupquery where nama_group='" & usergroup & "' and nama_table='ms_supplier'", conn
    If Not rs.EOF Then
        If rs!columnlist <> "" Then
            kolom = rs!columnlist

            txtField(0).Visible = False
            lblKode_1.Visible = False
            lblKode_2.Visible = False
            lblKode_3.Visible = False
        
            txtField(1).Visible = False
            lblNama_1.Visible = False
            lblNama_2.Visible = False
            lblNama_3.Visible = False
        
            cmbKategori.Visible = False
            lblKategori_1.Visible = False
            lblKategori_2.Visible = False
        
            txtField(2).Visible = False
            lblAlamat_1.Visible = False
            lblAlamat_2.Visible = False
        
            txtField(3).Visible = False
            lblTelp_1.Visible = False
            lblTelp_2.Visible = False
        
            txtField(4).Visible = False
            lblHP_1.Visible = False
            lblHP_2.Visible = False
        
            txtField(10).Visible = False
            lblKeterangan_1.Visible = False
            lblKeterangan_2.Visible = False
        
            txtField(5).Visible = False
            lblFax_1.Visible = False
            lblFax_2.Visible = False
        
            txtField(6).Visible = False
            lblEmail_1.Visible = False
            lblEmail_2.Visible = False
        
            txtField(7).Visible = False
            lblRek_1.Visible = False
            lblRek_2.Visible = False
        
            txtField(8).Visible = False
            lblCP_1.Visible = False
            lblCP_2.Visible = False
        
            cmbWilayah.Visible = False
            lblWilayah_1.Visible = False
            lblWilayah_2.Visible = False
        
            cmbWilayah1.Visible = False
            lblWilayah1_1.Visible = False
            lblWilayah1_2.Visible = False
        
            cmbStatus.Visible = False
            lblStatus_1.Visible = False
            lblStatus_2.Visible = False
        
            txtField(9).Visible = False
            lblNPWP_1.Visible = False
            lblNPWP_2.Visible = False
        
            txtField(11).Visible = False
            lblLimit_1.Visible = False
            lblLimit_2.Visible = False
        
            cmbWilayah2.Visible = False
            lblWilayah2_1.Visible = False
            lblWilayah2_2.Visible = False
        End If
    End If
    rs.Close
    split_kolom = Split(kolom, ",")
    For Each Column In split_kolom
        Select Case LCase(Column)
        Case "kode_supplier"
            txtField(0).Visible = True
            lblKode_1.Visible = True
            lblKode_2.Visible = True
            lblKode_3.Visible = True
        Case "nama_supplier"
            txtField(1).Visible = True
            lblNama_1.Visible = True
            lblNama_2.Visible = True
            lblNama_3.Visible = True
        Case "kategori"
            cmbKategori.Visible = True
            lblKategori_1.Visible = True
            lblKategori_2.Visible = True
        Case "alamat"
            txtField(2).Visible = True
            lblAlamat_1.Visible = True
            lblAlamat_2.Visible = True
        Case "telp"
            txtField(3).Visible = True
            lblTelp_1.Visible = True
            lblTelp_2.Visible = True
        Case "hp"
            txtField(4).Visible = True
            lblHP_1.Visible = True
            lblHP_2.Visible = True
        Case "keterangan"
            txtField(10).Visible = True
            lblKeterangan_1.Visible = True
            lblKeterangan_2.Visible = True
        Case "fax"
            txtField(5).Visible = True
            lblFax_1.Visible = True
            lblFax_2.Visible = True
        Case "email"
            txtField(6).Visible = True
            lblEmail_1.Visible = True
            lblEmail_2.Visible = True
        Case "no_rekening"
            txtField(7).Visible = True
            lblRek_1.Visible = True
            lblRek_2.Visible = True
        Case "contact_person"
            txtField(8).Visible = True
            lblCP_1.Visible = True
            lblCP_2.Visible = True
        Case "area"
            cmbWilayah.Visible = True
            lblWilayah_1.Visible = True
            lblWilayah_2.Visible = True
        Case "wilayah"
            cmbWilayah1.Visible = True
            lblWilayah1_1.Visible = True
            lblWilayah1_2.Visible = True
        Case "status"
            cmbStatus.Visible = True
            lblStatus_1.Visible = True
            lblStatus_2.Visible = True
        Case "npwp"
            txtField(9).Visible = True
            lblNPWP_1.Visible = True
            lblNPWP_2.Visible = True
        Case "limitkredit"
            txtField(11).Visible = True
            lblLimit_1.Visible = True
            lblLimit_2.Visible = True
        Case "wilayah1"
            cmbWilayah2.Visible = True
            lblWilayah2_1.Visible = True
            lblWilayah2_2.Visible = True
        End Select
    Next
    conn.Close
End Sub
