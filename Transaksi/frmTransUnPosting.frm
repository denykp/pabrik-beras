VERSION 5.00
Begin VB.Form frmTransUnposting 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "UnPosting / Hapus Transaksi"
   ClientHeight    =   4620
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5235
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   5235
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   735
      Left            =   1155
      TabIndex        =   10
      Top             =   4875
      Width           =   2895
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Proses"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   180
      Picture         =   "frmTransUnPosting.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   5115
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Frame Frame2 
      Caption         =   "UnPosting Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1905
      Left            =   135
      TabIndex        =   6
      Top             =   150
      Width           =   4995
      Begin VB.CommandButton cmdSimpan 
         Caption         =   "&Proses"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   2115
         Picture         =   "frmTransUnPosting.frx":0102
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   1020
         UseMaskColor    =   -1  'True
         Width           =   1230
      End
      Begin VB.TextBox txtField 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   1770
         TabIndex        =   0
         Top             =   435
         Width           =   2145
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   345
         TabIndex        =   7
         Top             =   465
         Width           =   1320
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Hapus Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2250
      Left            =   135
      TabIndex        =   2
      Top             =   2190
      Width           =   4995
      Begin VB.CommandButton cmdHapus 
         Caption         =   "&Hapus"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Left            =   2130
         Picture         =   "frmTransUnPosting.frx":0204
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   900
         UseMaskColor    =   -1  'True
         Width           =   1230
      End
      Begin VB.TextBox txtField 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   1740
         TabIndex        =   3
         Top             =   390
         Width           =   2145
      End
      Begin VB.Label Label3 
         Caption         =   "Catatan : Yang bisa dihapus adalah transaksi yang belum diposting"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   150
         TabIndex        =   8
         Top             =   1620
         Width           =   4725
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "No. Transaksi :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   315
         TabIndex        =   5
         Top             =   420
         Width           =   1320
      End
   End
End
Attribute VB_Name = "frmTransUnposting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub UnPosting_ReturJual(NoTransaksi As String)
Dim rs As New ADODB.Recordset, valid As Boolean, StatusJual As String

    conn.Open strcon
    conn.BeginTrans
    
    valid = True
    
    
    rs.Open "Select t.Nomer_BayarPiutang,t.Status_Posting from t_bayarPiutang_retur j " & _
            "inner join t_bayarPiutangh t on t.nomer_bayarpiutang=j.nomer_bayarpiutang " & _
            "where j.Nomer_retur='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        If rs!status_posting = "1" Then
            MsgBox "Nomor Pembayaran Piutang : " & rs!nomer_bayarpiutang & " harus di-unposting dulu !", vbExclamation
            valid = False
        End If
    End If
    rs.Close

    If valid Then
        deletekartustock "ReturJual", NoTransaksi, conn
        rs.Open "select d.kode_bahan,d.nomer_serial,d.qty,d.berat,d.hpp,d.kode_gudang from t_returjuald d " & _
        " where d.nomer_returjual='" & NoTransaksi & "'", conn, adOpenKeyset
        While Not rs.EOF
            updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty * -1, rs!berat * -1, rs!hpp, conn
            rs.MoveNext
        Wend
        rs.Close
        conn.Execute "update t_returjualh set status_posting='0' where nomer_returjual='" & NoTransaksi & "'"
    
        conn.Execute "delete from list_hutang_retur where nomer_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
        
    End If
        
    conn.CommitTrans
    conn.Close
End Sub
    
    


Private Sub UnPosting_Produksi(NoTransaksi As String)
Dim rs As New ADODB.Recordset
Dim tanggal As Date
    conn.Open strcon
    conn.BeginTrans

    conn.Execute "update t_produksih set status_posting='0' where nomer_produksi='" & NoTransaksi & "'"

    conn.Execute "delete from tmp_kartustock where id='" & NoTransaksi & "'"
    
    
    rs.Open "select tanggal_produksi from t_produksih where nomer_produksi='" & NoTransaksi & "'", conn
    If Not rs.EOF Then tanggal = rs("tanggal_produksi")
    rs.Close

    rs.Open "select t.kode_gudang,d.kode_bahan,d.nomer_serial from t_stockkoreksid d " & _
            "inner join t_stockkoreksih t on t.nomer_koreksi=d.nomer_koreksi " & _
            "where d.nomer_koreksi='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn
        rs.MoveNext
    Wend
    rs.Close
    
    
    rs.Open "select t.kode_gudang,t.kode_bahan,t.nomer_serial from t_produksi_bahan t " & _
            "where t.nomer_produksi='" & NoTransaksi & "' ", conn
    While Not rs.EOF
        HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select t.kode_gudang,t.kode_bahan,t.nomer_serial from t_produksi_kemasan t " & _
            "where t.nomer_produksi='" & NoTransaksi & "'", conn
    While Not rs.EOF
        HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn
        rs.MoveNext
    Wend
    rs.Close
    Dim nomer_serial As String
    rs.Open "select t.kode_gudang,t.kode_bahan from t_produksi_hasil t " & _
            "where t.nomer_produksi='" & NoTransaksi & "'", conn
    While Not rs.EOF
        nomer_serial = Format(tanggal, "dd/MM/yy") & "-" & Right(NoTransaksi, 5)
        HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, nomer_serial, conn
        rs.MoveNext
    Wend
    rs.Close

    conn.CommitTrans
    conn.Close
End Sub

Private Sub UnPosting_SuratJalan(NoTransaksi As String)
Dim rs As New ADODB.Recordset
Dim tanggal As Date
    conn.Open strcon
    rs.Open "select * from list_hutang where nomer_transaksi='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
        If rs!total_bayar > 0 Then
            MsgBox "Tidak dapat diproses karena sudah dilakukan pembayaran ekspedisi"
            rs.Close
            conn.Close
            Exit Sub
        End If
    End If
    rs.Close
    conn.BeginTrans

    conn.Execute "update t_suratjalanh set status_posting='0' where nomer_suratjalan='" & NoTransaksi & "'"

    deletekartustock "", NoTransaksi, conn
    conn.Execute "delete from tmp_kartustock where id='" & NoTransaksi & "'"
    conn.Execute "delete from list_hutang where nomer_transaksi='" & NoTransaksi & "'"
    
    rs.Open "select t.kode_gudang,t.kode_bahan,t.nomer_serial,qty,berat from t_suratjalan_timbang t " & _
            "where t.nomer_suratjalan='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        updatestock NoTransaksi, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!qty, rs!berat, 0, conn
        'HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn
        rs.MoveNext
    Wend
    rs.Close

    conn.CommitTrans
    hitungulang_so
    conn.Close
End Sub

Private Sub UnPosting_PenerimaanBarang(NoTransaksi As String)
Dim rs As New ADODB.Recordset
Dim tanggal As Date
    conn.Open strcon
    conn.BeginTrans

    conn.Execute "update t_terimabarangh set status_posting='0' where nomer_terimabarang='" & NoTransaksi & "'"

    conn.Execute "delete from tmp_kartustock where id='" & NoTransaksi & "'"
    
    
    rs.Open "select g.kode_gudang,t.kode_bahan,t.nomer_serial from t_terimabarangd t " & _
            "inner join t_terimabarangh h on t.nomer_terimabarang=h.nomer_terimabarang " & _
            "inner join t_terimabarang_timbang g on g.nomer_terimabarang=h.nomer_terimabarang and g.kode_bahan=t.kode_bahan " & _
            "where t.nomer_terimabarang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select distinct t.kode_bahan,t.nomer_serial,t.no_urut from t_terimabarangd t inner join t_terimabarangh h on t.nomer_terimabarang=h.nomer_terimabarang " & _
    " where t.nomer_terimabarang='" & NoTransaksi & "' ", conn
    While Not rs.EOF
        conn.Execute "delete from stock_komposisi where kode_bahan='" & rs!kode_bahan & "' and nomer_serial='" & rs!nomer_serial & "' and kode_bahanbaku='" & rs!kode_bahan & "'"
        conn.Execute "delete from t_terimabarang_komposisi where nomer_terimabarang='" & NoTransaksi & "'"
        rs.MoveNext
    Wend
    rs.Close
    

    conn.CommitTrans
    conn.Close
End Sub

Private Sub UnPosting_Hutang(NoTransaksi As String)
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_Kas As String, Acc_Bank As String, Acc_PiutangReturBeli As String
Dim Acc_PendapatanLain As String, Acc_BiayaLain As String
Dim Acc_UMBeli As String
Dim Acc_Hutang As String
Dim CaraBayar As String
Dim JumlahBayar As Double, NilaiTransfer As Double, UangMuka As Double
Dim TotalRetur As Double
Dim KodeBank As String
Dim selisih As Double
Dim TotalHutang As Double
Dim Kodesupplier As String
Dim Acc_HutangGiro As String, NilaiBG As Double
Dim NoBG As String, TanggalCair As Date
Dim AccSelisih As String

    conn.Open strcon
    conn.BeginTrans
    
    rs.Open "select tanggal_bayarhutang as tanggal,kode_supplier,jumlah_tunai,deposit,selisih,acc_selisih from t_bayarhutangh where nomer_bayarhutang='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    AccSelisih = rs("acc_selisih")
    UangMuka = rs("deposit")
    Kodesupplier = rs("kode_supplier")
    rs.Close
    
    
    conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from list_bg where nomer_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from tmp_kartuhutang where id='" & NoTransaksi & "'"
    
    
    rs.Open "select nomer_beli,jumlah_bayar from t_bayarhutang_beli " & _
            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenKeyset
    While Not rs.EOF
        conn.Execute "update list_hutang set total_bayar=total_bayar - " & rs("jumlah_bayar") & " " & _
                     "where nomer_transaksi='" & rs("nomer_beli") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select nomer_retur,jumlah from t_bayarhutang_retur " & _
            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_piutang_retur set total_bayar=total_bayar - " & rs("jumlah") & " " & _
                     "where nomer_transaksi='" & rs("nomer_retur") & "'"
                     
        rs.MoveNext
    Wend
    rs.Close
    conn.Execute "update ms_supplier set deposit=deposit+" & Replace(UangMuka, ",", ".") & " where kode_supplier='" & Kodesupplier & "'"
    conn.Execute "update t_bayarhutangh set status_posting='0' where nomer_bayarhutang='" & NoTransaksi & "'"

    conn.CommitTrans
    conn.Close
End Sub

Private Sub UnPosting_Piutang(NoTransaksi As String)
Dim i As Integer, y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_Kas As String, Acc_Bank As String, Acc_HutangReturJual As String
Dim Acc_PendapatanLain As String, Acc_BiayaLain As String
Dim Acc_UMJual As String
Dim Acc_piutang As String
Dim CaraBayar As String
Dim JumlahBayar As Double, NilaiTransfer As Double, UangMuka As Double
Dim TotalRetur As Double
Dim KodeBank As String
Dim selisih As Double
Dim TotalPiutang As Double
Dim KodeCustomer As String
Dim AccSelisih As String
Dim Acc_PiutangGiroDiTangan As String, NilaiBG As Double
Dim NoBG As String, TanggalCair As Date
Dim Acc_Komisi As String, Acc_KomisiHarusBayar As String
Dim TotalSelisih As Double


    conn.Open strcon
    conn.BeginTrans
    
    rs.Open "select tanggal_bayarPiutang as tanggal,kode_Customer,jumlah_tunai,selisih, " & _
            "deposit from t_bayarPiutangh where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    If Not rs.EOF Then
        tanggal = rs("tanggal")
    
        JumlahBayar = rs("jumlah_tunai")
    '    AccSelisih = rs("acc_selisih")
        selisih = rs!selisih
        
        
        UangMuka = rs("deposit")
        
        KodeCustomer = rs("kode_Customer")
    End If
    rs.Close
    
    
    conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from list_bg where nomer_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from tmp_kartupiutang where id='" & NoTransaksi & "'"
    
    rs.Open "select b.nomer_Jual,b.jumlah_bayar from t_bayarPiutang_Jual b " & _
            "left join t_jualh j on j.nomer_jual=b.nomer_jual " & _
            "where b.nomer_bayarPiutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_Piutang set total_bayar=total_bayar - " & rs("jumlah_bayar") & " " & _
                     "where nomer_transaksi='" & rs("nomer_Jual") & "'"
        
            
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select nomer_retur,jumlah from t_bayarPiutang_retur " & _
            "where nomer_bayarPiutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_Hutang_Retur set total_bayar=total_bayar - " & rs("jumlah") & " " & _
                     "where nomer_transaksi='" & rs("nomer_retur") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Execute "update ms_Customer set deposit=deposit+" & UangMuka & " where kode_Customer='" & KodeCustomer & "'"

    
    
    conn.Execute "update t_bayarPiutangh set status_posting='0' where nomer_bayarPiutang='" & NoTransaksi & "'"

    conn.CommitTrans
    
    conn.Close
End Sub

Private Sub UnPosting_KasBank(NoTransaksi As String)
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.BeginTrans
    
    rs.Open "select ket_bg from t_kasbankh " & _
            "where no_transaksi='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_bg set status_cair='0' where nomer_bg='" & rs!ket_bg & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select no_pengeluaran from t_kasbankd " & _
            "where no_transaksi='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "Update t_suratJalanH set status_kas='0' where Nomer_SuratJalan='" & rs("No_Pengeluaran") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Execute "update t_kasbankh set status_posting='0' where no_transaksi='" & NoTransaksi & "'"

    conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
    
    conn.CommitTrans
    conn.Close
End Sub

Private Sub UnPosting_Penjualan(NoTransaksi As String)
Dim rs As New ADODB.Recordset, valid As Boolean, StatusJual As String

    conn.Open strcon
    conn.BeginTrans
    
    valid = True
    
    rs.Open "Select Cara_Bayar from t_jualH " & _
            "where Nomer_Jual='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        StatusJual = rs!cara_bayar
    End If
    rs.Close
    
    If StatusJual = "K" Then
        rs.Open "Select t.Nomer_BayarPiutang,t.Status_Posting from t_bayarPiutang_Jual j " & _
                "inner join t_bayarPiutangh t on t.nomer_bayarpiutang=j.nomer_bayarpiutang " & _
                "where j.Nomer_Jual='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
        If Not rs.EOF Then
            If rs!status_posting = "1" Then
                MsgBox "Nomor Pembayaran Piutang : " & rs!nomer_bayarpiutang & " harus di-unposting dulu !", vbExclamation
                valid = False
            End If
        End If
        rs.Close
    End If
    
    If valid Then
        conn.Execute "update t_jualh set status_posting='0' where nomer_jual='" & NoTransaksi & "'"
    
        conn.Execute "delete from list_piutang where nomer_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from tmp_kartupiutang where id='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_bayarpiutangh where nomer_bayarpiutang in (select Nomer_bayarpiutang from t_bayarpiutang_jual where nomer_jual='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarpiutang_cek where nomer_bayarpiutang in (select Nomer_bayarpiutang from t_bayarpiutang_jual where nomer_jual='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarpiutang_transfer where nomer_bayarpiutang in (select Nomer_bayarpiutang from t_bayarpiutang_jual where nomer_jual='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarpiutang_retur where nomer_bayarpiutang in (select Nomer_bayarpiutang from t_bayarpiutang_jual where nomer_jual='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarpiutang_jual where nomer_jual='" & NoTransaksi & "'"
    End If
        
    conn.CommitTrans
    conn.Close
End Sub

Private Sub UnPosting_Pembelian(NoTransaksi As String)
Dim rs As New ADODB.Recordset, valid As Boolean
Dim CaraBayar As String
    conn.Open strcon
    conn.BeginTrans
    
    valid = True
    
    rs.Open "Select cara_bayar from t_Belih  " & _
            "where Nomer_Beli='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        CaraBayar = rs!cara_bayar
    End If
    rs.Close
    
    If CaraBayar = "K" Then
        rs.Open "Select t.Nomer_BayarHutang,t.Status_Posting from t_bayarHutang_Beli j " & _
                "inner join t_bayarHutangh t on t.nomer_bayarHutang=j.nomer_bayarHutang " & _
                "where j.Nomer_Beli='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
        If Not rs.EOF Then
            If rs!status_posting = "1" Then
                MsgBox "Nomor Pembayaran Hutang : " & rs!nomer_bayarhutang & " harus di-unposting dulu !", vbExclamation
                valid = False
            End If
        End If
        rs.Close
    End If
    
    If valid Then
        conn.Execute "update t_Belih set status_posting='0' where nomer_Beli='" & NoTransaksi & "'"
    
        conn.Execute "delete from list_Hutang where nomer_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from tmp_kartuHutang where id='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
        
        conn.Execute "delete from t_bayarHutangh where nomer_bayarHutang in (select Nomer_bayarHutang from t_bayarHutang_Beli where nomer_Beli='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarHutang_cek where nomer_bayarHutang in (select Nomer_bayarHutang from t_bayarHutang_Beli where nomer_Beli='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarHutang_transfer where nomer_bayarHutang in (select Nomer_bayarHutang from t_bayarHutang_Beli where nomer_Beli='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarHutang_retur where nomer_bayarHutang in (select Nomer_bayarHutang from t_bayarHutang_Beli where nomer_Beli='" & NoTransaksi & "')"
        conn.Execute "delete from t_bayarHutang_Beli where nomer_Beli='" & NoTransaksi & "'"
    End If
        
    conn.CommitTrans
    conn.Close
End Sub


Private Sub UnPosting_Pengiriman(NoTransaksi As String)
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.BeginTrans

    conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
    
    conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
    
    conn.CommitTrans
    conn.Close
End Sub

Private Sub UnPosting_Manol(NoTransaksi As String)
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.BeginTrans
    conn.Execute "delete from t_jurnald where no_transaksi = '" & NoTransaksi & "'"
    conn.Execute "delete from t_jurnalh where no_transaksi = '" & NoTransaksi & "'"
    conn.Execute "update t_bayarkulih set status_posting = 0 where nomer_bayarkuli = '" & NoTransaksi & "'"
    conn.CommitTrans
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
Dim NoTransaksi As String
    NoTransaksi = Trim(txtField(0).text)
    
    Select Case Left(UCase(NoTransaksi), 2)
        Case "PR"
            If CekStatus(NoTransaksi, "t_produksih", "nomer_produksi") = False Then Exit Sub
            If CekUrutanProduksi(NoTransaksi) = False Then
                If MsgBox("Apakah tetap akan dilanjutkan ?", vbQuestion + vbYesNo, "Konfirmasi") = vbNo Then
                    Exit Sub
                End If
            End If
            UnPosting_Produksi NoTransaksi
            
        Case "KB"
            If CekStatus(NoTransaksi, "t_suratjalanh", "nomer_suratjalan") = False Then Exit Sub
            UnPosting_SuratJalan NoTransaksi
            
        Case "TB"
            If CekStatus(NoTransaksi, "t_terimabarangh", "nomer_terimabarang") = False Then Exit Sub
            UnPosting_PenerimaanBarang NoTransaksi
            
        Case "PH"
            If CekStatus(NoTransaksi, "t_bayarhutangh", "nomer_bayarhutang") = False Then Exit Sub
            UnPosting_Hutang NoTransaksi
            
        Case "PP"
            If CekStatus(NoTransaksi, "t_bayarpiutangh", "nomer_bayarpiutang") = False Then Exit Sub
            UnPosting_Piutang NoTransaksi
            
        Case "PJ"
            If CekStatus(NoTransaksi, "t_jualh", "nomer_jual") = False Then Exit Sub
            UnPosting_Penjualan NoTransaksi
        Case "PO", "RJ"
            If CekStatus(NoTransaksi, "t_returjualh", "nomer_returjual") = False Then Exit Sub
            UnPosting_ReturJual NoTransaksi
        Case "PB"
            If CekStatus(NoTransaksi, "t_belih", "nomer_beli") = False Then Exit Sub
            UnPosting_Pembelian NoTransaksi

        Case "KM", "KK", "BM", "BK"
            If CekStatus(NoTransaksi, "t_kasbankh", "no_transaksi") = False Then Exit Sub
            UnPosting_KasBank NoTransaksi
        Case "MN"
            If CekStatus(NoTransaksi, "t_bayarkulih", "nomer_bayarkuli") = False Then Exit Sub
            UnPosting_Manol NoTransaksi
        Case Else
            MsgBox "Belum ada fasilitas untuk transaksi ini !", vbExclamation
            Exit Sub
    End Select
    
    MsgBox "Proses Selesai !", vbInformation
End Sub

Private Function CekStatus(NoTransaksi As String, NamaTabel As String, Nomor As String) As Boolean
Dim rs As New ADODB.Recordset
    CekStatus = True
    conn.Open strcon
    
    NoTransaksi = Trim(txtField(0).text)
    
    rs.Open "select status_posting from " & NamaTabel & " " & _
            "where " & Nomor & "='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        If (rs!status_posting) = "0" Then
            MsgBox "Transaksi belum diposting !", vbExclamation
            CekStatus = False
        End If
    End If
    rs.Close
    
    conn.Close
End Function

Private Function CekUrutanProduksi(NoTransaksi As String) As Boolean
Dim rs As New ADODB.Recordset, KodeHasil As String
Dim rs2 As New ADODB.Recordset
    CekUrutanProduksi = True
    conn.Open strcon
    
    NoTransaksi = Trim(txtField(0).text)
    
    rs.Open "select kode_bahan,nomer_serial,kode_gudang,nomer_urut from tmp_kartustock " & _
            "where id='" & NoTransaksi & "' and tipe='Hasil Produksi'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        Do While Not rs.EOF
            rs2.Open "select top 1 nomer_urut from tmp_kartustock where kode_bahan='" & rs!kode_bahan & "'and nomer_serial='" & rs!nomer_serial & "' " & _
                     "and kode_gudang='" & rs!kode_gudang & "' order by nomer_urut desc", conn, adOpenDynamic, adLockOptimistic
            If (rs2!nomer_urut) > (rs!nomer_urut) Then
                CekUrutanProduksi = False
                MsgBox "Bahan : " & rs!kode_bahan & "  dengan nomer seri : " & rs!nomer_serial & "  sudah diproses lebih lanjut !" & vbCrLf & _
                       "Sebelum UnPosting transaksi ini, proses2 lanjutan tersebut harus di-UnPosting atau dibatalkan terlebih dahulu. " & vbCrLf & _
                       "Informasi lebih detail tentang proses2 tersebut bisa dilihat pada laporan kartustock.", vbExclamation
            End If
            rs2.Close
            rs.MoveNext
        Loop
    End If
    rs.Close
    
    conn.Close
End Function

Private Function CekPosting(NoTransaksi As String, NamaTabel As String, Nomor As String) As Boolean
Dim rs As New ADODB.Recordset
    CekPosting = True
    conn.Open strcon
    
    NoTransaksi = Trim(txtField(1).text)
    
    rs.Open "select status_posting from " & NamaTabel & " " & _
            "where " & Nomor & "='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        If (rs!status_posting) <> "0" Then
            MsgBox "Transaksi sudah diposting, tidak bisa dihapus !", vbExclamation
            CekPosting = False
        End If
    End If
    rs.Close
    conn.Close
End Function

Private Function CekPostingSO(NoTransaksi As String, NamaTabel As String, Nomor As String) As Boolean
Dim rs As New ADODB.Recordset
    CekPostingSO = True
    conn.Open strcon
    
    NoTransaksi = Trim(txtField(1).text)
    
    rs.Open "select status from " & NamaTabel & " " & _
            "where " & Nomor & "='" & NoTransaksi & "'", conn, adOpenDynamic, adLockOptimistic
    If Not rs.EOF Then
        If (rs!status) <> "0" Then
            MsgBox "Transaksi sudah diposting, tidak bisa dihapus !", vbExclamation
            CekPostingSO = False
        End If
    End If
    rs.Close
    conn.Close
End Function



Private Sub cmdHapus_Click()
Dim NoTransaksi As String
    NoTransaksi = Trim(txtField(1).text)
    
    Select Case Left(UCase(NoTransaksi), 2)
        Case "KM", "KK", "BM", "BK"
            If CekPosting(NoTransaksi, "t_kasbankh", "no_transaksi") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_kasbankh where no_transaksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_kasbankd where no_transaksi='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "GL"
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_jurnalh where no_transaksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_jurnald where no_transaksi='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "PR"
            If CekPosting(NoTransaksi, "t_produksih", "nomer_produksi") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_produksih where nomer_produksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_produksi_komposisihasil where nomer_produksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_produksi_komposisibahan where nomer_produksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_produksi_kemasan where nomer_produksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_produksi_hasil where nomer_produksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_produksi_biaya where nomer_produksi='" & NoTransaksi & "'"
            conn.Execute "delete from t_produksi_bahan where nomer_produksi='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "PJ"
            If CekPosting(NoTransaksi, "t_jualh", "nomer_jual") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_jualh where nomer_jual='" & NoTransaksi & "'"
            conn.Execute "delete from t_juald where nomer_jual='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "PB"
            If CekPosting(NoTransaksi, "t_belih", "nomer_beli") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_belih where nomer_beli='" & NoTransaksi & "'"
            conn.Execute "delete from t_belid where nomer_beli='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "PP"
            If CekPosting(NoTransaksi, "t_bayarpiutangh", "nomer_bayarpiutang") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_bayarpiutangh where nomer_bayarpiutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarpiutang_transfer where nomer_bayarpiutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarpiutang_retur where nomer_bayarpiutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarpiutang_jual where nomer_bayarpiutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarpiutang_cek where nomer_bayarpiutang='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "PH"
            If CekPosting(NoTransaksi, "t_bayarhutangh", "nomer_bayarhutang") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_bayarhutangh where nomer_bayarhutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarhutang_transfer where nomer_bayarhutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarhutang_retur where nomer_bayarhutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarhutang_beli where nomer_bayarhutang='" & NoTransaksi & "'"
            conn.Execute "delete from t_bayarhutang_cek where nomer_bayarhutang='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "SO"
            If CekPostingSO(NoTransaksi, "t_SOH", "nomer_SO") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_SOH where nomer_SO='" & NoTransaksi & "'"
            conn.Execute "delete from t_SOD where nomer_SO='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case "KB"
            If CekPosting(NoTransaksi, "t_suratjalanh", "nomer_suratjalan") = False Then Exit Sub
            conn.Open strcon
            conn.BeginTrans
            conn.Execute "delete from t_suratjalanh where nomer_suratjalan='" & NoTransaksi & "'"
            conn.Execute "delete from t_suratjaland where nomer_suratjalan='" & NoTransaksi & "'"
            conn.Execute "delete from t_suratjalan_timbang where nomer_suratjalan='" & NoTransaksi & "'"
            conn.Execute "delete from t_suratjalan_angkut where nomer_suratjalan='" & NoTransaksi & "'"
            conn.CommitTrans
            conn.Close
        Case Else
            MsgBox "Belum ada fasilitas untuk transaksi ini !", vbExclamation
            Exit Sub
    End Select
    
    MsgBox "Proses Selesai !", vbInformation
End Sub

Private Sub Command1_Click()
HitungUlang_KartuStock rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, conn
End Sub

Private Sub Command2_Click()
Dim connxx As New ADODB.Connection
Dim rsxx As New ADODB.Recordset
    connxx.Open strcon
    connxx.BeginTrans
    
    rsxx.Open "select nomer_bayarpiutang from t_bayarpiutangh where status_posting='0' and nilaitransfer+nilai_cek+jumlah_tunai>0", connxx, adOpenDynamic, adLockOptimistic
    While Not rsxx.EOF
        Posting_BayarPiutang rsxx!nomer_bayarpiutang, conn
        rsxx.MoveNext
    Wend
    rsxx.Close
    
    connxx.CommitTrans
    connxx.Close
    MsgBox "Selesai"
End Sub
