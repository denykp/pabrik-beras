VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmLaporanPenjualanVsHPP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Laporan Penjualan Vs HPP"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5640
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   5640
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Tampilkan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2145
      Picture         =   "frmLaporanPenjualanVsHPP.frx":0000
      TabIndex        =   2
      Top             =   870
      UseMaskColor    =   -1  'True
      Width           =   1320
   End
   Begin MSComCtl2.DTPicker DTPicker0 
      Height          =   375
      Left            =   2385
      TabIndex        =   0
      Top             =   210
      Width           =   1860
      _ExtentX        =   3281
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "MMMM yyyy"
      Format          =   16515075
      CurrentDate     =   40960
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1290
      TabIndex        =   3
      Top             =   1965
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   16515075
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   3375
      TabIndex        =   4
      Top             =   1995
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   16515075
      CurrentDate     =   38927
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Label1"
      Height          =   255
      Left            =   690
      TabIndex        =   9
      Top             =   2400
      Width           =   720
   End
   Begin VB.Label Label2 
      Caption         =   "Label1"
      Height          =   255
      Left            =   2040
      TabIndex        =   8
      Top             =   2400
      Width           =   810
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "dari"
      Height          =   255
      Left            =   1515
      TabIndex        =   7
      Top             =   2400
      Width           =   345
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   285
      TabIndex        =   6
      Top             =   1995
      Width           =   915
   End
   Begin VB.Label Label3 
      Caption         =   "s/d"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   2850
      TabIndex        =   5
      Top             =   2055
      Width           =   450
   End
   Begin VB.Label Label7 
      Caption         =   "Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1260
      TabIndex        =   1
      Top             =   255
      Width           =   1050
   End
End
Attribute VB_Name = "frmLaporanPenjualanVsHPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim conn As New ADODB.Connection


Private Sub cmdSimpan_Click()
Dim paramX As String
conn.Open strcon
conn.BeginTrans
conn.Execute "delete from tmp_PenjualanVsHPP"
conn.Execute "insert into tmp_PenjualanVsHPP (nomer_order,nomer_jual,tanggal,kode_bahan,nama_bahan,berat,harga_jual,hpp,total_jual,total_hpp) " & _
             "select t.nomer_order,d.nomer_jual,CONVERT(VARCHAR(10),t.tanggal_jual,111),d.kode_bahan,m.nama_bahan,d.berat,d.harga,(s.totalhpp/d.berat) as hpp,(d.berat*d.harga) as totaljual,s.totalhpp from t_juald d " & _
            "left join t_jualh t on t.nomer_jual=d.nomer_jual " & _
            "left join ms_bahan m on m.kode_bahan=d.kode_bahan " & _
            "left join (select nomer_order,kode_bahan,sum(berat*hpp) as Totalhpp from t_suratjalan_timbang " & _
            "group by nomer_order,kode_bahan) s on s.nomer_order=t.nomer_order and s.kode_bahan=d.kode_bahan " & _
            "where Month(t.tanggal_jual) = '" & Format(DTPicker0, "M") & "' And Year(t.tanggal_jual) = '" & Format(DTPicker0, "YYYY") & "'"
conn.CommitTrans
conn.Close

On Error GoTo err
Dim LogonInfo As String

        With CRPrint
            .reset
            .ReportFileName = reportDir & "\Laporan Penjualan Vs HPP.rpt"
            
            For i = 0 To CRPrint.RetrieveLogonInfo - 1
                .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
                LogonInfo = .LogonInfo(i)
            Next
            paramX = "1"
            If paramX <> "" Then .ParameterFields(3) = "Detail;" & paramX & ";True"
            .Destination = crptToWindow
            .WindowTitle = "Cetak" & PrintMode
            .WindowState = crptMaximized
            .WindowShowPrintBtn = True
            .WindowShowExportBtn = True
    '        Me.Hide
            .action = 1
      
        End With
    
    
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub Form_Load()
Dim sDate As String
    sDate = "01-" & Right("0" & frmLaporanA.cmbBulan.text, 2) & "-" & frmLaporanA.cmbTahun.text
    If IsDate(sDate) = True Then
        DTPicker0.value = sDate
    End If
End Sub
