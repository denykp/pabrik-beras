VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmTransPlatRusak 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Plat Rusak"
   ClientHeight    =   4020
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   6900
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   6900
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3420
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3180
      Width           =   1230
   End
   Begin VB.ComboBox cmbSerial 
      Height          =   360
      Left            =   1530
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1530
      Width           =   1875
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   960
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3180
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4650
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3180
      Width           =   1230
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2190
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3180
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   855
      Left            =   1530
      MaxLength       =   50
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   3
      Top             =   2040
      Width           =   3465
   End
   Begin VB.TextBox txtKode 
      Height          =   345
      Left            =   1530
      MaxLength       =   35
      TabIndex        =   1
      Top             =   750
      Width           =   1725
   End
   Begin VB.CommandButton cmdSearchPlat 
      Caption         =   "F3"
      Height          =   375
      Left            =   3390
      TabIndex        =   9
      Top             =   720
      Width           =   555
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   5220
      TabIndex        =   12
      Top             =   210
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   661
      _Version        =   393216
      Format          =   58064897
      CurrentDate     =   40498
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   375
      Left            =   3390
      TabIndex        =   8
      Top             =   240
      Width           =   555
   End
   Begin VB.Label lblNama 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Plat"
      Height          =   240
      Left            =   1560
      TabIndex        =   21
      Top             =   1170
      Width           =   870
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1380
      TabIndex        =   20
      Top             =   2040
      Width           =   165
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   5070
      TabIndex        =   19
      Top             =   240
      Width           =   165
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1380
      TabIndex        =   18
      Top             =   1530
      Width           =   165
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1380
      TabIndex        =   17
      Top             =   780
      Width           =   165
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   285
      Left            =   1380
      TabIndex        =   16
      Top             =   300
      Width           =   165
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      Height          =   240
      Left            =   300
      TabIndex        =   15
      Top             =   2070
      Width           =   975
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "No.Register"
      Height          =   240
      Left            =   300
      TabIndex        =   14
      Top             =   1560
      Width           =   990
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Plat"
      Height          =   240
      Left            =   300
      TabIndex        =   13
      Top             =   780
      Width           =   795
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      Height          =   240
      Left            =   4290
      TabIndex        =   11
      Top             =   270
      Width           =   690
   End
   Begin VB.Label lblNoTrans 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      Height          =   240
      Left            =   1560
      TabIndex        =   10
      Top             =   300
      Width           =   1275
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "No.ID"
      Height          =   240
      Left            =   300
      TabIndex        =   0
      Top             =   270
      Width           =   465
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu menu2 
         Caption         =   "Reset Form"
      End
      Begin VB.Menu menu 
         Caption         =   "Simpan"
      End
      Begin VB.Menu menu1 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmTransPlatRusak"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim qty_serial As Long
Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPosting_Click()
 If simpan Then
    posting_PlatRusak lblNoTrans
    reset_form
 End If
End Sub

Private Sub cmdReset_Click()
reset_form
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then cmdSearchID_Click
If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub
Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select t.nomer_transaksi,t.tanggal,t.keterangan from t_plat_rusak t where t.status_posting=0"
    frmSearch.nmform = "frmTransPlatrusak"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "platRusakH"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchPlat_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select distinct (a.kode_plat) as kode_plat,a.nama_plat,a.kategori from ms_plat a inner join t_plat_baru b on a.kode_plat=b.kode_plat"
    frmSearch.nmform = "frmTransPlatrusak"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_plat"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_barang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
     cmbSerial.SetFocus
End Sub

Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    If txtKode = "" Then
        MsgBox "Silahkan masukkan Kode plat terlebih dahulu"
        txtKode.SetFocus
        Exit Function
    End If
    If cmbSerial = "" Then
        MsgBox "Silahkan masukkan No.Register terlebih dahulu"
        cmbSerial.SetFocus
        Exit Function
    End If
    
    conn.Open strcon
    conn.BeginTrans
i = 1
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_plat_rusak", "nomer_transaksi", DTPicker1, "PLR")
    Else
        conn.Execute "delete from t_plat_rusak where nomer_transaksi='" & lblNoTrans & "'"
    End If
    
    add_dataheader
   
    
    conn.CommitTrans
i = 0
    DropConnection
    simpan = True
    txtKode.SetFocus
    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(6)
    ReDim nilai(6)
    table_name = "t_plat_rusak"
    fields(0) = "nomer_transaksi"
    fields(1) = "kode_plat"
    fields(2) = "tanggal"
    fields(3) = "nomer_serial"
    fields(4) = "qty"
    fields(5) = "keterangan"

    nilai(0) = lblNoTrans
    nilai(1) = txtKode
    nilai(2) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(3) = cmbSerial.text
    nilai(4) = Replace(qty_serial, ",", ".")
    nilai(5) = txtKeterangan.text
    
    tambah_data table_name, fields, nilai
End Sub

Public Function cari_barang() As Boolean
If txtKode = "" And cari_barang = False Then Exit Function
cari_barang = False
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select a.kode_plat,a.nama_plat,b.qty from ms_plat a inner join t_plat_baru b on  a.kode_plat=b.kode_plat where a.kode_plat='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        cari_barang = True
        lblNama = rs(1)
        qty_serial = rs!qty
    Else
        lblNama = ""
    End If
    rs.Close
    cmbSerial.Clear
    rs.Open "select nomer_serial from serial_plat where kode_plat='" & txtKode & "' ", conn
    While Not rs.EOF
        cmbSerial.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    
End Function

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select a.*,b.nama_plat from t_plat_rusak a inner join ms_plat b on a.kode_plat=b.kode_plat where a.nomer_transaksi='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = Format(rs!tanggal, "dd/MM/yyyy")
        txtKode = rs!kode_plat
        lblNama = rs!nama_plat
        serial = rs!nomer_serial
         If Not IsNull(rs!keterangan) Then txtKeterangan.text = rs!keterangan Else txtKeterangan = ""

        rs.Close
        cmbSerial.Clear
        rs.Open "select nomer_serial from serial_plat where kode_plat='" & txtKode & "' ", conn
        While Not rs.EOF
            cmbSerial.AddItem rs(0)
        rs.MoveNext
        Wend
        rs.Close
        SetComboText serial, cmbSerial
    End If

    conn.Close

End Sub
Private Sub reset_form()
    lblNoTrans = "-"
    txtKode = ""
    cmbSerial.Clear
    lblNama = ""

    txtKeterangan = ""
    DTPicker1 = Format(Now, "dd/MM/yyyy hh:mm:ss")

End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub menu2_Click()
    reset_form
End Sub


Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchPlat_Click
End Sub

Private Sub txtKode_LostFocus()
    If Not cari_barang And txtKode <> "" Then txtKode.SetFocus
End Sub
