VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterCustomer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Customer"
   ClientHeight    =   8295
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12465
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8295
   ScaleWidth      =   12465
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6660
      Picture         =   "frmMasterCustomer.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   85
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   7695
      Picture         =   "frmMasterCustomer.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   84
      Top             =   0
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.ListBox lstKategoriBarang 
      Height          =   2085
      Left            =   1890
      Style           =   1  'Checkbox
      TabIndex        =   12
      Top             =   5640
      Width           =   3375
   End
   Begin VB.ComboBox cmbKategoriBarang 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0A64
      Left            =   13995
      List            =   "frmMasterCustomer.frx":0A66
      TabIndex        =   81
      Top             =   3240
      Width           =   1605
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Pajak"
      Height          =   240
      Left            =   1890
      TabIndex        =   11
      Top             =   5400
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Index           =   12
      Left            =   8505
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Top             =   585
      Width           =   3840
   End
   Begin VB.TextBox txtFile 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6750
      TabIndex        =   23
      Top             =   4680
      Width           =   2280
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "Browse"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9090
      Picture         =   "frmMasterCustomer.frx":0A68
      TabIndex        =   76
      Top             =   4680
      Width           =   915
   End
   Begin VB.CommandButton cmdLoadRpt 
      Caption         =   "Load History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4455
      Picture         =   "frmMasterCustomer.frx":0B6A
      TabIndex        =   75
      Top             =   540
      Width           =   1320
   End
   Begin VB.CommandButton cmdNewKategori 
      Caption         =   "Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5850
      TabIndex        =   74
      Top             =   90
      Width           =   735
   End
   Begin VB.TextBox txtField 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   8505
      TabIndex        =   22
      Text            =   "0"
      Top             =   4275
      Width           =   1815
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   1890
      TabIndex        =   5
      Top             =   2745
      Width           =   3525
   End
   Begin VB.ComboBox cmbDisc 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0C6C
      Left            =   8505
      List            =   "frmMasterCustomer.frx":0C7C
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   3870
      Width           =   1590
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   2
      Left            =   1890
      MaxLength       =   50
      TabIndex        =   2
      Top             =   1305
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   8
      Left            =   1890
      TabIndex        =   8
      Top             =   3825
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   7
      Left            =   1890
      TabIndex        =   7
      Top             =   3465
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   6
      Left            =   1890
      TabIndex        =   6
      Top             =   3105
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Index           =   10
      Left            =   1890
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   4515
      Width           =   4725
   End
   Begin VB.CheckBox chkPPN 
      Caption         =   "PPN"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   14115
      TabIndex        =   20
      Top             =   4320
      Width           =   1230
   End
   Begin VB.ComboBox cmbWilayah2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0C8C
      Left            =   8505
      List            =   "frmMasterCustomer.frx":0C8E
      TabIndex        =   17
      Top             =   2655
      Width           =   3885
   End
   Begin VB.ComboBox cmbWilayah1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0C90
      Left            =   8505
      List            =   "frmMasterCustomer.frx":0C92
      TabIndex        =   16
      Top             =   2250
      Width           =   3885
   End
   Begin VB.ComboBox cmbWilayah 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0C94
      Left            =   8505
      List            =   "frmMasterCustomer.frx":0C96
      TabIndex        =   15
      Top             =   1845
      Width           =   3885
   End
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0C98
      Left            =   1890
      List            =   "frmMasterCustomer.frx":0C9A
      Style           =   2  'Dropdown List
      TabIndex        =   50
      Top             =   90
      Width           =   3885
   End
   Begin VB.ComboBox cmbStatus 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterCustomer.frx":0C9C
      Left            =   8505
      List            =   "frmMasterCustomer.frx":0C9E
      TabIndex        =   14
      Top             =   1440
      Width           =   3885
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   9
      Left            =   1890
      TabIndex        =   9
      Top             =   4185
      Width           =   3795
   End
   Begin VB.CommandButton cmdSearchAccHutang 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10530
      TabIndex        =   42
      Top             =   3495
      Width           =   465
   End
   Begin VB.TextBox txtAccHutang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8505
      MaxLength       =   25
      TabIndex        =   19
      Top             =   3465
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearchAccPiutang 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10530
      TabIndex        =   40
      Top             =   3090
      Width           =   465
   End
   Begin VB.TextBox txtAccPiutang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8505
      MaxLength       =   25
      TabIndex        =   18
      Top             =   3105
      Width           =   1950
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   3375
      Picture         =   "frmMasterCustomer.frx":0CA0
      TabIndex        =   26
      Top             =   7815
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterCustomer.frx":0DA2
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   540
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   1890
      TabIndex        =   4
      Top             =   2355
      Width           =   3525
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Index           =   3
      Left            =   1890
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   1665
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      MaxLength       =   50
      TabIndex        =   1
      Top             =   945
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      MaxLength       =   25
      TabIndex        =   0
      Top             =   540
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4635
      Picture         =   "frmMasterCustomer.frx":0EA4
      TabIndex        =   27
      Top             =   7815
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2115
      Picture         =   "frmMasterCustomer.frx":0FA6
      TabIndex        =   25
      Top             =   7815
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   855
      Picture         =   "frmMasterCustomer.frx":10A8
      TabIndex        =   24
      Top             =   7815
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   11790
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblAccHutRetur_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   87
      Top             =   3465
      Width           =   105
   End
   Begin VB.Label lblAccPiutang_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   86
      Top             =   3105
      Width           =   105
   End
   Begin VB.Label lblKatBarang_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   83
      Top             =   5640
      Width           =   105
   End
   Begin VB.Label lblKatBarang_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   120
      TabIndex        =   82
      Top             =   5640
      Width           =   1545
   End
   Begin VB.Label lblPajak_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Pajak - Non Pajak"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   80
      Top             =   5355
      Width           =   1545
   End
   Begin VB.Label lblPajak_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   79
      Top             =   5355
      Width           =   105
   End
   Begin VB.Label lblAtasNama_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   78
      Top             =   630
      Width           =   105
   End
   Begin VB.Label lblAtasNama_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Atas Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   77
      Top             =   630
      Width           =   1320
   End
   Begin VB.Image Image1 
      Height          =   2535
      Left            =   6750
      Stretch         =   -1  'True
      Top             =   5130
      Width           =   4470
   End
   Begin VB.Label lblLimit_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   73
      Top             =   4305
      Width           =   105
   End
   Begin VB.Label lblLimit_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Limit Kredit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   72
      Top             =   4305
      Width           =   1320
   End
   Begin VB.Label lblHP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   71
      Top             =   2775
      Width           =   105
   End
   Begin VB.Label lblHP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "HP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   70
      Top             =   2745
      Width           =   1320
   End
   Begin VB.Label lblKatDisc_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori Disc"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   69
      Top             =   3900
      Width           =   1320
   End
   Begin VB.Label lblKatDisc_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   68
      Top             =   3900
      Width           =   105
   End
   Begin VB.Label lblNamaT_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Toko"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   67
      Top             =   1350
      Width           =   1320
   End
   Begin VB.Label lblNamaT_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   66
      Top             =   1350
      Width           =   105
   End
   Begin VB.Label lblNamaT_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   65
      Top             =   1350
      Width           =   150
   End
   Begin VB.Label lblWebsite_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   64
      Top             =   3855
      Width           =   105
   End
   Begin VB.Label lblWebsite_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Website"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   63
      Top             =   3855
      Width           =   1320
   End
   Begin VB.Label lblEmail_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   62
      Top             =   3495
      Width           =   105
   End
   Begin VB.Label lblEmail_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Email"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   61
      Top             =   3495
      Width           =   1320
   End
   Begin VB.Label lblFax_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   60
      Top             =   3135
      Width           =   105
   End
   Begin VB.Label lblFax_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   59
      Top             =   3135
      Width           =   1320
   End
   Begin VB.Label lblKeterangan_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   58
      Top             =   4560
      Width           =   105
   End
   Begin VB.Label lblKeterangan_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   57
      Top             =   4560
      Width           =   1320
   End
   Begin VB.Label lblWilayah3_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   56
      Top             =   2685
      Width           =   105
   End
   Begin VB.Label lblWilayah3_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   55
      Top             =   2685
      Width           =   1320
   End
   Begin VB.Label lblWilayah2_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   54
      Top             =   2280
      Width           =   105
   End
   Begin VB.Label lblWilayah2_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   53
      Top             =   2280
      Width           =   1320
   End
   Begin VB.Label lblWilayah1_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Wilayah 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   52
      Top             =   1875
      Width           =   1320
   End
   Begin VB.Label lblWilayah1_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   51
      Top             =   1875
      Width           =   105
   End
   Begin VB.Label lblKategori_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   49
      Top             =   120
      Width           =   105
   End
   Begin VB.Label lblKategori_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   48
      Top             =   120
      Width           =   1320
   End
   Begin VB.Label lblStatus_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8280
      TabIndex        =   47
      Top             =   1470
      Width           =   105
   End
   Begin VB.Label lblStatus_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   46
      Top             =   1470
      Width           =   1320
   End
   Begin VB.Label lblNPWP_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   45
      Top             =   4215
      Width           =   105
   End
   Begin VB.Label lblNPWP_1 
      BackStyle       =   0  'Transparent
      Caption         =   "NPWP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   44
      Top             =   4215
      Width           =   1320
   End
   Begin VB.Label lblAccHutRetur_1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Hutang Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   43
      Top             =   3495
      Width           =   1485
   End
   Begin VB.Label lblAccPiutang_1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Acc Piutang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   41
      Top             =   3135
      Width           =   990
   End
   Begin VB.Label lblNamaM_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   38
      Top             =   990
      Width           =   150
   End
   Begin VB.Label lblKode_2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   37
      Top             =   585
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   60
      TabIndex        =   36
      Top             =   7440
      Width           =   1275
   End
   Begin VB.Label lblTelp_1 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   35
      Top             =   2385
      Width           =   1320
   End
   Begin VB.Label lblTelp_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   34
      Top             =   2400
      Width           =   105
   End
   Begin VB.Label lblAlamat_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   33
      Top             =   1710
      Width           =   1320
   End
   Begin VB.Label lblAlamat_2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   32
      Top             =   1710
      Width           =   105
   End
   Begin VB.Label lblNamaM_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   31
      Top             =   990
      Width           =   105
   End
   Begin VB.Label lblKode_3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   30
      Top             =   585
      Width           =   105
   End
   Begin VB.Label lblNamaM_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Member"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   29
      Top             =   990
      Width           =   1320
   End
   Begin VB.Label lblKode_1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Customer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   28
      Top             =   585
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterCustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Check1_Click()
    If Check1.value = 1 Then
        lstKategoriBarang.Enabled = True
    Else
        lstKategoriBarang.Enabled = False
    End If
End Sub

Private Sub cmbKategori_Click()
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open strcon

    rs.Open "select * from var_kategoricustomer where kategori='" & cmbKategori.text & "'", conn
    If Not rs.EOF Then
        txtAccPiutang = rs!Acc_piutang
        txtAccHutang = rs!acc_Hutangretur
        SetComboText rs!kategori_disc, cmbDisc
    End If
    rs.Close

conn.Close
End Sub


Private Sub cmbWilayah_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbWilayah1.Clear
    rs.Open "select distinct wilayah1 from ms_customer where wilayah='" & cmbWilayah.text & "' order by wilayah1", conn
    While Not rs.EOF
        cmbWilayah1.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub



Private Sub cmbWilayah1_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbWilayah2.Clear
    rs.Open "select distinct wilayah2 from ms_customer where wilayah='" & cmbWilayah.text & "' and wilayah1='" & cmbWilayah1.text & "' order by wilayah2", conn
    While Not rs.EOF
        cmbWilayah2.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub cmdBrowse_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.jpg|*.jpg"
    CommonDialog1.filename = "Image Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        Set Image1.Picture = LoadPicture(CommonDialog1.filename)
        txtFile = CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
Dim rs As New ADODB.Recordset
Dim hapus As Boolean
On Error GoTo err
    i = 0
    hapus = False
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    rs.Open "select  * from t_jualh where kode_customer='" & txtField(0).text & "' ", conn
    If rs.EOF Then
        conn.Execute "delete from ms_customer where kode_customer='" & txtField(0).text & "'"
        conn.Execute "delete from setting_acccustomer where kode_customer='" & txtField(0).text & "'"
        hapus = True
    End If
    rs.Close
    conn.CommitTrans
    i = 0
    If hapus Then
        MsgBox "Data sudah dihapus"
    Else
        MsgBox "Data sudah dipakai untuk penjualan, tidak boleh dihapus !", vbExclamation
    End If
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoadRpt_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchRptCustomer
    frmSearch.nmform = "frmMasterCustomer"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_customer"
    frmSearch.proc = "cari_data_rpt"
    frmSearch.Col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    frmSearch.Show vbModal
End Sub

Private Sub cmdNewKategori_Click()
    frmMasterKategoriCustomer.Show vbModal
    load_combo1
End Sub
Private Sub load_combo1()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbKategori.Clear
    rs.Open "select * from var_kategoricustomer order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
End Sub

Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_customer] from ms_customer where [kode_customer]>'" & txtField(0) & "' order by [kode_customer]", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_customer] from ms_customer where [kode_customer]<'" & txtField(0) & "' order by [kode_customer] desc", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
'    If User <> "sugik" Then
'        SearchCustomer = "select kode_customer,nama_customer,nama_toko,alamat from ms_customer"
'    End If
    frmSearch.query = SearchCustomer
    frmSearch.nmform = "frmMasterCustomer"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_customer"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.Col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccHutang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='210.010'"
    frmSearch.nmform = "frmMastercustomer"
    frmSearch.nmctrl = "txtAccHutang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchAccPiutang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa where fg_aktif = 1 and kode_subgrup_acc='110.030'"
    frmSearch.nmform = "frmMastercustomer"
    frmSearch.nmctrl = "txtAccpiutang"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal

    
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    For J = 0 To 1
     If txtField(J).text = "" Then
         MsgBox "Semua field yang bertanda * harus diisi"
         txtField(J).SetFocus
         Exit Sub
     End If
    Next
    
    If txtAccPiutang.text = "" Then
        MsgBox "Acc Piutang harus diisi !", vbExclamation
        txtAccPiutang.SetFocus
        Exit Sub
    End If
    
    If txtAccHutang.text = "" Then
        MsgBox "Acc Hutang Retur harus diisi !", vbExclamation
        txtAccHutang.SetFocus
        Exit Sub
    End If

    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    rs.Open " select * from ms_customer where kode_customer='" & txtField(0) & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_customer where kode_customer='" & txtField(0).text & "'"
        conn.Execute "delete from setting_accCustomer where kode_customer='" & txtField(0).text & "'"
'        MsgBox "1"
        add_dataacc 2, conn
    Else
'        MsgBox "2"
        add_dataacc 1, conn
    End If
    rs.Close
'    MsgBox "3"
    add_dataheader
    Dim nourut As Integer
    nourut = 1
    conn.Execute "delete from kategori_barang_customer where kode_customer = '" & txtField(0) & "'"
    If lstKategoriBarang.ListCount > 0 Then
        For J = 1 To lstKategoriBarang.ListCount - 1
            If lstKategoriBarang.Selected(J) = True Then
                conn.Execute "insert into kategori_barang_customer values('" & txtField(0) & "','" & lstKategoriBarang.List(J) & "'," & nourut & " )"
                nourut = nourut + 1
            End If
        Next
    End If
'    MsgBox "4"
    conn.Execute "delete from rpt_ms_customer where kode_customer='" & txtField(0).text & "'"
    add_dataheaderRpt
'    MsgBox "5"
'    If txtFile <> "" Then
'        rs.Open "select * from ms_customer where [kode_customer]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
'        Dim S As New ADODB.Stream
'        With S
'            .Type = adTypeBinary
'            .Open
'            .LoadFromFile txtFile
'            rs("foto").value = .Read
'            rs.Update
'        End With
'        rs.Close
'        rs.Open "select * from rpt_ms_customer where [kode_customer]='" & txtField(0).text & "'", conn, adOpenKeyset, adLockOptimistic
'        rs("foto").value = S.Read
'        Set S = Nothing
'    End If
    
    
    
    conn.CommitTrans
    
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(4).text = ""
    txtField(5).text = ""
    txtField(6).text = ""
    txtField(7).text = ""
    txtField(8).text = ""
    txtField(9).text = ""
    txtField(10).text = ""
    txtField(11).text = "0"
    txtField(0).SetFocus
    cmbStatus.text = ""
    cmbWilayah.text = ""
    cmbWilayah1.text = ""
    cmbWilayah2.text = ""
    txtFile = ""
    Image1.Picture = Nothing
    chkPPN.value = True
    Check1.value = 0
    txtAccPiutang.text = ""
    txtAccHutang.text = ""
    load_combo
    lstKategoriBarang.Enabled = False
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
End Sub

Public Sub cari_data_rpt()
    cari_data "rpt_"
End Sub

Public Sub cari_data(Optional prefix As String)
On Error Resume Next
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select *  from " & prefix & "ms_customer  " & _
            "where kode_customer='" & txtField(0).text & "' ", conn
    
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        txtField(4).text = rs(4)
        txtField(5).text = rs(5)
        txtField(6).text = rs(6)
        txtField(7).text = rs(7)
        txtField(8).text = rs(8)
        txtField(9).text = rs!npwp
        txtField(10).text = rs!keterangan
        txtField(11).text = rs!limitkredit
        txtField(12).text = rs!atasnama
        cmbStatus.text = rs!status
        cmbWilayah.text = rs!wilayah
        cmbWilayah1.text = rs!wilayah1
        cmbWilayah2.text = rs!wilayah2
        SetComboText rs!kategori, cmbKategori
        chkPPN.value = IIf(rs!ppn, 1, 0)
        SetComboText rs!kategori_disc, cmbDisc
        Check1.value = IIf(rs!pajak = "1", 1, 0)
        cmbKategoriBarang.text = rs!kategori_barang
        Dim mStream As New ADODB.Stream
            
            Dim fs As New Scripting.FileSystemObject
            
            fs.DeleteFile App.Path & "\temp.jpg"
            
          With mStream
            .Type = adTypeBinary
            .Open
            .Write rs("foto").value
            .SaveToFile App.Path & "\temp.jpg"
            Image1.Picture = LoadPicture(App.Path & "\temp.jpg")
            txtFile = App.Path & "\temp.jpg"
          End With
        
          Set mStream = Nothing
        rs.Close
        rs.Open "select *  from setting_acccustomer  " & _
            "where kode_customer='" & txtField(0).text & "' ", conn
        If Not rs.EOF Then
            txtAccHutang = rs!acc_Hutangretur
            txtAccPiutang = rs!Acc_piutang
        End If
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    
    rs.Open "select * from kategori_barang_customer where kode_customer = '" & txtField(0) & "'", conn
    While Not rs.EOF
        For J = 1 To lstKategoriBarang.ListCount - 1
            If lstKategoriBarang.List(J) = rs!kategori Then
                lstKategoriBarang.Selected(J) = True
            End If
        Next
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    load_combo
    reset_form
    changewebsite
    altercustomer
    security_setting
    If Not right_deletemaster Then cmdHapus.Visible = False
End Sub
Private Sub load_combo()
    altercustomer
    conn.ConnectionString = strcon
    conn.Open
    
    cmbKategori.Clear
    rs.Open "select * from var_kategoricustomer order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbStatus.Clear
    rs.Open "select distinct status from ms_customer order by status", conn
    While Not rs.EOF
        cmbStatus.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbWilayah.Clear
    rs.Open "select distinct wilayah from ms_customer order by wilayah", conn
    While Not rs.EOF
        cmbWilayah.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    lstKategoriBarang.Clear
    rs.Open "select distinct kategori from ms_bahan order by kategori", conn
    While Not rs.EOF
        lstKategoriBarang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    If cmbKategori.ListCount > 0 Then cmbKategori.ListIndex = 0
    
    conn.Close
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index))
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 And txtField(0).text <> "" Then cari_data
End Sub

Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(21)
    ReDim nilai(21)
    
    table_name = "ms_customer"
    fields(0) = "kode_customer"
    fields(1) = "nama_customer"
    fields(2) = "nama_toko"
    fields(3) = "alamat"
    fields(4) = "telp"
    fields(5) = "hp"
    fields(6) = "fax"
    fields(7) = "email"
    fields(8) = "website"
    fields(9) = "keterangan"
    fields(10) = "kategori"
    fields(11) = "status"
    fields(12) = "npwp"
    fields(13) = "wilayah"
    fields(14) = "wilayah1"
    fields(15) = "wilayah2"
    fields(16) = "ppn"
    fields(17) = "kategori_disc"
    fields(18) = "limitkredit"
    fields(19) = "atasnama"
    fields(20) = "pajak"
    
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = txtField(2).text
    nilai(3) = txtField(3).text
    nilai(4) = txtField(4).text
    nilai(5) = txtField(5).text
    nilai(6) = txtField(6).text
    nilai(7) = txtField(7).text
    nilai(8) = txtField(8).text
    nilai(9) = txtField(10).text
    nilai(10) = Trim(cmbKategori.text)
    nilai(11) = Trim(cmbStatus.text)
    nilai(12) = txtField(9).text
    nilai(13) = cmbWilayah.text
    nilai(14) = cmbWilayah1.text
    nilai(15) = cmbWilayah2.text
    nilai(16) = chkPPN.value
    nilai(17) = cmbDisc.text
    nilai(18) = txtField(11).text
    nilai(19) = txtField(12).text
    nilai(20) = IIf(Check1.value = 1, "1", "0")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub add_dataheaderRpt()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(18)
    ReDim nilai(18)
    
    table_name = "Rpt_ms_customer"
    
    fields(0) = "kode_customer"
    fields(1) = "nama_customer"
    fields(2) = "nama_toko"
    fields(3) = "alamat"
    fields(4) = "telp"
    fields(5) = "hp"
    fields(6) = "fax"
    fields(7) = "email"
    fields(8) = "website"
    fields(9) = "keterangan"
    fields(10) = "kategori"
    fields(11) = "status"
    fields(12) = "npwp"
    fields(13) = "wilayah"
    fields(14) = "wilayah1"
    fields(15) = "wilayah2"
    fields(16) = "ppn"
    fields(17) = "atasnama"
    
    nilai(0) = txtField(0).text
    nilai(1) = txtField(1).text
    nilai(2) = txtField(2).text
    nilai(3) = txtField(3).text
    nilai(4) = txtField(4).text
    nilai(5) = txtField(5).text
    nilai(6) = txtField(6).text
    nilai(7) = txtField(7).text
    nilai(8) = txtField(8).text
    nilai(9) = txtField(10).text
    nilai(10) = Trim(cmbKategori.text)
    nilai(11) = Trim(cmbStatus.text)
    nilai(12) = txtField(9).text
    nilai(13) = cmbWilayah.text
    nilai(14) = cmbWilayah1.text
    nilai(15) = cmbWilayah2.text
    nilai(16) = chkPPN.value
    nilai(17) = txtField(12).text
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_dataacc(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(3)
    ReDim nilai(3)

    table_name = "setting_accCustomer"
    fields(0) = "kode_customer"
    fields(1) = "acc_piutang"
    fields(2) = "acc_hutangRetur"
    
    nilai(0) = txtField(0).text
    nilai(1) = txtAccPiutang
    nilai(2) = txtAccHutang
'    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
'    Else
'    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
'    End If
End Sub

Private Sub security_setting()
    Dim kolom As String
    Dim split_kolom() As String
    
    conn.Open strcon
    rs.Open "select * from user_groupquery where nama_group='" & usergroup & "' and nama_table='ms_customer'", conn
    If Not rs.EOF Then
        If rs!columnlist <> "" Then
            kolom = rs!columnlist
            txtField(0).Visible = False
            lblKode_1.Visible = False
            lblKode_2.Visible = False
            lblKode_3.Visible = False
        
            txtField(1).Visible = False
            lblNamaM_1.Visible = False
            lblNamaM_2.Visible = False
            lblNamaM_3.Visible = False
        
            txtField(2).Visible = False
            lblNamaT_1.Visible = False
            lblNamaT_2.Visible = False
            lblNamaT_3.Visible = False
        
            txtField(3).Visible = False
            lblAlamat_1.Visible = False
            lblAlamat_2.Visible = False
        
            txtField(4).Visible = False
            lblTelp_1.Visible = False
            lblTelp_2.Visible = False
        
            txtField(5).Visible = False
            lblHP_1.Visible = False
            lblHP_2.Visible = False
        
            txtField(6).Visible = False
            lblFax_1.Visible = False
            lblFax_2.Visible = False
        
            txtField(7).Visible = False
            lblEmail_1.Visible = False
            lblEmail_2.Visible = False
        
            txtField(8).Visible = False
            lblWebsite_1.Visible = False
            lblWebsite_2.Visible = False
        
            txtField(10).Visible = False
            lblKeterangan_1.Visible = False
            lblKeterangan_2.Visible = False
        
            cmbKategori.Visible = False
            lblKategori_1.Visible = False
            lblKategori_2.Visible = False
        
            cmbStatus.Visible = False
            lblStatus_1.Visible = False
            lblStatus_2.Visible = False
        
            txtField(9).Visible = False
            lblNPWP_1.Visible = False
            lblNPWP_2.Visible = False
        
            cmbWilayah.Visible = False
            lblWilayah1_1.Visible = False
            lblWilayah1_2.Visible = False
        
            cmbWilayah1.Visible = False
            lblWilayah2_1.Visible = False
            lblWilayah2_2.Visible = False
        
            cmbWilayah2.Visible = False
            lblWilayah3_1.Visible = False
            lblWilayah3_2.Visible = False
            
            
            
            cmbDisc.Visible = False
            lblKatDisc_1.Visible = False
            lblKatDisc_2.Visible = False
        
            txtField(11).Visible = False
            lblLimit_1.Visible = False
            lblLimit_2.Visible = False
        
            txtField(12).Visible = False
            lblAtasNama_1.Visible = False
            lblAtasNama_2.Visible = False
        
            Check1.Visible = False
            lblPajak_1.Visible = False
            lblPajak_2.Visible = False
            
            txtFile.Visible = False
            cmdBrowse.Visible = False
            Image1.Visible = False
        End If
    End If
    rs.Close
    split_kolom = Split(kolom, ",")
    For Each Column In split_kolom
        Select Case LCase(Column)
        Case "kode_customer"
            txtField(0).Visible = True
            lblKode_1.Visible = True
            lblKode_2.Visible = True
            lblKode_3.Visible = True
        Case "nama_customer"
            txtField(1).Visible = True
            lblNamaM_1.Visible = True
            lblNamaM_2.Visible = True
            lblNamaM_3.Visible = True
        Case "nama_toko"
            txtField(2).Visible = True
            lblNamaT_1.Visible = True
            lblNamaT_2.Visible = True
            lblNamaT_3.Visible = True
        Case "alamat"
            txtField(3).Visible = True
            lblAlamat_1.Visible = True
            lblAlamat_2.Visible = True
        Case "telp"
            txtField(4).Visible = True
            lblTelp_1.Visible = True
            lblTelp_2.Visible = True
        Case "hp"
            txtField(5).Visible = True
            lblHP_1.Visible = True
            lblHP_2.Visible = True
        Case "fax"
            txtField(6).Visible = True
            lblFax_1.Visible = True
            lblFax_2.Visible = True
        Case "email"
            txtField(7).Visible = True
            lblEmail_1.Visible = True
            lblEmail_2.Visible = True
        Case "website"
            txtField(8).Visible = True
            lblWebsite_1.Visible = True
            lblWebsite_2.Visible = True
        Case "keterangan"
            txtField(10).Visible = True
            lblKeterangan_1.Visible = True
            lblKeterangan_2.Visible = True
        Case "kategori"
            cmbKategori.Visible = True
            lblKategori_1.Visible = True
            lblKategori_2.Visible = True
        Case "status"
            cmbStatus.Visible = True
            lblStatus_1.Visible = True
            lblStatus_2.Visible = True
        Case "npwp"
            txtField(9).Visible = True
            lblNPWP_1.Visible = True
            lblNPWP_2.Visible = True
        Case "wilayah"
            cmbWilayah.Visible = True
            lblWilayah1_1.Visible = True
            lblWilayah1_2.Visible = True
        Case "wilayah1"
            cmbWilayah1.Visible = True
            lblWilayah2_1.Visible = True
            lblWilayah2_2.Visible = True
        Case "wilayah2"
            cmbWilayah2.Visible = True
            lblWilayah3_1.Visible = True
            lblWilayah3_2.Visible = True
        Case "ppn"
            
        Case "kategori_disc"
            cmbDisc.Visible = True
            lblKatDisc_1.Visible = True
            lblKatDisc_2.Visible = True
        Case "limitkredit"
            txtField(11).Visible = True
            lblLimit_1.Visible = True
            lblLimit_2.Visible = True
        Case "atasnama"
            txtField(12).Visible = True
            lblAtasNama_1.Visible = True
            lblAtasNama_2.Visible = True
        Case "pajak"
            Check1.Visible = True
            lblPajak_1.Visible = True
            lblPajak_2.Visible = True
        Case "foto"
            txtFile.Visible = True
            cmdBrowse.Visible = True
            Image1.Visible = True
        End Select
    Next
    conn.Close
End Sub
