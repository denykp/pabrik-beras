VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmLaporanA 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Out Laporan"
   ClientHeight    =   8175
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10620
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8175
   ScaleWidth      =   10620
   ShowInTaskbar   =   0   'False
   Begin VB.Frame frKaryawan 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   5400
      TabIndex        =   112
      Top             =   1485
      Visible         =   0   'False
      Width           =   4230
      Begin VB.CommandButton cmdSearchKaryawan 
         Caption         =   "..."
         Height          =   375
         Left            =   3600
         TabIndex        =   116
         Top             =   90
         Width           =   420
      End
      Begin VB.TextBox txtNIK 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   113
         Top             =   120
         Width           =   2220
      End
      Begin VB.Label Label34 
         Caption         =   "NIK"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   115
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label33 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   114
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame FrLabaRugi 
      Height          =   1305
      Left            =   4620
      TabIndex        =   107
      Top             =   5025
      Visible         =   0   'False
      Width           =   1515
      Begin VB.OptionButton OptLR 
         Caption         =   "Toko"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   90
         TabIndex        =   110
         Top             =   885
         Width           =   1275
      End
      Begin VB.OptionButton OptLR 
         Caption         =   "Pabrik"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   90
         TabIndex        =   109
         Top             =   540
         Width           =   1305
      End
      Begin VB.OptionButton OptLR 
         Caption         =   "Semua"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   108
         Top             =   210
         Value           =   -1  'True
         Width           =   1245
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   7845
      Top             =   5730
   End
   Begin VB.CommandButton CmdHitung 
      Caption         =   "Hitung Ulang Stock"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   315
      TabIndex        =   102
      Top             =   7260
      Width           =   3150
   End
   Begin VB.CommandButton bJualHPP 
      Caption         =   "Laporan Penjualan Vs HPP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   300
      TabIndex        =   101
      Top             =   6465
      Visible         =   0   'False
      Width           =   4140
   End
   Begin VB.Frame Frame1 
      Caption         =   "Jenis Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1290
      Left            =   1650
      TabIndex        =   97
      Top             =   2940
      Visible         =   0   'False
      Width           =   2910
      Begin VB.OptionButton OptTanggal 
         Caption         =   "Tgl. Jatuh Tempo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   150
         TabIndex        =   100
         Top             =   915
         Width           =   2670
      End
      Begin VB.OptionButton OptTanggal 
         Caption         =   "Tgl. Pembelian / Penjualan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   150
         TabIndex        =   99
         Top             =   615
         Width           =   2505
      End
      Begin VB.OptionButton OptTanggal 
         Caption         =   "Tgl. Pembayaran"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   150
         TabIndex        =   98
         Top             =   315
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.CheckBox cHarga 
      Caption         =   "Dengan Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7095
      TabIndex        =   96
      Top             =   4560
      Width           =   1545
   End
   Begin VB.CheckBox chkActiveStock 
      Caption         =   "Active Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7185
      TabIndex        =   95
      Top             =   1545
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.Frame frNoTrans 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   88
      Top             =   5220
      Visible         =   0   'False
      Width           =   3780
      Begin VB.TextBox txtNoTrans 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   89
         Top             =   120
         Width           =   2220
      End
      Begin VB.Label Label30 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   91
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label4 
         Caption         =   "No Trans"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   90
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame frMesin 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   105
      TabIndex        =   75
      Top             =   4800
      Visible         =   0   'False
      Width           =   4440
      Begin VB.CommandButton cmdSearchMesin 
         Caption         =   "..."
         Height          =   375
         Left            =   3630
         TabIndex        =   77
         Top             =   30
         Width           =   420
      End
      Begin VB.TextBox txtMesin 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   76
         Top             =   45
         Width           =   2325
      End
      Begin VB.Label Label29 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   79
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label28 
         Caption         =   "Mesin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   150
         TabIndex        =   78
         Top             =   90
         Width           =   870
      End
   End
   Begin VB.Frame frOperator 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   135
      TabIndex        =   70
      Top             =   4335
      Visible         =   0   'False
      Width           =   4440
      Begin VB.TextBox txtOpr 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   72
         Top             =   45
         Width           =   2325
      End
      Begin VB.CommandButton cmdOperator 
         Caption         =   "..."
         Height          =   375
         Left            =   3630
         TabIndex        =   71
         Top             =   30
         Width           =   420
      End
      Begin VB.Label Label27 
         Caption         =   "Operator"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   74
         Top             =   90
         Width           =   870
      End
      Begin VB.Label Label26 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   73
         Top             =   90
         Width           =   105
      End
   End
   Begin VB.Frame FrBahanHasil 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   120
      TabIndex        =   64
      Top             =   3375
      Visible         =   0   'False
      Width           =   4080
      Begin VB.CommandButton cmdSearchHsl 
         Caption         =   "..."
         Height          =   375
         Left            =   3600
         TabIndex        =   69
         Top             =   0
         Width           =   420
      End
      Begin VB.TextBox txtKodeHsl 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   65
         Top             =   45
         Width           =   2325
      End
      Begin VB.Label Label23 
         Caption         =   "Kode Hasil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   30
         TabIndex        =   67
         Top             =   90
         Width           =   1005
      End
      Begin VB.Label Label22 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   66
         Top             =   90
         Width           =   105
      End
   End
   Begin VB.Frame frStatus 
      Height          =   945
      Left            =   5895
      TabIndex        =   61
      Top             =   360
      Visible         =   0   'False
      Width           =   2325
      Begin VB.CheckBox ckFinish 
         Caption         =   "Status Finish"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   63
         Top             =   510
         Value           =   1  'Checked
         Width           =   1755
      End
      Begin VB.CheckBox ckPosting 
         Caption         =   "Status Posting"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   62
         Top             =   120
         Value           =   1  'Checked
         Width           =   1755
      End
   End
   Begin VB.Frame frKodePlat 
      BorderStyle     =   0  'None
      Height          =   1020
      Left            =   8400
      TabIndex        =   53
      Top             =   7170
      Visible         =   0   'False
      Width           =   4125
      Begin VB.CommandButton Command1 
         Caption         =   "..."
         Height          =   375
         Left            =   3420
         TabIndex        =   60
         Top             =   120
         Width           =   420
      End
      Begin VB.TextBox txtNoSerialPlate 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         Locked          =   -1  'True
         TabIndex        =   58
         Top             =   525
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdSearchSerial 
         BackColor       =   &H8000000C&
         Caption         =   "..."
         Height          =   360
         Left            =   3420
         Picture         =   "frmLaporanA.frx":0000
         TabIndex        =   57
         Top             =   540
         Width           =   420
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1530
         TabIndex        =   54
         Top             =   135
         Visible         =   0   'False
         Width           =   1545
      End
      Begin VB.Label Label21 
         Caption         =   "No. Serial Plat :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   59
         Top             =   555
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.Label Label20 
         Caption         =   "Plat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   56
         Top             =   180
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label Label19 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1380
         TabIndex        =   55
         Top             =   180
         Visible         =   0   'False
         Width           =   105
      End
   End
   Begin VB.CheckBox chkGroupby 
      Caption         =   "Group By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4860
      TabIndex        =   15
      Top             =   4635
      Width           =   1005
   End
   Begin VB.Frame frGroup 
      Height          =   1515
      Left            =   4860
      TabIndex        =   45
      Top             =   4860
      Visible         =   0   'False
      Width           =   2445
      Begin VB.OptionButton optGroup 
         Caption         =   "per Customer per Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   45
         TabIndex        =   19
         Top             =   1080
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Item per Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   45
         TabIndex        =   18
         Top             =   765
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   45
         TabIndex        =   17
         Top             =   450
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   30
         TabIndex        =   16
         Top             =   150
         Width           =   2310
      End
   End
   Begin VB.Frame frSupCus 
      BorderStyle     =   0  'None
      Height          =   2385
      Left            =   4530
      TabIndex        =   41
      Top             =   2025
      Visible         =   0   'False
      Width           =   5955
      Begin VB.TextBox txtSupCus 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1485
         MaxLength       =   25
         TabIndex        =   105
         Top             =   150
         Width           =   2310
      End
      Begin VB.CommandButton cmdSearch 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3855
         Picture         =   "frmLaporanA.frx":0102
         Style           =   1  'Graphical
         TabIndex        =   104
         Top             =   150
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.ComboBox cmbKategori 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmLaporanA.frx":0204
         Left            =   1260
         List            =   "frmLaporanA.frx":020E
         TabIndex        =   92
         Top             =   1665
         Width           =   1320
      End
      Begin VB.ListBox cmbSupCus 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1380
         MultiSelect     =   1  'Simple
         TabIndex        =   87
         Top             =   1200
         Visible         =   0   'False
         Width           =   2895
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2790
         ScaleHeight     =   375
         ScaleWidth      =   465
         TabIndex        =   42
         Top             =   135
         Width           =   465
      End
      Begin VB.Label LabelSupCus 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   1500
         TabIndex        =   106
         Top             =   525
         Width           =   4395
      End
      Begin VB.Label Label32 
         Caption         =   "Kategori"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   94
         Top             =   1710
         Width           =   825
      End
      Begin VB.Label Label31 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1125
         TabIndex        =   93
         Top             =   1710
         Width           =   105
      End
      Begin VB.Label lblSupCus 
         Caption         =   "Supplier"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   44
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label16 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   43
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame frKodeBarang 
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   120
      TabIndex        =   37
      Top             =   2505
      Visible         =   0   'False
      Width           =   4440
      Begin VB.ComboBox cmbSerial 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   80
         Top             =   450
         Width           =   2355
      End
      Begin VB.CommandButton cmdSearchKdBrg 
         Caption         =   "..."
         Height          =   375
         Left            =   3630
         TabIndex        =   68
         Top             =   30
         Width           =   420
      End
      Begin VB.TextBox txtKdBarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1275
         TabIndex        =   4
         Top             =   45
         Width           =   2325
      End
      Begin VB.Label Label24 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1095
         TabIndex        =   82
         Top             =   480
         Width           =   105
      End
      Begin VB.Label Label25 
         Caption         =   "No.Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   105
         TabIndex        =   81
         Top             =   480
         Width           =   870
      End
      Begin VB.Label Label15 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1140
         TabIndex        =   39
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Kode Bahan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   90
         TabIndex        =   38
         Top             =   90
         Width           =   975
      End
   End
   Begin VB.Frame frUserID 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   3375
      TabIndex        =   34
      Top             =   855
      Visible         =   0   'False
      Width           =   2310
      Begin VB.ComboBox cmbUserID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   810
         TabIndex        =   14
         Top             =   135
         Width           =   1320
      End
      Begin VB.Label Label13 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   675
         TabIndex        =   36
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label12 
         Caption         =   "User"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   35
         Top             =   180
         Width           =   510
      End
   End
   Begin VB.Frame frMember 
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   105
      TabIndex        =   31
      Top             =   5805
      Visible         =   0   'False
      Width           =   3435
      Begin VB.CommandButton cmdSearchCustomer 
         Caption         =   "..."
         Height          =   375
         Left            =   2745
         TabIndex        =   46
         Top             =   90
         Width           =   420
      End
      Begin VB.TextBox txtKodeCustomer 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   6
         Top             =   135
         Width           =   1545
      End
      Begin VB.Label Label11 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   33
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label10 
         Caption         =   "Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   32
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.OptionButton OptTipe 
      Caption         =   "per Tahun"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   2
      Left            =   180
      TabIndex        =   11
      Top             =   810
      Width           =   2715
   End
   Begin VB.Frame frTanggal 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   24
      Top             =   1530
      Width           =   4785
      Begin MSComCtl2.DTPicker DTDari 
         Height          =   330
         Left            =   1260
         TabIndex        =   0
         Top             =   90
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   127729667
         CurrentDate     =   38986
      End
      Begin MSComCtl2.DTPicker DTSampai 
         Height          =   330
         Left            =   3150
         TabIndex        =   1
         Top             =   90
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   127729667
         CurrentDate     =   38986
      End
      Begin VB.Label Label1 
         Caption         =   "Tanggal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   27
         Top             =   135
         Width           =   780
      End
      Begin VB.Label Label3 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1125
         TabIndex        =   26
         Top             =   135
         Width           =   105
      End
      Begin VB.Label Label6 
         Caption         =   "s/d"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2820
         TabIndex        =   25
         Top             =   135
         Width           =   285
      End
   End
   Begin VB.Frame frDetail 
      Height          =   780
      Left            =   3465
      TabIndex        =   23
      Top             =   0
      Width           =   1905
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   600
         Left            =   90
         ScaleHeight     =   600
         ScaleWidth      =   1455
         TabIndex        =   40
         Top             =   135
         Width           =   1455
         Begin VB.OptionButton Option2 
            Caption         =   "Rekap"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            TabIndex        =   13
            Top             =   270
            Value           =   -1  'True
            Width           =   1635
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Detail"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   1635
         End
      End
   End
   Begin VB.OptionButton OptTipe 
      Caption         =   "per Bulan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   180
      TabIndex        =   10
      Top             =   450
      Width           =   2715
   End
   Begin VB.OptionButton OptTipe 
      Caption         =   "Per Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   180
      TabIndex        =   9
      Top             =   90
      Value           =   -1  'True
      Width           =   2715
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   6285
      Picture         =   "frmLaporanA.frx":021B
      TabIndex        =   8
      Top             =   6480
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4635
      Picture         =   "frmLaporanA.frx":031D
      TabIndex        =   7
      Top             =   6465
      Width           =   1230
   End
   Begin VB.Frame FrAcc 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   47
      Top             =   1950
      Visible         =   0   'False
      Width           =   3780
      Begin VB.TextBox txtKodeAcc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   50
         Top             =   120
         Width           =   1545
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2910
         ScaleHeight     =   375
         ScaleWidth      =   1275
         TabIndex        =   48
         Top             =   90
         Width           =   1275
         Begin VB.CommandButton cmdSearchAcc 
            Caption         =   "..."
            Height          =   375
            Left            =   0
            TabIndex        =   49
            Top             =   0
            Width           =   480
         End
      End
      Begin VB.Label Label18 
         Caption         =   "Kode Acc"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   52
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label17 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   51
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame FrKategori 
      BorderStyle     =   0  'None
      Height          =   2445
      Left            =   4530
      TabIndex        =   83
      Top             =   2085
      Visible         =   0   'False
      Width           =   4065
      Begin VB.ListBox listKategori 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2205
         Left            =   1125
         TabIndex        =   84
         Top             =   90
         Width           =   2895
      End
      Begin VB.Label Label8 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   945
         TabIndex        =   86
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label9 
         Caption         =   "Kategori"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   85
         Top             =   90
         Width           =   870
      End
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   6930
      Top             =   -90
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame frGudang 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   135
      TabIndex        =   20
      Top             =   3855
      Visible         =   0   'False
      Width           =   4215
      Begin VB.ComboBox cmbGudang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   5
         Top             =   45
         Width           =   2280
      End
      Begin VB.Label Label5 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   22
         Top             =   90
         Width           =   105
      End
      Begin VB.Label lblGudang 
         Caption         =   "Gudang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   21
         Top             =   90
         Width           =   870
      End
   End
   Begin VB.Frame frBulan 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   180
      TabIndex        =   28
      Top             =   1530
      Visible         =   0   'False
      Width           =   4830
      Begin VB.ComboBox cmbTahun 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2265
         TabIndex        =   3
         Top             =   90
         Width           =   1050
      End
      Begin VB.ComboBox cmbBulan 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmLaporanA.frx":041F
         Left            =   1215
         List            =   "frmLaporanA.frx":0447
         TabIndex        =   2
         Top             =   90
         Width           =   1005
      End
      Begin VB.Label Label7 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   30
         Top             =   135
         Width           =   105
      End
      Begin VB.Label Label2 
         Caption         =   "Bulan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   29
         Top             =   135
         Width           =   780
      End
   End
   Begin VB.Label lblNamaBahan 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4200
      TabIndex        =   111
      Top             =   3420
      Width           =   60
   End
   Begin VB.Label lblProses 
      BackColor       =   &H00000000&
      Caption         =   " Harap menunggu, data sedang diproses ..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   3630
      TabIndex        =   103
      Top             =   7365
      Visible         =   0   'False
      Width           =   4230
   End
   Begin VB.Line Line1 
      X1              =   90
      X2              =   8280
      Y1              =   1485
      Y2              =   1485
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmLaporanA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public kategori As String
Public Tipe As Byte
Public status As String
Dim LihatHPP As Boolean

Private Sub bJualHPP_Click()
Dim conn As New ADODB.Connection
Dim paramX As String
    conn.Open strcon
    conn.BeginTrans
    conn.Execute "delete from tmp_PenjualanVsHPP"
    conn.Execute "insert into tmp_PenjualanVsHPP (nomer_order,nomer_jual,tanggal,kode_bahan,nama_bahan,berat,qty,harga_jual,harga_kemasan,hpp,total_jual,total_hpp) " & _
                 "select t.nomer_order,d.nomer_jual,CONVERT(VARCHAR(10),t.tanggal_jual,111),d.kode_bahan,m.nama_bahan,d.berat,d.qty,d.harga,d.harga_kemasan,(case when (d.harga = 0) then (s.totalhpp/d.qty) else (s.totalhpp/d.berat) end) as hpp,(d.berat*d.harga)+(d.qty*d.harga_kemasan) as totaljual,s.totalhpp from t_juald d " & _
                "inner join t_jualh t on t.nomer_jual=d.nomer_jual " & _
                "left join ms_bahan m on m.kode_bahan=d.kode_bahan " & _
                "left join (select nomer_order,kode_bahan,sum(berat*hpp) as Totalhpp from t_suratjalan_timbang " & _
                "group by nomer_order,kode_bahan) s on s.nomer_order=t.nomer_order and s.kode_bahan=d.kode_bahan " & _
                "where Month(t.tanggal_jual) = '" & cmbBulan.text & "' And Year(t.tanggal_jual) = '" & cmbTahun.text & "' and t.status_posting<>'0'"
    conn.CommitTrans
    conn.Close
    
    On Error GoTo err
    Dim LogonInfo As String
        With CRPrint
            .reset
            .ReportFileName = reportDir & "\Laporan Penjualan Vs HPP.rpt"
            
            For i = 0 To CRPrint.RetrieveLogonInfo - 1
                .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & Mid(.LogonInfo(i), InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") + 1, InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), ";") - InStr(InStr(1, .LogonInfo(i), ";") + 1, .LogonInfo(i), "=") - 1) & ";UID=" & User & ";PWD=" & pwd & ";"
                LogonInfo = .LogonInfo(i)
            Next
            
            paramX = "1"
            If paramX <> "" Then .ParameterFields(3) = "Detail;" & paramX & ";True"
            .Destination = crptToWindow
            .WindowTitle = "Cetak" & PrintMode
            .WindowState = crptMaximized
            .WindowShowPrintSetupBtn = True
            .WindowShowPrintBtn = True
            .PrinterSelect
            .WindowShowExportBtn = True
    '        Me.Hide
            .action = 1
        
        End With
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
'    frmLaporanPenjualanVsHPP.Show
End Sub

Private Sub CmdHitung_Click()
    conn.Open strcon
    conn.BeginTrans
    Timer1.Enabled = True
    HitungUlang_KartuStock txtKdBarang, Trim(Right(cmbGudang.text, 30)), cmbSerial, conn
    Timer1.Enabled = False
    lblProses.Visible = False
    conn.CommitTrans
    conn.Close
    MsgBox "Proses Selesai !", vbInformation
End Sub

Private Sub cmdSearch_Click()
    If LCase(lblSupCus) = "customer" Then
'        If User <> "sugik" Then
'            SearchCustomer = "select kode_customer,nama_customer,nama_toko,alamat from ms_customer"
'        End If
        frmSearch.query = SearchCustomer
        frmSearch.nmform = "frmLaporanA"
        frmSearch.nmctrl = "txtSupCus"
        frmSearch.nmctrl2 = ""
        frmSearch.keyIni = "ms_customer"
        frmSearch.connstr = strcon
        frmSearch.proc = "cari_data"
        frmSearch.Index = -1
        frmSearch.Col = 0
        Set frmSearch.frm = Me
        frmSearch.loadgrid frmSearch.query
        frmSearch.Show vbModal
    Else
        frmSearch.query = SearchSupplier
        frmSearch.nmform = "frmLaporanA"
        frmSearch.nmctrl = "txtSupCus"
        frmSearch.nmctrl2 = ""
        frmSearch.keyIni = "ms_supplier"
        frmSearch.connstr = strcon
        frmSearch.proc = "cari_data"
        frmSearch.Index = -1
        frmSearch.Col = 0
        Set frmSearch.frm = Me
        frmSearch.loadgrid frmSearch.query
        frmSearch.Show vbModal
    End If
End Sub

Public Sub cari_data()
On Error Resume Next
    conn.ConnectionString = strcon
    conn.Open
    If LCase(lblSupCus) = "customer" Then
        rs.Open "select *  from ms_customer  " & _
                "where kode_customer='" & txtSupCus & "' ", conn
    Else
        rs.Open "select *  from ms_supplier  " & _
                "where kode_supplier='" & txtSupCus & "' ", conn
    End If
    
    If Not rs.EOF Then
        LabelSupCus = rs(1)
    End If
    rs.Close
    conn.Close
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_acc,nama from ms_coa"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodeAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cekAcc"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub cekAcc()
    'rwerw
End Sub

Private Sub cmdSearchCustomer_Click()
'    If User <> "sugik" Then
'        SearchCustomer = "select kode_customer,nama_customer,nama_toko,alamat from ms_customer"
'    End If
    frmSearch.connstr = strcon
    frmSearch.query = querycustomer
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodecustomer"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchHsl_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_bahan,nama_bahan,kategori from ms_bahan"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodeHsl"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_bahan"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub cari_bahan()
On Error Resume Next
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select *  from ms_bahan  " & _
            "where kode_bahan='" & txtKodeHsl & "' ", conn
    If Not rs.EOF Then
        lblNamaBahan = rs("nama_bahan")
    End If
    rs.Close
    conn.Close
End Sub


Private Sub cmdOperator_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_operator,nama_operator from ms_operator"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtOpr"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchKaryawan_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select NIK,nama_karyawan,alamat from ms_karyawan"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtNIK"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchMesin_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_gudang,nama_gudang,jenis_mesin from ms_gudang where status=1"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtMesin"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub Command2_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_bank from ms_bank order by kode_bank"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtBank"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "bank"
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub optGroup_Click(Index As Integer)
    If optGroup(0).value Or optGroup(2).value Or optGroup(3).value Then
    frKodeBarang.Visible = True
    FrKategori.Visible = True
    Else
    frKodeBarang.Visible = False
    FrKategori.Visible = False
    End If
    If (optGroup(1).value Or optGroup(2).value Or optGroup(3).value) And InStr(1, optGroup(i), "Customer") > 1 Then
    frMember.Visible = True
    Else
    frMember.Visible = False
    End If
End Sub



Private Sub chkGroupby_Click()
    If chkGroupby.value = "1" Then
        frGroup.Visible = True
    Else
        frGroup.Visible = False
    End If
End Sub


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
Dim filtergudang, filtertanggal, filterbulan, filtertahun, filtercarabayar, filter3, filteruserid, filterkodebarang As String
Dim filterkategori, filtersupplier, filtersupcus, filtercustomer As String, filterkodeacc As String, filterPosting As String, filterFinish As String
Dim sBulanPrev As String, sTahunPrev As String, filterNomerProduksi As String
Dim FilterKodePlat As String, FilterSerialPlat As String
Dim filterkodeHasil, filterOperator, filterMesin, filterNo_serial, filterNopol, filterNoTrans As String
Dim filterKategoriCust As String
Dim filterKaryawan As String
Dim ext As String
Dim query() As String
Dim param3 As String
    Select Case filename
' Opaname
        Case "\EkspedisiPengeluaran"
            filtertanggal = "{t_suratjalanh.tanggal_suratjalan}"
        Case "\Laporan Opname Kertas" ' via
           filtertanggal = "{t_stockOpnameH.tanggal}"
           filterkategori = "{rpt_ms_bahan.kategori}"
           filterkodebarang = "{t_stockOpnameD.kode_bahan}"
        Case "\Laporan Opname Tinta" ' via
           filtertanggal = "{t_stockopnameTintaH.tanggal}"
        Case "\Laporan Opname Plat" ' via
           filtertanggal = "{t_stockOpnamePlatH.tanggal}"

' koreksi
        Case "\Laporan Koreksi Kertas" ' via
           filtertanggal = "{t_stockKoreksiH.tanggal}"
           filterkategori = "{rpt_ms_bahan.kategori}"
           filterkodebarang = "{t_stockKoreksiD.kode_bahan}"
        Case "\Laporan Koreksi Tinta" ' via
           filtertanggal = "{t_stockoKoreksiTintaH.tanggal}"
        Case "\Laporan Koreksi Plat" ' via
           filtertanggal = "{t_stockKoreksiPlatH.tanggal}"
'mutasi
        Case "\Laporan Mutasi Tinta"
           filtertanggal = "{t_stockMutasiTintaH.tanggal}"
'Penjualan
        Case "\Laporan Penjualan Kertas"
            filtertanggal = "{t_jualKertasH.tanggal_jualKertas}"
            filtersupplier = "{t_jualKertasH.kode_customer}"
        Case "\laporan angkutan", "\laporan angkutan lain"
            filtertanggal = "{vw_angkutan.tanggal_suratjalan}"
            filtersupplier = "{vw_angkutan.kode_ekspedisi}"
        Case "\Laporan Penjualan Kertas per Item":
            filtertanggal = "{t_jualKertasH.tanggal_jualKertas}"
            filtersupplier = "{t_jualKertasH.kode_customer}"
            filterkodebarang = "{t_jualKertasd.kode_bahan}"
'Pembelian
        Case "\Laporan Pembelian Kertas"
            filtertanggal = "{t_beliKertasH.tanggal_beliKertas}"
            filtersupplier = "{t_beliKertasH.kode_supplier}"
        Case "\Laporan Pembelian Roll"
            filtertanggal = "{t_beliRollH.tanggal_beliRoll}"
            filtersupplier = "{t_beliRollH.kode_supplier}"
        Case "\Laporan Pembelian Tinta"
            filtertanggal = "{t_beliTintaH.tanggal_beliTinta}"
            filtersupplier = "{t_beliTintaH.kode_supplier}"
        Case "\Laporan Pembelian Plat"
            filtertanggal = "{t_beliPlatH.tanggal_beliPlat}"
            filtersupplier = "{t_beliPlatH.kode_supplier}"
            
        Case "\Laporan Pembelian"
            filtertanggal = "{t_belih.tanggal_beli}"
            filtersupplier = "{t_belih.kode_supplier}"
            filterkategori = "{ms_bahan.kategori}"
            filterkodebarang = "{t_belid.kode_bahan}"
            filterPosting = "{t_beliH.status_posting}"
            filterNo_serial = "{t_belid.nomer_serial}"
        Case "\Laporan Penerimaan Barang"
            filtertanggal = "{t_terimabarangh.tanggal_terimabarang}"
            filtersupplier = "{t_terimabarangh.kode_supplier}"
            filterkodebarang = "{t_terimabarang_timbang.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
        Case "\Laporan Order Pembelian"
            filtertanggal = "{t_poh.tanggal_po}"
            filtersupplier = "{t_poh.kode_supplier}"
            filterkodebarang = "{t_pod.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
        Case "\Laporan Penjualan"
            filtertanggal = "{t_jualh.tanggal_jual}"
            filtersupplier = "{t_jualh.kode_customer}"
            filterkategori = "{ms_bahan.kategori}"
            filterkodebarang = "{t_juald.kode_bahan}"
            filterPosting = "{t_jualH.status_posting}"
        Case "\Laporan Retur Penjualan"
            filtertanggal = "{t_returjualh.tanggal_returjual}"
            filtersupplier = "{t_returjualh.kode_customer}"
            filterkategori = "{ms_bahan.kategori}"
            filterkodebarang = "{t_returjuald.kode_bahan}"
            filterPosting = "{t_returjualh.status_posting}"
'Order kertas dan roll

        Case "\Laporan Order Roll"
            filtertanggal = "{vw_orderRoll.tanggal_OrderBeliRoll}"
            filterkodebarang = "{vw_orderRoll.kode_bahan}"
        Case "\Laporan Rekap Order Roll"
            filtertanggal = "{vw_orderRoll.tanggal_OrderBeliRoll}"
            filterkodebarang = "{vw_orderRoll.kode_bahan}"
        Case "\Laporan Order Kertas"
            filtertanggal = "{vw_orderKertas.tanggal_OrderBeliKertas}"
            filterkodebarang = "{vw_orderKertas.kode_bahan}"
        Case "\Laporan Rekap Order Kertas"
            filtertanggal = "{vw_orderKertas.tanggal_OrderBeliKertas}"
            filterkodebarang = "{vw_orderKertas.kode_bahan}"
        Case "\Laporan Coating Bungkus"
            filtertanggal = "{t_hasilcoatingbungkush.tanggal}"
'Retur Pembelian
        Case "\Laporan Retur Pembelian Kertas"
            filtertanggal = "{t_returbeliKertasH.tanggal_returbeliKertas}"
            filterkodebarang = "{t_returbeliKertasD.kode_bahan}"
        Case "\Laporan Retur Pembelian Tinta"
            filtertanggal = "{t_returbeliTintaH.tanggal_returbeliTinta}"
            filtersupplier = "{t_returbeliTintaH.kode_supplier}"
        Case "\Laporan Retur Pembelian Plat"
            filtertanggal = "{t_returbeliPlatH.tanggal_returbeliPlat}"
            filtersupplier = "{t_returbeliPlatH.kode_supplier}"
        Case "\Laporan Retur Pembelian"
            filtertanggal = "{t_returbeliH.tanggal_returbeli}"
            filtersupplier = "{t_returbelih.kode_supplier}"
            filterkodebarang = "{t_returbelid.kode_bahan}"
            filterNo_serial = "{t_returbelid.nomer_serial}"
'Keuangan
        Case "\Laporan Hutang"
            filtersupplier = "{list_hutang.kode_supplier}"
        Case "\Laporan Piutang"
            filtersupplier = "{list_piutang.kode_customer}"
            filterKategoriCust = "{rpt_ms_customer.kategori}"
        Case "\Laporan Outstanding BG"
            filtersupplier = "{list_bg.kode_supplier}"
            filtercustomer = "{list_bg.kode_customer}"
        Case "\Laporan History Produksi"
'            ProsesHistoryProduksi DTDari.value, DTSampai.value
            filtertanggal = "{vw_analisaproduksi.tglpotong}"
        Case "\Laporan Analisa PotongCD"
            filtertanggal = "{t_potongcdh.tanggal}"
        Case "\Laporan Buku Besar"
            generate_BukuBesar DTDari, DTSampai, txtKodeAcc
'            filtertanggal = "{tmp_bukubesar.tanggal}"
            filterkodeacc = "{tmp_bukubesar.kode_acc}"
            'filternotrans="{t_jurnalh.no_transaksi}"
        Case "\Laporan Jurnal"
            filtertanggal = "{t_jurnalH.tanggal}"
            filterNoTrans = "{t_jurnalh.no_transaksi}"
        Case "\Laporan Pembayaran Hutang"
            If OptTanggal(0).value = True Then
                filtertanggal = "{t_bayarhutangh.tanggal_bayarhutang}"
            ElseIf OptTanggal(1).value = True Then
                filtertanggal = "{t_belih.tanggal_beli}"
            Else
                filtertanggal = "{t_belih.tanggal_jatuhTempo}"
            End If
            filtersupplier = "{t_bayarhutangh.kode_supplier}"
        Case "\Laporan Penerimaan Piutang"
            If OptTanggal(0).value = True Then
                filtertanggal = "{t_bayarpiutangh.tanggal_bayarpiutang}"
            ElseIf OptTanggal(1).value = True Then
                filtertanggal = "{t_jualh.tanggal_jual}"
            Else
                filtertanggal = "{t_jualh.tanggal_jatuhTempo}"
            End If
            filtersupplier = "{t_bayarpiutangh.kode_customer}"
        Case "\Neraca"
            If cmbBulan.text = "" Or cmbTahun.text = "" Then
                MsgBox "Bulan dan Tahun harus ditentukan dulu !", vbExclamation
                Exit Sub
            End If
            HitungNeraca
        Case "\LabaRugi":
            If cmbBulan.text = "" Or cmbTahun.text = "" Then
                MsgBox "Bulan dan Tahun harus ditentukan dulu !", vbExclamation
                Exit Sub
            End If
            
            Select Case Val(cmbBulan.text)
                Case 1
                    strPeriodeLaba = "JANUARI " & cmbTahun.text
                Case 2
                    strPeriodeLaba = "FEBRUARI " & cmbTahun.text
                Case 3
                    strPeriodeLaba = "MARET " & cmbTahun.text
                Case 4
                    strPeriodeLaba = "APRIL " & cmbTahun.text
                Case 5
                    strPeriodeLaba = "MEI " & cmbTahun.text
                Case 6
                    strPeriodeLaba = "JUNI " & cmbTahun.text
                Case 7
                    strPeriodeLaba = "JULI " & cmbTahun.text
                Case 8
                    strPeriodeLaba = "AGUSTUS " & cmbTahun.text
                Case 9
                    strPeriodeLaba = "SEPTEMBER " & cmbTahun.text
                Case 10
                    strPeriodeLaba = "OKTOBER " & cmbTahun.text
                Case 11
                    strPeriodeLaba = "NOVEMBER " & cmbTahun.text
                Case 12
                    strPeriodeLaba = "DESEMBER " & cmbTahun.text
            End Select
            HitungLabaRugi
        Case "\Laporan Kartu Hutang"
            filtertanggal = "{tmp_kartuHutang.tanggal}"
            filtersupplier = "{tmp_kartuHutang.kode_supplier}"
        Case "\Laporan Kartu Piutang"
            filtertanggal = "{tmp_kartuPiutang.tanggal}"
            filtersupplier = "{tmp_kartuPiutang.kode_customer}"
        Case "\Laporan Produksi"
            filtertanggal = "{t_produksiH.tanggal_produksi}"
            filterNomerProduksi = "{t_produksiH.nomer_produksi}"
            filterkodebarang = "{t_produksi_bahan_1.kode_bahan}"
            filterkodeHasil = "{t_produksi_hasil_1.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
        Case "\Laporan All Proses"
            'filtertanggal = "{}"
            filterkodebarang = "{t_coatingd.kode_asal}"
'stock
        Case "\Laporan Stock"
            filterkodebarang = "{stock.kode_bahan}"
            filterkategori = "{rpt_ms_bahan.kategori}"
        Case "\Laporan Stock Per Tanggal"
            filterkodebarang = "{tmp_stockawal.kode_bahan}"
            filtergudang = "{tmp_stockawal.kode_gudang}"
            filterkategori = "{ms_bahan.kategori}"
            hitung_stockawal
' kartu stock
        Case "\Laporan Kartu Stock"
            filtertanggal = "{tmp_kartustock.tanggal}"
            filterkodebarang = "{tmp_kartustock.kode_bahan}"
            filterNo_serial = "{tmp_kartustock.nomer_serial}"
            hitung_stockawal
        Case "\Laporan Kartu Stock Tinta"
            filtertanggal = "{tmp_kartustocktinta.tanggal}"
            filterkodebarang = "{tmp_kartustocktinta.kode_tinta}"
        Case "\Laporan Kartu Stock Plat"
            filtertanggal = "{tmp_kartustockplat.tanggal}"
            filterkodebarang = "{tmp_kartustockplat.kode_plat}"
            
            sBulanPrev = BulanPrev(cmbBulan.text, cmbTahun.text)
            sTahunPrev = TahunPrev(cmbBulan.text, cmbTahun.text)
            
        Case "\Laporan Mutasi Stock Kertas"
            ProsesMutasi DTDari.value, DTSampai.value
            filterkategori = "{rpt_ms_bahan.kategori}"
            filtergudang = "{tmp_mutasistock.kode_gudang}"
'            MsgBox "selesai"
            
' PENGIRIMAN BARANG
        Case "\Laporan Analisa Pengiriman"
            filtertanggal = "{t_trukAngkut.tanggal_keluar}"
            filterNopol = "{t_trukAngkut.nopol}"
        Case "\Laporan Penilaian Karyawan"
            filtertanggal = "{penilaian_karyawan.tanggal}"
            filterKaryawan = "{penilaian_karyawan.NIK}"
            
'            conn.ConnectionString = strcon
'
'            conn.Open
'
''            rs.Open "Select * from mutasi_coa where bulan='" & sBulanPrev & "' and tahun='" & sTahunPrev & "'", conn
''            If rs.EOF Then
''                If (cmbBulan.text <> gBulanAwal Or cmbTahun.text <> gTahunAwal) Then
''                    MsgBox "Neraca tidak dapat ditampilkan karena data untuk Bulan " & sBulanPrev & "  Tahun " & sTahunPrev & "  belum ditutup !", vbExclamation
''                    rs.Close
''                    conn.Close
''                    Exit Sub
''                End If
''            End If
''            rs.Close
'            conn.Close
            
    End Select
    
    Formula = ""
    km = " " & km
    If frTanggal.Visible = True And filtertanggal <> "" Then
        
    If DTSampai.Visible = True Then
        Formula = Formula & filtertanggal & ">=#" & Format(DTDari, "MM/dd/yyyy") & "# and " & filtertanggal & "<#" & Format(DTSampai + 1, "MM/dd/yyyy") & "#"
    Else
        Formula = filtertanggal & "=#" & Format(DTDari, "MM/dd/yyyy") & "#"
    End If
    End If
    If cmbGudang.text <> "" And filtergudang <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filtergudang & "='" & Trim(Right(cmbGudang.text, 30)) & "'"
    End If
    If cmbKategori.text <> "" And filterKategoriCust <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterKategoriCust & "='" & cmbKategori.text & "'"
    End If
    If frKodeBarang.Visible = True And txtKdBarang.text <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterkodebarang & "='" & txtKdBarang.text & "'"
    End If
    If frNoTrans.Visible = True And txtNoTrans.text <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterNoTrans & "  like '*" & txtNoTrans.text & "*'"
    End If
    If FrKategori.Visible = True Then
    Dim kategori As String
    kategori = ""
        For i = 0 To listKategori.ListCount - 1
            If listKategori.Selected(i) Then kategori = kategori & "'" & listKategori.List(i) & "',"
        Next
        If filterkategori <> "" And kategori <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filterkategori & " in [" & Left(kategori, Len(kategori) - 1) & "]"
        End If
    End If
    If frSupCus.Visible = True Then
    Dim supcus As String
    supcus = ""
'        Dim X As Integer, StrSupCus As String
'        For i = 0 To cmbSupCus.ListCount - 1
'            If cmbSupCus.Selected(i) Then
'                X = InStr(1, cmbSupCus.List(i), ";")
'                StrSupCus = Left(cmbSupCus.List(i), X - 1)
'                supcus = supcus & "'" & StrSupCus & "',"
'            End If
'        Next
        supcus = txtSupCus.text
        
        If supcus <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filtersupplier & " = '" & supcus & "'"
         End If
        
'     If cmbSupCus.text <> "" And supcus <> "" Then
'        If Formula <> "" Then Formula = Formula & " and "
'        Formula = Formula & filtersupplier & " in  [" & Left(supcus, Len(supcus) - 1) & "]"
''     Else
''        If Formula <> "" Then Formula = Formula & " and "
''        Formula = Formula & filtersupplier & "='" & cmbSupCus.text & "'"
'     End If
    End If
    If chkActiveStock.Visible = True Then
    If chkActiveStock.value = 1 Then
      If Formula <> "" Then Formula = Formula & " and "
      Formula = Formula & "({tmp_mutasistock.masuk}>0 or {tmp_mutasistock.keluar}>0)"
    End If
    End If
    'filter untuk show-kriteria
    
    If frOperator.Visible = True Then
    If txtOpr <> "" Then
      If Formula <> "" Then Formula = Formula & " and "
      Formula = Formula & filterOperator & "='" & txtOpr & "'"
    End If
    End If
    
    If frMesin.Visible = True Then
    If txtMesin <> "" Then
      If Formula <> "" Then Formula = Formula & " and "
      Formula = Formula & filterMesin & "='" & txtMesin & "'"
    End If
    End If
    
'    If framCrByr.Visible = True Then
'        If chkCrByr(0).value = 0 Or chkCrByr(1).value = 0 Then
'            If Formula <> "" Then Formula = Formula & " and "
'            If chkCrByr(0).value = 1 And chkCrByr(1).value = 0 Then
'                Formula = Formula & filtercarabayar & "='1'"
'            ElseIf chkCrByr(0).value = 0 And chkCrByr(1).value = 1 Then
'                Formula = Formula & filtercarabayar & "='2' or " & filtercarabayar & "='3'"
'            End If
'        End If
'        If chkLunas(0).value = 0 Or chkLunas(1).value = 0 Then
'            If Formula <> "" Then Formula = Formula & " and "
'            If chkLunas(0).value = 1 And chkLunas(1).value = 0 Then
'                Formula = Formula & filter3 & "='1'"
'            ElseIf chkLunas(0).value = 0 And chkLunas(1).value = 1 Then
'                Formula = Formula & filter3 & "='0'"
'            End If
'        End If
'    End If
    If Trim(filtertanggal) <> "" Then
        If frBulan.Visible = True Then
            If cmbBulan.text <> "" And cmbTahun.text <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & " year(" & filtertanggal & ")=" & cmbTahun
            If cmbBulan.Visible = True Then Formula = Formula & " and month(" & filtertanggal & ")=" & cmbBulan & ""
    '        End If
            End If
        End If
    End If
    If frUserID.Visible = True Then
        If cmbUserID.text <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filteruserid & "='" & cmbUserID.text & "'"
        End If
    End If
    
    If cmbSerial.text <> "" And filterNo_serial <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterNo_serial & "='" & cmbSerial.text & "'"
    End If
    If frKaryawan.Visible = True And filterKaryawan <> "" And txtNIK.text <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterKaryawan & "='" & txtNIK.text & "'"
    End If
'    If frMember.Visible = True Then
'        If cmbMember.text <> "" Then
'        If Formula <> "" Then Formula = Formula & " and "
'        Formula = Formula & filtermember & "='" & cmbMember & "'"
'        End If
'    End If

    If filename = "\Laporan Produksi" Then
        If LihatHPP = False Then filename = "\Laporan Produksi User"
    End If
    
    frmprint.strfilename = filename & ".rpt"

    If frmprint.strfilename = "\Laporan Stock Kertas.rpt" Then
        If Formula <> "" Then Formula = Formula & " and "
'        If chkHabis.value = "1" Then
'        Formula = Formula & " {v_stock.stock}<=0"
'        Else
'        Formula = Formula & " {v_stock.stock}>0"
'        End If
        
        If cHarga.value = 1 Then
            frmprint.strfilename = "\Laporan Nilai Stock.rpt"
        Else
            frmprint.strfilename = "\Laporan Stock.rpt"
        End If
        
    End If
    
    
    Dim cek As String
    cek = ""
     If ckPosting.value = 1 And ckPosting.Visible = True Then cek = cek & filterPosting & "='1' and "
     If ckFinish.value = 1 And ckFinish.Visible = True Then cek = cek & filterFinish & "='1' and "
     If ckPosting.value = 0 And ckPosting.Visible = True Then cek = cek & filterPosting & "='0' and "
     If ckFinish.value = 0 And ckFinish.Visible = True Then cek = cek & filterFinish & "='0' and "
     If cek <> "" Then
        cek = Left(cek, Len(cek) - 4)
    End If
    
    Select Case frmprint.strfilename
    Case "\Laporan Proses Plong.rpt", "\Laporan Proses Potong.rpt", "\Laporan Proses Cetak.rpt", "\Laporan Pemakaian Tinta.rpt"
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & "( " & cek & " )"
    End Select
    
    If FrBahanHasil.Visible = True Then
        If txtKodeHsl <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filterkodeHasil & "='" & txtKodeHsl & "'"
        End If
        If cmbSerial.text <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filterNo_serial & "='" & cmbSerial.text & "'"
        End If
    End If
    
    
    param3 = ""
    If Option1.value = True Then
        param3 = "1"
    Else
        param3 = "0"
    End If
    
    
'    Select Case frmprint.strfilename
'    Case "\Laporan Penerimaan Piutang.rpt"
'        If optPiutang(1).value = True Then
'            frmprint.strfilename = "\Laporan Penerimaan Piutang Tunai.rpt"
'        ElseIf optPiutang(2).value = True Then
'            Dim filterbank As String
'            frmprint.strfilename = "\Laporan Penerimaan Piutang Transfer.rpt"
'            filterbank = "{t_bayarpiutangh.kode_bank}"
'            If txtBank <> "" Then
'              If Formula <> "" Then Formula = Formula & " and "
'              Formula = Formula & filterbank & "='" & txtBank & "'"
'            End If
'
'        ElseIf optPiutang(3).value = True Then
'            frmprint.strfilename = "\Laporan Penerimaan Piutang CekBG.rpt"
'        End If
'    End Select
'
    Select Case frmprint.strfilename
        Case "\Laporan Penerimaan Piutang.rpt"
            If Option2.value = True Then
                frmprint.strfilename = "\Laporan Rekap Penerimaan Piutang.rpt"
            End If
        Case "\Laporan Pembayaran Hutang.rpt"
            If Option2.value = True Then
                frmprint.strfilename = "\Laporan Rekap Pembayaran Hutang.rpt"
            End If
        Case "\EkspedisiPengeluaran.rpt"
            Formula = Formula & " and {t_suratjalanh.biaya_angkut} > 0"
        Case "\Laporan Kartu Stock.rpt"
            If Option1.value = True Then frmprint.strfilename = "\Laporan Kartu Stock Old.rpt"
        Case "\Laporan Penjualan.rpt"
            If Option2.value = True Then frmprint.strfilename = "\Laporan Penjualan Rekap.rpt"
        Case "\Laporan Pembelian.rpt"
            If Option2.value = True Then frmprint.strfilename = "\Laporan Pembelian Rekap.rpt"
        End Select
    
    frmprint.strFormula = Formula
    frmprint.param1 = Format(DTDari, "dd/MM/yyyy")
    frmprint.param2 = Format(DTSampai, "dd/MM/yyyy")
    frmprint.param3 = param3
    frmprint.Show vbModal
End Sub
Public Sub cekbarang()
'    cmbKategori.text = ""
End Sub
Private Sub cmdSearchKdBrg_Click()
Dim query As String
    frmSearch.connstr = strcon
    query = "Select kode_bahan,nama_bahan,kategori from ms_bahan"
    
    frmSearch.query = query
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKdBarang"
    frmSearch.nmctrl2 = ""
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    cari_serial
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_Load()

    
    conn.ConnectionString = strcon
    conn.Open
    
    LihatHPP = True
    
    If LCase(User) <> "sa" Then
        rs.Open "select nama_group from user_group where userid='" & User & "'", conn
        If Not rs.EOF Then
            NamaGrup = rs("nama_group")
        End If
        rs.Close
        
        rs.Open "select hpp_produksi from var_usergroup where [group]='" & NamaGrup & "'", conn
        If Not rs.EOF Then
            If rs!hpp_produksi = "0" Then LihatHPP = False
        End If
        rs.Close
    End If
    
    DTDari = Now
    DTSampai = Now
    cmbGudang.Clear
    cmbGudang.AddItem ""
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    listKategori.Clear
    listKategori.AddItem ""
    rs.Open "select distinct kategori from var_kategori  order by [kategori]", conn
    While Not rs.EOF
        listKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbSupCus.Clear
    
    Select Case filename
    Case "\Laporan Hutang", "\Laporan Pembayaran Hutang", "\Laporan Pembelian Kertas", "\Laporan Pembelian Tinta", "\Laporan Pembelian Plat", "\Laporan Kartu Hutang"
        rs.Open "select distinct(a.kode_supplier),b.nama_supplier from list_hutang a inner join ms_supplier b on a.kode_supplier=b.kode_supplier order by b.nama_supplier", conn
        While Not rs.EOF
            cmbSupCus.AddItem rs(0) & ";" & rs(1)
            rs.MoveNext
        Wend
        rs.Close
    End Select
    
    If filename = "\Laporan Piutang" Or filename = "\Laporan Penerimaan Piutang" Or filename = "\Laporan Penjualan" Or filename = "\Laporan Kartu Piutang" Then
        cmbSupCus.Clear
        rs.Open "select distinct(a.kode_customer),(case b.atasnama when '' then b.nama_customer else b.atasnama end) as nama from list_piutang a inner join ms_customer b on a.kode_customer=b.kode_customer order by nama", conn
        While Not rs.EOF
            cmbSupCus.AddItem rs(0) & ";" & rs(1)
            rs.MoveNext
        Wend
        rs.Close
    End If
    
    If filename = "\Laporan Pemakaian Tinta" Then
    cmbGudang.Clear
        rs.Open "select kode_gudang from ms_gudang where status='1' and jenis_mesin='Cetak'", conn
        While Not rs.EOF
            cmbGudang.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    
    rs.Open "select * from login order by username", conn
    While Not rs.EOF
        cmbUserID.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbTahun.Clear
    For i = Year(Now) To "2009" Step -1
        cmbTahun.AddItem i
    Next
    
    conn.Close
    Select Case filename
        Case "\EkspedisiLaporan":
        
        Case "\Laporan Penjualan", "\Laporan Retur Penjualan":
'            framCrByr.Visible = True
'            frGudang.Visible = True
'            frMember.Visible = True
'            frUserID.Visible = True
            frStatus.Visible = True
            frSupCus.Visible = True
            frKodeBarang.Visible = True
            lblSupCus.Caption = "Customer"
        Case "\laporan angkutan", "\laporan angkutan lain"
            frSupCus.Visible = True
        Case "\Laporan Penjualan Kertas per Item":
            frSupCus.Visible = True
            frKodeBarang.Visible = True
            lblSupCus.Caption = "Customer"
        Case "\Laporan Pembelian Kertas", "\Laporan Pembelian Tinta", "\Laporan Pembelian Plat", "\Laporan Retur Pembelian Kertas", "\Laporan Retur Pembelian Tinta", "\Laporan Retur Pembelian Plat", "\Laporan Pembelian", "\Laporan Retur Pembelian":
            'frGudang.Visible = True
            frSupCus.Visible = True
            optGroup(1).Caption = "per Supplier"
            optGroup(2).Caption = "per Item per Supplier"
            optGroup(3).Caption = "per Supplier per Item"
            frKodeBarang.Visible = True
        Case "\Laporan Penerimaan Barang"
            frSupCus.Visible = True
            optGroup(1).Caption = "per Supplier"
            optGroup(2).Caption = "per Item per Supplier"
            optGroup(3).Caption = "per Supplier per Item"
            frKodeBarang.Visible = True
            cmbSerial.Enabled = False
        Case "\Laporan Order Pembelian"
            frSupCus.Visible = True
            optGroup(1).Caption = "per Supplier"
            optGroup(2).Caption = "per Item per Supplier"
            optGroup(3).Caption = "per Supplier per Item"
            frKodeBarang.Visible = True
            cmbSerial.Enabled = False
            
        Case "\Laporan Opname Kertas", "\Laporan Opname Tinta", "\Laporan Opname Plat": 'via
            FrKategori.Visible = True
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
            frGudang.Visible = True
            
        Case "\Laporan Opname Tinta": 'via
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
            frGudang.Visible = True
        Case "\Laporan Opname Plat": 'via
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
            frGudang.Visible = True
            
        Case "\Laporan Hutang": 'via
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGroup.Visible = False
            
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frTanggal.Visible = False
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGudang.Visible = False
            chkGroupby.Visible = False
        Case "\Laporan Piutang":
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGroup.Visible = False
            
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frTanggal.Visible = False
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGudang.Visible = False
            chkGroupby.Visible = False
        Case "\Laporan Outstanding BG":
            frSupCus.Visible = True
        Case "\Laporan Member":
            frTanggal.Visible = True
            frMember.Visible = False
            OptTipe(0).Visible = True
            OptTipe(1).Visible = True
            OptTipe(1).Caption = "per Member"
            OptTipe(2).Visible = True
            OptTipe(2).Caption = "per Tanggal Per Member"
        Case "\Laporan Penjualan HPP":
            frGudang.Visible = True
        Case "\Laporan All Proses":
            frKodeBarang.Visible = True
        
        Case "\Laporan Pembayaran Hutang":
            frGudang.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frTanggal.Visible = True
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            chkGroupby.Visible = False
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGudang.Visible = False
            Option1.value = True
            frDetail.Visible = True
            Frame1.Visible = True
        Case "\Laporan Penerimaan Piutang":
            frGudang.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frTanggal.Visible = True
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            chkGroupby.Visible = False
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGudang.Visible = False
            Option1.value = True
            frDetail.Visible = True
            Frame1.Visible = True
        Case "\Laporan Kartu Stock", "\Laporan History Harga Pokok":
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frTanggal.Visible = True
            frDetail.Visible = True
            chkGroupby.Visible = False
            frGudang.Visible = True
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            frGroup.Visible = False
        Case "\Laporan Stock Kertas", "\Laporan Stock Habis", "\Laporan Kartu Stock", "\Laporan Produksi", "\Laporan Stock Per Tanggal":
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            Option1.value = True
            frGudang.Visible = True
            frDetail.Visible = False
            frTanggal.Visible = True
            frGroup.Visible = False
            chkGroupby.Visible = False
            frKodeBarang.Visible = True
            FrKategori.Visible = True
        Case "\Laporan Buku Besar":
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            FrAcc.Visible = True
        Case "\Laporan Mutasi Stock Kertas":
            frTanggal.Visible = True
            frDetail.Visible = False
            frGudang.Visible = True
            frGroup.Visible = False
            FrKategori.Visible = True
            Option1.value = True
            chkGroupby.Visible = False
            chkActiveStock.Visible = True
        Case "\Neraca", "\LabaRugi":
            OptTipe(1).value = True
            chkGroupby.Visible = False
            frDetail.Visible = False
            frBulan.Visible = True
            Line1.Visible = False
            bJualHPP.Visible = True
            OptTipe(0).Visible = False: OptTipe(1).Visible = False: OptTipe(2).Visible = False
        Case "\Laporan Proses Potong", "\Laporan Proses Cetak", "\Laporan Proses Plong", "\Laporan Proses Potong CD"
            frStatus.Visible = True
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            FrAcc.Visible = False
            show_kriteria
        
        Case "\Laporan Penerimaan Hasil Potong", "\Laporan Penerimaan Hasil Cetak", "\Laporan Penerimaan Hasil Plong", "\Laporan Penerimaan Hasil Coating"
            frStatus.Visible = True
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            FrAcc.Visible = False
            show_kriteria
        Case "\Laporan Coating Bungkus"
            frTanggal.Visible = True
            frDetail.Visible = True
        Case "\Laporan Pemakaian Tinta"
            frGudang.Visible = True
            ckFinish.Visible = False
            frStatus.Visible = True
            frKodeBarang.Visible = True
        Case "\Laporan Pemakaian Plat"
            frKodeBarang.Visible = True
            
        Case "\Laporan Jurnal"
            chkGroupby.Visible = False
            frNoTrans.Visible = True
        Case "\Laporan Kartu Hutang"
            frTanggal.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            chkGroupby.Visible = False
        Case "\Laporan Kartu Piutang"
            frTanggal.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Customer"
            chkGroupby.Visible = False
        Case "\Laporan Order Roll", "\Laporan Rekap Order Roll", "\Laporan Order Kertas", "\Laporan Rekap Order Kertas"
            frTanggal.Visible = True
            frSupCus.Visible = True
            chkGroupby.Visible = True
            frKodeBarang.Visible = True
        Case "\Laporan Penilaian Karyawan"
            frKaryawan.Visible = True
            chkGroupby.Visible = False
            cHarga.Visible = False
            frDetail.Visible = False
    End Select
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub OptTipe_Click(Index As Integer)
    If Index = 0 Then
        frBulan.Visible = False
        frTanggal.Visible = True
        cmbBulan.Visible = True
    Select Case filename
        Case "\Laporan Transfer Barang":
            
        Case "\Laporan Pembelian":
        Case "\Laporan Penjualan HPP":
        
        Case "\Laporan Penjualan":
            frUserID.Visible = True
            
        Case "\Laporan Member":
            frMember.Visible = False
            frTanggal.Visible = True
        Case "\Laporan Pembayaran Hutang":
            
        Case "\Laporan Stock":
            
        Case "\Laporan Kartu Stok":
            
    End Select
    End If
    If Index = 1 Then
            frBulan.Visible = True
            frTanggal.Visible = False
            cmbBulan.Visible = True
    Select Case filename
        Case "\Laporan Transfer Barang":
            
        Case "\Laporan Pembelian":
        Case "\Laporan Penjualan HPP":
            
            
        Case "\Laporan Penjualan":
            frUserID.Visible = True
            
        Case "\Laporan Member":
            frMember.Visible = True
            frTanggal.Visible = False
            
        Case "\Laporan Pembayaran Hutang":
            
        Case "\Laporan Stock":
            
        Case "\Laporan Kartu Stok":
            
    End Select
    End If
    If Index = 2 Then
        frBulan.Visible = True
        cmbBulan.Visible = False
        frTanggal.Visible = False
    Select Case filename
        Case "\Laporan Transfer Barang":
            
        Case "\Laporan Pembelian":
        Case "\Laporan Penjualan HPP":
            
        Case "\Laporan Penjualan":
            frUserID.Visible = True
            
        Case "\Laporan Member":
            frMember.Visible = True
            frTanggal.Visible = True
        Case "\Laporan Pembayaran Hutang":
            
        Case "\Laporan Stock":
            
        Case "\Laporan Kartu Stok":
            
    End Select
    End If
End Sub

Private Sub Timer1_Timer()
    If lblProses.Visible = False Then
        lblProses.Visible = True
    Else
        lblProses.Visible = False
    End If
End Sub

Private Sub txtKdBarang_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cekbarang
End Sub

Private Sub txtKdBarang_LostFocus()
    cari_serial
End Sub

Private Sub HitungLabaRugi()
Dim Laba As Double, AccLaba As String, strTgl As String
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Execute "delete from tmp_labarugi"
    
'    conn.Execute "insert into tmp_labarugi(kode_acc,debet,kredit) select kode_acc,sum(debet),sum(kredit) from t_jurnald d inner join t_jurnalh h on d.no_transaksi=h.no_transaksi " & _
'    " where month(h.tanggal) = '" & cmbBulan.text & "' and year(h.tanggal) = '" & cmbTahun.text & "' and left(kode_acc,1)>='4' group by kode_acc"

    If OptLR(0).value = True Then 'semua
        conn.Execute "insert into tmp_labarugi(kode_acc,debet,kredit) select d.kode_acc,sum(d.debet),sum(d.kredit) from t_jurnald_Tmp d inner join t_jurnalh_Tmp h on d.no_transaksi=h.no_transaksi " & _
        " where month(h.tanggal) = '" & cmbBulan.text & "' and year(h.tanggal) = '" & cmbTahun.text & "' and left(kode_acc,1)>='4' group by d.kode_acc"
    ElseIf OptLR(1).value = True Then 'pabrik saja
        conn.Execute "insert into tmp_labarugi(kode_acc,debet,kredit) select d.kode_acc,sum(d.debet),sum(d.kredit) from t_jurnald_Tmp d inner join t_jurnalh_Tmp h on d.no_transaksi=h.no_transaksi " & _
        " where h.kode_pc='p' and month(h.tanggal) = '" & cmbBulan.text & "' and year(h.tanggal) = '" & cmbTahun.text & "' and left(kode_acc,1)>='4' group by d.kode_acc"
    ElseIf OptLR(2).value = True Then 'toko saja
        conn.Execute "insert into tmp_labarugi(kode_acc,debet,kredit) select d.kode_acc,sum(d.debet),sum(d.kredit) from t_jurnald_Tmp d inner join t_jurnalh_Tmp h on d.no_transaksi=h.no_transaksi " & _
        " where h.kode_pc='s' and month(h.tanggal) = '" & cmbBulan.text & "' and year(h.tanggal) = '" & cmbTahun.text & "' and left(kode_acc,1)>='4' group by d.kode_acc"
    End If
'    rs.Open "Select kode_acc from setting_coa where kode='laba berjalan'", conn
'    If Not rs.EOF Then
'        AccLaba = rs("kode_acc")
'    End If
'    rs.Close
'
'
'    rs.Open "select sum(d.kredit-d.debet) as Laba from (t_jurnald d " & _
'            "inner join t_jurnalh t on t.no_transaksi=d.no_transaksi) " & _
'            "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
'            "where (p.kategori<>'HT' and p.kategori<>'KW' and p.kategori<>'MD')", conn
'    Laba = 0
'    If Not rs.EOF And IsNull(rs!Laba) = False Then
'        Laba = (rs!Laba)
'    End If
'    rs.Close
'
'    conn.BeginTrans
'    conn.Execute "Delete from t_jurnald where no_transaksi='00'"
'    conn.Execute "Delete from t_jurnalh where no_transaksi='00'"
'
'    InsertJurnal ("PD")
'    InsertJurnal ("HPP")
'    InsertJurnal ("BO")
'    InsertJurnal ("BNO")
'    InsertJurnal ("PL")
'    InsertJurnal ("BL")
'
'    strTgl = cmbTahun.text & "-" & cmbBulan.text & "-" & "01"
'
'
'    conn.Execute "insert into t_jurnald (no_transaksi,kode_acc,kredit) values ('00','" & AccLaba & "'," & Replace(Laba, ",", ".") & ")"
'    conn.Execute "insert into t_jurnalh (no_transaksi,no_jurnal,tanggal) values ('00','00','" & strTgl & "')"
'    conn.CommitTrans
    conn.Close
End Sub

Private Sub HitungNeraca()
Dim Laba As Double, AccLaba As String, strTgl As String
Dim sBulanPrev As String, sTahunPrev As String

    conn.ConnectionString = strcon
    conn.Open

    AccLaba = getAcc("laba berjalan")

    rs.Open "select sum(d.kredit-d.debet) as Laba from (t_jurnald d " & _
            "inner join t_jurnalh t on t.no_transaksi=d.no_transaksi) " & _
            "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
            "where CONVERT(VARCHAR(7), t.tanggal, 111)='" & cmbTahun & "/" & cmbBulan & "' " & _
            "and (p.kategori<>'HT' and p.kategori<>'KW' and p.kategori<>'MD')", conn
    Laba = 0
    If Not rs.EOF And IsNull(rs!Laba) = False Then
        Laba = (rs!Laba)
    End If
    rs.Close

'    cmbBulan.text = Val(cmbBulan.text)
    cmbTahun.text = Val(cmbTahun.text)

    If Val(cmbBulan.text) = 1 Then
        sBulanPrev = "12"
        sTahunPrev = Val(cmbTahun.text) - 1
    Else
        sBulanPrev = Val(cmbBulan.text) - 1
        sTahunPrev = cmbTahun.text
    End If

'    If Len(cmbBulan.text) = 1 Then cmbBulan.text = "0" & cmbBulan.text

    conn.BeginTrans
    conn.Execute "Delete from tmp_neraca"
    conn.Execute "insert into tmp_neraca (kode_acc,debet,kredit) " & _
                 "select d.kode_acc,sum(d.debet),sum(d.kredit) " & _
                 "from (t_jurnald d " & _
                 "inner join t_jurnalh h on h.no_transaksi=d.no_transaksi) " & _
                 "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
                 "where CONVERT(VARCHAR(7), h.tanggal, 111)='" & cmbTahun & "/" & cmbBulan & "' " & _
                 "and (p.kategori='HT' or p.kategori='KW' or p.kategori='MD') group by d.kode_acc"

    conn.Execute "insert into tmp_neraca (kode_acc,debet,kredit) " & _
                 "select m.kode_acc,case when p.kategori='HT' then m.saldoakhir else 0 end as debet, " & _
                 "case when p.kategori='HT' then 0 else m.saldoakhir end as kredit " & _
                 "from mutasi_coa m " & _
                 "inner join ms_parentAcc p on p.kode_parent=left(m.kode_acc,3) " & _
                 "where m.bulan='" & sBulanPrev & "' and m.tahun='" & sTahunPrev & "' " & _
                 "and (p.kategori='HT' or p.kategori='KW' or p.kategori='MD')"

    conn.Execute "insert into tmp_neraca (kode_acc,kredit) values ('" & AccLaba & "'," & Replace(Laba, ",", ".") & ")"
    conn.CommitTrans
    conn.Close
End Sub


Private Sub InsertJurnal(kategori As String)
    rs.Open "Select t.kode_acc from tmp_labarugi t " & _
            "inner join ms_parentAcc p on p.kode_parent=left(t.kode_acc,3) " & _
            "where p.kategori='" & kategori & "'", conn
    If rs.EOF Then
        conn.Execute "insert into tmp_labarugi(kode_acc) " & _
                     "select top 1 c.kode_acc from ms_coa c " & _
                     "inner join ms_parentAcc p on p.kode_parent=left(c.kode_acc,3) " & _
                     "where p.kategori='" & kategori & "'"
    End If
    rs.Close
End Sub
Private Sub show_kriteria()
    frKodeBarang.Visible = True
    FrBahanHasil.Visible = True
    frGudang.Visible = True
    frOperator.Visible = True
    frMesin.Visible = True
End Sub
Private Sub cari_serial()
conn.Open strcon
cmbSerial.Clear
    rs.Open "select distinct nomer_serial from stock where kode_bahan='" & txtKdBarang & "' order by nomer_serial", conn
    While Not rs.EOF
        cmbSerial.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
conn.Close
End Sub

Private Sub hitung_stockawal()
    conn.Open strcon
    If filename = "\Laporan Stock Per Tanggal" Then
        conn.Execute "exec sp_stockawal '','" & txtKdBarang.text & "','" & Trim(Right(cmbGudang.text, 30)) & "','" & Format(DTDari.value + 1, "yyyyMMdd") & "'"
    Else
        conn.Execute "exec sp_stockawal '','" & txtKdBarang.text & "','" & Trim(Right(cmbGudang.text, 30)) & "','" & Format(DTDari.value, "yyyyMMdd") & "'"
    End If
    conn.Close
End Sub
