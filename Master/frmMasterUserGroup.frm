VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMasterUserGroup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "User Group"
   ClientHeight    =   8955
   ClientLeft      =   45
   ClientTop       =   690
   ClientWidth     =   11190
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8955
   ScaleWidth      =   11190
   Begin VB.CheckBox chkExport 
      Caption         =   "Export"
      BeginProperty DataFormat 
         Type            =   5
         Format          =   ""
         HaveTrueFalseNull=   1
         TrueValue       =   "True"
         FalseValue      =   "False"
         NullValue       =   "False"
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   7
      EndProperty
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8670
      TabIndex        =   24
      Top             =   135
      Width           =   1140
   End
   Begin VB.CheckBox chkDeleteMaster 
      Caption         =   "Delete Master"
      BeginProperty DataFormat 
         Type            =   5
         Format          =   ""
         HaveTrueFalseNull=   1
         TrueValue       =   "True"
         FalseValue      =   "False"
         NullValue       =   "False"
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   7
      EndProperty
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   8670
      TabIndex        =   23
      Top             =   480
      Width           =   1395
   End
   Begin VB.CheckBox ChkHPP2 
      Caption         =   "Hpp Produksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6210
      TabIndex        =   22
      Top             =   800
      Width           =   2160
   End
   Begin VB.CheckBox chkUser 
      Caption         =   "Add/Delete user"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4500
      TabIndex        =   21
      Top             =   800
      Width           =   1635
   End
   Begin VB.Frame frDetail 
      Height          =   3165
      Index           =   3
      Left            =   4635
      TabIndex        =   19
      Top             =   5760
      Width           =   3975
      Begin VB.ListBox ListMsContact 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2460
         Left            =   270
         Style           =   1  'Checkbox
         TabIndex        =   20
         Top             =   360
         Width           =   3210
      End
   End
   Begin VB.Frame frDetail 
      Height          =   3165
      Index           =   2
      Left            =   315
      TabIndex        =   17
      Top             =   5670
      Width           =   3975
      Begin VB.ListBox ListMsSupplier 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2460
         Left            =   270
         Style           =   1  'Checkbox
         TabIndex        =   18
         Top             =   360
         Width           =   3210
      End
   End
   Begin VB.Frame frDetail 
      Height          =   4425
      Index           =   1
      Left            =   4635
      TabIndex        =   15
      Top             =   1260
      Width           =   3975
      Begin VB.ListBox listMsCustomer 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3660
         Left            =   270
         Style           =   1  'Checkbox
         TabIndex        =   16
         Top             =   360
         Width           =   3210
      End
   End
   Begin VB.Frame frDetail 
      Height          =   4425
      Index           =   0
      Left            =   225
      TabIndex        =   13
      Top             =   1215
      Width           =   4290
      Begin MSComctlLib.TreeView tvwMain 
         Height          =   3840
         Left            =   270
         TabIndex        =   14
         Top             =   225
         Width           =   3660
         _ExtentX        =   6456
         _ExtentY        =   6773
         _Version        =   393217
         LineStyle       =   1
         Style           =   6
         Checkboxes      =   -1  'True
         HotTracking     =   -1  'True
         SingleSel       =   -1  'True
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CheckBox chkUbahHarga 
      Caption         =   "Mengubah Harga Jual"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6210
      TabIndex        =   12
      Top             =   480
      Width           =   2085
   End
   Begin VB.CheckBox chkAlert 
      Caption         =   "Stock Alert"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6210
      TabIndex        =   11
      Top             =   135
      Width           =   1185
   End
   Begin VB.CheckBox chkHPP 
      Caption         =   "Hpp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4500
      TabIndex        =   2
      Top             =   480
      Width           =   1185
   End
   Begin VB.CheckBox chkStock 
      Caption         =   "Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4500
      TabIndex        =   1
      Top             =   135
      Width           =   1140
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   9360
      Picture         =   "frmMasterUserGroup.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5175
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   9360
      Picture         =   "frmMasterUserGroup.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4410
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3195
      Picture         =   "frmMasterUserGroup.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   270
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   270
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   9360
      Picture         =   "frmMasterUserGroup.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3645
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   10
      Top             =   315
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   720
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   7
      Top             =   315
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Group"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   315
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterUserGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim uncheck As Boolean

Dim xnode As Node


Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "delete from var_usergroup where [id]='" & txtField(0).text & "'"
    conn.Execute "delete from user_groupmenu where [group]='" & txtField(0).text & "'"
    conn.CommitTrans
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [group] from var_usergroup" ' order by satuan"
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.nmform = "frmMasteruserGroup"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "varUserGroup"
    frmSearch.Col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.top = 800
    frmSearch.Left = 70
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        txtField(0).SetFocus
        Exit Sub
    End If

    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "delete from var_usergroup where [group] = '" & txtField(0).text & "'"
    add_datagroup
'    rs.Open "select * from var_usergroup where [group]='" & txtField(0).text & "'", conn
'    If rs.EOF Then
'        conn.Execute "insert into var_usergroup([group], stock, hpp, alert, ubahharga, adduser, hpp_produksi) " & _
'                     "values ('" & txtField(0).text & "','" & chkStock.value & "','" & chkHPP.value & "', " & _
'                     "'" & chkAlert.value & "','" & chkUbahHarga.value & "','" & chkUser.value & "','" & ChkHPP2.value & "')"
'    Else
'        conn.Execute "update var_usergroup set stock='" & chkStock.value & "',hpp='" & chkHPP.value & "', " & _
'                     "alert='" & chkAlert.value & "',ubahharga='" & chkUbahHarga.value & "', " & _
'                     "adduser='" & chkUser.value & "',hpp_produksi='" & ChkHPP2.value & "' where [group]='" & txtField(0).text & "'"
'    End If
'    rs.Close
    
    conn.Execute "delete from user_groupmenu where [nama_group]='" & txtField(0).text & "'"
    For J = 1 To tvwMain.Nodes.Count
        Menu = Split(tvwMain.Nodes.item(J).key, ".")
        conn.Execute "insert into user_groupmenu values ('" & txtField(0).text & "','" & tvwMain.Nodes.item(J).key & "','" & Menu(0) & "','" & Menu(1) & "','" & IIf(tvwMain.Nodes.item(J).Checked, "1", "0") & "')"
    Next
    
    conn.Execute "delete from user_groupquery where [nama_group]='" & txtField(0).text & "'"
    Dim query As String
    query = ""
    
    'filter query untuk select data customer per group security
    For J = 0 To listMsCustomer.ListCount - 1
        If listMsCustomer.Selected(J) Then query = query & listMsCustomer.List(J) & ","
    Next
    If query <> "" Then query = Left(query, Len(query) - 1)
    
    conn.Execute "insert into user_groupquery values ('" & txtField(0).text & "','ms_customer','" & query & "')"
    
    query = ""
    
    'filter query untuk select data supplier per group security
    For J = 0 To ListMsSupplier.ListCount - 1
        If ListMsSupplier.Selected(J) Then query = query & ListMsSupplier.List(J) & ","
    Next
    If query <> "" Then query = Left(query, Len(query) - 1)
    
    conn.Execute "insert into user_groupquery values ('" & txtField(0).text & "','ms_supplier','" & query & "')"
    query = ""
    
    'filter query untuk select data contact per group security
    For J = 0 To ListMsContact.ListCount - 1
        If ListMsContact.Selected(J) Then query = query & ListMsContact.List(J) & ","
    Next
    If query <> "" Then query = Left(query, Len(query) - 1)
    
    conn.Execute "insert into user_groupquery values ('" & txtField(0).text & "','ms_contact','" & query & "')"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub add_datagroup()
    Dim table_name As String
    Dim field() As String
    Dim nilai() As String
    
    ReDim field(9)
    ReDim nilai(9)
    
    table_name = "var_usergroup"
    field(0) = "group"
    field(1) = "stock"
    field(2) = "hpp"
    field(3) = "alert"
    field(4) = "ubahharga"
    field(5) = "adduser"
    field(6) = "hpp_produksi"
    field(7) = "export"
    field(8) = "delete_master"
    
    nilai(0) = txtField(0).text
    nilai(1) = chkStock.value
    nilai(2) = chkHPP.value
    nilai(3) = chkAlert.value
    nilai(4) = chkUbahHarga.value
    nilai(5) = chkUser.value
    nilai(6) = ChkHPP2.value
    nilai(7) = chkExport.value
    nilai(8) = chkDeleteMaster.value
    
    conn.Execute tambah_data2(table_name, field, nilai)
End Sub

Private Sub reset_form()
    txtField(0).text = ""
    txtField(0).SetFocus
End Sub
Public Sub cari_data()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from var_usergroup where [group]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        chkHPP = rs("hpp")
        chkStock = rs("stock")
        chkAlert = rs!alert
        chkUbahHarga = rs!ubahharga
        chkUser = rs!adduser
        ChkHPP2 = rs("hpp_produksi")
        chkExport.value = rs!Export
        chkDeleteMaster.value = rs!delete_master
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        chkHPP = 0
        ChkHPP2 = 0
        chkStock = 0
        chkAlert = 0
        chkUbahHarga = 0
        chkUser = 0
        chkExport.value = 0
        chkDeleteMaster.value = 0
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    On Error Resume Next
    rs.Open "select * from user_groupmenu where [nama_group]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        While Not rs.EOF
            tvwMain.Nodes.item(rs("key").value).Checked = CBool(rs("value"))
            rs.MoveNext
        Wend
    Else
    End If
    On Error GoTo err
    Dim columnlist() As String
    rs.Close
    
    For i = 0 To listMsCustomer.ListCount - 1
    listMsCustomer.Selected(i) = False
    Next
    rs.Open "select * from user_groupquery where [nama_group]='" & txtField(0).text & "' and nama_table='ms_customer'", conn
    If Not rs.EOF Then
        columnlist = Split(rs(2), ",")
        For i = 0 To UBound(columnlist)
            For J = 0 To listMsCustomer.ListCount - 1
                If listMsCustomer.List(J) = columnlist(i) Then listMsCustomer.Selected(J) = True
            Next
        Next
    End If
    rs.Close
    For i = 0 To ListMsSupplier.ListCount - 1
    ListMsSupplier.Selected(i) = False
    Next
    rs.Open "select * from user_groupquery where [nama_group]='" & txtField(0).text & "' and nama_table='ms_supplier'", conn
    If Not rs.EOF Then
        columnlist = Split(rs(2), ",")
        For i = 0 To UBound(columnlist)
            For J = 0 To ListMsSupplier.ListCount - 1
                If ListMsSupplier.List(J) = columnlist(i) Then ListMsSupplier.Selected(J) = True
            Next
        Next
    End If
    rs.Close
    For i = 0 To ListMsContact.ListCount - 1
    ListMsContact.Selected(i) = False
    Next
    rs.Open "select * from user_groupquery where [nama_group]='" & txtField(0).text & "' and nama_table='ms_contact'", conn
    If Not rs.EOF Then
        columnlist = Split(rs(2), ",")
        For i = 0 To UBound(columnlist)
            For J = 0 To ListMsContact.ListCount - 1
                If ListMsContact.List(J) = columnlist(i) Then ListMsContact.Selected(J) = True
            Next
        Next
    End If
    rs.Close
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
     
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
Dim nodex As Node
Dim hwnd As Long
Dim hmenu As Long
Dim parent As String
Dim Index As Integer

        For Each obj In frmMain.Controls
          If TypeOf obj Is Menu And obj.Enabled = True Then
            
            If nodex Is Nothing Or obj.Name = parent Then
                Set nodex = tvwMain.Nodes.Add(Null, Null, obj.Name & "." & obj.Index, obj.Caption)
                parent = obj.Name
                Index = obj.Index
            Else
                Set nodex = tvwMain.Nodes.Add(parent & "." & Index, tvwChild, obj.Name & "." & obj.Index, obj.Caption)
            End If
            nodex.Checked = True
           End If
    Next

    
    conn.Open strcon

    
    rs.Open "select top 1 * from ms_customer", conn
    For i = 0 To rs.fields.Count - 1
        listMsCustomer.AddItem rs.fields(i).Name
    Next
    rs.Close
    rs.Open "select top 1 * from ms_supplier", conn
    For i = 0 To rs.fields.Count - 1
        ListMsSupplier.AddItem rs.fields(i).Name
    Next
    rs.Close
    rs.Open "select top 1 * from ms_contact", conn
    For i = 0 To rs.fields.Count - 1
        ListMsContact.AddItem rs.fields(i).Name
    Next
    rs.Close
    
    conn.Close
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub tvwMain_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
Dim nod As Node
    If Not uncheck Then Exit Sub
    Set nod = xnode.FirstSibling
    If nod.Checked = True Then Exit Sub
    While nod <> xnode.LastSibling
        Set nod = nod.Next
        If nod.Checked = True Then Exit Sub
    Wend
    
    uncheck = False
gaksip:
    MsgBox "Minimal harus ada satu menu yang dipakai, apabila anda ingin membuat serangkaian menu tidak kelihatan silahkan uncheck pada parent rangkaian menu tersebut."
    xnode.Checked = True
End Sub

Private Sub tvwMain_NodeCheck(ByVal Node As MSComctlLib.Node)
Dim nod As Node
    Node.Checked = True
    uncheck = False
    Set nod = Node.Child
    For i = 1 To Node.Children
        nod.Checked = True
        Set nod = nod.Next
    Next
    Set xnode = Node
    uncheck = True
    

End Sub


Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
