VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmTransNotaDebet 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nota Debet / Kredit Piutang"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9000
   DrawStyle       =   1  'Dash
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   9000
   Begin VB.TextBox txtKodeCustomer 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1584
      TabIndex        =   1
      Top             =   972
      Width           =   1530
   End
   Begin VB.CommandButton cmdSearchCustomer 
      Appearance      =   0  'Flat
      Caption         =   "F4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3180
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   32
      Top             =   960
      Width           =   420
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting (F6)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3864
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   7020
      Width           =   1230
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   23
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4572
      TabIndex        =   22
      Top             =   240
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1656
      Left            =   135
      TabIndex        =   15
      Top             =   1776
      Width           =   6990
      Begin VB.TextBox txtNilai 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         TabIndex        =   4
         Top             =   675
         Width           =   1515
      End
      Begin VB.CommandButton cmdSearchJual 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3105
         TabIndex        =   16
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2745
         TabIndex        =   7
         Top             =   1164
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1635
         TabIndex        =   6
         Top             =   1164
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   225
         TabIndex        =   5
         Top             =   1164
         Width           =   1350
      End
      Begin VB.TextBox txtNoJual 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1545
         TabIndex        =   3
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label LblSisaPiutang 
         BackColor       =   &H8000000C&
         Caption         =   "Sisa Piutang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   345
         Left            =   3570
         TabIndex        =   17
         Top             =   315
         Width           =   3300
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         Caption         =   "Nilai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   29
         Top             =   720
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4185
         TabIndex        =   20
         Top             =   1575
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   19
         Top             =   1125
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "No. Penjualan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   18
         Top             =   315
         Width           =   1245
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   495
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   7020
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3795
      TabIndex        =   14
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1572
      TabIndex        =   2
      Top             =   1356
      Width           =   3885
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5271
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7020
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2499
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   7020
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1605
      TabIndex        =   0
      Top             =   585
      Width           =   2475
      _ExtentX        =   4366
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   100794371
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2856
      Left            =   108
      TabIndex        =   21
      Top             =   3516
      Width           =   8616
      _ExtentX        =   15214
      _ExtentY        =   5054
      _Version        =   393216
      Cols            =   4
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8460
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblNamaCustomer 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   228
      Left            =   3732
      TabIndex        =   34
      Top             =   1032
      Width           =   72
   End
   Begin VB.Label Label19 
      Caption         =   "Customer :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   33
      Top             =   1020
      Width           =   1368
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   195
      TabIndex        =   30
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6204
      TabIndex        =   28
      Top             =   228
      Visible         =   0   'False
      Width           =   744
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6975
      TabIndex        =   27
      Top             =   225
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7155
      TabIndex        =   26
      Top             =   225
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   6180
      TabIndex        =   25
      Top             =   6504
      Width           =   2112
   End
   Begin VB.Label Label17 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   5268
      TabIndex        =   24
      Top             =   6504
      Width           =   732
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   192
      TabIndex        =   13
      Top             =   624
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   192
      TabIndex        =   12
      Top             =   1404
      Width           =   1272
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   11
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransNotaDebet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryCOA As String = "select * from ms_coa where fg_aktif = 1"

Dim mode As Byte
Dim totalNilai As Long

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String
Public Tipe As String

Private Sub cmdClear_Click()
    txtNoJual.text = ""
    debet = True
    LblSisaPiutang = ""
    txtNilai.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim Row, Col As Integer
    Row = flxGrid.Row
    If flxGrid.Rows <= 2 Then Exit Sub
    totalNilai = totalNilai - (flxGrid.TextMatrix(Row, 3))
    lblTotal = Format(totalNilai, "#,##0")
    For Row = Row To flxGrid.Rows - 1
        If Row = flxGrid.Rows - 1 Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(Row + 1, 1) = "" Then
            For Col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(Row, Col) = ""
            Next
        ElseIf flxGrid.TextMatrix(Row + 1, 1) <> "" Then
            For Col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(Row, Col) = flxGrid.TextMatrix(Row + 1, Col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim Row As Integer
    Row = 0
    If txtNoJual.text <> "" And LblSisaPiutang <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtNoJual.text) Then Row = i
        Next
        If Row = 0 Then
            Row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(Row, 1) = txtNoJual.text
        flxGrid.TextMatrix(Row, 2) = LblSisaPiutang
        
        If flxGrid.TextMatrix(Row, 3) <> "" Then totalNilai = totalNilai - (flxGrid.TextMatrix(Row, 3))
        flxGrid.TextMatrix(Row, 3) = txtNilai
        totalNilai = totalNilai + (flxGrid.TextMatrix(Row, 3))
        flxGrid.Row = Row
        flxGrid.Col = 0
        flxGrid.ColSel = 3
        
        lblTotal = Format(totalNilai, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If Row > 8 Then
            flxGrid.TopRow = Row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtNoJual.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub cmdPosting_Click()
    Call Posting_NotaDebet(lblNoTrans)
    reset_form
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdSearchAcc_Click()
    frmSearch.query = queryCOA
    frmSearch.nmform = "frmTransap"
    frmSearch.nmctrl = "txtNoJual"
    frmSearch.connstr = strcon
    frmSearch.Col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_jual Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select nomer_notadebet,Tanggal,kode_customer,Keterangan from t_notadebeth "
    frmSearch.nmform = "frmAddjurnal"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.Col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub




Private Sub cmdSearchCustomer_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchCustomer
    frmSearch.nmform = "frmTransNotaDebet"
    frmSearch.nmctrl = "txtKodeCustomer"
    frmSearch.Col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_customer"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub CmdSearchjual_Click()
    With frmSearch
    .query = "select nomer_transaksi,kode_customer, nomer_referensi,tanggal_jatuhtempo,piutang,total_bayar from list_piutang where piutang-total_bayar>0 and kode_customer='" & txtKodeCustomer.text & "'"
    .nmform = "frmAddBayarhutang"
    .nmctrl = "txtnojual"
    .nmctrl2 = ""
    .connstr = strcon
    .keyIni = "hutangpiutang"
    .Col = 0
    .Index = -1
    .proc = "cek_jual"
    .loadgrid frmSearch.query
    Set .frm = Me
    
    .Show vbModal
End With
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim Row As Integer

On Error GoTo err
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan detail terlebih dahulu"
        txtNoJual.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_notadebeth", "nomer_notadebet", DTPicker1, "ND")
'        NoJurnal = Nomor_Jurnal(DTPicker1)
    Else
        
            rs.Open "select no_jurnal from t_jurnalh where nomer_notadebet='" & lblNoTrans & "'", conn
            If Not rs.EOF Then
                NoJurnal = rs("no_jurnal")
            End If
            rs.Close
            
            conn.Execute "delete from t_notadebeth where nomer_notadebet='" & lblNoTrans & "'"
            conn.Execute "delete from t_notadebetd where nomer_notadebet='" & lblNoTrans & "'"
            
            conn.Execute "delete from t_jurnalh where no_transaksi='" & lblNoTrans & "'"
            conn.Execute "delete from t_jurnald where no_transaksi='" & lblNoTrans & "'"
            
            
'            conn.Execute "delete from jurnal where no_trans='" & lblNoTrans & "'"
        
    End If
    
    
    
    add_dataheader
'    add_jurnalheader
    
    For Row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(Row, 1) <> "" Then
            add_datadetail (Row)
'            add_jurnaldetail (row)
        End If
    Next
    
    
    
    
    conn.CommitTrans
    DropConnection
    MsgBox "Data sudah tersimpan"
'    reset_form
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub


Public Sub reset_form()
    lblNoTrans = "-"
    txtKodeCustomer.text = ""
    lblNamaCustomer = ""
    txtKeterangan.text = ""
    totalNilai = 0
    DTPicker1 = Now
    DTPicker2 = DateAdd("m", 1, Now)
    cmdClear_Click
    cmdSimpan.Enabled = True
    cmdPosting.Enabled = True
    lblTotal = 0
    flxGrid.Rows = 1
    flxGrid.Rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(6)
    ReDim nilai(6)
    table_name = "t_notadebeth"
    fields(0) = "nomer_notadebet"
    fields(1) = "tanggal"
    fields(2) = "kode_Customer"
    fields(3) = "keterangan"
    fields(4) = "total"
    fields(5) = "userid"
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKodeCustomer.text
    nilai(3) = txtKeterangan.text
    nilai(4) = Format(totalNilai, "###0")
    nilai(5) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(Row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)
    
    table_name = "t_notadebetd"
    fields(0) = "nomer_notadebet"
    fields(1) = "nomer_jual"
    fields(2) = "nilai"
    fields(3) = "no_urut"
    
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(Row, 1)
    nilai(2) = Replace(flxGrid.TextMatrix(Row, 3), ",", ".")
    nilai(3) = Row
    
    tambah_data table_name, fields, nilai
End Sub




Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim StatusPosting As String
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_notadebeth t where nomer_notadebet='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
        
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtKodeCustomer.text = rs("kode_Customer")
        cek_customer
        StatusPosting = rs("status_posting")
        totalNilai = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        Row = 1
        
        rs.Open "select d.nomer_jual,d.nilai from t_notadebetd d where d.nomer_notadebet='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(Row, 1) = rs(0)
                flxGrid.TextMatrix(Row, 3) = rs(1)
                
                Row = Row + 1
                totalNilai = totalNilai + rs(1)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(totalNilai, "#,##0")
        
        If StatusPosting = "1" Then
            cmdPosting.Enabled = False
            cmdSimpan.Enabled = False
        Else
            cmdPosting.Enabled = True
            cmdSimpan.Enabled = True
        End If

        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub Command1_Click()
End Sub

Private Sub DTPicker1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then

    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.Row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtNoJual.text = flxGrid.TextMatrix(flxGrid.Row, 1)
            cek_jual
            
            txtNilai.text = flxGrid.TextMatrix(flxGrid.Row, 3)
            txtNilai.SetFocus
        End If
    End If
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF4 Then cmdSearchCustomer_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF6 Then
        If cmdPosting.Visible Then cmdPosting_Click
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtNoJual.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    LblSisaPiutang = ""
    loadcombo
    reset_form
    total = 0
    
    
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    'End If
    'rs.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1400
    flxGrid.ColWidth(2) = 0
    flxGrid.ColWidth(3) = 1500
    
    
    flxGrid.TextMatrix(0, 1) = "No. Penjualan"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
'    flxGrid.TextMatrix(0, 2) = "Piutang"
    flxGrid.TextMatrix(0, 3) = "Nilai"
    


End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_jual() As Boolean
Dim kode As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select (piutang-total_bayar) from list_piutang where nomer_transaksi='" & txtNoJual.text & "' ", conn
    If Not rs.EOF Then
        LblSisaPiutang = Format(rs(0), "#,##0")
        cek_jual = True
        'If rs(4) = "D" Then debet = True Else debet = False
    Else
        LblSisaPiutang = ""
        lblKode = ""
        
        cek_jual = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Function

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub



Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtNoJual_GotFocus()
    txtNoJual.SelStart = 0
    txtNoJual.SelLength = Len(txtNoJual.text)
'    foc = 1
End Sub

Private Sub txtNoJual_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 Then
'        If txtNoJual.text = "" Then
'            flxGrid_Click
'        Else
        If txtNoJual.text = "" Then KeyCode = 0
        
        If txtNoJual.text <> "" Then
            If Not cek_jual Then
                MsgBox "Nomer yang anda masukkan salah"
            Else
'            cmdOK_Click
            End If
        End If
'        End If
'        cari_id
    ElseIf KeyCode = vbKeyF3 Then
        CmdSearchjual_Click
    End If
End Sub

Public Function cek_customer() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    cek_customer = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_customer] from ms_customer where kode_customer='" & txtKodeCustomer & "'", conn
    If Not rs.EOF Then
        cek_customer = True
        lblNamaCustomer = rs(0)
    Else
        lblNamaCustomer = ""
    End If
    rs.Close
    conn.Close
err:
End Function


Private Sub txtKodecustomer_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_customer
End Sub

Private Sub txtKodecustomer_LostFocus()
    If Not cek_customer And txtKodeCustomer <> "" Then
        MsgBox "Kode Customer yang anda masukkan salah"
        txtKodeCustomer.SetFocus
    End If
End Sub
Private Sub txtNilai_GotFocus()
    txtNilai.SelStart = 0
    txtNilai.SelLength = Len(txtNilai.text)
End Sub

Private Sub txtNilai_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtNilai_LostFocus()
    If Not IsNumeric(txtNilai.text) Then txtNilai.text = "1"
End Sub


