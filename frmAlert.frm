VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAlert 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Reminder"
   ClientHeight    =   6870
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13155
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   13155
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   525
      TabIndex        =   0
      Top             =   6285
      Width           =   1185
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   60
      Top             =   30
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   5970
      Left            =   90
      TabIndex        =   1
      Top             =   180
      Width           =   12870
      _Version        =   196616
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   1323
      Columns.Count   =   4
      Columns(0).Width=   3572
      Columns(0).Caption=   "Nomer"
      Columns(0).Name =   "Nomer"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   4207
      Columns(1).Caption=   "Dari User"
      Columns(1).Name =   "Dari User"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   10478
      Columns(2).Caption=   "Pesan"
      Columns(2).Name =   "Pesan"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3360
      Columns(3).Caption=   "Tanggal"
      Columns(3).Name =   "Tanggal"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      _ExtentX        =   22701
      _ExtentY        =   10530
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAlert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDelete_Click()
Dim conn1 As New ADODB.Connection
    conn1.Open strcon
    conn1.BeginTrans
    conn1.Execute "update alert set status='1',status_popup='1' where nomer='" & DBGrid.Columns(0).text & "' "
    conn1.Execute "update alert set tanggal_alert=dateadd(month,1,tanggal_alert),status='0',status_popup='0' where nomer='" & DBGrid.Columns(0).text & "' and bulanan='y'"
    conn1.CommitTrans
    conn1.Close
    TampilData
End Sub

Private Sub DBGrid_DblClick()
Dim connrs As New ADODB.Connection
    connrs.Open strcon
    frmReminder.lblNomer.Caption = DBGrid.Columns(0).text
    frmReminder.lblUser.Caption = DBGrid.Columns(1).text
    frmReminder.LblPesan.Caption = DBGrid.Columns(2).text
    If DBGrid.Columns(3).text <> "" Then
        frmReminder.DTPicker1.value = Format(DBGrid.Columns(3).text, "dd-MM-yyyy hh:mm")
    End If
    connrs.Execute "update alert set status_popup='1' where nomer='" & DBGrid.Columns(0).text & "' "
    connrs.Close
    frmReminder.Show
End Sub

Private Sub Form_Load()
    TampilData
End Sub

Sub TampilData()
Dim connrs As New ADODB.Connection
Dim rs As New ADODB.Recordset
    connrs.Open strcon
    
    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    rs.Open "select nomer,dari,pesan,tanggal_alert from alert where  status='0' and (userid='" & User & "' or userid='') " & _
            "and convert(varchar(16),tanggal_alert,120)<= '" & Format(Now, "YYYY-MM-DD HH:MM") & "' order by tanggal_alert desc", connrs
    While Not rs.EOF
        DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & Format(rs(3), "dd-MM-yyyy hh:mm")
        rs.MoveNext
    Wend
    rs.Close

    connrs.Close
End Sub

Private Sub Timer1_Timer()
    TampilData
End Sub
