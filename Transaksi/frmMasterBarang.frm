VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterBarang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Kertas"
   ClientHeight    =   7560
   ClientLeft      =   48
   ClientTop       =   732
   ClientWidth     =   7848
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   7848
   Begin VB.Frame FrHrgJual 
      Caption         =   "Harga Jual"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1965
      Left            =   3960
      TabIndex        =   58
      Top             =   2190
      Width           =   3555
      Begin VB.TextBox txtHrgJual4 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   11
         Top             =   1560
         Width           =   1455
      End
      Begin VB.TextBox txtHrgJual1 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1590
         TabIndex        =   8
         Top             =   330
         Width           =   1455
      End
      Begin VB.TextBox txtHrgJual2 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   9
         Top             =   750
         Width           =   1455
      End
      Begin VB.TextBox txtHrgJual3 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   10
         Top             =   1170
         Width           =   1455
      End
      Begin VB.Label Label19 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga D"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   66
         Top             =   1590
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   7
         Left            =   1260
         TabIndex        =   65
         Top             =   1590
         Width           =   285
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   6
         Left            =   1260
         TabIndex        =   64
         Top             =   375
         Width           =   285
      End
      Begin VB.Label Label18 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   63
         Top             =   345
         Width           =   1140
      End
      Begin VB.Label Label17 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga B"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   62
         Top             =   750
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   5
         Left            =   1260
         TabIndex        =   61
         Top             =   780
         Width           =   285
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga C"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   60
         Top             =   1200
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   4
         Left            =   1260
         TabIndex        =   59
         Top             =   1200
         Width           =   285
      End
   End
   Begin VB.Frame frHarga 
      Caption         =   "Harga Beli"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   240
      TabIndex        =   51
      Top             =   2190
      Width           =   3555
      Begin VB.TextBox txtHrg3 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   7
         Top             =   1170
         Width           =   1455
      End
      Begin VB.TextBox txtHrg2 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1575
         TabIndex        =   6
         Top             =   750
         Width           =   1455
      End
      Begin VB.TextBox txtField 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   1575
         TabIndex        =   5
         Top             =   345
         Width           =   1455
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   3
         Left            =   1260
         TabIndex        =   57
         Top             =   1200
         Width           =   285
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga C"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   56
         Top             =   1200
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   2
         Left            =   1260
         TabIndex        =   55
         Top             =   780
         Width           =   285
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga B"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   54
         Top             =   750
         Width           =   1140
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   53
         Top             =   345
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   1260
         TabIndex        =   52
         Top             =   375
         Width           =   285
      End
   End
   Begin VB.CommandButton cmdSearchGroup 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3870
      TabIndex        =   50
      Top             =   1845
      Width           =   465
   End
   Begin VB.TextBox txtGroup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1845
      MaxLength       =   25
      TabIndex        =   4
      Top             =   1845
      Width           =   1950
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dimensi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      Left            =   210
      TabIndex        =   41
      Top             =   4200
      Width           =   2835
      Begin VB.TextBox txtField 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   5
         Left            =   1110
         TabIndex        =   14
         Top             =   1155
         Width           =   795
      End
      Begin VB.TextBox txtField 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   1110
         TabIndex        =   13
         Top             =   750
         Width           =   795
      End
      Begin VB.TextBox txtField 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   1110
         TabIndex        =   12
         Top             =   345
         Width           =   795
      End
      Begin VB.Label Label4 
         Caption         =   "gram"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1995
         TabIndex        =   47
         Top             =   1185
         Width           =   570
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Berat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   46
         Top             =   1185
         Width           =   945
      End
      Begin VB.Label Label14 
         Caption         =   "cm"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1995
         TabIndex        =   45
         Top             =   780
         Width           =   300
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Lebar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   44
         Top             =   765
         Width           =   945
      End
      Begin VB.Label Label10 
         Caption         =   "cm"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1995
         TabIndex        =   43
         Top             =   375
         Width           =   300
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Panjang "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   42
         Top             =   360
         Width           =   945
      End
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   1845
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1380
      Width           =   1995
   End
   Begin VB.Frame frStock 
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      Height          =   330
      Left            =   5145
      TabIndex        =   36
      Top             =   1440
      Visible         =   0   'False
      Width           =   3345
      Begin VB.TextBox lblStock 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1650
         TabIndex        =   22
         Top             =   0
         Visible         =   0   'False
         Width           =   1545
      End
      Begin VB.Label Label32 
         BackStyle       =   0  'Transparent
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   285
         TabIndex        =   37
         Top             =   15
         Visible         =   0   'False
         Width           =   1140
      End
   End
   Begin VB.Frame frHPP 
      BorderStyle     =   0  'None
      Caption         =   "Frame6"
      Height          =   375
      Left            =   4740
      TabIndex        =   34
      Top             =   4365
      Visible         =   0   'False
      Width           =   3345
      Begin VB.TextBox lblHPP 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2190
         TabIndex        =   21
         Top             =   60
         Width           =   1455
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   1875
         TabIndex        =   48
         Top             =   75
         Width           =   285
      End
      Begin VB.Label Label23 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga Pokok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   570
         TabIndex        =   35
         Top             =   75
         Width           =   1140
      End
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5715
      TabIndex        =   33
      Top             =   6240
      Width           =   1230
   End
   Begin VB.CommandButton cmdBaru 
      Caption         =   "&Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3840
      Picture         =   "frmMasterBarang.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   6825
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3330
      Picture         =   "frmMasterBarang.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   6150
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4545
      Picture         =   "frmMasterBarang.frx":0A64
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   6150
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdHistory 
      Caption         =   "&History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   7515
      Picture         =   "frmMasterBarang.frx":0F96
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   6825
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5970
      Picture         =   "frmMasterBarang.frx":1098
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   5355
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CheckBox chkAktif 
      Caption         =   "Aktif"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   180
      TabIndex        =   25
      Top             =   5970
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   1860
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   795
      Picture         =   "frmMasterBarang.frx":119A
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   6825
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3870
      Picture         =   "frmMasterBarang.frx":129C
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2340
      Picture         =   "frmMasterBarang.frx":139E
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   6825
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5355
      Picture         =   "frmMasterBarang.frx":14A0
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   6825
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1845
      MaxLength       =   70
      TabIndex        =   1
      Top             =   585
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Index           =   0
      Left            =   1860
      MaxLength       =   35
      TabIndex        =   0
      Top             =   180
      Width           =   1950
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8100
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Group"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   49
      Top             =   1890
      Width           =   810
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   39
      Top             =   1410
      Width           =   810
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1605
      TabIndex        =   38
      Top             =   1440
      Width           =   150
   End
   Begin VB.Label Label28 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1605
      TabIndex        =   32
      Top             =   1005
      Width           =   150
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1605
      TabIndex        =   31
      Top             =   630
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1605
      TabIndex        =   30
      Top             =   180
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   29
      Top             =   6495
      Width           =   2130
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Satuan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   28
      Top             =   1005
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   27
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   40
      Top             =   225
      Width           =   1140
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim foc As Byte
Dim colname() As String

Private Sub cmbField_Click(Index As Integer)
If Index = 1 And cmbField(1).text <> "" Then
    If cmbField(1).text = UCase("Barang Jadi") Then
        frHarga.Visible = False
        FrHrgJual.Visible = True
    ElseIf cmbField(1).text = UCase("Sheet") Or cmbField(1).text = UCase("roll") Then
        frHarga.Visible = True
        FrHrgJual.Visible = False
    Else
        frHarga.Visible = False
        FrHrgJual.Visible = False
    End If
End If
End Sub

Private Sub cmdBaru_Click()
    reset_form
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
'Dim rs As New ADODB.Recordset
'Dim conn As New ADODB.Connection
On Error GoTo err
   If txtField(0).text = "" Then
    MsgBox "Silahkan masukkan Kode bahan terlebih dahulu!", vbCritical
    txtField(0).SetFocus
    Exit Sub
   End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    'rs.Open "select * from (select [kode_bahan] from t_penjualand where [kode_bahan]='" & txtField(0).text & "' union select [kode_bahan] from t_pembeliand where [kode_bahan]='" & txtField(0).text & "' union select [kode_bahan] from t_stockopnamed where [kode_bahan]='" & txtField(0).text & "' union select [kode_bahan] from t_adjustmentd where [kode_bahan]='" & txtField(0).text & "' union select [kode_bahan] from t_returbelid where [kode_bahan]='" & txtField(0).text & "' union select [kode_bahan] from t_returjuald where [kode_bahan]='" & txtField(0).text & "') a", conn
    rs.Open "select  distinct [kode_bahan],nomer_serial from stock where [kode_bahan]='" & txtField(0).text & "' and stock <=0", conn
    If Not rs.EOF Then
        While Not rs.EOF
    '        conn.Execute "update ms_bahan set flag='0' where [kode_bahan]='" & txtField(0).text & "'"
    '    Else
            conn.Execute "delete from ms_bahan where [kode_bahan]='" & txtField(0).text & "'"
            conn.Execute "delete from stock where [kode_bahan]='" & txtField(0).text & "' and nomer_serial='" & rs!nomer_serial & "'"
    '        conn.Execute "delete from hpp where [kode_bahan]='" & txtField(0).text & "'"
            rs.MoveNext
        Wend
       MsgBox "Data sudah dihapus"
    Else
         MsgBox "Data tidak dapat dihapus!"
    End If
    If rs.State Then rs.Close

    'conn.Execute "insert into HST_ms_bahan values ('" & txtField(0).Text & "','" & Replace(txtField(1).Text, "'", "''") & "','" & Replace(txtField(2).Text, "'", "''") & "','" & Trim(Right(cmbField(0).Text, 20)) & "','" & Trim(Right(cmbField(3).Text, 20)) & "','" & cmbField(1).Text & "','" & cmbField(2).Text & "','" & txtField(3).Text & "','" & txtField(4).Text & "','" & txtField(5).Text & "','" & txtField(6).Text & "','" & chkAktif.value & "','Delete','" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "','" & user & "')"
    conn.CommitTrans
    i = 0
    
    
    DropConnection
    Set conn = Nothing
    cmdPrev_Click
    cmdNext_Click
    'reset_form
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    conn.RollbackTrans
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdHistory_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [kode_bahan] , nama_bahan , " & _
    "PriceList,Harga1,Harga2,H_Eceran,userid,tanggal   from hst_ms_bahan" ' order by [kode_bahan]"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = ""
    frmSearch.proc = ""
    frmSearch.col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    frmSearch.chkDate.Visible = True
    frmSearch.DTPicker1.Visible = True
    frmSearch.Show vbModal

End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_bahan] from ms_bahan where [kode_bahan]>'" & txtField(0).text & "' order by [kode_bahan]", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection

    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode_bahan] from ms_bahan where [kode_bahan]<'" & txtField(0).text & "' order by [kode_bahan] desc", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
'    frmPrintPLU.kode = txtField(0).text
'    frmPrintPLU.Show
    frmPrintListBarang.Show vbModal
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select kode_bahan , nama_bahan , " & _
    "Kategori   from ms_bahan"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtField"
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchGroup_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select *   from var_groupGramatur" ' order by [kode_bahan]"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtGroup"
    frmSearch.proc = "cari_group"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 1
    frmSearch.chkDate.Visible = True
    frmSearch.DTPicker1.Visible = True
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
'On Error GoTo err
    i = 0


    If Trim(lblStock.text) = "" Then lblStock.text = 0
    If Trim(lblHPP.text) = "" Then lblHPP.text = 0

    For J = 0 To 2
    If txtField(J).text = "" Then
        MsgBox "semua field yang bertanda * harus diisi"
        txtField(J).SetFocus
        Exit Sub
    End If
    Next J
    If txtField(5).text = "" Then
        MsgBox "semua field yang bertanda * harus diisi"
        txtField(5).SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
'    rs.Open "select * from stock where [kode_bahan]='" & txtField(0).text & "'", conn
'    If rs.EOF Then
'        rs.Close
'        conn.Execute "insert into stock  (kode_gudang,[kode_bahan],stock,hpp)  select kode_gudang,'" & txtField(0).text & "',0,'" & Format(lblHPP.text, "###0") & "' from var_gudang"
'    End If
'    If rs.State Then rs.Close
    
'    rs.Open "select * from hst_hpp  where [kode_bahan]='" & txtField(0).text & "'", conn
'    If rs.EOF Then
'        rs.Close
''            conn.Execute "insert into hpp  (gudang,[kode_bahan],nomer_serial)  select gudang,'" & txtField(0).text & "','' from var_gudang"
'        conn.Execute "insert into hst_hpp  (jenis,kode_gudang,id,tanggal,kode_bahan,stockawal,HPPawal,Qty,Harga,HPP,StockAkhir)  " & _
'                    "values ('Awal','" & gudang & "','-','" & Format(Now, "yyyy/MM/dd hh:mm:ss") & "','" & txtField(0).text & "',0,0,'" & CDbl(lblStock.text) & "','" & CDbl(lblHPP.text) & "','" & CDbl(lblHPP.text) & "','" & CDbl(lblStock.text) & "' )"
'
'    End If
'    If rs.State Then rs.Close
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        add_dataheader 2, conn
        add_dataheaderRpt 2, conn
        add_dataAcc 2, conn
    Else
        add_dataheader 1, conn
        add_dataheaderRpt 1, conn
        add_dataAcc 1, conn
    End If
    If rs.State Then rs.Close

    
    If rs.State Then rs.Close
    If lblHPP.Locked = False And lblStock.Locked = False Then
        conn.Execute "update stock set stock=" & lblStock & ",hpp=" & Format(lblHPP, "###0") & " where [kode_bahan]='" & txtField(0).text & "' and kode_gudang='" & gudang & "'"
        conn.Execute "update hst_hpp set qty=" & lblStock & ",harga=" & lblHPP & ",hpp=" & lblHPP & ",stockakhir=" & lblStock & " where [kode_bahan]='" & txtField(0).text & "' and kode_gudang='" & gudang & "' and id='Awal'"
    End If
    conn.CommitTrans
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form

    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = "0"
    txtField(3).text = "0"
    txtField(4).text = "0"
    txtField(5).text = "0"
    txtHrg2 = 0
    txtHrg3 = 0
    txtHrgJual1 = 0
    txtHrgJual2 = 0
    txtHrgJual3 = 0
    txtHrgJual4 = 0
    
    txtGroup.text = ""
    cmbField(0).ListIndex = 0
    cmbField(1).ListIndex = 0
    lblStock.Locked = False
    lblHPP.Locked = False
    lblStock = "0"
    lblHPP = "0"
    txtField(0).SetFocus
    cmdPrev.Enabled = False
    cmdNext.Enabled = False
End Sub
Private Sub add_dataheader(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(15)
    ReDim nilai(15)

    table_name = "ms_bahan"
    fields(0) = "[kode_bahan]"
    fields(1) = "nama_bahan"
    fields(2) = "SATUAN"
    fields(3) = "[KATEGORI]"
    fields(4) = "panjang"
    fields(5) = "lebar"
    fields(6) = "berat"
    fields(7) = "harga_belia"
    fields(8) = "harga_belib"
    fields(9) = "harga_belic"
    fields(10) = "harga_juala"
    fields(11) = "harga_jualb"
    fields(12) = "harga_jualc"
    fields(13) = "harga_juald"
    fields(14) = "kode_group"

    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(0).text
    nilai(3) = cmbField(1).text
    nilai(4) = Replace(txtField(3).text, ",", ".")
    nilai(5) = Replace(txtField(4).text, ",", ".")
    nilai(6) = Replace(txtField(5).text, ",", ".")
    nilai(7) = Replace(txtField(2).text, ",", ".")
    nilai(8) = Replace(txtHrg2.text, ",", ".")
    nilai(9) = Replace(txtHrg3.text, ",", ".")
    nilai(10) = Replace(txtHrgJual1.text, ",", ".")
    nilai(11) = Replace(txtHrgJual2.text, ",", ".")
    nilai(12) = Replace(txtHrgJual3.text, ",", ".")
    nilai(13) = Replace(txtHrgJual4.text, ",", ".")
    
    nilai(14) = txtGroup.text
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_dataheaderRpt(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(7)
    ReDim nilai(7)

    table_name = "rpt_ms_bahan"
    fields(0) = "[kode_bahan]"
    fields(1) = "nama_bahan"
    fields(2) = "SATUAN"
    fields(3) = "[KATEGORI]"
    fields(4) = "panjang"
    fields(5) = "lebar"
    fields(6) = "berat"

    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(0).text
    nilai(3) = cmbField(1).text
    nilai(4) = Replace(txtField(3).text, ",", ".")
    nilai(5) = Replace(txtField(4).text, ",", ".")
    nilai(6) = Replace(txtField(5).text, ",", ".")

    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_dataAcc(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)

    table_name = "setting_accbahan"
    fields(0) = "[kode_bahan]"
    fields(1) = "acc_persediaan"
    fields(2) = "acc_hpp"
    
    nilai(0) = txtField(0).text
    nilai(1) = ""
    nilai(2) = ""
    
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
'    update_data table_name, fields, nilai, "[kode_bahan]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_datahst(cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(13)
    ReDim nilai(13)

    table_name = "HST_ms_bahan"
    fields(0) = "[kode barang]"
    fields(1) = "nama_bahan"
    fields(2) = "[KATEGORI]"
    fields(3) = "SATUAN"
    fields(4) = "merk"
    fields(5) = "harga1"
    fields(6) = "harga2"
    fields(7) = "h_eceran"
    fields(8) = "flag"
    fields(9) = "userid"
    fields(10) = "tanggal"
    fields(11) = "PriceList"
    fields(12) = "Barcode"


    nilai(0) = txtField(5).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(0).text
    nilai(3) = cmbField(1).text
    nilai(4) = cmbField(2).text
    nilai(5) = txtField(2).text
    nilai(6) = txtField(3).text
    nilai(7) = txtField(4).text
    nilai(8) = chkAktif
    nilai(9) = User
    nilai(10) = Format(Now, "yyyy/mm/dd hh:mm:ss")
    nilai(11) = txtField(6).text
    nilai(12) = txtField(0).text

    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Public Sub cari_data()
    If txtField(0).text = "" Then Exit Sub
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from ms_bahan  " & _
            "where kode_bahan='" & txtField(0).text & "' ", conn

    If Not rs.EOF Then
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        txtField(1).text = rs("nama_bahan")
        txtField(2).text = rs("harga_belia")
        txtField(3).text = rs("panjang")
        txtField(4).text = rs("lebar")
        txtField(5).text = rs("berat")
        txtHrg2.text = rs("harga_belib")
        txtHrg3.text = rs("harga_belic")
        txtHrgJual1.text = rs("harga_juala")
        txtHrgJual2.text = rs("harga_jualb")
        txtHrgJual3.text = rs("harga_jualc")
        txtHrgJual4.text = rs("harga_juald")
        
        If Not IsNull(rs!kode_group) Then txtGroup = rs!kode_group Else txtGroup = ""


        SetComboTextRight rs(2), cmbField(1)
        SetComboTextRight rs(3), cmbField(0)
'        SetComboTextRight rs(4), cmbField(1)
'        chkAktif.Value = IIf(IsNull(rs("flag")), 1, rs("flag"))
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        If rs.State Then rs.Close
        lblHPP.Locked = True
'        lblStock = getStockKertas(txtField(0).text, gudang, "", strcon)
        
'        lblHPP = getHppKertas(txtField(0).text, conn)
        lblStock.Locked = True
    Else
        cmdPrev.Enabled = False
        cmdPrev.Enabled = False
        txtField(1).text = ""
        txtGroup = ""
        
        txtField(3).text = "0"
        txtField(4).text = "0"
        txtField(2).text = "0"
        txtField(5).text = "0"
        txtHrg2 = 0
        txtHrg3 = 0
        txtHrgJual1 = 0
        txtHrgJual2 = 0
        txtHrgJual3 = 0
        txtHrgJual4 = 0

        cmdHapus.Enabled = False
        mnuHapus.Enabled = True
        lblHPP = 0
        lblStock = 0
        lblHPP.Locked = False
        lblStock.Locked = False
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cari_group()
 conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from var_groupGramatur where kode_group='" & txtGroup.text & "' ", conn
    If Not rs.EOF Then
        txtField(5).text = rs!berat
    Else
        txtField(5).text = ""
    End If
    rs.Close
    conn.Close
End Sub
Private Sub load_combo()
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(1).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close

    rs.Open "select * from var_satuan order by satuan", conn
    While Not rs.EOF
        cmbField(0).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close

    If cmbField(0).ListCount > 0 Then cmbField(0).ListIndex = 0
    If cmbField(1).ListCount > 0 Then cmbField(1).ListIndex = 0

    conn.Close
End Sub



Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
 
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
'    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        If foc = 1 And txtField(0).text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If

End Sub

Private Sub Form_Load()
    load_combo
    reset_form

End Sub

Private Sub lblHPP_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub lblStock_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub


Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index).text)
    foc = 1
End Sub

Private Sub lblHPP_GotFocus()
    lblHPP.SelStart = 0
    lblHPP.SelLength = Len(lblHPP.text)
    foc = 1
End Sub

Private Sub lblStock_GotFocus()
    lblStock.SelStart = 0
    lblStock.SelLength = Len(lblStock.text)
    foc = 1
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Index = 0 Then
            cari_data
        End If
    ElseIf Index = 2 Then
        Angka KeyAscii
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    foc = 0
    If Index = 0 And txtField(0).text <> "" Then cari_data
    If Index = 5 And txtField(5).text <> "" Then
'        If Not cek_barcode(txtField(0).text, txtField(5).text) Then
'            MsgBox "Barcode tersebut sudah pernah digunakan sebelumnya"
'            txtField(5).SetFocus
'        End If
    End If
'    If Index = 8 Then
'        If txtField(Index).text <> "" Then
'        If Not cek_barcode(txtField(0).text, txtField(Index).text) Then
'            txtField(Index).SetFocus
'            MsgBox "Barcode yang anda masukkan sudah dipakai sebelumnya"
'            txtField(Index).SelStart = 0
'            txtField(Index).SelLength = Len(txtField(Index).text)
'        End If
'        End If
'    End If

End Sub


Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
        MsgBox "Loading selesai"
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Function loadexcelfile1(filename As String) As Boolean
On Error GoTo err
Dim exapp As Object
Dim range As String
Dim rs2 As New ADODB.Recordset
Dim col As Byte
Dim i As Integer
loadexcelfile1 = False
Set exapp = CreateObject("excel.application")
exapp.Workbooks.Open filename
exapp.Sheets(1).Select
conn.Open strcon
col = Asc("A")
range = Chr(col) & "1"

While exapp.Workbooks.Application.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exapp.Workbooks.Application.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

i = 2
While exapp.range("A" & CStr(i)) <> ""
        rs2.Open "select * from var_kategori where kategori='" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_bahan where [kode_bahan]='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            newitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))
        Else
            updateitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))

        End If
        rs2.Close
'        rs2.Open "select * from stock where [kode_bahan]='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
'        If rs2.EOF Then
'        conn.Execute "insert into stock  (kode_gudang,[kode_bahan],stock)  values ('" & gudang & "'," & _
'            "'" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
'            "'" & exapp.range(Chr(getcolindex("stok", colname) + 65) & CStr(i)) & "')"
'        End If
'        rs2.Close
'        rs2.Open "select * from hpp where [kode_bahan]='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
'        If rs2.EOF Then
'        conn.Execute "insert into hpp (kode_gudang,[kode_bahan],hpp)  values ('" & gudang & "'," & _
'            "'" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
'            "" & IIf(IsNumeric(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), CDbl(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), 0) & ")"
'        End If
'        rs2.Close

    i = i + 1
Wend
exapp.Workbooks.Close
loadexcelfile1 = True
exapp.Application.Quit
Set exapp = Nothing

MsgBox "Success"
conn.Close
Exit Function
err:
MsgBox err.Description
Resume Next
''If conn.State Then conn.Close
''If exapp.Workbooks.Count > 0 Then exapp.Workbooks.Close
''exapp.Application.Quit
''Set exapp = Nothing
End Function
Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset

Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=Excel 8.0"
con.Open stcon
conn.Open strcon
rs.Open "select * from [sheet1$]", con
While Not rs.EOF
    If rs(0) <> "E" Then
        rs2.Open "select * from var_kategori where kategori='" & rs("kategori") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & rs("kategori") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & rs("merk") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & rs("merk") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & rs("satuan") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & rs("satuan") & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_bahan where [kode_bahan]='" & rs("kodebarang") & "'", conn
        If rs2.EOF Then
            newitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")

            conn.Execute "insert into stock  (kode_gudang,[kode_bahan],stock)  select gudang,'" & rs("kodebarang") & "','" & rs("stok") & "' from var_gudang"
            conn.Execute "insert into hpp (kode_gudang,[kode_bahan],hpp)  select gudang,'" & rs("kodebarang") & "','" & rs("hargapokok") & "' from var_gudang"
        Else
            updateitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")
        End If
        rs2.Close
    End If
    rs.MoveNext
Wend
rs.Close
con.Close
DropConnection
Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub


Private Sub txtGroup_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchGroup_Click
End Sub


Private Sub txtHrgJual1_GotFocus()
   txtHrgJual1.SelStart = 0
   txtHrgJual1.SelLength = Len(txtHrgJual1)
End Sub

Private Sub txtHrgJual1_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub
Private Sub txtHrgJual2_GotFocus()
   txtHrgJual2.SelStart = 0
   txtHrgJual2.SelLength = Len(txtHrgJual2)
End Sub

Private Sub txtHrgJual2_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub
Private Sub txtHrgJual3_GotFocus()
   txtHrgJual3.SelStart = 0
   txtHrgJual3.SelLength = Len(txtHrgJual3)
End Sub

Private Sub txtHrgJual3_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub
Private Sub txtHrgJual4_GotFocus()
   txtHrgJual4.SelStart = 0
   txtHrgJual4.SelLength = Len(txtHrgJual4)
End Sub

Private Sub txtHrgJual4_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub

Private Sub txtHrg2_GotFocus()
   txtHrg2.SelStart = 0
   txtHrg2.SelLength = Len(txtHrg2)
End Sub

Private Sub txtHrg2_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub
Private Sub txtHrg3_GotFocus()
   txtHrg3.SelStart = 0
   txtHrg3.SelLength = Len(txtHrg3)
End Sub

Private Sub txtHrg3_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub
