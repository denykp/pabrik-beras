alter table var_usergroup
add export varchar(1)
go
alter table var_usergroup
add delete_master varchar(1)
go
update var_usergroup set export = 0,delete_master = 0