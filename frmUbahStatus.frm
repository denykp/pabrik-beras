VERSION 5.00
Begin VB.Form frmUbahStatus 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ubah Status Sales Order"
   ClientHeight    =   2460
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4560
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   4560
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Appearance      =   0  'Flat
      Caption         =   "Ok"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1395
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   2
      Top             =   1800
      Width           =   1455
   End
   Begin VB.ComboBox cmbStatusBaru 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmUbahStatus.frx":0000
      Left            =   1575
      List            =   "frmUbahStatus.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1170
      Width           =   2355
   End
   Begin VB.TextBox txtNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   180
      Width           =   1875
   End
   Begin VB.CommandButton cmdSearchPO 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3465
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   3
      Top             =   180
      Width           =   420
   End
   Begin VB.Label Label2 
      Caption         =   "Status Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   7
      Top             =   1170
      Width           =   1410
   End
   Begin VB.Label lblStatus 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1575
      TabIndex        =   6
      Top             =   675
      Width           =   2310
   End
   Begin VB.Label Label1 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   5
      Top             =   675
      Width           =   1410
   End
   Begin VB.Label Label7 
      Caption         =   "No.Order"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   4
      Top             =   195
      Width           =   1410
   End
End
Attribute VB_Name = "frmUbahStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub cek_order()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select * from t_soh where nomer_so='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        Select Case rs("status")
        Case "0": lblStatus = "PENDING"
        Case "1": lblStatus = "SDH PRINT"
        Case "2": lblStatus = "BATAL"
        Case "3": lblStatus = "FINISH"
        End Select
    End If
    rs.Close
    conn.Close
End Sub

Private Sub cmdOk_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select * from t_jualh where nomer_order='" & txtNoOrder & "'", conn, adOpenForwardOnly
    If Not rs.EOF Then
        If rs!status_posting = 1 Then
            MsgBox "Tidak bisa diubah karena order tersebut sudah dibuat invoice dan telah diposting"
            Exit Sub
        End If
    End If
    rs.Close
    conn.Execute "update t_soh set status='" & Left(cmbStatusBaru.text, 1) & "' where nomer_so='" & txtNoOrder & "'"
    conn.Close
    reset_form
End Sub
Private Sub reset_form()
    lblStatus = ""
    txtNoOrder = ""
End Sub
Private Sub cmdSearchPO_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchSO
    frmSearch.nmform = "frmUbahStatus"
    frmSearch.nmctrl = "txtnoorder"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "soh"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_order"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub txtNoOrder_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPO_Click
End Sub

Private Sub txtNoOrder_LostFocus()
    cek_order
End Sub
