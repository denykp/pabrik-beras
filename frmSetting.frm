VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSetting 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Setting"
   ClientHeight    =   2205
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6990
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   6990
   Begin VB.CommandButton Command2 
      Caption         =   "Hitung Ulang Pembayaran Hutang Piutang"
      Height          =   660
      Left            =   4740
      TabIndex        =   6
      Top             =   1320
      Width           =   2100
   End
   Begin VB.CommandButton Command1 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5715
      Picture         =   "frmSetting.frx":0000
      TabIndex        =   5
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   420
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1710
      TabIndex        =   2
      Top             =   270
      Width           =   3930
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2925
      Picture         =   "frmSetting.frx":0102
      TabIndex        =   1
      Top             =   1305
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   540
      Picture         =   "frmSetting.frx":0204
      TabIndex        =   0
      Top             =   1305
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6525
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Report Files"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   4
      Top             =   315
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   3
      Top             =   315
      Width           =   105
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmSetting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdKeluar_Click()
    Unload Me
End Sub
Private Sub loadfolder()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*."
    CommonDialog1.filename = "Folder"
    CommonDialog1.ShowOpen
    
    txtField(1) = fs.GetParentFolderName(CommonDialog1.filename)
    
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
        
    writeINI App.Path & "\setting.ini", "setting", "Report Folder", txtField(1)
    reportDir = txtField(1)
    Exit Sub
err:
    MsgBox err.Description
End Sub
Public Sub cari_data()
    txtField(1) = sGetINI(App.Path & "\setting.ini", "setting", "Report Folder", "")
'    conn.ConnectionString = strcon
'    conn.Open
'    rs.Open "select * from setting", conn
'    If Not rs.EOF Then
'        txtField(0) = rs(0)
'    Else
'        txtField(0) = 0
'    End If
'    rs.Close
'    conn.Close
End Sub

Private Sub Command1_Click()
    loadfolder
End Sub

Private Sub Command2_Click()
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
    conn.Open strcon
    rs.Open "select nomer_bayarpiutang,sum(nilai_transfer) as total_transfer from t_bayarPiutang_transfer " & _
            "group by nomer_bayarpiutang ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update t_bayarpiutangh set nilaitransfer=" & rs("total_transfer") & " " & _
                     "where nomer_bayarpiutang='" & rs("nomer_bayarpiutang") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select nomer_bayarpiutang,sum(nilai_cek) as total_cek from t_bayarPiutang_cek " & _
            "group by nomer_bayarpiutang ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update t_bayarpiutangh set nilai_cek=" & rs("total_cek") & " " & _
                     "where nomer_bayarpiutang='" & rs("nomer_bayarpiutang") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Execute "update t_bayarpiutangh set jumlah_bayar=jumlah_tunai+nilaitransfer+nilai_cek"
    
    
    rs.Open "select nomer_bayarhutang,sum(nilai_transfer) as total_transfer from t_bayarhutang_transfer " & _
            "group by nomer_bayarhutang ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update t_bayarhutangh set nilaitransfer=" & rs("total_transfer") & " " & _
                     "where nomer_bayarhutang='" & rs("nomer_bayarhutang") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select nomer_bayarhutang,sum(nilai_cek) as total_cek from t_bayarhutang_cek " & _
            "group by nomer_bayarhutang ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update t_bayarhutangh set nilai_cek=" & rs("total_cek") & " " & _
                     "where nomer_bayarhutang='" & rs("nomer_bayarhutang") & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Execute "update t_bayarhutangh set jumlah_bayar=jumlah_tunai+nilaitransfer+nilai_cek"
    conn.Close
    
    MsgBox "Proses Selesai !"
    
    
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    cari_data
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub
