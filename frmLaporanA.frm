VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmLaporanA 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Out Laporan"
   ClientHeight    =   7200
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7200
   ScaleWidth      =   9000
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkTglKirim 
      Caption         =   "Tgl. Kirim"
      Height          =   270
      Left            =   5280
      TabIndex        =   96
      Top             =   1695
      Width           =   2205
   End
   Begin VB.Frame FrStatus2 
      Caption         =   "Status"
      Height          =   1125
      Left            =   7320
      TabIndex        =   92
      Top             =   5820
      Width           =   1530
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'None
         Height          =   600
         Left            =   135
         ScaleHeight     =   600
         ScaleWidth      =   1365
         TabIndex        =   93
         Top             =   255
         Width           =   1365
         Begin VB.OptionButton OptStatus 
            Caption         =   "Semua"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   30
            TabIndex        =   95
            Top             =   -15
            Width           =   1635
         End
         Begin VB.OptionButton OptStatus 
            Caption         =   "Expired"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   30
            TabIndex        =   94
            Top             =   285
            Value           =   -1  'True
            Width           =   960
         End
      End
   End
   Begin VB.CheckBox cHarga 
      Caption         =   "Dengan Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7305
      TabIndex        =   91
      Top             =   5250
      Width           =   1545
   End
   Begin VB.CheckBox chkActiveStock 
      Caption         =   "Active Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6900
      TabIndex        =   90
      Top             =   1515
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.Frame frNoTrans 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   86
      Top             =   5220
      Visible         =   0   'False
      Width           =   3780
      Begin VB.TextBox txtNoTrans 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   87
         Top             =   120
         Width           =   2220
      End
      Begin VB.Label Label30 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   89
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label4 
         Caption         =   "No Trans"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   88
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame frMesin 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   105
      TabIndex        =   73
      Top             =   4800
      Visible         =   0   'False
      Width           =   4440
      Begin VB.CommandButton cmdSearchMesin 
         Caption         =   "..."
         Height          =   375
         Left            =   3630
         TabIndex        =   75
         Top             =   30
         Width           =   420
      End
      Begin VB.TextBox txtMesin 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   74
         Top             =   45
         Width           =   2325
      End
      Begin VB.Label Label29 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   77
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label28 
         Caption         =   "Mesin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   150
         TabIndex        =   76
         Top             =   90
         Width           =   870
      End
   End
   Begin VB.Frame frOperator 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   135
      TabIndex        =   68
      Top             =   4335
      Visible         =   0   'False
      Width           =   4440
      Begin VB.TextBox txtOpr 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   70
         Top             =   45
         Width           =   2325
      End
      Begin VB.CommandButton cmdOperator 
         Caption         =   "..."
         Height          =   375
         Left            =   3630
         TabIndex        =   69
         Top             =   30
         Width           =   420
      End
      Begin VB.Label Label27 
         Caption         =   "Operator"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   72
         Top             =   90
         Width           =   870
      End
      Begin VB.Label Label26 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   71
         Top             =   90
         Width           =   105
      End
   End
   Begin VB.Frame FrBahanHasil 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   120
      TabIndex        =   62
      Top             =   3375
      Visible         =   0   'False
      Width           =   4395
      Begin VB.CommandButton cmdSearchHsl 
         Caption         =   "..."
         Height          =   375
         Left            =   3600
         TabIndex        =   67
         Top             =   0
         Width           =   420
      End
      Begin VB.TextBox txtKodeHsl 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   63
         Top             =   45
         Width           =   2325
      End
      Begin VB.Label Label23 
         Caption         =   "Kode Hasil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   65
         Top             =   90
         Width           =   870
      End
      Begin VB.Label Label22 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   64
         Top             =   90
         Width           =   105
      End
   End
   Begin VB.Frame frStatus 
      Caption         =   "Status"
      Height          =   1335
      Left            =   6165
      TabIndex        =   61
      Top             =   45
      Visible         =   0   'False
      Width           =   1950
      Begin VB.OptionButton OptPosting 
         Caption         =   "Belum Posting"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   150
         TabIndex        =   99
         Top             =   885
         Width           =   1650
      End
      Begin VB.OptionButton OptPosting 
         Caption         =   "Sudah Posting"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   150
         TabIndex        =   98
         Top             =   570
         Width           =   1680
      End
      Begin VB.OptionButton OptPosting 
         Caption         =   "Semua"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   150
         TabIndex        =   97
         Top             =   255
         Value           =   -1  'True
         Width           =   1635
      End
   End
   Begin VB.Frame frKodePlat 
      BorderStyle     =   0  'None
      Height          =   1020
      Left            =   8400
      TabIndex        =   53
      Top             =   7170
      Visible         =   0   'False
      Width           =   4125
      Begin VB.CommandButton Command1 
         Caption         =   "..."
         Height          =   375
         Left            =   3420
         TabIndex        =   60
         Top             =   120
         Width           =   420
      End
      Begin VB.TextBox txtNoSerialPlate 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         Locked          =   -1  'True
         TabIndex        =   58
         Top             =   525
         Width           =   1815
      End
      Begin VB.CommandButton cmdSearchSerial 
         BackColor       =   &H8000000C&
         Caption         =   "..."
         Height          =   360
         Left            =   3420
         Picture         =   "frmLaporanA.frx":0000
         TabIndex        =   57
         Top             =   540
         Width           =   420
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1530
         TabIndex        =   54
         Top             =   135
         Width           =   1545
      End
      Begin VB.Label Label21 
         Caption         =   "No. Serial Plat :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   59
         Top             =   555
         Width           =   1395
      End
      Begin VB.Label Label20 
         Caption         =   "Plat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   56
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label19 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1380
         TabIndex        =   55
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.CheckBox chkGroupby 
      Caption         =   "Group By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4755
      TabIndex        =   15
      Top             =   5280
      Width           =   1005
   End
   Begin VB.Frame frGroup 
      Height          =   1515
      Left            =   4800
      TabIndex        =   45
      Top             =   5535
      Visible         =   0   'False
      Width           =   2445
      Begin VB.OptionButton optGroup 
         Caption         =   "per Customer per Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   45
         TabIndex        =   19
         Top             =   1080
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Item per Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   45
         TabIndex        =   18
         Top             =   765
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   45
         TabIndex        =   17
         Top             =   450
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   30
         TabIndex        =   16
         Top             =   150
         Width           =   2310
      End
   End
   Begin VB.Frame frSupCus 
      BorderStyle     =   0  'None
      Height          =   1635
      Left            =   4545
      TabIndex        =   41
      Top             =   3645
      Visible         =   0   'False
      Width           =   4440
      Begin VB.ListBox cmbSupCus 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Left            =   1260
         MultiSelect     =   1  'Simple
         TabIndex        =   85
         Top             =   150
         Width           =   2895
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2790
         ScaleHeight     =   375
         ScaleWidth      =   465
         TabIndex        =   42
         Top             =   135
         Width           =   465
      End
      Begin VB.Label lblSupCus 
         Caption         =   "Supplier"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   210
         TabIndex        =   44
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label16 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   43
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame frKodeBarang 
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   120
      TabIndex        =   37
      Top             =   2505
      Visible         =   0   'False
      Width           =   4440
      Begin VB.ComboBox cmbSerial 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   78
         Top             =   450
         Width           =   2355
      End
      Begin VB.CommandButton cmdSearchKdBrg 
         Caption         =   "..."
         Height          =   375
         Left            =   3630
         TabIndex        =   66
         Top             =   30
         Width           =   420
      End
      Begin VB.TextBox txtKdBarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1275
         TabIndex        =   4
         Top             =   45
         Width           =   2325
      End
      Begin VB.Label Label24 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1095
         TabIndex        =   80
         Top             =   480
         Width           =   105
      End
      Begin VB.Label Label25 
         Caption         =   "No.Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   105
         TabIndex        =   79
         Top             =   480
         Width           =   870
      End
      Begin VB.Label Label15 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1140
         TabIndex        =   39
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Kode Bahan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   90
         TabIndex        =   38
         Top             =   90
         Width           =   975
      End
   End
   Begin VB.Frame frUserID 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   3375
      TabIndex        =   34
      Top             =   855
      Visible         =   0   'False
      Width           =   2310
      Begin VB.ComboBox cmbUserID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   810
         TabIndex        =   14
         Top             =   135
         Width           =   1320
      End
      Begin VB.Label Label13 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   675
         TabIndex        =   36
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label12 
         Caption         =   "User"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   35
         Top             =   180
         Width           =   510
      End
   End
   Begin VB.Frame frMember 
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   105
      TabIndex        =   31
      Top             =   5805
      Visible         =   0   'False
      Width           =   3435
      Begin VB.CommandButton cmdSearchCustomer 
         Caption         =   "..."
         Height          =   375
         Left            =   2745
         TabIndex        =   46
         Top             =   90
         Width           =   420
      End
      Begin VB.TextBox txtKodeCustomer 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   6
         Top             =   135
         Width           =   1545
      End
      Begin VB.Label Label11 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   33
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label10 
         Caption         =   "Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   32
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame frBulan 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   180
      TabIndex        =   28
      Top             =   1530
      Visible         =   0   'False
      Width           =   4830
      Begin VB.ComboBox cmbTahun 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2265
         TabIndex        =   3
         Top             =   90
         Width           =   1050
      End
      Begin VB.ComboBox cmbBulan 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmLaporanA.frx":0102
         Left            =   1215
         List            =   "frmLaporanA.frx":012A
         TabIndex        =   2
         Top             =   90
         Width           =   1005
      End
      Begin VB.Label Label7 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   30
         Top             =   135
         Width           =   105
      End
      Begin VB.Label Label2 
         Caption         =   "Bulan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   29
         Top             =   135
         Width           =   780
      End
   End
   Begin VB.OptionButton OptTipe 
      Caption         =   "per Tahun"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   2
      Left            =   180
      TabIndex        =   11
      Top             =   810
      Width           =   2715
   End
   Begin VB.Frame frTanggal 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   24
      Top             =   1530
      Width           =   4785
      Begin MSComCtl2.DTPicker DTDari 
         Height          =   330
         Left            =   1260
         TabIndex        =   0
         Top             =   90
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   48562179
         CurrentDate     =   38986
      End
      Begin MSComCtl2.DTPicker DTSampai 
         Height          =   330
         Left            =   3150
         TabIndex        =   1
         Top             =   90
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   48562179
         CurrentDate     =   38986
      End
      Begin VB.Label Label1 
         Caption         =   "Tanggal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   27
         Top             =   135
         Width           =   780
      End
      Begin VB.Label Label3 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1125
         TabIndex        =   26
         Top             =   135
         Width           =   105
      End
      Begin VB.Label Label6 
         Caption         =   "s/d"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2820
         TabIndex        =   25
         Top             =   135
         Width           =   285
      End
   End
   Begin VB.Frame frDetail 
      Height          =   780
      Left            =   3465
      TabIndex        =   23
      Top             =   0
      Width           =   1905
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   600
         Left            =   90
         ScaleHeight     =   600
         ScaleWidth      =   1455
         TabIndex        =   40
         Top             =   135
         Width           =   1455
         Begin VB.OptionButton Option2 
            Caption         =   "Rekap"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            TabIndex        =   13
            Top             =   270
            Value           =   -1  'True
            Width           =   1635
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Detail"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   1635
         End
      End
   End
   Begin VB.Frame frGudang 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   135
      TabIndex        =   20
      Top             =   3855
      Visible         =   0   'False
      Width           =   4215
      Begin VB.ComboBox cmbGudang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   5
         Top             =   45
         Width           =   2280
      End
      Begin VB.Label Label5 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   22
         Top             =   90
         Width           =   105
      End
      Begin VB.Label lblGudang 
         Caption         =   "Gudang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   21
         Top             =   90
         Width           =   870
      End
   End
   Begin VB.OptionButton OptTipe 
      Caption         =   "per Bulan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   180
      TabIndex        =   10
      Top             =   450
      Width           =   2715
   End
   Begin VB.OptionButton OptTipe 
      Caption         =   "Per Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   180
      TabIndex        =   9
      Top             =   90
      Value           =   -1  'True
      Width           =   2715
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   2550
      Picture         =   "frmLaporanA.frx":015E
      TabIndex        =   8
      Top             =   6570
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1215
      Picture         =   "frmLaporanA.frx":0260
      TabIndex        =   7
      Top             =   6585
      Width           =   1230
   End
   Begin VB.Frame FrAcc 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   47
      Top             =   1950
      Visible         =   0   'False
      Width           =   3780
      Begin VB.TextBox txtKodeAcc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   50
         Top             =   120
         Width           =   1545
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2910
         ScaleHeight     =   375
         ScaleWidth      =   1275
         TabIndex        =   48
         Top             =   90
         Width           =   1275
         Begin VB.CommandButton cmdSearchAcc 
            Caption         =   "..."
            Height          =   375
            Left            =   0
            TabIndex        =   49
            Top             =   0
            Width           =   480
         End
      End
      Begin VB.Label Label18 
         Caption         =   "Kode Acc"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   52
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label17 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   51
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame FrKategori 
      BorderStyle     =   0  'None
      Height          =   1620
      Left            =   4680
      TabIndex        =   81
      Top             =   1995
      Visible         =   0   'False
      Width           =   4155
      Begin VB.ListBox listKategori 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Left            =   1125
         MultiSelect     =   1  'Simple
         TabIndex        =   82
         Top             =   90
         Width           =   2895
      End
      Begin VB.Label Label8 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   945
         TabIndex        =   84
         Top             =   90
         Width           =   105
      End
      Begin VB.Label Label9 
         Caption         =   "Kategori"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   83
         Top             =   90
         Width           =   870
      End
   End
   Begin VB.Line Line1 
      X1              =   90
      X2              =   8280
      Y1              =   1485
      Y2              =   1485
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmLaporanA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public kategori As String
Public Tipe As Byte
Public status As String

Private Sub cmdSearchAcc_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_acc,nama from ms_coa"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodeAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cekAcc"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub cekAcc()
    'rwerw
End Sub

Private Sub cmdSearchCustomer_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = querycustomer
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodecustomer"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchHsl_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_bahan,nama_bahan,kategori from ms_bahan"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodeHsl"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
   
End Sub
Private Sub cmdOperator_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_operator,nama_operator from ms_operator"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtOpr"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchMesin_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_gudang,nama_gudang,jenis_mesin from ms_gudang where status=1"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtMesin"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub optGroup_Click(Index As Integer)
    If optGroup(0).value Or optGroup(2).value Or optGroup(3).value Then
    frKodeBarang.Visible = True
    FrKategori.Visible = True
    Else
    frKodeBarang.Visible = False
    FrKategori.Visible = False
    End If
    If (optGroup(1).value Or optGroup(2).value Or optGroup(3).value) And InStr(1, optGroup(i), "Customer") > 1 Then
    frMember.Visible = True
    Else
    frMember.Visible = False
    End If
End Sub



Private Sub chkGroupby_Click()
    If chkGroupby.value = "1" Then
        frGroup.Visible = True
    Else
        frGroup.Visible = False
    End If
End Sub


Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
Dim filtergudang, filtertanggal, filterbulan, filtertahun, filtercarabayar, filter3, filteruserid, filterkodebarang As String
Dim filterkategori, filtersupplier, filtersupcus, filtercustomer As String, filterkodeacc As String, filterPosting As String, filterFinish As String
Dim sBulanPrev As String, sTahunPrev As String
Dim FilterKodePlat As String, FilterSerialPlat As String
Dim filterkodeHasil, filterOperator, filterMesin, filterNo_serial, filterNopol, filterNoTrans As String
Dim filterKategoriCust As String
Dim filterExpired As String
Dim ext As String
Dim query() As String
Dim param3 As String
    Select Case filename
' Opaname
        Case "\Laporan Opname Kertas" ' via
           filtertanggal = "{t_stockOpnameH.tanggal}"
           filterkategori = "{rpt_ms_bahan.kategori}"
           filterkodebarang = "{t_stockOpnameD.kode_bahan}"
        Case "\Laporan Opname Tinta" ' via
           filtertanggal = "{t_stockopnameTintaH.tanggal}"
        Case "\Laporan Opname Plat" ' via
           filtertanggal = "{t_stockOpnamePlatH.tanggal}"

' koreksi
        Case "\Laporan Koreksi Kertas" ' via
           filtertanggal = "{t_stockKoreksiH.tanggal}"
           filterkategori = "{rpt_ms_bahan.kategori}"
           filterkodebarang = "{t_stockKoreksiD.kode_bahan}"
        Case "\Laporan Koreksi Tinta" ' via
           filtertanggal = "{t_stockoKoreksiTintaH.tanggal}"
        Case "\Laporan Koreksi Plat" ' via
           filtertanggal = "{t_stockKoreksiPlatH.tanggal}"
'mutasi
        Case "\Laporan Mutasi Tinta"
           filtertanggal = "{t_stockMutasiTintaH.tanggal}"
'Penjualan
        Case "\Laporan Penjualan Kertas"
            filtertanggal = "{t_jualKertasH.tanggal_jualKertas}"
            filtersupplier = "{t_jualKertasH.kode_customer}"
        Case "\Laporan Penjualan Kertas per Item":
            filtertanggal = "{t_jualKertasH.tanggal_jualKertas}"
            filtersupplier = "{t_jualKertasH.kode_customer}"
            filterkodebarang = "{t_jualKertasd.kode_bahan}"
'Pembelian
        Case "\Laporan Pembelian Kertas"
            filtertanggal = "{t_beliKertasH.tanggal_beliKertas}"
            filtersupplier = "{t_beliKertasH.kode_supplier}"
        Case "\Laporan Pembelian Roll"
            filtertanggal = "{t_beliRollH.tanggal_beliRoll}"
            filtersupplier = "{t_beliRollH.kode_supplier}"
        Case "\Laporan Pembelian Tinta"
            filtertanggal = "{t_beliTintaH.tanggal_beliTinta}"
            filtersupplier = "{t_beliTintaH.kode_supplier}"
        Case "\Laporan Pembelian Plat"
            filtertanggal = "{t_beliPlatH.tanggal_beliPlat}"
            filtersupplier = "{t_beliPlatH.kode_supplier}"
'Order kertas dan roll
        Case "\Laporan Order Roll"
            filtertanggal = "{vw_orderRoll.tanggal_OrderBeliRoll}"
            filterkodebarang = "{vw_orderRoll.kode_bahan}"
        Case "\Laporan Rekap Order Roll"
            filtertanggal = "{vw_orderRoll.tanggal_OrderBeliRoll}"
            filterkodebarang = "{vw_orderRoll.kode_bahan}"
        Case "\Laporan Order Kertas"
            filtertanggal = "{vw_orderKertas.tanggal_OrderBeliKertas}"
            filterkodebarang = "{vw_orderKertas.kode_bahan}"
        Case "\Laporan Rekap Order Kertas"
            filtertanggal = "{vw_orderKertas.tanggal_OrderBeliKertas}"
            filterkodebarang = "{vw_orderKertas.kode_bahan}"
        Case "\Laporan Coating Bungkus"
            filtertanggal = "{t_hasilcoatingbungkush.tanggal}"
'Retur Pembelian
        Case "\Laporan Retur Pembelian Kertas"
            filtertanggal = "{t_returbeliKertasH.tanggal_returbeliKertas}"
            filterkodebarang = "{t_returbeliKertasD.kode_bahan}"
        Case "\Laporan Retur Pembelian Tinta"
            filtertanggal = "{t_returbeliTintaH.tanggal_returbeliTinta}"
            filtersupplier = "{t_returbeliTintaH.kode_supplier}"
        Case "\Laporan Retur Pembelian Plat"
            filtertanggal = "{t_returbeliPlatH.tanggal_returbeliPlat}"
            filtersupplier = "{t_returbeliPlatH.kode_supplier}"
'Keuangan
        Case "\Laporan Hutang"
            filtersupplier = "{list_hutang.kode_supplier}"
        Case "\Laporan Piutang"
            filtersupplier = "{list_piutang.kode_customer}"
            filterKategoriCust = "{rpt_ms_customer.kategori}"
        Case "\Laporan Outstanding BG"
            filtersupplier = "{list_bg.kode_supplier}"
            filtercustomer = "{list_bg.kode_customer}"
        Case "\Laporan History Produksi"
'            ProsesHistoryProduksi DTDari.value, DTSampai.value
            filtertanggal = "{vw_analisaproduksi.tglpotong}"
        Case "\Laporan Analisa PotongCD"
            filtertanggal = "{t_potongcdh.tanggal}"
        Case "\Laporan Buku Besar"
            generate_BukuBesar DTDari, DTSampai, txtKodeAcc
'            filtertanggal = "{tmp_bukubesar.tanggal}"
            filterkodeacc = "{tmp_bukubesar.kode_acc}"
            'filternotrans="{t_jurnalh.no_transaksi}"
        Case "\Laporan Jurnal"
            filtertanggal = "{t_jurnalH.tanggal}"
            filterNoTrans = "{t_jurnalh.no_transaksi}"
        Case "\Laporan Pembayaran Hutang"
            filtertanggal = "{t_bayarhutangh.tanggal_bayarhutang}"
            filtersupplier = "{t_bayarhutangh.kode_supplier}"
        Case "\Laporan Penerimaan Piutang"
            filtertanggal = "{t_bayarpiutangh.tanggal_bayarpiutang}"
            filtersupplier = "{t_bayarpiutangh.kode_customerr}"
'        Case "\Neraca"
'            If cmbBulan.text = "" Or cmbTahun.text = "" Then
'                MsgBox "Bulan dan Tahun harus ditentukan dulu !", vbExclamation
'                Exit Sub
'            End If
'            HitungNeraca
        Case "\LabaRugi", "\Neraca"
            If cmbBulan.text = "" Or cmbTahun.text = "" Then
                MsgBox "Bulan dan Tahun harus ditentukan dulu !", vbExclamation
                Exit Sub
            End If
            
            If filename = "\Neraca" Then HitungNeraca
            
            Select Case Val(cmbBulan.text)
                Case 1
                    strPeriodeLaba = "JANUARI " & cmbTahun.text
                Case 2
                    strPeriodeLaba = "FEBRUARI " & cmbTahun.text
                Case 3
                    strPeriodeLaba = "MARET " & cmbTahun.text
                Case 4
                    strPeriodeLaba = "APRIL " & cmbTahun.text
                Case 5
                    strPeriodeLaba = "MEI " & cmbTahun.text
                Case 6
                    strPeriodeLaba = "JUNI " & cmbTahun.text
                Case 7
                    strPeriodeLaba = "JULI " & cmbTahun.text
                Case 8
                    strPeriodeLaba = "AGUSTUS " & cmbTahun.text
                Case 9
                    strPeriodeLaba = "SEPTEMBER " & cmbTahun.text
                Case 10
                    strPeriodeLaba = "OKTOBER " & cmbTahun.text
                Case 11
                    strPeriodeLaba = "NOVEMBER " & cmbTahun.text
                Case 12
                    strPeriodeLaba = "DESEMBER " & cmbTahun.text
            End Select
            
            HitungLabaRugi
        Case "\Laporan Kartu Hutang"
            filtertanggal = "{tmp_kartuHutang.tanggal}"
            filtersupplier = "{tmp_kartuHutang.kode_supplier}"
        Case "\Laporan Kartu Piutang"
            filtertanggal = "{tmp_kartuPiutang.tanggal}"
            filtersupplier = "{tmp_kartuPiutang.kode_customer}"

'Produksi
        Case "\Laporan Proses Potong"
            filtertanggal = "{t_potongH.tanggal}"
            filterPosting = "{t_potongH.status_posting}"
            filterFinish = "{t_potongH.status_finish}"
            filterkodebarang = "{t_potongH.kode_bahan}"
            filterkodeHasil = "{t_potongD.kode_bahan}"
            filterNo_serial = "{t_potongH.nomer_serial}"
            filterOperator = "{t_potongh.kode_operator}"
            filterMesin = "{t_potongH.kode_mesin}"
        Case "\Laporan Proses Cetak"
            filtertanggal = "{t_cetakH.tanggal}"
            filterPosting = "{t_cetakH.status_posting}"
            filterFinish = "{t_cetakH.status_finish}"
            filterkodebarang = "{t_cetakH.kode_bahan}"
            filterkodeHasil = "{t_cetakD.kode_bahan}"
            filterNo_serial = "{t_cetakH.nomer_serial}"
            filterOperator = "{t_cetakh.kode_operator}"
            filterMesin = "{t_cetakH.kode_mesin}"
        Case "\Laporan Proses Plong"
            filtertanggal = "{t_plongH.tanggal}"
            filterPosting = "{t_plongH.status_posting}"
            filterFinish = "{t_plongH.status_finish}"
            filterkodebarang = "{t_plongH.kode_bahan}"
            filterkodeHasil = "{t_plongD.kode_bahan}"
            filterNo_serial = "{t_plongH.nomer_serial}"
            filterOperator = "{t_plongh.kode_operator}"
            filterMesin = "{t_plongH.kode_mesin}"
            
        Case "\Laporan Pemakaian Tinta"
            filtertanggal = "{vwAnalisaTinta.tanggal}"
            filterPosting = "{vwAnalisaTinta.status_posting}"
            filtergudang = "{vwAnalisaTinta.kode_mesin}"
            filterkodebarang = "{t_hasilcetakD_tinta.kode_tinta}"
        Case "\Laporan Pemakaian Plat"
            filtertanggal = "{t_hasilcetakH.tanggal}"
            filterkodebarang = "{t_hasilcetakh.kode_Plat}"
            FilterSerialPlat = "{t_hasilcetakh.nomer_serial_Plat}"
        Case "\Laporan Proses Potong CD"
            filtertanggal = "{t_PotongcdH.tanggal}"
            filterPosting = "{t_PotongcdH.status_posting}"
            filterFinish = "{t_potongcdH.status_finish}"
            filterkodebarang = "{t_potongcdD.kode_asal}"
            filterkodeHasil = "{t_potongcdH.kode_hasil}"
            filterOperator = "{t_potongcdH.kode_operator}"
            filtergudang = "{t_potongcdH.kode_gudang_proses}"
        Case "\Laporan All Proses"
            'filtertanggal = "{}"
            filterkodebarang = "{t_coatingd.kode_asal}"
' penerimaan hasil produksi
        Case "\Laporan Penerimaan Hasil Potong"
            filtertanggal = "{t_hasilpotongH.tanggal}"
            filterPosting = "{t_hasilpotongH.status_posting}"
        Case "\Laporan Penerimaan Hasil Cetak"
            filtertanggal = "{t_hasilcetakH.tanggal}"
            filterPosting = "{t_hasilcetakH.status_posting}"
        Case "\Laporan Penerimaan Hasil Plong"
            filtertanggal = "{t_hasilplongH.tanggal}"
            filterPosting = "{t_hasilplongH.status_posting}"
'stock
        Case "\Laporan Stock Kertas"
            filterkodebarang = "{stock.kode_bahan}"
            filterkategori = "{rpt_ms_bahan.kategori}"
      
' kartu stock
        Case "\Laporan Kartu Stock"
            filtertanggal = "{tmp_kartustock.tanggal}"
            filterkodebarang = "{tmp_kartustock.kode_bahan}"
        Case "\Laporan Kartu Stock Tinta"
            filtertanggal = "{tmp_kartustocktinta.tanggal}"
            filterkodebarang = "{tmp_kartustocktinta.kode_tinta}"
        Case "\Laporan Kartu Stock Plat"
            filtertanggal = "{tmp_kartustockplat.tanggal}"
            filterkodebarang = "{tmp_kartustockplat.kode_plat}"
            
            sBulanPrev = BulanPrev(cmbBulan.text, cmbTahun.text)
            sTahunPrev = TahunPrev(cmbBulan.text, cmbTahun.text)
            
        Case "\Laporan Mutasi Stock Kertas"
            ProsesMutasi DTDari.value, DTSampai.value
            filterkategori = "{rpt_ms_bahan.kategori}"
            filtergudang = "{tmp_mutasistock.kode_gudang}"
'            MsgBox "selesai"
            
' PENGIRIMAN BARANG
        Case "\Laporan Analisa Pengiriman"
            filtertanggal = "{t_trukAngkut.tanggal_keluar}"
            filterNopol = "{t_trukAngkut.nopol}"
            
            
        Case "\Laporan Request Pembelian"
            filtertanggal = "{t_RequestPOH.tanggal_requestPO}"
            filterExpired = "{t_RequestPOH.tanggal_expired}"
            
        Case "\Laporan Order Pembelian"
            If chkTglKirim.Visible = True And chkTglKirim.value = 1 Then
                filtertanggal = "{t_POH.tanggal_kirim}"
            Else
                filtertanggal = "{t_POH.tanggal_PO}"
            End If
            filtersupplier = "{t_POH.kode_supplier}"
            filterkodebarang = "{t_POD.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
        
        Case "\Laporan Penerimaan Barang"
            filtertanggal = "{t_terimabarangH.tanggal_terimabarang}"
            filterkodebarang = "{t_terimabarang_timbang.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
            filterPosting = "{t_terimabarangH.status_posting}"
            
        Case "\Laporan Pembelian"
            filtertanggal = "{t_belih.tanggal_beli}"
            filtersupplier = "{t_belih.kode_supplier}"
            filterkategori = "{ms_bahan.kategori}"
            filterkodebarang = "{t_belid.kode_bahan}"
            filterPosting = "{t_beliH.status_posting}"
            
        Case "\Laporan Retur Pembelian"
            filtertanggal = "{t_returbelih.tanggal_returbeli}"
            filtersupplier = "{t_returbelih.kode_supplier}"
            filterkategori = "{ms_bahan.kategori}"
            filterkodebarang = "{t_returbelid.kode_bahan}"
            filterPosting = "{t_returbeliH.status_posting}"
        
        Case "\Laporan Request Penjualan"
            filtertanggal = "{t_RequestSOH.tanggal_requestSO}"
            filterExpired = "{t_RequestSOH.tanggal_expired}"
            filtersupplier = "{t_RequestSOH.kode_customer}"
            
        Case "\Laporan Order Penjualan"
            If chkTglKirim.Visible = True And chkTglKirim.value = 1 Then
                filtertanggal = "{t_SOH.tanggal_kirim}"
            Else
                filtertanggal = "{t_SOH.tanggal_SO}"
            End If
            filtersupplier = "{t_SOH.kode_customer}"
            filterkodebarang = "{t_SOD.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
            
        Case "\Laporan Pengeluaran Barang"
            filtertanggal = "{t_suratjalanH.tanggal_suratjalan}"
            filterkodebarang = "{t_suratjalan_timbang.kode_bahan}"
            filterkategori = "{ms_bahan.kategori}"
            filterPosting = "{t_suratjalanH.status_posting}"
            
        Case "\Laporan Penjualan"
            filtertanggal = "{t_jualh.tanggal_jual}"
            filtersupplier = "{t_jualh.kode_customer}"
            filterkodebarang = "{t_juald.kode_bahan}"
            filterPosting = "{t_jualH.status_posting}"
            
        Case "\Laporan Retur Penjualan"
            filtertanggal = "{t_returjualh.tanggal_returjual}"
            filtersupplier = "{t_returjualh.kode_customer}"
            filterkodebarang = "{t_returjuald.kode_bahan}"
            filterPosting = "{t_returjualH.status_posting}"
            
        Case "\Laporan Produksi"
            filtertanggal = "{t_produksih.tanggal_produksi}"
            filterPosting = "{t_produksiH.status_posting}"
'            conn.ConnectionString = strcon
'
'            conn.Open
'
''            rs.Open "Select * from mutasi_coa where bulan='" & sBulanPrev & "' and tahun='" & sTahunPrev & "'", conn
''            If rs.EOF Then
''                If (cmbBulan.text <> gBulanAwal Or cmbTahun.text <> gTahunAwal) Then
''                    MsgBox "Neraca tidak dapat ditampilkan karena data untuk Bulan " & sBulanPrev & "  Tahun " & sTahunPrev & "  belum ditutup !", vbExclamation
''                    rs.Close
''                    conn.Close
''                    Exit Sub
''                End If
''            End If
''            rs.Close
'            conn.Close
            
            
            
 
    End Select
    
    Formula = ""
    km = " " & km
    If frTanggal.Visible = True And filtertanggal <> "" Then
        
    If DTSampai.Visible = True Then
        Formula = Formula & filtertanggal & ">=#" & Format(DTDari, "MM/dd/yyyy") & "# and " & filtertanggal & "<#" & Format(DTSampai + 1, "MM/dd/yyyy") & "#"
    Else
        Formula = filtertanggal & "=#" & Format(DTDari, "MM/dd/yyyy") & "#"
    End If
    
    
    
    End If
    If cmbGudang.text <> "" And filtergudang <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filtergudang & "='" & cmbGudang.text & "'"
    End If
'    If cmbKategori.text <> "" And filterKategoriCust <> "" Then
'        If Formula <> "" Then Formula = Formula & " and "
'        Formula = Formula & filterKategoriCust & "='" & cmbKategori.text & "'"
'    End If
    If frKodeBarang.Visible = True And txtKdBarang.text <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterkodebarang & "='" & txtKdBarang.text & "'"
    End If
    If frNoTrans.Visible = True And txtNoTrans.text <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filterNoTrans & "  like '*" & txtNoTrans.text & "*'"
    End If
    If FrKategori.Visible = True Then
    Dim kategori As String
    kategori = ""
        For i = 0 To listKategori.ListCount - 1
            If listKategori.Selected(i) Then kategori = kategori & "'" & listKategori.List(i) & "',"
        Next
        If filterkategori <> "" And kategori <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filterkategori & " in [" & Left(kategori, Len(kategori) - 1) & "]"
        End If
    End If
    If frSupCus.Visible = True Then
    Dim supcus As String
    supcus = ""
        For i = 0 To cmbSupCus.ListCount - 1
            If cmbSupCus.Selected(i) Then supcus = supcus & "'" & cmbSupCus.List(i) & "',"
        Next
        
     If cmbSupCus.text <> "" And supcus <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filtersupplier & " in  [" & Left(supcus, Len(supcus) - 1) & "]"
'     Else
'        If Formula <> "" Then Formula = Formula & " and "
'        Formula = Formula & filtersupplier & "='" & cmbSupCus.text & "'"
     End If
    End If
    If chkActiveStock.Visible = True Then
    If chkActiveStock.value = 1 Then
      If Formula <> "" Then Formula = Formula & " and "
      Formula = Formula & "({tmp_mutasistock.masuk}>0 or {tmp_mutasistock.keluar}>0)"
    End If
    End If
    'filter untuk show-kriteria
    If FrBahanHasil.Visible = True Then
        If txtKodeHsl <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filterkodeHasil & "='" & txtKodeHsl & "'"
        End If
        If cmbSerial.text <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filterNo_serial & "='" & cmbSerial.text & "'"
    End If
    
    If frOperator.Visible = True Then
    If txtOpr <> "" Then
      If Formula <> "" Then Formula = Formula & " and "
      Formula = Formula & filterOperator & "='" & txtOpr & "'"
    End If
    End If
    
    If frMesin.Visible = True Then
    If txtMesin <> "" Then
      If Formula <> "" Then Formula = Formula & " and "
      Formula = Formula & filterMesin & "='" & txtMesin & "'"
    End If
    End If
    
        
    
'    If framCrByr.Visible = True Then
'        If chkCrByr(0).value = 0 Or chkCrByr(1).value = 0 Then
'            If Formula <> "" Then Formula = Formula & " and "
'            If chkCrByr(0).value = 1 And chkCrByr(1).value = 0 Then
'                Formula = Formula & filtercarabayar & "='1'"
'            ElseIf chkCrByr(0).value = 0 And chkCrByr(1).value = 1 Then
'                Formula = Formula & filtercarabayar & "='2' or " & filtercarabayar & "='3'"
'            End If
'        End If
'        If chkLunas(0).value = 0 Or chkLunas(1).value = 0 Then
'            If Formula <> "" Then Formula = Formula & " and "
'            If chkLunas(0).value = 1 And chkLunas(1).value = 0 Then
'                Formula = Formula & filter3 & "='1'"
'            ElseIf chkLunas(0).value = 0 And chkLunas(1).value = 1 Then
'                Formula = Formula & filter3 & "='0'"
'            End If
'        End If
'    End If
    If frBulan.Visible = True Then
        If cmbBulan.text <> "" And cmbTahun.text <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & " year(" & filtertanggal & ")=" & cmbTahun
        If cmbBulan.Visible = True Then Formula = Formula & " and month(" & filtertanggal & ")=" & cmbBulan & ""
        End If
        End If
    End If
    If frUserID.Visible = True Then
        If cmbUserID.text <> "" Then
            If Formula <> "" Then Formula = Formula & " and "
            Formula = Formula & filteruserid & "='" & cmbUserID.text & "'"
        End If
    End If
'    If frMember.Visible = True Then
'        If cmbMember.text <> "" Then
'        If Formula <> "" Then Formula = Formula & " and "
'        Formula = Formula & filtermember & "='" & cmbMember & "'"
'        End If
'    End If
    
    frmprint.strfilename = filename & ".rpt"

    If frmprint.strfilename = "\Laporan Stock.rpt" Then
        If Formula <> "" Then Formula = Formula & " and "
        If chkHabis.value = "1" Then
        Formula = Formula & " {v_stock.stock}<=0"
        Else
        Formula = Formula & " {v_stock.stock}>0"
        End If
    End If
    
    If frStatus.Visible = True Then
    Dim cek As String
    cek = ""
     If OptPosting(1).value = True Then cek = cek & filterPosting & "='1' and "
     If OptPosting(2).value = True Then cek = cek & filterPosting & "='0' and "
     If cek <> "" Then
        cek = Left(cek, Len(cek) - 4)
     End If
    End If
    
'    If FrStatus2.Visible = True Then
'        If OptStatus(1).value = True Then
'            If Formula <> "" Then Formula = Formula & " and "
'            Formula = Formula & filterExpired & "<=#" & Format(Date, "MM/dd/yyyy") & "# "
'        End If
'    End If
    
'    Select Case frmprint.strfilename
'    Case "\Laporan Proses Plong.rpt", "\Laporan Proses Potong.rpt", "\Laporan Proses Cetak.rpt", "\Laporan Pemakaian Tinta.rpt"
        
    If cek <> "" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & "( " & cek & " )"
    End If
'    End Select
    
    
    param3 = ""
    If Option1.value = True Then
        param3 = "1"
    Else
        param3 = "0"
    End If
    frmprint.strFormula = Formula
    frmprint.param1 = Format(DTDari, "dd/MM/yyyy")
    frmprint.param2 = Format(DTSampai, "dd/MM/yyyy")
    frmprint.param3 = param3
    frmprint.Show vbModal
End Sub
Public Sub cekbarang()
'    cmbKategori.text = ""
End Sub
Private Sub cmdSearchKdBrg_Click()
Dim query As String
 frmSearch.connstr = strcon
        query = "Select kode_bahan,nama_bahan,kategori from ms_bahan"
    
    frmSearch.query = query
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKdBarang"
    frmSearch.nmctrl2 = ""
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    cari_serial
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_Load()
    
    conn.ConnectionString = strcon
    conn.Open
    DTDari = Now
    DTSampai = Now
    cmbGudang.Clear
    cmbGudang.AddItem ""
    rs.Open "select * from ms_gudang order by kode_gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    listKategori.Clear
    listKategori.AddItem ""
    rs.Open "select distinct kategori from var_kategori  order by [kategori]", conn
    While Not rs.EOF
        listKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    cmbSupCus.Clear
    
    Select Case filename
    Case "\Laporan Hutang", "\Laporan Pembayaran Hutang", "\Laporan Pembelian", "\Laporan Request Pembelian", "\Laporan Order Pembelian", "\Laporan Retur Pembelian", "\Laporan Kartu Hutang"
        rs.Open "select distinct(a.kode_supplier),b.nama_supplier from list_hutang a inner join ms_supplier b on a.kode_supplier=b.kode_supplier order by a.kode_supplier", conn
        While Not rs.EOF
            cmbSupCus.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End Select
    
    If filename = "\Laporan Piutang" Or filename = "\Laporan Pembayaran Piutang" Or filename = "\Laporan Penjualan" Or filename = "\Laporan Kartu Piutang" Or filename = "\Laporan Request Penjualan" Or filename = "\Laporan Order Penjualan" Or filename = "\Laporan Retur Penjualan" Then
        cmbSupCus.Clear
        rs.Open "select distinct(a.kode_customer),b.nama_customer from list_piutang a inner join ms_customer b on a.kode_customer=b.kode_customer order by a.kode_customer", conn
        While Not rs.EOF
            cmbSupCus.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    
    If filename = "\Laporan Pemakaian Tinta" Then
    cmbGudang.Clear
        rs.Open "select kode_gudang from ms_gudang where status='1' and jenis_mesin='Cetak'", conn
        While Not rs.EOF
            cmbGudang.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
    End If
    
    rs.Open "select * from login order by username", conn
    While Not rs.EOF
        cmbUserID.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbTahun.Clear
    For i = Year(Now) To "2009" Step -1
        cmbTahun.AddItem i
    Next
    
    conn.Close
    Select Case filename
        
        Case "\Laporan Penjualan Kertas", "\Laporan Retur Penjualan Kertas":
'            framCrByr.Visible = True
'            frGudang.Visible = True
'            frMember.Visible = True
'            frUserID.Visible = True
            frSupCus.Visible = True
            frKodeBarang.Visible = True
            lblSupCus.Caption = "Customer"
        Case "\Laporan Penjualan Kertas per Item":
            frSupCus.Visible = True
            frKodeBarang.Visible = True
            lblSupCus.Caption = "Customer"
        Case "\Laporan Pembelian Kertas", "\Laporan Pembelian Tinta", "\Laporan Pembelian Plat", "\Laporan Retur Pembelian Kertas", "\Laporan Retur Pembelian Tinta", "\Laporan Retur Pembelian Plat":
            'frGudang.Visible = True
            frSupCus.Visible = True
            optGroup(1).Caption = "per Supplier"
            optGroup(2).Caption = "per Item per Supplier"
            optGroup(3).Caption = "per Supplier per Item"
            frKodeBarang.Visible = True
        Case "\Laporan Opname Kertas", "\Laporan Opname Tinta", "\Laporan Opname Plat": 'via
            FrKategori.Visible = True
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
            frGudang.Visible = True
            
        Case "\Laporan Opname Tinta": 'via
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
            frGudang.Visible = True
        Case "\Laporan Opname Plat": 'via
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
            frGudang.Visible = True
            
        Case "\Laporan Hutang": 'via
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGroup.Visible = False
            
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frTanggal.Visible = False
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGudang.Visible = False
            chkGroupby.Visible = False
        Case "\Laporan Piutang":
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGroup.Visible = False
            
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frTanggal.Visible = False
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGudang.Visible = False
            chkGroupby.Visible = False
        Case "\Laporan Outstanding BG":
            frSupCus.Visible = True
        Case "\Laporan Member":
            frTanggal.Visible = True
            frMember.Visible = False
            OptTipe(0).Visible = True
            OptTipe(1).Visible = True
            OptTipe(1).Caption = "per Member"
            OptTipe(2).Visible = True
            OptTipe(2).Caption = "per Tanggal Per Member"
        Case "\Laporan Penjualan HPP":
            frGudang.Visible = True
        Case "\Laporan All Proses":
            frKodeBarang.Visible = True
        
        Case "\Laporan Pembayaran Hutang":
            frGudang.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frTanggal.Visible = True
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            chkGroupby.Visible = False
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGudang.Visible = False
            Option1.value = True
            frDetail.Visible = False
        Case "\Laporan Penerimaan Piutang":
            frGudang.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frTanggal.Visible = True
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            chkGroupby.Visible = False
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frGudang.Visible = False
            Option1.value = True
            frDetail.Visible = False
       
        Case "\Laporan Kartu Stock", "\Laporan History Harga Pokok":
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            frGudang.Visible = True
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            frGroup.Visible = False
            
        Case "\Laporan Stock Kertas", "\Laporan Stock Habis", "\Laporan Kartu Stock":
            OptTipe(0).Visible = False
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            Option1.value = True
            DTSampai.Visible = False
            Label6.Visible = False
            frGudang.Visible = True
            frDetail.Visible = False
            frTanggal.Visible = False
            frGroup.Visible = False
            chkGroupby.Visible = False
            frKodeBarang.Visible = True
            FrKategori.Visible = True
        Case "\Laporan Buku Besar":
            OptTipe(1).Visible = False
            OptTipe(2).Visible = False
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            FrAcc.Visible = True
        Case "\Laporan Mutasi Stock Kertas":
            frTanggal.Visible = True
            frDetail.Visible = False
            frGudang.Visible = True
            frGroup.Visible = False
            FrKategori.Visible = True
            Option1.value = True
            chkGroupby.Visible = False
            chkActiveStock.Visible = True
        Case "\Neraca", "\LabaRugi":
            OptTipe(1).value = True
            chkGroupby.Visible = False
            frDetail.Visible = False
            frBulan.Visible = True
            Line1.Visible = False
            OptTipe(0).Visible = False: OptTipe(1).Visible = False: OptTipe(2).Visible = False
        Case "\Laporan Proses Potong", "\Laporan Proses Cetak", "\Laporan Proses Plong", "\Laporan Proses Potong CD"
            frStatus.Visible = True
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            FrAcc.Visible = False
            show_kriteria
        
        Case "\Laporan Penerimaan Hasil Potong", "\Laporan Penerimaan Hasil Cetak", "\Laporan Penerimaan Hasil Plong", "\Laporan Penerimaan Hasil Coating"
            frStatus.Visible = True
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
            FrAcc.Visible = False
            show_kriteria
        Case "\Laporan Coating Bungkus"
            frTanggal.Visible = True
            frDetail.Visible = True
        Case "\Laporan Pemakaian Tinta"
            frGudang.Visible = True
            ckFinish.Visible = False
            frStatus.Visible = True
            frKodeBarang.Visible = True
        Case "\Laporan Pemakaian Plat"
            frKodeBarang.Visible = True
            
        Case "\Laporan Jurnal"
            chkGroupby.Visible = False
            frNoTrans.Visible = True
        Case "\Laporan Kartu Hutang", "\Laporan Kartu Piutang"
            frTanggal.Visible = True
            frSupCus.Visible = True
            chkGroupby.Visible = False
        Case "\Laporan Order Roll", "\Laporan Rekap Order Roll", "\Laporan Order Kertas", "\Laporan Rekap Order Kertas"
            frTanggal.Visible = True
            frSupCus.Visible = True
            chkGroupby.Visible = True
            frKodeBarang.Visible = True
        Case "\Laporan Order Pembelian"
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frKodeBarang.Visible = True
            FrKategori.Visible = True
        Case "\Laporan Penerimaan Barang"
            frKodeBarang.Visible = True
            FrKategori.Visible = True
            frStatus.Visible = True
        Case "\Laporan Pembelian"
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frKodeBarang.Visible = True
            frStatus.Visible = True
        Case "\Laporan Retur Pembelian"
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frKodeBarang.Visible = True
            FrKategori.Visible = True
            frStatus.Visible = True
        Case "\Laporan Request Penjualan"
            frSupCus.Visible = True
            lblSupCus = "Customer"
        Case "\Laporan Order Penjualan"
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frKodeBarang.Visible = True
            FrKategori.Visible = True
        Case "\Laporan Pengeluaran Barang"
            frKodeBarang.Visible = True
            FrKategori.Visible = True
            frStatus.Visible = True
        Case "\Laporan Penjualan"
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frKodeBarang.Visible = True
            frStatus.Visible = True
        Case "\Laporan Retur Penjualan"
            frSupCus.Visible = True
            lblSupCus = "Customer"
            frKodeBarang.Visible = True
            frStatus.Visible = True
        Case "\Laporan Produksi"
'            frSupCus.Visible = True
'            lblSupCus = "Customer"
'            frKodeBarang.Visible = True
            frStatus.Visible = True
            
    End Select
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub OptTipe_Click(Index As Integer)
    If Index = 0 Then
        frBulan.Visible = False
        frTanggal.Visible = True
        cmbBulan.Visible = True
    Select Case filename
        Case "\Laporan Transfer Barang":
            
        Case "\Laporan Pembelian":
        Case "\Laporan Penjualan HPP":
        
        Case "\Laporan Penjualan":
            frUserID.Visible = True
            
        Case "\Laporan Member":
            frMember.Visible = False
            frTanggal.Visible = True
        Case "\Laporan Pembayaran Hutang":
            
        Case "\Laporan Stock":
            
        Case "\Laporan Kartu Stok":
            
    End Select
    End If
    If Index = 1 Then
            frBulan.Visible = True
            frTanggal.Visible = False
            cmbBulan.Visible = True
    Select Case filename
        Case "\Laporan Transfer Barang":
            
        Case "\Laporan Pembelian":
        Case "\Laporan Penjualan HPP":
            
            
        Case "\Laporan Penjualan":
            frUserID.Visible = True
            
        Case "\Laporan Member":
            frMember.Visible = True
            frTanggal.Visible = False
            
        Case "\Laporan Pembayaran Hutang":
            
        Case "\Laporan Stock":
            
        Case "\Laporan Kartu Stok":
            
    End Select
    End If
    If Index = 2 Then
        frBulan.Visible = True
        cmbBulan.Visible = False
        frTanggal.Visible = False
    Select Case filename
        Case "\Laporan Transfer Barang":
            
        Case "\Laporan Pembelian":
        Case "\Laporan Penjualan HPP":
            
        Case "\Laporan Penjualan":
            frUserID.Visible = True
            
        Case "\Laporan Member":
            frMember.Visible = True
            frTanggal.Visible = True
        Case "\Laporan Pembayaran Hutang":
            
        Case "\Laporan Stock":
            
        Case "\Laporan Kartu Stok":
            
    End Select
    End If
End Sub

Private Sub txtKdBarang_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cekbarang
End Sub

Private Sub txtKdBarang_LostFocus()
    cekbarang
End Sub

'Private Sub HitungLabaRugi()
'Dim Laba As Double, AccLaba As String, strTgl As String
'    conn.ConnectionString = strcon
'    conn.Open
'
'    conn.Execute "delete from tmp_labarugi"
'
'    conn.Execute "insert into tmp_labarugi select kode_acc,sum(debet),sum(kredit) from t_jurnald d inner join t_jurnalh h on d.no_transaksi=h.no_transaksi " & _
'    " where month(h.tanggal) = '" & cmbBulan.text & "' and year(h.tanggal) = '" & cmbTahun.text & "' and left(kode_acc,1)>='4' group by kode_acc"
''    rs.Open "Select kode_acc from setting_coa where kode='laba berjalan'", conn
''    If Not rs.EOF Then
''        AccLaba = rs("kode_acc")
''    End If
''    rs.Close
''
''
''    rs.Open "select sum(d.kredit-d.debet) as Laba from (t_jurnald d " & _
''            "inner join t_jurnalh t on t.no_transaksi=d.no_transaksi) " & _
''            "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
''            "where (p.kategori<>'HT' and p.kategori<>'KW' and p.kategori<>'MD')", conn
''    Laba = 0
''    If Not rs.EOF And IsNull(rs!Laba) = False Then
''        Laba = (rs!Laba)
''    End If
''    rs.Close
''
''    conn.BeginTrans
''    conn.Execute "Delete from t_jurnald where no_transaksi='00'"
''    conn.Execute "Delete from t_jurnalh where no_transaksi='00'"
''
''    InsertJurnal ("PD")
''    InsertJurnal ("HPP")
''    InsertJurnal ("BO")
''    InsertJurnal ("BNO")
''    InsertJurnal ("PL")
''    InsertJurnal ("BL")
''
''    strTgl = cmbTahun.text & "-" & cmbBulan.text & "-" & "01"
''
''
''    conn.Execute "insert into t_jurnald (no_transaksi,kode_acc,kredit) values ('00','" & AccLaba & "'," & Replace(Laba, ",", ".") & ")"
''    conn.Execute "insert into t_jurnalh (no_transaksi,no_jurnal,tanggal) values ('00','00','" & strTgl & "')"
''    conn.CommitTrans
'    conn.Close
'End Sub

Private Sub HitungLabaRugi()
Dim Laba As Double, AccLaba As String, strTgl As String
Dim Nilai As Double
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Execute "delete from tmp_labarugi"
    
    conn.Execute "insert into tmp_labarugi select kode_acc,sum(debet),sum(kredit),0,0 from t_jurnald d inner join t_jurnalh h on d.no_transaksi=h.no_transaksi " & _
                " where month(h.tanggal) = '" & cmbBulan.text & "' and year(h.tanggal) = '" & cmbTahun.text & "' and left(kode_acc,1)>='4' group by kode_acc"

    
    
    rs.Open "select sum(d.kredit-d.debet) as Pendapatan from tmp_labarugi d " & _
            "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
            "where p.kategori='PL' or p.kategori='PD'", conn
    PendapatanLaba = 0
    If Not rs.EOF And IsNull(rs!Pendapatan) = False Then
        PendapatanLaba = (rs!Pendapatan)
    End If
    rs.Close
    
    conn.Execute "UPDATE tmp_labarugi set pendapatan=" & PendapatanLaba & " "
    
    Nilai = 0
    rs.Open "select (kredit-debet) as Nilai from tmp_labarugi where kode_acc='410.000.0100'", conn
    If Not rs.EOF And IsNull(rs!Nilai) = False Then
        Nilai = rs!Nilai
    End If
    rs.Close
    
    conn.Execute "UPDATE tmp_labarugi set Persen=100 where kode_acc='410.000.0100' "
    
    conn.Execute "UPDATE tmp_labarugi set Persen=(debet-kredit)*100/" & Nilai & " where kode_acc='510.010.0100' "
    
    Nilai = 0
    rs.Open "select (kredit-debet) as Nilai from tmp_labarugi where kode_acc='410.000.0150'", conn
    If Not rs.EOF And IsNull(rs!Nilai) = False Then
        Nilai = rs!Nilai
    End If
    rs.Close
    
    conn.Execute "UPDATE tmp_labarugi set Persen=100 where kode_acc='410.000.0150' "
    
    conn.Execute "UPDATE tmp_labarugi set Persen=(debet-kredit)*100/" & Nilai & " where kode_acc='510.010.0200' "
    
    

    conn.Close
End Sub




Private Sub HitungNeraca()
Dim Laba As Double, AccLaba As String, strTgl As String
Dim sBulanPrev As String, sTahunPrev As String

    conn.ConnectionString = strcon
    conn.Open

    AccLaba = getAcc("laba berjalan")

    rs.Open "select sum(d.kredit-d.debet) as Laba from (t_jurnald d " & _
            "inner join t_jurnalh t on t.no_transaksi=d.no_transaksi) " & _
            "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
            "where CONVERT(VARCHAR(7), t.tanggal, 111)='" & cmbTahun & "/" & cmbBulan & "' " & _
            "and (p.kategori<>'HT' and p.kategori<>'KW' and p.kategori<>'MD')", conn
    Laba = 0
    If Not rs.EOF And IsNull(rs!Laba) = False Then
        Laba = (rs!Laba)
    End If
    rs.Close

'    cmbBulan.text = Val(cmbBulan.text)
    cmbTahun.text = Val(cmbTahun.text)

    If Val(cmbBulan.text) = 1 Then
        sBulanPrev = "12"
        sTahunPrev = Val(cmbTahun.text) - 1
    Else
        sBulanPrev = Val(cmbBulan.text) - 1
        sTahunPrev = cmbTahun.text
    End If

'    If Len(cmbBulan.text) = 1 Then cmbBulan.text = "0" & cmbBulan.text

    conn.BeginTrans
    conn.Execute "Delete from tmp_neraca"
    conn.Execute "insert into tmp_neraca (kode_acc,debet,kredit) " & _
                 "select d.kode_acc,sum(d.debet),sum(d.kredit) " & _
                 "from (t_jurnald d " & _
                 "inner join t_jurnalh h on h.no_transaksi=d.no_transaksi) " & _
                 "inner join ms_parentAcc p on p.kode_parent=left(d.kode_acc,3) " & _
                 "where CONVERT(VARCHAR(7), h.tanggal, 111)='" & cmbTahun & "/" & cmbBulan & "' " & _
                 "and (p.kategori='HT' or p.kategori='KW' or p.kategori='MD') group by d.kode_acc"

    conn.Execute "insert into tmp_neraca (kode_acc,debet,kredit) " & _
                 "select m.kode_acc,case when p.kategori='HT' then m.saldoakhir else 0 end as debet, " & _
                 "case when p.kategori='HT' then 0 else m.saldoakhir end as kredit " & _
                 "from mutasi_coa m " & _
                 "inner join ms_parentAcc p on p.kode_parent=left(m.kode_acc,3) " & _
                 "where m.bulan='" & sBulanPrev & "' and m.tahun='" & sTahunPrev & "' " & _
                 "and (p.kategori='HT' or p.kategori='KW' or p.kategori='MD')"

    conn.Execute "insert into tmp_neraca (kode_acc,kredit) values ('" & AccLaba & "'," & Replace(Laba, ",", ".") & ")"
    conn.CommitTrans
    conn.Close
End Sub


Private Sub InsertJurnal(kategori As String)
    rs.Open "Select t.kode_acc from tmp_labarugi t " & _
            "inner join ms_parentAcc p on p.kode_parent=left(t.kode_acc,3) " & _
            "where p.kategori='" & kategori & "'", conn
    If rs.EOF Then
        conn.Execute "insert into tmp_labarugi(kode_acc) " & _
                     "select top 1 c.kode_acc from ms_coa c " & _
                     "inner join ms_parentAcc p on p.kode_parent=left(c.kode_acc,3) " & _
                     "where p.kategori='" & kategori & "'"
    End If
    rs.Close
End Sub
Private Sub show_kriteria()
    frKodeBarang.Visible = True
    FrBahanHasil.Visible = True
    frGudang.Visible = True
    frOperator.Visible = True
    frMesin.Visible = True
End Sub
Private Sub cari_serial()
conn.Open strcon
cmbSerial.Clear
    rs.Open "select distinct nomer_serial from stock where kode_bahan='" & txtKdBarang & "'", conn
    While Not rs.EOF
        cmbSerial.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
conn.Close
End Sub

