VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmKomposisiBarang 
   Caption         =   "Komposisi Barang"
   ClientHeight    =   7410
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10335
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7410
   ScaleWidth      =   10335
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4095
      TabIndex        =   26
      Top             =   6750
      Width           =   1230
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5385
      Index           =   0
      Left            =   90
      TabIndex        =   6
      Top             =   1260
      Width           =   10080
      Begin VB.Frame Frame2 
         BackColor       =   &H8000000C&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1725
         Left            =   72
         TabIndex        =   7
         Top             =   156
         Width           =   6990
         Begin VB.TextBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   0
            Top             =   270
            Width           =   1500
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   1
            Top             =   672
            Width           =   1140
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   135
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   10
            Top             =   1080
            Width           =   3795
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   2
               Top             =   45
               Width           =   1350
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   3
               Top             =   45
               Width           =   1050
            End
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   4
               Top             =   45
               Width           =   1050
            End
         End
         Begin VB.PictureBox Picture2 
            BackColor       =   &H8000000C&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   2790
            ScaleHeight     =   375
            ScaleWidth      =   465
            TabIndex        =   8
            Top             =   225
            Width           =   465
            Begin VB.CommandButton cmdSearchBrg 
               BackColor       =   &H8000000C&
               Caption         =   "F3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   0
               Picture         =   "frmKomposisiBarang.frx":0000
               TabIndex        =   9
               Top             =   45
               Width           =   420
            End
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            Height          =   330
            Left            =   2520
            TabIndex        =   16
            Top             =   675
            Width           =   1050
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   15
            Top             =   1305
            Width           =   600
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3285
            TabIndex        =   14
            Top             =   315
            Width           =   3480
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            Caption         =   "Persentase"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   13
            Top             =   720
            Width           =   960
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   12
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label lblNamaBarang2 
            BackColor       =   &H8000000C&
            Caption         =   "-"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   288
            Left            =   2556
            TabIndex        =   11
            Top             =   708
            Visible         =   0   'False
            Width           =   3480
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   3195
         Left            =   90
         TabIndex        =   5
         Top             =   2040
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   5636
         _Version        =   393216
         Cols            =   4
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Label lblKeterangan 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3465
      TabIndex        =   28
      Top             =   855
      Width           =   6720
   End
   Begin VB.Label lblNoSerial 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Serial"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1710
      TabIndex        =   27
      Top             =   855
      Width           =   1545
   End
   Begin VB.Label Label16 
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   90
      TabIndex        =   25
      Top             =   6660
      Width           =   735
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1035
      TabIndex        =   24
      Top             =   6660
      Width           =   1140
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6975
      TabIndex        =   23
      Top             =   6660
      Width           =   735
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   22
      Top             =   6660
      Width           =   2175
   End
   Begin VB.Label lblKodeBarang 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1710
      TabIndex        =   21
      Top             =   540
      Width           =   1545
   End
   Begin VB.Label Label2 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   20
      Top             =   540
      Width           =   1050
   End
   Begin VB.Label lblNamaBarang 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "caption"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3465
      TabIndex        =   19
      Top             =   540
      Width           =   3480
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   18
      Top             =   165
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1710
      TabIndex        =   17
      Top             =   90
      Width           =   2085
   End
End
Attribute VB_Name = "frmKomposisiBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public transaksi As String
Dim total As Double
Dim mode As Byte
Public urut As Integer
Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 1 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    
    row = flxGrid.row

    total = total - (flxGrid.TextMatrix(row, 3))
        
        lblTotal = Format(total, "##0.##")

    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1
    lblItem = flxGrid.Rows - 2
    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKdBrg.text) Then row = i
        Next
        If row = 0 Then
            flxGrid.Rows = flxGrid.Rows + 1
            row = flxGrid.Rows - 1
            
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang1
        
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            
            total = total - (flxGrid.TextMatrix(row, 3))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = flxGrid.cols - 1
        
        total = total + (flxGrid.TextMatrix(row, 3))
        
        lblTotal = Format(total, "##0.##")
        lblItem = flxGrid.Rows - 2
        
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang1 = ""
        txtQty.text = "0"
        
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmKomposisiBarang"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang1"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
On Error GoTo err
Dim i As Byte
    i = 0
    If Format(total, "##0.00") <> "100.00" Then
        MsgBox "Total komposisi harus 100%"
        Exit Sub
    End If
    conn.Open strcon
    conn.BeginTrans
    conn.Execute "delete from t_terimabarang_komposisi where nomer_terimabarang='" & lblNoTrans & "' and kode_bahan='" & lblKodeBarang & "'"
    conn.Execute "delete from stock_komposisi where kode_bahan='" & lblKodeBarang & "' and nomer_serial='" & lblNoSerial & "'"
    i = 1
    For X = 1 To flxGrid.Rows - 1
        add_datadetail (X)
        add_komposisistock (X)
    Next
    conn.CommitTrans
    i = 0
    conn.Close
    Unload Me
    Exit Sub
err:
    MsgBox err.Description
    If i = 1 Then conn.RollbackTrans
    DropConnection
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_terimabarang_komposisi"
    fields(0) = "nomer_terimabarang"
    fields(1) = "kode_bahan"
    fields(2) = "urut"
    fields(3) = "kode_bahanbaku"
    fields(4) = "pct"
    fields(5) = "no_urut"
    
    nilai(0) = lblNoTrans
    nilai(1) = lblKodeBarang
    nilai(2) = urut
    nilai(3) = flxGrid.TextMatrix(row, 1)
    nilai(4) = flxGrid.TextMatrix(row, 3)
    nilai(5) = row
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_komposisistock(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(4)
    ReDim nilai(4)

    table_name = "stock_komposisi"
    
    fields(0) = "kode_bahan"
    fields(1) = "nomer_serial"
    fields(2) = "kode_bahanbaku"
    fields(3) = "pct"
    
    
    
    nilai(0) = lblKodeBarang
    nilai(1) = lblNoSerial
    nilai(2) = flxGrid.TextMatrix(row, 1)
    nilai(3) = flxGrid.TextMatrix(row, 3)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtQty.SetFocus
    End If

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub
Private Sub loaddetil()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
        If Not cek_kodebarang1 Then
            LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
        End If
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
        txtQty.SetFocus
    End If
End Sub
Private Sub Form_Load()
    flxGrid.TextMatrix(0, 1) = "Kode Bahan"
    flxGrid.TextMatrix(0, 2) = "Nama Bahan"
    flxGrid.TextMatrix(0, 3) = "%"
    flxGrid.ColWidth(0) = 200
    flxGrid.ColWidth(1) = 1500
    flxGrid.ColWidth(2) = 3700
    flxGrid.ColWidth(3) = 1000
    flxGrid.Rows = 1
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
'    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       
        KeyAscii = 0
        Call MySendKeys("{tab}")
       
    End If
End Sub
Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan

        lblSatuan = ""
        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""
        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Sub loadkomposisi()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
flxGrid.Rows = 1
total = 0
conn.Open strcon
rs.Open "select k.kode_bahanbaku,m.nama_bahan,pct from t_" & transaksi & "_komposisi k inner join ms_bahan m on k.kode_bahanbaku=m.kode_bahan where k.nomer_terimabarang='" & lblNoTrans & "' and k.kode_bahan='" & lblKodeBarang & "'  order by urut", conn
While Not rs.EOF
    flxGrid.Rows = flxGrid.Rows + 1
    flxGrid.TextMatrix(flxGrid.Rows - 1, 1) = rs!kode_bahanbaku
    flxGrid.TextMatrix(flxGrid.Rows - 1, 2) = rs!nama_bahan
    flxGrid.TextMatrix(flxGrid.Rows - 1, 3) = rs!pct
    total = total + rs!pct
rs.MoveNext
Wend
rs.Close
conn.Close
lblTotal = Format(total, "##0.##")
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub
