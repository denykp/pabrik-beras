/****** Object:  Table [dbo].[tmp_stockawal]    Script Date: 03/13/2015 14:55:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tmp_stockawalserial](
	[kode_gudang] [varchar](15) NULL,
	[kode_bahan] [varchar](15) NULL,
	[nomer_serial] [varchar](25) NULL,
	[stockawal] [numeric](18, 0) NULL,
	[qtyawal] [numeric](18, 0) NULL,
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


