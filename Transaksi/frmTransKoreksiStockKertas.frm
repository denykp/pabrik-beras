VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransKoreksiStockKertas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Koreksi Stock"
   ClientHeight    =   7755
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9120
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7755
   ScaleWidth      =   9120
   Begin VB.ComboBox cmbGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5730
      Style           =   2  'Dropdown List
      TabIndex        =   39
      Top             =   180
      Width           =   2505
   End
   Begin VB.CommandButton cmdopentreeview 
      Appearance      =   0  'Flat
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8235
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   38
      Top             =   210
      Width           =   330
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3030
      Left            =   5730
      TabIndex        =   37
      Top             =   540
      Visible         =   0   'False
      Width           =   3210
      _ExtentX        =   5662
      _ExtentY        =   5345
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   6
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7155
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   375
      Style           =   1  'Graphical
      TabIndex        =   33
      Top             =   7830
      Visible         =   0   'False
      Width           =   1290
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "&Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2700
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   7155
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1605
      MaxLength       =   80
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   1005
      Width           =   5910
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2400
      Left            =   135
      TabIndex        =   17
      Top             =   1590
      Width           =   7395
      Begin VB.TextBox txtBerat 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3960
         TabIndex        =   5
         Top             =   1065
         Width           =   1140
      End
      Begin VB.CommandButton cmdHistoryHpp 
         Caption         =   "History"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2700
         TabIndex        =   35
         Top             =   1470
         Width           =   825
      End
      Begin VB.TextBox txtHPP 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   6
         Top             =   1440
         Width           =   1140
      End
      Begin VB.ComboBox cmbSerial 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   3
         Text            =   "cmbSerial"
         Top             =   660
         Width           =   2325
      End
      Begin VB.CommandButton cmdSearchBarang 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3840
         TabIndex        =   28
         Top             =   285
         Width           =   375
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   4
         Top             =   1050
         Width           =   1140
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2760
         TabIndex        =   13
         Top             =   1890
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1650
         TabIndex        =   12
         Top             =   1890
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   1890
         Width           =   1350
      End
      Begin VB.TextBox txtKode 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   2
         Top             =   270
         Width           =   2310
      End
      Begin VB.Label Label4 
         BackColor       =   &H8000000C&
         Caption         =   "Berat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3345
         TabIndex        =   36
         Top             =   1110
         Width           =   600
      End
      Begin VB.Label Label2 
         BackColor       =   &H8000000C&
         Caption         =   "HPP"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   34
         Top             =   1485
         Width           =   600
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000C&
         Caption         =   "No. Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   32
         Top             =   705
         Width           =   1170
      End
      Begin VB.Label lblStock 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   0  'Transparent
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   5820
         TabIndex        =   30
         Top             =   1935
         Width           =   2160
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   0  'Transparent
         Caption         =   "Satuan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   5730
         TabIndex        =   29
         Top             =   585
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label Label6 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   27
         Top             =   1095
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   4185
         TabIndex        =   21
         Top             =   1575
         Width           =   870
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   240
         Left            =   4050
         TabIndex        =   20
         Top             =   1545
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   19
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label LblNamaBarang 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   4260
         TabIndex        =   18
         Top             =   330
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      TabIndex        =   16
      Top             =   165
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5220
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7155
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1470
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   7155
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   1620
      TabIndex        =   0
      Top             =   585
      Width           =   2490
      _ExtentX        =   4392
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   48562179
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2850
      Left            =   135
      TabIndex        =   22
      Top             =   4155
      Width           =   8760
      _ExtentX        =   15452
      _ExtentY        =   5027
      _Version        =   393216
      Cols            =   8
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8925
      Top             =   210
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9555
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label3 
      Caption         =   "Gudang :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4830
      TabIndex        =   31
      Top             =   195
      Width           =   1320
   End
   Begin VB.Label Label8 
      Caption         =   "Keterangan :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   26
      Top             =   1035
      Width           =   1275
   End
   Begin VB.Label Label7 
      Caption         =   "Nomor :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   25
      Top             =   210
      Width           =   1320
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7650
      TabIndex        =   24
      Top             =   5760
      Visible         =   0   'False
      Width           =   2400
   End
   Begin VB.Label label36 
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6765
      TabIndex        =   23
      Top             =   6210
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   150
      TabIndex        =   15
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   14
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransKoreksiStockKertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mode As Byte
Dim total As Long
Dim totalHpp As Double

Dim foc As Byte
Dim colname() As String
Dim debet As Boolean
Dim NoJurnal As String

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 nomer_koreksi from t_stockkoreksih where substring(nomer_koreksi,4,4)='" & Format(DTPicker1, "yyMM") & "' order by nomer_koreksi desc", conn
    If Not rs.EOF Then
        No = "KRK" & Format(DTPicker1, "yyMM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "KRK" & Format(DTPicker1, "yyMM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub




Private Sub cmbSerial_LostFocus()
    conn.Open strcon
    txtHPP = GetHPPKertas2(txtKode, cmbSerial.text, cmbGudang.text, conn)
    conn.Close
End Sub

Private Sub cmdClear_Click()
    txtKode.text = ""
    LblNamaBarang = ""
    cmbSerial.Clear
    lblSatuan = ""
    txtQty.text = "0"
    txtBerat.text = "0"
    txtHPP = "0"
    lblStock = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    If flxGrid.Rows <= 2 Then Exit Sub
'    total = total - (flxGrid.TextMatrix(row, 6))
'    lblTotal = Format(total, "#,##0")
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdHistoryHpp_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select jenis,nomer_transaksi,kode_bahan,nomer_serial,hpp from hst_hpp where kode_bahan='" & txtKode & "'"
    frmSearch.nmform = "frmTransKoreksiStockKertas"
    frmSearch.nmctrl = ""
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "history_hpp"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtQty.SetFocus
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKode.text <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKode.text) And flxGrid.TextMatrix(i, 3) = cmbSerial.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = txtKode.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        flxGrid.TextMatrix(row, 3) = cmbSerial.text
        
        If flxGrid.TextMatrix(row, 1) = txtKode.text Then
             flxGrid.TextMatrix(row, 4) = txtQty.text
        Else
            If mode = 1 Then
                flxGrid.TextMatrix(row, 4) = CDbl(flxGrid.TextMatrix(row, 4)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 4) = CDbl(txtQty)
            End If
        End If
        
       
        flxGrid.TextMatrix(row, 5) = lblSatuan
        flxGrid.TextMatrix(row, 6) = txtBerat.text
        flxGrid.TextMatrix(row, 7) = txtHPP
        
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 4
        
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        cmdClear_Click
        txtKode.SetFocus
        
    End If
    mode = 1

End Sub

Private Sub cmdopentreeview_Click()
On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub cmdPosting_Click()
    simpan
    postingKoreksiKertas lblNoTrans, True
    reset_form
End Sub

Private Sub cmdPrint_Click()
    cetaknota lblNoTrans
End Sub



Private Sub cmdreset_Click()
reset_form
End Sub

Private Sub cmdSearchBarang_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select kode_bahan,nama_bahan from ms_bahan "
    frmSearch.nmform = "frmTransKoreksiStockKertas"
    frmSearch.nmctrl = "txtkode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_barang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    txtQty.SetFocus
End Sub

Public Sub cari_barang()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select kode_bahan,nama_bahan from ms_bahan where kode_bahan='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(1)
        txtQty.text = 1
'        lblSatuan = rs(2)
'        lblStock = getStock2(txtKode.text, gudang, strcon)
    Else
        LblNamaBarang = ""
        txtQty.text = 0
'        lblSatuan = ""
        lblStock = "0"
    End If
    rs.Close
    cmbSerial.Clear
    rs.Open "select nomer_serial from stock where kode_bahan='" & txtKode.text & "' and kode_gudang='" & cmbGudang.text & "' ", conn
        While Not rs.EOF
            cmbSerial.AddItem rs(0)
            cmbSerial.Refresh
            rs.MoveNext
        Wend
    rs.Close
    
    conn.Close
End Sub


Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select t.nomer_koreksi,t.tanggal,t.keterangan from t_stockkoreksih t where t.status_posting=0"
    frmSearch.nmform = "frmTransStockOpname"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "koreksiStockKertas"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double
On Error GoTo err
simpan = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan item detail terlebih dahulu"
        txtKeterangan.SetFocus
        Exit Function
    End If
    
    conn.Open strcon
    
    conn.BeginTrans
    
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
        
    Else
        conn.Execute "delete from t_stockkoreksih where nomer_koreksi='" & lblNoTrans & "'"
        conn.Execute "delete from t_stockkoreksid where nomer_koreksi='" & lblNoTrans & "'"
    End If
    
    add_dataheader
    
    
    totalHpp = 0
    For row = 1 To flxGrid.Rows - 2
            add_datadetail (row)
    Next row
    
    conn.CommitTrans
    DropConnection
    simpan = True

    Exit Function
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    MsgBox err.Description
End Function
Public Sub reset_form()
    lblNoTrans = "-"
    lblTotal = "0"
    txtKeterangan.text = ""
    total = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    txtQty = 0
    cmdPrint.Visible = False
'    cmdPosting.Enabled = False
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "t_stockkoreksih"
    fields(0) = "nomer_koreksi"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "kode_gudang"
    fields(4) = "userid"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = cmbGudang.text
    nilai(4) = User
    
    tambah_data table_name, fields, nilai
End Sub

Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    Dim selisih As Double
    
    ReDim fields(7)
    ReDim nilai(7)
    
    table_name = "t_stockkoreksid"
    fields(0) = "nomer_koreksi"
    fields(1) = "kode_bahan"
    fields(2) = "nomer_serial"
    fields(3) = "qty"
'    fields(4) = "stock"
'    fields(5) = "selisih"
    fields(4) = "no_urut"
    fields(5) = "hpp"
    fields(6) = "berat"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = Replace(flxGrid.TextMatrix(row, 4), ",", ".")
'    nilai(4) = flxGrid.TextMatrix(row, 6)
'    selisih = CDbl(nilai(3)) - CDbl(nilai(4))
'    nilai(5) = selisih
    nilai(4) = row
    nilai(5) = Replace(flxGrid.TextMatrix(row, 7), ",", ".")
    nilai(6) = Replace(flxGrid.TextMatrix(row, 6), ",", ".")
    tambah_data table_name, fields, nilai
End Sub



Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select t.* from t_stockkoreksih t " & _
            "where t.nomer_koreksi='" & lblNoTrans & "'", conn
            
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
          SetComboText (rs("kode_gudang")), cmbGudang
        total = 0
'        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.kode_bahan,b.nama_bahan,d.qty,d.nomer_serial,d.hpp,d.berat " & _
                "from t_stockkoreksid d inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                "where d.nomer_koreksi='" & lblNoTrans & "' order by d.no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(3) ' no.serial
                flxGrid.TextMatrix(row, 4) = rs(2) 'qty
                flxGrid.TextMatrix(row, 7) = rs(4)
                flxGrid.TextMatrix(row, 6) = rs(5)
                row = row + 1
                
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(total, "#,##0")
        cmdPosting.Enabled = True
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub DTPicker2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtKode.SetFocus
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKode.text = flxGrid.TextMatrix(flxGrid.row, 1)
        LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
        SetComboText flxGrid.TextMatrix(flxGrid.row, 3), cmbSerial
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 4)
        lblSatuan = flxGrid.TextMatrix(flxGrid.row, 5)
        txtHPP.text = flxGrid.TextMatrix(flxGrid.row, 7)
        txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
'        txtKode.SetFocus
    Else
    Exit Sub
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKode.text = flxGrid.TextMatrix(flxGrid.row, 1)
            LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
            SetComboText flxGrid.TextMatrix(flxGrid.row, 3), cmbSerial
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 4)
            lblSatuan = flxGrid.TextMatrix(flxGrid.row, 5)
            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtKode.SetFocus
        End If
    End If
End Sub

Private Sub Form_Activate()
'    MySendKeys "{tab}"
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    tvwMain.Visible = False
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    load_combo
    LblNamaBarang = ""
    lblSatuan = ""
    lblStock = ""
    loadgrup "0", tvwMain
    reset_form
    total = 0
   
    lblGudang = gudang
    
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1200
    flxGrid.ColWidth(2) = 4000
    flxGrid.ColWidth(3) = 800 ' serial
    flxGrid.ColWidth(4) = 800
    flxGrid.ColWidth(5) = 0
    flxGrid.ColWidth(6) = 1000
    flxGrid.ColWidth(7) = 1000
    
    
     
    flxGrid.TextMatrix(0, 1) = "Kode Barang"
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 3) = "Serial"
    flxGrid.TextMatrix(0, 4) = "Qty"
    flxGrid.TextMatrix(0, 5) = "Satuan"
    flxGrid.TextMatrix(0, 6) = "Berat"
    flxGrid.TextMatrix(0, 7) = "HPP"
    
    
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
End Sub

Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub mnuDelete_Click()
    cmdDelete_Click
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub txtHpp_GotFocus()
    txtHPP.SelStart = 0
    txtHPP.SelLength = Len(txtHPP.text)
End Sub

Private Sub txtKeterangan_GotFocus()
    txtKeterangan.SelStart = 0
    txtKeterangan.SelLength = Len(txtKeterangan.text)
    foc = 1
End Sub



Private Sub txtKode_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdSearchBarang_Click
'If txtKode <> "" Then cari_barang
End Sub

Private Sub txtKode_LostFocus()
    If txtKode <> "" Then cari_barang
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

'Private Sub txtQty_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then cmdOk.SetFocus
'End Sub

'Private Sub txtQty_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        txtQty.text = calc(txtQty.text)
'    End If
'End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "1"
End Sub


Public Sub cetaknota(no_nota As String)
On Error GoTo err
Dim query() As String
Dim rs() As New ADODB.Recordset


    
    conn.Open strcon
    
    ReDim query(2) As String
    query(0) = "select * from t_stockkoreksid"
    query(1) = "select * from t_stockkoreksih"
    query(2) = "select * from ms_bahan"
    
    

    ReDim rs(UBound(query) - 1)
    For i = 0 To UBound(query) - 1
        rs(i).Open query(i), conn, adOpenForwardOnly
    Next
    With CRPrint
        .reset
        .ReportFileName = App.Path & "\Report\PO Stockkoreksi.rpt"
        For i = 0 To UBound(query) - 1
            .SetTablePrivateData i, 3, rs(i)
        Next
        .Formulas(0) = "NamaPerusahaan='" & GNamaPerusahaan & "'"
        .Formulas(1) = "AlamatPerusahaan='" & GAlamatPerusahaan & "'"
        .Formulas(2) = "TeleponPerusahaan='" & GTeleponPerusahaan & "'"

        .SelectionFormula = "{t_stockkoreksih.ID}='" & no_nota & "'"
'        .PrinterSelect
        .ProgressDialog = False
        .WindowState = crptMaximized
'        .Destination = crptToPrinter
        .action = 1
    End With
    conn.Close
    Exit Sub
err:
MsgBox err.Description
End Sub
