VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterPlat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Plat"
   ClientHeight    =   3915
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6960
   ForeColor       =   &H8000000A&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3915
   ScaleWidth      =   6960
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   324
      Left            =   4824
      TabIndex        =   30
      Text            =   "Plat"
      Top             =   1512
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.TextBox txtHpp 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8220
      TabIndex        =   9
      Top             =   3090
      Width           =   1740
   End
   Begin VB.TextBox txtKodeAcc 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   1356
      Width           =   1950
   End
   Begin VB.CommandButton cmdAcc 
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3945
      Picture         =   "frmMasterPlate.frx":0000
      TabIndex        =   25
      Top             =   1296
      Width           =   465
   End
   Begin VB.TextBox txtIsi 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5832
      TabIndex        =   3
      Text            =   "1"
      Top             =   1944
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.ComboBox cmbSatuan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   324
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   972
      Width           =   1995
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel"
      Height          =   330
      Left            =   6450
      TabIndex        =   20
      Top             =   4830
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdBaru 
      Caption         =   "&Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3720
      Picture         =   "frmMasterPlate.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3096
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3240
      Picture         =   "frmMasterPlate.frx":0634
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   2424
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4455
      Picture         =   "frmMasterPlate.frx":0B66
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   2424
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdHistory 
      Caption         =   "&History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   7425
      Picture         =   "frmMasterPlate.frx":1098
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   3870
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6990
      Picture         =   "frmMasterPlate.frx":119A
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   2730
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CheckBox chkAktif 
      Caption         =   "Aktif"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   14
      Top             =   2244
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   735
      Picture         =   "frmMasterPlate.frx":129C
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3096
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3960
      Picture         =   "frmMasterPlate.frx":139E
      TabIndex        =   15
      Top             =   120
      Width           =   465
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   2220
      Picture         =   "frmMasterPlate.frx":14A0
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3096
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5145
      Picture         =   "frmMasterPlate.frx":15A2
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3096
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtNama 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      TabIndex        =   1
      Top             =   570
      Width           =   3480
   End
   Begin VB.TextBox txtKode 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1920
      TabIndex        =   0
      Top             =   180
      Width           =   1950
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8100
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5256
      TabIndex        =   31
      Top             =   1188
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Harga Pokok"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6960
      TabIndex        =   29
      Top             =   3120
      Width           =   1065
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Perkiraan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   228
      TabIndex        =   28
      Top             =   1404
      Width           =   1296
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1560
      TabIndex        =   27
      Top             =   1356
      Width           =   156
   End
   Begin VB.Label lblNamaPerkiraan 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Perkiraan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   192
      Left            =   1920
      TabIndex        =   26
      Top             =   1800
      Width           =   1128
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Lembar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5436
      TabIndex        =   24
      Top             =   2340
      Visible         =   0   'False
      Width           =   1308
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5544
      TabIndex        =   23
      Top             =   1980
      Visible         =   0   'False
      Width           =   156
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Satuan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   228
      TabIndex        =   22
      Top             =   972
      Width           =   780
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1545
      TabIndex        =   19
      Top             =   600
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1575
      TabIndex        =   18
      Top             =   180
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   48
      TabIndex        =   17
      Top             =   2772
      Width           =   2136
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Plat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   16
      Top             =   540
      Width           =   1080
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Plat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   21
      Top             =   225
      Width           =   1140
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterPlat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim foc As Byte
Dim colname() As String

Private Sub cmdAcc_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select * from ms_coa"
    frmSearch.nmform = "frmMasterplat"
    frmSearch.nmctrl = "txtKodeAcc"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_coa"
    frmSearch.proc = "cari_perkiraan"
    frmSearch.col = 0
    
    frmSearch.Index = -1
    
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
'    cmdSimpan.SetFocus
End Sub

Private Sub cmdBaru_Click()
    reset_form
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
i = 0
    If txtKode.text = "" Then
        MsgBox "Silahkan masukkan Kode plat terlebih dahulu!", vbCritical
        txtKode.SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    rs.Open "select * from stock_plat where kode_plat='" & txtKode.text & "'" And stock < 0, conn
    While Not rs.EOF
        'conn.Execute "delete from ms_plat where kode_plat='" & txtKode.text & "'"
        conn.Execute "delete from stock_plat where kode_plat='" & txtKode.text & "' and stock='" & rs!stock & "'"
    rs.MoveNext
    Wend
    If rs.State Then rs.Close

    conn.CommitTrans
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    Set conn = Nothing
    cmdPrev_Click
    cmdNext_Click
    reset_form
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    conn.RollbackTrans
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 kode_plat from ms_plat where kode_plat>'" & txtKode.text & "' order by kode_plat", conn
    If Not rs.EOF Then
        txtKode.text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection

    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 kode_plat from ms_plat where kode_plat<'" & txtKode.text & "' order by kode_plat desc", conn
    If Not rs.EOF Then
        txtKode.text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
'    frmPrintPLU.kode = txtkode.text
'    frmPrintPLU.Show
    frmPrintListBarang.Show vbModal
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select *  from ms_plat" ' order by kode_plat"
    frmSearch.nmform = "frmMasterplat"
    frmSearch.nmctrl = "txtKode"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_plat"
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = -1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.requery
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
        If txtKode.text = "" Then
            MsgBox "Kode plat harus diisi"
            txtKode.SetFocus
            Exit Sub
        End If
        If txtNama.text = "" Then
            MsgBox "Nama plat harus diisi"
            txtNama.SetFocus
            Exit Sub
        End If
        If txtIsi.text = "" Then
            MsgBox "Jumlah plat harus diisi"
            txtIsi.SetFocus
            Exit Sub
        End If
        If txtKodeAcc.text = "" Then
            MsgBox "Kode Perkiraan harus diisi"
            txtKodeAcc.SetFocus
            Exit Sub
        End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
i = 1
    
'    rs.Open "select * from hpp_plat where kode_plat='" & txtKode.text & "' ", conn
'    If rs.EOF Then
'        rs.Close
'        conn.Execute "insert into hpp_plat  (kode_plat,hpp) values ('" & txtKode.text & "','" & Replace(txtHpp, ",", ".") & "')"
'
'    End If
'    If rs.State Then rs.Close
    rs.Open "select * from ms_plat where kode_plat='" & txtKode.text & "'", conn
    If Not rs.EOF Then
        add_dataheader 2, conn
        add_dataheaderRpt 2, conn
    Else
        add_dataheader 1, conn
        add_dataheaderRpt 1, conn

    End If
    If rs.State Then rs.Close
    
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"

    DropConnection
    reset_form

    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    load_combo
    reset_form
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub


Private Sub txthpp_KeyPress(KeyAscii As Integer)
 Angka KeyAscii
End Sub

Private Sub txtIsi_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtKodeAcc_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then cmdAcc_Click
End Sub

Private Sub txtnama_GotFocus()
    txtNama.SelStart = 0
    txtNama.SelLength = Len(txtNama.text)
    foc = 1
End Sub



Private Sub txtKode_LostFocus()
    foc = 0
    If txtKode.text <> "" Then cari_data

End Sub
Private Sub txtIsi_GotFocus()
    txtIsi.SelStart = 0
    txtIsi.SelLength = Len(txtIsi)
End Sub
Private Sub reset_form()
On Error Resume Next
    txtKode.text = ""
    txtNama.text = ""
    txtIsi.text = 0
    txtKodeAcc = ""
    lblNamaPerkiraan = ""
    txtHPP = 0
    If cmbSatuan.ListCount > 0 Then cmbSatuan.ListIndex = 0
    If cmbKategori.ListCount > 0 Then cmbKategori.ListIndex = 0
    cmdPrev.Enabled = False
    cmdNext.Enabled = False
    txtKode.SetFocus
    
End Sub
Private Sub add_dataheader(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "ms_plat"
    
    fields(0) = "kode_plat"
    fields(1) = "nama_plat"
    fields(2) = "satuan"
    fields(3) = "kategori"
    fields(4) = "jumlah_lembar"
    fields(5) = "kode_acc"
    
    nilai(0) = txtKode.text
    nilai(1) = Replace(txtNama.text, "'", "''")
    nilai(2) = cmbSatuan.text
    nilai(3) = cmbKategori.text
    nilai(4) = Replace(txtIsi.text, ",", ".")
    nilai(5) = txtKodeAcc.text

    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "kode_plat='" & txtKode.text & "'", cn
    End If
End Sub
Private Sub add_dataheaderRpt(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(3)
    ReDim nilai(3)

    table_name = "rpt_ms_plat"
    fields(0) = "kode_plat"
    fields(1) = "nama_plat"
    fields(2) = "jumlah_lembar"
    
    nilai(0) = txtKode.text
    nilai(1) = Replace(txtNama.text, "'", "''")
    nilai(2) = Replace(txtIsi.text, ",", ".")

    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "kode_plat='" & txtKode.text & "'", cn
    End If
End Sub



Public Sub cari_data()
    If txtKode.text = "" Then Exit Sub
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from ms_plat where kode_plat='" & txtKode.text & "' ", conn

    If Not rs.EOF Then
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        txtNama.text = rs!nama_plat
        If Not IsNull(rs!jumlah_lembar) Then txtIsi.text = rs!jumlah_lembar Else txtIsi = 0
        txtKodeAcc.text = rs("kode_acc")
            
        SetComboText rs!satuan, cmbSatuan
        SetComboText rs!kategori, cmbKategori

        cmdHapus.Enabled = True
        mnuHapus.Enabled = True

    Else
        cmdPrev.Enabled = False
        cmdPrev.Enabled = False
        txtNama.text = ""
        txtKodeAcc = ""
        txtIsi = 0
        cmdHapus.Enabled = False
        mnuHapus.Enabled = True

    End If
    If rs.State Then rs.Close
    conn.Close
    cari_perkiraan
End Sub

Private Sub load_combo()
    
    conn.ConnectionString = strcon
    conn.Open
     
    cmbSatuan.Clear
    rs.Open "select * from var_satuan order by satuan", conn
        While Not rs.EOF
            cmbSatuan.AddItem rs(0)
        rs.MoveNext
        Wend
    rs.Close
    cmbKategori.Clear
    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    If cmbKategori.ListCount > 0 Then cmbKategori.ListIndex = 0
    If cmbSatuan.ListCount > 0 Then cmbSatuan.ListIndex = 0
    
End Sub
Public Sub cari_perkiraan()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_coa where kode_acc='" & txtKodeAcc.text & "'", conn
    If Not rs.EOF Then
        lblNamaPerkiraan.Caption = rs(1)
    Else
        lblNamaPerkiraan.Caption = ""
    End If
    rs.Close
    conn.Close
End Sub
Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
        MsgBox "Loading selesai"
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Function loadexcelfile1(filename As String) As Boolean
On Error GoTo err
Dim exapp As Object
Dim range As String
Dim rs2 As New ADODB.Recordset
Dim col As Byte
Dim i As Integer
loadexcelfile1 = False
Set exapp = CreateObject("excel.application")
exapp.Workbooks.Open filename
exapp.Sheets(1).Select
conn.Open strcon
col = Asc("A")
range = Chr(col) & "1"

While exapp.Workbooks.Application.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exapp.Workbooks.Application.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

i = 2
While exapp.range("A" & CStr(i)) <> ""
        rs2.Open "select * from var_kategori where kategori='" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
'        rs2.Open "select * from ms_bahan where kode_plat='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
'        If rs2.EOF Then
'            newitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))
'        Else
'            updateitem exapp.range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("nama", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("PriceList", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga1", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("harga2", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("h_eceran", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("kategori", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("merk", colname) + 65) & CStr(i)), _
'            exapp.range(Chr(getcolindex("satuan", colname) + 65) & CStr(i))
'
'        End If
'        rs2.Close
        rs2.Open "select * from stock_plat where kode_plat='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
        conn.Execute "insert into stock_plat  (kode_gudang,kode_plat,stock)  values ('" & gudang & "'," & _
            "'" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
            "'" & exapp.range(Chr(getcolindex("stok", colname) + 65) & CStr(i)) & "')"
        End If
        rs2.Close
        rs2.Open "select * from hpp_plat where kode_plat='" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'", conn
        If rs2.EOF Then
        conn.Execute "insert into hpp_plat (kode_plat,hpp)  values ('" & exapp.range(Chr(getcolindex("barcode", colname) + 65) & CStr(i)) & "'," & _
            "" & IIf(IsNumeric(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), CDbl(exapp.range(Chr(getcolindex("hargaPokok", colname) + 65) & CStr(i))), 0) & ")"
        End If
        rs2.Close

    i = i + 1
Wend
exapp.Workbooks.Close
loadexcelfile1 = True
exapp.Application.Quit
Set exapp = Nothing

MsgBox "Success"
conn.Close
Exit Function
err:
MsgBox err.Description
Resume Next
''If conn.State Then conn.Close
''If exapp.Workbooks.Count > 0 Then exapp.Workbooks.Close
''exapp.Application.Quit
''Set exapp = Nothing
End Function
Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset

Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=Excel 8.0"
con.Open stcon
conn.Open strcon
rs.Open "select * from [sheet1$]", con
While Not rs.EOF
    If rs(0) <> "E" Then
        rs2.Open "select * from var_kategori where kategori='" & rs("kategori") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & rs("kategori") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & rs("merk") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & rs("merk") & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_satuan where satuan='" & rs("satuan") & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_satuan values ('" & rs("satuan") & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_plat where kode_plat='" & rs("kode_plat") & "'", conn
        If rs2.EOF Then
            newitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")

            conn.Execute "insert into stock_plat  (kode_gudang,kode_plat,stock)  select gudang,'" & rs("kodebarang") & "','" & rs("stok") & "' from var_gudang"
            conn.Execute "insert into hpp (kode_gudang,kode_plat,hpp)  select gudang,'" & rs("kodebarang") & "','" & rs("hargapokok") & "' from var_gudang"
        Else
            updateitem rs("kodebarang"), rs("barcode"), rs("nama"), rs("pricelist"), rs("harga1"), rs("harga2"), rs("h_eceran"), rs("kategori"), rs("merk"), rs("satuan")
        End If
        rs2.Close
    End If
    rs.MoveNext
Wend
rs.Close
con.Close
DropConnection
Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub

