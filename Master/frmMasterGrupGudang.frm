VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMasterGroupGudang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Grup Gudang"
   ClientHeight    =   5880
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8445
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5880
   ScaleWidth      =   8445
   Begin VB.CommandButton cmdNewItem 
      Caption         =   "&New"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5760
      Picture         =   "frmMasterGrupGudang.frx":0000
      TabIndex        =   2
      Top             =   2430
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmd_dn 
      Caption         =   "Dn"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4770
      Picture         =   "frmMasterGrupGudang.frx":0102
      TabIndex        =   6
      Top             =   1530
      UseMaskColor    =   -1  'True
      Width           =   465
   End
   Begin VB.CommandButton cmd_up 
      Caption         =   "Up"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4770
      Picture         =   "frmMasterGrupGudang.frx":0204
      TabIndex        =   5
      Top             =   1035
      UseMaskColor    =   -1  'True
      Width           =   465
   End
   Begin VB.TextBox txtGrup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6570
      TabIndex        =   0
      Top             =   630
      Width           =   1725
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   5460
      Left            =   225
      TabIndex        =   7
      Top             =   180
      Width           =   4380
      _ExtentX        =   7726
      _ExtentY        =   9631
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   4
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5760
      Picture         =   "frmMasterGrupGudang.frx":0306
      TabIndex        =   4
      Top             =   3510
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "&New Child"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5760
      Picture         =   "frmMasterGrupGudang.frx":0408
      TabIndex        =   3
      Top             =   2970
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   5760
      Picture         =   "frmMasterGrupGudang.frx":050A
      TabIndex        =   1
      Top             =   1935
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label lblKey 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6615
      TabIndex        =   12
      Top             =   675
      Width           =   1005
   End
   Begin VB.Label lblKetParent 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6570
      TabIndex        =   11
      Top             =   270
      Width           =   1830
   End
   Begin VB.Label lblParent 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6615
      TabIndex        =   10
      Top             =   270
      Width           =   1005
   End
   Begin VB.Label Label2 
      Caption         =   "Grup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5490
      TabIndex        =   9
      Top             =   675
      Width           =   1005
   End
   Begin VB.Label Label1 
      Caption         =   "Parent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5490
      TabIndex        =   8
      Top             =   270
      Width           =   1005
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterGroupGudang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim uncheck As Boolean
Private Declare Function SendMessageLong Lib "user32" Alias "SendMessageA" _
    (ByVal hwnd As Long, ByVal Msg As Long, ByVal wParam As Long, _
    ByVal lParam As Long) As Long
Private Const WM_SETREDRAW As Long = &HB
Private Const TV_FIRST As Long = &H1100
Private Const TVM_GETNEXTITEM As Long = (TV_FIRST + 10)
Private Const TVM_DELETEITEM As Long = (TV_FIRST + 1)
Private Const TVGN_ROOT As Long = &H0


' Quicky clear the treeview identified by the hWnd parameter
Sub ClearTreeViewNodes(ByVal hwnd As Long)
    Dim hItem As Long
    
    ' lock the window update to avoid flickering
    SendMessageLong hwnd, WM_SETREDRAW, False, &O0

    ' clear the treeview
    Do
        hItem = SendMessageLong(hwnd, TVM_GETNEXTITEM, TVGN_ROOT, 0)
        If hItem <= 0 Then Exit Do
        SendMessageLong hwnd, TVM_DELETEITEM, &O0, hItem
    Loop
    
    ' unlock the window
    SendMessageLong hwnd, WM_SETREDRAW, True, &O0
End Sub

Private Sub cmdHapus_Click()
    
End Sub

Private Sub cmd_dn_Click()
   Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim key, grup, keyswitch, grupswitch As String
    Dim index As Integer
    index = tvwMain.SelectedItem.index
    key = IIf(Len(lblKey) > 0, Mid(lblKey, 2, Len(lblKey)), "0")
    
    conn.Open strcon
    rs.Open "select top 1 urut,grup,parent from ms_grupgudang where parent='" & IIf(Len(lblParent) > 0, Mid(lblParent, 2, Len(lblParent)), "0") & "' and urut>'" & key & "' order by urut asc", conn
    If Not rs.EOF Then
        keyswitch = rs(0)
        grupswitch = rs(1)
    End If
    rs.Close
    rs.Open "select grup from ms_grupgudang where urut='" & key & "'", conn
    If Not rs.EOF Then
        grup = rs(0)
    End If
    rs.Close
    conn.Execute "update ms_grupgudang set grup='" & grupswitch & "' where urut='" & key & "'"
    conn.Execute "update ms_grupgudang set grup='" & grup & "' where urut='" & keyswitch & "'"
    conn.Execute "update ms_grupgudang set parent='-1' where parent='" & keyswitch & "'"
    conn.Execute "update ms_grupgudang set parent='" & keyswitch & "' where parent='" & key & "'"
    conn.Execute "update ms_grupgudang set parent='" & key & "' where parent='-1'"
    conn.Close
    ClearTreeViewNodes tvwMain.hwnd
    loadgrup "0"
    
    tvwMain.Nodes(index).Selected = True
    index = tvwMain.SelectedItem.Next.index
    tvwMain.Nodes(tvwMain.SelectedItem.Next.index).Selected = True
    
    tvwMain_NodeClick tvwMain.Nodes(index)
End Sub

Private Sub cmd_up_Click()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim key, grup, keyswitch, grupswitch As String
    Dim index As Integer
    index = tvwMain.SelectedItem.Previous.index
    key = IIf(Len(lblKey) > 0, Mid(lblKey, 2, Len(lblKey)), "0")
    
    conn.Open strcon
    rs.Open "select top 1 urut,grup,parent from ms_grupgudang where parent='" & IIf(Len(lblParent) > 0, Mid(lblParent, 2, Len(lblParent)), "0") & "' and urut<'" & key & "' order by urut desc", conn
    If Not rs.EOF Then
        keyswitch = rs(0)
        grupswitch = rs(1)
    End If
    rs.Close
    rs.Open "select grup from ms_grupgudang where urut='" & key & "'", conn
    If Not rs.EOF Then
        grup = rs(0)
    End If
    rs.Close
    conn.Execute "update ms_grupgudang set grup='" & grupswitch & "' where urut='" & key & "'"
    conn.Execute "update ms_grupgudang set grup='" & grup & "' where urut='" & keyswitch & "'"
    conn.Execute "update ms_grupgudang set parent='-1' where parent='" & keyswitch & "'"
    conn.Execute "update ms_grupgudang set parent='" & keyswitch & "' where parent='" & key & "'"
    conn.Execute "update ms_grupgudang set parent='" & key & "' where parent='-1'"
    conn.Close
    ClearTreeViewNodes tvwMain.hwnd
    loadgrup "0"
    tvwMain.Nodes(index).Selected = True
    tvwMain_NodeClick tvwMain.Nodes(index)
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [group] from var_usergroup" ' order by satuan"
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.nmform = "frmMasteruserGroup"
    frmSearch.nmctrl = "txtField"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "varUserGroup"
    frmSearch.col = 0
    frmSearch.index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.top = 800
    frmSearch.Left = 70
    frmSearch.Show vbModal
    
End Sub

Private Sub reset_form()
On Error Resume Next
    tvwMain.SetFocus
    lblParent = tvwMain.SelectedItem.key
    lblKetParent = tvwMain.SelectedItem.text
    txtGrup = ""
    ClearTreeViewNodes tvwMain.hwnd
    loadgrup "0"
End Sub


Private Sub cmdNew_Click()
    lblKetParent = tvwMain.SelectedItem.text
    lblParent = tvwMain.SelectedItem.key
    lblKey = ""
    txtGrup = ""
    txtGrup.SetFocus
End Sub

Private Sub cmdNewItem_Click()
On Error Resume Next
    lblKey = ""
    txtGrup.SetFocus
End Sub

Private Sub cmdSimpan_Click()
On Error Resume Next
    conn.Open strcon
    If lblKey <> "" Then
        conn.Execute "update ms_grupgudang set parent='" & IIf(Len(lblParent) > 0, Mid(lblParent, 2, Len(lblParent)), "0") & "',[grup]='" & txtGrup.text & "' where [urut]='" & Mid(lblKey, 2, Len(lblKey)) & "'"
    Else
        conn.Execute "insert into ms_grupgudang (parent,grup) values ('" & IIf(Len(lblParent) > 0, Mid(lblParent, 2, Len(lblParent)), "0") & "','" & txtGrup.text & "')"
    End If
    conn.Close
    
    ClearTreeViewNodes tvwMain.hwnd
    loadgrup "0"
    lblKey = ""
    txtGrup = ""
    txtGrup.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
loadgrup "0"
'lblParent = tvwMain.Nodes.Item(0).key
'lblKetParent = tvwMain.Nodes.Item(0).text
'
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub tvwMain_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        deletegrup Mid(tvwMain.SelectedItem.key, 2, Len(tvwMain.SelectedItem.key))
        deletekey Mid(tvwMain.SelectedItem.key, 2, Len(tvwMain.SelectedItem.key))
        reset_form
    End If
End Sub
Private Sub deletegrup(key As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.Open strcon
    rs.Open "select * from ms_grupgudang where parent='" & key & "'", conn
    While Not rs.EOF
        deletegrup (rs!urut)
        conn.Execute "delete from ms_grupgudang where urut='" & rs!urut & "'"
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub
Private Sub deletekey(key As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.Execute "delete from ms_grupgudang where urut='" & key & "'"
    conn.Close
End Sub
Private Sub tvwMain_NodeCheck(ByVal Node As MSComctlLib.Node)
'Dim nod As Node
'    Node.Checked = True
'    uncheck = False
'    Set nod = Node.Child
'    For i = 1 To Node.Children
'        nod.Checked = True
'        Set nod = nod.Next
'    Next
'    Set xnode = Node
'    uncheck = True
'

End Sub


Private Sub txtField_KeyPress(index As Integer, KeyAscii As Integer)

End Sub

Private Sub txtField_LostFocus(index As Integer)
'    If Index = 0 Then cari_data
End Sub

Private Sub loadgrup(parent As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim nodex As Node
conn.Open strcon
rs.Open "select * from ms_grupgudang where parent='" & parent & "' order by urut", conn
While Not rs.EOF
    Set nodex = tvwMain.Nodes.Add(IIf(rs("parent") = "0", Null, "a" & rs("parent").value), IIf(rs("parent") = "", Null, tvwChild), "a" & rs("urut"), rs("grup"))
    nodex.Expanded = True
    loadgrup (rs!urut)
    rs.MoveNext
Wend
rs.Close
conn.Close
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    If InStr(1, Node.FullPath, "\") Then
        lblParent = Node.parent.key
        lblKetParent = Node.parent.text
    Else
        lblParent = ""
        lblKetParent = ""
    End If
    lblKey = Node.key
    txtGrup.text = Node.text
    If tvwMain.Nodes(tvwMain.SelectedItem.index).FirstSibling.key = Node.key Then
        cmd_up.Enabled = False
    Else
        cmd_up.Enabled = True
    End If
    If tvwMain.Nodes(tvwMain.SelectedItem.index).LastSibling.key = Node.key Then
        cmd_dn.Enabled = False
    Else
        cmd_dn.Enabled = True
    End If
End Sub
