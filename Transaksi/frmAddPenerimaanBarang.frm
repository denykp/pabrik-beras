VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPenerimaanbarang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Penerimaan Barang"
   ClientHeight    =   9225
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   13560
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9225
   ScaleWidth      =   13560
   Begin VB.CheckBox chkAngkut 
      Caption         =   "Ongkos Angkut"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4275
      TabIndex        =   98
      Top             =   1035
      Value           =   1  'Checked
      Width           =   1590
   End
   Begin VB.CommandButton cmdPrev 
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5580
      Picture         =   "frmAddPenerimaanBarang.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   76
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6675
      Picture         =   "frmAddPenerimaanBarang.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   75
      Top             =   45
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.TextBox txtKodeEkspedisi 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5445
      TabIndex        =   72
      Top             =   1380
      Width           =   2025
   End
   Begin VB.CommandButton cmdSearchEkspedisi 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   71
      Top             =   1350
      Width           =   420
   End
   Begin VB.CommandButton Command1 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11925
      Picture         =   "frmAddPenerimaanBarang.frx":0A64
      TabIndex        =   70
      Top             =   90
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchPosting 
      Caption         =   "Search Post"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4185
      Picture         =   "frmAddPenerimaanBarang.frx":0B66
      TabIndex        =   69
      Top             =   90
      Width           =   1320
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   3030
      Left            =   7065
      TabIndex        =   64
      Top             =   3735
      Visible         =   0   'False
      Width           =   3210
      _ExtentX        =   5662
      _ExtentY        =   5345
      _Version        =   393217
      HideSelection   =   0   'False
      LineStyle       =   1
      Style           =   6
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtNoSJ 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   2
      Top             =   1395
      Width           =   1935
   End
   Begin VB.CommandButton cmdSearchPO 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3510
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   31
      Top             =   540
      Width           =   420
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3015
      Picture         =   "frmAddPenerimaanBarang.frx":0C68
      TabIndex        =   29
      Top             =   8595
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   225
      Picture         =   "frmAddPenerimaanBarang.frx":0D6A
      TabIndex        =   16
      Top             =   8595
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4410
      Picture         =   "frmAddPenerimaanBarang.frx":0E6C
      TabIndex        =   18
      Top             =   8595
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   4
      Top             =   1785
      Width           =   8250
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3708
      Picture         =   "frmAddPenerimaanBarang.frx":0F6E
      TabIndex        =   22
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10755
      TabIndex        =   21
      Top             =   1875
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11655
      TabIndex        =   20
      Top             =   1860
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3555
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   19
      Top             =   1395
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   1005
      Width           =   1530
   End
   Begin VB.TextBox txtNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   585
      Width           =   1875
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1620
      Picture         =   "frmAddPenerimaanBarang.frx":1070
      TabIndex        =   17
      Top             =   8595
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   9225
      TabIndex        =   3
      Top             =   105
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   93913091
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   9225
      TabIndex        =   65
      Top             =   495
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   163053571
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker3 
      Height          =   330
      Left            =   9225
      TabIndex        =   66
      Top             =   855
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "HH:mm"
      Format          =   163053571
      UpDown          =   -1  'True
      CurrentDate     =   38927
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5835
      Index           =   0
      Left            =   270
      TabIndex        =   34
      Top             =   2565
      Width           =   12330
      Begin VB.Frame Frame2 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Detail"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1635
         Left            =   90
         TabIndex        =   35
         Top             =   135
         Width           =   9960
         Begin VB.CommandButton cmdopentreeview 
            Appearance      =   0  'Flat
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   9495
            MaskColor       =   &H00C0FFFF&
            TabIndex        =   62
            Top             =   630
            Width           =   330
         End
         Begin VB.ComboBox cmbGudang 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   6660
            Style           =   2  'Dropdown List
            TabIndex        =   61
            Top             =   585
            Width           =   2805
         End
         Begin VB.TextBox txtQty 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   6
            Top             =   675
            Width           =   1140
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   3330
            ScaleHeight     =   465
            ScaleWidth      =   3795
            TabIndex        =   37
            Top             =   945
            Width           =   3795
            Begin VB.CommandButton cmdOk 
               BackColor       =   &H8000000C&
               Caption         =   "&Tambahkan"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   0
               TabIndex        =   8
               Top             =   90
               Width           =   1350
            End
            Begin VB.CommandButton cmdClear 
               BackColor       =   &H8000000C&
               Caption         =   "&Baru"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   1440
               TabIndex        =   9
               Top             =   90
               Width           =   1050
            End
            Begin VB.CommandButton cmdDelete 
               BackColor       =   &H8000000C&
               Caption         =   "&Hapus"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   2595
               TabIndex        =   14
               Top             =   90
               Width           =   1050
            End
         End
         Begin VB.TextBox txtBerat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            TabIndex        =   7
            Top             =   1050
            Width           =   1140
         End
         Begin VB.CommandButton cmdSearchBrg 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3885
            Picture         =   "frmAddPenerimaanBarang.frx":1172
            TabIndex        =   36
            Top             =   240
            Width           =   420
         End
         Begin VB.ComboBox txtKdBrg 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1260
            TabIndex        =   5
            Text            =   "Combo1"
            Top             =   225
            Width           =   2580
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Gudang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   5175
            TabIndex        =   63
            Top             =   645
            Width           =   1320
         End
         Begin VB.Label lblSatuan 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Height          =   330
            Left            =   2475
            TabIndex        =   44
            Top             =   675
            Width           =   1050
         End
         Begin VB.Label lblKode 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Label12"
            ForeColor       =   &H8000000C&
            Height          =   330
            Left            =   3825
            TabIndex        =   43
            Top             =   1305
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label LblNamaBarang1 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "caption"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   4545
            TabIndex        =   42
            Top             =   315
            Width           =   3480
         End
         Begin VB.Label Label3 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kemasan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   41
            Top             =   720
            Width           =   1005
         End
         Begin VB.Label Label14 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kode Barang"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   40
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Berat"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   39
            Top             =   1080
            Width           =   1020
         End
         Begin VB.Label Label18 
            BackColor       =   &H8000000C&
            BackStyle       =   0  'Transparent
            Caption         =   "Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   2475
            TabIndex        =   38
            Top             =   1110
            Width           =   600
         End
      End
      Begin MSFlexGridLib.MSFlexGrid flxGrid 
         Height          =   2295
         Left            =   90
         TabIndex        =   15
         Top             =   1890
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   4048
         _Version        =   393216
         Cols            =   9
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxgridRekap 
         Height          =   1125
         Left            =   90
         TabIndex        =   53
         Top             =   4230
         Width           =   9960
         _ExtentX        =   17568
         _ExtentY        =   1984
         _Version        =   393216
         Cols            =   5
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label24 
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6210
         TabIndex        =   55
         Top             =   5400
         Width           =   960
      End
      Begin VB.Label lblBerat 
         Alignment       =   1  'Right Justify
         Caption         =   "lblBerat"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7155
         TabIndex        =   54
         Top             =   5400
         Width           =   1140
      End
      Begin VB.Label Label16 
         Caption         =   "Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   45
         TabIndex        =   48
         Top             =   5400
         Width           =   735
      End
      Begin VB.Label lblItem 
         Alignment       =   1  'Right Justify
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   990
         TabIndex        =   47
         Top             =   5400
         Width           =   1140
      End
      Begin VB.Label lblQty 
         Alignment       =   1  'Right Justify
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4320
         TabIndex        =   46
         Top             =   5400
         Width           =   1140
      End
      Begin VB.Label Label17 
         Caption         =   "Jml Kemasan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2520
         TabIndex        =   45
         Top             =   5400
         Width           =   1770
      End
   End
   Begin VB.Frame frDetail 
      BorderStyle     =   0  'None
      Height          =   5565
      Index           =   1
      Left            =   270
      TabIndex        =   49
      Top             =   2655
      Visible         =   0   'False
      Width           =   12870
      Begin VB.Frame frangkut 
         Caption         =   "Rincian Biaya"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3090
         Left            =   5355
         TabIndex        =   77
         Top             =   2340
         Visible         =   0   'False
         Width           =   6705
         Begin VB.TextBox txtBiayaAngkutKG 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3045
            TabIndex        =   86
            Top             =   540
            Width           =   975
         End
         Begin VB.TextBox txtBiayaAngkutBerat 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1755
            TabIndex        =   85
            Top             =   540
            Width           =   975
         End
         Begin VB.TextBox txtBiayaLain 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   4425
            TabIndex        =   84
            Top             =   1020
            Width           =   2070
         End
         Begin VB.TextBox txtBiayaAngkut 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   4425
            Locked          =   -1  'True
            TabIndex        =   83
            Top             =   540
            Width           =   2070
         End
         Begin VB.PictureBox Picture3 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   1800
            ScaleHeight     =   345
            ScaleWidth      =   2670
            TabIndex        =   79
            Top             =   2580
            Width           =   2700
            Begin VB.OptionButton OptCaraBayar 
               Caption         =   "Kredit"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   1575
               TabIndex        =   81
               Top             =   45
               Width           =   915
            End
            Begin VB.OptionButton OptCaraBayar 
               Caption         =   "Tunai"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   90
               TabIndex        =   80
               Top             =   45
               Value           =   -1  'True
               Width           =   915
            End
            Begin VB.Label Label30 
               BackStyle       =   0  'Transparent
               Caption         =   "Jumlah bayar"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   135
               TabIndex        =   82
               Top             =   1035
               Visible         =   0   'False
               Width           =   1725
            End
         End
         Begin VB.TextBox txtKeteranganEkspedisi 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1755
            TabIndex        =   78
            Top             =   2025
            Width           =   4770
         End
         Begin VB.Line Line1 
            X1              =   4170
            X2              =   6540
            Y1              =   1515
            Y2              =   1515
         End
         Begin VB.Label Label33 
            Caption         =   "Total Biaya Angkut"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4590
            TabIndex        =   97
            Top             =   240
            Width           =   1680
         End
         Begin VB.Label Label28 
            Caption         =   "="
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   4155
            TabIndex        =   96
            Top             =   585
            Width           =   165
         End
         Begin VB.Label Label27 
            Caption         =   "Biaya / Kg"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3135
            TabIndex        =   95
            Top             =   240
            Width           =   885
         End
         Begin VB.Label Label26 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   2835
            TabIndex        =   94
            Top             =   585
            Width           =   165
         End
         Begin VB.Label Label25 
            Caption         =   "Berat"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1980
            TabIndex        =   93
            Top             =   240
            Width           =   450
         End
         Begin VB.Label Label23 
            Caption         =   "Biaya Lain-Lain"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   270
            TabIndex        =   92
            Top             =   1065
            Width           =   1380
         End
         Begin VB.Label Label21 
            Caption         =   "Biaya Angkut"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   270
            TabIndex        =   91
            Top             =   585
            Width           =   1170
         End
         Begin VB.Label Label29 
            Caption         =   "Total Biaya :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2175
            TabIndex        =   90
            Top             =   1605
            Width           =   1575
         End
         Begin VB.Label Label31 
            Caption         =   "Cara Bayar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   285
            TabIndex        =   89
            Top             =   2640
            Width           =   1380
         End
         Begin VB.Label lblTotalBiaya 
            Alignment       =   1  'Right Justify
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3840
            TabIndex        =   88
            Top             =   1605
            Width           =   2700
         End
         Begin VB.Label Label32 
            Caption         =   "Keterangan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   270
            TabIndex        =   87
            Top             =   2055
            Width           =   1380
         End
      End
      Begin VB.TextBox txtNoTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1485
         TabIndex        =   59
         Top             =   180
         Width           =   1755
      End
      Begin VB.ListBox listTruk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   3870
         TabIndex        =   58
         Top             =   180
         Width           =   3660
      End
      Begin VB.CommandButton cmdSearchPengawas 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3375
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   56
         Top             =   1575
         Width           =   420
      End
      Begin VB.ListBox listKuli 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2760
         Left            =   1530
         Style           =   1  'Checkbox
         TabIndex        =   13
         Top             =   2655
         Width           =   3705
      End
      Begin VB.ListBox listPengawas 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   3915
         TabIndex        =   12
         Top             =   1575
         Width           =   3660
      End
      Begin VB.TextBox txtPengawas 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         TabIndex        =   11
         Top             =   1575
         Width           =   1755
      End
      Begin VB.TextBox txtSupir 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1530
         TabIndex        =   10
         Top             =   1125
         Width           =   5220
      End
      Begin VB.Label Label5 
         Caption         =   "No. Truk"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   60
         Top             =   225
         Width           =   960
      End
      Begin VB.Label lblNamaPengawas 
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1530
         TabIndex        =   57
         Top             =   1980
         Width           =   2265
      End
      Begin VB.Label Label9 
         Caption         =   "Supir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   52
         Top             =   1215
         Width           =   960
      End
      Begin VB.Label Label8 
         Caption         =   "Kuli"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   51
         Top             =   2700
         Width           =   960
      End
      Begin VB.Label Label6 
         Caption         =   "Pengawas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   50
         Top             =   1620
         Width           =   960
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   6315
      Left            =   180
      TabIndex        =   33
      Top             =   2205
      Width           =   13065
      _ExtentX        =   23045
      _ExtentY        =   11139
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Barang"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Pekerja"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblNamaEkspedisi 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8100
      TabIndex        =   74
      Top             =   1410
      Width           =   3825
   End
   Begin VB.Label Label10 
      Caption         =   "Ekspedisi :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4290
      TabIndex        =   73
      Top             =   1425
      Width           =   960
   End
   Begin VB.Label Label11 
      Caption         =   "Mulai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7740
      TabIndex        =   68
      Top             =   540
      Width           =   1050
   End
   Begin VB.Label Label15 
      Caption         =   "Selesai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7740
      TabIndex        =   67
      Top             =   900
      Width           =   1050
   End
   Begin VB.Label Label1 
      Caption         =   "No SJ"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   32
      Top             =   1440
      Width           =   960
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   150
      TabIndex        =   30
      Top             =   1335
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   28
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   27
      Top             =   1785
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7740
      TabIndex        =   26
      Top             =   150
      Width           =   1320
   End
   Begin VB.Label Label7 
      Caption         =   "No.Order"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   25
      Top             =   600
      Width           =   1275
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   24
      Top             =   1050
      Width           =   960
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   23
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPenerimaanbarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty, totalberat As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte

Private Sub chkAngkut_Click()
    If chkAngkut.value = False Then frAngkut.Visible = False Else frAngkut.Visible = True
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang1 = ""
    txtQty.text = "1"
    txtBerat.text = "0"

    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.Rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    row = flxGrid.row
    totalQty = totalQty - flxGrid.TextMatrix(row, 5)
    totalberat = totalberat - flxGrid.TextMatrix(row, 6)
    lblQty = Format(totalQty, "#,##0.##")
    lblBerat = Format(totalberat, "#,##0.##")
    delete_rekap
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1

    flxGrid.Rows = flxGrid.Rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = flxGrid.cols - 1
    cmdClear_Click
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub cmdNext_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_terimabarang] from t_terimabarangh where [nomer_terimabarang]>'" & lblNoTrans & "' order by [nomer_terimabarang]", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdOk_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If cmbGudang.text = "" Then
        MsgBox "Gudang harus diisi"
        Exit Sub
    End If
    If txtKdBrg.text <> "" And LblNamaBarang1 <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text And flxGrid.TextMatrix(i, 5) = txtQty.text And flxGrid.TextMatrix(i, 6) = txtBerat.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang1
'        flxGrid.TextMatrix(row, 3) = lblNamaBarang2
        flxGrid.TextMatrix(row, 5) = txtQty
        flxGrid.TextMatrix(row, 6) = txtBerat
        flxGrid.TextMatrix(row, 7) = cmbGudang.text
        
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = flxGrid.cols - 1
'        TotalQty = TotalQty + flxGrid.TextMatrix(row, 5)
'        TotalBerat = TotalBerat + flxGrid.TextMatrix(row, 6)
'        lblQty = Format(TotalQty, "#,##0.##")
'        lblBerat = Format(TotalBerat, "#,##0.##")
        
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 9 Then
            flxGrid.TopRow = row - 8
        Else
            flxGrid.TopRow = 1
        End If
        add_rekap
        lblSatuan = ""
        
        
        txtQty.text = "1"
        txtBerat.text = "1"
        
        txtQty.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
End Sub
Private Sub add_rekap()
Dim i As Integer
Dim row As Integer
Dim totalQty As Double, totalberat As Double
    
    With flxGrid
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = txtKdBrg.text Then
                totalQty = totalQty + .TextMatrix(i, 5)
                totalberat = totalberat + .TextMatrix(i, 6)
            End If
        Next
    End With
    
    With flxgridRekap
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = txtKdBrg.text Then row = i
    Next
    If row = 0 Then
        row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.row, 0) = ""
    .row = row


    .TextMatrix(row, 1) = txtKdBrg.text
    .TextMatrix(row, 2) = LblNamaBarang1
'    If .TextMatrix(row, 3) = "" Then .TextMatrix(row, 3) = txtQty Else .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) + txtQty
'    If .TextMatrix(row, 4) = "" Then .TextMatrix(row, 4) = txtBerat Else .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) + txtBerat
    .TextMatrix(row, 3) = totalQty
    .TextMatrix(row, 4) = totalberat
    
    
    
    hitungtotal
    End With
End Sub
Private Sub delete_rekap()
Dim i As Integer
Dim row As Integer

    With flxgridRekap
    For i = 1 To .Rows - 1
        If .TextMatrix(i, 1) = flxGrid.TextMatrix(flxGrid.row, 1) Then row = i
    Next
    If row = 0 Then
        Exit Sub
    End If
    If IsNumeric(.TextMatrix(row, 3)) Then .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) - flxGrid.TextMatrix(flxGrid.row, 5)
    If IsNumeric(.TextMatrix(row, 4)) Then .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) - flxGrid.TextMatrix(flxGrid.row, 6)
    If .TextMatrix(row, 4) = 0 Or .TextMatrix(row, 3) = 0 Then
        For row = row To .Rows - 1
            If row = .Rows - 1 Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf .TextMatrix(row + 1, 1) = "" Then
                For col = 1 To .cols - 1
                    .TextMatrix(row, col) = ""
                Next
            ElseIf .TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To .cols - 1
                .TextMatrix(row, col) = .TextMatrix(row + 1, col)
                Next
            End If
        Next
        If .Rows > 1 Then .Rows = .Rows - 1
        lblItem = .Rows - 2
    End If
    
    
    End With
    hitungtotal
End Sub

Private Sub hitungtotal()
Dim i As Integer, totalQty As Double, totalberat As Double
Dim row As Integer
    
    If flxgridRekap.Rows > 1 Then
        With flxgridRekap
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) <> "" Then
                If .TextMatrix(i, 3) = "" Then .TextMatrix(i, 3) = 0
                If .TextMatrix(i, 4) = "" Then .TextMatrix(i, 4) = 0
                totalQty = totalQty + .TextMatrix(i, 3)
                totalberat = totalberat + .TextMatrix(i, 4)
            End If
        Next
        lblQty = Format(totalQty, "#,##0.##")
        lblBerat = Format(totalberat, "#,##0.##")
        txtBiayaAngkutBerat = Format(totalberat, "#,##0.##")
        lblItem = .Rows - 2
        End With
    End If
End Sub


Private Sub printBukti()
On Error GoTo err
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdopentreeview_Click()
On Error Resume Next
    If tvwMain.Visible = False Then
    tvwMain.Visible = True
    tvwMain.SetFocus
    Else
    tvwMain.Visible = False
    End If
End Sub

Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    simpan
    If posting_terimabarang(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdPrev_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [nomer_terimabarang] from t_terimabarangh where [nomer_terimabarang]<'" & lblNoTrans & "' order by [nomer_terimabarang] desc", conn
    If Not rs.EOF Then
        lblNoTrans = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cek_notrans
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Private Sub cmdSearchEkspedisi_Click()

    frmSearch.connstr = strcon
    
    frmSearch.query = SearchSupplier
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "txtKodeEkspedisi"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "supplier"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_ekspedisi"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Public Sub cek_ekspedisi()
Dim conn As New ADODB.Connection

    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_supplier " & _
            "where kode_supplier='" & txtKodeEkspedisi.text & "'", conn
    If Not rs.EOF Then
        lblNamaEkspedisi = rs!Nama_Supplier
'
'        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
'        Dim pengawas() As String
'        Dim kuli() As String
'        Dim notruk() As String
'        notruk = Split(rs!no_truk, ",")
'        listTruk.Clear
'        For A = 0 To UBound(notruk)
'            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
'            txtNoTruk_KeyDown 13, 0
'        Next
'        pengawas = Split(rs!pengawas, ",")
'        listPengawas.Clear
'        For A = 0 To UBound(pengawas)
'            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
'            txtPengawas_KeyDown 13, 0
'        Next
'        kuli = Split(rs!kuli, ",")
'        For J = 0 To listKuli.ListCount - 1
'            listKuli.Selected(J) = False
'        Next
'        For A = 0 To UBound(kuli)
'            For J = 0 To listKuli.ListCount - 1
'                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
'            Next
'        Next
    End If

    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub cmdSearchBrg_Click()
    
    If Trim(txtNoOrder.text) = "" Then
        MsgBox "Nomer Order harus diisi !", vbExclamation
        txtNoOrder.SetFocus
        Exit Sub
    End If
    
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmAddPenerimaanBarang"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchTerimaBarang
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "TerimaBarang"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchPengawas_Click()
    With frmSearch
        .connstr = strcon
        
        .query = searchKaryawan
        .nmform = "frmAddPenerimaanBarang"
        .nmctrl = "txtpengawas"
        .nmctrl2 = ""
        .keyIni = "ms_karyawan"
        .col = 0
        .Index = -1
        .proc = "cek_pengawas"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        .requery
        Set .frm = Me
        .Show vbModal
    End With
End Sub

Private Sub cmdSearchPO_Click()
    With frmSearch
    .connstr = strcon
    
    .query = SearchPO & " where status='0'"
    .nmform = "frmAddPenerimaanBarang"
    .nmctrl = "txtNoOrder"
    .nmctrl2 = ""
    .keyIni = "PO"
    .col = 0
    .Index = -1

    .proc = "cek_noPO"

    .loadgrid .query
    .cmbSort.ListIndex = 1
    .requery
    Set .frm = Me
    .Show vbModal
    End With
End Sub

Private Sub cmdSearchPosting_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchTerimaBarang & " where status_posting='0'"
    frmSearch.nmform = Me.Name
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "TerimaBarang"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal

End Sub

Private Sub cmdSearchSupplier_Click()
'    If User <> "sugik" Then
'        SearchSupplier = "select kode_supplier,nama_supplier,alamat from ms_supplier"
'    End If
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmAddPenerimaanBarang"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.col = 0
    frmSearch.Index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If validasi Then
    If simpan Then
        MsgBox "Data sudah tersimpan"
        cmdPosting.Visible = True
        'reset_form
        Call MySendKeys("{tab}")
    End If
    End If
End Sub
Private Function validasi() As Boolean
On Error GoTo err
    validasi = False
    If flxGrid.Rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang ingin dikirim terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Function
    End If
    If cmbGudang.text = "" Then
        MsgBox "Pilih gudang terlebih dahulu"
        cmbGudang.SetFocus
        Exit Function
    End If
    If Trim(txtNoOrder.text) = "" Then
        MsgBox "Nomer Order harus diisi !", vbExclamation
        txtNoOrder.SetFocus
        Exit Function
    End If
    If Not IsNumeric(txtBiayaAngkut.text) Then txtBiayaAngkut.text = "0"
    If CCur(txtBiayaAngkut.text) > 0 Then
        If txtKodeEkspedisi.text = "" Or lblNamaEkspedisi = "" Then
            MsgBox "Ekspedisi harus diisi"
            Exit Function
        End If
    End If
    validasi = True
err:
End Function

Private Function simpan() As Boolean

Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    
    id = lblNoTrans
    If lblNoTrans = "-" Then
        lblNoTrans = newid("t_terimabarangh", "nomer_terimabarang", DTPicker1, "TB")
    End If
    conn.ConnectionString = strcon
    conn.Open
    
    conn.BeginTrans
    i = 1
    
'    rs.Open "select d.nomer_terimabarang,kode_bahan,nomer_serial,qty,berat,kode_gudang from t_terimabarangd d inner join t_terimabarangh h on d.nomer_terimabarang=h.nomer_terimabarang where d.nomer_terimabarang='" & id & "'", conn, adOpenForwardOnly, adLockBatchOptimistic
'    While Not rs.EOF
'
'        updatestock id, rs!kode_bahan, rs!kode_gudang, rs!nomer_serial, rs!berat * -1, 0, conn
'        deletekartustock "Penerimaan Barang", id, conn
'        rs.MoveNext
'    Wend
'    rs.Close
    conn.Execute "delete from t_terimabarangh where nomer_terimabarang='" & id & "'"
    conn.Execute "delete from t_terimabarangd where nomer_terimabarang='" & id & "'"
    conn.Execute "delete from t_terimabarang_timbang where nomer_terimabarang='" & id & "'"
    conn.Execute "delete from t_terimabarang_angkut where nomer_terimabarang='" & id & "' and no_urut=1"
    'conn.Execute "delete from t_terimabarang_komposisi where nomer_terimabarang='" & id & "'"
    
    add_dataheader

    For row = 1 To flxGrid.Rows - 2
        add_datadetail (row)
    Next
    
    If chkAngkut.value = 1 Then add_ekspedisi
    Dim counter As Integer
    counter = 1

    rs.Open "select nomer_terimabarang,kode_bahan,sum(qty),sum(berat) from t_terimabarang_timbang where nomer_terimabarang='" & lblNoTrans & "' group by nomer_terimabarang,kode_bahan order by min(no_urut)", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "insert into t_terimabarangd (nomer_terimabarang,kode_bahan,nomer_serial,qty,berat,no_urut) values " & _
        "('" & lblNoTrans & "','" & rs!kode_bahan & "','t" & Format(DTPicker1, "dd/MM/yy") & "-" & Right(rs!nomer_terimabarang, 5) & "','" & rs(2) & "','" & rs(3) & "','" & counter & "')"
'        insertkartustock "Surat Jalan", lblNoTrans, DTPicker1, rs!kode_bahan, rs(3), Trim(Right(cmbGudang.text, 10)), rs!nomer_terimabarang & "-" & counter, 0, conn
'        updatestock lblNoTrans, rs!kode_bahan, Trim(Right(cmbGudang.text, 10)), rs!nomer_terimabarang & "-" & counter, rs(3), 0, conn, False
'        'conn.Execute "insert into t_terimabarang_komposisi (nomer_terimabarang,kode_bahan,urut,kode_bahanbaku,pct,no_urut) values ('" & lblNoTrans & "','" & rs!kode_bahan & "','" & counter & "','" & rs!kode_bahan & "' & " ','100','0')"
        counter = counter + 1
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    i = 0
    simpan = True
    conn.Close
    DropConnection
    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Function

Public Sub reset_form()
On Error Resume Next
    frAngkut.Visible = False
    lblNoTrans = "-"
    txtNoOrder.text = ""
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    txtNoSJ.text = ""
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    txtKodeEkspedisi = ""
    chkAngkut.value = False
    listPengawas.Clear
    
    listTruk.Clear
    txtSupir = ""
    status_posting = False
    totalQty = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblBerat = 0
    lblItem = 0
    DTPicker1 = Now
    txtNamaQCFisik = ""
    txtNamaQCLab = ""
    txtQCFisik = ""
    txtQCLab = ""
    txtQCKeterangan = ""
    cmdClear_Click
    txtNoAngkut.text = ""
    cmdSimpan.Enabled = True
    cmdPosting.Visible = False
    flxGrid.Rows = 1
    flxGrid.Rows = 2
    flxgridRekap.Rows = 1
    flxgridRekap.Rows = 2
    
    TabStrip1.Tabs(1).Selected = True
    
    txtNoOrder.SetFocus
    cmdNext.Enabled = False
    cmdPrev.Enabled = False
    txtBiayaAngkut = ""
    txtBiayaAngkutBerat = ""
    txtBiayaAngkutKG = ""
    txtKeteranganEkspedisi = ""
    OptCaraBayar(0).value = True
    txtBiayaLain = ""
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(21)
    ReDim nilai(21)
    table_name = "t_terimabarangh"
    fields(0) = "nomer_terimabarang"
    fields(1) = "tanggal_terimabarang"
    fields(2) = "kode_supplier"
    fields(3) = "nomer_PO"
    fields(4) = "keterangan"
    fields(5) = "userid"
    fields(6) = "status_posting"
    fields(7) = "nomer_suratjalan"
    fields(8) = "no_truk"
    fields(9) = "pengawas"
    fields(10) = "kuli"
    fields(11) = "supir"
    
    fields(12) = "fg_angkut"
    fields(13) = "jam_mulai"
    fields(14) = "jam_selesai"
    fields(15) = "nomer_angkut"
    
    fields(16) = "biaya_angkut_berat"
    fields(17) = "biaya_angkut_kg"
    fields(18) = "biaya_angkut"
    fields(19) = "biaya_lain"
    fields(20) = "cara_bayar"
    Dim pengawas, supir, kuli, notruk As String
    pengawas = ""
    For A = 0 To listPengawas.ListCount - 1
        pengawas = pengawas & "[" & Left(listPengawas.List(A), InStr(1, listPengawas.List(A), "-") - 1) & "],"
    Next
    If pengawas <> "" Then pengawas = Left(pengawas, Len(pengawas) - 1)
    kuli = ""
    For A = 0 To listKuli.ListCount - 1
        If listKuli.Selected(A) Then kuli = kuli & "[" & Left(listKuli.List(A), InStr(1, listKuli.List(A), "-") - 1) & "],"
    Next
    If kuli <> "" Then kuli = Left(kuli, Len(kuli) - 1)
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = txtKodeSupplier.text
    nilai(3) = txtNoOrder.text
    nilai(4) = txtKeterangan.text
    nilai(5) = User
    nilai(6) = 0
    nilai(7) = txtNoSJ
    nilai(8) = notruk
    nilai(9) = pengawas
    nilai(10) = kuli
    nilai(11) = supir
    
    nilai(12) = chkAngkut.value
    nilai(13) = Format(DTPicker2, "HH:mm:ss")
    nilai(14) = Format(DTPicker3, "HH:mm:ss")
    nilai(15) = ""
    If Trim(txtBiayaAngkutBerat.text) = "" Then txtBiayaAngkutBerat = 0
    If Trim(txtBiayaAngkutKG.text) = "" Then txtBiayaAngkutKG = 0
    If Trim(txtBiayaAngkut.text) = "" Then txtBiayaAngkut = 0
    If Trim(txtBiayaLain.text) = "" Then txtBiayaLain = 0
    nilai(16) = CDbl(txtBiayaAngkutBerat.text)
    nilai(17) = CDbl(txtBiayaAngkutKG.text)
    nilai(18) = CDbl(txtBiayaAngkut.text)
    nilai(19) = CDbl(txtBiayaLain.text)
    nilai(20) = IIf(OptCaraBayar(0).value = True, "T", "K")
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_terimabarang_timbang"
    fields(0) = "nomer_terimabarang"
    fields(1) = "kode_bahan"
    fields(2) = "qty"
    fields(3) = "berat"
    fields(4) = "NO_URUT"
    fields(5) = "kode_gudang"
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    
    nilai(2) = Replace(Format(flxGrid.TextMatrix(row, 5), "###0.##"), ",", ".")
    nilai(3) = Replace(Format(flxGrid.TextMatrix(row, 6), "###0.##"), ",", ".")
    nilai(4) = row
    nilai(5) = Trim(Right(flxGrid.TextMatrix(row, 7), 10))
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_ekspedisi()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_terimabarang_angkut"
    fields(0) = "nomer_terimabarang"
    fields(1) = "tanggal"
    fields(2) = "kode_ekspedisi"
    fields(3) = "fg_bayarangkut"
    fields(4) = "no_truk"
    fields(5) = "supir"
    fields(6) = "no_urut"
    fields(7) = "status"
    fields(8) = "keterangan"
    Dim supir, notruk As String
    notruk = ""
    For A = 0 To listTruk.ListCount - 1
        notruk = notruk & "[" & listTruk.List(A) & "],"
    Next
    If notruk <> "" Then notruk = Left(notruk, Len(notruk) - 1)
    supir = "[" & Replace(txtSupir, ",", "],[") & "]"
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd")
    
    nilai(2) = txtKodeEkspedisi
    nilai(3) = 0
    nilai(4) = notruk
    nilai(5) = supir
    nilai(6) = 1
    nilai(7) = 1
    nilai(8) = txtKeteranganEkspedisi
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select t.*,a.kode_ekspedisi,m.nama_ekspedisi from t_terimabarangh t " & _
            "left join t_angkut a on a.nomer_angkut=t.nomer_angkut " & _
            "left join ms_ekspedisi m on m.kode_ekspedisi=a.kode_ekspedisi " & _
            "where t.nomer_terimabarang='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
         
        DTPicker1 = rs("tanggal_terimabarang")
        DTPicker2 = rs("jam_mulai")
        DTPicker3 = rs("jam_selesai")
        If IsNull(rs("kode_ekspedisi")) = False Then
            txtKodeEkspedisi.text = rs("kode_ekspedisi")
            lblNamaEkspedisi = rs("nama_ekspedisi")
        End If
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        txtNoOrder.text = rs("nomer_PO")
        txtNoSJ = rs!nomer_suratjalan
'        txtNoAngkut.text = rs("nomer_angkut")
        chkAngkut.value = CInt(rs!fg_angkut) * -1
        
        txtSupir = Replace(Replace(rs!supir, "[", ""), "]", "")
        
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        Dim pengawas() As String
        Dim kuli() As String
        Dim notruk() As String
        notruk = Split(rs!no_truk, ",")
        listTruk.Clear
        For A = 0 To UBound(notruk)
            txtNoTruk = Replace(Replace(notruk(A), "[", ""), "]", "")
            txtNoTruk_KeyDown 13, 0
        Next
        pengawas = Split(rs!pengawas, ",")
        listPengawas.Clear
        For A = 0 To UBound(pengawas)
            txtPengawas = Replace(Replace(pengawas(A), "[", ""), "]", "")
            txtPengawas_KeyDown 13, 0
        Next
        kuli = Split(rs!kuli, ",")
        For J = 0 To listKuli.ListCount - 1
            listKuli.Selected(J) = False
        Next
        For A = 0 To UBound(kuli)
            For J = 0 To listKuli.ListCount - 1
                If Left(listKuli.List(J), InStr(1, listKuli.List(J), "-") - 1) = Replace(Replace(kuli(A), "[", ""), "]", "") Then listKuli.Selected(J) = True
            Next
'            txtKuli = Replace(Replace(kuli(A), "[", ""), "]", "")
'            txtKuli_KeyDown 13, 0
        Next
               
        txtBiayaAngkut = rs!biaya_angkut
        txtBiayaAngkutBerat = rs!biaya_angkut_berat
        txtBiayaAngkutKG = rs!biaya_angkut_kg
        txtBiayaLain = rs!biaya_lain
        lblTotalBiaya = rs!biaya_angkut + rs!biaya_lain
        If rs!cara_bayar = "T" Then
            OptCaraBayar(0).value = True
        ElseIf rs!cara_bayar = "K" Then
            OptCaraBayar(1).value = True
        End If
        OptCaraBayar(0).value = True
        totalQty = 0
        totalberat = 0
        total = 0
'        cmdPrint.Visible = True
    If rs!status_posting = "0" Then
            cmdPosting.Visible = True
        Else
            cmdSimpan.Enabled = False
        End If
        
        If rs.State Then rs.Close
        If chkAngkut.value = True Then
            rs.Open "select * from t_terimabarang_angkut where nomer_terimabarang='" & lblNoTrans & "' and status=1 ", conn
            If Not rs.EOF Then
                txtKodeEkspedisi = rs!kode_ekspedisi
                cek_ekspedisi
            End If
            If rs.State Then rs.Close
        End If
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1

        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat,kode_gudang from t_terimabarang_timbang d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_terimabarang='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 5) = rs(2)
                flxGrid.TextMatrix(row, 6) = rs(3)
                SetComboTextRight rs(4), cmbGudang
                flxGrid.TextMatrix(row, 7) = cmbGudang.text
                'flxGrid.TextMatrix(row, 8) = Format(rs(3) * rs(2), "#,##0")
                row = row + 1
                totalQty = totalQty + rs(2)
                totalberat = totalberat + rs(3)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        cmbGudang.ListIndex = 0
        With flxgridRekap
        .Rows = 1
        .Rows = 2
        row = 1
        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat from t_terimabarangd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_terimabarang='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                .TextMatrix(row, 1) = rs(0)
                .TextMatrix(row, 2) = rs(1)
                .TextMatrix(row, 3) = rs(2)
                .TextMatrix(row, 4) = rs(3)
                row = row + 1
                
                .Rows = .Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        End With
        lblQty = totalQty
        lblBerat = totalberat
        
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_noPO()
Dim conn As New ADODB.Connection

    cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_poh where nomer_po='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        txtKodeSupplier.text = rs("kode_supplier")
        cek_supplier
        
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        txtKdBrg.Clear
        rs.Open "select distinct(d.[kode_bahan]) from t_POd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_PO='" & txtNoOrder & "' order by d.kode_bahan", conn
        While Not rs.EOF
            txtKdBrg.AddItem rs(0)
            rs.MoveNext
        Wend
        rs.Close
        
'        flxGrid.Rows = 1
'        flxGrid.Rows = 2
'        row = 1
'        If rs.State Then rs.Close
'        rs.Open "select d.[kode_bahan],m.[nama_bahan], qty,d.berat from t_POD d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_PO='" & txtNoOrder.text & "' order by no_urut", conn
'        While Not rs.EOF
'                flxGrid.TextMatrix(row, 1) = rs(0)
'                flxGrid.TextMatrix(row, 2) = rs(1)
'                flxGrid.TextMatrix(row, 5) = rs(2)
'                flxGrid.TextMatrix(row, 6) = rs(3)
''                SetComboTextRight rs(4), cmbGudang
''                flxGrid.TextMatrix(row, 7) = cmbGudang.text
'                'flxGrid.TextMatrix(row, 8) = Format(rs(3) * rs(2), "#,##0")
'
'                add_rekap2 rs(0), rs(1), rs(2), rs(3)
'                row = row + 1
'                totalQty = totalQty + rs(2)
'                totalberat = totalberat + rs(3)
'                flxGrid.Rows = flxGrid.Rows + 1
''                total = total + (rs(3) * rs(4))
'                rs.MoveNext
'        Wend
'        rs.Close
'
'        HitungTotal
'
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub loaddetil()

        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang1 = flxGrid.TextMatrix(flxGrid.row, 2)
                lblNamaBarang2 = flxGrid.TextMatrix(flxGrid.row, 3)
                End If
            
            txtBerat.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 5)
            SetComboText flxGrid.TextMatrix(flxGrid.row, 7), cmbGudang
            txtBerat.SetFocus
        End If

End Sub

Private Sub Command1_Click()
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection

    conn.Open strcon
    rs.Open "select * from t_terimabarangh where status_posting=0 order by nomer_terimabarang", conn
    While Not rs.EOF
        posting_terimabarang rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    
End Sub

Private Sub DTPicker1_Change()
    Load_ListKuli
End Sub

Private Sub DTPicker1_Validate(Cancel As Boolean)
    Load_ListKuli
End Sub

Private Sub flxgrid_DblClick()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        loaddetil
        mode = 2
        mode2 = 2
        txtBerat.SetFocus
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        flxgrid_DblClick
    End If
End Sub

Private Sub FlxGrid_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = 13 Then txtBerat.SetFocus
End Sub

Private Sub txtBerat_GotFocus()
    txtBerat.SelStart = 0
    txtBerat.SelLength = Len(txtBerat.text)
End Sub

Private Sub txtBerat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then txtBerat = calc(txtBerat)
End Sub

Private Sub txtBerat_KeyPress(KeyAscii As Integer)
'    Angka KeyAscii
    
End Sub

Private Sub txtBerat_LostFocus()
    If Not IsNumeric(txtBerat.text) Then txtBerat.text = "0"
    
    
End Sub


Private Sub txtBiayaAngkutBerat_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtBiayaAngkutKG.SetFocus
End Sub

Private Sub txtBiayaAngkutBerat_Validate(Cancel As Boolean)
    HitungBiayaAngkut
End Sub

Private Sub HitungBiayaAngkut()
    If Trim(txtBiayaAngkutBerat.text) = "" Then txtBiayaAngkutBerat.text = 0
    If Trim(txtBiayaAngkutKG.text) = "" Then txtBiayaAngkutKG.text = 0
    If Trim(txtBiayaAngkut.text) = "" Then txtBiayaAngkut.text = 0
    If Trim(txtBiayaLain.text) = "" Then txtBiayaLain.text = 0
    txtBiayaAngkut.text = CDbl(txtBiayaAngkutBerat.text) * CDbl(txtBiayaAngkutKG.text)
    lblTotalBiaya.Caption = CDbl(txtBiayaAngkut.text) + CDbl(txtBiayaLain.text)
End Sub

Private Sub txtBiayaAngkutKG_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = 13 Then txtBiayaLain.SetFocus
End Sub

Private Sub txtBiayaAngkutKG_Validate(Cancel As Boolean)
    HitungBiayaAngkut
End Sub

Private Sub txtBiayaLain_Validate(Cancel As Boolean)
    HitungBiayaAngkut
End Sub

Private Sub txtBiayaLain_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        HitungBiayaAngkut
        OptCaraBayar(0).SetFocus
    End If
End Sub

Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
'    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
'    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub



Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        Call MySendKeys("{tab}")
        End If
    End If
End Sub

Private Sub Form_Load()
    load_combo
    DTPicker2 = Now
    DTPicker3 = Now
    loadgrup "0", tvwMain
    
    
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    Load_ListKuli
    total = 0
    seltab = 0
    
    With flxGrid
    .ColWidth(0) = 300
    .ColWidth(1) = 1400
    .ColWidth(2) = 3000
    .ColWidth(3) = 0
    .ColWidth(4) = 0
    .ColWidth(5) = 900
    .ColWidth(6) = 900
    .ColWidth(7) = 1500
    .ColWidth(8) = 0
    .ColAlignment(8) = 1
    
    .TextMatrix(0, 1) = "Kode Bahan"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .ColAlignment(3) = 1
    .TextMatrix(0, 2) = "Nama"
    .TextMatrix(0, 3) = "Serial"
    .TextMatrix(0, 4) = ""
    .TextMatrix(0, 5) = "Qty"
    .TextMatrix(0, 6) = "Berat"
    .TextMatrix(0, 7) = "Gudang"
    .TextMatrix(0, 8) = ""
    End With
    With flxgridRekap
    .ColWidth(0) = 300
    .ColWidth(1) = 1400
    .ColWidth(2) = 3000
    .ColWidth(3) = 900
    .ColWidth(4) = 900
        
    .TextMatrix(0, 1) = "Kode Bahan"
    .ColAlignment(2) = 1 'vbAlignLeft
    .ColAlignment(1) = 1
    .TextMatrix(0, 2) = "Nama"
    .TextMatrix(0, 3) = "Qty"
    .TextMatrix(0, 4) = "Berat"
    End With
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    cmbGudang.Clear
    rs.Open "select grup,urut from ms_grupgudang  order by urut", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0) & Space(50) & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    conn.Close
End Sub

Private Sub Load_ListKuli()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    listKuli.Clear
    rs.Open "select distinct a.kode_karyawan,m.nama_karyawan " & _
            "from absen_karyawan a " & _
            "left join ms_karyawan m on m.NIK=a.kode_karyawan " & _
            "where CONVERT(VARCHAR(10), a.tanggal, 111)='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
            "order by a.kode_karyawan", conn
    While Not rs.EOF
        listKuli.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan
        cek_kodebarang1 = True
    Else
        LblNamaBarang1 = ""

        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_supplier],kategori_harga from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
        lblKategori = rs(1)
        
    Else
        lblNamaSupplier = ""
        lblKategori = ""
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Public Sub cek_pengawas()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtPengawas & "'", conn
    If Not rs.EOF Then
        lblNamaPengawas = rs(1)
    Else
        lblNamaPengawas = ""
    End If
    rs.Close
    conn.Close

err:
End Sub
Public Sub cek_kuli()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_karyawan where nik='" & txtKuli & "'", conn
    If Not rs.EOF Then
        lblNamaKuli = rs(1)
    Else
        lblNamaKuli = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub

Private Sub hitung_ulang()
'On Error GoTo err
'    conn.Open strcon
'    With flxGrid
'    For i = 1 To .Rows - 2
'        rs.Open "select * from ms_bahan where kode_bahan='" & .TextMatrix(i, 1) & "'", conn
'        If Not rs.EOF Then
'            total = total - .TextMatrix(i, 8)
'            .TextMatrix(i, 7) = rs("harga_beli" & lblKategori)
'            .TextMatrix(i, 8) = .TextMatrix(i, 6) * .TextMatrix(i, 7)
'            total = total + .TextMatrix(i, 8)
'        End If
'        rs.Close
'    Next
'    End With
'    conn.Close
'    lblTotal = Format(total, "#,##0")
'    Exit Sub
'err:
'    MsgBox err.Description
'    If conn.State Then conn.Close
End Sub



Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyDelete Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub listTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listTruk.RemoveItem (listTruk.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub







Private Sub TabStrip1_Click()
    frDetail(seltab).Visible = False
    frDetail(TabStrip1.SelectedItem.Index - 1).Visible = True
    seltab = TabStrip1.SelectedItem.Index - 1
End Sub

Private Sub tvwMain_NodeClick(ByVal Node As MSComctlLib.Node)
    SetComboTextRight Right(Node.key, Len(Node.key) - 1), cmbGudang
    tvwMain.Visible = False
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next

    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If

        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeEkspedisi_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyF3 Then cmdSearchEkspedisi_Click
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub





Private Sub txtNoOrder_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPO_Click
End Sub

Private Sub txtNoOrder_LostFocus()
    cek_noPO
End Sub


Private Sub txtNoTruk_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        listTruk.AddItem txtNoTruk
        txtNoTruk = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPengawas_Click
    If KeyCode = 13 Then
        cek_pengawas
        listPengawas.AddItem txtPengawas & "-" & lblNamaPengawas
        txtPengawas = ""
        lblNamaPengawas = ""
        MySendKeys "+{tab}"
    End If
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    Angka KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub



Private Sub txtKodeEkspedisi_LostFocus()
'    If txtKodeEkspedisi.text <> "" And Not cek_ekspedisi Then
'        MsgBox "Kode ekspedisi tidak ditemukan"
'        txtKodeEkspedisi.SetFocus
'    End If
End Sub

Private Sub add_rekap2(kodebarang As String, namabarang As String, qty As Double, berat As Double)
Dim i As Integer
Dim row As Integer
Dim totalQty As Double, totalberat As Double
    
    With flxGrid
        For i = 1 To .Rows - 1
            If .TextMatrix(i, 1) = LCase(kodebarang) Then
                totalQty = totalQty + .TextMatrix(i, 5)
                totalberat = totalberat + .TextMatrix(i, 6)
            End If
        Next
    End With
'

    With flxgridRekap
    For i = 1 To .Rows - 1
        If LCase(.TextMatrix(i, 1)) = LCase(kodebarang) Then row = i
    Next
    If row = 0 Then
        row = .Rows - 1
        .Rows = .Rows + 1
    End If
    .TextMatrix(.row, 0) = ""
    .row = row


    .TextMatrix(row, 1) = kodebarang
    .TextMatrix(row, 2) = namabarang
'    If .TextMatrix(row, 3) = "" Then .TextMatrix(row, 3) = qty Else .TextMatrix(row, 3) = CDbl(.TextMatrix(row, 3)) + qty
'    If .TextMatrix(row, 4) = "" Then .TextMatrix(row, 4) = berat Else .TextMatrix(row, 4) = CDbl(.TextMatrix(row, 4)) + berat
'
    .TextMatrix(row, 3) = totalQty
    .TextMatrix(row, 4) = totalberat
'
    hitungtotal
    End With
End Sub

Public Sub reset_angkut()
Dim J As Integer
On Error Resume Next
    txtNoTruk.text = ""
    txtPengawas = ""
    txtKuli = ""
    txtKodeEkspedisi = ""
    listPengawas.Clear
    
    listTruk.Clear
    txtSupir = ""
    For J = 0 To listKuli.ListCount - 1
        listKuli.Selected(J) = False
    Next
End Sub


