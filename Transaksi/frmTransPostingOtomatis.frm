VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmTransPostingOtomatis 
   Caption         =   "Posting Otomatis"
   ClientHeight    =   2775
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5190
   FillColor       =   &H8000000B&
   ForeColor       =   &H8000000A&
   LinkTopic       =   "Form1"
   ScaleHeight     =   2775
   ScaleWidth      =   5190
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "INSERT HISTORY POSTING"
      Height          =   435
      Left            =   1275
      TabIndex        =   5
      Top             =   2115
      Visible         =   0   'False
      Width           =   2715
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   300
      Left            =   945
      TabIndex        =   4
      Top             =   1170
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Left            =   675
      TabIndex        =   3
      Top             =   210
      Visible         =   0   'False
      Width           =   2490
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&POSTING"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   930
      TabIndex        =   0
      Top             =   675
      Visible         =   0   'False
      Width           =   1965
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1080
      TabIndex        =   6
      Top             =   1620
      Visible         =   0   'False
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   89980931
      CurrentDate     =   38927
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   3060
      TabIndex        =   9
      Top             =   1620
      Visible         =   0   'False
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   89980931
      CurrentDate     =   38927
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "dari"
      Height          =   255
      Left            =   3795
      TabIndex        =   10
      Top             =   765
      Width           =   345
   End
   Begin VB.Label Label3 
      Caption         =   "s/d"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   2655
      TabIndex        =   8
      Top             =   1650
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.Label Label8 
      Caption         =   "Tanggal :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   7
      Top             =   1650
      Width           =   915
   End
   Begin VB.Label Label2 
      Caption         =   "Label1"
      Height          =   255
      Left            =   4320
      TabIndex        =   2
      Top             =   765
      Width           =   810
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Label1"
      Height          =   255
      Left            =   2970
      TabIndex        =   1
      Top             =   765
      Width           =   720
   End
End
Attribute VB_Name = "frmTransPostingOtomatis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim rs As New ADODB.Recordset
Dim rs1 As New ADODB.Recordset
Dim conn1 As New ADODB.Connection

Dim no_transaksi As String
Dim nomer As String
On Error GoTo err
conn1.Open strcon
rs.Open "select count(*) from history_posting_tmp  where status='0';select * from history_posting_tmp where status='0' order by tanggal asc", conn1
Label2 = rs(0)
Set rs = rs.NextRecordset
Dim counter As Long
Dim break As Boolean
counter = 0

While Not rs.EOF
    Label1 = counter
    no_transaksi = rs(0)
    
If IsNumeric(Mid(no_transaksi, 3, 1)) Then
    Select Case Left(no_transaksi, 2)
        Case "PH"
            PostingJurnal_BayarHutang no_transaksi
        Case "PP"
            PostingJurnal_BayarPiutang no_transaksi
        Case "BK", "BM", "KM", "KK"
            PostingJurnal_KasBank no_transaksi
        Case "PB"
            PostingJurnal_Pembelian no_transaksi
        Case "PJ"
            PostingJurnal_Penjualan no_transaksi
        Case "PO", "RJ"
            postingjurnal_returjual no_transaksi
'        Case "KB"
'            PostingJurnal_SuratJalan no_transaksi
    End Select

End If
    conn1.Execute "update history_posting_tmp set status='1' where nomer_transaksi='" & no_transaksi & "'"
    rs.MoveNext
    counter = counter + 1
    DoEvents
Wend
conn1.Close
Set conn1 = Nothing
MsgBox "posting selesai"

Exit Sub
err:
MsgBox err.Description
MsgBox no_transaksi

End Sub

Private Sub Command2_Click()
'    PostingJurnal_Penjualan Text1.text
'    PostingJurnal_SuratJalan Text1.text
'    PostingJurnal_Pembelian Text1.text
'    PostingJurnal_BayarHutang Text1.text
PostingJurnal_BayarPiutang Text1.text
'PostingJurnal_KasBank Text1.text
    MsgBox "Proses Selesai !"
End Sub


Private Sub Command3_Click()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim i As Long, Saldo As Double
Dim Acc As String
Dim Baru As Boolean

conn.Open strcon

conn.BeginTrans

conn.Execute "delete from history_posting_tmp " & _
             "where  CONVERT(VARCHAR(10), tanggal, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "'  " & _
             "and CONVERT(VARCHAR(10), tanggal, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "

conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_jual,tanggal_jual,'0' from t_jualh " & _
             "where CONVERT(VARCHAR(10), tanggal_jual, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_jual, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "
             
conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_suratjalan,tanggal_suratjalan,'0' from t_suratjalanh " & _
             "where CONVERT(VARCHAR(10), tanggal_suratjalan, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_suratjalan, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "

conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_beli,tanggal_beli,'0' from t_belih " & _
             "where CONVERT(VARCHAR(10), tanggal_beli, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_beli, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "

conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_returbeli,tanggal_returbeli,'0' from t_returbelih " & _
             "where CONVERT(VARCHAR(10), tanggal_returbeli, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_returbeli, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "
             
conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_returjual,tanggal_returjual,'0' from t_returjualh " & _
             "where CONVERT(VARCHAR(10), tanggal_returjual, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_returjual, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "

conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select no_transaksi,tanggal,'0' from t_kasbankh " & _
             "where CONVERT(VARCHAR(10), tanggal, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "

conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_bayarpiutang,tanggal_bayarpiutang,'0' from t_bayarpiutangh " & _
             "where CONVERT(VARCHAR(10), tanggal_bayarpiutang, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_bayarpiutang, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "
             
conn.Execute "insert into history_posting_tmp (nomer_transaksi,tanggal,status) " & _
             "select nomer_bayarhutang,tanggal_bayarhutang,'0' from t_bayarhutangh " & _
             "where CONVERT(VARCHAR(10), tanggal_bayarhutang, 111)>='" & Format(DTPicker1.value, "yyyy/MM/dd") & "' " & _
             "and CONVERT(VARCHAR(10), tanggal_bayarhutang, 111)<='" & Format(DTPicker2.value, "yyyy/MM/dd") & "'  "


conn.CommitTrans

conn.Close
MsgBox "Proses Selesai !"
End Sub

Private Sub Form_Load()
    DTPicker1.value = Date
    DTPicker2.value = Date
End Sub
