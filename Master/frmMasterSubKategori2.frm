VERSION 5.00
Begin VB.Form frmMasterSubKategori1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sub Kategori 1"
   ClientHeight    =   2745
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6255
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2745
   ScaleWidth      =   6255
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   1710
      TabIndex        =   13
      Text            =   "cmbField"
      Top             =   585
      Width           =   3450
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   1710
      TabIndex        =   9
      Text            =   "cmbField"
      Top             =   180
      Width           =   3450
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3593
      Picture         =   "frmMasterSubKategori2.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1875
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1973
      Picture         =   "frmMasterSubKategori2.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1875
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4725
      Picture         =   "frmMasterSubKategori2.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   990
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   990
      Width           =   2850
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   488
      Picture         =   "frmMasterSubKategori2.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1875
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   615
      Width           =   810
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   14
      Top             =   645
      Width           =   150
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   11
      Top             =   240
      Width           =   150
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   210
      Width           =   810
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   8
      Top             =   1035
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   1515
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   5
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   4
      Top             =   1020
      Width           =   1320
   End
   Begin VB.Label lblID 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1755
      TabIndex        =   12
      Top             =   990
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterSubKategori1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbField_Click(Index As Integer)
    Dim rs As New ADODB.Recordset
    Dim conn As New ADODB.Connection
    If Index = 0 Then
        conn.Open strcon
        cmbField(1).Clear
        rs.Open "select * from var_subkategori where kategori='" & cmbField(0).text & "'", conn
        While Not rs.EOF
            cmbField(1).AddItem rs(1)
            rs.MoveNext
        Wend
        rs.Close
        conn.Close
    End If
End Sub


Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    
    conn.ConnectionString = strcon
    conn.Open
  
    
    conn.BeginTrans
    conn.Execute "delete from var_subkategori where kategori='" & txtField(0).text & "'"
    conn.CommitTrans
    
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.connstr = strcon
    frmSearch.query = "select subkategori1,subkategori,kategori from var_subkategori1 where kategori='" & cmbField(0).text & "'" ' order by kategori"
    frmSearch.nmform = "frmMasterkategori"
    frmSearch.nmctrl = "txtfield"
    frmSearch.nmctrl2 = "cmbfield"
    frmSearch.keyIni = "subkategori"

    frmSearch.col = 0
    frmSearch.col2 = 1
    frmSearch.Index = 0
    frmSearch.Index2 = 1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
   
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        txtField(0).SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    
    
    conn.BeginTrans
    conn.Execute "delete from var_subkategori1 where  kategori='" & cmbField(0).text & "' and subkategori='" & cmbField(1).text & "' and subkategori1='" & txtField(0).text & "'"
    conn.Execute "insert into var_subkategori1 values ('" & cmbField(0).text & "','" & cmbField(1).text & "','" & txtField(0).text & "')"
    conn.CommitTrans
    
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
    txtField(0).text = ""
    txtField(0).SetFocus
End Sub

Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    cmbField(0).Clear
    rs.Open "select kategori from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(0).AddItem rs(0)
        rs.MoveNext
    Wend
    conn.Close
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    load_combo
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    
End Sub



