Attribute VB_Name = "moduleTjandra"
Dim AccSelisihHPP As String
Public Const SearchMasterProses As String = "Select kode_proses, nama_proses from ms_prosesh"
Public Const SearchPotong As String = "Select nomer_potong,kode_bahan,nomer_serial,qty,tanggal,status_posting from t_potongh"
Public Const SearchPotongInput = "select kode_bahan,nama_bahan from ms_bahan where kategori like '%ROLL%' or kategori like '%SHEET%'"
Public Const searchProsesStock As String = "Select nomer_serial,kode_gudang,stock from stock"
Public Const SearchHasilPotong As String = "Select Nomer_HasilPotong, Tanggal, Nomer_Potong,status_posting"
Public Const SearchHasilCetak As String = "Select Nomer_HasilCetak, Tanggal, Nomer_Cetak from t_hasilCetakh"
Public Const SearchHasilCoating As String = "Select Nomer_HasilCoating, Tanggal,status_posting from t_hasilCoatingh"
Public Const SearchHasilCoatingBungkus As String = "Select Nomer_HasilCoatingBungkus, Tanggal,status_posting from t_hasilCoatingBungkush"
Public Const SearchPlong As String = "SELECT nomer_plong, tanggal, kode_bahan, nomer_serial, qty, kode_operator, kode_mesin, kode_gudang, status_posting, status_finish from t_plongh"
Public Const SearchCetak As String = "SELECT nomer_cetak, kode_proses, kode_bahan, nomer_serial, qty, kode_operator, kode_mesin, status_posting, status_finish from t_cetakh"

'Public Function GetHPPTinta(ByVal kdbrg As String, cn As ADODB.Connection) As Currency
'Dim rs As New ADODB.Recordset
'rs.Open "select hpp from hpp_tinta where [kode_tinta]='" & kdbrg & "' ", cn
'If Not rs.EOF And IsNull(rs!hpp) = False Then
'    GetHPPTinta = rs(0)
'Else
'    GetHPPTinta = 0
'End If
'rs.Close
'End Function


'Public Function getHppKertas(ByVal kdbrg As String, cn As ADODB.Connection) As Currency
'Dim rs As New ADODB.Recordset
'rs.Open "select hpp from stock where [kode_bahan]='" & kdbrg & "' ", cn
'If Not rs.EOF And IsNull(rs!hpp) = False Then
'    getHppKertas = rs(0)
'Else
'    getHppKertas = 0
'End If
'rs.Close
'End Function

Public Function GetHPPKertas2(ByVal kdBrg As String, ByVal noseri As String, ByVal kode_gudang As String, cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select max(hpp) as hpp from stock where kode_bahan='" & kdBrg & "' and [nomer_serial]='" & noseri & "'", cn
If Not rs.EOF And IsNull(rs!hpp) = False Then
    GetHPPKertas2 = rs(0)
Else
    GetHPPKertas2 = 0
End If
rs.Close
Set rs = Nothing
End Function

Private Function GetStockKertas(ByVal kdBrg As String, ByVal noseri As String, ByVal kode_gudang As String, cn As ADODB.Connection) As Currency
Dim rs As New ADODB.Recordset

rs.Open "select Stock as stock from stock where kode_bahan='" & kdBrg & "' and nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "'", cn
If Not rs.EOF And IsNull(rs!stock) = False Then
    GetStockKertas = rs(0)
Else
    GetStockKertas = 0
End If
rs.Close
Set rs = Nothing
End Function


Public Function getNoHistory(ByVal kdBrg As String, ByVal kdgdg As String, ByVal NoSerial As String, cn As ADODB.Connection) As String
Dim rs As New ADODB.Recordset
rs.Open "select no_history from stock where kode_bahan='" & kdBrg & "' and kode_gudang='" & kdgdg & "' and nomer_serial='" & NoSerial & "'", cn
If Not rs.EOF Then
    getNoHistory = rs(0)
Else
    getNoHistory = ""
End If
rs.Close
Set rs = Nothing
End Function

Public Function getSerialHistory(ByVal kdBrg As String, ByVal kdgdg As String, ByVal NoSerial As String, cn As ADODB.Connection) As String
Dim rs As New ADODB.Recordset
rs.Open "select serial_history from stock where kode_bahan='" & kdBrg & "' and kode_gudang='" & kdgdg & "' and nomer_serial='" & NoSerial & "'", cn
If Not rs.EOF Then
    getSerialHistory = rs(0)
Else
    getSerialHistory = ""
End If
rs.Close
Set rs = Nothing
End Function

Public Function getKodeHistory(ByVal kdBrg As String, ByVal kdgdg As String, ByVal NoSerial As String, cn As ADODB.Connection) As String
Dim rs As New ADODB.Recordset
rs.Open "select kode_history from stock where kode_bahan='" & kdBrg & "' and kode_gudang='" & kdgdg & "' and nomer_serial='" & NoSerial & "'", cn
If Not rs.EOF Then
    getKodeHistory = rs(0)
Else
    getKodeHistory = ""
End If
rs.Close
Set rs = Nothing
End Function

Public Function getStockToko(ByVal kdBrg As String, ByVal kdtoko As String, ByVal NoSerial As String, cn As ADODB.Connection) As Double
Dim rs As New ADODB.Recordset
rs.Open "select stock from stock_toko where kode_bahan='" & kdBrg & "' and kode_toko='" & kdtoko & "' and nomer_serial='" & NoSerial & "'", cn
If Not rs.EOF Then
    getStockKertasToko = rs(0)
Else
    getStockKertasToko = 0
End If
rs.Close
Set rs = Nothing
End Function

Public Function getStockKertas2(ByVal kdBrg As String, ByVal kdgdg As String, ByVal NoSerial As String, cn As ADODB.Connection) As Double
Dim rs As New ADODB.Recordset
rs.Open "select stock from stock where kode_bahan='" & kdBrg & "' and kode_gudang='" & kdgdg & "' and nomer_serial='" & NoSerial & "'", cn
If Not rs.EOF Then
    getStockKertas2 = rs(0)
Else
    getStockKertas2 = 0
End If
rs.Close
Set rs = Nothing
End Function
Public Function getQty(ByVal kdBrg As String, ByVal kdgdg As String, ByVal NoSerial As String, cn As ADODB.Connection) As Double
Dim rs As New ADODB.Recordset
rs.Open "select qty from stock where kode_bahan='" & kdBrg & "' and kode_gudang='" & kdgdg & "' and nomer_serial='" & NoSerial & "'", cn
If Not rs.EOF Then
    getQty = rs(0)
Else
    getQty = 0
End If
rs.Close
Set rs = Nothing
End Function
Public Function GetStockKertasGlobal(ByVal kdBrg As String, cn As ADODB.Connection) As Double
Dim rs As New ADODB.Recordset
rs.Open "select sum(stock) as Total from stock where kode_bahan='" & kdBrg & "' ", cn
If Not rs.EOF And IsNull(rs!total) = False Then
    GetStockKertasGlobal = rs(0)
Else
    GetStockKertasGlobal = 0
End If
rs.Close
Set rs = Nothing
End Function


Public Function getAccBahan(ByVal KodeBahan As String, cn As ADODB.Connection) As String
Dim rs As New ADODB.Recordset
rs.Open "Select acc_persediaan from setting_accbahan where kode_bahan='" & KodeBahan & "'", cn
If Not rs.EOF Then
    getAccBahan = rs(0)
End If
rs.Close
Set rs = Nothing
End Function

Private Function getAccHPP(ByVal KodeBahan As String, cn As ADODB.Connection) As String
Dim rs As New ADODB.Recordset
rs.Open "Select acc_hpp from setting_accbahan where kode_bahan='" & KodeBahan & "'", cn
If Not rs.EOF Then
    getAccHPP = rs(0)
End If
rs.Close
Set rs = Nothing
End Function

Public Function getAccTinta(ByVal KodeTinta As String, ByVal KodeGudang As String, cn As ADODB.Connection) As String
Dim rs As New ADODB.Recordset
rs.Open "Select kode_acc from setting_accTinta where kode_tinta='" & KodeTinta & "' and kode_gudang='" & KodeGudang & "'", cn
If Not rs.EOF Then
    getAccTinta = rs(0)
End If
rs.Close
Set rs = Nothing
End Function

Public Function getAccBank(ByVal Kode_Bank As String) As String
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open strcon
rs.Open "Select kode_acc from ms_bank where kode_bank='" & Kode_Bank & "'", conn
If Not rs.EOF Then
    getAccBank = rs(0)
End If
rs.Close
conn.Close
Set rs = Nothing
Set conn = Nothing
End Function


Sub Posting_Coating(ByRef NoTransaksi As String, ByRef conn As ADODB.Connection)
'
End Sub
'-----------------------tambah

Private Sub updatestock(ByRef NoTransaksi As String, kode_bahan As String, kode_gudang As String, noseri As String, qty As Double, harga As Double, cn As ADODB.Connection, Optional beratasal As Currency, Optional KodeAsal As String)
    If IsNull(kode_asal) Then kode_asal = ""
    If IsNull(berat_asal) Then berat_asal = "0"
    cn.Execute "if exists(select * from stock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "') " & _
        "update stock set stock=stock+'" & Replace(Format(qty, "###0.##"), ",", ".") & "' where kode_bahan='" & kode_bahan & "' and nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "' else " & _
        "insert into stock (kode_bahan,nomer_serial,stock,hpp,kode_gudang,kode_asal,berat_asal) values ('" & kode_bahan & "','" & noseri & "','" & Replace(Format(qty, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & kode_gudang & "','" & Replace(Format(KodeAsal, "###0.##"), ",", ".") & "','" & Replace(Format(beratasal, "###0.##"), ",", ".") & "')"
    'cn.Execute "if (select stock from stock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "')=0 delete from stock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "'"
End Sub

Sub Posting_KasBank(ByRef NoTransaksi As String, ByRef conn As ADODB.Connection)
Dim i As Integer, Y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Currency, NilaiJurnalH As Currency, NilaiJurnalD2 As Currency
Dim keterangan As String, KetBG As String
Dim NoJurnal As String
Dim ket As String

'On Error GoTo err

    conn.Open strcon
    
    conn.BeginTrans

    Tipe = Left(NoTransaksi, 2)

    rs.Open "select tanggal,acc_kasbank,total,keterangan,ket_BG from t_kasbankh where no_transaksi='" & NoTransaksi & "'", conn
    If rs.EOF Then GoTo err
    tanggal = rs("tanggal")
    NilaiJurnalH = rs("total")
    ket = rs!keterangan
    KetBG = rs!ket_bg
    rs.Close
    
    Select Case Tipe
        Case "KM"
            keterangan = "Kas Masuk"
        Case "KK"
            keterangan = "Kas Keluar"
        Case "BM"
            keterangan = "Bank Masuk"
        Case "BK"
            keterangan = "Bank Keluar"
    End Select
    
    NoJurnal = Nomor_Jurnal(tanggal)
    
'    If Tipe = "KM" Or Tipe = "BM" Then
'        JurnalD NoTransaksi, NoJurnal, AccKasBank, "d", NilaiJurnalH, ket, KetBG, , , y + 1
'    End If
    
    rs.Open "select d.kode_acc,d.nilai,d.keterangan,d.No_Pengeluaran from t_kasbankd d " & _
            "where d.no_transaksi='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        Y = Y + 1
        NilaiJurnalD = rs("nilai")
                
'        If Tipe = "KM" Or Tipe = "BM" Then
'            JurnalD NoTransaksi, NoJurnal, rs("kode_acc"), "k", NilaiJurnalD, rs!keterangan, , , , y
'        Else
'            JurnalD NoTransaksi, NoJurnal, rs("kode_acc"), "d", NilaiJurnalD, rs!keterangan, , , , y
'        End If
        
        
        If Tipe = "KM" Or Tipe = "BM" Then
            JurnalD NoTransaksi, NoJurnal, rs("kode_acc"), "k", NilaiJurnalD, rs!keterangan, , , , Y
            Y = Y + 1
        Else
            JurnalD NoTransaksi, NoJurnal, rs("kode_acc"), "d", NilaiJurnalD, rs!keterangan, , , , Y
            Y = Y + 1
        End If
        
        conn.Execute "Update t_suratJalanH set status_kas='1' where Nomer_SuratJalan='" & rs("No_Pengeluaran") & "'"
        
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select * from t_kasbankd2 where no_transaksi = '" & NoTransaksi & "'", conn, adOpenKeyset
    While Not rs.EOF
        NilaiJurnalD2 = rs!nilai
        Y = 1
        If Tipe = "KM" Or Tipe = "BM" Then
            JurnalD NoTransaksi, NoJurnal, rs("acc_kasbank"), "d", NilaiJurnalD2, , , , , Y
            Y = Y + 1
        Else
            JurnalD NoTransaksi, NoJurnal, rs("acc_kasbank"), "k", NilaiJurnalD2, , , , , Y
            Y = Y + 1
        End If
        rs.MoveNext
    Wend
'    If Tipe = "KK" Or Tipe = "BK" Then
'        JurnalD NoTransaksi, NoJurnal, AccKasBank, "k", NilaiJurnalH, ket, , , , y + 1
'    End If
    
    JurnalH NoTransaksi, NoJurnal, tanggal, keterangan
    
    conn.Execute "update t_kasbankh set status_posting='1' where no_transaksi='" & NoTransaksi & "'"

    conn.Execute "update list_BG set status_cair='1' where nomer_bg='" & KetBG & "' and nominal=" & NilaiJurnalH & ""
    
    conn.CommitTrans
    
    DropConnection

'    MsgBox "Data sudah diposting"

    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub

Sub Posting_Manol(ByRef NoTransaksi As String, ByRef conn As ADODB.Connection)
Dim i As Integer, Y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Currency, NilaiJurnalH As Currency
Dim keterangan As String, KetBG As String
Dim NoJurnal As String
Dim AccKas As String, AccBiaya As String
Dim ket As String

On Error GoTo err

    conn.Open strcon
    
    conn.BeginTrans

    Tipe = Left(NoTransaksi, 2)
    AccKas = getAcc("kas kecil") '"110.010.0102" 'acclama = 110.010.0200
    AccBiaya = getAcc("biaya manol") '"610.000.0102" 'acclama = 520.100.0100

    rs.Open "select * from t_bayarkulih where nomer_bayarkuli='" & NoTransaksi & "'", conn
'    If rs.EOF Then GoTo err
    tanggal = rs("tanggal")
    NilaiJurnalH = rs("total")
    ket = "Bayar Kuli"
    rs.Close
    
    NoJurnal = Nomor_Jurnal(tanggal)
            
    JurnalD NoTransaksi, NoJurnal, AccBiaya, "d", NilaiJurnalH, ket, , , , Y
    
    JurnalD NoTransaksi, NoJurnal, AccKas, "k", NilaiJurnalH, ket, , , , Y + 1
    
    JurnalH NoTransaksi, NoJurnal, tanggal, ket
    
    conn.Execute "update t_bayarkulih set status_posting='1' where nomer_bayarkuli='" & NoTransaksi & "'"

    conn.CommitTrans
    
    DropConnection

    MsgBox "Data sudah diposting"

    Exit Sub
err:
    conn.RollbackTrans
    MsgBox err.Description
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub



Public Sub Posting_BayarHutang(ByRef NoTransaksi As String, ByRef conn As ADODB.Connection)
Dim i As Integer, Y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String
Dim NoJurnal As String
Dim CaraBayar As String
Dim JumlahBayar As Double, deposit As Double
Dim TotalRetur As Double
Dim TotalBayar As Double
Dim KodeBank As String
Dim selisih As Double

Dim Kodesupplier As String
Dim Acc_HutangGiro As String, NilaiBG As Double
Dim NoBG As String, TanggalCair As Date

'On Error GoTo err

    conn.Open strcon
    conn.BeginTrans
    
        
    rs.Open "select tanggal_bayarhutang as tanggal,kode_supplier,jumlah_tunai, deposit,selisih from t_bayarhutangh where nomer_bayarhutang='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    
    deposit = rs!deposit
    Kodesupplier = rs("kode_supplier")
    rs.Close

    
    rs.Open "select sum(jumlah) as TotalRetur from t_bayarhutang_retur where nomer_bayarhutang='" & NoTransaksi & "'", conn
    If Not rs.EOF And IsNull(rs("TotalRetur")) = False Then
        TotalRetur = rs("TotalRetur")
    End If
    rs.Close
    Y = 1
        
    NilaiTransfer = 0
    Dim rs1 As New ADODB.Recordset
    NilaiBG = 0
    
    rs.Open "select * from t_bayarhutang_cek where nomer_bayarhutang='" & NoTransaksi & "'", conn
    While Not rs.EOF
        conn.Execute "insert into list_bg(nomer_transaksi,tanggal,nomer_bg,tanggal_cair, " & _
                     "jenis,kode_supplier,keterangan_bank,nominal) " & _
                    "values ('" & NoTransaksi & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "','" & rs!no_cek & "','" & Format(rs!tanggal_cair, "yyyy/MM/dd HH:mm:ss") & "', " & _
                    "'h','" & Kodesupplier & "','" & rs!Kode_Bank & "'," & Replace(rs!nilai_cek, ",", ".") & " )"
        NilaiBG = NilaiBG + rs("nilai_cek")
        conn.Execute "update t_belibukud set status=0 where jenis='" & rs!Jenis & "' and nomer='" & rs!no_cek & "'"
        rs.MoveNext
    Wend
    rs.Close
    
    
'
    TotalBayar = 0
    rs.Open "select nomer_beli,jumlah_bayar from t_bayarhutang_beli " & _
            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_hutang set total_bayar=total_bayar + " & rs("jumlah_bayar") & " " & _
                     "where nomer_transaksi='" & rs("nomer_beli") & "'"
        
        TotalBayar = TotalBayar + rs("jumlah_bayar")
        
        rs.MoveNext
    Wend
    rs.Close
    
    insertkartuHutang "Bayar Hutang", NoTransaksi, tanggal, Kodesupplier, CCur(TotalBayar), conn
    
    rs.Open "select nomer_retur,jumlah from t_bayarhutang_retur " & _
            "where nomer_bayarhutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_piutang_retur set total_bayar=total_bayar + " & rs("jumlah") & " " & _
                     "where nomer_transaksi='" & rs("nomer_retur") & "'"
        
        rs.MoveNext
    Wend
    rs.Close
    
    insertkartuHutang "Bayar Hutang (Retur Beli)", NoTransaksi, tanggal, Kodesupplier, CCur(TotalRetur), conn
    
    conn.Execute "update ms_supplier set deposit=deposit+" & deposit & " where kode_supplier='" & Kodesupplier & "'"
    conn.Execute "update t_bayarhutangh set status_posting='1' where nomer_bayarhutang='" & NoTransaksi & "'"
    
    conn.CommitTrans
    
    DropConnection

    'MsgBox "Data sudah diposting"

    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub

Public Sub Posting_BayarPiutang(ByRef NoTransaksi As String, ByRef conn As ADODB.Connection)
Dim i As Integer, Y As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim keterangan As String
Dim NoJurnal As String
Dim CaraBayar As String
Dim JumlahBayar As Double, NilaiTransfer As Double, deposit As Double
Dim TotalRetur As Double
Dim KodeBank As String
Dim selisih As Double
Dim TotalBayar As Double
Dim KodeCustomer As String
Dim NilaiBG As Double
Dim NoBG As String, TanggalCair As Date

On Error GoTo err

    conn.Open strcon
    conn.BeginTrans
    
    rs.Open "select tanggal_bayarPiutang as tanggal,kode_Customer,jumlah_tunai,selisih, " & _
            "deposit from t_bayarPiutangh where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")

    JumlahBayar = rs("jumlah_tunai")
    selisih = rs!selisih
    deposit = rs!deposit
    

    
    KodeCustomer = rs("kode_Customer")
    rs.Close

    rs.Open "select sum(jumlah) as TotalRetur from t_bayarPiutang_retur where nomer_bayarPiutang='" & NoTransaksi & "'", conn
    If Not rs.EOF And IsNull(rs("TotalRetur")) = False Then
        TotalRetur = rs("TotalRetur")
    End If
    rs.Close
    
    NilaiTransfer = 0
    Dim rs1 As New ADODB.Recordset
    NilaiBG = 0
    rs.Open "select * from t_bayarpiutang_cek where nomer_bayarpiutang='" & NoTransaksi & "'", conn
    While Not rs.EOF
        conn.Execute "insert into list_bg(nomer_transaksi,tanggal,nomer_bg,tanggal_cair, " & _
                     "jenis,kode_customer,keterangan_bank,nominal) " & _
                    "values ('" & NoTransaksi & "','" & tanggal & "','" & rs!no_cek & "','" & rs!tanggal_cair & "', " & _
                    "'p','" & KodeCustomer & "','" & rs!Kode_Bank & "'," & Replace(rs!nilai_cek, ",", ".") & " )"
        NilaiBG = NilaiBG + rs("nilai_cek")
        rs.MoveNext
    Wend
    rs.Close
    TotalBayar = 0
    rs.Open "select nomer_Jual,jumlah_bayar from t_bayarPiutang_Jual " & _
            "where nomer_bayarPiutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_piutang set total_bayar=total_bayar + " & rs("jumlah_bayar") & " " & _
                     "where nomer_transaksi='" & rs("nomer_Jual") & "'"
        TotalBayar = TotalBayar + rs("Jumlah_Bayar")
        rs.MoveNext
    Wend
    rs.Close
    
    insertkartuPiutang "Bayar Piutang", NoTransaksi, tanggal, KodeCustomer, CCur(TotalBayar) * -1, conn
    
    rs.Open "select nomer_retur,jumlah from t_bayarPiutang_retur " & _
            "where nomer_bayarPiutang='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
        conn.Execute "update list_piutang set total_bayar=total_bayar + " & rs("jumlah") & " " & _
                     "where nomer_transaksi='" & rs("nomer_retur") & "'"
        
        rs.MoveNext
    Wend
    rs.Close
    
    insertkartuPiutang "Bayar Piutang (Retur Jual)", NoTransaksi, tanggal, KodeCustomer, CCur(TotalBayar) * -1, conn
    
    conn.Execute "update ms_Customer set deposit=deposit+" & deposit & " where kode_Customer='" & KodeCustomer & "'"

    conn.Execute "update t_bayarPiutangh set status_posting='1' where nomer_bayarPiutang='" & NoTransaksi & "'"

    conn.CommitTrans
    
    DropConnection

    'MsgBox "Data sudah diposting"

    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub

Public Sub JurnalH(ByRef NoTransaksi As String, NoJurnal As String, tanggal As Date, keterangan As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(6)
    ReDim nilai(6)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
    fields(4) = "tipe"
    fields(5) = "userid"

    nilai(0) = NoTransaksi
    nilai(1) = NoJurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
    nilai(4) = Left(NoTransaksi, 2)
    nilai(5) = User

    tambah_data table_name, fields, nilai

End Sub

Public Sub JurnalD(ByRef NoTransaksi As String, NoJurnal As String, kodeAcc As String, Posisi As String, nilaiJurnal As Currency, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String, Optional KeteranganNilai As String, Optional NomorUrut As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(9)
    ReDim nilai(9)

    
    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    If UCase(Posisi) = "D" Then
        fields(2) = "debet"
    Else
        fields(2) = "kredit"
    End If
    fields(3) = "no_jurnal"
    fields(4) = "keterangan"
    fields(5) = "kode1"
    fields(6) = "kode2"
    fields(7) = "nilai"
    fields(8) = "no_urut"
    

    nilai(0) = NoTransaksi
    nilai(1) = kodeAcc
    nilai(2) = Replace(nilaiJurnal, ",", ".")
    nilai(3) = NoJurnal
    nilai(4) = keterangan
    nilai(5) = kode1
    nilai(6) = kode2
    nilai(7) = Replace(KeteranganNilai, ",", ".")
    nilai(8) = NomorUrut

    tambah_data table_name, fields, nilai
End Sub


Sub ProsesHistoryProduksi(tgldari As Date, tglsampai As Date)
    conn.ConnectionString = strcon
    conn.Open

    conn.BeginTrans
    conn.Execute "Delete from tmp_hst_produksi"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_input, qty_input, nama_bahan, urut, jenis, satuan) " & _
                 "select '1'+t.nomer_potong, t.tanggal, t.nomer_potong, " & _
                 "t.kode_bahan, t.qty, b.nama_bahan, 1, 'i', b.satuan " & _
                 "from t_potongh t " & _
                 "inner join ms_bahan b on b.kode_bahan=t.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), t.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), t.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_standart, qty_output_hasil, nama_bahan, urut, jenis, satuan) " & _
                 "select '1'+t.nomer_potong, t.tanggal, t.nomer_potong, " & _
                 "d.kode_bahan, d.qty, d.qty_manual, b.nama_bahan, 1, 'o', b.satuan " & _
                 "from t_potongd d " & _
                 "inner join t_potongh t on t.nomer_potong=d.nomer_potong " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), t.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), t.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_hasil, nama_bahan, urut, jenis, satuan, no_rincian) " & _
                 "select '1'+t.nomer_potong, t.tanggal, t.nomer_potong, " & _
                 "d.kode_bahan, d.qty, b.nama_bahan, 1, 'r', b.satuan, t.nomer_hasilpotong " & _
                 "from t_hasilpotongd d " & _
                 "inner join t_hasilpotongh t on t.nomer_hasilpotong=d.nomer_hasilpotong " & _
                 "inner join t_potongh p on p.nomer_potong=t.nomer_potong " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where t.status_posting='1' and CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    
    'cetak dgn potong
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_input, qty_input, nama_bahan, urut, jenis, satuan) " & _
                 "select '2'+t.nomer_cetak, t.tanggal, t.no_history, " & _
                 "t.kode_bahan, t.qty, b.nama_bahan, 2, 'i', b.satuan " & _
                 "from t_cetakh t " & _
                 "inner join t_potongh p on p.nomer_potong=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=t.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_standart, qty_output_hasil, nama_bahan, urut, jenis, satuan) " & _
                 "select '2'+t.nomer_cetak, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, d.qty_manual, b.nama_bahan, 2, 'o', b.satuan " & _
                 "from t_cetakd d " & _
                 "inner join t_cetakh t on t.nomer_cetak=d.nomer_cetak " & _
                 "inner join t_potongh p on p.nomer_potong=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_hasil, nama_bahan, urut, jenis, satuan, no_rincian) " & _
                 "select '2'+t.nomer_cetak, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, b.nama_bahan, 2, 'r', b.satuan, t.nomer_hasilcetak " & _
                 "from t_hasilcetakd d " & _
                 "inner join t_hasilcetakh t on t.nomer_hasilcetak=d.nomer_hasilcetak " & _
                 "inner join t_potongh p on p.nomer_potong=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where t.status_posting='1' and CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    
    'cetak tanpa potong
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_input, qty_input, nama_bahan, urut, jenis, satuan) " & _
                 "select '2'+t.nomer_cetak, t.tanggal, t.no_history, " & _
                 "t.kode_bahan, t.qty, b.nama_bahan, 2, 'i', b.satuan " & _
                 "from t_cetakh t " & _
                 "inner join ms_bahan b on b.kode_bahan=t.kode_bahan " & _
                 "where t.no_history=t.nomer_cetak and CONVERT(VARCHAR(10), t.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), t.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_standart, qty_output_hasil, nama_bahan, urut, jenis, satuan) " & _
                 "select '2'+t.nomer_cetak, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, d.qty_manual, b.nama_bahan, 2, 'o', b.satuan " & _
                 "from t_cetakd d " & _
                 "inner join t_cetakh t on t.nomer_cetak=d.nomer_cetak " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where t.no_history=t.nomer_cetak and CONVERT(VARCHAR(10), t.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), t.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_hasil, nama_bahan, urut, jenis, satuan, no_rincian) " & _
                 "select '2'+t.nomer_cetak, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, b.nama_bahan, 2, 'r', b.satuan, t.nomer_hasilcetak " & _
                 "from t_hasilcetakd d " & _
                 "inner join t_hasilcetakh t on t.nomer_hasilcetak=d.nomer_hasilcetak " & _
                 "inner join t_cetakh c on c.nomer_cetak=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where t.no_history=t.nomer_cetak and t.status_posting='1' and CONVERT(VARCHAR(10), c.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), c.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
                 
    'plong dgn potong
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_input, qty_input, nama_bahan, urut, jenis, satuan) " & _
                 "select '3'+t.nomer_plong, t.tanggal, t.no_history, " & _
                 "t.kode_bahan, t.qty, b.nama_bahan, 3, 'i', b.satuan " & _
                 "from t_plongh t " & _
                 "inner join t_potongh p on p.nomer_potong=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=t.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_standart, qty_output_hasil, nama_bahan, urut, jenis, satuan) " & _
                 "select '3'+t.nomer_plong, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, d.qty_manual, b.nama_bahan, 3, 'o', b.satuan " & _
                 "from t_plongd d " & _
                 "inner join t_plongh t on t.nomer_plong=d.nomer_plong " & _
                 "inner join t_potongh p on p.nomer_potong=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_hasil, nama_bahan, urut, jenis, satuan, no_rincian) " & _
                 "select '3'+t.nomer_plong, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, b.nama_bahan, 3, 'r', b.satuan, t.nomer_hasilplong " & _
                 "from t_hasilplongd d " & _
                 "inner join t_hasilplongh t on t.nomer_hasilplong=d.nomer_hasilplong " & _
                 "inner join t_potongh p on p.nomer_potong=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where t.status_posting='1' and CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
                 
    'plong tanpa potong
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_input, qty_input, nama_bahan, urut, jenis, satuan) " & _
                 "select '3'+t.nomer_plong, t.tanggal, t.no_history, " & _
                 "t.kode_bahan, t.qty, b.nama_bahan, 3, 'i', b.satuan " & _
                 "from t_plongh t " & _
                 "inner join t_cetakh p on p.nomer_cetak=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=t.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_standart, qty_output_hasil, nama_bahan, urut, jenis, satuan) " & _
                 "select '3'+t.nomer_plong, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, d.qty_manual, b.nama_bahan, 3, 'o', b.satuan " & _
                 "from t_plongd d " & _
                 "inner join t_plongh t on t.nomer_plong=d.nomer_plong " & _
                 "inner join t_cetakh p on p.nomer_cetak=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
    conn.Execute "insert into tmp_hst_produksi (no_transaksi, tanggal, no_history, " & _
                 "kode_output, qty_output_hasil, nama_bahan, urut, jenis, satuan, no_rincian) " & _
                 "select '3'+t.nomer_plong, t.tanggal, t.no_history, " & _
                 "d.kode_bahan, d.qty, b.nama_bahan, 3, 'r', b.satuan, t.nomer_hasilplong " & _
                 "from t_hasilplongd d " & _
                 "inner join t_hasilplongh t on t.nomer_hasilplong=d.nomer_hasilplong " & _
                 "inner join t_cetakh p on p.nomer_cetak=t.no_history " & _
                 "inner join ms_bahan b on b.kode_bahan=d.kode_bahan " & _
                 "where t.status_posting='1' and CONVERT(VARCHAR(10), p.tanggal, 111)>='" & Format(tgldari, "yyyy/MM/dd") & "' " & _
                 "and CONVERT(VARCHAR(10), p.tanggal, 111)<='" & Format(tglsampai, "yyyy/MM/dd") & "'"
                 
    conn.CommitTrans
    conn.Close
End Sub

Private Sub updatehppRoll(ByRef NoTransaksi As String, kode_bahan As String, kode_gudang As String, noseri As String, qty As Double, harga As Currency, cn As ADODB.Connection)
cn.Execute "if exists(select * from stock where nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "') " & _
        "update stock set hpp=((stock*hpp)+(" & Replace(Format(qty, "###0.##"), ",", ".") & "*" & Replace(Format(harga, "###0.##"), ",", ".") & "))/(stock+" & Replace(Format(qty, "###0.##"), ",", ".") & ") where nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "'"
        
cn.Execute "insert into hst_hpp (jenis,nomer_transaksi,kode_bahan,stockawal,hppawal,qty,harga,hpp,stockakhir) values " & _
    "('Pembelian','" & NoTransaksi & "','" & kode_bahan & "','0','0','" & Replace(Format(qty, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & Replace(Format(qty, "###0.##"), ",", ".") & "')"
End Sub

Private Sub updatestockRoll(ByRef NoTransaksi As String, kode_bahan As String, kode_gudang As String, noseri As String, qty As Double, harga As Currency, cn As ADODB.Connection)
    cn.Execute "if exists(select * from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "') " & _
        "update stock set stock=stock+'" & Replace(Format(qty, "###0.##"), ",", ".") & "' where nomer_serial='" & noseri & "' and kode_gudang='" & kode_gudang & "' else " & _
        "insert into stock (kode_bahan,nomer_serial,stock,hpp,kode_gudang) values ('" & kode_bahan & "','" & noseri & "','" & Replace(Format(qty, "###0.##"), ",", ".") & "','" & Replace(Format(harga, "###0.##"), ",", ".") & "','" & kode_gudang & "')"
    
    'cn.Execute "if (select stock from stock where  nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "')=0 delete from stock where nomer_serial='" & noseri & "'  and kode_gudang='" & kode_gudang & "'"
End Sub

Private Sub insertHutang(kode_supplier As String, ByRef NoTransaksi As String, total As Currency, cn As ADODB.Connection)
    cn.Execute "if not exists (select * from list_hutang where nomer_transaksi='" & NoTransaksi & "') " & _
            "insert into list_hutang (kode_supplier,nomer_transaksi,hutang) values ('" & kode_supplier & "','" & NoTransaksi & "','" & Replace(Format(total, "###0.##"), ",", ".") & "')"
End Sub

Public Sub ProsesMutasi(tanggal As Date, tanggal2 As Date)
    conn.Open strcon
    conn.BeginTrans
    conn.Execute "delete from tmp_mutasistock"
    conn.Execute "insert into tmp_mutasistock(kode_bahan,kode_gudang,nomer_serial,awal,masuk,keluar) " & _
                 "select distinct k.kode_bahan,k.kode_gudang,k.nomer_serial,isnull(k1.awal,0),isnull(k2.masuk,0),isnull(k2.keluar,0) " & _
                 "from tmp_kartustock k " & _
                 "left join (select kode_bahan,kode_gudang,nomer_serial,sum(masuk-keluar) as awal from tmp_kartustock " & _
                 "where CONVERT(VARCHAR(10), tanggal, 111)<'" & Format(tanggal, "yyyy/MM/dd") & "' group by kode_bahan,kode_gudang,nomer_serial) k1 on k1.kode_bahan=k.kode_bahan and k1.kode_gudang=k.kode_gudang and k1.nomer_serial=k.nomer_serial " & _
                 "left join (select kode_bahan,kode_gudang,nomer_serial,isnull(sum(masuk),0) as masuk,isnull(sum(keluar),0) as keluar from tmp_kartustock " & _
                 "where CONVERT(VARCHAR(10), tanggal, 111)>='" & Format(tanggal, "yyyy/MM/dd") & "' and CONVERT(VARCHAR(10), tanggal, 111)<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by kode_bahan,kode_gudang,nomer_serial) k2 on k2.kode_bahan=k.kode_bahan  and k2.kode_gudang=k.kode_gudang and k2.nomer_serial=k.nomer_serial " & _
                 "where CONVERT(VARCHAR(10), k.tanggal, 111)<='" & Format(tanggal2, "yyyy/MM/dd") & "' "
    conn.CommitTrans
    conn.Close
End Sub

Public Sub HitungUlang_KartuStock(KodeBahan As String, KodeGudang As String, NomerSerial As String, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset
Dim StockAwal As Double, hpp As Double
Dim StockAwalQty As Double

    If Trim(NomerSerial) <> "" Then
        
        StockAwal = 0
        StockAwalQty = 0
        
        rs.Open "Select Sum(masuk) as awal,sum(qtymasuk) as awalQty from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & NomerSerial & "' and tipe ='Saldo Awal'", conn
        
        If Not rs.EOF Then
            If IsNull(rs!Awal) = False Then StockAwal = (rs!Awal)
            If IsNull(rs!AwalQty) = False Then StockAwalQty = (rs!AwalQty)
        End If
        rs.Close
    
        rs.Open "Select nomer_urut,masuk,keluar,qtymasuk,qtykeluar from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & NomerSerial & "' " & _
                "and tipe <>'Saldo Awal' order by nomer_urut", conn, adOpenDynamic
        
        While Not rs.EOF
            
            conn.Execute "Update tmp_kartustock set stockawal=" & Replace(StockAwal, ",", ".") & ",qtyawal=" & Replace(StockAwalQty, ",", ".") & " where nomer_urut='" & rs!nomer_urut & "'"
            
            StockAwal = StockAwal + (rs!masuk) - (rs!keluar)
            StockAwalQty = StockAwalQty + (rs!qtymasuk) - (rs!qtykeluar)
            
            rs.MoveNext
        Wend
        rs.Close
        
        conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & KodeBahan & "' and nomer_serial='" & NomerSerial & "' and kode_gudang='" & KodeGudang & "' ) " & _
                         "insert into stock  (kode_bahan,kode_gudang,nomer_serial,stock,qty) values ('" & KodeBahan & "','" & KodeGudang & "','" & NomerSerial & "'," & Replace(StockAwal, ",", ".") & "," & Replace(StockAwalQty, ",", ".") & ") else " & _
                         "update stock set stock =" & Replace(StockAwal, ",", ".") & ",qty=" & Replace(StockAwalQty, ",", ".") & "  " & _
                        "where kode_bahan='" & KodeBahan & "' " & _
                        "and nomer_serial='" & NomerSerial & "' " & _
                        "and kode_gudang='" & KodeGudang & "' "
                        
'            rs.Open "select top 1 hpp from hst_hpp where kode_bahan='" & KodeBahan & "' " & _
'                        "and nomer_serial='" & NomerSerial & "' " & _
'                        "and kode_gudang='" & KodeGudang & "' " & _
'                        "order by tanggal desc", conn
'            hpp = 0
'            If Not rs.EOF Then
'                hpp = (rs!hpp)
'            End If
'            rs.Close
'            If hpp > 0 Then
'                conn.Execute "update stock set hpp=" & Replace(hpp, ",", ".") & ",harga_produksi=" & Replace(hpp, ",", ".") & " " & _
'                             "where kode_bahan='" & KodeBahan & "' " & _
'                            "and nomer_serial='" & NomerSerial & "' " & _
'                            "and kode_gudang='" & KodeGudang & "' "
'            End If
'
        
    Else
        If KodeGudang <> "" Then
            StockAwal = 0
            StockAwalQty = 0
            rs2.Open "Select distinct nomer_serial from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                     "and kode_gudang='" & KodeGudang & "'", conn, adOpenDynamic, adLockOptimistic
            While Not rs2.EOF
                StockAwal = 0
                StockAwalQty = 0
                rs.Open "Select Sum(masuk) as awal,sum(qtymasuk) as AwalQty  from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                    "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & (rs2!nomer_serial) & "' and tipe ='Saldo Awal'", conn
            
                If Not rs.EOF Then
                    If IsNull(rs!Awal) = False Then StockAwal = (rs!Awal)
                    If IsNull(rs!AwalQty) = False Then StockAwalQty = (rs!AwalQty)
                End If
                rs.Close
            
                rs.Open "Select nomer_urut,masuk,keluar,qtymasuk,qtykeluar from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                        "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & (rs2!nomer_serial) & "' " & _
                        "and tipe <>'Saldo Awal' order by nomer_urut", conn, adOpenDynamic
                While Not rs.EOF
                    conn.Execute "Update tmp_kartustock set stockawal=" & Replace(StockAwal, ",", ".") & ",qtyawal=" & Replace(StockAwalQty, ",", ".") & " where nomer_urut='" & rs!nomer_urut & "'"
                    
                    StockAwal = StockAwal + (rs!masuk) - (rs!keluar)
                    StockAwalQty = StockAwalQty + (rs!qtymasuk) - (rs!qtykeluar)
                    
                    rs.MoveNext
                Wend
                rs.Close
                 
                 conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & KodeBahan & "' and nomer_serial='" & (rs2!nomer_serial) & "' and kode_gudang='" & KodeGudang & "' ) " & _
                             "insert into stock  (kode_bahan,kode_gudang,nomer_serial,stock,qty) values ('" & KodeBahan & "','" & KodeGudang & "','" & (rs2!nomer_serial) & "'," & Replace(StockAwal, ",", ".") & "," & Replace(StockAwalQty, ",", ".") & ") else " & _
                             "update stock set stock =" & Replace(StockAwal, ",", ".") & ",qty =" & Replace(StockAwalQty, ",", ".") & " " & _
                            "where kode_bahan='" & KodeBahan & "' " & _
                            "and nomer_serial='" & (rs2!nomer_serial) & "' " & _
                            "and kode_gudang='" & KodeGudang & "' "
                rs2.MoveNext
            Wend
            rs2.Close
        Else
            StockAwal = 0
            StockAwalQty = 0
            rs3.Open "select distinct kode_gudang from tmp_kartustock where kode_bahan='" & KodeBahan & "'", conn, adOpenDynamic, adLockOptimistic
            While Not rs3.EOF
                KodeGudang = rs3!kode_gudang
                rs2.Open "Select distinct nomer_serial from tmp_kartustock where kode_bahan='" & KodeBahan & "' ", conn, adOpenDynamic, adLockOptimistic
                While Not rs2.EOF
                    StockAwalQty = 0
                    StockAwal = 0
                    rs.Open "Select Sum(masuk) as awal,sum(qtymasuk) as AwalQty  from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                        "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & (rs2!nomer_serial) & "' and tipe ='Saldo Awal'", conn
                
                    If Not rs.EOF Then
                        If IsNull(rs!Awal) = False Then StockAwal = (rs!Awal)
                        If IsNull(rs!AwalQty) = False Then StockAwalQty = (rs!AwalQty)
                    End If
                    rs.Close
                
                    rs.Open "Select nomer_urut,masuk,keluar,qtymasuk,qtykeluar from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                            "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & (rs2!nomer_serial) & "' " & _
                            "and tipe <>'Saldo Awal' order by nomer_urut", conn, adOpenDynamic
                    While Not rs.EOF
                        conn.Execute "Update tmp_kartustock set stockawal=" & Replace(StockAwal, ",", ".") & ",qtyawal=" & Replace(StockAwalQty, ",", ".") & " where nomer_urut='" & rs!nomer_urut & "'"
                        
                        StockAwal = StockAwal + (rs!masuk) - (rs!keluar)
                        StockAwalQty = StockAwalQty + (rs!qtymasuk) - (rs!qtykeluar)
                        
                        rs.MoveNext
                    Wend
                    rs.Close
                     
                     conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & KodeBahan & "' and nomer_serial='" & (rs2!nomer_serial) & "' and kode_gudang='" & KodeGudang & "' ) " & _
                                 "insert into stock  (kode_bahan,kode_gudang,nomer_serial,stock,qty) values ('" & KodeBahan & "','" & KodeGudang & "','" & (rs2!nomer_serial) & "'," & Replace(StockAwal, ",", ".") & "," & Replace(StockAwalQty, ",", ".") & ") else " & _
                                 "update stock set stock =" & Replace(StockAwal, ",", ".") & ",qty =" & Replace(StockAwalQty, ",", ".") & " " & _
                                "where kode_bahan='" & KodeBahan & "' " & _
                                "and nomer_serial='" & (rs2!nomer_serial) & "' " & _
                                "and kode_gudang='" & KodeGudang & "' "
                    rs2.MoveNext
                Wend
                rs2.Close
                rs3.MoveNext
            Wend
            rs3.Close
        End If
    End If
End Sub

Public Sub Posting_NotaDebet(ByRef NoTransaksi As String)
Dim connrs As New ADODB.Connection
Dim conn As New ADODB.Connection
Dim i As Integer
Dim J As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim AccPiutang As String
Dim KodeCustomer As String
Dim NilaiJurnalH As Currency
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_NotaDebet As String
'On Error GoTo err
    i = 0
    connrs.Open strcon
    conn.Open strcon
    conn.BeginTrans
    i = 1
    rs.Open "select t.total,t.kode_customer,s.acc_piutang from t_notadebeth t inner join setting_accCustomer s on t.kode_customer=s.kode_customer where nomer_notadebet='" & NoTransaksi & "'", connrs
    
    AccPiutang = rs("acc_piutang")
    KodeCustomer = rs("kode_customer")
    NilaiJurnalH = rs("total")
    rs.Close
    insertkartuPiutang "Piutang Lain-Lain", NoTransaksi, tanggal, KodeCustomer, NilaiJurnalH * -1, conn
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "ND")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Nota Debet", NilaiJurnalH, "ND", conn
    
    If NilaiJurnalH > 0 Then
        add_jurnaldetail NoTransaksi, NoJurnal, AccPiutang, NilaiJurnalH, 0, 0, conn, "Nota Debet", KodeCustomer
        add_jurnaldetail NoTransaksi, NoJurnal, Acc_NotaDebet, 0, NilaiJurnalH, 1, conn
    ElseIf NilaiJurnalH < 0 Then
        add_jurnaldetail NoTransaksi, NoJurnal, Acc_NotaDebet, -NilaiJurnalH, 0, 0, conn
        add_jurnaldetail NoTransaksi, NoJurnal, AccPiutang, 0, -NilaiJurnalH, 1, conn, "Nota Debet", KodeCustomer
    End If
    J = 1
    rs.Open "select d.nomer_jual,d.nilai from t_notadebetd d " & _
            "where d.nomer_notadebet='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
'        conn.Execute "update list_piutang set koreksi=koreksi+ " & Replace((rs!nilai), ",", ".") & " where nomer_transaksi='" & (rs!nomer_jual) & "'"
        
        conn.Execute "update list_piutang set total_bayar=total_bayar - " & rs("nilai") & " " & _
                     "where nomer_transaksi='" & rs("nomer_jual") & "'"
        
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Execute "update t_notadebeth set status_posting='1' where nomer_notadebet='" & NoTransaksi & "'"
    conn.CommitTrans
    i = 0
    DropConnection
    'MsgBox "Data sudah diposting"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub

Public Sub Posting_NotaKredit(ByRef NoTransaksi As String)
Dim connrs As New ADODB.Connection
Dim conn As New ADODB.Connection
Dim i As Integer
Dim J As Integer
Dim Tipe As String
Dim Row As Integer
Dim tanggal As Date
Dim tanggal_jatuhtempo As Date
Dim AccHutang As String
Dim Kodesupplier As String
Dim NilaiJurnalH As Currency
Dim keterangan As String
Dim NoJurnal As String
Dim Acc_notakredit As String
'On Error GoTo err
    i = 0
    connrs.Open strcon
    conn.Open strcon
    conn.BeginTrans
    i = 1
    rs.Open "select t.total,t.kode_supplier,s.acc_hutang from t_notakredith t inner join setting_accsupplier s on t.kode_supplier=s.kode_supplier where nomer_notakredit='" & NoTransaksi & "'", connrs
    
    AccHutang = rs("acc_hutang")
    Kodesupplier = rs("kode_supplier")
    NilaiJurnalH = rs("total")
    rs.Close
    insertkartuHutang "hutang Lain-Lain", NoTransaksi, tanggal, Kodesupplier, NilaiJurnalH * -1, conn
    NoJurnal = newid("t_jurnalh", "no_jurnal", tanggal, "NK")
    add_jurnal NoTransaksi, NoJurnal, tanggal, "Nota Debet", NilaiJurnalH, "NK", conn
    
    If NilaiJurnalH > 0 Then
        add_jurnaldetail NoTransaksi, NoJurnal, AccHutang, 0, NilaiJurnalH, 0, conn, "Nota Kredit", Kodesupplier
        add_jurnaldetail NoTransaksi, NoJurnal, Acc_notakredit, NilaiJurnalH, 0, 1, conn
    ElseIf NilaiJurnalH < 0 Then
        add_jurnaldetail NoTransaksi, NoJurnal, Acc_notakredit, 0, -NilaiJurnalH, 0, conn
        add_jurnaldetail NoTransaksi, NoJurnal, AccHutang, -NilaiJurnalH, 0, 1, conn, "Nota Kredit", Kodesupplier
    End If
    J = 1
    rs.Open "select d.nomer_Beli,d.nilai from t_notakreditd d " & _
            "where d.nomer_notakredit='" & NoTransaksi & "' ", conn, adOpenDynamic, adLockOptimistic
    While Not rs.EOF
'        conn.Execute "update list_hutang set koreksi=koreksi+ " & Replace((rs!nilai), ",", ".") & " where nomer_transaksi='" & (rs!nomer_Beli) & "'"
        
        conn.Execute "update list_hutang set total_bayar=total_bayar - " & rs("nilai") & " " & _
                     "where nomer_transaksi='" & rs("nomer_beli") & "'"
        
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Execute "update t_notakredith set status_posting='1' where nomer_notakredit='" & NoTransaksi & "'"
    conn.CommitTrans
    i = 0
    DropConnection
    'MsgBox "Data sudah diposting"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    DropConnection
    Debug.Print no_transaksi & " " & err.Description
End Sub

Private Sub add_jurnal(ByRef NoTransaksi As String, no_jurnal As String, tanggal As Date, keterangan As String, Nominal As Currency, Tipe As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(5)
    ReDim nilai(5)

    table_name = "t_jurnalh"
    fields(0) = "no_transaksi"
    fields(1) = "no_jurnal"
    fields(2) = "tanggal"
    fields(3) = "keterangan"
'    fields(4) = "totaldebet"
'    fields(5) = "totalkredit"
    fields(4) = "tipe"

    nilai(0) = NoTransaksi
    nilai(1) = no_jurnal
    nilai(2) = Format(tanggal, "yyyy/MM/dd HH:mm:ss")
    nilai(3) = keterangan
'    nilai(4) = CCur(nominal)
'    nilai(5) = CCur(nominal)
    nilai(4) = Tipe

    cn.Execute tambah_data2(table_name, fields, nilai)

End Sub

Private Sub add_jurnaldetail(ByRef NoTransaksi As String, NoJurnal As String, kode_acc As String, debet As Currency, kredit As Currency, Row As Integer, cn As ADODB.Connection, Optional keterangan As String, Optional kode1 As String, Optional kode2 As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(9)
    ReDim nilai(9)

    table_name = "t_jurnald"
    fields(0) = "no_transaksi"
    fields(1) = "kode_acc"
    fields(2) = "debet"
    fields(3) = "kredit"
    fields(4) = "no_urut"
    fields(5) = "no_jurnal"
    fields(6) = "keterangan"
    fields(7) = "kode1"
    fields(8) = "kode2"

    nilai(0) = NoTransaksi
    nilai(1) = kode_acc
    nilai(2) = Replace(Format(debet, "###0.##"), ",", ".")
    nilai(3) = Replace(Format(kredit, "###0.##"), ",", ".")
    nilai(4) = Row
    nilai(5) = NoJurnal
    nilai(6) = keterangan
    nilai(7) = kode1
    nilai(8) = kode2

    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Public Sub insertkartustockBeras(Tipe As String, No As String, tanggal As Date, kode_bahan As String, qty As Double, berat As Double, gudang As String, nomer_serial As String, hpp As Double, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim StockAwal As Double
Dim qtyawal As Double
Dim masuk, keluar As Double
Dim qtymasuk, qtykeluar As Double

rs.Open "select top 1 stockawal+masuk-keluar,qtyawal+qtymasuk-qtykeluar from tmp_kartustock where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "' and  kode_gudang='" & gudang & "' order by nomer_urut desc", conn, adOpenForwardOnly
If Not rs.EOF Then
StockAwal = rs(0)
qtyawal = rs(1)
Else
StockAwal = getStockKertas2(kode_bahan, gudang, nomer_serial, conn)
qtyawal = getQty(kode_bahan, gudang, nomer_serial, conn)
End If
rs.Close
masuk = 0
keluar = 0
qtymasuk = 0
qtykeluar = 0
If berat > 0 Then masuk = berat
If berat < 0 Then keluar = berat * -1
If qty > 0 Then qtymasuk = qty
If qty < 0 Then qtykeluar = qty * -1
'Debug.Print kode_bahan & "=" & stockawal & " ->" & qty
conn.Execute "insert into tmp_kartustock (kode_bahan,nomer_serial,tipe,kode_gudang,id,tanggal,masuk,keluar,stockawal,qtyawal,qtymasuk,qtykeluar,hpp) values ('" & kode_bahan & "','" & nomer_serial & "','" & Tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd HH:mm:ss") & "'," & Replace(masuk, ",", ".") & "," & Replace(keluar, ",", ".") & "," & Replace(StockAwal, ",", ".") & "," & Replace(qtyawal, ",", ".") & "," & Replace(qtymasuk, ",", ".") & "," & Replace(qtykeluar, ",", ".") & "," & Replace(hpp, ",", ".") & ")"

conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & kode_bahan & "'and nomer_serial='" & nomer_serial & "'  and kode_gudang='" & gudang & "') insert into stock (kode_bahan,stock,qty,nomer_serial,kode_gudang) values ('" & kode_bahan & "'," & Replace(masuk + keluar, ",", ".") & "," & Replace(qtymasuk + qtykeluar, ",", ".") & ",'" & nomer_serial & "','" & gudang & "') else " & _
            "update stock set stock=stock+" & Replace(masuk + keluar, ",", ".") & ", qty=qty+" & Replace(qtymasuk + qtykeluar, ",", ".") & "  where kode_bahan='" & kode_bahan & "' and nomer_serial='" & nomer_serial & "'"
            

End Sub

Public Sub HitungStock(KodeBahan As String, KodeGudang As String, NomerSerial As String, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim total As Double, totalQty As Double, hpp As Double
        
        rs.Open "Select Sum(masuk-keluar) as Total,Sum(qtymasuk-qtykeluar) as TotalQty from tmp_kartustock where kode_bahan='" & KodeBahan & "' " & _
                "and kode_gudang='" & KodeGudang & "' and nomer_serial='" & NomerSerial & "' ", conn, adOpenDynamic, adLockOptimistic
        
        total = 0
        If Not rs.EOF And IsNull(rs!total) = False Then
            total = (rs!total)
            totalQty = (rs!totalQty)
        End If
        rs.Close
    
        conn.Execute "if not exists (select kode_bahan from stock where kode_bahan='" & KodeBahan & "' and nomer_serial='" & NomerSerial & "' and kode_gudang='" & KodeGudang & "' ) " & _
                     "insert into stock  (kode_bahan,kode_gudang,nomer_serial,stock,qty) values ('" & KodeBahan & "','" & KodeGudang & "','" & NomerSerial & "'," & Replace(total, ",", ".") & "," & Replace(totalQty, ",", ".") & ") else " & _
                     "update stock set stock =" & Replace(total, ",", ".") & ",qty =" & Replace(totalQty, ",", ".") & " " & _
                    "where kode_bahan='" & KodeBahan & "' " & _
                    "and nomer_serial='" & NomerSerial & "' " & _
                    "and kode_gudang='" & KodeGudang & "' "
                        
End Sub
