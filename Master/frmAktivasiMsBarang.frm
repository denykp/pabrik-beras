VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAktivasiMsBarang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Barang"
   ClientHeight    =   8070
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   17055
   ClipControls    =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   17055
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3150
      TabIndex        =   15
      Top             =   7380
      Width           =   1110
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Tambah/ubah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8910
      TabIndex        =   14
      Top             =   630
      Width           =   1590
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   375
      Left            =   3645
      Picture         =   "frmAktivasiMsBarang.frx":0000
      TabIndex        =   12
      Top             =   135
      Width           =   420
   End
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1530
      TabIndex        =   10
      Top             =   1035
      Width           =   2895
   End
   Begin VB.ComboBox txtColHeader 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5895
      TabIndex        =   9
      Top             =   630
      Width           =   2895
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7110
      TabIndex        =   7
      Top             =   1125
      Width           =   1140
   End
   Begin VB.CommandButton cmdNew 
      Caption         =   "Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5895
      TabIndex        =   6
      Top             =   1125
      Width           =   1140
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      TabIndex        =   2
      Top             =   7380
      Width           =   1110
   End
   Begin SSDataWidgets_B.SSDBGrid dbgrid 
      Height          =   5460
      Left            =   180
      TabIndex        =   1
      Top             =   1575
      Width           =   16755
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   7
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   238
      Columns.Count   =   7
      Columns(0).Width=   3519
      Columns(0).Caption=   "Kode Barang"
      Columns(0).Name =   "Kode Barang"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   1
      Columns(1).Width=   4445
      Columns(1).Caption=   "Nama Barang"
      Columns(1).Name =   "Nama Barang"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4207
      Columns(2).Caption=   "Pemenang"
      Columns(2).Name =   "Pemenang"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   1
      Columns(3).Width=   4366
      Columns(3).Caption=   "Nama Pemenang"
      Columns(3).Name =   "Nama Pemenang"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   4233
      Columns(4).Caption=   "Dimenangkan(Ton)"
      Columns(4).Name =   "Dimenangkan(Ton)"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3519
      Columns(5).Caption=   "Harga (Kg)"
      Columns(5).Name =   "Harga (Kg)"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Harga Eceran"
      Columns(6).Name =   "Harga Eceran"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   29554
      _ExtentY        =   9631
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdout 
      Caption         =   "[ESC]  &Exit"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4500
      TabIndex        =   3
      Top             =   7380
      Width           =   1065
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1530
      TabIndex        =   0
      Top             =   630
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   99287043
      CurrentDate     =   38927
   End
   Begin VB.Label lblkodesupplier 
      Caption         =   "-"
      Height          =   330
      Left            =   11115
      TabIndex        =   17
      Top             =   900
      Width           =   1005
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   510
      Left            =   7920
      TabIndex        =   16
      Top             =   3780
      Width           =   1230
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1530
      TabIndex        =   13
      Top             =   135
      Width           =   2085
   End
   Begin VB.Label Label2 
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   11
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Kolom"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5895
      TabIndex        =   8
      Top             =   270
      Width           =   525
   End
   Begin VB.Label lblCaption 
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5940
      TabIndex        =   5
      Top             =   675
      Width           =   2895
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   4
      Top             =   675
      Width           =   1320
   End
End
Attribute VB_Name = "frmAktivasiMsBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbKategori_Click()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim id As String
    conn.Open strcon
    dbgrid.RemoveAll
    dbgrid.FieldSeparator = "#"
    
    While dbgrid.Columns.Count > 6
    dbgrid.Columns.Remove (dbgrid.Columns.Count - 1)
    Wend
    rs.Open "select top 1 id from hst_hargajualh where kategori='" & cmbKategori.text & "' order by id desc", conn
    If Not rs.EOF Then id = rs!id
    rs.Close
    rs.Open "select distinct d.kode_bahan,m.nama_bahan,d.urut_barang from hst_hargajuald d inner join ms_bahan m on d.kode_bahan=m.kode_bahan where id='" & id & "' order by urut_barang", conn
    While Not rs.EOF
        dbgrid.AddItem rs(0) & "#" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select distinct d.nama_harga,harga,d.no_urut from hst_hargajuald d where id='" & id & "' order by no_urut", conn
    While Not rs.EOF
        If rs!no_urut > 3 Then
            dbgrid.Columns.Add (dbgrid.Columns.Count)
            dbgrid.Columns(dbgrid.Columns.Count - 1).Caption = rs(0)
        End If
        dbgrid.Columns(rs!no_urut + 2).text = CStr(rs(1))
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub cmdAdd_Click()
    If txtColHeader.text <> "" Then
        If lblCaption = "" Then
            dbgrid.Columns.Add (dbgrid.Columns.Count)
            dbgrid.Columns(dbgrid.Columns.Count - 1).Caption = txtColHeader
        Else
            dbgrid.Columns(CInt(lblCaption)).Caption = txtColHeader
        End If
        txtColHeader = ""
        lblCaption = ""
    End If
End Sub

Private Sub cmdDelete_Click()
    conn.Open strcon
    conn.Execute "delete from hst_hargajualh where id='" & lblNoTrans & "'"
    conn.Execute "delete from hst_hargajuald where id='" & lblNoTrans & "'"
    conn.Close
    MsgBox "Data sudah dihapus"
    reset_form
End Sub

Private Sub cmdHapus_Click()
    dbgrid.Columns.Remove (CLng(lblCaption))
    lblCaption = ""
    txtColHeader = ""
    cmdHapus.Enabled = False
End Sub

Private Sub cmdNew_Click()
    lblCaption = ""
    txtColHeader = ""
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = searchBarang
    frmSearch.nmform = "frmAktivasiMsBarang"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_bahan"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang1"
    
    frmSearch.loadgrid frmSearch.query

'    frmSearch.cmbKey.ListIndex = 2
'    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    cek_kodebarang1 = False
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & dbgrid.Columns(0).text & "'", conn
    If Not rs.EOF Then
        dbgrid.Columns(1).text = rs!nama_bahan
        cek_kodebarang1 = True
    Else
        dbgrid.Columns(1).text = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Private Sub cmdout_Click()
    Unload Me
End Sub

Private Sub cmdSearchID_Click()
On Error GoTo salah
    With frmSearch
        .connstr = strcon
        .query = "select id,tanggal,kategori,userid from hst_hargajualh"
        .nmform = "frmAktivasiMsBarang"
        .nmctrl = "lblNoTrans"
        .nmctrl2 = ""
        .keyIni = "hargajual"
        .col = 0
        frmSearch.Index = -1
        .proc = "cek_notrans"
        .loadgrid .query
        .cmbSort.ListIndex = 1
        
        .requery
        Set .frm = Me
        .Show vbModal
    End With
Exit Sub:
salah:
End Sub
Public Sub cek_notrans()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    Dim id As String
    conn.Open strcon
    dbgrid.RemoveAll
    dbgrid.FieldSeparator = "#"
    
    While dbgrid.Columns.Count > 6
    dbgrid.Columns.Remove (dbgrid.Columns.Count - 1)
    Wend
    rs.Open "select * from hst_hargajualh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        id = rs!id
        DTPicker1 = rs!tanggal
        cmbKategori.text = rs!kategori
    End If
    rs.Close
    
'    rs.Open "select distinct d.nama_harga,harga,d.no_urut from hst_hargajuald d where id='" & id & "' order by no_urut", conn
    rs.Open "select distinct d.nama_harga,d.no_urut from hst_hargajuald d where id='" & id & "' order by no_urut", conn
    While Not rs.EOF
        If rs!no_urut > 1 Then
            dbgrid.Columns.Add (dbgrid.Columns.Count)
            dbgrid.Columns(dbgrid.Columns.Count - 1).Caption = rs(0)
        End If
        
        rs.MoveNext
    Wend
    rs.Close
    Dim items As String
    rs.Open "select distinct d.kode_bahan,m.nama_bahan,d.urut_barang,isnull(d.pemenang,''),isnull(d.dimenangkan,0),s.nama_supplier from hst_hargajuald d inner join ms_bahan m on d.kode_bahan=m.kode_bahan left join ms_supplier s on d.pemenang = s.kode_supplier where id='" & id & "' order by urut_barang", conn
    While Not rs.EOF
        items = rs(0) & "#" & rs(1) & "#" & rs(3) & "#" & rs!Nama_Supplier
        rs1.Open "select distinct d.harga,no_urut from hst_hargajuald d where kode_bahan='" & rs(0) & "' and id='" & id & "' order by no_urut", conn
        While Not rs1.EOF
            items = items & "#" & rs1(0)
            rs1.MoveNext
        Wend
        rs1.Close
        dbgrid.AddItem items
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
On Error GoTo err
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim id As String
    Dim counter As Integer
    Dim status As Byte
    status = 0
    id = lblNoTrans
    
    conn.Open strcon
    conn.BeginTrans
    status = 1
    If lblNoTrans = "" Then
        lblNoTrans = newid("hst_hargajualh", "id", DTPicker1, "hst")
    Else
        conn.Execute "delete from hst_hargajualh where id='" & lblNoTrans & "'"
        conn.Execute "delete from hst_hargajuald where id='" & lblNoTrans & "'"
    End If
    
    conn.Execute "insert into hst_hargajualh values ('" & lblNoTrans & "','" & Format(DTPicker1, "yyyy/MM/dd HH:mm:ss") & "','" & User & "','" & cmbKategori.text & "')"
    counter = 0
    dbgrid.MoveFirst
    
    While counter < dbgrid.Rows
        If dbgrid.Columns(0).text <> "" Then
            For i = 4 To dbgrid.Columns.Count - 1
                conn.Execute "insert into hst_hargajuald values ('" & dbgrid.Columns(0).text & "','" & lblNoTrans & "','" & dbgrid.Columns(i).Caption & "','" & IIf(dbgrid.Columns(i).text = "", 0, dbgrid.Columns(i).text) & "','" & i - 4 & "','" & counter & "','" & dbgrid.Columns(2).text & "'," & CDbl(dbgrid.Columns(4).text) & ")"
            Next
            conn.Execute "update ms_bahan set harga_sat=(select top 1 harga from hst_hargajuald where kode_bahan='" & dbgrid.Columns(0).text & "' and nama_harga='Harga Kg' order by id desc) where kode_bahan='" & dbgrid.Columns(0).text & "'"
        End If
        counter = counter + 1
        dbgrid.MoveNext
    Wend
    
    conn.CommitTrans
    status = 0
    conn.Close
    reset_form
    Exit Sub
err:
    If status = 1 Then conn.RollbackTrans
    MsgBox err.Description
End Sub

Private Sub reset_form()
On Error Resume Next
    dbgrid.RemoveAll
    For i = 7 To dbgrid.Columns.Count
    dbgrid.Columns.Remove (i)
    Next
    txtKdBrg = ""
    LblNamaBarang1 = ""
    lblNoTrans = ""
    txtKdBrg.SetFocus
    loadcombo
End Sub

Private Sub loadcombo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    txtColHeader.Clear
    rs.Open "select distinct nama_harga from hst_hargajuald where nama_harga not in ('Harga Kg','Harga Eceran','Harga Partai','Harga Ton') order by nama_harga", conn
    While Not rs.EOF
        txtColHeader.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbKategori.Clear
    rs.Open "select distinct kategori from hst_hargajualh order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub dbgrid_AfterColUpdate(ByVal ColIndex As Integer)
    If ColIndex = 0 Then
        For i = 4 To dbgrid.Columns.Count - 1
        If Not IsNumeric(dbgrid.Columns(i).text) Then dbgrid.Columns(i).text = "0"
        Next
    End If
End Sub

Private Sub dbgrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim row As Long
Dim counter As Long
Dim kodebarang As String
    If ColIndex = 0 Then
        row = dbgrid.row
        kodebarang = dbgrid.ActiveCell.text
        
        counter = 0
        While counter < dbgrid.Rows
            If dbgrid.Columns(0).CellText(counter) = kodebarang And counter <> row Then
                MsgBox "Kode barang yang anda masukkan sudah ada"
                Cancel = True
                Exit Sub
            End If
            
            counter = counter + 1
        Wend
        If Not cek_kodebarang1 And kodebarang <> "" Then
            MsgBox "Kode barang yang anda masukkan salah"
            Cancel = True
        End If
    End If
    
End Sub

Private Sub DBGrid_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    If dbgrid.row < 4 Then Cancel = True
End Sub

Private Sub dbgrid_BtnClick()
On Error GoTo salah
With frmSearch
    If dbgrid.col = 0 Then
        .connstr = strcon
        .query = searchBarang
        .nmform = "frmaktivasimsbarang"
        .nmctrl = "dbgrid"
        .nmctrl2 = ""
        .keyIni = "ms_bahan"
        .col = 0
        .Index = 0
        .proc = "cek_kodebarang1"
    ElseIf dbgrid.col = 2 Then
        .connstr = strcon
        .query = SearchSupplier
        .nmform = "frmaktivasimsbarang"
        .nmctrl = "dbgrid"
        .nmctrl2 = "dbgrid"
        .keyIni = "ms_supplier"
        .col = 0
        .Index = 2
        .col2 = 1
        .Index2 = 3
        .proc = ""
    End If
    .loadgrid .query
    .cmbSort.ListIndex = 0
    .cmbSortby.ListIndex = 1
    .requery
    Set .frm = Me
    .Show vbModal
End With
Exit Sub:
salah:
End Sub

Public Sub cek_supplier()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

conn.Open strcon
rs.Open "select * from ms_supplier where kode_supplier"

End Sub

Private Sub dbgrid_HeadClick(ByVal ColIndex As Integer)
    If ColIndex > 5 Then
        txtColHeader = dbgrid.Columns(ColIndex).Caption
        lblCaption = ColIndex
        cmdHapus.Enabled = True
    End If
End Sub

Private Sub dbgrid_KeyDown(KeyCode As Integer, Shift As Integer)
   ' MsgBox dbgrid.row
'    If KeyCode = vbKeyDelete Then
'        dbgrid.RemoveItem (dbgrid.row)
'    End If
End Sub

Private Sub dbgrid_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
'MsgBox X & Y & Button & Shift
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
End Sub

Private Sub txtKey_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyUp Then
        If Not Adodc1.Recordset.BOF Then Adodc1.Recordset.MovePrevious
    ElseIf KeyCode = vbKeyDown Then
        If Not Adodc1.Recordset.EOF Then Adodc1.Recordset.MoveNext
    End If
End Sub

Private Sub txtKey_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
go_search
End If

End Sub

Sub go_search()
 Adodc1.RecordSource = "Select  m.[kode_bahan],m.[nama_bahan],kategori,flag from ms_bahan m inner join stock on m.[kode_bahan]=stock.[kode_bahan] where m.[kode_bahan]+[nama_bahan] like '%" & txtKey & "%'"
 Adodc1.Refresh
 Set flxGrid.DataSource = Adodc1
 flxGrid.Refresh
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        MySendKeys "{tab}"
        KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    reset_form
    DTPicker1 = Now
    
    
End Sub
Private Sub txtKdBrg_LostFocus()
    cek_kodebarang1
End Sub

