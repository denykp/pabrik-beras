VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPembelian 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembelian"
   ClientHeight    =   9405
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12930
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9405
   ScaleWidth      =   12930
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3150
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   32
      Top             =   990
      Width           =   420
   End
   Begin VB.CommandButton cmdSearchPO 
      Appearance      =   0  'Flat
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3510
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   29
      Top             =   585
      Width           =   420
   End
   Begin VB.Frame frJT 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   420
      Left            =   6660
      TabIndex        =   26
      Top             =   1260
      Width           =   3480
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   1530
         TabIndex        =   27
         Top             =   45
         Width           =   1875
         _ExtentX        =   3307
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   208404483
         CurrentDate     =   38927
      End
      Begin VB.Label Label9 
         Caption         =   "Jatuh Tempo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   45
         TabIndex        =   28
         Top             =   75
         Width           =   1320
      End
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   6615
      ScaleHeight     =   345
      ScaleWidth      =   3090
      TabIndex        =   22
      Top             =   855
      Width           =   3120
      Begin VB.OptionButton OptLunas 
         Caption         =   "Tunai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         TabIndex        =   24
         Top             =   45
         Width           =   915
      End
      Begin VB.OptionButton OptLunas 
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1575
         TabIndex        =   23
         Top             =   45
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah bayar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   25
         Top             =   1035
         Visible         =   0   'False
         Width           =   1725
      End
   End
   Begin VB.TextBox txtNoSJ 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   19
      Top             =   1395
      Width           =   2205
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3105
      Picture         =   "frmAddPembelian.frx":0000
      TabIndex        =   17
      Top             =   8640
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   315
      Picture         =   "frmAddPembelian.frx":0102
      TabIndex        =   3
      Top             =   8640
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4500
      Picture         =   "frmAddPembelian.frx":0204
      TabIndex        =   5
      Top             =   8640
      Width           =   1230
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   2
      Top             =   1785
      Width           =   8250
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3708
      Picture         =   "frmAddPembelian.frx":0306
      TabIndex        =   8
      Top             =   105
      Width           =   420
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9270
      TabIndex        =   7
      Top             =   165
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10170
      TabIndex        =   6
      Top             =   150
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   1
      Top             =   1005
      Width           =   1530
   End
   Begin VB.TextBox txtNoOrder 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1575
      TabIndex        =   0
      Top             =   585
      Width           =   1875
   End
   Begin VB.CommandButton cmdPosting 
      Caption         =   "Posting"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1710
      Picture         =   "frmAddPembelian.frx":0408
      TabIndex        =   4
      Top             =   8640
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10080
      Top             =   630
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   8100
      TabIndex        =   9
      Top             =   510
      Width           =   2430
      _ExtentX        =   4286
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy HH:mm:ss"
      Format          =   208404483
      CurrentDate     =   38927
   End
   Begin SSDataWidgets_B.SSDBGrid dbgrid 
      Height          =   5460
      Left            =   90
      TabIndex        =   21
      Top             =   2385
      Width           =   12435
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   7
      MultiLine       =   0   'False
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   238
      Columns.Count   =   7
      Columns(0).Width=   3307
      Columns(0).Caption=   "Kode Bahan"
      Columns(0).Name =   "Kode Bahan"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   5398
      Columns(1).Caption=   "Nama Bahan"
      Columns(1).Name =   "Nama Bahan"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "Serial No"
      Columns(2).Name =   "Serial No"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1614
      Columns(3).Caption=   "Qty"
      Columns(3).Name =   "Qty"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1773
      Columns(4).Caption=   "Berat"
      Columns(4).Name =   "Berat"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   2408
      Columns(5).Caption=   "Harga"
      Columns(5).Name =   "Harga"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2752
      Columns(6).Caption=   "Total"
      Columns(6).Name =   "Total"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      _ExtentX        =   21934
      _ExtentY        =   9631
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7605
      TabIndex        =   31
      Top             =   8010
      Width           =   735
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8550
      TabIndex        =   30
      Top             =   8010
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "No Faktur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   20
      Top             =   1440
      Width           =   960
   End
   Begin VB.Label lblKategori 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   150
      TabIndex        =   18
      Top             =   1335
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1575
      TabIndex        =   16
      Top             =   105
      Width           =   2085
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   15
      Top             =   1785
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6615
      TabIndex        =   14
      Top             =   555
      Width           =   1320
   End
   Begin VB.Label Label7 
      Caption         =   "No.Penerimaan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   13
      Top             =   600
      Width           =   1410
   End
   Begin VB.Label Label19 
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   12
      Top             =   1050
      Width           =   960
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3645
      TabIndex        =   11
      Top             =   990
      Width           =   2895
   End
   Begin VB.Label Label22 
      Caption         =   "No. Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   10
      Top             =   180
      Width           =   1320
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPembelian"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim total As Currency
Dim mode As Byte
Dim mode2 As Byte
Dim foc As Byte
Dim totalQty As Double
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim nobayar As String
Dim colname() As String
Dim NoJurnal As String
Dim status_posting As Boolean
Dim seltab As Byte
Private Sub chkNumbering_Click()
    If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub


Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\Transferpembelian.rpt"
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'        .SelectionFormula = " {t_terimabarangh.ID}='" & lblNoTrans & "'"
'        .Destination = crptToWindow
'        .ParameterFields(0) = "login;" + user + ";True"
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
'        .action = 1
'    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub cmdPosting_Click()
On Error GoTo err
    If Not Cek_qty Then
        MsgBox "Berat Serial tidak sama dengan jumlah pembelian"
        Exit Sub
    End If
    If Not Cek_Serial Then
        MsgBox "Ada Serial Number yang sudah terpakai"
        Exit Sub
    End If
    simpan
    If posting_pembelian(lblNoTrans) Then
        MsgBox "Proses Posting telah berhasil"
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description

End Sub
Private Function Cek_Serial() As Boolean
Cek_Serial = True
End Function
        
Private Function Cek_qty() As Boolean
    Cek_qty = True
End Function

Private Sub cmdreset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
'On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
'    CommonDialog1.filter = "*.xls"
'    CommonDialog1.filename = "Excel Filename"
'    CommonDialog1.ShowSave
'    If CommonDialog1.filename <> "" Then
'        saveexcelfile CommonDialog1.filename
'    End If
'    Exit Sub
'err:
'    MsgBox err.Description
End Sub

Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then Call MySendKeys("{tab}")
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchBeli
    frmSearch.nmform = "frmAddPembelian"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "Beli"
    frmSearch.col = 0
    frmSearch.index = -1

    frmSearch.proc = "cek_notrans"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchPO_Click()
    frmSearch.connstr = strcon
    
    frmSearch.query = SearchTerimaBarang
    frmSearch.nmform = "frmAddPembelian"
    frmSearch.nmctrl = "txtNoOrder"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "PO"
    frmSearch.col = 0
    frmSearch.index = -1

    frmSearch.proc = "cek_noTerima"

    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchSupplier_Click()
    frmSearch.connstr = strcon
    frmSearch.query = SearchSupplier
    frmSearch.nmform = "frmAddPembelian"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.nmctrl2 = ""
    frmSearch.keyIni = "ms_supplier"
    frmSearch.col = 0
    frmSearch.index = -1

    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If simpan Then
        MsgBox "Data sudah tersimpan"
        reset_form
        Call MySendKeys("{tab}")
    End If
End Sub
Private Function simpan() As Boolean
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahLama As Long, jumlah As Long, HPPLama As Double, harga As Double, HPPBaru As Double
i = 0
On Error GoTo err
    simpan = False
    id = lblNoTrans
    If lblNoTrans = "-" And chkNumbering = "1" Then
        lblNoTrans = newid("t_belih", "nomer_beli", DTPicker1, "PB")
    End If
    conn.ConnectionString = strcon
    conn.Open
    If chkNumbering <> "1" Then
    rs.Open "select * from t_belih where id='" & txtID.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Nomor ID " & txtID.text & " sudah digunakan, silahkan menggunakan ID yang lain"
        rs.Close
        DropConnection
        Exit Function
    End If
    rs.Close
    End If
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_belih where nomer_beli='" & id & "'"
    conn.Execute "delete from t_belid where nomer_beli='" & id & "'"
    
    add_dataheader
    dbgrid.MoveFirst
    row = 0
    While row < dbgrid.Rows
        add_datadetail row
        row = row + 1
        dbgrid.MoveNext
    Wend
    conn.CommitTrans
    i = 0
    simpan = True
    DropConnection

    Exit Function
err:
    If i = 1 Then conn.RollbackTrans
    DropConnection
    
    MsgBox err.Description
End Function
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtNoOrder.text = ""
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    txtNoSJ.text = ""
   
  
    status_posting = False
    dbgrid.RemoveAll
    
    
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = Now
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(10)
    ReDim nilai(10)
    table_name = "t_belih"
    fields(0) = "nomer_beli"
    fields(1) = "tanggal_beli"
    fields(2) = "tanggal_jatuhtempo"
    fields(3) = "kode_supplier"
    fields(4) = "nomer_terimabarang"
    fields(5) = "total"
    fields(6) = "keterangan"
    fields(7) = "userid"
    fields(8) = "cara_bayar"
    fields(9) = "nomer_invoice"
    

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd HH:mm:ss")
    nilai(2) = Format(DTPicker2, "yyyy/MM/dd")
    nilai(3) = txtKodeSupplier.text
    nilai(4) = txtNoOrder.text
    nilai(5) = Replace(Format(total, "###0.##"), ",", ".")
    nilai(6) = txtKeterangan.text
    nilai(7) = User
    nilai(9) = txtNoSJ
    
    
    If OptLunas(0).value = True Then
        nilai(8) = "T"
    Else
        nilai(8) = "K"
    End If
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String

    ReDim fields(7)
    ReDim nilai(7)

    table_name = "t_belid"
    fields(0) = "nomer_beli"
    fields(1) = "kode_Bahan"
    fields(2) = "nomer_serial"
    fields(3) = "qty"
    fields(4) = "NO_URUT"
    fields(5) = "berat"
    fields(6) = "harga"

    nilai(0) = lblNoTrans
    nilai(1) = dbgrid.Columns(0).text
    nilai(2) = dbgrid.Columns(2).text
    nilai(3) = dbgrid.Columns(3).text
    nilai(4) = row
    nilai(5) = Replace(Format(dbgrid.Columns(4).text, "###0.###"), ",", ".")
    nilai(6) = Replace(Format(dbgrid.Columns(5).text, "###0.###"), ",", ".")

    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_belih where nomer_beli='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        
        DTPicker1 = rs("tanggal_beli")
        
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode_supplier")
        txtNoOrder.text = rs("nomer_terimabarang")
        txtNoSJ = rs!nomer_invoice
        cek_supplier
        
        totalQty = 0
        total = 0
'        cmdPrint.Visible = True
        If rs!status_posting = "0" Then cmdPosting.Visible = True
        If rs.State Then rs.Close
        dbgrid.RemoveAll
        dbgrid.FieldSeparator = "#"
        row = 1

        rs.Open "select d.[kode_bahan],m.[nama_bahan], nomer_serial,qty,d.berat,harga,berat*harga as total from t_belid d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_beli='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                dbgrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & Format(rs(5), "#,##0") & "#" & Format(rs(6), "#,##0")
                total = total + (rs(5) * rs(4))
                rs.MoveNext
        Wend
        rs.Close
        lblTotal = Format(total, "#,##0")
        
        
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_noTerima()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cmdPosting.Visible = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_terimabarangh where nomer_terimabarang='" & txtNoOrder & "'", conn
    If Not rs.EOF Then
        
        txtKodeSupplier.text = rs("kode_supplier")
        cek_supplier
        
        totalQty = 0
        total = 0
'        cmdPrint.Visible = True
        
        If rs.State Then rs.Close
        dbgrid.RemoveAll
        dbgrid.FieldSeparator = "#"
        row = 1
        
        rs.Open "select d.[kode_bahan],m.[nama_bahan],nomer_serial, qty,d.berat from t_terimabarangd d inner join ms_bahan m on d.[kode_bahan]=m.[kode_bahan] where d.nomer_terimabarang='" & txtNoOrder & "' order by no_urut", conn
        While Not rs.EOF
                dbgrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#0#0"
                totalQty = totalQty + rs(4)
                rs.MoveNext
        Wend
        rs.Close
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub


Private Sub dbgrid_AfterColUpdate(ByVal ColIndex As Integer)
    dbgrid.Columns(6).text = Format(dbgrid.Columns(4).text * dbgrid.Columns(5).text, "#,##0")
    total = total + dbgrid.Columns(6).text
    lblTotal = Format(total, "#,##0")
End Sub

Private Sub dbgrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If Not IsNumeric(dbgrid.Columns(ColIndex).text) Then
        Cancel = True
    Else
        total = total - dbgrid.Columns(6).text
    End If
End Sub

Private Sub Form_Activate()
'    call mysendkeys("{tab}")
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        
        KeyAscii = 0
        Call MySendKeys("{tab}")
        
    End If
End Sub

Private Sub Form_Load()
    
    
'    cmbGudang.text = "GUDANG1"
    reset_form
    total = 0
    seltab = 0
    conn.ConnectionString = strcon
    conn.Open
    
    conn.Close
    
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_bahan where [kode_bahan]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang1 = rs!nama_bahan

        cek_kodebarang1 = True
        

    Else
        LblNamaBarang1 = ""

        lblSatuan = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close


ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Function cek_barang(kode As String) As Boolean
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama_bahan] from ms_bahan where [kode_bahan]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        cek_barang = False
    End If
    rs.Close
    conn.Close
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function

Public Sub cek_supplier()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim kode As String
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select [nama_supplier] from ms_supplier where kode_supplier='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
        lblKategori = rs(1)
        
    Else
        lblNamaSupplier = ""
        lblKategori = ""
    End If
    rs.Close
    conn.Close
    If flxGrid.Rows > 2 Then hitung_ulang
err:
End Sub
Private Sub hitung_ulang()

End Sub

Private Sub listKuli_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listKuli.RemoveItem (listKuli.ListIndex)
End Sub

Private Sub listPengawas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then listPengawas.RemoveItem (listPengawas.ListIndex)
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub TabStrip1_Click()
    If TabStrip1.SelectedItem.index = 1 Then
        dbgrid.Visible = True
        frDetail.Visible = False
    Else
        dbgrid.Visible = False
        frDetail.Visible = True
    End If
    
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub

Private Sub txtNoOrder_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchPO_Click
End Sub

Private Sub txtNoOrder_LostFocus()
    cek_noTerima
End Sub
