VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Config"
   ClientHeight    =   2775
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   5520
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2775
   ScaleWidth      =   5520
   Begin VB.ComboBox cmbButtonColor 
      Height          =   315
      Left            =   1170
      TabIndex        =   7
      Text            =   "Combo1"
      Top             =   990
      Width           =   1140
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4770
      Top             =   2070
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ComboBox cmbBackColor 
      Height          =   315
      Left            =   1170
      TabIndex        =   6
      Text            =   "Combo1"
      Top             =   585
      Width           =   1140
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   4140
      Picture         =   "frmConfig.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3375
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1845
      Width           =   1125
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   765
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1845
      Width           =   1125
   End
   Begin VB.TextBox txtBGPicture 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1155
      TabIndex        =   1
      Top             =   180
      Width           =   2895
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Button Color :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   8
      Top             =   1035
      Width           =   930
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Back Color :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   5
      Top             =   630
      Width           =   930
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "BG Picture : "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   0
      Top             =   210
      Width           =   975
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim asc1 As New AscSecurity.Cls_security
Dim seltab As Byte
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    setConfig txtBGPicture.text, cmbBackColor, cmbButtonColor
    Unload Me
End Sub

Private Sub cmdSearchID_Click()
    CommonDialog1.Filter = "*.jpg|*.gif|*.bmp"
    CommonDialog1.filename = BG
    CommonDialog1.ShowOpen
    txtBGPicture.text = CommonDialog1.filename
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF9 Then frmDeleteTrans.Show vbModal
    If KeyCode = vbKeyF8 Then frmDelete.Show vbModal
End Sub

Private Sub Form_Load()
    getConfig
    txtBGPicture.text = BG
    cmbBackColor = BGColor1
    cmbButtonColor = btncolor
End Sub

Private Sub mnuCancel_Click()
Unload Me
End Sub

Private Sub mnuok_Click()
cmdOK_Click
End Sub

Public Sub cari_data(ByVal key As String)

End Sub

