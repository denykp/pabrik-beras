Attribute VB_Name = "ModuleUnPosting"
Sub Finish_Cetak_UnPosting(NoTransaksi As String, conn As ADODB.Connection)
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahInput As Double, hpp As Double, totalHpp As Double
Dim HPPTinta As Double, TotalHPPKertas As Double, TotalHPPTinta As Double
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim HPPPLat As Double, TotalHPPPlat As Double
Dim QtyStandart As Double, QtyManual As Double, HargaOutput As Double, selisih As Double
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim KodeBahan As String, KodeGudang As String, NoSerial As String, KodeMesin As String
Dim tanggal As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double, HPPBaru As Double
Dim AccInput As String, AccInputTinta As String, AccOutput As String
Dim NoJurnal As String
Dim NoHistory As String
Dim QtyGanti As Double, KodePlat As String
Dim Acc_PersediaanPlat As String
Dim Acc_BiayaTinta As String
Dim Acc_BiayaPlat As String
Dim SelisihJumlahInput As Double

On Error GoTo err

    rs.Open "select tanggal,kode_mesin,kode_bahan,kode_gudang,nomer_serial,qty,no_history from t_Cetakh where nomer_Cetak='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    KodeBahan = rs("kode_bahan")
    KodeMesin = rs("kode_mesin")
    NoSerial = rs("nomer_serial")
    JumlahInput = rs("qty")
    NoHistory = rs("no_history")
    hpp = GetHPPKertas2(KodeBahan, NoSerial, KodeMesin, conn)
    TotalHPPKertas = JumlahInput * hpp
    rs.Close
    
    AccInput = getAccBahan(KodeBahan, conn)
    AccSelisihHPP = getAcc("selisih cetak")
    
    
    
    rs.Open "select sum(qty) as QtyStandart, sum(qty_manual) as QtyManual  from t_Cetakd where nomer_Cetak='" & NoTransaksi & "'", conn
    QtyStandart = rs("QtyStandart")
    QtyManual = rs("QtyManual")
    rs.Close
    
    HargaOutput = TotalHPPKertas / QtyStandart
    selisih = (QtyManual - QtyStandart) * HargaOutput
    
    'Hitung jumlah input yg masih sisa utk dikurangkan ke stock
    SelisihJumlahInput = (QtyStandart - QtyManual) * JumlahInput / QtyStandart
    
    'Kurangi stock bahan input di mesin
    conn.Execute "update stock set stock=stock + " & Replace(SelisihJumlahInput, ",", ".") & " where kode_bahan='" & KodeBahan & "' " & _
                 "and kode_gudang='" & KodeMesin & "' and nomer_serial='" & NoSerial & "' "
    
    

    
    conn.Execute "Delete from t_jurnald where no_transaksi='" & NoTransaksi & "' and keterangan='Finish Cetak'"
    conn.Execute "Delete from t_jurnalh where no_transaksi='" & NoTransaksi & "' and keterangan='Finish Cetak'"
    
    conn.Execute "update t_cetakh set status_finish='0' where nomer_cetak='" & NoTransaksi & "'"

    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Sub Finish_Plong_UnPosting(NoTransaksi As String, conn As ADODB.Connection)
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahInput As Double, hpp As Double, totalHpp As Double
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim QtyManual As Double
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim KodeBahan As String, KodeGudang As String, NoSerial As String, KodeMesin As String
Dim tanggal As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double, HPPBaru As Double
Dim AccInput As String, AccOutput As String
Dim NoJurnal As String
Dim NoHistory As String
Dim QtyStandart As Double, HargaOutput As Double, selisih As Double
Dim SelisihJumlahInput As Double


On Error GoTo err

    rs.Open "select tanggal,kode_mesin,kode_bahan,kode_gudang,nomer_serial,qty,no_history from t_Plongh where nomer_Plong='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    KodeBahan = rs("kode_bahan")
    KodeMesin = rs("kode_mesin")
    NoSerial = rs("nomer_serial")
    JumlahInput = rs("qty")
    NoHistory = rs("no_history")
    hpp = GetHPPKertas2(KodeBahan, NoSerial, KodeMesin, conn)
    totalHpp = JumlahInput * hpp
    rs.Close
    
    AccInput = getAccBahan(KodeBahan, conn)
    AccSelisihHPP = getAcc("selisih plong")
    
    rs.Open "select sum(qty) as QtyStandart, sum(qty_manual) as QtyManual from t_Plongd where nomer_Plong='" & NoTransaksi & "'", conn
    QtyStandart = rs("QtyStandart")
    QtyManual = rs("QtyManual")
    rs.Close
    
    HargaOutput = totalHpp / QtyStandart
    selisih = (QtyManual - QtyStandart) * HargaOutput
    
    'Hitung jumlah input yg masih sisa utk dikurangkan ke stock
    SelisihJumlahInput = (QtyStandart - QtyManual) * JumlahInput / QtyStandart
    
    
    'Kurangi stock bahan input di mesin
    conn.Execute "update stock set stock=stock + " & Replace(SelisihJumlahInput, ",", ".") & " where kode_bahan='" & KodeBahan & "' " & _
                 "and kode_gudang='" & KodeMesin & "' and nomer_serial='" & NoSerial & "' "
    
    
    
'    NoJurnal = Nomor_Jurnal(tanggal)
'
'    If selisih < 0 Then
'        JurnalD NoTransaksi, NoJurnal, AccSelisihHPP, "D", -selisih
'        JurnalD NoTransaksi, NoJurnal, AccInput, "K", -selisih
'    Else
'        JurnalD NoTransaksi, NoJurnal, AccInput, "D", selisih
'        JurnalD NoTransaksi, NoJurnal, AccSelisihHPP, "K", selisih
'    End If
'
'    JurnalH NoTransaksi, NoJurnal, tanggal, "Proses Plong"

    conn.Execute "Delete from t_jurnald where no_transaksi='" & NoTransaksi & "' and keterangan='Finish Plong'"
    conn.Execute "Delete from t_jurnalh where no_transaksi='" & NoTransaksi & "' and keterangan='Finish Plong'"
    
    conn.Execute "update t_Plongh set status_finish='0' where nomer_Plong='" & NoTransaksi & "'"

    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub

Sub Finish_Potong_UnPosting(NoTransaksi As String, conn As ADODB.Connection)
Dim i As Integer
Dim id As String
Dim row As Integer
Dim JumlahInput As Double, hpp As Double, totalHpp As Double
Dim NilaiJurnalD As Double, NilaiJurnalH As Double
Dim QtyManual As Double
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim KodeBahan As String, KodeGudang As String, NoSerial As String, KodeMesin As String
Dim tanggal As Date
Dim JumlahLama As Double, jumlah As Double, HPPLama As Double, HPPBaru As Double
'Dim Harga As Double
Dim AccInput As String, AccOutput As String
Dim NoJurnal As String
Dim QtyStandart As Double, HargaOutput As Double, selisih As Double
Dim SelisihJumlahInput As Double

On Error GoTo err

    rs.Open "select tanggal,kode_mesin,kode_bahan,kode_gudang,nomer_serial,qty from t_potongh where nomer_potong='" & NoTransaksi & "'", conn
    tanggal = rs("tanggal")
    KodeBahan = rs("kode_bahan")
    KodeMesin = rs("kode_mesin")
    NoSerial = rs("nomer_serial")
    JumlahInput = rs("qty")
    hpp = GetHPPKertas2(KodeBahan, NoSerial, KodeMesin, conn)
    totalHpp = JumlahInput * hpp
    rs.Close
    
    AccInput = getAccBahan(KodeBahan, conn)
    AccSelisihHPP = getAcc("selisih potong")
    
    
    
    rs.Open "select sum(qty) as QtyStandart, sum(qty_manual) as QtyManual from t_potongd where nomer_potong='" & NoTransaksi & "'", conn
    QtyStandart = rs("QtyStandart")
    QtyManual = rs("QtyManual")
    rs.Close
    
    HargaOutput = totalHpp / QtyStandart
    selisih = (QtyManual - QtyStandart) * HargaOutput
    
    'Hitung jumlah input yg masih sisa utk dikurangkan ke stock
    SelisihJumlahInput = (QtyStandart - QtyManual) * JumlahInput / QtyStandart
    
    'Kurangi stock bahan input di mesin
    conn.Execute "update stock set stock=stock + " & Replace(SelisihJumlahInput, ",", ".") & " where kode_bahan='" & KodeBahan & "' " & _
                 "and kode_gudang='" & KodeMesin & "' and nomer_serial='" & NoSerial & "' "
    
    
    
'    NoJurnal = Nomor_Jurnal(tanggal)
'
'    If selisih < 0 Then
'        JurnalD NoTransaksi, NoJurnal, AccSelisihHPP, "D", -selisih
'        JurnalD NoTransaksi, NoJurnal, AccInput, "K", -selisih
'    Else
'        JurnalD NoTransaksi, NoJurnal, AccInput, "D", selisih
'        JurnalD NoTransaksi, NoJurnal, AccSelisihHPP, "K", selisih
'    End If
'
'    JurnalH NoTransaksi, NoJurnal, tanggal, "Proses Potong"
    
    conn.Execute "Delete from t_jurnald where no_transaksi='" & NoTransaksi & "' and keterangan='Finish Potong'"
    conn.Execute "Delete from t_jurnalh where no_transaksi='" & NoTransaksi & "' and keterangan='Finish Potong'"
    
    conn.Execute "update t_potongh set status_finish='0' where nomer_potong='" & NoTransaksi & "'"

    Exit Sub
err:
    conn.RollbackTrans
    DropConnection
    MsgBox err.Description
End Sub


