VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Begin VB.Form frmPenilaianKaryawan 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Penilaian Karyawan"
   ClientHeight    =   7050
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10920
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7050
   ScaleWidth      =   10920
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4590
      TabIndex        =   7
      Top             =   6435
      Width           =   1365
   End
   Begin VSFlex8LCtl.VSFlexGrid vsflex 
      Height          =   5460
      Left            =   135
      TabIndex        =   6
      Top             =   810
      Width           =   10590
      _cx             =   18680
      _cy             =   9631
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      BackColorFixed  =   -2147483633
      ForeColorFixed  =   -2147483630
      BackColorSel    =   -2147483635
      ForeColorSel    =   -2147483634
      BackColorBkg    =   -2147483636
      BackColorAlternate=   -2147483643
      GridColor       =   -2147483633
      GridColorFixed  =   -2147483632
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   1
      HighLight       =   1
      AllowSelection  =   -1  'True
      AllowBigSelection=   -1  'True
      AllowUserResizing=   0
      SelectionMode   =   0
      GridLines       =   1
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   1
      Cols            =   5
      FixedRows       =   1
      FixedCols       =   1
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   0   'False
      FormatString    =   $"frmPenilaianKaryawan.frx":0000
      ScrollTrack     =   0   'False
      ScrollBars      =   3
      ScrollTips      =   0   'False
      MergeCells      =   0
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   0
      AutoSearchDelay =   2
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   0
      OutlineCol      =   0
      Ellipsis        =   0
      ExplorerBar     =   0
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   2
      ShowComboButton =   1
      WordWrap        =   0   'False
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   3
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   0
      WallPaperAlignment=   9
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   24
   End
   Begin VB.Label lblTanggal 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1800
      TabIndex        =   5
      Top             =   450
      Width           =   1320
   End
   Begin VB.Label lblUserID 
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1800
      TabIndex        =   4
      Top             =   90
      Width           =   1320
   End
   Begin VB.Label Label4 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1530
      TabIndex        =   3
      Top             =   450
      Width           =   195
   End
   Begin VB.Label Label3 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1530
      TabIndex        =   2
      Top             =   90
      Width           =   195
   End
   Begin VB.Label Label2 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   135
      TabIndex        =   1
      Top             =   450
      Width           =   1320
   End
   Begin VB.Label Label1 
      Caption         =   "Penilai"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   135
      TabIndex        =   0
      Top             =   90
      Width           =   1320
   End
End
Attribute VB_Name = "frmPenilaianKaryawan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub load_datakaryawan()
    Dim conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    conn.Open strcon
    rs.Open "select kode_karyawan,nama_karyawan from absen_karyawan a left join ms_karyawan b on a.kode_karyawan = b.NIK where convert(varchar,tanggal,112) = '" & Format(Now, "yyyyMMdd") & "'", conn
    While Not rs.EOF
        vsflex.AddItem vbTab & rs!kode_karyawan & vbTab & rs!nama_karyawan & vbTab & 0 & vbTab
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub cmdSimpan_Click()
    Dim trans As Boolean
    Dim Row As Integer
    
    On Error GoTo err
    If MsgBox("Penilaian terhadap karyawan hanya bisa dilakukan satu kali per hari. " & _
                "Semua penilaian yang sudah tersimpan termasuk baris yang belum diisi, tidak dapat dirubah. " & _
                "Jika penilaian tidak diisi akan diaanggap tidak ada penilaian yang perlu dilaporkan." & vbCrLf & _
                "Apakah anda yakin ingin melanjutkan?", vbYesNo, "Peringatan") = vbNo Then
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    trans = False
    For Row = 1 To vsflex.Rows - 1
        add_data (Row)
    Next
    conn.CommitTrans
    trans = True
    MsgBox "Data sudah tersimpan"
    conn.Close
    Unload Me
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    
    If trans = False Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    lblUserID.Caption = User
    lbltanggal.Caption = Format(Now, "dd/MM/yyyy")
    load_datakaryawan
End Sub

Private Sub add_data(Row As Integer)
    Dim table_name As String
    Dim field() As String
    Dim nilai() As String
    
    ReDim field(5)
    ReDim nilai(5)
    
    table_name = "penilaian_karyawan"
    field(0) = "userid"
    field(1) = "tanggal"
    field(2) = "NIK"
    field(3) = "nilai"
    field(4) = "keterangan"
    
    nilai(0) = lblUserID.Caption
    nilai(1) = Format(Now, "MM/dd/yyyy")
    nilai(2) = vsflex.TextMatrix(Row, 1)
    nilai(3) = vsflex.TextMatrix(Row, 3)
    nilai(4) = vsflex.TextMatrix(Row, 4)
    
    tambah_data table_name, field, nilai
End Sub

Private Sub vsflex_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If Col = 0 Or Col = 1 Or Col = 2 Then Cancel = True
End Sub

