VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSettingUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New User"
   ClientHeight    =   5730
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   10485
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   10485
   Begin VB.CommandButton cmdAddUser 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   465
      Left            =   3240
      MaskColor       =   &H00000000&
      Picture         =   "frmNewUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   1755
      Width           =   465
   End
   Begin VB.TextBox txtlogin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   135
      TabIndex        =   0
      Top             =   585
      Width           =   3030
   End
   Begin VB.CommandButton cmdHapusLogin 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   465
      Left            =   3240
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmNewUser.frx":0702
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1170
      Width           =   465
   End
   Begin VB.CommandButton cmdAddLogin 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   465
      Left            =   3240
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmNewUser.frx":0E04
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   540
      Width           =   465
   End
   Begin VB.ListBox lstLogin 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4470
      Left            =   135
      TabIndex        =   3
      Top             =   1125
      Width           =   3030
   End
   Begin VB.ComboBox cmbGroup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5175
      TabIndex        =   5
      Text            =   "Combo1"
      Top             =   945
      Width           =   2130
   End
   Begin VB.CommandButton cmdDel 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6885
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1440
      Width           =   1125
   End
   Begin VB.CommandButton cmdSearchID 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   8145
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmNewUser.frx":1506
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   495
      Width           =   420
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8460
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   1440
      Width           =   1125
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Add"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5265
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   1440
      Width           =   1125
   End
   Begin VB.TextBox txtUserID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   5160
      TabIndex        =   2
      Top             =   585
      Width           =   2895
   End
   Begin MSDataGridLib.DataGrid DBGrid 
      Height          =   3660
      Left            =   4140
      TabIndex        =   14
      Top             =   1935
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   6456
      _Version        =   393216
      AllowUpdate     =   0   'False
      BackColor       =   16777215
      BorderStyle     =   0
      HeadLines       =   1
      RowHeight       =   21
      RowDividerStyle =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   8865
      Top             =   135
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   582
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Application Users"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4140
      TabIndex        =   13
      Top             =   135
      Width           =   2325
   End
   Begin VB.Line Line1 
      X1              =   3870
      X2              =   3870
      Y1              =   180
      Y2              =   5580
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Login"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   135
      TabIndex        =   12
      Top             =   135
      Width           =   975
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Group :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4140
      TabIndex        =   11
      Top             =   990
      Width           =   930
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4140
      TabIndex        =   9
      Top             =   615
      Width           =   975
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmSettingUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim asc1 As New Cls_security
Dim seltab As Byte

Private Sub cmdAddLogin_Click()
On Error Resume Next
Dim conn As New ADODB.Connection
    conn.Open strcon
    conn.Execute "DROP LOGIN " & txtlogin & ""
    conn.Execute "CREATE LOGIN " & txtlogin & " WITH PASSWORD = '" & txtlogin & "', DEFAULT_DATABASE= master, CHECK_POLICY = OFF "
    conn.Close
    txtlogin.text = ""
    load_combo
End Sub

Private Sub cmdAddUser_Click()
    For i = 0 To lstLogin.ListCount - 1
        If lstLogin.Selected(i) Then txtUserID = lstLogin.list(i)
        cari_data
    Next
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdDel_Click()
Dim i As Byte
On Error GoTo err
    If txtUserID = "" Then Exit Sub
    i = 0
    conn.ConnectionString = strcon
    conn.Open
'    conn.BeginTrans
    i = 1
    'conn.Execute "delete from userid where userid='" & txtUserID.text & "'"
    conn.Execute "delete from user_group where [group]='" & cmbGroup.text & "' and username='" & txtUserID.text & "'"
    asc1.deluser txtUserID.text, conn
'    asc1.dellogin txtUserID.text, modul, conn
'    conn.CommitTrans
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
'    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdHapusLogin_Click()
On Error Resume Next
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim rs1 As New ADODB.Recordset
Dim login As String
    For i = 0 To lstLogin.ListCount - 1
        If lstLogin.Selected(i) Then login = lstLogin.list(i)
    Next
    If MsgBox("Yakin hendak menghapus login " & login & "? Login ini mungkin dipakai di database lain", vbYesNo) = vbNo Then Exit Sub
    conn.Open strcon
    
    rs.Open "select * from sys.databases", conn
    While Not rs.EOF
        
        rs1.Open "select * from " & rs!Name & ".sys.sys.database_principals where [name]='" & login & "'", conn
        If Not rs.EOF Then
            conn.Execute "DROP schema " & login & ""
            conn.Execute "drop user " & login
        End If
        rs1.Close
        rs.MoveNext
    Wend
    rs.Close
    conn.Execute "drop login " & login
    conn.Close
    load_combo
End Sub

Private Sub cmdOk_Click()
Dim result, result1, result2 As String
Dim counter As Byte
    If txtUserID.text <> "" Then


        conn.ConnectionString = strcon
        conn.Open
        On Error Resume Next
        
        
'        conn.BeginTrans
    
'        Set rs = conn.Execute("select * from userid where userid='" & txtUserID & "'")
'        If Not rs.EOF Then
'            conn.Execute "delete from userid where userid='" & txtUserID & "'"
'        End If
'        rs.Close
'        conn.Execute "insert into [userid] values ('" & txtUserID & "')"

'        addlogin = "1"
'        conn.CommitTrans
 
        conn.Execute "EXEC sp_adduser '" & txtUserID & "', '" & txtUserID & "', 'db_owner'"

B:
        On Error Resume Next

        conn.Execute "delete from user_group where username='" & txtUserID.text & "' and modul='" & modul & "'"
        conn.Execute "insert into user_group values('" & txtUserID.text & "','" & cmbGroup.text & "','" & modul & "')"

'        rs.Open "select * from var_usergroup where [group]='" & cmbGroup.text & "'", conn
'        If Not rs.EOF Then
'            If rs("adduser") Then
'                conn.Execute "exec sp_addrolemember 'db_securityadmin', '" & txtUserID.text & "'"
'                conn.Execute "EXEC master.dbo.sp_addsrvrolemember @loginame = N'" & txtUserID.text & "', @rolename = N'sysadmin'"
'            End If
'
'            'If rs("backuprestore") Then conn.Execute "exec sp_addrolemember 'db_backupoperator', '" & txtUserID.text & "'"
'        End If
'        rs.Close
'        conn.Execute "exec sp_addrolemember 'db_owner', '" & txtUserID.text & "'"
        conn.Close
        If result = "1" Or result = "" Then
            MsgBox "New User Added"
            reset_form
        Else
            MsgBox result
        End If

    Else
        MsgBox "semua field harus diisi"
    End If
    refresh_grid
End Sub
Private Sub reset()
    txtUserID.text = ""

End Sub

Private Sub refresh_grid()
    Adodc1.RecordSource = "select * from user_group where [username] like '%" & txtUserID & "%' and modul='" & modul & "' order by username"
    Set DBGrid.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid.Refresh
End Sub

Private Sub cmdSearchID_Click()
    refresh_grid
End Sub

Private Sub DBGrid_DblClick()
    txtUserID = DBGrid.Columns(0).text
    cari_data
End Sub

Private Sub DBGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtUserID = DBGrid.Columns(0).text
        cari_data
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then Unload Me

End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
    Adodc1.ConnectionString = strcon
    load_combo
    refresh_grid
    seltab = 0
End Sub
Private Sub load_combo()
    
    conn.ConnectionString = strcon
    conn.Open
    cmbGroup.clear
    rs.Open "select * from var_usergroup where  modul='" & modul & "'", conn
    While Not rs.EOF
        cmbGroup.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    lstLogin.clear
    rs.Open "select name from sys.server_principals where [type]='S' order by name", conn
    While Not rs.EOF
        lstLogin.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
'    rs.Open "select * from ms_gudang", conn
'    While Not rs.EOF
'        DBGrid.AddItem "#" & rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close

    conn.Close
End Sub
Private Sub mnuCancel_Click()
Unload Me
End Sub

Private Sub mnuok_Click()
cmdOk_Click
End Sub

Public Sub cari_data()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    If txtUserID.text <> "" Then

        rs.Open "select [group] from user_group where username='" & txtUserID.text & "'", conn
        If Not rs.EOF Then
            SetComboText rs(0), cmbGroup
        Else
            cmbGroup.ListIndex = -1
        End If
        rs.Close
'
    End If
    conn.Close
End Sub

Private Sub txtUserID_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then cari_data
End Sub

Private Sub txtUserID_LostFocus()
    cari_data
End Sub
Private Sub reset_form()
txtUserID = ""
cmbGroup.text = ""
Adodc1.Refresh
DBGrid.Refresh
End Sub
